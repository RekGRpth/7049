.class public Lcom/mediatek/bluetooth/hid/BluetoothHidService;
.super Landroid/app/Service;
.source "BluetoothHidService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;,
        Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;
    }
.end annotation


# static fields
.field private static final BT_HID_NOT_FOUNT:Ljava/lang/String; = "BT_HID_NOT_FOUNT"

.field private static final BT_HID_SETTING_INFO:Ljava/lang/String; = "BT_HID_SETTING_INFO"

.field private static final DEBUG:Z = true

.field public static final FINISH_ACTION:Ljava/lang/String; = "com.mediatek.bluetooth.hid.finish"

.field private static final HID_ID_START:I = 0xa

.field private static final TAG:Ljava/lang/String; = "[BT][HID][BluetoothHidService]"

.field private static hid_connect_notify:I

.field private static service_disable:Z


# instance fields
.field cx:Landroid/content/Context;

.field private mBluetoothService:Landroid/bluetooth/IBluetooth;

.field private mConnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

.field private mDisconnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

.field private final mHid:Landroid/bluetooth/IBluetoothHid$Stub;

.field private final mHidServerNotify:Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;

.field private mNM:Landroid/app/NotificationManager;

.field private mNativeData:I

.field mPreference:Landroid/preference/Preference;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mServerState:I

.field private mServiceHandler:Landroid/os/Handler;

.field private mSocketListener:Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;

.field notifyMap:Ljava/util/Map;

.field pc:Landroid/preference/PreferenceCategory;

.field stateMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field update_state_intent:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "exthid_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    const/16 v0, 0xb

    sput v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->hid_connect_notify:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object p0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->cx:Landroid/content/Context;

    iput-object v2, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mNM:Landroid/app/NotificationManager;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->notifyMap:Ljava/util/Map;

    iput-object v2, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mPreference:Landroid/preference/Preference;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.bluetooth.profilemanager.action.PROFILE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->update_state_intent:Landroid/content/Intent;

    iput-object v2, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mSocketListener:Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;

    iput-object v2, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mConnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    iput-object v2, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mDisconnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    new-instance v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$1;-><init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mHid:Landroid/bluetooth/IBluetoothHid$Stub;

    new-instance v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$2;-><init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mHidServerNotify:Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;

    new-instance v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$3;-><init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mServiceHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$4;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$4;-><init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->listentoSocketNative()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->wakeupListenerNative()V

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;IZ)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->updateActivityUI(Ljava/lang/String;IZ)V

    return-void
.end method

.method static synthetic access$1100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->getBluetoothDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/bluetooth/BluetoothDevice;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->updateSettingsState(Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverUnplugReqNative(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverSendReportReqNative(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverSetReportReqNative(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverGetReportReqNative(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverSetProtocolReqNative(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverGetProtocolReqNative(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverSetIdleReqNative(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverDisconnectReqNative(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverGetIdleReqNative(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverAuthorizeReqNative(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$2300(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->getDeviceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2402(Lcom/mediatek/bluetooth/hid/BluetoothHidService;I)I
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mServerState:I

    return p1
.end method

.method static synthetic access$2500(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->updateDeviceState(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2600()I
    .locals 1

    sget v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->hid_connect_notify:I

    return v0
.end method

.method static synthetic access$2608()I
    .locals 2

    sget v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->hid_connect_notify:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->hid_connect_notify:I

    return v0
.end method

.method static synthetic access$2700(Lcom/mediatek/bluetooth/hid/BluetoothHidService;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/Notification;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    invoke-direct/range {p0 .. p5}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->genHidNotification(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2800(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Landroid/app/NotificationManager;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mNM:Landroid/app/NotificationManager;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mConnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    iput-object p1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mConnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    return-object p1
.end method

.method static synthetic access$300(Lcom/mediatek/bluetooth/hid/BluetoothHidService;ILjava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->sendServiceMsg(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$3000()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->service_disable:Z

    return v0
.end method

.method static synthetic access$3100(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mDisconnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;)Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    iput-object p1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mDisconnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    return-object p1
.end method

.method static synthetic access$500(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->getDeviceState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->connectHidDevice(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->disconnectHidDevice(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverActivateReqNative()V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/bluetooth/hid/BluetoothHidService;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/hid/BluetoothHidService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverDeactivateReqNative()V

    return-void
.end method

.method private native cleanServiceNative()V
.end method

.method private connectHidDevice(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mConnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;-><init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Lcom/mediatek/bluetooth/hid/BluetoothHidService$1;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mConnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mConnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    const-string v1, "hidConnectTimeoutThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mConnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    iput-object p1, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->BT_Addr:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mConnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    const-string v1, "connected"

    iput-object v1, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->state:Ljava/lang/String;

    const/16 v0, 0x69

    iput v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mServerState:I

    const v0, 0x7f0600b3

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->updateActivityUI(Ljava/lang/String;IZ)V

    const-string v0, "connecting"

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->getBluetoothDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->updateSettingsState(Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V

    const-string v0, "connecting"

    invoke-direct {p0, p1, v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->updateDeviceState(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverConnectReqNative(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mConnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mConnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_1
    return-void
.end method

.method private convertStatusToInt(Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "connected"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const-string v1, "connecting"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "authorize"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const-string v1, "disconnect"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "disconnecting"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "unplug"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_5
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private disconnectHidDevice(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "connected"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mDisconnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;-><init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Lcom/mediatek/bluetooth/hid/BluetoothHidService$1;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mDisconnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mDisconnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    const-string v1, "hidDisconnectTimeoutThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mDisconnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    iput-object p1, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->BT_Addr:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mDisconnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    const-string v1, "disconnect"

    iput-object v1, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->state:Ljava/lang/String;

    const/16 v0, 0x6a

    iput v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mServerState:I

    const v0, 0x7f0600c6

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->updateActivityUI(Ljava/lang/String;IZ)V

    const-string v0, "disconnecting"

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->getBluetoothDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->updateSettingsState(Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V

    const-string v0, "disconnecting"

    invoke-direct {p0, p1, v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->updateDeviceState(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverDisconnectReqNative(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mDisconnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mDisconnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "error state to disconnect"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private native forceClearServerNative()V
.end method

.method private genHidNotification(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/Notification;
    .locals 11
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const/4 v7, 0x0

    const/4 v5, 0x0

    const/4 v2, -0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v8, "[BT][HID][BluetoothHidService]"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "genHidNotification "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v2, 0x7f020020

    const-class v8, Lcom/mediatek/bluetooth/hid/BluetoothHidAlert;

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "device_addr"

    invoke-virtual {v8, v9, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "connected"

    invoke-virtual {p4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    const v8, 0x7f0600b7

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v8, 0x7f0600b8

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v7, Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v7, v2, v3, v8, v9}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    const/4 v8, 0x2

    iput v8, v7, Landroid/app/Notification;->flags:I

    const-string v8, "action"

    const-string v9, "disconnect"

    invoke-virtual {v6, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const/high16 v9, 0x10000000

    invoke-static {v8, p1, v6, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    const v8, 0x7f0600bb

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p2, v9, v10

    invoke-virtual {p0, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v1, v4, v8, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    :cond_0
    :goto_0
    return-object v7

    :cond_1
    const-string v8, "authorize"

    invoke-virtual {p4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const v8, 0x7f0600b9

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v8, 0x7f0600ba

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v7, Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v7, v2, v3, v8, v9}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    const/16 v8, 0x8

    iput v8, v7, Landroid/app/Notification;->flags:I

    if-eqz p5, :cond_2

    iget v8, v7, Landroid/app/Notification;->defaults:I

    or-int/lit8 v8, v8, 0x1

    iput v8, v7, Landroid/app/Notification;->defaults:I

    iget v8, v7, Landroid/app/Notification;->defaults:I

    or-int/lit8 v8, v8, 0x2

    iput v8, v7, Landroid/app/Notification;->defaults:I

    :cond_2
    const-string v8, "action"

    const-string v9, "authorize"

    invoke-virtual {v6, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const/high16 v9, 0x10000000

    invoke-static {v8, p1, v6, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    const v8, 0x7f0600bc

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p2, v9, v10

    invoke-virtual {p0, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v1, v4, v8, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private getBluetoothDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    return-object v0
.end method

.method private getDeviceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    const-string v4, "BT_HID_SETTING_INFO"

    invoke-virtual {p0, v4, v5}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const/4 v0, 0x0

    const-string v4, "preferenceCount"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deviceAddr"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "BT_HID_NOT_FOUNT"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deviceName"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "BT_HID_NOT_FOUNT"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_1
    return-object v4

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private getDeviceState(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private hasOtherConnectedHidDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 6
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "connected"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private native initServiceNative()Z
.end method

.method private native listentoSocketNative()Z
.end method

.method private printLog(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "[BT][HID][BluetoothHidService]"

    invoke-static {v0, p1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private sendServiceMsg(ILjava/lang/String;)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    const-string v2, "[BT][HID][BluetoothHidService]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendServiceMsg status="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "address="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    const/4 v2, 0x3

    if-ne p1, v2, :cond_1

    :cond_0
    const/16 v2, 0x68

    iput v2, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mServerState:I

    :cond_1
    if-nez p1, :cond_2

    const/16 v2, 0x64

    iput v2, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mServerState:I

    :cond_2
    iput p1, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "device_addr"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mServiceHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private native serverActivateReqNative()V
.end method

.method private native serverAuthorizeReqNative(Ljava/lang/String;Z)V
.end method

.method private native serverConnectReqNative(Ljava/lang/String;)V
.end method

.method private native serverDeactivateReqNative()V
.end method

.method private native serverDisconnectReqNative(Ljava/lang/String;)V
.end method

.method private native serverGetIdleReqNative(Ljava/lang/String;)V
.end method

.method private native serverGetProtocolReqNative(Ljava/lang/String;)V
.end method

.method private native serverGetReportReqNative(Ljava/lang/String;)V
.end method

.method private native serverSendReportReqNative(Ljava/lang/String;)V
.end method

.method private native serverSetIdleReqNative(Ljava/lang/String;)V
.end method

.method private native serverSetProtocolReqNative(Ljava/lang/String;)V
.end method

.method private native serverSetReportReqNative(Ljava/lang/String;)V
.end method

.method private native serverUnplugReqNative(Ljava/lang/String;)V
.end method

.method private native stopListentoSocketNative()V
.end method

.method private updateActivityUI(Ljava/lang/String;IZ)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z

    const-string v1, "[BT][HID][BluetoothHidService]"

    const-string v2, "updateActivityUI"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.BluetoothHidActivity.ACTION_SUMMARY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.mediatek.bluetooth.BluetoothHidActivity.extra.EXTRA_DEVICE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.BluetoothHidActivity.extra.EXTRA_SUMMARY"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.mediatek.bluetooth.BluetoothHidActivity.extra.EXTRA_ENABLE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private updateDeviceState(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private updateSettingsState(Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/bluetooth/BluetoothDevice;

    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stateMap:Ljava/util/Map;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->convertStatusToInt(Ljava/lang/String;)I

    move-result v2

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->convertStatusToInt(Ljava/lang/String;)I

    move-result v0

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {v3, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v4, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "disconnect"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "disconnect"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->hasOtherConnectedHidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    invoke-virtual {p0, v3}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mBluetoothService:Landroid/bluetooth/IBluetooth;

    const/4 v5, 0x4

    invoke-interface {v4, p2, v5, v0, v2}, Landroid/bluetooth/IBluetooth;->sendConnectionStateChange(Landroid/bluetooth/BluetoothDevice;III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v4, "[BT][HID][BluetoothHidService]"

    const-string v5, "updateSettingsState"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v1

    const-string v4, "[BT][HID][BluetoothHidService]"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sendConnectionStateChange Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private native wakeupListenerNative()V
.end method


# virtual methods
.method localClearService()V
    .locals 7

    const/16 v6, 0x68

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mServerState:I

    if-eq v3, v6, :cond_0

    const/4 v3, 0x1

    sput-boolean v3, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->service_disable:Z

    invoke-direct {p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverDeactivateReqNative()V

    :goto_0
    iget v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mServerState:I

    if-eq v3, v6, :cond_0

    const/16 v3, 0x1388

    if-lt v0, v3, :cond_5

    const/4 v2, 0x1

    :cond_0
    if-eqz v2, :cond_1

    const-string v3, "[BT][HID][BluetoothHidService]"

    const-string v4, "Waiting DEREGISTER_SERVER_CNF time-out. Force clear server context."

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput v6, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mServerState:I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->forceClearServerNative()V

    const/4 v3, 0x3

    invoke-direct {p0, v3, v5}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->sendServiceMsg(ILjava/lang/String;)V

    :cond_1
    iget-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mSocketListener:Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;

    if-eqz v3, :cond_2

    :try_start_0
    const-string v3, "mSocketListener close."

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->printLog(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mSocketListener:Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;->shutdown()V

    iget-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mSocketListener:Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;

    invoke-virtual {v3}, Ljava/lang/Thread;->join()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mSocketListener:Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;

    const-string v3, "mSocketListener close OK."

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->printLog(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mConnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    if-eqz v3, :cond_3

    :try_start_1
    const-string v3, "[BT][HID][BluetoothHidService]"

    const-string v4, "mConnectTimeout close."

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mConnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->shutdown()V

    iget-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mConnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    invoke-virtual {v3}, Ljava/lang/Thread;->join()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mConnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    const-string v3, "[BT][HID][BluetoothHidService]"

    const-string v4, "mConnectTimeout close OK."

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_3
    :goto_2
    iget-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mDisconnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    if-eqz v3, :cond_4

    :try_start_2
    const-string v3, "[BT][HID][BluetoothHidService]"

    const-string v4, "mDisconnectTimeout close."

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mDisconnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;->shutdown()V

    iget-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mDisconnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    invoke-virtual {v3}, Ljava/lang/Thread;->join()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mDisconnectTimeout:Lcom/mediatek/bluetooth/hid/BluetoothHidService$actionTimeoutThread;

    const-string v3, "[BT][HID][BluetoothHidService]"

    const-string v4, "mDisconnectTimeout close OK."

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_3

    :cond_4
    :goto_3
    invoke-direct {p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->stopListentoSocketNative()V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->cleanServiceNative()V

    return-void

    :cond_5
    const-wide/16 v3, 0x64

    :try_start_3
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    add-int/lit8 v0, v0, 0x64

    goto/16 :goto_0

    :catch_0
    move-exception v1

    const-string v3, "[BT][HID][BluetoothHidService]"

    const-string v4, "Waiting for server deregister-cnf was interrupted."

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :catch_1
    move-exception v1

    const-string v3, "[BT][HID][BluetoothHidService]"

    const-string v4, "mSocketListener close error."

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_2
    move-exception v1

    const-string v3, "[BT][HID][BluetoothHidService]"

    const-string v4, "mConnectTimeout close error."

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_3
    move-exception v1

    const-string v3, "[BT][HID][BluetoothHidService]"

    const-string v4, "mDisconnectTimeout close error."

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method localCreateService()V
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mServerState:I

    const/16 v1, 0x64

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->update_state_intent:Landroid/content/Intent;

    const-string v1, "android.bluetooth.profilemanager.extra.PROFILE"

    sget-object v2, Landroid/bluetooth/BluetoothProfileManager$Profile;->Bluetooth_HID:Landroid/bluetooth/BluetoothProfileManager$Profile;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->update_state_intent:Landroid/content/Intent;

    const-string v1, "android.bluetooth.profilemanager.extra.EXTRA_NEW_STATE"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->update_state_intent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->initServiceNative()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Succeed to init BluetoothHidService."

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->printLog(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mSocketListener:Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;-><init>(Lcom/mediatek/bluetooth/hid/BluetoothHidService;Lcom/mediatek/bluetooth/hid/BluetoothHidService$1;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mSocketListener:Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mSocketListener:Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;

    const-string v1, "BTHidSocketListener"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mSocketListener:Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;

    iput-boolean v3, v0, Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;->stopped:Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mSocketListener:Lcom/mediatek/bluetooth/hid/BluetoothHidService$SocketListenerThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const-string v0, "SocketListener started."

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->printLog(Ljava/lang/String;)V

    :cond_0
    sput-boolean v3, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->service_disable:Z

    const/16 v0, 0x68

    iput v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mServerState:I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->serverActivateReqNative()V

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "Pre-enable HID Server"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "Failed to init BluetoothHidService."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[BT][HID][BluetoothHidService]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enter onBind(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-class v1, Landroid/bluetooth/IBluetoothHid;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mHid:Landroid/bluetooth/IBluetoothHid$Stub;

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mHidServerNotify:Lcom/mediatek/bluetooth/hid/IBluetoothHidServerNotify$Stub;

    goto :goto_0
.end method

.method public onCreate()V
    .locals 4

    const-string v2, "Enter onCreate()"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->printLog(Ljava/lang/String;)V

    const-string v2, "notification"

    invoke-virtual {p0, v2}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    iput-object v2, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mNM:Landroid/app/NotificationManager;

    iget-object v2, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mNM:Landroid/app/NotificationManager;

    if-nez v2, :cond_0

    const-string v2, "[BT][HID][BluetoothHidService]"

    const-string v3, "Get Notification-Manager failed. Stop HID service."

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    :cond_0
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.bluetooth.device.action.NAME_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->localCreateService()V

    const-string v2, "bluetooth"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Bluetooth service not available"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-static {v0}, Landroid/bluetooth/IBluetooth$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetooth;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mBluetoothService:Landroid/bluetooth/IBluetooth;

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "[BT][HID][BluetoothHidService]"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/hid/BluetoothHidService;->localClearService()V

    return-void
.end method
