.class Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;
.super Landroid/os/Handler;
.source "BluetoothMapServerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/map/BluetoothMapServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mRegSrvCnt:I

.field final synthetic this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)V
    .locals 1

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->mRegSrvCnt:I

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 23
    .param p1    # Landroid/os/Message;

    const/16 v17, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "message received: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v20, v0

    sparse-switch v20, :sswitch_data_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const-string v21, "Unrecognized message"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const/16 v21, 0xb

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$100(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;I)V

    const/16 v20, 0x64

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const/16 v21, 0xb

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$200(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;I)V

    goto :goto_0

    :sswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/InstanceManager;->removeAllInstances()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const/16 v21, 0xd

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$100(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const/16 v21, 0xd

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$200(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$400(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)V

    goto :goto_0

    :sswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/InstanceManager;->getInstanceById(I)Lcom/mediatek/bluetooth/map/Instance;

    move-result-object v10

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg2:I

    move/from16 v20, v0

    if-ltz v20, :cond_1

    if-eqz v10, :cond_1

    invoke-virtual {v10}, Lcom/mediatek/bluetooth/map/Instance;->onInstanceRegistered()V

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/InstanceManager;->removeInstance(I)V

    goto/16 :goto_0

    :sswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/InstanceManager;->getInstanceById(I)Lcom/mediatek/bluetooth/map/Instance;

    move-result-object v10

    if-eqz v10, :cond_2

    invoke-virtual {v10}, Lcom/mediatek/bluetooth/map/Instance;->onInstanceDeregistered()V

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/InstanceManager;->removeInstance(I)V

    goto/16 :goto_0

    :sswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/InstanceManager;->getInstanceById(I)Lcom/mediatek/bluetooth/map/Instance;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$500(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    if-eqz v10, :cond_3

    invoke-virtual {v10, v6}, Lcom/mediatek/bluetooth/map/Instance;->onDeviceConnected(Landroid/bluetooth/BluetoothDevice;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$600(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/BluetoothMapNotification;

    move-result-object v20

    const/16 v21, 0x1

    const/16 v22, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v6, v2}, Lcom/mediatek/bluetooth/map/BluetoothMapNotification;->createNotification(ILandroid/bluetooth/BluetoothDevice;Z)V

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const-string v21, "invalid instance ID is received"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/InstanceManager;->getInstanceById(I)Lcom/mediatek/bluetooth/map/Instance;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$500(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    if-eqz v10, :cond_5

    invoke-virtual {v10, v6}, Lcom/mediatek/bluetooth/map/Instance;->onDeviceDisconnected(Landroid/bluetooth/BluetoothDevice;)V

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Lcom/mediatek/bluetooth/map/InstanceManager;->getState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v20

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$600(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/BluetoothMapNotification;

    move-result-object v20

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v1, v6}, Lcom/mediatek/bluetooth/map/BluetoothMapNotification;->removeNotification(ILandroid/bluetooth/BluetoothDevice;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$700(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)I

    move-result v20

    const/16 v21, 0xc

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_0

    const/16 v20, 0x65

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1, v10}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$800(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Z)V

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "invalid instance ID is received:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    goto :goto_1

    :sswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/InstanceManager;->getInstanceById(I)Lcom/mediatek/bluetooth/map/Instance;

    move-result-object v10

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    if-eqz v20, :cond_6

    if-nez v10, :cond_7

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const-string v21, "SetFolderRequest or instance is null"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Lcom/mediatek/bluetooth/map/cache/SetFolderRequest;

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/mediatek/bluetooth/map/Instance;->setFolder(Lcom/mediatek/bluetooth/map/cache/SetFolderRequest;)I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-virtual/range {v18 .. v18}, Lcom/mediatek/bluetooth/map/cache/SetFolderRequest;->getAddress()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v22, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v17

    invoke-static {v0, v1, v2, v3}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$900(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;II)Z

    goto/16 :goto_0

    :sswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/InstanceManager;->getInstanceById(I)Lcom/mediatek/bluetooth/map/Instance;

    move-result-object v10

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    if-eqz v20, :cond_8

    if-nez v10, :cond_9

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const-string v21, "address or instance is null"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v10}, Lcom/mediatek/bluetooth/map/Instance;->updateInbox()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v17

    invoke-static {v0, v4, v1, v2}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$1000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;II)Z

    goto/16 :goto_0

    :sswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/InstanceManager;->getInstanceById(I)Lcom/mediatek/bluetooth/map/Instance;

    move-result-object v10

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    if-eqz v20, :cond_a

    if-nez v10, :cond_b

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const-string v21, "address is null or instance is null"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    move-object/from16 v0, p1

    iget-object v14, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v14, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;

    invoke-virtual {v10, v14}, Lcom/mediatek/bluetooth/map/Instance;->getMessagelist(Lcom/mediatek/bluetooth/map/cache/MessageListRequest;)Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    move-result-object v15

    invoke-virtual {v14}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getAddress()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v0, v4, v1, v2, v15}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$1100(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;IILcom/mediatek/bluetooth/map/cache/MessageListObject;)Z

    goto/16 :goto_0

    :sswitch_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/InstanceManager;->getInstanceById(I)Lcom/mediatek/bluetooth/map/Instance;

    move-result-object v10

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    if-eqz v20, :cond_c

    if-nez v10, :cond_d

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const-string v21, "address is null or instance is null"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    const/4 v7, 0x0

    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v8, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;

    invoke-virtual {v8}, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;->getAddress()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$500(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    if-eqz v6, :cond_e

    invoke-virtual {v10}, Lcom/mediatek/bluetooth/map/Instance;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_e

    invoke-virtual {v10, v8}, Lcom/mediatek/bluetooth/map/Instance;->getFolderlist(Lcom/mediatek/bluetooth/map/cache/FolderListRequest;)[Lcom/mediatek/bluetooth/map/cache/FolderListObject;

    move-result-object v7

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v0, v4, v1, v2, v7}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$1200(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;II[Lcom/mediatek/bluetooth/map/cache/FolderListObject;)Z

    goto/16 :goto_0

    :sswitch_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/InstanceManager;->getInstanceById(I)Lcom/mediatek/bluetooth/map/Instance;

    move-result-object v10

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    if-eqz v20, :cond_f

    if-nez v10, :cond_10

    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const-string v21, "address is null or instance is null"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_10
    move-object/from16 v0, p1

    iget-object v12, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v12, Lcom/mediatek/bluetooth/map/cache/MessageRequest;

    invoke-virtual {v10, v12}, Lcom/mediatek/bluetooth/map/Instance;->getMessage(Lcom/mediatek/bluetooth/map/cache/MessageRequest;)Lcom/mediatek/bluetooth/map/cache/BMessageObject;

    move-result-object v13

    if-eqz v13, :cond_11

    const/16 v17, 0x1

    :cond_11
    invoke-virtual {v12}, Lcom/mediatek/bluetooth/map/cache/MessageRequest;->getAddress()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v17

    invoke-static {v0, v4, v1, v2, v13}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$1300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;IILcom/mediatek/bluetooth/map/cache/BMessageObject;)Z

    if-eqz v13, :cond_0

    invoke-virtual {v13}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->reset()V

    goto/16 :goto_0

    :sswitch_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/InstanceManager;->getInstanceById(I)Lcom/mediatek/bluetooth/map/Instance;

    move-result-object v10

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    if-eqz v20, :cond_12

    if-nez v10, :cond_13

    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const-string v21, "address is null or instance is null"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_13
    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/String;

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move/from16 v2, v22

    move/from16 v3, v17

    invoke-static {v0, v1, v2, v3}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$1400(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;II)Z

    goto/16 :goto_0

    :sswitch_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/InstanceManager;->getInstanceById(I)Lcom/mediatek/bluetooth/map/Instance;

    move-result-object v10

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    if-eqz v20, :cond_14

    if-nez v10, :cond_0

    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const-string v21, "address is null or instance is null"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_d
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    if-nez v20, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const-string v21, "address is null"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$500(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$600(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/BluetoothMapNotification;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Lcom/mediatek/bluetooth/map/BluetoothMapNotification;->getDeviceState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v19

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$600(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/BluetoothMapNotification;

    move-result-object v20

    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v6, v2}, Lcom/mediatek/bluetooth/map/BluetoothMapNotification;->createNotification(ILandroid/bluetooth/BluetoothDevice;Z)V

    goto/16 :goto_0

    :cond_16
    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v0, v6, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$1500(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Landroid/bluetooth/BluetoothDevice;Z)Z

    goto/16 :goto_0

    :cond_17
    const/16 v20, 0x6

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const-string v21, "the device is authorizing"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "unexpected state : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v0, v6, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$1500(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Landroid/bluetooth/BluetoothDevice;Z)Z

    goto/16 :goto_0

    :sswitch_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/InstanceManager;->getInstanceById(I)Lcom/mediatek/bluetooth/map/Instance;

    move-result-object v10

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    if-eqz v20, :cond_19

    if-nez v10, :cond_1a

    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const-string v21, "address is null or instance is null"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1a
    invoke-virtual {v10}, Lcom/mediatek/bluetooth/map/Instance;->getBMessageObject()Lcom/mediatek/bluetooth/map/cache/BMessageObject;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->reset()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/String;

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move/from16 v2, v22

    invoke-static {v0, v1, v2, v5}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$1600(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;ILcom/mediatek/bluetooth/map/cache/BMessageObject;)Z

    invoke-virtual {v10, v5}, Lcom/mediatek/bluetooth/map/Instance;->pushMessage(Lcom/mediatek/bluetooth/map/cache/BMessageObject;)Z

    goto/16 :goto_0

    :sswitch_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/InstanceManager;->getInstanceById(I)Lcom/mediatek/bluetooth/map/Instance;

    move-result-object v10

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    if-eqz v20, :cond_1b

    if-nez v10, :cond_1c

    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const-string v21, "address is null or instance is null"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1c
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v16, v0

    check-cast v16, Lcom/mediatek/bluetooth/map/cache/StatusSwitchRequest;

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/mediatek/bluetooth/map/Instance;->setMessageStatus(Lcom/mediatek/bluetooth/map/cache/StatusSwitchRequest;)Z

    move-result v20

    if-eqz v20, :cond_1d

    const/16 v17, 0x1

    :goto_2
    invoke-virtual/range {v16 .. v16}, Lcom/mediatek/bluetooth/map/cache/StatusSwitchRequest;->getAddress()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v21, v0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v17

    invoke-static {v0, v4, v1, v2}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$1700(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;II)Z

    goto/16 :goto_0

    :cond_1d
    const/16 v17, 0x0

    goto :goto_2

    :sswitch_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$500(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Lcom/mediatek/bluetooth/map/InstanceManager;->getInstanceByDevice(Landroid/bluetooth/BluetoothDevice;)Ljava/util/ArrayList;

    move-result-object v11

    const/4 v9, 0x0

    :goto_3
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v20

    if-ge v9, v0, :cond_1e

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/mediatek/bluetooth/map/Instance;

    invoke-virtual/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/Instance;->deregisterCallback()V

    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$700(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)I

    move-result v20

    const/16 v21, 0xc

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$800(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Z)V

    goto/16 :goto_0

    :sswitch_11
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v20, v0

    if-nez v20, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const-string v21, "fail to set up mns connection"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$500(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v21

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Lcom/mediatek/bluetooth/map/InstanceManager;->getInstanceByDevice(Landroid/bluetooth/BluetoothDevice;)Ljava/util/ArrayList;

    move-result-object v11

    const/4 v9, 0x0

    :goto_4
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v20

    if-ge v9, v0, :cond_0

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/mediatek/bluetooth/map/Instance;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/Instance;->registerCallback(Lcom/mediatek/bluetooth/map/Instance$Listener;)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    :sswitch_12
    invoke-static {}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->isPhoneRadioReady()Z

    move-result v20

    if-eqz v20, :cond_20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$1800(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)V

    goto/16 :goto_0

    :cond_20
    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->mRegSrvCnt:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->mRegSrvCnt:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->mRegSrvCnt:I

    move/from16 v20, v0

    const/16 v21, 0xa

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_21

    const/16 v20, 0x64

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v20

    const-wide/16 v21, 0x3e8

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-wide/from16 v2, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const-string v21, "Network is still not ready after 10s, so phone radio maybe is in error"

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_13
    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v10, Lcom/mediatek/bluetooth/map/Instance;

    if-eqz v10, :cond_0

    invoke-virtual {v10}, Lcom/mediatek/bluetooth/map/Instance;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/mediatek/bluetooth/map/Instance;->onDeviceDisconnected(Landroid/bluetooth/BluetoothDevice;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$700(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)I

    move-result v20

    const/16 v21, 0xc

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$1;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$800(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Z)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x9 -> :sswitch_8
        0xa -> :sswitch_9
        0xb -> :sswitch_a
        0xc -> :sswitch_b
        0xd -> :sswitch_c
        0xe -> :sswitch_d
        0xf -> :sswitch_e
        0x10 -> :sswitch_f
        0x11 -> :sswitch_10
        0x12 -> :sswitch_11
        0x64 -> :sswitch_12
        0x65 -> :sswitch_13
    .end sparse-switch
.end method
