.class Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;
.super Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub;
.source "BluetoothMapServerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/map/BluetoothMapServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public disableServer()V
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    const-string v2, "mSettingBinder:disableServer"

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$2400(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    const-string v2, "BluetoothMapServerService"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "map server enable setting"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public enableServer()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    const-string v2, "mSettingBinder:enableServer"

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$2300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    const-string v2, "BluetoothMapServerService"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "map server enable setting"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public getEmailAccount()J
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/map/InstanceManager;->getEmailAccount()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSims()[I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/map/InstanceManager;->getSims()[I

    move-result-object v0

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$2500(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Z

    move-result v0

    return v0
.end method

.method public registerCallback(Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback;)V
    .locals 1
    .param p1    # Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$2600(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    return-void
.end method

.method public registerSim(I)Z
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mSettingBinder:registerSim,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mediatek/bluetooth/map/InstanceManager;->registerSim(I)Z

    move-result v0

    return v0
.end method

.method public replaceAccount(J)Z
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/bluetooth/map/InstanceManager;->replaceAccount(J)Z

    move-result v0

    return v0
.end method

.method public unregisterCallback(Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback;)V
    .locals 1
    .param p1    # Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$2600(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    return-void
.end method

.method public unregisterSim(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mSettingBinder:unregisterSim, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$3;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mediatek/bluetooth/map/InstanceManager;->unregisterSim(I)Z

    return-void
.end method
