.class Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;
.super Landroid/bluetooth/IBluetoothMap$Stub;
.source "BluetoothMapServerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/map/BluetoothMapServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-direct {p0}, Landroid/bluetooth/IBluetoothMap$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public close(Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    return-void
.end method

.method public connect(Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    return-void
.end method

.method public disconnect(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    const-string v1, "disconnect"

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$2500(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    const-string v1, "error, the service has not been ready "

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, p1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public getConnectedDevices()[Landroid/bluetooth/BluetoothDevice;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    const-string v1, "getConnectedDevice"

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$2500(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    const-string v1, "error, the service has not been ready "

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/map/InstanceManager;->getConnectedDevices()[Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    return-object v0
.end method

.method public getState(Landroid/bluetooth/BluetoothDevice;)I
    .locals 2
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    const-string v1, "getState"

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$2500(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    const-string v1, "error, the service has not been ready "

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mediatek/bluetooth/map/InstanceManager;->getState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    return v0
.end method

.method public isConnected(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    const-string v1, "isConnetected"

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$2500(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    const-string v1, "error, the service has not been ready "

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerService$4;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerService;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerService;)Lcom/mediatek/bluetooth/map/InstanceManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mediatek/bluetooth/map/InstanceManager;->isConnected(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method
