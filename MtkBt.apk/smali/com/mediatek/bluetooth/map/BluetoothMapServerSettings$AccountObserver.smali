.class Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$AccountObserver;
.super Landroid/database/ContentObserver;
.source "BluetoothMapServerSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AccountObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;


# direct methods
.method public constructor <init>(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;Landroid/os/Handler;)V
    .locals 0
    .param p2    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$AccountObserver;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$AccountObserver;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;

    const-string v1, "AccountObserver: onChange"

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->access$100(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$AccountObserver;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->access$200(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$AccountObserver;->this$0:Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;)Lcom/mediatek/bluetooth/map/AccountListPreference;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/map/AccountListPreference;->onAccountChanged()V

    return-void
.end method
