.class public Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;
.super Ljava/lang/Object;
.source "BluetoothMapServerSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$AccountObserver;
    }
.end annotation


# static fields
.field private static final KEY_ACCOUNT:Ljava/lang/String; = "Accounts"

.field private static final KEY_EMAIL_ADDR:Ljava/lang/String; = "Email_addr"

.field private static final KEY_MAP_SERVER_ACCOUNT_INDEX:Ljava/lang/String; = "map_server_account_index"

.field private static final KEY_MAP_SERVER_CATEGORY:Ljava/lang/String; = "map_server_category"

.field private static final KEY_MAP_SERVER_ENABLE:Ljava/lang/String; = "map_server_enable"

.field private static final KEY_MAP_SERVER_SIM_INDEX:Ljava/lang/String; = "map_server_sim_index"

.field private static final KEY_SIM_CARD:Ljava/lang/String; = "Sim_card"

.field private static final KEY_SIM_ID:Ljava/lang/String; = "Sim_id"

.field private static final TAG:Ljava/lang/String; = "BluetoothMapServerSettings"

.field private static mSavedState:Landroid/os/Bundle;


# instance fields
.field private mAccountButtonListener:Landroid/content/DialogInterface$OnClickListener;

.field private mAccountIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAccountObserver:Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$AccountObserver;

.field private mAccountUri:Landroid/net/Uri;

.field private mCallback:Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback;

.field private mCategory:Landroid/preference/PreferenceCategory;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mDialog:Landroid/app/AlertDialog;

.field private mEmailAddresses:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mIntentFilter:Landroid/content/IntentFilter;

.field private mMapServerAccountIndex:Lcom/mediatek/bluetooth/map/AccountListPreference;

.field private mMapServerEnabler:Landroid/preference/CheckBoxPreference;

.field private mMapServerSimIndex:Lcom/mediatek/bluetooth/map/MultiSelectListPreference;

.field private mPreferredAccountId:J

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

.field private mSimButtonListener:Landroid/content/DialogInterface$OnClickListener;

.field private mSimCards:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSimIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSimItemListener:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

.field private parentActivity:Landroid/preference/PreferenceActivity;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mEmailAddresses:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountIds:Ljava/util/ArrayList;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mPreferredAccountId:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimCards:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimIds:Ljava/util/ArrayList;

    const-string v0, "content://com.android.email.provider/account"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountUri:Landroid/net/Uri;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mIntentFilter:Landroid/content/IntentFilter;

    new-instance v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$1;-><init>(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$2;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$2;-><init>(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimItemListener:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    new-instance v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$3;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$3;-><init>(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimButtonListener:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$4;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$4;-><init>(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountButtonListener:Landroid/content/DialogInterface$OnClickListener;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    new-instance v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$5;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$5;-><init>(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mCallback:Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback;

    new-instance v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$6;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$6;-><init>(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerEnabler:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->updateAccount()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;)Lcom/mediatek/bluetooth/map/AccountListPreference;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerAccountIndex:Lcom/mediatek/bluetooth/map/AccountListPreference;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;)Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    return-object v0
.end method

.method static synthetic access$402(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;)Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;
    .param p1    # Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    return-object p1
.end method

.method static synthetic access$500(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;I)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->updateMapState(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;)Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mCallback:Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->updateSim()V

    return-void
.end method

.method static synthetic access$800(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->registerAccountOberver()V

    return-void
.end method

.method private getEmailAccountInfo(Ljava/util/ArrayList;Ljava/util/ArrayList;)J
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    const-string v10, "_id"

    const-string v8, "emailAddress"

    const-string v9, "isDefault"

    const-wide/16 v11, -0x1

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v10, v3, v2

    const/4 v2, 0x1

    aput-object v8, v3, v2

    const/4 v2, 0x2

    aput-object v9, v3, v2

    const-string v2, "getEmailAccountInfo()"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountUri:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_0

    const-string v2, "fail to query email account"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    move-wide v13, v11

    :goto_0
    return-wide v13

    :cond_0
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    goto :goto_1

    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-wide v13, v11

    goto :goto_0
.end method

.method private isBtEnabled()Z
    .locals 2

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "BluetoothMapServerSettings"

    invoke-static {v0, p1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private registerAccountOberver()V
    .locals 4

    new-instance v0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$AccountObserver;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$AccountObserver;-><init>(Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountObserver:Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$AccountObserver;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/mediatek/bluetooth/map/Email;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountObserver:Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$AccountObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method private showWarningDialog()V
    .locals 4

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0600de

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0600e0

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountButtonListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0600e1

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountButtonListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0600df

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mDialog:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private updateAccount()V
    .locals 6

    const-string v2, "updateAccount"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mEmailAddresses:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountIds:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mEmailAddresses:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountIds:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v3}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->getEmailAccountInfo(Ljava/util/ArrayList;Ljava/util/ArrayList;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    invoke-interface {v2}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;->getEmailAccount()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mPreferredAccountId:J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    iget-wide v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mPreferredAccountId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountIds:Ljava/util/ArrayList;

    iget-wide v3, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mPreferredAccountId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    iput-wide v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mPreferredAccountId:J

    :try_start_1
    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    iget-wide v3, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mPreferredAccountId:J

    invoke-interface {v2, v3, v4}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;->replaceAccount(J)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    :goto_2
    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->updateAccountPreference()V

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_2

    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method private updateAccountPreference()V
    .locals 4

    const-string v1, "updateAccountPreference()"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mEmailAddresses:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountIds:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerAccountIndex:Lcom/mediatek/bluetooth/map/AccountListPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mEmailAddresses:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mEmailAddresses:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerAccountIndex:Lcom/mediatek/bluetooth/map/AccountListPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountIds:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountIds:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerAccountIndex:Lcom/mediatek/bluetooth/map/AccountListPreference;

    iget-wide v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mPreferredAccountId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerAccountIndex:Lcom/mediatek/bluetooth/map/AccountListPreference;

    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountIds:Ljava/util/ArrayList;

    iget-wide v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mPreferredAccountId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerAccountIndex:Lcom/mediatek/bluetooth/map/AccountListPreference;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerAccountIndex:Lcom/mediatek/bluetooth/map/AccountListPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mEmailAddresses:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerAccountIndex:Lcom/mediatek/bluetooth/map/AccountListPreference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateMapState(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x1

    const/16 v0, 0xb

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerEnabler:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerEnabler:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerEnabler:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0xd

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerEnabler:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerEnabler:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerEnabler:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f0600dc

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0
.end method

.method private updateSim()V
    .locals 10

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimIds:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v5, v8, [Z

    :try_start_0
    iget-object v8, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    invoke-interface {v8}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;->getSims()[I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    :goto_0
    if-eqz v7, :cond_1

    array-length v8, v7

    if-eqz v8, :cond_1

    move-object v0, v7

    array-length v4, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_1

    aget v6, v0, v2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "sim:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimIds:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    const/4 v8, -0x1

    if-eq v3, v8, :cond_0

    const/4 v8, 0x1

    aput-boolean v8, v5, v3

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v8, "invalid sim card index"

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    invoke-direct {p0, v5}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->updateSimPreference([Z)V

    return-void
.end method

.method private updateSimPreference([Z)V
    .locals 3
    .param p1    # [Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimCards:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerSimIndex:Lcom/mediatek/bluetooth/map/MultiSelectListPreference;

    if-nez v0, :cond_1

    const-string v0, "sim preference is null"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimCards:Ljava/util/ArrayList;

    const-string v1, "SIM1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimIds:Ljava/util/ArrayList;

    sget v1, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->SIM1:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimCards:Ljava/util/ArrayList;

    const-string v1, "SIM2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimIds:Ljava/util/ArrayList;

    sget v1, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->SIM2:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerSimIndex:Lcom/mediatek/bluetooth/map/MultiSelectListPreference;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimCards:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimCards:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/mediatek/bluetooth/map/MultiSelectListPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerSimIndex:Lcom/mediatek/bluetooth/map/MultiSelectListPreference;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimIds:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimIds:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerSimIndex:Lcom/mediatek/bluetooth/map/MultiSelectListPreference;

    invoke-virtual {v0, p1}, Lcom/mediatek/bluetooth/map/MultiSelectListPreference;->setSelectedItems([Z)V

    goto :goto_0
.end method


# virtual methods
.method public getPreferenceResourceId()I
    .locals 1

    const/high16 v0, 0x7f040000

    return v0
.end method

.method public onCreate(Landroid/preference/PreferenceActivity;)V
    .locals 4
    .param p1    # Landroid/preference/PreferenceActivity;

    const-string v1, "onCreate"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "fail to bind service"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    const-string v2, "map_server_enable"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerEnabler:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    const-string v2, "map_server_sim_index"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/map/MultiSelectListPreference;

    iput-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerSimIndex:Lcom/mediatek/bluetooth/map/MultiSelectListPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    const-string v2, "map_server_account_index"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/map/AccountListPreference;

    iput-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerAccountIndex:Lcom/mediatek/bluetooth/map/AccountListPreference;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerEnabler:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerEnabler:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerSimIndex:Lcom/mediatek/bluetooth/map/MultiSelectListPreference;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerSimIndex:Lcom/mediatek/bluetooth/map/MultiSelectListPreference;

    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_2
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerAccountIndex:Lcom/mediatek/bluetooth/map/AccountListPreference;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerAccountIndex:Lcom/mediatek/bluetooth/map/AccountListPreference;

    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_3
    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->updateAccountPreference()V

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->updateSimPreference([Z)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    const-string v1, "BluetoothMapServerSettings"

    const-string v2, "onDestroy()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountObserver:Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$AccountObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountObserver:Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings$AccountObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    :cond_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    if-eqz v1, :cond_2

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mCallback:Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback;

    invoke-interface {v1, v2}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;->unregisterCallback(Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 11
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onPreferenceChange(), key:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    if-nez v9, :cond_0

    :goto_0
    return v8

    :cond_0
    const-string v9, "map_server_enable"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerEnabler:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v9, v8}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v8, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerEnabler:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v8}, Landroid/preference/TwoStatePreference;->isChecked()Z

    move-result v8

    if-nez v8, :cond_2

    :try_start_0
    iget-object v8, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    invoke-interface {v8}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;->enableServer()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const/4 v3, 0x0

    :cond_1
    :goto_2
    move v8, v3

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    :try_start_1
    iget-object v8, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    invoke-interface {v8}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;->disableServer()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const-string v8, "map_server_account_index"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    move-object v8, p2

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const-string v8, "BluetoothMapServerSettings"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "MAP Server accpunt index Changed: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_2
    iget-object v8, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    invoke-interface {v8, v6, v7}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;->replaceAccount(J)Z

    move-result v8

    if-eqz v8, :cond_4

    iput-wide v6, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mPreferredAccountId:J
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    const/4 v3, 0x1

    :goto_3
    iget-object v8, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountIds:Ljava/util/ArrayList;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v8, p2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    const/4 v8, -0x1

    if-ne v1, v8, :cond_5

    const-string v8, "invalid index"

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    :goto_4
    iget-object v8, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerAccountIndex:Lcom/mediatek/bluetooth/map/AccountListPreference;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    const/4 v3, 0x0

    goto :goto_3

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    iget-object v8, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mEmailAddresses:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_6
    const-string v8, "map_server_sim_index"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    check-cast p1, Lcom/mediatek/bluetooth/map/MultiSelectListPreference;

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/MultiSelectListPreference;->getSelectedItems()[Z

    move-result-object v4

    const/4 v1, 0x0

    :goto_5
    if-eqz v4, :cond_8

    :try_start_3
    array-length v8, v4

    if-ge v1, v8, :cond_8

    aget-boolean v8, v4, v1

    if-eqz v8, :cond_7

    iget-object v9, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    iget-object v8, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimIds:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v9, v8}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;->registerSim(I)Z

    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_7
    iget-object v9, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mService:Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    iget-object v8, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mSimIds:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v9, v8}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;->unregisterSim(I)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_6

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->log(Ljava/lang/String;)V

    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_8
    const/4 v3, 0x1

    goto/16 :goto_2
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    if-eqz p1, :cond_0

    const-string v0, "Accounts"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountIds:Ljava/util/ArrayList;

    const-string v0, "Email_addr"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mEmailAddresses:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mEmailAddresses:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->updateAccountPreference()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    const-string v0, "BluetoothMapServerSettings"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mMapServerAccountIndex:Lcom/mediatek/bluetooth/map/AccountListPreference;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/map/AccountListPreference;->onResume()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "Accounts"

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mAccountIds:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "Email_addr"

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/BluetoothMapServerSettings;->mEmailAddresses:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method
