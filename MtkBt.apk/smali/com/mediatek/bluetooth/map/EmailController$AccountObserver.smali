.class Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;
.super Landroid/database/ContentObserver;
.source "EmailController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/map/EmailController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AccountObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/map/EmailController;


# direct methods
.method public constructor <init>(Lcom/mediatek/bluetooth/map/EmailController;Landroid/os/Handler;)V
    .locals 0
    .param p2    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;->this$0:Lcom/mediatek/bluetooth/map/EmailController;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;->this$0:Lcom/mediatek/bluetooth/map/EmailController;

    const-string v1, "AccountObserver:onChange"

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/map/EmailController;->access$000(Lcom/mediatek/bluetooth/map/EmailController;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;->this$0:Lcom/mediatek/bluetooth/map/EmailController;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;->this$0:Lcom/mediatek/bluetooth/map/EmailController;

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/EmailController;->access$100(Lcom/mediatek/bluetooth/map/EmailController;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/bluetooth/map/EmailController;->queryAccount(J)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;->this$0:Lcom/mediatek/bluetooth/map/EmailController;

    const-wide/16 v1, -0x1

    invoke-static {v0, v1, v2}, Lcom/mediatek/bluetooth/map/EmailController;->access$102(Lcom/mediatek/bluetooth/map/EmailController;J)J

    :cond_0
    return-void
.end method
