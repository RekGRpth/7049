.class Lcom/mediatek/bluetooth/map/EmailController;
.super Lcom/mediatek/bluetooth/map/Controller;
.source "EmailController.java"

# interfaces
.implements Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;
    }
.end annotation


# static fields
.field public static final CONTENT_BYTES:Ljava/lang/String; = "content_bytes"

.field private static final DEFAULT_ACCOUNT_ID_PROJECTION:[Ljava/lang/String;

.field private static final DEFAULT_MESSAGE_LIST_PROJECTION:[Ljava/lang/String;

.field private static final EMAIL_ADDRESS:Ljava/lang/String; = "emailAddress"

.field private static final IS_DEFAULT:Ljava/lang/String; = "isDefault"

.field private static final READ_STATUS_PROJECTION:[Ljava/lang/String;

.field private static final RECORD_ID:Ljava/lang/String; = "_id"


# instance fields
.field private final ACTION_DELIVER_RESULT:Ljava/lang/String;

.field private final ACTION_SEND:Ljava/lang/String;

.field private final ACTION_SEND_RESULT:Ljava/lang/String;

.field private final ACTION_UPDATE_INBOX:Ljava/lang/String;

.field private final ACTION_UPDATE_INBOX_RESULT:Ljava/lang/String;

.field private final COLUMN_ACCOUT:I

.field private final COLUMN_ATTACHMENT:I

.field private final COLUMN_BCC:I

.field private final COLUMN_CC:I

.field private final COLUMN_FROM:I

.field private final COLUMN_ID:I

.field private final COLUMN_LOADED:I

.field private final COLUMN_MAILBOX:I

.field private final COLUMN_MESSAGE_ID:I

.field private final COLUMN_READ:I

.field private final COLUMN_REPLAY:I

.field private final COLUMN_SIZE:I

.field private final COLUMN_SUBJECT:I

.field private final COLUMN_TIMESTATP:I

.field private final COLUMN_TO:I

.field private final CONTROLLER_NOT_AVAILABLE:I

.field private final DELAYED_TIMEOUT:I

.field private final EXTRA_ACCOUNT:Ljava/lang/String;

.field private final EXTRA_ACCOUNT_ARRAY:Ljava/lang/String;

.field private final EXTRA_FLAG:Ljava/lang/String;

.field private final EXTRA_MAILBOX_TYPE:Ljava/lang/String;

.field private final EXTRA_NEED_RESULT:Ljava/lang/String;

.field private final EXTRA_RESULT:Ljava/lang/String;

.field private final EXTRA_STREAM:Ljava/lang/String;

.field private final FAILURE:I

.field private final MESSAGE_SEND_TIMEOUT:I

.field private final MESSAGE_UPDATE_INBOX_TIMEOUT:I

.field private final NO_SAVE_INBOX:I

.field private final PROJECTION_COLUMM_TIMESTAMP:I

.field private final SAVE_TO_INBOX:I

.field private final SUCCESS:I

.field private final TAG:Ljava/lang/String;

.field private mAccountId:J

.field private mAccountObserver:Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;

.field private mContext:Landroid/content/Context;

.field private mEmailAddress:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mInstance:Lcom/mediatek/bluetooth/map/Instance;

.field private mMasId:I

.field private mMessageListObject:Lcom/mediatek/bluetooth/map/cache/MessageListObject;

.field private mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

.field private mMsgType:I

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "emailAddress"

    aput-object v1, v0, v3

    const-string v1, "isDefault"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/bluetooth/map/EmailController;->DEFAULT_ACCOUNT_ID_PROJECTION:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "timeStamp"

    aput-object v1, v0, v3

    const-string v1, "messageId"

    aput-object v1, v0, v4

    const-string v1, "flagRead"

    aput-object v1, v0, v5

    sput-object v0, Lcom/mediatek/bluetooth/map/EmailController;->READ_STATUS_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "subject"

    aput-object v1, v0, v3

    const-string v1, "timeStamp"

    aput-object v1, v0, v4

    const-string v1, "fromList"

    aput-object v1, v0, v5

    const-string v1, "replyToList"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "toList"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "flagAttachment"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "flagLoaded"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "flagRead"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "messageId"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "mailboxKey"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "bccList"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "ccList"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "size"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/bluetooth/map/EmailController;->DEFAULT_MESSAGE_LIST_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/mediatek/bluetooth/map/Instance;J)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/bluetooth/map/Instance;
    .param p3    # J

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/Controller;-><init>()V

    const-string v0, "EmailController"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->TAG:Ljava/lang/String;

    const-string v0, "com.android.email.action.DIRECT_SEND"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->ACTION_SEND:Ljava/lang/String;

    const-string v0, "com.android.email.action.SEND_RESULT"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->ACTION_SEND_RESULT:Ljava/lang/String;

    const-string v0, "com.android.email.action.DELIVER_RESULT"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->ACTION_DELIVER_RESULT:Ljava/lang/String;

    const-string v0, "com.android.email.action.UPDATE_INBOX"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->ACTION_UPDATE_INBOX:Ljava/lang/String;

    const-string v0, "com.android.email.action.UPDATE_INBOX_RESULT"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->ACTION_UPDATE_INBOX_RESULT:Ljava/lang/String;

    const-string v0, "com.android.email.extra.ACCOUNT"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->EXTRA_ACCOUNT:Ljava/lang/String;

    const-string v0, "com.android.email.extra.STREAM"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->EXTRA_STREAM:Ljava/lang/String;

    const-string v0, "com.android.email.extra.FLAG"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->EXTRA_FLAG:Ljava/lang/String;

    const-string v0, "com.android.email.extra.MAILBOX_TYPE"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->EXTRA_MAILBOX_TYPE:Ljava/lang/String;

    const-string v0, "com.android.email.extra.RESULT"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->EXTRA_RESULT:Ljava/lang/String;

    const-string v0, "com.android.email.extra.ACCOUNT_ARRAY"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->EXTRA_ACCOUNT_ARRAY:Ljava/lang/String;

    const-string v0, "com.android.email.extra.NEED_RESULT"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->EXTRA_NEED_RESULT:Ljava/lang/String;

    iput v1, p0, Lcom/mediatek/bluetooth/map/EmailController;->SAVE_TO_INBOX:I

    iput v2, p0, Lcom/mediatek/bluetooth/map/EmailController;->NO_SAVE_INBOX:I

    iput v1, p0, Lcom/mediatek/bluetooth/map/EmailController;->SUCCESS:I

    iput v2, p0, Lcom/mediatek/bluetooth/map/EmailController;->FAILURE:I

    const/16 v0, 0x7530

    iput v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->DELAYED_TIMEOUT:I

    iput v1, p0, Lcom/mediatek/bluetooth/map/EmailController;->MESSAGE_SEND_TIMEOUT:I

    iput v2, p0, Lcom/mediatek/bluetooth/map/EmailController;->MESSAGE_UPDATE_INBOX_TIMEOUT:I

    iput v3, p0, Lcom/mediatek/bluetooth/map/EmailController;->CONTROLLER_NOT_AVAILABLE:I

    iput v1, p0, Lcom/mediatek/bluetooth/map/EmailController;->COLUMN_ID:I

    iput v2, p0, Lcom/mediatek/bluetooth/map/EmailController;->COLUMN_SUBJECT:I

    iput v3, p0, Lcom/mediatek/bluetooth/map/EmailController;->COLUMN_TIMESTATP:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->COLUMN_FROM:I

    iput v4, p0, Lcom/mediatek/bluetooth/map/EmailController;->COLUMN_REPLAY:I

    const/4 v0, 0x5

    iput v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->COLUMN_TO:I

    const/4 v0, 0x6

    iput v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->COLUMN_ATTACHMENT:I

    const/4 v0, 0x7

    iput v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->COLUMN_LOADED:I

    const/16 v0, 0x8

    iput v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->COLUMN_READ:I

    const/16 v0, 0x9

    iput v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->COLUMN_MESSAGE_ID:I

    const/16 v0, 0xa

    iput v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->COLUMN_ACCOUT:I

    const/16 v0, 0xb

    iput v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->COLUMN_MAILBOX:I

    const/16 v0, 0xc

    iput v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->COLUMN_BCC:I

    const/16 v0, 0xd

    iput v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->COLUMN_CC:I

    const/16 v0, 0xe

    iput v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->COLUMN_SIZE:I

    iput v1, p0, Lcom/mediatek/bluetooth/map/EmailController;->PROJECTION_COLUMM_TIMESTAMP:I

    iput v4, p0, Lcom/mediatek/bluetooth/map/EmailController;->mMsgType:I

    new-instance v0, Lcom/mediatek/bluetooth/map/EmailController$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/map/EmailController$1;-><init>(Lcom/mediatek/bluetooth/map/EmailController;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/bluetooth/map/EmailController$2;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/map/EmailController$2;-><init>(Lcom/mediatek/bluetooth/map/EmailController;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/EmailController;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/mediatek/bluetooth/map/Instance;->getInstanceId()I

    move-result v0

    iput v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mMasId:I

    iput-object p2, p0, Lcom/mediatek/bluetooth/map/EmailController;->mInstance:Lcom/mediatek/bluetooth/map/Instance;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    iput-wide p3, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountId:J

    new-instance v0, Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    invoke-direct {v0}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mMessageListObject:Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/EmailController;->onStart()V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/map/EmailController;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/map/EmailController;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/map/EmailController;)J
    .locals 2
    .param p0    # Lcom/mediatek/bluetooth/map/EmailController;

    iget-wide v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountId:J

    return-wide v0
.end method

.method static synthetic access$102(Lcom/mediatek/bluetooth/map/EmailController;J)J
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/map/EmailController;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountId:J

    return-wide p1
.end method

.method private composeMessageItem(Landroid/database/Cursor;)Lcom/mediatek/bluetooth/map/cache/MessageItem;
    .locals 16
    .param p1    # Landroid/database/Cursor;

    new-instance v10, Lcom/mediatek/bluetooth/map/cache/MessageItem;

    invoke-direct {v10}, Lcom/mediatek/bluetooth/map/cache/MessageItem;-><init>()V

    const/4 v15, 0x0

    const/4 v7, 0x0

    const/4 v14, -0x1

    const-string v1, "composeMessageItem()"

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/EmailController;->convertLoadStatus(I)I

    move-result v14

    const/4 v1, -0x1

    if-ne v14, v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invalid recipient Status: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    const/4 v10, 0x0

    :goto_0
    return-object v10

    :cond_0
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/EmailController;->convertReadStatus(I)I

    move-result v13

    const/4 v1, -0x1

    if-ne v13, v1, :cond_1

    const/4 v13, 0x2

    :cond_1
    const/4 v9, 0x1

    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_3

    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    sget-object v1, Lcom/mediatek/bluetooth/map/Email;->ATTACHMENT_URI:Landroid/net/Uri;

    invoke-static {v1, v11, v12}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "size"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_3

    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    add-int/2addr v7, v1

    goto :goto_1

    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/bluetooth/map/EmailController;->mMsgType:I

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v1, v3, v4}, Lcom/mediatek/bluetooth/map/util/HandleUtil;->getHandle(IJ)J

    move-result-wide v3

    invoke-virtual {v10, v3, v4}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setHandle(J)V

    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSubject(Ljava/lang/String;)V

    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v10, v3, v4}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setDatetime(J)V

    const/4 v1, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/Address;->unpack(Ljava/lang/String;)[Lcom/mediatek/bluetooth/map/Address;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/Address;->toString([Lcom/mediatek/bluetooth/map/Address;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSenderAddr(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSenderName(Ljava/lang/String;)V

    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/Address;->unpack(Ljava/lang/String;)[Lcom/mediatek/bluetooth/map/Address;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/Address;->toString([Lcom/mediatek/bluetooth/map/Address;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setReplyAddr(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setRecipientName(Ljava/lang/String;)V

    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/Address;->unpack(Ljava/lang/String;)[Lcom/mediatek/bluetooth/map/Address;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/Address;->toString([Lcom/mediatek/bluetooth/map/Address;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/Address;->unpack(Ljava/lang/String;)[Lcom/mediatek/bluetooth/map/Address;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/Address;->toString([Lcom/mediatek/bluetooth/map/Address;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setRecipientAddr(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/mediatek/bluetooth/map/EmailController;->mMsgType:I

    invoke-virtual {v10, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setMsgType(I)V

    const/16 v1, 0xe

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v10, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSize(I)V

    invoke-virtual {v10, v9}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setText(Z)V

    invoke-virtual {v10, v14}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setRecipientStatus(I)V

    invoke-virtual {v10, v7}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setAttachSize(I)V

    invoke-virtual {v10, v13}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setReadStatus(I)V

    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setProtected(Z)V

    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setPriority(Z)V

    goto/16 :goto_0
.end method

.method private convertFilterToSelection(Lcom/mediatek/bluetooth/map/cache/MessageListRequest;Ljava/lang/StringBuilder;Ljava/util/ArrayList;)[Ljava/lang/String;
    .locals 10
    .param p1    # Lcom/mediatek/bluetooth/map/cache/MessageListRequest;
    .param p2    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/bluetooth/map/cache/MessageListRequest;",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getStartTime()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getEndTime()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getReadStatus()I

    move-result v5

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getFolder()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/EmailController;->convertMailBoxType(Ljava/lang/String;)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/EmailController;->getMaiboxId(I)J

    move-result-wide v3

    const-string v8, "accountKey=?"

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v8, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v8, " AND "

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "mailboxKey=?"

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-lez v8, :cond_0

    const-wide/16 v8, 0x0

    cmp-long v8, v0, v8

    if-lez v8, :cond_0

    const-string v8, " AND "

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "timeStamp"

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " between ? AND ?"

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 v8, 0x2

    if-ne v5, v8, :cond_2

    const-string v8, " AND "

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "flagRead=?"

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    const/4 v8, 0x0

    return-object v8

    :cond_2
    const/4 v8, 0x1

    if-ne v5, v8, :cond_1

    const-string v8, " AND "

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, "flagRead=?"

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private convertLoadStatus(I)I
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private convertMailBoxType(Ljava/lang/String;)I
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v0, -0x1

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    const-string v1, "inbox"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v1, "outbox"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    const-string v1, "sent"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x5

    goto :goto_0

    :cond_3
    const-string v1, "deleted"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v0, 0x6

    goto :goto_0

    :cond_4
    const-string v1, "draft"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x3

    goto :goto_0

    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "convertMailBoxType():invalid mail box type->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private convertMaskToProjection(Ljava/util/Set;)[Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    new-array v2, v3, [Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    sget-object v3, Lcom/mediatek/bluetooth/map/EmailController;->DEFAULT_MESSAGE_LIST_PROJECTION:[Ljava/lang/String;

    return-object v3
.end method

.method private convertReadStatus(I)I
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const-string v0, "error: the read status from email provider is invalid"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private deleteAttachment(J)V
    .locals 8
    .param p1    # J

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteAttachment():"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    sget-object v0, Lcom/mediatek/bluetooth/map/Email;->MESSAGE_ID_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_0

    :goto_0
    return-void

    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private getMaiboxId(I)J
    .locals 14
    .param p1    # I

    sget-object v1, Lcom/mediatek/bluetooth/map/Email;->MAILBOX_URI:Landroid/net/Uri;

    const-wide/16 v7, -0x1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v0

    const-string v3, "accountKey=? AND type=?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-wide v10, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMaiboxId(): mAccountId is "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v10, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountId:J

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "mailbox type is "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    iget-wide v10, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountId:J

    const-wide/16 v12, 0x0

    cmp-long v0, v10, v12

    if-gez v0, :cond_0

    const-string v0, "mAccountId is invalid"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    const-wide/16 v10, -0x1

    :goto_0
    return-wide v10

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    const-string v0, "no available mailbox for the account"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    move-wide v10, v7

    goto :goto_0

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Ljava/lang/Throwable;->printStackTrace()V

    const-wide/16 v10, -0x1

    goto :goto_0
.end method

.method private getMailboxType(J)I
    .locals 9
    .param p1    # J

    const/4 v8, 0x0

    const/4 v3, 0x0

    const/4 v7, -0x1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "type"

    aput-object v0, v2, v8

    sget-object v0, Lcom/mediatek/bluetooth/map/Email;->MAILBOX_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v7

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "EmailController"

    invoke-static {v0, p1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private reverseReadStatus(I)I
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const-string v0, "other map state"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public deleteMessage(J)Z
    .locals 19
    .param p1    # J

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "mailboxKey"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "flagAttachment"

    aput-object v5, v4, v2

    const/4 v8, 0x1

    const/4 v2, 0x6

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/EmailController;->getMaiboxId(I)J

    move-result-wide v13

    const-wide/16 v15, -0x1

    move-wide/from16 v10, p1

    sget-object v2, Lcom/mediatek/bluetooth/map/Email;->MESSAGE_URI:Landroid/net/Uri;

    invoke-static {v2, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteMessage(): id is "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "accountKey=? AND _id=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountId:J

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v6, v7

    const/4 v7, 0x1

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    if-nez v12, :cond_0

    const-string v2, "fail to find message"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v15

    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "oldMailboxId is "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide v0, v15

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ",newMailboxId is "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    cmp-long v2, v15, v13

    if-nez v2, :cond_2

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/mediatek/bluetooth/map/EmailController;->deleteAttachment(J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v2, "delete the record for ever"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/map/Controller;->mDeleteFolder:Ljava/util/HashMap;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const-wide/16 v15, -0x1

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_2
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "mailboxKey"

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v9, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v9, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/map/Controller;->mDeleteFolder:Ljava/util/HashMap;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    long-to-int v6, v15

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "move record to deleted folder"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public deregisterListener()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/mediatek/bluetooth/map/Controller;->deregisterListener()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountObserver:Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountObserver:Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iput-object v2, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountObserver:Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/EmailController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iput-object v2, p0, Lcom/mediatek/bluetooth/map/EmailController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    :cond_1
    return-void
.end method

.method public getAccount()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountId:J

    return-wide v0
.end method

.method public getMessage(Lcom/mediatek/bluetooth/map/cache/MessageRequest;)Lcom/mediatek/bluetooth/map/cache/BMessageObject;
    .locals 25
    .param p1    # Lcom/mediatek/bluetooth/map/cache/MessageRequest;

    new-instance v24, Lcom/mediatek/bluetooth/map/VCard;

    invoke-direct/range {v24 .. v24}, Lcom/mediatek/bluetooth/map/VCard;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageRequest;->isAttachDelivered()Z

    move-result v9

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageRequest;->getMessageId()J

    move-result-wide v14

    sget-object v4, Lcom/mediatek/bluetooth/map/EmailController;->DEFAULT_MESSAGE_LIST_PROJECTION:[Ljava/lang/String;

    sget-object v2, Lcom/mediatek/bluetooth/map/Email;->MESSAGE_URI:Landroid/net/Uri;

    invoke-static {v2, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMessage(): id is "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ",attachment is "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    if-eqz v13, :cond_0

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const-string v2, "no message record for the id"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    const/16 v17, 0x0

    :goto_0
    return-object v17

    :catch_0
    move-exception v10

    invoke-virtual {v10}, Ljava/lang/Throwable;->printStackTrace()V

    const/16 v17, 0x0

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/map/EmailController;->mInstance:Lcom/mediatek/bluetooth/map/Instance;

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/map/Instance;->getBMessageObject()Lcom/mediatek/bluetooth/map/cache/BMessageObject;

    move-result-object v17

    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setEncoding(I)Z

    const/4 v2, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setCharset(I)Z

    const/4 v2, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setLang(I)Z

    const/4 v2, 0x4

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setMessageType(I)Z

    const/4 v2, 0x3

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/bluetooth/map/Address;->unpackToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    const/16 v20, 0x0

    if-eqz v18, :cond_2

    const-string v2, "@"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    :cond_2
    if-eqz v20, :cond_3

    const/4 v2, 0x0

    aget-object v2, v20, v2

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lcom/mediatek/bluetooth/map/VCard;->setName(Ljava/lang/String;)V

    :cond_3
    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/map/VCard;->setEmail(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Lcom/mediatek/bluetooth/map/VCard;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setOrignator(Ljava/lang/String;)Z

    invoke-virtual/range {v24 .. v24}, Lcom/mediatek/bluetooth/map/VCard;->reset()V

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x5

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/bluetooth/map/Address;->unpack(Ljava/lang/String;)[Lcom/mediatek/bluetooth/map/Address;

    move-result-object v23

    move-object/from16 v8, v23

    array-length v12, v8

    const/4 v11, 0x0

    :goto_1
    if-ge v11, v12, :cond_5

    aget-object v21, v8, v11

    invoke-virtual/range {v21 .. v21}, Lcom/mediatek/bluetooth/map/Address;->getAddress()Ljava/lang/String;

    move-result-object v22

    const-string v2, "@"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    if-eqz v20, :cond_4

    const/4 v2, 0x0

    aget-object v2, v20, v2

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lcom/mediatek/bluetooth/map/VCard;->setName(Ljava/lang/String;)V

    :cond_4
    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/map/VCard;->setEmail(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Lcom/mediatek/bluetooth/map/VCard;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_5
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->addRecipient(Ljava/lang/String;)Z

    const/16 v2, 0x8

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/EmailController;->convertReadStatus(I)I

    move-result v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setReadStatus(I)V

    new-instance v16, Lcom/mediatek/bluetooth/map/mime/EmailMime;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v14, v15}, Lcom/mediatek/bluetooth/map/mime/EmailMime;-><init>(Landroid/content/ContentResolver;J)V

    :try_start_1
    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getFile()Ljava/io/File;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-static {v2, v0}, Lcom/mediatek/bluetooth/map/Rfc822Output;->writeTo(Ljava/io/File;Lcom/mediatek/bluetooth/map/mime/MimeBase;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getFile()Ljava/io/File;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setContentSize(Ljava/io/File;)Z

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :catch_1
    move-exception v10

    invoke-virtual {v10}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2
.end method

.method public getMessageList(Lcom/mediatek/bluetooth/map/cache/MessageListRequest;)Lcom/mediatek/bluetooth/map/cache/MessageListObject;
    .locals 26
    .param p1    # Lcom/mediatek/bluetooth/map/cache/MessageListRequest;

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getListSize()I

    move-result v15

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getListOffset()I

    move-result v14

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getMaxSubjectLen()I

    move-result v19

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getRecipient()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getOrignator()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getFolder()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/EmailController;->convertMailBoxType(Ljava/lang/String;)I

    move-result v16

    const-wide/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/map/EmailController;->mInstance:Lcom/mediatek/bluetooth/map/Instance;

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/map/Instance;->getMsgListRspCache()Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/mediatek/bluetooth/map/EmailController;->mMessageListObject:Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMessageList(): listsize is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",listoffset is"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", maxSubjectLen is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",mailbox is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    const/4 v12, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMessageList(): Account id is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountId:J

    invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    sget-object v6, Lcom/mediatek/bluetooth/map/EmailController;->DEFAULT_MESSAGE_LIST_PROJECTION:[Ljava/lang/String;

    if-ltz v15, :cond_0

    if-ltz v14, :cond_0

    if-gez v19, :cond_1

    :cond_0
    const-string v4, "message request is not avalaible"

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_1
    if-gez v16, :cond_2

    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v24

    move-object/from16 v3, v25

    invoke-direct {v0, v1, v2, v3}, Lcom/mediatek/bluetooth/map/EmailController;->convertFilterToSelection(Lcom/mediatek/bluetooth/map/cache/MessageListRequest;Ljava/lang/StringBuilder;Ljava/util/ArrayList;)[Ljava/lang/String;

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/mediatek/bluetooth/map/Email;->MESSAGE_URI:Landroid/net/Uri;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v8, v8, [Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    const-string v9, "timeStamp desc"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    if-nez v13, :cond_3

    const/4 v4, 0x0

    goto :goto_0

    :catch_0
    move-exception v10

    invoke-virtual {v10}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 v4, 0x0

    goto :goto_0

    :cond_3
    if-lez v15, :cond_4

    if-lez v14, :cond_4

    invoke-interface {v13, v14}, Landroid/database/Cursor;->move(I)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    const/4 v4, 0x0

    goto :goto_0

    :cond_4
    const/16 v20, 0x0

    :cond_5
    :goto_1
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_a

    if-eqz v15, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/map/EmailController;->mMessageListObject:Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;->getCurrentSize()I

    move-result v4

    if-ge v4, v15, :cond_a

    :cond_6
    if-eqz v22, :cond_7

    const-string v4, "toList"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    if-eqz v23, :cond_5

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_5

    :cond_7
    if-eqz v21, :cond_8

    const-string v4, "fromList"

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_5

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_5

    :cond_8
    add-int/lit8 v12, v12, 0x1

    if-lez v15, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/map/EmailController;->mMessageListObject:Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/mediatek/bluetooth/map/EmailController;->composeMessageItem(Landroid/database/Cursor;)Lcom/mediatek/bluetooth/map/cache/MessageItem;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;->addMessageItem(Lcom/mediatek/bluetooth/map/cache/MessageItem;)Z

    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/map/EmailController;->mMessageListObject:Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;->addSize(I)Z

    goto :goto_1

    :cond_a
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    if-eqz v20, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/map/EmailController;->mMessageListObject:Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;->setNewMessage()Z

    :cond_b
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->declineListOffset(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/map/EmailController;->mMessageListObject:Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    goto/16 :goto_0
.end method

.method public handleDeliverResult(JI)V
    .locals 2
    .param p1    # J
    .param p3    # I

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

    iget v1, p0, Lcom/mediatek/bluetooth/map/EmailController;->mMsgType:I

    invoke-interface {v0, p1, p2, v1, p3}, Lcom/mediatek/bluetooth/map/ControllerListener;->onMessageDelivered(JII)V

    return-void
.end method

.method public handleSendResult(JI)V
    .locals 2
    .param p1    # J
    .param p3    # I

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

    iget v1, p0, Lcom/mediatek/bluetooth/map/EmailController;->mMsgType:I

    invoke-interface {v0, p1, p2, v1, p3}, Lcom/mediatek/bluetooth/map/ControllerListener;->onMessageSent(JII)V

    return-void
.end method

.method public onStart()V
    .locals 0

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/EmailController;->deregisterListener()V

    return-void
.end method

.method public pushMessage(Lcom/mediatek/bluetooth/map/cache/BMessageObject;)Z
    .locals 13
    .param p1    # Lcom/mediatek/bluetooth/map/cache/BMessageObject;

    const/4 v7, 0x0

    const/4 v8, -0x1

    const-string v9, "pushMessage"

    invoke-direct {p0, v9}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->isTransparent()Z

    move-result v6

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getFolder()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    move v3, v8

    :goto_0
    const/4 v9, 0x4

    if-ne v3, v9, :cond_3

    if-eqz v6, :cond_2

    const/4 v5, 0x1

    :goto_1
    iget-wide v9, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountId:J

    const-wide/16 v11, -0x1

    cmp-long v9, v9, v11

    if-eqz v9, :cond_0

    if-ne v3, v8, :cond_4

    :cond_0
    const-string v8, "invalid account ID or invalid mailbox"

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    :goto_2
    return v7

    :cond_1
    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/EmailController;->convertMailBoxType(Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    :cond_3
    const/4 v5, 0x2

    goto :goto_1

    :cond_4
    new-instance v2, Landroid/content/Intent;

    const-string v8, "com.android.email.action.DIRECT_SEND"

    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v8, "com.android.email.extra.ACCOUNT"

    iget-wide v9, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountId:J

    invoke-virtual {v2, v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v8, "com.android.email.extra.STREAM"

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v8, "com.android.email.extra.FLAG"

    invoke-virtual {v2, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v8, 0x2

    if-ne v5, v8, :cond_5

    const-string v8, "com.android.email.extra.MAILBOX_TYPE"

    invoke-virtual {v2, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_5
    iget-object v8, p0, Lcom/mediatek/bluetooth/map/EmailController;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v8, p0, Lcom/mediatek/bluetooth/map/EmailController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    iget-object v7, p0, Lcom/mediatek/bluetooth/map/EmailController;->mHandler:Landroid/os/Handler;

    const-wide/16 v8, 0x7530

    invoke-virtual {v7, v4, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const/4 v7, 0x1

    goto :goto_2
.end method

.method public queryAccount(J)Z
    .locals 9
    .param p1    # J

    const/4 v7, 0x1

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/mediatek/bluetooth/map/Email;->ACCOUNT_URI:Landroid/net/Uri;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v8

    const-string v3, "_id=?"

    new-array v4, v7, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "account is not in database at current"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    move v0, v8

    :goto_0
    return v0

    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move v0, v7

    goto :goto_0
.end method

.method public queryAccounts(Ljava/util/Set;)Z
    .locals 9
    .param p1    # Ljava/util/Set;

    const/4 v8, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    if-nez p1, :cond_0

    move v0, v7

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/mediatek/bluetooth/map/Email;->ACCOUNT_URI:Landroid/net/Uri;

    new-array v2, v8, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v2, v7

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_1

    move v0, v7

    goto :goto_0

    :cond_1
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v8

    goto :goto_0
.end method

.method public queryDefaultAccount()J
    .locals 8

    const/4 v5, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/mediatek/bluetooth/map/Email;->ACCOUNT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/mediatek/bluetooth/map/EmailController;->DEFAULT_ACCOUNT_ID_PROJECTION:[Ljava/lang/String;

    const-string v3, "isDefault=?"

    new-array v4, v5, [Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-string v0, "can not find default email account"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public queryMessage(Ljava/util/HashMap;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v7, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/mediatek/bluetooth/map/Email;->MESSAGE_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "mailboxKey"

    aput-object v4, v2, v3

    const-string v3, "accountKey=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v8, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const-string v5, "timeStamp desc"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    :goto_0
    if-nez v7, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_0
    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v7, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method public registerListener(Lcom/mediatek/bluetooth/map/ControllerListener;)V
    .locals 4
    .param p1    # Lcom/mediatek/bluetooth/map/ControllerListener;

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/mediatek/bluetooth/map/Controller;->registerListener(Lcom/mediatek/bluetooth/map/ControllerListener;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountObserver:Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;-><init>(Lcom/mediatek/bluetooth/map/EmailController;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountObserver:Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    if-nez v0, :cond_1

    new-instance v0, Lcom/mediatek/bluetooth/map/MessageObserver;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Controller;->mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

    iget v2, p0, Lcom/mediatek/bluetooth/map/EmailController;->mMsgType:I

    invoke-direct {v0, p0, v1, v2}, Lcom/mediatek/bluetooth/map/MessageObserver;-><init>(Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;Lcom/mediatek/bluetooth/map/ControllerListener;I)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    :cond_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/mediatek/bluetooth/map/Email;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountObserver:Lcom/mediatek/bluetooth/map/EmailController$AccountObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/mediatek/bluetooth/map/Email;->MESSAGE_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/EmailController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public restoreMessage(J)Z
    .locals 13
    .param p1    # J

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "restoreMessage():id is "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/EmailController;->getMaiboxId(I)J

    move-result-wide v8

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "mailboxKey"

    aput-object v3, v2, v0

    sget-object v0, Lcom/mediatek/bluetooth/map/Email;->MESSAGE_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    int-to-long v3, v11

    cmp-long v0, v3, v8

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mDeleteFolder:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    if-eqz v12, :cond_1

    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "mailboxKey"

    invoke-virtual {v7, v0, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v7, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_0
    :goto_0
    const/4 v10, 0x1

    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v10

    :cond_1
    const-string v0, "no record in delete folder"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, "the message does not exist in MMS provider"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    const/4 v10, 0x0

    goto :goto_1
.end method

.method public revertMailboxType(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    int-to-long v1, p1

    invoke-direct {p0, v1, v2}, Lcom/mediatek/bluetooth/map/EmailController;->getMailboxType(J)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unsupported tye:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_1
    const-string v1, "inbox"

    goto :goto_0

    :pswitch_2
    const-string v1, "outbox"

    goto :goto_0

    :pswitch_3
    const-string v1, "sent"

    goto :goto_0

    :pswitch_4
    const-string v1, "deleted"

    goto :goto_0

    :pswitch_5
    const-string v1, "draft"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setAccount(J)Z
    .locals 3
    .param p1    # J

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAccount():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    iput-wide p1, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountId:J

    const/4 v1, 0x1

    return v1
.end method

.method public setMessageStatus(JI)Z
    .locals 8
    .param p1    # J
    .param p3    # I

    const/4 v4, 0x0

    invoke-direct {p0, p3}, Lcom/mediatek/bluetooth/map/EmailController;->reverseReadStatus(I)I

    move-result v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setMessageStatus():id is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", state is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    const/4 v5, -0x1

    if-ne v1, v5, :cond_0

    :goto_0
    return v4

    :cond_0
    sget-object v5, Lcom/mediatek/bluetooth/map/Email;->MESSAGE_URI:Landroid/net/Uri;

    invoke-static {v5, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "flagRead"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    iget-object v5, p0, Lcom/mediatek/bluetooth/map/EmailController;->mResolver:Landroid/content/ContentResolver;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v2, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateInbox()Z
    .locals 8

    const/4 v7, 0x1

    const-string v2, "updateInbox()"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/EmailController;->log(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.email.action.UPDATE_INBOX"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.android.email.extra.ACCOUNT"

    new-array v3, v7, [J

    const/4 v4, 0x0

    iget-wide v5, p0, Lcom/mediatek/bluetooth/map/EmailController;->mAccountId:J

    aput-wide v5, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    const-string v2, "com.android.email.extra.NEED_RESULT"

    invoke-virtual {v0, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/EmailController;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/EmailController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/EmailController;->mHandler:Landroid/os/Handler;

    const-wide/16 v3, 0x7530

    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return v7
.end method
