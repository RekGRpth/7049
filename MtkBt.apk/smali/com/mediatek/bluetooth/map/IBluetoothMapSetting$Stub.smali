.class public abstract Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub;
.super Landroid/os/Binder;
.source "IBluetoothMapSetting.java"

# interfaces
.implements Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.mediatek.bluetooth.map.IBluetoothMapSetting"

.field static final TRANSACTION_disableServer:I = 0x2

.field static final TRANSACTION_enableServer:I = 0x1

.field static final TRANSACTION_getEmailAccount:I = 0x8

.field static final TRANSACTION_getSims:I = 0x7

.field static final TRANSACTION_isEnabled:I = 0x3

.field static final TRANSACTION_registerCallback:I = 0x9

.field static final TRANSACTION_registerSim:I = 0x4

.field static final TRANSACTION_replaceAccount:I = 0x6

.field static final TRANSACTION_unregisterCallback:I = 0xa

.field static final TRANSACTION_unregisterSim:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.mediatek.bluetooth.map.IBluetoothMapSetting"

    invoke-virtual {p0, p0, v0}, Landroid/os/Binder;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;
    .locals 2
    .param p0    # Landroid/os/IBinder;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "com.mediatek.bluetooth.map.IBluetoothMapSetting"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Landroid/os/Parcel;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v5, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v5

    :goto_0
    return v5

    :sswitch_0
    const-string v4, "com.mediatek.bluetooth.map.IBluetoothMapSetting"

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v4, "com.mediatek.bluetooth.map.IBluetoothMapSetting"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub;->enableServer()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_2
    const-string v4, "com.mediatek.bluetooth.map.IBluetoothMapSetting"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub;->disableServer()V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_3
    const-string v6, "com.mediatek.bluetooth.map.IBluetoothMapSetting"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub;->isEnabled()Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_0

    move v4, v5

    :cond_0
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_4
    const-string v6, "com.mediatek.bluetooth.map.IBluetoothMapSetting"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub;->registerSim(I)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_1

    move v4, v5

    :cond_1
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_5
    const-string v4, "com.mediatek.bluetooth.map.IBluetoothMapSetting"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub;->unregisterSim(I)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    :sswitch_6
    const-string v6, "com.mediatek.bluetooth.map.IBluetoothMapSetting"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub;->replaceAccount(J)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_2

    move v4, v5

    :cond_2
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_7
    const-string v4, "com.mediatek.bluetooth.map.IBluetoothMapSetting"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub;->getSims()[I

    move-result-object v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeIntArray([I)V

    goto :goto_0

    :sswitch_8
    const-string v4, "com.mediatek.bluetooth.map.IBluetoothMapSetting"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub;->getEmailAccount()J

    move-result-wide v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    :sswitch_9
    const-string v4, "com.mediatek.bluetooth.map.IBluetoothMapSetting"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub;->registerCallback(Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_a
    const-string v4, "com.mediatek.bluetooth.map.IBluetoothMapSetting"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/map/IBluetoothMapSetting$Stub;->unregisterCallback(Lcom/mediatek/bluetooth/map/IBluetoothMapSettingCallback;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
