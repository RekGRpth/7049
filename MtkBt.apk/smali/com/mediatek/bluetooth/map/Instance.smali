.class Lcom/mediatek/bluetooth/map/Instance;
.super Ljava/lang/Object;
.source "Instance.java"

# interfaces
.implements Lcom/mediatek/bluetooth/map/ControllerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/map/Instance$Listener;
    }
.end annotation


# static fields
.field private static final ACCOUNT_ID_SETTING:Ljava/lang/String; = "ACCOUNT_ID_SETTING"

.field private static final INSTANCE_STATE_MAS_CONNECTED:I = 0x2

.field private static final INSTANCE_STATE_MAS_CONNECTING:I = 0x1

.field private static final INSTANCE_STATE_MAS_DISCONNECTED:I = 0x0

.field private static final INSTANCE_STATE_MAS_DISCONNECTING:I = 0x3

.field private static final SIM_ID_SETTING:Ljava/lang/String; = "SIM_ID_SETTING"


# instance fields
.field private final NAME_BASE:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private folderHirarchy:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mAccountId:J

.field private mContext:Landroid/content/Context;

.field mControllers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/mediatek/bluetooth/map/Controller;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentFolder:Ljava/lang/String;

.field mDevice:Landroid/bluetooth/BluetoothDevice;

.field private mFolderListReqCache:Lcom/mediatek/bluetooth/map/cache/FolderListRequest;

.field private mFolderReqCache:Lcom/mediatek/bluetooth/map/cache/SetFolderRequest;

.field private mInstanceId:I

.field private mListener:Lcom/mediatek/bluetooth/map/Instance$Listener;

.field private mMasConnected:Z

.field private mMasState:I

.field private mMessageCache:Lcom/mediatek/bluetooth/map/cache/BMessageObject;

.field private mMnsConnected:Z

.field private mMsgListReqCache:Lcom/mediatek/bluetooth/map/cache/MessageListRequest;

.field private mMsgListRspCache:Lcom/mediatek/bluetooth/map/cache/MessageListObject;

.field private mMsgReqCache:Lcom/mediatek/bluetooth/map/cache/MessageRequest;

.field private mName:Ljava/lang/String;

.field private mRegistered:Z

.field private mServer:Lcom/mediatek/bluetooth/map/BluetoothMapServerService;

.field private mSimId:I

.field private mType:I


# direct methods
.method constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "MAPInstance"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->TAG:Ljava/lang/String;

    const-string v0, "instance"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->NAME_BASE:Ljava/lang/String;

    const-string v0, "/data/@btmtk/profile/map"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mCurrentFolder:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mAccountId:J

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mRegistered:Z

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mMasConnected:Z

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mMnsConnected:Z

    iput v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mMasState:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->folderHirarchy:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/Instance;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/mediatek/bluetooth/map/util/InstanceUtil;->assignInstanceId()I

    move-result v0

    iput v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "instance_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mName:Ljava/lang/String;

    iput p2, p0, Lcom/mediatek/bluetooth/map/Instance;->mType:I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/Instance;->initCache()V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;II)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "MAPInstance"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->TAG:Ljava/lang/String;

    const-string v0, "instance"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->NAME_BASE:Ljava/lang/String;

    const-string v0, "/data/@btmtk/profile/map"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mCurrentFolder:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mAccountId:J

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mRegistered:Z

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mMasConnected:Z

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mMnsConnected:Z

    iput v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mMasState:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->folderHirarchy:Ljava/util/ArrayList;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "simid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/Instance;->mContext:Landroid/content/Context;

    iput p3, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    invoke-static {}, Lcom/mediatek/bluetooth/map/util/InstanceUtil;->assignInstanceId()I

    move-result v0

    iput v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "instance"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mName:Ljava/lang/String;

    iput p2, p0, Lcom/mediatek/bluetooth/map/Instance;->mType:I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/Instance;->initCache()V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/Instance;->initController()V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;III)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "MAPInstance"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->TAG:Ljava/lang/String;

    const-string v0, "instance"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->NAME_BASE:Ljava/lang/String;

    const-string v0, "/data/@btmtk/profile/map"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mCurrentFolder:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mAccountId:J

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mRegistered:Z

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mMasConnected:Z

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mMnsConnected:Z

    iput v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mMasState:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->folderHirarchy:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/Instance;->mContext:Landroid/content/Context;

    iput p3, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    int-to-long v0, p4

    iput-wide v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mAccountId:J

    invoke-static {}, Lcom/mediatek/bluetooth/map/util/InstanceUtil;->assignInstanceId()I

    move-result v0

    iput v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "instance"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mName:Ljava/lang/String;

    iput p2, p0, Lcom/mediatek/bluetooth/map/Instance;->mType:I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/Instance;->initCache()V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/Instance;->initController()V

    return-void
.end method

.method private clearCache()V
    .locals 4

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/map/Controller;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/map/Controller;->onStop()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private getChildFolders(Ljava/lang/String;)[Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_0

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_0
    iget-boolean v3, p0, Lcom/mediatek/bluetooth/map/Instance;->mRegistered:Z

    if-nez v3, :cond_1

    const-string v3, "instance has not been registered"

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v3, "/"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    if-nez v3, :cond_2

    const-string v3, "telecom"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    goto :goto_0

    :cond_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v2, v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\n "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    const-string v3, "map"

    invoke-virtual {v1, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "telecom"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const-string v3, "telecom"

    invoke-virtual {v1, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "msg"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    const-string v3, "msg"

    invoke-virtual {v1, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "draft"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v3, "inbox"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v3, "outbox"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v3, "deleted"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v3, "sent"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unexpected folder:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private getControllersByType(I)[Lcom/mediatek/bluetooth/map/Controller;
    .locals 7
    .param p1    # I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v1, v4, :cond_1

    and-int v3, v2, p1

    invoke-virtual {p0, v3}, Lcom/mediatek/bluetooth/map/Instance;->containsMessageController(I)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    shl-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Lcom/mediatek/bluetooth/map/Controller;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/mediatek/bluetooth/map/Controller;

    return-object v5
.end method

.method private getMasState()I
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mMasState:I

    return v0
.end method

.method private initCache()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    new-instance v0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;

    iget v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;-><init>(I)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mMsgListReqCache:Lcom/mediatek/bluetooth/map/cache/MessageListRequest;

    new-instance v0, Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    invoke-direct {v0}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mMsgListRspCache:Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    new-instance v0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mName:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mMessageCache:Lcom/mediatek/bluetooth/map/cache/BMessageObject;

    new-instance v0, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;

    iget v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;-><init>(I)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mFolderListReqCache:Lcom/mediatek/bluetooth/map/cache/FolderListRequest;

    new-instance v0, Lcom/mediatek/bluetooth/map/cache/MessageRequest;

    iget v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/cache/MessageRequest;-><init>(I)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mMsgReqCache:Lcom/mediatek/bluetooth/map/cache/MessageRequest;

    new-instance v0, Lcom/mediatek/bluetooth/map/cache/SetFolderRequest;

    iget v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/cache/SetFolderRequest;-><init>(I)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mFolderReqCache:Lcom/mediatek/bluetooth/map/cache/SetFolderRequest;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/Instance;->initFolderStructure()V

    return-void
.end method

.method private initController()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/Instance;->mContext:Landroid/content/Context;

    const-string v4, "MAPInstance"

    invoke-virtual {v3, v4, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mType:I

    invoke-virtual {p0, v8}, Lcom/mediatek/bluetooth/map/Instance;->isMsgTypeSupported(I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "ACCOUNT_ID_SETTING"

    const-wide/16 v4, -0x1

    invoke-interface {v1, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/mediatek/bluetooth/map/Instance;->mAccountId:J

    invoke-virtual {p0, v8}, Lcom/mediatek/bluetooth/map/Instance;->addMessageController(I)Z

    :cond_0
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/mediatek/bluetooth/map/Instance;->isMsgTypeSupported(I)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0, v7}, Lcom/mediatek/bluetooth/map/Instance;->isMsgTypeSupported(I)Z

    move-result v3

    if-nez v3, :cond_1

    const/16 v3, 0x8

    invoke-virtual {p0, v3}, Lcom/mediatek/bluetooth/map/Instance;->isMsgTypeSupported(I)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SIM_ID_SETTING"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v6}, Lcom/mediatek/bluetooth/map/Instance;->enableSim(Z)Z

    :cond_2
    return-void
.end method

.method private initFolderStructure()V
    .locals 5

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/Instance;->folderHirarchy:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const-string v4, "telecom"

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/Instance;->folderHirarchy:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    const-string v4, "msg"

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/Instance;->folderHirarchy:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    const-string v4, "inbox"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string v4, "outbox"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string v4, "sent"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string v4, "deleted"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string v4, "draft"

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/Instance;->folderHirarchy:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private isValidPath(Ljava/lang/String;)Z
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz p1, :cond_0

    const-string v3, "/data/@btmtk/profile/map"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v3, v5

    :goto_0
    return v3

    :cond_1
    const-string v3, "/data/@btmtk/profile/map"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    move v3, v4

    goto :goto_0

    :cond_3
    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    array-length v3, v1

    if-lez v3, :cond_4

    aget-object v3, v1, v5

    if-nez v3, :cond_4

    const-string v3, ""

    aput-object v3, v1, v5

    :cond_4
    const/4 v0, 0x0

    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_5

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/Instance;->folderHirarchy:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_5

    aget-object v3, v1, v0

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/Instance;->folderHirarchy:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashSet;

    aget-object v6, v1, v0

    invoke-virtual {v3, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    :cond_5
    array-length v3, v1

    if-ne v0, v3, :cond_7

    move v3, v4

    goto :goto_0

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    move v3, v5

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "MAPInstance"

    invoke-static {v0, p1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private setMasState(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x2

    iget v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mMasState:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mas state is abnormal:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mMasState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    if-ne p1, v1, :cond_0

    :cond_1
    iput p1, p0, Lcom/mediatek/bluetooth/map/Instance;->mMasState:I

    goto :goto_0

    :pswitch_1
    if-eqz p1, :cond_2

    if-ne p1, v1, :cond_0

    :cond_2
    iput p1, p0, Lcom/mediatek/bluetooth/map/Instance;->mMasState:I

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    if-eq p1, v0, :cond_3

    if-nez p1, :cond_0

    :cond_3
    iput p1, p0, Lcom/mediatek/bluetooth/map/Instance;->mMasState:I

    goto :goto_0

    :pswitch_3
    if-nez p1, :cond_0

    iput p1, p0, Lcom/mediatek/bluetooth/map/Instance;->mMasState:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public addMessageController(I)Z
    .locals 8
    .param p1    # I

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    and-int/lit8 v1, p1, 0x4

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/mediatek/bluetooth/map/EmailController;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mContext:Landroid/content/Context;

    iget-wide v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mAccountId:J

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/mediatek/bluetooth/map/EmailController;-><init>(Landroid/content/Context;Lcom/mediatek/bluetooth/map/Instance;J)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/map/EmailController;->getAccount()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mAccountId:J

    :cond_0
    and-int/lit8 v1, p1, 0x8

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/mediatek/bluetooth/map/MmsController;

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/Instance;->mContext:Landroid/content/Context;

    iget v5, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    invoke-direct {v3, v4, p0, v5}, Lcom/mediatek/bluetooth/map/MmsController;-><init>(Landroid/content/Context;Lcom/mediatek/bluetooth/map/Instance;I)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    and-int/lit8 v1, p1, 0x2

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/mediatek/bluetooth/map/SmsController;

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/Instance;->mContext:Landroid/content/Context;

    iget v5, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    invoke-direct {v3, v4, p0, v5, v7}, Lcom/mediatek/bluetooth/map/SmsController;-><init>(Landroid/content/Context;Lcom/mediatek/bluetooth/map/Instance;II)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    and-int/lit8 v1, p1, 0x1

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/mediatek/bluetooth/map/SmsController;

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/Instance;->mContext:Landroid/content/Context;

    iget v5, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    invoke-direct {v3, v4, p0, v5, v6}, Lcom/mediatek/bluetooth/map/SmsController;-><init>(Landroid/content/Context;Lcom/mediatek/bluetooth/map/Instance;II)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    return v6
.end method

.method public containsMessageController(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public deregisterCallback()V
    .locals 3

    const-string v2, "deregisterCallback()"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mMnsConnected:Z

    if-nez v2, :cond_0

    const-string v2, "mns is not connected"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mMnsConnected:Z

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/bluetooth/map/Controller;

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/map/Controller;->deregisterListener()V

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mListener:Lcom/mediatek/bluetooth/map/Instance$Listener;

    goto :goto_0
.end method

.method public disableSim()V
    .locals 4

    const/4 v3, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "disableSim():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SIM_ID_SETTING+mSimId:SIM_ID_SETTING"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/mediatek/bluetooth/map/Instance;->removeMessageController(I)V

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/mediatek/bluetooth/map/Instance;->removeMessageController(I)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/mediatek/bluetooth/map/Instance;->removeMessageController(I)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mContext:Landroid/content/Context;

    const-string v2, "MAPInstance"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SIM_ID_SETTING"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public enableSim(Z)Z
    .locals 8
    .param p1    # Z

    const/16 v7, 0x8

    const/4 v3, 0x0

    const/4 v6, 0x2

    const/4 v2, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "enableSim():"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", isSave:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SIM_ID_SETTING+mSimId:SIM_ID_SETTING"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/Instance;->mContext:Landroid/content/Context;

    const-string v5, "MAPInstance"

    invoke-virtual {v4, v5, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SIM_ID_SETTING"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    invoke-static {}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->isGeminiSupport()Z

    move-result v4

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    invoke-static {v4}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->getGeminiSmsType(I)I

    move-result v1

    :goto_0
    invoke-virtual {p0, v7}, Lcom/mediatek/bluetooth/map/Instance;->isMsgTypeSupported(I)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0, v7}, Lcom/mediatek/bluetooth/map/Instance;->addMessageController(I)Z

    :cond_1
    if-ne v1, v6, :cond_3

    invoke-virtual {p0, v6}, Lcom/mediatek/bluetooth/map/Instance;->isMsgTypeSupported(I)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0, v6}, Lcom/mediatek/bluetooth/map/Instance;->addMessageController(I)Z

    :goto_1
    return v2

    :cond_2
    invoke-static {}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->getSmsType()I

    move-result v1

    goto :goto_0

    :cond_3
    if-ne v1, v2, :cond_4

    invoke-virtual {p0, v2}, Lcom/mediatek/bluetooth/map/Instance;->isMsgTypeSupported(I)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p0, v2}, Lcom/mediatek/bluetooth/map/Instance;->addMessageController(I)Z

    goto :goto_1

    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unexpected sim type:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    move v2, v3

    goto :goto_1
.end method

.method public getAccountId()J
    .locals 3

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/map/EmailController;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/map/EmailController;->getAccount()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mAccountId:J

    :goto_0
    iget-wide v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mAccountId:J

    return-wide v1

    :cond_0
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mAccountId:J

    goto :goto_0
.end method

.method public getBMessageObject()Lcom/mediatek/bluetooth/map/cache/BMessageObject;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mMessageCache:Lcom/mediatek/bluetooth/map/cache/BMessageObject;

    return-object v0
.end method

.method public getDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mDevice:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method public getFolderListReqCache()Lcom/mediatek/bluetooth/map/cache/FolderListRequest;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mFolderListReqCache:Lcom/mediatek/bluetooth/map/cache/FolderListRequest;

    return-object v0
.end method

.method public getFolderReqCache()Lcom/mediatek/bluetooth/map/cache/SetFolderRequest;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mFolderReqCache:Lcom/mediatek/bluetooth/map/cache/SetFolderRequest;

    return-object v0
.end method

.method public getFolderlist(Lcom/mediatek/bluetooth/map/cache/FolderListRequest;)[Lcom/mediatek/bluetooth/map/cache/FolderListObject;
    .locals 9
    .param p1    # Lcom/mediatek/bluetooth/map/cache/FolderListRequest;

    const/4 v4, 0x0

    const-string v7, "getFolderlist"

    invoke-direct {p0, v7}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;->getSize()I

    move-result v6

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;->getOffset()I

    move-result v5

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->isMasConnected()Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    if-gez v6, :cond_2

    const-string v7, "size is smaller than 0"

    invoke-direct {p0, v7}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v7, p0, Lcom/mediatek/bluetooth/map/Instance;->mCurrentFolder:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/mediatek/bluetooth/map/Instance;->getChildFolders(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v7, v0

    if-eqz v7, :cond_0

    move v3, v5

    :goto_1
    array-length v7, v0

    if-ge v3, v7, :cond_3

    new-instance v1, Lcom/mediatek/bluetooth/map/cache/FolderListObject;

    invoke-direct {v1}, Lcom/mediatek/bluetooth/map/cache/FolderListObject;-><init>()V

    aget-object v7, v0, v3

    invoke-virtual {v1, v7}, Lcom/mediatek/bluetooth/map/cache/FolderListObject;->setName(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v7, v7, [Lcom/mediatek/bluetooth/map/cache/FolderListObject;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/mediatek/bluetooth/map/cache/FolderListObject;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "child folder size "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, v0

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", foldersize is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getInstanceId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    return v0
.end method

.method public getMessage(Lcom/mediatek/bluetooth/map/cache/MessageRequest;)Lcom/mediatek/bluetooth/map/cache/BMessageObject;
    .locals 5
    .param p1    # Lcom/mediatek/bluetooth/map/cache/MessageRequest;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageRequest;->getHandle()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/mediatek/bluetooth/map/util/HandleUtil;->getMessageType(J)I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMessage(): type->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->isMasConnected()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    iget-object v3, p0, Lcom/mediatek/bluetooth/map/Instance;->mMessageCache:Lcom/mediatek/bluetooth/map/cache/BMessageObject;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->reset()V

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/map/Controller;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/mediatek/bluetooth/map/Controller;->getMessage(Lcom/mediatek/bluetooth/map/cache/MessageRequest;)Lcom/mediatek/bluetooth/map/cache/BMessageObject;

    move-result-object v2

    goto :goto_0
.end method

.method public getMessageListCache()Lcom/mediatek/bluetooth/map/cache/MessageListRequest;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mMsgListReqCache:Lcom/mediatek/bluetooth/map/cache/MessageListRequest;

    return-object v0
.end method

.method public getMessageReqCache()Lcom/mediatek/bluetooth/map/cache/MessageRequest;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mMsgReqCache:Lcom/mediatek/bluetooth/map/cache/MessageRequest;

    return-object v0
.end method

.method public getMessagelist(Lcom/mediatek/bluetooth/map/cache/MessageListRequest;)Lcom/mediatek/bluetooth/map/cache/MessageListObject;
    .locals 9
    .param p1    # Lcom/mediatek/bluetooth/map/cache/MessageListRequest;

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getMessageType()I

    move-result v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getMessagelist(): type is"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->isMasConnected()Z

    move-result v7

    if-nez v7, :cond_0

    const/4 v7, 0x0

    :goto_0
    return-object v7

    :cond_0
    iget-object v7, p0, Lcom/mediatek/bluetooth/map/Instance;->mMsgListRspCache:Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    invoke-virtual {v7}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;->reset()V

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getListSize()I

    move-result v7

    if-nez v7, :cond_1

    const/4 v4, 0x1

    :cond_1
    if-nez v6, :cond_2

    iget v6, p0, Lcom/mediatek/bluetooth/map/Instance;->mType:I

    :goto_1
    invoke-direct {p0, v6}, Lcom/mediatek/bluetooth/map/Instance;->getControllersByType(I)[Lcom/mediatek/bluetooth/map/Controller;

    move-result-object v2

    if-eqz v2, :cond_3

    array-length v7, v2

    if-eqz v7, :cond_3

    move-object v0, v2

    array-length v5, v0

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v5, :cond_3

    aget-object v1, v0, v3

    invoke-virtual {v1, p1}, Lcom/mediatek/bluetooth/map/Controller;->getMessageList(Lcom/mediatek/bluetooth/map/cache/MessageListRequest;)Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    iget v7, p0, Lcom/mediatek/bluetooth/map/Instance;->mType:I

    and-int/2addr v6, v7

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->getMsgListRspCache()Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    move-result-object v7

    goto :goto_0
.end method

.method public getMsgListRspCache()Lcom/mediatek/bluetooth/map/cache/MessageListObject;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mMsgListRspCache:Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getRootPath()Ljava/lang/String;
    .locals 1

    const-string v0, "/data/@btmtk/profile/map"

    return-object v0
.end method

.method public getSimId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mSimId:I

    return v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mType:I

    return v0
.end method

.method public isMasConnected()Z
    .locals 3

    iget-boolean v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mRegistered:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mMasState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    const-string v1, "instance has not initialized or no mas connection exist"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMnsConnected()Z
    .locals 2

    iget-boolean v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mRegistered:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mMnsConnected:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    const-string v1, "instance has not initialized or no mns connection exist or device is null"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMsgTypeSupported(I)Z
    .locals 2
    .param p1    # I

    iget v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mType:I

    and-int v0, v1, p1

    if-lez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onDeviceConnected(Landroid/bluetooth/BluetoothDevice;)V
    .locals 5
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDeviceConnected():"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->isMasConnected()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "error:the instance has been connected"

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/mediatek/bluetooth/map/Instance;->mDevice:Landroid/bluetooth/BluetoothDevice;

    const/4 v3, 0x2

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/Instance;->setMasState(I)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/map/Controller;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/map/Controller;->onStart()V

    goto :goto_0
.end method

.method public onDeviceDisconnected(Landroid/bluetooth/BluetoothDevice;)V
    .locals 2
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDeviceDisconnected: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/Instance;->getMasState()I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "the mas connection in the mas has been disconnected"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/Instance;->setMasState(I)V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/Instance;->clearCache()V

    const-string v0, "/data/@btmtk/profile/map"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mCurrentFolder:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v0, "error: the device is null, or other device disconnect event is received"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDeviceDisconnecting(Landroid/bluetooth/BluetoothDevice;)V
    .locals 1
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->isMasConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/Instance;->setMasState(I)V

    :cond_0
    return-void
.end method

.method public onInstanceDeregistered()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "onInstanceDeregistered"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mRegistered:Z

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mMnsConnected:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->deregisterCallback()V

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mMnsConnected:Z

    :cond_0
    return-void
.end method

.method public onInstanceRegistered()V
    .locals 1

    const-string v0, "onInstanceRegistered"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mRegistered:Z

    return-void
.end method

.method public onMessageDeleted(JILjava/lang/String;)V
    .locals 3
    .param p1    # J
    .param p3    # I
    .param p4    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->isMnsConnected()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/mediatek/bluetooth/map/cache/EventReport;

    iget v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/cache/EventReport;-><init>(I)V

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/mediatek/bluetooth/map/cache/EventReport;->notifyMessageDeleted(JILjava/lang/String;)Z

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mListener:Lcom/mediatek/bluetooth/map/Instance$Listener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mListener:Lcom/mediatek/bluetooth/map/Instance$Listener;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v1, v2, v0}, Lcom/mediatek/bluetooth/map/Instance$Listener;->onInstanceChanged(Landroid/bluetooth/BluetoothDevice;Lcom/mediatek/bluetooth/map/cache/EventReport;)V

    goto :goto_0
.end method

.method public onMessageDelivered(JII)V
    .locals 6
    .param p1    # J
    .param p3    # I
    .param p4    # I

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->isMnsConnected()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/mediatek/bluetooth/map/cache/EventReport;

    iget v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/cache/EventReport;-><init>(I)V

    const-string v4, "sent"

    move-wide v1, p1

    move v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mediatek/bluetooth/map/cache/EventReport;->notifyDeliverResult(JILjava/lang/String;I)Z

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mListener:Lcom/mediatek/bluetooth/map/Instance$Listener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mListener:Lcom/mediatek/bluetooth/map/Instance$Listener;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v1, v2, v0}, Lcom/mediatek/bluetooth/map/Instance$Listener;->onInstanceChanged(Landroid/bluetooth/BluetoothDevice;Lcom/mediatek/bluetooth/map/cache/EventReport;)V

    goto :goto_0
.end method

.method public onMessageSent(JII)V
    .locals 6
    .param p1    # J
    .param p3    # I
    .param p4    # I

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->isMnsConnected()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/mediatek/bluetooth/map/cache/EventReport;

    iget v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/cache/EventReport;-><init>(I)V

    const-string v4, "sent"

    move-wide v1, p1

    move v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mediatek/bluetooth/map/cache/EventReport;->notifySendResult(JILjava/lang/String;I)Z

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mListener:Lcom/mediatek/bluetooth/map/Instance$Listener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mListener:Lcom/mediatek/bluetooth/map/Instance$Listener;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v1, v2, v0}, Lcom/mediatek/bluetooth/map/Instance$Listener;->onInstanceChanged(Landroid/bluetooth/BluetoothDevice;Lcom/mediatek/bluetooth/map/cache/EventReport;)V

    goto :goto_0
.end method

.method public onMessageShifted(JILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # J
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->isMnsConnected()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/mediatek/bluetooth/map/cache/EventReport;

    iget v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/cache/EventReport;-><init>(I)V

    move-wide v1, p1

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/mediatek/bluetooth/map/cache/EventReport;->notifyMessageShifted(JILjava/lang/String;Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mListener:Lcom/mediatek/bluetooth/map/Instance$Listener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mListener:Lcom/mediatek/bluetooth/map/Instance$Listener;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v1, v2, v0}, Lcom/mediatek/bluetooth/map/Instance$Listener;->onInstanceChanged(Landroid/bluetooth/BluetoothDevice;Lcom/mediatek/bluetooth/map/cache/EventReport;)V

    goto :goto_0
.end method

.method public onNewMessage(JI)V
    .locals 3
    .param p1    # J
    .param p3    # I

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->isMnsConnected()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/mediatek/bluetooth/map/cache/EventReport;

    iget v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/cache/EventReport;-><init>(I)V

    const-string v1, "inbox"

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/mediatek/bluetooth/map/cache/EventReport;->notifyNewMessageEvent(JILjava/lang/String;)Z

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mListener:Lcom/mediatek/bluetooth/map/Instance$Listener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mListener:Lcom/mediatek/bluetooth/map/Instance$Listener;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v1, v2, v0}, Lcom/mediatek/bluetooth/map/Instance$Listener;->onInstanceChanged(Landroid/bluetooth/BluetoothDevice;Lcom/mediatek/bluetooth/map/cache/EventReport;)V

    goto :goto_0
.end method

.method public pushMessage(Lcom/mediatek/bluetooth/map/cache/BMessageObject;)Z
    .locals 4
    .param p1    # Lcom/mediatek/bluetooth/map/cache/BMessageObject;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->isMasConnected()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getMessageType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/map/Controller;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/mediatek/bluetooth/map/Controller;->pushMessage(Lcom/mediatek/bluetooth/map/cache/BMessageObject;)Z

    const/4 v1, 0x1

    :cond_1
    move v2, v1

    goto :goto_0
.end method

.method public registerCallback(Lcom/mediatek/bluetooth/map/Instance$Listener;)V
    .locals 3
    .param p1    # Lcom/mediatek/bluetooth/map/Instance$Listener;

    const-string v2, "registerCallback()"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->isMasConnected()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "MAS has not been connected"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mMnsConnected:Z

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/bluetooth/map/Controller;

    invoke-virtual {v2, p0}, Lcom/mediatek/bluetooth/map/Controller;->registerListener(Lcom/mediatek/bluetooth/map/ControllerListener;)V

    goto :goto_1

    :cond_1
    iput-object p1, p0, Lcom/mediatek/bluetooth/map/Instance;->mListener:Lcom/mediatek/bluetooth/map/Instance$Listener;

    goto :goto_0
.end method

.method public removeMessageController(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/bluetooth/map/Instance;->mAccountId:J

    :cond_1
    return-void
.end method

.method public setFolder(Lcom/mediatek/bluetooth/map/cache/SetFolderRequest;)I
    .locals 7
    .param p1    # Lcom/mediatek/bluetooth/map/cache/SetFolderRequest;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/SetFolderRequest;->getFolder()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/SetFolderRequest;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->isMasConnected()Z

    move-result v5

    if-eqz v5, :cond_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/SetFolderRequest;->getMasId()I

    move-result v5

    int-to-long v2, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setFolder(): new folder is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iget v5, p0, Lcom/mediatek/bluetooth/map/Instance;->mInstanceId:I

    int-to-long v5, v5

    cmp-long v5, v2, v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/bluetooth/map/Instance;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/Instance;->isValidPath(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    iput-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mCurrentFolder:Ljava/lang/String;

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    const-string v5, "mas id mismatch or device mismatch"

    invoke-direct {p0, v5}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setMessageStatus(Lcom/mediatek/bluetooth/map/cache/StatusSwitchRequest;)Z
    .locals 14
    .param p1    # Lcom/mediatek/bluetooth/map/cache/StatusSwitchRequest;

    const/4 v13, 0x1

    const/4 v10, 0x0

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/StatusSwitchRequest;->getHandle()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/StatusSwitchRequest;->getIndicator()I

    move-result v5

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/StatusSwitchRequest;->getValue()I

    move-result v9

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/map/util/HandleUtil;->getMessageType(J)I

    move-result v6

    invoke-static {v1, v2}, Lcom/mediatek/bluetooth/map/util/HandleUtil;->getId(J)J

    move-result-wide v3

    const/4 v8, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "setMessageStatus(): handle is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "indicator is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "value is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->isMasConnected()Z

    move-result v11

    if-nez v11, :cond_0

    :goto_0
    return v10

    :cond_0
    iget-object v11, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/map/Controller;

    if-eqz v0, :cond_7

    if-ne v5, v13, :cond_3

    if-ne v9, v13, :cond_1

    invoke-virtual {v0, v3, v4}, Lcom/mediatek/bluetooth/map/Controller;->deleteMessage(J)Z

    move-result v8

    :goto_1
    move v10, v8

    goto :goto_0

    :cond_1
    if-nez v9, :cond_2

    invoke-virtual {v0, v3, v4}, Lcom/mediatek/bluetooth/map/Controller;->restoreMessage(J)Z

    move-result v8

    goto :goto_1

    :cond_2
    const-string v11, "invalid status value"

    invoke-direct {p0, v11}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    if-nez v5, :cond_6

    if-ne v9, v13, :cond_4

    const/4 v7, 0x2

    :goto_2
    invoke-virtual {v0, v3, v4, v7}, Lcom/mediatek/bluetooth/map/Controller;->setMessageStatus(JI)Z

    move-result v8

    goto :goto_1

    :cond_4
    if-nez v9, :cond_5

    const/4 v7, 0x1

    goto :goto_2

    :cond_5
    const-string v11, "invalid status value"

    invoke-direct {p0, v11}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "invalid indicator: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "invalid message type:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/mediatek/bluetooth/map/Instance;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public updateInbox()I
    .locals 3

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/Instance;->isMasConnected()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/map/Controller;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/map/Controller;->updateInbox()Z

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public updateMessageController(IJ)Z
    .locals 6
    .param p1    # I
    .param p2    # J

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x4

    if-eq p1, v4, :cond_0

    :goto_0
    return v3

    :cond_0
    iget-object v4, p0, Lcom/mediatek/bluetooth/map/Instance;->mControllers:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/map/EmailController;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p2, p3}, Lcom/mediatek/bluetooth/map/EmailController;->setAccount(J)Z

    move-result v2

    if-eqz v2, :cond_1

    iput-wide p2, p0, Lcom/mediatek/bluetooth/map/Instance;->mAccountId:J

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/Instance;->mContext:Landroid/content/Context;

    const-string v5, "MAPInstance"

    invoke-virtual {v4, v5, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "ACCOUNT_ID_SETTING"

    invoke-interface {v1, v3, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    move v3, v2

    goto :goto_0
.end method
