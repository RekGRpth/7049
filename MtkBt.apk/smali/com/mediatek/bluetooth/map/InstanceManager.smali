.class Lcom/mediatek/bluetooth/map/InstanceManager;
.super Ljava/lang/Object;
.source "InstanceManager.java"


# static fields
.field private static mManager:Lcom/mediatek/bluetooth/map/InstanceManager;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mInstances:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/bluetooth/map/Instance;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "MAP InstanceManager"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    return-void
.end method

.method private createInstance(Z)Lcom/mediatek/bluetooth/map/Instance;
    .locals 4
    .param p1    # Z

    const/4 v1, 0x0

    const/4 v0, -0x1

    or-int/lit8 v1, v1, 0x8

    if-eqz p1, :cond_0

    or-int/lit8 v1, v1, 0x4

    :cond_0
    invoke-static {}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->isGeminiSupport()Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz p1, :cond_1

    sget v0, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->SIM1:I

    :goto_0
    invoke-static {v0}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->getGeminiSmsType(I)I

    move-result v2

    or-int/2addr v1, v2

    :goto_1
    new-instance v2, Lcom/mediatek/bluetooth/map/Instance;

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3, v1, v0}, Lcom/mediatek/bluetooth/map/Instance;-><init>(Landroid/content/Context;II)V

    return-object v2

    :cond_1
    sget v0, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->SIM2:I

    goto :goto_0

    :cond_2
    sget v0, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->SIM1:I

    invoke-static {}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->getSmsType()I

    move-result v2

    or-int/2addr v1, v2

    goto :goto_1
.end method

.method public static getDefaultManager(Landroid/content/Context;)Lcom/mediatek/bluetooth/map/InstanceManager;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/mediatek/bluetooth/map/InstanceManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mediatek/bluetooth/map/InstanceManager;->mManager:Lcom/mediatek/bluetooth/map/InstanceManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/bluetooth/map/InstanceManager;

    invoke-direct {v0}, Lcom/mediatek/bluetooth/map/InstanceManager;-><init>()V

    sput-object v0, Lcom/mediatek/bluetooth/map/InstanceManager;->mManager:Lcom/mediatek/bluetooth/map/InstanceManager;

    :cond_0
    sget-object v0, Lcom/mediatek/bluetooth/map/InstanceManager;->mManager:Lcom/mediatek/bluetooth/map/InstanceManager;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/map/InstanceManager;->init(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/mediatek/bluetooth/map/InstanceManager;->mManager:Lcom/mediatek/bluetooth/map/InstanceManager;

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private init(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mContext:Landroid/content/Context;

    const/4 v0, 0x1

    return v0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "MAP InstanceManager"

    invoke-static {v0, p1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method


# virtual methods
.method public generateInstances(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 2
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/bluetooth/map/Instance;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/InstanceManager;->createInstance(Z)Lcom/mediatek/bluetooth/map/Instance;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->isGeminiSupport()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/InstanceManager;->createInstance(Z)Lcom/mediatek/bluetooth/map/Instance;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getAllInstances()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/bluetooth/map/Instance;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getConnectedDevices()[Landroid/bluetooth/BluetoothDevice;
    .locals 5

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/bluetooth/map/Instance;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "instance device is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/map/Instance;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/map/Instance;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/InstanceManager;->log(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/map/Instance;->isMasConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/map/Instance;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const-string v3, "null"

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v3

    new-array v3, v3, [Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v3}, Ljava/util/AbstractCollection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/bluetooth/BluetoothDevice;

    return-object v3
.end method

.method public getEmailAccount()J
    .locals 4

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/map/Instance;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/mediatek/bluetooth/map/Instance;->isMsgTypeSupported(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/map/Instance;->getAccountId()J

    move-result-wide v2

    :goto_1
    return-wide v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-wide/16 v2, -0x1

    goto :goto_1
.end method

.method public getInstanceByDevice(Landroid/bluetooth/BluetoothDevice;)Ljava/util/ArrayList;
    .locals 4
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/bluetooth/BluetoothDevice;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/bluetooth/map/Instance;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_1

    :cond_0
    return-object v2

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/map/Instance;

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/map/Instance;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getInstanceById(I)Lcom/mediatek/bluetooth/map/Instance;
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/map/Instance;

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/map/Instance;->getInstanceId()I

    move-result v2

    if-ne v2, p1, :cond_0

    :goto_1
    return-object v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getSims()[I
    .locals 5

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v2, v3, [I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSims():"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/InstanceManager;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/map/Instance;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Lcom/mediatek/bluetooth/map/Instance;->containsMessageController(I)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/mediatek/bluetooth/map/Instance;->containsMessageController(I)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/mediatek/bluetooth/map/Instance;->containsMessageController(I)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/mediatek/bluetooth/map/Instance;->getSimId()I

    move-result v3

    aput v3, v2, v0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, -0x1

    aput v3, v2, v0

    goto :goto_1

    :cond_2
    return-object v2
.end method

.method public getState(Landroid/bluetooth/BluetoothDevice;)I
    .locals 1
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {p0, p1}, Lcom/mediatek/bluetooth/map/InstanceManager;->isConnected(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public isConnected(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 4
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    const/4 v2, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/map/Instance;

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/map/Instance;->isMasConnected()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/map/Instance;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public registerSim(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    const-string v2, "registerSim"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/InstanceManager;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/map/Instance;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "instance.getSimId()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/map/Instance;->getSimId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/InstanceManager;->log(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/map/Instance;->getSimId()I

    move-result v2

    if-ne v2, p1, :cond_1

    invoke-virtual {v1, v4}, Lcom/mediatek/bluetooth/map/Instance;->enableSim(Z)Z

    :cond_0
    return v4

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized removeAllInstances()V
    .locals 1

    monitor-enter p0

    :try_start_0
    const-string v0, "removeAllInstances()"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/InstanceManager;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeInstance(I)V
    .locals 4
    .param p1    # I

    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removeInstance():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/InstanceManager;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/map/Instance;

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/map/Instance;->getInstanceId()I

    move-result v2

    if-ne v2, p1, :cond_1

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public replaceAccount(J)Z
    .locals 4
    .param p1    # J

    const/4 v3, 0x4

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/map/Instance;

    invoke-virtual {v1, v3}, Lcom/mediatek/bluetooth/map/Instance;->isMsgTypeSupported(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v3, p1, p2}, Lcom/mediatek/bluetooth/map/Instance;->updateMessageController(IJ)Z

    :cond_0
    const/4 v2, 0x1

    return v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public unregisterSim(I)Z
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/InstanceManager;->mInstances:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/map/Instance;

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/map/Instance;->getSimId()I

    move-result v2

    if-ne v2, p1, :cond_1

    invoke-virtual {v1}, Lcom/mediatek/bluetooth/map/Instance;->disableSim()V

    :cond_0
    const/4 v2, 0x1

    return v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
