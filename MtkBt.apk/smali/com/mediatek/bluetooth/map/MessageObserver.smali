.class public Lcom/mediatek/bluetooth/map/MessageObserver;
.super Landroid/database/ContentObserver;
.source "MessageObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MessageObserver"


# instance fields
.field private mHelper:Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;

.field private mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

.field private mType:I

.field previousMessage:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;Lcom/mediatek/bluetooth/map/ControllerListener;I)V
    .locals 2
    .param p1    # Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;
    .param p2    # Lcom/mediatek/bluetooth/map/ControllerListener;
    .param p3    # I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->previousMessage:Ljava/util/HashMap;

    iput-object p2, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mHelper:Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;

    iput p3, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mType:I

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mHelper:Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->previousMessage:Ljava/util/HashMap;

    invoke-interface {v0, v1}, Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;->queryMessage(Ljava/util/HashMap;)V

    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "MessageObserver"

    invoke-static {v0, p1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 11
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mHelper:Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;

    invoke-interface {v0, v6}, Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;->queryMessage(Ljava/util/HashMap;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "database has been changed, mType is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " previous size is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->previousMessage:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "current size is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MessageObserver;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->previousMessage:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-virtual {v6}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mHelper:Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;

    invoke-virtual {v6, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;->revertMailboxType(I)Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->previousMessage:Ljava/util/HashMap;

    invoke-virtual {v0, v10}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v8, :cond_1

    const-string v0, "inbox"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget v3, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mType:I

    invoke-interface {v0, v1, v2, v3}, Lcom/mediatek/bluetooth/map/ControllerListener;->onNewMessage(JI)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->previousMessage:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v6, v10}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mHelper:Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->previousMessage:Ljava/util/HashMap;

    invoke-virtual {v0, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;->revertMailboxType(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget v3, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mType:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/mediatek/bluetooth/map/ControllerListener;->onMessageDeleted(JILjava/lang/String;)V

    goto :goto_2

    :cond_4
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mHelper:Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;->revertMailboxType(I)Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mHelper:Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;

    invoke-virtual {v6, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;->revertMailboxType(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    if-eqz v4, :cond_3

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "deleted"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget v3, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mType:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/mediatek/bluetooth/map/ControllerListener;->onMessageDeleted(JILjava/lang/String;)V

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget v3, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->mType:I

    invoke-interface/range {v0 .. v5}, Lcom/mediatek/bluetooth/map/ControllerListener;->onMessageShifted(JILjava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    iput-object v6, p0, Lcom/mediatek/bluetooth/map/MessageObserver;->previousMessage:Ljava/util/HashMap;

    goto/16 :goto_0
.end method
