.class public Lcom/mediatek/bluetooth/map/MmsConnection$ProgressCallbackEntity;
.super Lorg/apache/http/entity/ByteArrayEntity;
.source "MmsConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/map/MmsConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ProgressCallbackEntity"
.end annotation


# static fields
.field private static final DEFAULT_PIECE_SIZE:I = 0x1000


# instance fields
.field private final mContent:[B

.field private final mContext:Landroid/content/Context;

.field private final mToken:J

.field final synthetic this$0:Lcom/mediatek/bluetooth/map/MmsConnection;


# direct methods
.method public constructor <init>(Lcom/mediatek/bluetooth/map/MmsConnection;Landroid/content/Context;J[B)V
    .locals 0
    .param p2    # Landroid/content/Context;
    .param p3    # J
    .param p5    # [B

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/MmsConnection$ProgressCallbackEntity;->this$0:Lcom/mediatek/bluetooth/map/MmsConnection;

    invoke-direct {p0, p5}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    iput-object p2, p0, Lcom/mediatek/bluetooth/map/MmsConnection$ProgressCallbackEntity;->mContext:Landroid/content/Context;

    iput-object p5, p0, Lcom/mediatek/bluetooth/map/MmsConnection$ProgressCallbackEntity;->mContent:[B

    iput-wide p3, p0, Lcom/mediatek/bluetooth/map/MmsConnection$ProgressCallbackEntity;->mToken:J

    return-void
.end method


# virtual methods
.method public writeTo(Ljava/io/OutputStream;)V
    .locals 6
    .param p1    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Output stream may not be null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    const/4 v0, 0x0

    const/4 v2, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/bluetooth/map/MmsConnection$ProgressCallbackEntity;->mContent:[B

    array-length v3, v4

    :goto_0
    if-ge v2, v3, :cond_2

    sub-int v1, v3, v2

    const/16 v4, 0x1000

    if-le v1, v4, :cond_1

    const/16 v1, 0x1000

    :cond_1
    iget-object v4, p0, Lcom/mediatek/bluetooth/map/MmsConnection$ProgressCallbackEntity;->mContent:[B

    invoke-virtual {p1, v4, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/2addr v2, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    return-void

    :catchall_0
    move-exception v4

    throw v4
.end method
