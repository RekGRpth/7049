.class public Lcom/mediatek/bluetooth/map/MmsConnection;
.super Ljava/lang/Object;
.source "MmsConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/map/MmsConnection$ProgressCallbackEntity;,
        Lcom/mediatek/bluetooth/map/MmsConnection$ConnectionListener;,
        Lcom/mediatek/bluetooth/map/MmsConnection$Task;
    }
.end annotation


# static fields
.field private static final APN_PROJECTION:[Ljava/lang/String;

.field private static final CALLING_LINE:Ljava/lang/String; = "x-up-calling-line-id"

.field private static final COLUMN_MMSC:I = 0x1

.field private static final COLUMN_MMSPORT:I = 0x3

.field private static final COLUMN_MMSPROXY:I = 0x2

.field private static final COLUMN_TYPE:I = 0x0

.field private static final DEFAULT_USER_AGENT:Ljava/lang/String; = "Android-Mms/2.0"

.field private static final HDR_KEY_ACCEPT:Ljava/lang/String; = "Accept"

.field private static final HDR_KEY_ACCEPT_LANGUAGE:Ljava/lang/String; = "Accept-Language"

.field private static final HDR_VALUE_ACCEPT:Ljava/lang/String; = "*/*, application/vnd.wap.mms-message, application/vnd.wap.sic"

.field private static final HDR_VALUE_ACCEPT_LANGUAGE:Ljava/lang/String;

.field private static final HTTP_PARAMS_LINE1_KEY:Ljava/lang/String; = "##LINE1##"

.field private static final MMS_EVENT_NETWORK_READY:I = 0x1

.field private static final MMS_EVENT_NEW_PDU:I = 0x0

.field private static final MMS_EVENT_SEND_COMPLETE:I = 0x2

.field public static final NO_TOKEN:J = -0x1L

.field public static final STATE_FAIL:I = 0x2

.field public static final STATE_PENDING:I = 0x0

.field public static final STATE_SUCCESS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MmsConnection"

.field private static final UAPROF:Ljava/lang/String; = "http://www.google.com/oha/rdf/ua-profile-kila.xml"

.field private static final UAPROF_TAG_NAME:Ljava/lang/String; = "x-wap-profile"

.field private static mContext:Landroid/content/Context;

.field private static mHttpSocketTimeout:I

.field private static mMmsConnection:Lcom/mediatek/bluetooth/map/MmsConnection;

.field private static mProxyAddress:Ljava/lang/String;

.field private static mProxyPort:I

.field private static mServiceCenter:Ljava/lang/String;

.field private static mSimId:I

.field private static mUserAgent:Ljava/lang/String;


# instance fields
.field private mConnMgr:Landroid/net/ConnectivityManager;

.field private mDataTransferring:Z

.field private final mHandler:Landroid/os/Handler;

.field private mLisenter:Lcom/mediatek/bluetooth/map/MmsConnection$ConnectionListener;

.field private mPendingTask:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/bluetooth/map/MmsConnection$Task;",
            ">;"
        }
    .end annotation
.end field

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mReceiverFlag:Z

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "mmsc"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mmsproxy"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "mmsport"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/bluetooth/map/MmsConnection;->APN_PROJECTION:[Ljava/lang/String;

    const v0, 0xea60

    sput v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mHttpSocketTimeout:I

    invoke-static {}, Lcom/mediatek/bluetooth/map/MmsConnection;->getHttpAcceptLanguage()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/bluetooth/map/MmsConnection;->HDR_VALUE_ACCEPT_LANGUAGE:Ljava/lang/String;

    const/4 v0, -0x1

    sput v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mProxyPort:I

    const-string v0, "Android-Mms/2.0"

    sput-object v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mUserAgent:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mPendingTask:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mReceiverFlag:Z

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mDataTransferring:Z

    new-instance v0, Lcom/mediatek/bluetooth/map/MmsConnection$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/map/MmsConnection$1;-><init>(Lcom/mediatek/bluetooth/map/MmsConnection;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/bluetooth/map/MmsConnection$2;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/map/MmsConnection$2;-><init>(Lcom/mediatek/bluetooth/map/MmsConnection;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mReceiver:Landroid/content/BroadcastReceiver;

    sput-object p1, Lcom/mediatek/bluetooth/map/MmsConnection;->mContext:Landroid/content/Context;

    sput p2, Lcom/mediatek/bluetooth/map/MmsConnection;->mSimId:I

    invoke-static {}, Lcom/mediatek/bluetooth/map/MmsConnection;->init()V

    sget-object v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mConnMgr:Landroid/net/ConnectivityManager;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mServiceCenter:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/mediatek/bluetooth/map/MmsConnection;->mServiceCenter:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mProxyAddress:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/mediatek/bluetooth/map/MmsConnection;->mProxyAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mProxyPort:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/mediatek/bluetooth/map/MmsConnection;->mProxyPort:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/map/MmsConnection;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/map/MmsConnection;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mPendingTask:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/bluetooth/map/MmsConnection;J[B)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/map/MmsConnection;
    .param p1    # J
    .param p3    # [B

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/bluetooth/map/MmsConnection;->processConnection(J[B)V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/bluetooth/map/MmsConnection;)I
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/map/MmsConnection;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/MmsConnection;->startConnection()I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/bluetooth/map/MmsConnection;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/map/MmsConnection;

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mDataTransferring:Z

    return v0
.end method

.method static synthetic access$302(Lcom/mediatek/bluetooth/map/MmsConnection;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/map/MmsConnection;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mDataTransferring:Z

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/bluetooth/map/MmsConnection;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/map/MmsConnection;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/MmsConnection;->process()V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/bluetooth/map/MmsConnection;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/map/MmsConnection;

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mReceiverFlag:Z

    return v0
.end method

.method static synthetic access$502(Lcom/mediatek/bluetooth/map/MmsConnection;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/map/MmsConnection;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mReceiverFlag:Z

    return p1
.end method

.method static synthetic access$600(Lcom/mediatek/bluetooth/map/MmsConnection;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/map/MmsConnection;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$700()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/bluetooth/map/MmsConnection;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/map/MmsConnection;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private acquireWakeLock()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    return-void
.end method

.method private static addLocaleToHttpAcceptLanguage(Ljava/lang/StringBuilder;Ljava/util/Locale;)V
    .locals 3
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # Ljava/util/Locale;

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "-"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method private createHttpClient()Landroid/net/http/AndroidHttpClient;
    .locals 6

    const/4 v5, 0x1

    sget-object v3, Lcom/mediatek/bluetooth/map/MmsConnection;->mUserAgent:Ljava/lang/String;

    sget-object v4, Lcom/mediatek/bluetooth/map/MmsConnection;->mContext:Landroid/content/Context;

    invoke-static {v3, v4}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;Landroid/content/Context;)Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    const-string v3, "UTF-8"

    invoke-static {v1, v3}, Lorg/apache/http/params/HttpProtocolParams;->setContentCharset(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    sget v2, Lcom/mediatek/bluetooth/map/MmsConnection;->mHttpSocketTimeout:I

    invoke-static {v1, v2}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    new-instance v3, Lorg/apache/http/impl/client/DefaultHttpRequestRetryHandler;

    invoke-direct {v3, v5, v5}, Lorg/apache/http/impl/client/DefaultHttpRequestRetryHandler;-><init>(IZ)V

    invoke-virtual {v0, v3}, Landroid/net/http/AndroidHttpClient;->setHttpRequestRetryHandler(Lorg/apache/http/impl/client/DefaultHttpRequestRetryHandler;)V

    return-object v0
.end method

.method private declared-synchronized createWakeLock()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    sget-object v1, Lcom/mediatek/bluetooth/map/MmsConnection;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "MMS Connectivity"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private endConnection()V
    .locals 1

    invoke-static {}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->isGeminiSupport()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mSimId:I

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/map/MmsConnection;->endMmsConnectivityGemini(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/MmsConnection;->endMmsConnectivity()V

    goto :goto_0
.end method

.method public static getDefault(Landroid/content/Context;I)Lcom/mediatek/bluetooth/map/MmsConnection;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # I

    sget-object v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mMmsConnection:Lcom/mediatek/bluetooth/map/MmsConnection;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/bluetooth/map/MmsConnection;

    invoke-direct {v0, p0, p1}, Lcom/mediatek/bluetooth/map/MmsConnection;-><init>(Landroid/content/Context;I)V

    sput-object v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mMmsConnection:Lcom/mediatek/bluetooth/map/MmsConnection;

    :cond_0
    invoke-static {}, Lcom/mediatek/bluetooth/map/MmsConnection;->init()V

    sget-object v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mMmsConnection:Lcom/mediatek/bluetooth/map/MmsConnection;

    return-object v0
.end method

.method private static getHttpAcceptLanguage()Ljava/lang/String;
    .locals 3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/map/MmsConnection;->addLocaleToHttpAcceptLanguage(Ljava/lang/StringBuilder;Ljava/util/Locale;)V

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0, v2}, Lcom/mediatek/bluetooth/map/MmsConnection;->addLocaleToHttpAcceptLanguage(Ljava/lang/StringBuilder;Ljava/util/Locale;)V

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static init()V
    .locals 10

    const/4 v4, 0x0

    const/4 v3, 0x0

    sget-object v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "current"

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/mediatek/bluetooth/map/MmsConnection;->APN_PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_1

    const-string v0, "MmsConnection"

    const-string v1, "Apn is not found in Database!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v9, 0x0

    :cond_2
    :goto_1
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mServiceCenter:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mms"

    invoke-static {v0, v1}, Lcom/mediatek/bluetooth/map/MmsConnection;->isValidApnType(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v9, 0x1

    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :goto_2
    sput-object v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mServiceCenter:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mProxyAddress:Ljava/lang/String;

    invoke-static {}, Lcom/mediatek/bluetooth/map/MmsConnection;->isProxySet()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    :try_start_1
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mProxyPort:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v7

    :try_start_2
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "MmsConnection"

    const-string v1, "mms port not set!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    move-object v0, v4

    goto :goto_2

    :cond_4
    :try_start_3
    const-string v0, "MmsConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad port number format: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :cond_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    if-eqz v9, :cond_0

    sget-object v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mServiceCenter:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MmsConnection"

    const-string v1, "Invalid APN setting: MMSC is empty"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static isProxySet()Z
    .locals 1

    sget-object v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mProxyAddress:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/bluetooth/map/MmsConnection;->mProxyAddress:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isValidApnType(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    const-string v5, ","

    invoke-virtual {p0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "*"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;

    if-eqz p0, :cond_0

    const-string v0, "MmsConnection"

    invoke-static {v0, p0}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private process()V
    .locals 1

    new-instance v0, Lcom/mediatek/bluetooth/map/MmsConnection$3;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/map/MmsConnection$3;-><init>(Lcom/mediatek/bluetooth/map/MmsConnection;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private processConnection(J[B)V
    .locals 4
    .param p1    # J
    .param p3    # [B

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/MmsConnection;->ensureRouteToHost()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/mediatek/bluetooth/map/MmsConnection;->httpConnection(J[B)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v1, "fail to rout to host"

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private releaseWakeLock()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    return-void
.end method

.method private startConnection()I
    .locals 2

    invoke-static {}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->isGeminiSupport()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/mediatek/bluetooth/map/MmsConnection;->mSimId:I

    invoke-virtual {p0, v1}, Lcom/mediatek/bluetooth/map/MmsConnection;->beginMmsConnectivityGemini(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/MmsConnection;->beginMmsConnectivity()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public beginMmsConnectivity()I
    .locals 4

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/MmsConnection;->createWakeLock()V

    const-string v1, "beginMmsConnectivity"

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mConnMgr:Landroid/net/ConnectivityManager;

    const/4 v2, 0x0

    const-string v3, "enableMMS"

    invoke-virtual {v1, v2, v3}, Landroid/net/ConnectivityManager;->startUsingNetworkFeature(ILjava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot establish MMS connectivity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/MmsConnection;->acquireWakeLock()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public beginMmsConnectivityGemini(I)I
    .locals 5
    .param p1    # I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/MmsConnection;->createWakeLock()V

    move v1, p1

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mConnMgr:Landroid/net/ConnectivityManager;

    const/4 v3, 0x0

    const-string v4, "enableMMS"

    invoke-virtual {v2, v3, v4, v1}, Landroid/net/ConnectivityManager;->startUsingNetworkFeatureGemini(ILjava/lang/String;I)I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "beginMmsConnectivityGemini: result is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    packed-switch v0, :pswitch_data_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot establish MMS connectivity:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/MmsConnection;->acquireWakeLock()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public endMmsConnectivity()V
    .locals 3

    const-string v0, "endMmsConnectivity"

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mConnMgr:Landroid/net/ConnectivityManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mConnMgr:Landroid/net/ConnectivityManager;

    const/4 v1, 0x0

    const-string v2, "enableMMS"

    invoke-virtual {v0, v1, v2}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/MmsConnection;->releaseWakeLock()V

    return-void

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/MmsConnection;->releaseWakeLock()V

    throw v0
.end method

.method public endMmsConnectivityGemini(I)V
    .locals 4
    .param p1    # I

    const-string v1, "endMmsConnectivityGemini()"

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    move v0, p1

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mConnMgr:Landroid/net/ConnectivityManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mConnMgr:Landroid/net/ConnectivityManager;

    const/4 v2, 0x0

    const-string v3, "enableMMS"

    invoke-virtual {v1, v2, v3, v0}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeatureGemini(ILjava/lang/String;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/MmsConnection;->releaseWakeLock()V

    return-void

    :catchall_0
    move-exception v1

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/MmsConnection;->releaseWakeLock()V

    throw v1
.end method

.method public ensureRouteToHost()Z
    .locals 7

    const/4 v6, 0x2

    const/4 v5, -0x1

    const-string v3, "ensureRouteToHost"

    invoke-static {v3}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mConnMgr:Landroid/net/ConnectivityManager;

    if-nez v3, :cond_0

    sget-object v3, Lcom/mediatek/bluetooth/map/MmsConnection;->mContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    iput-object v3, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mConnMgr:Landroid/net/ConnectivityManager;

    :cond_0
    invoke-static {}, Lcom/mediatek/bluetooth/map/MmsConnection;->isProxySet()Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Lcom/mediatek/bluetooth/map/MmsConnection;->mProxyAddress:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/mediatek/bluetooth/map/MmsConnection;->lookupHost(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v5, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot establish route for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/mediatek/bluetooth/map/MmsConnection;->mServiceCenter:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": Unknown host"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1

    :cond_2
    iget-object v3, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mConnMgr:Landroid/net/ConnectivityManager;

    invoke-virtual {v3, v6, v0}, Landroid/net/ConnectivityManager;->requestRouteToHost(II)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot establish route to proxy "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    sget-object v3, Lcom/mediatek/bluetooth/map/MmsConnection;->mServiceCenter:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/bluetooth/map/MmsConnection;->lookupHost(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v5, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot establish route for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/mediatek/bluetooth/map/MmsConnection;->mServiceCenter:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": Unknown host"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mConnMgr:Landroid/net/ConnectivityManager;

    invoke-virtual {v3, v6, v0}, Landroid/net/ConnectivityManager;->requestRouteToHost(II)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot establish route to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/mediatek/bluetooth/map/MmsConnection;->mServiceCenter:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public httpConnection(J[B)[B
    .locals 22
    .param p1    # J
    .param p3    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v21, Lcom/mediatek/bluetooth/map/MmsConnection;->mServiceCenter:Ljava/lang/String;

    const-string v3, "httpConnection()"

    invoke-static {v3}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    const/4 v9, 0x0

    :try_start_0
    new-instance v13, Ljava/net/URI;

    move-object/from16 v0, v21

    invoke-direct {v13, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    new-instance v20, Lorg/apache/http/HttpHost;

    invoke-virtual {v13}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13}, Ljava/net/URI;->getPort()I

    move-result v4

    const-string v5, "http"

    move-object/from16 v0, v20

    invoke-direct {v0, v3, v4, v5}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/bluetooth/map/MmsConnection;->createHttpClient()Landroid/net/http/AndroidHttpClient;

    move-result-object v9

    const/16 v17, 0x0

    new-instance v2, Lcom/mediatek/bluetooth/map/MmsConnection$ProgressCallbackEntity;

    sget-object v4, Lcom/mediatek/bluetooth/map/MmsConnection;->mContext:Landroid/content/Context;

    move-object/from16 v3, p0

    move-wide/from16 v5, p1

    move-object/from16 v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/mediatek/bluetooth/map/MmsConnection$ProgressCallbackEntity;-><init>(Lcom/mediatek/bluetooth/map/MmsConnection;Landroid/content/Context;J[B)V

    const-string v3, "application/vnd.wap.mms-message"

    invoke-virtual {v2, v3}, Lorg/apache/http/entity/AbstractHttpEntity;->setContentType(Ljava/lang/String;)V

    new-instance v16, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;->setEntity(Lorg/apache/http/HttpEntity;)V

    move-object/from16 v17, v16

    invoke-virtual {v9}, Landroid/net/http/AndroidHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v15

    invoke-static {}, Lcom/mediatek/bluetooth/map/MmsConnection;->isProxySet()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lorg/apache/http/HttpHost;

    sget-object v4, Lcom/mediatek/bluetooth/map/MmsConnection;->mProxyAddress:Ljava/lang/String;

    sget v5, Lcom/mediatek/bluetooth/map/MmsConnection;->mProxyPort:I

    invoke-direct {v3, v4, v5}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    invoke-static {v15, v3}, Lorg/apache/http/conn/params/ConnRouteParams;->setDefaultProxy(Lorg/apache/http/params/HttpParams;Lorg/apache/http/HttpHost;)V

    :cond_0
    move-object/from16 v0, v17

    invoke-interface {v0, v15}, Lorg/apache/http/HttpRequest;->setParams(Lorg/apache/http/params/HttpParams;)V

    const-string v3, "Accept"

    const-string v4, "*/*, application/vnd.wap.mms-message, application/vnd.wap.sic"

    move-object/from16 v0, v17

    invoke-interface {v0, v3, v4}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "x-wap-profile"

    const-string v4, "http://www.google.com/oha/rdf/ua-profile-kila.xml"

    move-object/from16 v0, v17

    invoke-interface {v0, v3, v4}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    sget v3, Lcom/mediatek/bluetooth/map/MmsConnection;->mSimId:I

    invoke-static {v3}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->getPhoneNumber(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    const-string v3, "x-up-calling-line-id"

    move-object/from16 v0, v17

    invoke-interface {v0, v3, v14}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v3, "Accept-Language"

    sget-object v4, Lcom/mediatek/bluetooth/map/MmsConnection;->HDR_VALUE_ACCEPT_LANGUAGE:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-interface {v0, v3, v4}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/16 v18, 0x0

    :try_start_1
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v9, v0, v1}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v18

    :goto_0
    if-nez v18, :cond_4

    :try_start_2
    const-string v3, "MmsConnection"

    const-string v4, "httpConnection: client.execute() return null !!"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const/4 v8, 0x0

    if-eqz v9, :cond_2

    invoke-virtual {v9}, Landroid/net/http/AndroidHttpClient;->close()V

    :cond_2
    :goto_1
    return-object v8

    :catch_0
    move-exception v11

    :try_start_3
    const-string v3, "MmsConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AndroidHttpClient.execute exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v11}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/net/URISyntaxException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_0

    :catch_1
    move-exception v11

    :try_start_4
    invoke-virtual {v11}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v9, :cond_3

    invoke-virtual {v9}, Landroid/net/http/AndroidHttpClient;->close()V

    :cond_3
    :goto_2
    const/4 v8, 0x0

    goto :goto_1

    :cond_4
    :try_start_5
    invoke-interface/range {v18 .. v18}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    const/16 v4, 0xc8

    if-eq v3, v4, :cond_5

    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "HTTP error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface/range {v19 .. v19}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_5
    .catch Ljava/net/URISyntaxException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catch_2
    move-exception v11

    :try_start_6
    invoke-virtual {v11}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    if-eqz v9, :cond_3

    invoke-virtual {v9}, Landroid/net/http/AndroidHttpClient;->close()V

    goto :goto_2

    :cond_5
    :try_start_7
    invoke-interface/range {v18 .. v18}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_7
    .catch Ljava/net/URISyntaxException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/net/SocketException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result-object v12

    const/4 v8, 0x0

    if-eqz v12, :cond_7

    :try_start_8
    invoke-interface {v12}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_6

    invoke-interface {v12}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v3

    long-to-int v3, v3

    new-array v8, v3, [B

    new-instance v10, Ljava/io/DataInputStream;

    invoke-interface {v12}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v10, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    invoke-virtual {v10, v8}, Ljava/io/DataInputStream;->readFully([B)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    invoke-virtual {v10}, Ljava/io/FilterInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_6
    :goto_3
    if-eqz v12, :cond_7

    :try_start_b
    invoke-interface {v12}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_b
    .catch Ljava/net/URISyntaxException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_4
    .catch Ljava/net/SocketException; {:try_start_b .. :try_end_b} :catch_6
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    :cond_7
    if-eqz v9, :cond_2

    invoke-virtual {v9}, Landroid/net/http/AndroidHttpClient;->close()V

    goto/16 :goto_1

    :catch_3
    move-exception v11

    :try_start_c
    const-string v3, "MmsConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error closing input stream: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v11}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v3

    if-eqz v12, :cond_8

    :try_start_d
    invoke-interface {v12}, Lorg/apache/http/HttpEntity;->consumeContent()V

    :cond_8
    throw v3
    :try_end_d
    .catch Ljava/net/URISyntaxException; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_d .. :try_end_d} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d .. :try_end_d} :catch_4
    .catch Ljava/net/SocketException; {:try_start_d .. :try_end_d} :catch_6
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_7
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    :catch_4
    move-exception v11

    :try_start_e
    invoke-virtual {v11}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    if-eqz v9, :cond_3

    invoke-virtual {v9}, Landroid/net/http/AndroidHttpClient;->close()V

    goto/16 :goto_2

    :catchall_1
    move-exception v3

    :try_start_f
    invoke-virtual {v10}, Ljava/io/FilterInputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_5
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :goto_4
    :try_start_10
    throw v3

    :catch_5
    move-exception v11

    const-string v4, "MmsConnection"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error closing input stream: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v11}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto :goto_4

    :catch_6
    move-exception v11

    :try_start_11
    invoke-virtual {v11}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    if-eqz v9, :cond_3

    invoke-virtual {v9}, Landroid/net/http/AndroidHttpClient;->close()V

    goto/16 :goto_2

    :catch_7
    move-exception v11

    :try_start_12
    invoke-virtual {v11}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    if-eqz v9, :cond_3

    invoke-virtual {v9}, Landroid/net/http/AndroidHttpClient;->close()V

    goto/16 :goto_2

    :catchall_2
    move-exception v3

    if-eqz v9, :cond_9

    invoke-virtual {v9}, Landroid/net/http/AndroidHttpClient;->close()V

    :cond_9
    throw v3
.end method

.method public lookupHost(Ljava/lang/String;)I
    .locals 6
    .param p1    # Ljava/lang/String;

    :try_start_0
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v1

    const/4 v4, 0x3

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x18

    const/4 v5, 0x2

    aget-byte v5, v1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v4, v5

    const/4 v5, 0x1

    aget-byte v5, v1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    const/4 v5, 0x0

    aget-byte v5, v1, v5

    and-int/lit16 v5, v5, 0xff

    or-int v0, v4, v5

    :goto_0
    return v0

    :catch_0
    move-exception v2

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public registerListener(Lcom/mediatek/bluetooth/map/MmsConnection$ConnectionListener;)V
    .locals 0
    .param p1    # Lcom/mediatek/bluetooth/map/MmsConnection$ConnectionListener;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mLisenter:Lcom/mediatek/bluetooth/map/MmsConnection$ConnectionListener;

    return-void
.end method

.method public send(J[B)V
    .locals 4
    .param p1    # J
    .param p3    # [B

    if-eqz p3, :cond_0

    array-length v1, p3

    if-nez v1, :cond_1

    :cond_0
    const-string v1, "pdu to send is null"

    invoke-static {v1}, Lcom/mediatek/bluetooth/map/MmsConnection;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/mediatek/bluetooth/map/MmsConnection$Task;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/bluetooth/map/MmsConnection$Task;-><init>(Lcom/mediatek/bluetooth/map/MmsConnection;Lcom/mediatek/bluetooth/map/MmsConnection$1;)V

    iput-wide p1, v0, Lcom/mediatek/bluetooth/map/MmsConnection$Task;->mToken:J

    iput-object p3, v0, Lcom/mediatek/bluetooth/map/MmsConnection$Task;->mPdu:[B

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mPendingTask:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public unregisterListener()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/MmsConnection;->mLisenter:Lcom/mediatek/bluetooth/map/MmsConnection$ConnectionListener;

    return-void
.end method
