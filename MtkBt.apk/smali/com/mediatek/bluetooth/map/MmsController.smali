.class Lcom/mediatek/bluetooth/map/MmsController;
.super Lcom/mediatek/bluetooth/map/Controller;
.source "MmsController.java"

# interfaces
.implements Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;
.implements Lcom/mediatek/bluetooth/map/MmsConnection$ConnectionListener;


# static fields
.field private static final ADDRESS_CHARSET_COLUMN:I = 0x2

.field private static final ADDRESS_COLUMN:I = 0x0

.field private static final ADDRESS_TYPE_BCC:I = 0x81

.field private static final ADDRESS_TYPE_CC:I = 0x82

.field private static final ADDRESS_TYPE_COLUMN:I = 0x1

.field private static final ADDRESS_TYPE_FROM:I = 0x89

.field private static final ADDRESS_TYPE_TO:I = 0x97

.field private static final BASE_FROM_COLUMN:I = 0x2

.field private static final BASE_ID_COLUMN:I = 0x0

.field private static final BASE_READ_COLUMN:I = 0x1

.field private static final BASE_TO_COLUMN:I = 0x3

.field private static final CC_COLUMN:I = 0x6

.field private static final CONTENT_TYPE_COLUMN:I = 0xe

.field private static final DATE_COLUMN:I = 0x3

.field private static final DEFAULT_PROJECTION:[Ljava/lang/String;

.field private static final EXTEND_DATE_COLUMN:I = 0x0

.field private static final EXTEND_PRIORITY_COLUMN:I = 0x3

.field private static final EXTEND_SUBJECT_CHARSET_COLUMN:I = 0x2

.field private static final EXTEND_SUBJECT_COLUMN:I = 0x1

.field private static final FROM_COLUMN:I = 0x4

.field private static final ID_COLUMN:I = 0x0

.field private static final MESSAGE_BOX_COLUMN:I = 0xc

.field private static final MESSAGE_ID_COLUMN:I = 0xd

.field private static final MESSAGE_SIZE_COLUMN:I = 0x7

.field private static final MESSAGE_TYPE_DELETE:I = 0x64

.field private static final MMS_READ_STATUS:I = 0x1

.field private static final MMS_UNREAD_STATUS:I = 0x0

.field private static final MMS_VERSION_COLUMN:I = 0xf

.field private static final PRIORITY_COLUMN:I = 0x8

.field private static final READ_COLUMN:I = 0xa

.field private static final STATUS_COLUMN:I = 0x9

.field private static final SUBJECT_CHARSET_COLUMN:I = 0x2

.field private static final SUBJECT_COLUMN:I = 0x1

.field private static final THREAD_ID_COLUMN:I = 0xb

.field private static final TO_COLUMN:I = 0x5


# instance fields
.field private final ADDRESS_PROJECTION:[Ljava/lang/String;

.field private BASE_PROJECTION:[Ljava/lang/String;

.field private EXTEND_PROJECTION:[Ljava/lang/String;

.field private ID_PROJECTION:[Ljava/lang/String;

.field private SIZE_PROJECTION:[Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mInstance:Lcom/mediatek/bluetooth/map/Instance;

.field private mInstanceId:I

.field private mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

.field private mMmsConnection:Lcom/mediatek/bluetooth/map/MmsConnection;

.field private mSimId:I

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "sub"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "sub_cs"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "from"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/bluetooth/map/MmsController;->DEFAULT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/mediatek/bluetooth/map/Instance;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/bluetooth/map/Instance;
    .param p3    # I

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/Controller;-><init>()V

    const-string v0, "MMSController"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->TAG:Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->ID_PROJECTION:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "read"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->BASE_PROJECTION:[Ljava/lang/String;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "date"

    aput-object v1, v0, v2

    const-string v1, "sub"

    aput-object v1, v0, v3

    const-string v1, "sub_cs"

    aput-object v1, v0, v4

    const-string v1, "pri"

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->EXTEND_PROJECTION:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "m_size"

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->SIZE_PROJECTION:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "address"

    aput-object v1, v0, v2

    const-string v1, "type"

    aput-object v1, v0, v3

    const-string v1, "charset"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->ADDRESS_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0x8

    iput v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mType:I

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    iput p3, p0, Lcom/mediatek/bluetooth/map/MmsController;->mSimId:I

    invoke-virtual {p2}, Lcom/mediatek/bluetooth/map/Instance;->getInstanceId()I

    move-result v0

    iput v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mInstanceId:I

    iput-object p2, p0, Lcom/mediatek/bluetooth/map/MmsController;->mInstance:Lcom/mediatek/bluetooth/map/Instance;

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/MmsController;->onStart()V

    return-void
.end method

.method private composeMessageItem(JLjava/lang/String;Ljava/lang/String;I)Lcom/mediatek/bluetooth/map/cache/MessageItem;
    .locals 18
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I

    new-instance v11, Lcom/mediatek/bluetooth/map/cache/MessageItem;

    invoke-direct {v11}, Lcom/mediatek/bluetooth/map/cache/MessageItem;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "composeMessageItem(): id is "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ",from is "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ",to is "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", read"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/MmsController;->revertLoadStatus(I)I

    move-result v16

    move-object/from16 v0, p0

    move/from16 v1, p5

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->revertReadStatus(I)I

    move-result v15

    const/4 v2, -0x1

    if-ne v15, v2, :cond_1

    const-string v2, "invalid read status"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    const/4 v11, 0x0

    :cond_0
    :goto_0
    return-object v11

    :cond_1
    const/4 v10, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/part"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "text"

    aput-object v7, v4, v6

    const-string v5, "mid=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    if-eqz v13, :cond_4

    :cond_2
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    const/4 v10, 0x1

    :cond_3
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_4
    invoke-direct/range {p0 .. p2}, Lcom/mediatek/bluetooth/map/MmsController;->getMessageUri(J)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/bluetooth/map/MmsController;->EXTEND_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    if-eqz v12, :cond_5

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_6

    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "no the record:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    const/4 v11, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v2, 0x3

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/MmsController;->revertPriority(I)Z

    move-result v14

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/bluetooth/map/MmsController;->mType:I

    move-wide/from16 v0, p1

    invoke-static {v2, v0, v1}, Lcom/mediatek/bluetooth/map/util/HandleUtil;->getHandle(IJ)J

    move-result-wide v6

    invoke-virtual {v11, v6, v7}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setHandle(J)V

    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSenderAddr(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSenderName(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setReplyAddr(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setRecipientName(Ljava/lang/String;)V

    move-object/from16 v0, p4

    invoke-virtual {v11, v0}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setRecipientAddr(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/bluetooth/map/MmsController;->mType:I

    invoke-virtual {v11, v2}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setMsgType(I)V

    invoke-virtual {v11, v10}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setText(Z)V

    move/from16 v0, v16

    invoke-virtual {v11, v0}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setRecipientStatus(I)V

    invoke-virtual {v11, v15}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setReadStatus(I)V

    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setProtected(Z)V

    const/4 v2, 0x1

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSubject(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v11, v6, v7}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setDatetime(J)V

    const/16 v2, 0x1c8

    invoke-virtual {v11, v2}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setAttachSize(I)V

    invoke-virtual {v11, v14}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setPriority(Z)V

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/bluetooth/map/MmsController;->SIZE_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    if-eqz v17, :cond_0

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v11, v2}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSize(I)V

    :cond_7
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method private composeMessageItem(Landroid/database/Cursor;JLjava/lang/String;Ljava/lang/String;)Lcom/mediatek/bluetooth/map/cache/MessageItem;
    .locals 13
    .param p1    # Landroid/database/Cursor;
    .param p2    # J
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    new-instance v8, Lcom/mediatek/bluetooth/map/cache/MessageItem;

    invoke-direct {v8}, Lcom/mediatek/bluetooth/map/cache/MessageItem;-><init>()V

    const/16 v1, 0x9

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->revertLoadStatus(I)I

    move-result v12

    const/4 v1, -0x1

    if-ne v12, v1, :cond_0

    const/4 v8, 0x0

    :goto_0
    return-object v8

    :cond_0
    const/16 v1, 0xa

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->revertReadStatus(I)I

    move-result v11

    const/4 v1, -0x1

    if-ne v11, v1, :cond_1

    const/4 v8, 0x0

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/part"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "text"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :cond_2
    if-eqz v9, :cond_3

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v7, 0x1

    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    const/16 v1, 0x8

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->revertPriority(I)Z

    move-result v10

    iget v1, p0, Lcom/mediatek/bluetooth/map/MmsController;->mType:I

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v1, v3, v4}, Lcom/mediatek/bluetooth/map/util/HandleUtil;->getHandle(IJ)J

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setHandle(J)V

    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSubject(Ljava/lang/String;)V

    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v8, v3, v4}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setDatetime(J)V

    move-object/from16 v0, p4

    invoke-virtual {v8, v0}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSenderAddr(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSenderName(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setReplyAddr(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setRecipientName(Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v8, v0}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setRecipientAddr(Ljava/lang/String;)V

    iget v1, p0, Lcom/mediatek/bluetooth/map/MmsController;->mType:I

    invoke-virtual {v8, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setMsgType(I)V

    const/4 v1, 0x7

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v8, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSize(I)V

    invoke-virtual {v8, v7}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setText(Z)V

    invoke-virtual {v8, v12}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setRecipientStatus(I)V

    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setAttachSize(I)V

    invoke-virtual {v8, v11}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setReadStatus(I)V

    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setProtected(Z)V

    invoke-virtual {v8, v10}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setPriority(Z)V

    goto/16 :goto_0
.end method

.method private convertFilterToSelection(Lcom/mediatek/bluetooth/map/cache/MessageListRequest;Ljava/lang/StringBuilder;Ljava/util/ArrayList;)[Ljava/lang/String;
    .locals 11
    .param p1    # Lcom/mediatek/bluetooth/map/cache/MessageListRequest;
    .param p2    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/bluetooth/map/cache/MessageListRequest;",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    const-wide/16 v9, 0x0

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getStartTime()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getEndTime()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getReadStatus()I

    move-result v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/MmsController;->convertReadStatus(I)I

    move-result v4

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getFolder()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/MmsController;->convertMailboxType(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getOrignator()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getRecipient()Ljava/lang/String;

    move-result-object v5

    cmp-long v8, v6, v9

    if-lez v8, :cond_1

    cmp-long v8, v0, v9

    if-lez v8, :cond_1

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_0

    const-string v8, " AND "

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v8, "date"

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " between ? AND ?"

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " AND "

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    const/4 v8, -0x1

    if-eq v4, v8, :cond_3

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_2

    const-string v8, " AND "

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v8, "read=?"

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0, v4}, Lcom/mediatek/bluetooth/map/MmsController;->convertReadStatus(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    const/4 v8, 0x0

    return-object v8
.end method

.method private convertMailboxType(Ljava/lang/String;)I
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v0, -0x1

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    const-string v1, "inbox"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const-string v1, "outbox"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    const-string v1, "sent"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    const-string v1, "draft"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v0, 0x3

    goto :goto_0

    :cond_4
    const-string v1, "deleted"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/16 v0, 0x64

    goto :goto_0

    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "convertMailboxType(): the mail box is invalid->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private convertMaskToProjection(I)[Ljava/lang/String;
    .locals 1
    .param p1    # I

    sget-object v0, Lcom/mediatek/bluetooth/map/MmsController;->DEFAULT_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method private convertReadStatus(I)I
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "other map state:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getAddress(JLjava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v3, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAddress(): msg id is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/mediatek/bluetooth/map/MmsController;->getAddressUri(J)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/MmsController;->ADDRESS_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "have no interest in the message type,"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_0
    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ";"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :sswitch_1
    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ";"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "form :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "to :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x89 -> :sswitch_0
        0x97 -> :sswitch_1
    .end sparse-switch
.end method

.method private getAddressUri(J)Landroid/net/Uri;
    .locals 3
    .param p1    # J

    sget-object v1, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/addr"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method private getMailboxUri(I)Landroid/net/Uri;
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getMailboxUri(): the mail box is invalid->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Landroid/provider/Telephony$Mms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    :pswitch_1
    sget-object v0, Landroid/provider/Telephony$Mms$Outbox;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    :pswitch_2
    sget-object v0, Landroid/provider/Telephony$Mms$Sent;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    :pswitch_3
    sget-object v0, Landroid/provider/Telephony$Mms$Draft;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private getMessageUri(J)Landroid/net/Uri;
    .locals 1
    .param p1    # J

    sget-object v0, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private isEmailAddress(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "@"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "MMSController"

    invoke-static {v0, p1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private normalizeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, " "

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "-"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    move-object v0, p1

    goto :goto_0
.end method

.method private revertLoadStatus(I)I
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method private revertPriority(I)Z
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x80
        :pswitch_0
    .end packed-switch
.end method

.method private revertReadStatus(I)I
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private splitAddress(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, ";"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public clearDeletedMessage()V
    .locals 12

    const/4 v11, 0x0

    const/4 v3, 0x0

    const-string v0, "clearDeletedMessage()"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "msg_box"

    aput-object v0, v2, v11

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mDeleteFolder:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    sget-object v0, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/16 v0, 0x64

    if-ne v10, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_1
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mDeleteFolder:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public deleteMessage(J)Z
    .locals 12
    .param p1    # J

    const/16 v11, 0x64

    const/4 v10, 0x0

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteMessage():id is "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "msg_box"

    aput-object v0, v2, v10

    sget-object v0, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    if-ne v9, v11, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mDeleteFolder:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    const/4 v8, 0x1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :goto_1
    return v8

    :cond_0
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "msg_box"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1, v7, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mDeleteFolder:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    const-string v0, "the message does not exist in SMS provider"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    const/4 v8, 0x0

    goto :goto_1
.end method

.method public deregisterListener()V
    .locals 2

    invoke-super {p0}, Lcom/mediatek/bluetooth/map/Controller;->deregisterListener()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/MmsController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    :cond_0
    return-void
.end method

.method public getMessage(Lcom/mediatek/bluetooth/map/cache/MessageRequest;)Lcom/mediatek/bluetooth/map/cache/BMessageObject;
    .locals 23
    .param p1    # Lcom/mediatek/bluetooth/map/cache/MessageRequest;

    new-instance v14, Ljava/lang/String;

    invoke-direct {v14}, Ljava/lang/String;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageRequest;->getMessageId()J

    move-result-wide v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/mediatek/bluetooth/map/MmsController;->getMessageUri(J)Landroid/net/Uri;

    move-result-object v11

    const/4 v15, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageRequest;->isAttachDelivered()Z

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageRequest;->getMessageId()J

    move-result-wide v9

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "getMessage(): id is "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ",attachment is "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/MmsController;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/google/android/mms/pdu/PduPersister;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lcom/google/android/mms/pdu/PduPersister;->load(Landroid/net/Uri;)Lcom/google/android/mms/pdu/GenericPdu;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v15

    :goto_0
    new-instance v12, Lcom/mediatek/bluetooth/map/mime/MmsMime;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-direct {v12, v0, v15}, Lcom/mediatek/bluetooth/map/mime/MmsMime;-><init>(Landroid/content/ContentResolver;Lcom/google/android/mms/pdu/GenericPdu;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/MmsController;->mInstance:Lcom/mediatek/bluetooth/map/Instance;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/mediatek/bluetooth/map/Instance;->getBMessageObject()Lcom/mediatek/bluetooth/map/cache/BMessageObject;

    move-result-object v13

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setEncoding(I)Z

    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setCharset(I)Z

    const/16 v21, -0x1

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setLang(I)Z

    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setMessageType(I)Z

    new-instance v20, Lcom/mediatek/bluetooth/map/VCard;

    invoke-direct/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/VCard;-><init>()V

    invoke-virtual {v12}, Lcom/mediatek/bluetooth/map/mime/MimeBase;->getHeader()Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    move-result-object v21

    move-object/from16 v0, v21

    iget-object v14, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mFrom:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/mediatek/bluetooth/map/MmsController;->isEmailAddress(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_0

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Lcom/mediatek/bluetooth/map/VCard;->setEmail(Ljava/lang/String;)V

    :goto_1
    invoke-virtual/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/VCard;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setOrignator(Ljava/lang/String;)Z

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12}, Lcom/mediatek/bluetooth/map/mime/MimeBase;->getHeader()Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    move-result-object v21

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->splitAddress(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_3

    move-object/from16 v2, v19

    array-length v8, v2

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v8, :cond_2

    aget-object v18, v2, v5

    invoke-virtual/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/VCard;->reset()V

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->isEmailAddress(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_1

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/map/VCard;->setEmail(Ljava/lang/String;)V

    :goto_3
    invoke-virtual/range {v20 .. v20}, Lcom/mediatek/bluetooth/map/VCard;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_0
    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Lcom/mediatek/bluetooth/map/VCard;->setTelephone(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/map/VCard;->setTelephone(Ljava/lang/String;)V

    goto :goto_3

    :cond_2
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->addRecipient(Ljava/lang/String;)Z

    :cond_3
    const/16 v21, 0x2

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setReadStatus(I)V

    :try_start_1
    invoke-virtual {v13}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getFile()Ljava/io/File;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-static {v0, v12}, Lcom/mediatek/bluetooth/map/Rfc822Output;->writeTo(Ljava/io/File;Lcom/mediatek/bluetooth/map/mime/MimeBase;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_4
    invoke-virtual {v13}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getFile()Ljava/io/File;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setContentSize(Ljava/io/File;)Z

    return-object v13

    :catch_1
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_4
.end method

.method public getMessageList(Lcom/mediatek/bluetooth/map/cache/MessageListRequest;)Lcom/mediatek/bluetooth/map/cache/MessageListObject;
    .locals 32
    .param p1    # Lcom/mediatek/bluetooth/map/cache/MessageListRequest;

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getListSize()I

    move-result v23

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getListOffset()I

    move-result v27

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getFolder()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/MmsController;->convertMailboxType(Ljava/lang/String;)I

    move-result v24

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getOrignator()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getRecipient()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getMaxSubjectLen()I

    move-result v25

    const/16 v19, 0x0

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v31, Ljava/util/ArrayList;

    invoke-direct/range {v31 .. v31}, Ljava/util/ArrayList;-><init>()V

    const/16 v4, 0x64

    move/from16 v0, v24

    if-eq v0, v4, :cond_0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->getMailboxUri(I)Landroid/net/Uri;

    move-result-object v5

    if-nez v5, :cond_1

    const-string v4, "unrecognized mailbox uri"

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    const/16 v21, 0x0

    :goto_0
    return-object v21

    :cond_0
    sget-object v5, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "msg_box=?"

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getMessageList(): mSimId is "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/mediatek/bluetooth/map/MmsController;->mSimId:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v30

    move-object/from16 v3, v31

    invoke-direct {v0, v1, v2, v3}, Lcom/mediatek/bluetooth/map/MmsController;->convertFilterToSelection(Lcom/mediatek/bluetooth/map/cache/MessageListRequest;Ljava/lang/StringBuilder;Ljava/util/ArrayList;)[Ljava/lang/String;

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v16

    check-cast v16, [Ljava/lang/String;

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v20, v0

    const/16 v18, 0x0

    :goto_1
    move/from16 v0, v18

    move/from16 v1, v20

    if-ge v0, v1, :cond_2

    aget-object v15, v16, v18

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/bluetooth/map/MmsController;->BASE_PROJECTION:[Ljava/lang/String;

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v9, "date DESC"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v22

    if-nez v22, :cond_3

    const/16 v21, 0x0

    goto/16 :goto_0

    :catch_0
    move-exception v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Throwable;->printStackTrace()V

    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/map/MmsController;->mInstance:Lcom/mediatek/bluetooth/map/Instance;

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/map/Instance;->getMsgListRspCache()Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    move-result-object v21

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "listsize is "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ",current size"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v21 .. v21}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;->getCurrentSize()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    if-lez v23, :cond_4

    if-lez v27, :cond_4

    move-object/from16 v0, v22

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->move(I)Z

    move-result v4

    if-nez v4, :cond_4

    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_4
    const/16 v26, 0x0

    :cond_5
    :goto_2
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_b

    if-eqz v23, :cond_6

    invoke-virtual/range {v21 .. v21}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;->getCurrentSize()I

    move-result v4

    move/from16 v0, v23

    if-ge v4, v0, :cond_b

    :cond_6
    const/4 v4, 0x1

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const/4 v4, 0x1

    if-ne v14, v4, :cond_7

    const/16 v26, 0x1

    :cond_7
    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    new-instance v12, Ljava/lang/String;

    invoke-direct {v12}, Ljava/lang/String;-><init>()V

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13}, Ljava/lang/String;-><init>()V

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11, v12, v13}, Lcom/mediatek/bluetooth/map/MmsController;->getAddress(JLjava/lang/String;Ljava/lang/String;)V

    if-eqz v28, :cond_8

    if-eqz v12, :cond_5

    move-object/from16 v0, v28

    invoke-virtual {v12, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v6, -0x1

    if-eq v4, v6, :cond_5

    :cond_8
    if-eqz v29, :cond_9

    if-eqz v13, :cond_5

    move-object/from16 v0, v29

    invoke-virtual {v13, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v6, -0x1

    if-eq v4, v6, :cond_5

    :cond_9
    if-lez v23, :cond_a

    move-object/from16 v9, p0

    invoke-direct/range {v9 .. v14}, Lcom/mediatek/bluetooth/map/MmsController;->composeMessageItem(JLjava/lang/String;Ljava/lang/String;I)Lcom/mediatek/bluetooth/map/cache/MessageItem;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;->addMessageItem(Lcom/mediatek/bluetooth/map/cache/MessageItem;)Z

    :cond_a
    add-int/lit8 v19, v19, 0x1

    const/4 v4, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;->addSize(I)Z

    goto :goto_2

    :cond_b
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    if-eqz v26, :cond_c

    invoke-virtual/range {v21 .. v21}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;->setNewMessage()Z

    :cond_c
    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->declineListOffset(I)V

    goto/16 :goto_0
.end method

.method public onSendResult(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mMmsConnection:Lcom/mediatek/bluetooth/map/MmsConnection;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/map/MmsConnection;->unregisterListener()V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 0

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/MmsController;->clearDeletedMessage()V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/MmsController;->deregisterListener()V

    return-void
.end method

.method public pushMessage(Lcom/mediatek/bluetooth/map/cache/BMessageObject;)Z
    .locals 32
    .param p1    # Lcom/mediatek/bluetooth/map/cache/BMessageObject;

    const-string v27, "pushMessage()"

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    const/16 v18, 0x0

    const/4 v14, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getContentSize()J

    move-result-wide v21

    new-instance v13, Lcom/mediatek/bluetooth/map/mime/MmsMime;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v13, v0}, Lcom/mediatek/bluetooth/map/mime/MmsMime;-><init>(Landroid/content/ContentResolver;)V

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getFolder()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_2

    const/4 v11, -0x1

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/MmsController;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/bluetooth/map/MmsController;->mSimId:I

    move/from16 v28, v0

    invoke-static/range {v27 .. v28}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->getSimIdBySlotId(Landroid/content/Context;I)J

    move-result-wide v19

    const-wide/16 v27, -0x1

    cmp-long v27, v19, v27

    if-eqz v27, :cond_0

    const/16 v27, -0x1

    move/from16 v0, v27

    if-ne v11, v0, :cond_3

    :cond_0
    const/16 v18, 0x0

    :cond_1
    :goto_1
    return v18

    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/mediatek/bluetooth/map/MmsController;->convertMailboxType(Ljava/lang/String;)I

    move-result v11

    goto :goto_0

    :cond_3
    const/4 v7, 0x0

    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getFile()Ljava/io/File;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-direct {v8, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v7, v8

    :goto_2
    new-instance v15, Lcom/mediatek/bluetooth/map/mime/MimeParser;

    invoke-direct {v15, v7, v13}, Lcom/mediatek/bluetooth/map/mime/MimeParser;-><init>(Ljava/io/InputStream;Lcom/mediatek/bluetooth/map/mime/MimeBase;)V

    invoke-virtual {v15}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->parse()V

    invoke-virtual {v13}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->isHeaderComplete()Z

    move-result v27

    if-nez v27, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getRecipient()Ljava/util/ArrayList;

    move-result-object v24

    new-instance v26, Lcom/mediatek/bluetooth/map/VCard;

    invoke-direct/range {v26 .. v26}, Lcom/mediatek/bluetooth/map/VCard;-><init>()V

    const/4 v9, 0x0

    :goto_3
    if-eqz v24, :cond_4

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v27

    move/from16 v0, v27

    if-ge v9, v0, :cond_4

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Lcom/mediatek/bluetooth/map/VCard;->parse(Ljava/lang/String;)V

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/bluetooth/map/VCard;->getTelephone()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->normalizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->addToField(Ljava/lang/String;)V

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/bluetooth/map/VCard;->reset()V

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "index:"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, ":"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getOrignator()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/mediatek/bluetooth/map/VCard;->parse(Ljava/lang/String;)V

    invoke-virtual/range {v26 .. v26}, Lcom/mediatek/bluetooth/map/VCard;->getTelephone()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->normalizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v13, v6}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->setFromField(Ljava/lang/String;)V

    :cond_5
    const/16 v27, 0x4

    move/from16 v0, v27

    if-ne v11, v0, :cond_6

    const/16 v25, 0x80

    :goto_4
    move/from16 v0, v25

    invoke-virtual {v13, v0}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->generatePdu(I)Lcom/google/android/mms/pdu/MultimediaMessagePdu;

    move-result-object v16

    if-nez v16, :cond_7

    const/16 v18, 0x0

    goto/16 :goto_1

    :cond_6
    const/16 v25, 0x84

    goto :goto_4

    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->isTransparent()Z

    move-result v27

    if-nez v27, :cond_9

    const/4 v10, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/MmsController;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/google/android/mms/pdu/PduPersister;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v17

    if-eqz v10, :cond_8

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/mediatek/bluetooth/map/MmsController;->getMailboxUri(I)Landroid/net/Uri;

    move-result-object v12

    :try_start_1
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v12}, Lcom/google/android/mms/pdu/PduPersister;->persist(Lcom/google/android/mms/pdu/GenericPdu;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v14

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v27, "read"

    const/16 v28, 0x0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v27, "sim_id"

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v28

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v27, "m_size"

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v28

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/16 v29, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v14, v3, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :cond_8
    :goto_6
    const/16 v27, 0x4

    move/from16 v0, v27

    if-ne v11, v0, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/MmsController;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/bluetooth/map/MmsController;->mSimId:I

    move/from16 v28, v0

    invoke-static/range {v27 .. v28}, Lcom/mediatek/bluetooth/map/MmsConnection;->getDefault(Landroid/content/Context;I)Lcom/mediatek/bluetooth/map/MmsConnection;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/bluetooth/map/MmsController;->mMmsConnection:Lcom/mediatek/bluetooth/map/MmsConnection;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/MmsController;->mMmsConnection:Lcom/mediatek/bluetooth/map/MmsConnection;

    move-object/from16 v27, v0

    const-wide/16 v28, -0x1

    new-instance v30, Lcom/google/android/mms/pdu/PduComposer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/MmsController;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    check-cast v16, Lcom/google/android/mms/pdu/SendReq;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/google/android/mms/pdu/PduComposer;-><init>(Landroid/content/Context;Lcom/google/android/mms/pdu/GenericPdu;)V

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/mms/pdu/PduComposer;->make()[B

    move-result-object v30

    invoke-virtual/range {v27 .. v30}, Lcom/mediatek/bluetooth/map/MmsConnection;->send(J[B)V

    if-eqz v14, :cond_1

    const/16 v27, 0x2

    :try_start_2
    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->getMailboxUri(I)Landroid/net/Uri;

    move-result-object v27

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-virtual {v0, v14, v1}, Lcom/google/android/mms/pdu/PduPersister;->move(Landroid/net/Uri;Landroid/net/Uri;)Landroid/net/Uri;
    :try_end_2
    .catch Lcom/google/android/mms/MmsException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_9
    const/4 v10, 0x0

    goto/16 :goto_5

    :catch_2
    move-exception v27

    goto :goto_6
.end method

.method public queryMessage(Ljava/util/HashMap;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v2, v7

    const-string v4, "msg_box"

    aput-object v4, v2, v8

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_0

    :goto_0
    return-void

    :cond_0
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public registerListener(Lcom/mediatek/bluetooth/map/ControllerListener;)V
    .locals 4
    .param p1    # Lcom/mediatek/bluetooth/map/ControllerListener;

    const-string v0, "registerListener"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/mediatek/bluetooth/map/Controller;->registerListener(Lcom/mediatek/bluetooth/map/ControllerListener;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/bluetooth/map/MessageObserver;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Controller;->mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

    iget v2, p0, Lcom/mediatek/bluetooth/map/MmsController;->mType:I

    invoke-direct {v0, p0, v1, v2}, Lcom/mediatek/bluetooth/map/MessageObserver;-><init>(Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;Lcom/mediatek/bluetooth/map/ControllerListener;I)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/MmsController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public restoreMessage(J)Z
    .locals 12
    .param p1    # J

    const/4 v11, 0x0

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "restoreMessage():id is "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "msg_box"

    aput-object v0, v2, v11

    sget-object v0, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const/16 v0, 0x64

    if-ne v9, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mDeleteFolder:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    if-eqz v10, :cond_1

    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "msg_box"

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1, v7, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_0
    :goto_0
    const/4 v8, 0x1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :goto_1
    return v8

    :cond_1
    const-string v0, "no record in delete folder"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, "the message does not exist in MMS provider"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    const/4 v8, 0x0

    goto :goto_1
.end method

.method public revertMailboxType(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "revertMailboxType(): the mail box is invalid->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "inbox"

    goto :goto_0

    :sswitch_1
    const-string v0, "outbox"

    goto :goto_0

    :sswitch_2
    const-string v0, "sent"

    goto :goto_0

    :sswitch_3
    const-string v0, "draft"

    goto :goto_0

    :sswitch_4
    const-string v0, "deleted"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_1
        0x64 -> :sswitch_4
    .end sparse-switch
.end method

.method public setMessageStatus(JI)Z
    .locals 11
    .param p1    # J
    .param p3    # I

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v3, 0x0

    sget-object v0, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v10, [Ljava/lang/String;

    const-string v0, "read"

    aput-object v0, v2, v9

    invoke-direct {p0, p3}, Lcom/mediatek/bluetooth/map/MmsController;->convertReadStatus(I)I

    move-result v8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setMessageStatus():id is "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", state is "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    const/4 v0, -0x1

    if-ne v8, v0, :cond_0

    const-string v0, "the status to be set is invalid"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    move v0, v9

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v8, :cond_1

    const-string v0, "state is same, no need to update"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :goto_2
    move v0, v10

    goto :goto_0

    :cond_1
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "read"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/MmsController;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1, v7, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    const-string v0, "no record for the id"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/MmsController;->log(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public updateInbox()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
