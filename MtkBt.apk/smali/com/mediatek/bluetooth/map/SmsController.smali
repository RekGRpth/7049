.class Lcom/mediatek/bluetooth/map/SmsController;
.super Lcom/mediatek/bluetooth/map/Controller;
.source "SmsController.java"

# interfaces
.implements Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;


# static fields
.field private static final ADDRESS_COLUMN:I = 0x3

.field private static final BODY_COLUMN:I = 0x7

.field private static final CONTACT_ID_COLUMN:I = 0x3

.field private static final CONTACT_NAME_COLUMN:I = 0x2

.field private static final CONTACT_PRESENCE_COLUMN:I = 0x4

.field private static final CONTACT_STATUS_COLUMN:I = 0x5

.field private static final DATE_COLUMN:I = 0x2

.field private static final DEFAULT_PROJECTION:[Ljava/lang/String;

.field private static final ID_COLUMN:I = 0x0

.field private static final INVALID_VALUE_ID:I = -0x1

.field private static final PERSON_COLUMN:I = 0x6

.field private static final PHONE_LABEL_COLUMN:I = 0x1

.field private static final PHONE_NUMBER_COLUMN:I = 0x0

.field private static final READ_COLUMN:I = 0x5

.field private static final REPLY_PATH_PRESENT_COLUMN:I = 0x0

.field private static final SERVICE_CENTER_COLUMN:I = 0x1

.field private static final SERVICE_CENTER_PROJECTION:[Ljava/lang/String;

.field private static final SIZE_COLUMN:I = 0xa

.field private static final STATUS_COLUMN:I = 0x4

.field private static final SUBJECT_COLUMN:I = 0x1

.field private static final THREAD_ID_COLUMN:I = 0x8

.field private static final TYPE_COLUMN:I = 0x9


# instance fields
.field private final EXTRA_FINAL_MESSAGE:Ljava/lang/String;

.field private final EXTRA_MESSAGE_ID:Ljava/lang/String;

.field private final MESSAGE_STATUS_DELIVERED_ACTION:Ljava/lang/String;

.field private final MESSAGE_STATUS_SENT_ACTION:Ljava/lang/String;

.field private final MESSAGE_TYPE_DELETE:I

.field private final SMS_READ_STATUS:I

.field private final SMS_UNREAD_STATUS:I

.field private final TAG:Ljava/lang/String;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mInstance:Lcom/mediatek/bluetooth/map/Instance;

.field private mMasId:I

.field private mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

.field private mPhoneType:I

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mSimId:I

.field private final mType:I

.field private mpendinglist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/bluetooth/map/cache/BMessageObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "subject"

    aput-object v1, v0, v4

    const-string v1, "date"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "address"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "status"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "read"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "person"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "body"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "thread_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "type"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "m_size"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/bluetooth/map/SmsController;->DEFAULT_PROJECTION:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "reply_path_present"

    aput-object v1, v0, v3

    const-string v1, "service_center"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/bluetooth/map/SmsController;->SERVICE_CENTER_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/mediatek/bluetooth/map/Instance;II)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/bluetooth/map/Instance;
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/Controller;-><init>()V

    const-string v0, "MAP-SmsController"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->TAG:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->SMS_READ_STATUS:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->SMS_UNREAD_STATUS:I

    const/16 v0, 0x64

    iput v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->MESSAGE_TYPE_DELETE:I

    const-string v0, "com.mediatek.bluetooth.map.SmsController.action.DELIVER_RESULT"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->MESSAGE_STATUS_DELIVERED_ACTION:Ljava/lang/String;

    const-string v0, "com.mediatek.bluetooth.map.SmsController.action.SENT_RESULT"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->MESSAGE_STATUS_SENT_ACTION:Ljava/lang/String;

    const-string v0, "com.mediatek.bluetooth.map.SmsController.extra.FINAL_MESSAGE"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->EXTRA_FINAL_MESSAGE:Ljava/lang/String;

    const-string v0, "com.mediatek.bluetooth.map.SmsController.extra.MESSAGE_ID"

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->EXTRA_MESSAGE_ID:Ljava/lang/String;

    new-instance v0, Lcom/mediatek/bluetooth/map/SmsController$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/map/SmsController$1;-><init>(Lcom/mediatek/bluetooth/map/SmsController;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    iput p3, p0, Lcom/mediatek/bluetooth/map/SmsController;->mSimId:I

    iput-object p2, p0, Lcom/mediatek/bluetooth/map/SmsController;->mInstance:Lcom/mediatek/bluetooth/map/Instance;

    iput p4, p0, Lcom/mediatek/bluetooth/map/SmsController;->mType:I

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/map/SmsController;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/map/SmsController;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/map/SmsController;Landroid/content/Intent;I)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/map/SmsController;
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/bluetooth/map/SmsController;->handleDeliverResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/bluetooth/map/SmsController;Landroid/content/Intent;I)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/map/SmsController;
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/bluetooth/map/SmsController;->handleSentResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private composeMessageItem(Landroid/database/Cursor;I)Lcom/mediatek/bluetooth/map/cache/MessageItem;
    .locals 11
    .param p1    # Landroid/database/Cursor;
    .param p2    # I

    new-instance v2, Lcom/mediatek/bluetooth/map/cache/MessageItem;

    invoke-direct {v2}, Lcom/mediatek/bluetooth/map/cache/MessageItem;-><init>()V

    const/4 v7, 0x0

    const-string v8, "composeMessageItem()"

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v8, 0x4

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/SmsController;->revertLoadStatus(I)I

    move-result v6

    const/4 v8, -0x1

    if-ne v6, v8, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    const/4 v8, 0x5

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/SmsController;->revertReadStatus(I)I

    move-result v4

    const/4 v8, -0x1

    if-ne v4, v8, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    const/4 v8, 0x7

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    const/4 v1, 0x1

    :goto_1
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x3

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v8, 0x1

    if-ne p2, v8, :cond_3

    move-object v3, v0

    :goto_2
    iget v8, p0, Lcom/mediatek/bluetooth/map/SmsController;->mType:I

    const/4 v9, 0x0

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-static {v8, v9, v10}, Lcom/mediatek/bluetooth/map/util/HandleUtil;->getHandle(IJ)J

    move-result-wide v8

    invoke-virtual {v2, v8, v9}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setHandle(J)V

    const/4 v8, 0x1

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSubject(Ljava/lang/String;)V

    const/4 v8, 0x2

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v2, v8, v9}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setDatetime(J)V

    invoke-virtual {v2, v3}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSenderAddr(Ljava/lang/String;)V

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSenderName(Ljava/lang/String;)V

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setReplyAddr(Ljava/lang/String;)V

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setRecipientName(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setRecipientAddr(Ljava/lang/String;)V

    iget v8, p0, Lcom/mediatek/bluetooth/map/SmsController;->mType:I

    invoke-virtual {v2, v8}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setMsgType(I)V

    const/16 v8, 0xa

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSize(I)V

    invoke-virtual {v2, v1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setText(Z)V

    invoke-virtual {v2, v6}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setRecipientStatus(I)V

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setAttachSize(I)V

    invoke-virtual {v2, v4}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setReadStatus(I)V

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setProtected(Z)V

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setPriority(Z)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    move-object v5, v0

    goto :goto_2
.end method

.method private convertFilterToSelection(Lcom/mediatek/bluetooth/map/cache/MessageListRequest;Ljava/lang/StringBuilder;Ljava/util/ArrayList;)[Ljava/lang/String;
    .locals 11
    .param p1    # Lcom/mediatek/bluetooth/map/cache/MessageListRequest;
    .param p2    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/bluetooth/map/cache/MessageListRequest;",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    const-wide/16 v9, 0x0

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getStartTime()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getEndTime()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getReadStatus()I

    move-result v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/SmsController;->convertReadStatus(I)I

    move-result v4

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getFolder()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/SmsController;->convertMailboxType(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getOrignator()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getRecipient()Ljava/lang/String;

    move-result-object v5

    cmp-long v8, v6, v9

    if-lez v8, :cond_1

    cmp-long v8, v0, v9

    if-lez v8, :cond_1

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_0

    const-string v8, " AND "

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v8, "date"

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " between ? AND ?"

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    const/4 v8, -0x1

    if-eq v4, v8, :cond_3

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_2

    const-string v8, " AND "

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v8, "read=?"

    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    const/4 v8, 0x0

    return-object v8
.end method

.method private convertMailboxType(Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v0, -0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "inbox"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const-string v1, "outbox"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    const-string v1, "sent"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v0, 0x2

    goto :goto_0

    :cond_4
    const-string v1, "draft"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x3

    goto :goto_0

    :cond_5
    const-string v1, "deleted"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x64

    goto :goto_0
.end method

.method private convertMaskToProjection(I)[Ljava/lang/String;
    .locals 1
    .param p1    # I

    sget-object v0, Lcom/mediatek/bluetooth/map/SmsController;->DEFAULT_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method private convertReadStatus(I)I
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "other map state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getMailboxUri(I)Landroid/net/Uri;
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Landroid/provider/Telephony$Sms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    :pswitch_1
    sget-object v0, Landroid/provider/Telephony$Sms$Outbox;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    :pswitch_2
    sget-object v0, Landroid/provider/Telephony$Sms$Sent;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    :pswitch_3
    sget-object v0, Landroid/provider/Telephony$Sms$Draft;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private handleDeliverResult(Landroid/content/Intent;I)V
    .locals 12
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    const-string v0, "pdu"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    move-object v11, v0

    check-cast v11, [B

    const-string v0, "com.mediatek.bluetooth.map.SmsController.extra.MESSAGE_ID"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    if-eqz v11, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v11}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v10

    if-eqz v10, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "status"

    invoke-virtual {v10}, Landroid/telephony/SmsMessage;->getStatus()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v7, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v0, "update status"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

    if-eqz v0, :cond_3

    const-wide/16 v3, -0x1

    cmp-long v0, v8, v3

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

    iget v3, p0, Lcom/mediatek/bluetooth/map/SmsController;->mType:I

    const/4 v4, 0x1

    invoke-interface {v0, v8, v9, v3, v4}, Lcom/mediatek/bluetooth/map/ControllerListener;->onMessageDelivered(JII)V

    :cond_3
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private handleSentResult(Landroid/content/Intent;I)V
    .locals 13
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    const/4 v12, 0x0

    const-string v0, "errorCode"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const-string v0, "com.mediatek.bluetooth.map.SmsController.extra.FINAL_MESSAGE"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    const-string v0, "com.mediatek.bluetooth.map.SmsController.extra.MESSAGE_ID"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "type"

    aput-object v3, v2, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleSentResult:result is "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", error is "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", id is "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const/4 v12, 0x1

    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const/4 v0, 0x4

    if-ne v11, v0, :cond_2

    const-string v0, "move message to sent folder"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContext:Landroid/content/Context;

    const/4 v3, 0x2

    invoke-static {v0, v1, v3, v7}, Landroid/provider/Telephony$Sms;->moveMessageToFolder(Landroid/content/Context;Landroid/net/Uri;II)Z

    :goto_0
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

    if-eqz v0, :cond_1

    const-wide/16 v3, -0x1

    cmp-long v0, v8, v3

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

    iget v3, p0, Lcom/mediatek/bluetooth/map/SmsController;->mType:I

    const/4 v4, 0x1

    invoke-interface {v0, v8, v9, v3, v4}, Lcom/mediatek/bluetooth/map/ControllerListener;->onMessageSent(JII)V

    :cond_1
    return-void

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "the message is not in outbox:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "no message record:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "MAP-SmsController"

    invoke-static {v0, p1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private normalizeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const-string v0, " "

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "-"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    move-object v0, p1

    goto :goto_0
.end method

.method private revertLoadStatus(I)I
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method private revertReadStatus(I)I
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "error: the read status from sms provider is invalid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public clearDeletedMessage()V
    .locals 12

    const/4 v11, 0x0

    const/4 v3, 0x0

    const-string v0, "clearDeletedMessage()"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "type"

    aput-object v0, v2, v11

    sget-object v1, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mDeleteFolder:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/16 v0, 0x64

    if-ne v10, v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_1
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mDeleteFolder:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public deleteMessage(J)Z
    .locals 12
    .param p1    # J

    const/16 v11, 0x64

    const/4 v10, 0x0

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteMessage():id is "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "type"

    aput-object v0, v2, v10

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    if-ne v9, v11, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mDeleteFolder:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    const/4 v8, 0x1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :goto_1
    return v8

    :cond_0
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "type"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1, v7, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mDeleteFolder:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "succeed"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "the message does not exist in SMS provider"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v8, 0x0

    goto :goto_1
.end method

.method public deregisterListener()V
    .locals 2

    const-string v0, "deregisterListener"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    invoke-super {p0}, Lcom/mediatek/bluetooth/map/Controller;->deregisterListener()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/SmsController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    :cond_0
    return-void
.end method

.method public getMessage(Lcom/mediatek/bluetooth/map/cache/MessageRequest;)Lcom/mediatek/bluetooth/map/cache/BMessageObject;
    .locals 25
    .param p1    # Lcom/mediatek/bluetooth/map/cache/MessageRequest;

    const-string v2, "getMessage()"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageRequest;->isAttachDelivered()Z

    move-result v9

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageRequest;->getMessageId()J

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageRequest;->getCharset()I

    move-result v12

    sget-object v2, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/mediatek/bluetooth/map/SmsController;->DEFAULT_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    if-eqz v19, :cond_0

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "find no record for the request : id is "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v10, 0x0

    :goto_0
    return-object v10

    :cond_1
    const/4 v2, 0x7

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    const/4 v2, 0x3

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v2, 0x9

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    new-instance v21, Ljava/lang/String;

    invoke-direct/range {v21 .. v21}, Ljava/lang/String;-><init>()V

    new-instance v22, Ljava/lang/String;

    invoke-direct/range {v22 .. v22}, Ljava/lang/String;-><init>()V

    const/16 v16, 0x0

    const/4 v2, 0x1

    move/from16 v0, v17

    if-ne v0, v2, :cond_2

    move-object/from16 v21, v8

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/SmsController;->normalizeString(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/bluetooth/map/SmsController;->mSimId:I

    invoke-static {v2}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->getPhoneNumber(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/SmsController;->normalizeString(Ljava/lang/String;)Ljava/lang/String;

    :goto_1
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v18

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/telephony/SmsManager;->divideMessage(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/map/SmsController;->mInstance:Lcom/mediatek/bluetooth/map/Instance;

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/map/Instance;->getBMessageObject()Lcom/mediatek/bluetooth/map/cache/BMessageObject;

    move-result-object v10

    new-instance v24, Lcom/mediatek/bluetooth/map/VCard;

    invoke-direct/range {v24 .. v24}, Lcom/mediatek/bluetooth/map/VCard;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/map/VCard;->setTelephone(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Lcom/mediatek/bluetooth/map/VCard;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setOrignator(Ljava/lang/String;)Z

    invoke-virtual/range {v24 .. v24}, Lcom/mediatek/bluetooth/map/VCard;->reset()V

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/map/VCard;->setTelephone(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Lcom/mediatek/bluetooth/map/VCard;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->addRecipient(Ljava/lang/String;)Z

    const/4 v2, 0x5

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/SmsController;->revertReadStatus(I)I

    move-result v2

    invoke-virtual {v10, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setReadStatus(I)V

    invoke-virtual {v10, v12}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setCharset(I)Z

    const/4 v2, -0x1

    invoke-virtual {v10, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setEncoding(I)Z

    const/4 v2, -0x1

    invoke-virtual {v10, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setLang(I)Z

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/bluetooth/map/SmsController;->mType:I

    invoke-virtual {v10, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->setMessageType(I)Z

    :goto_2
    if-eqz v20, :cond_6

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v16

    if-ge v0, v2, :cond_6

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    const/4 v2, 0x1

    if-ne v12, v2, :cond_3

    invoke-virtual {v13}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    :goto_3
    invoke-virtual {v10, v11}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->addContent([B)Z

    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    :cond_2
    move-object/from16 v22, v8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/mediatek/bluetooth/map/SmsController;->mSimId:I

    invoke-static {v2}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->getPhoneNumber(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/SmsController;->normalizeString(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/SmsController;->normalizeString(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_1

    :cond_3
    if-nez v12, :cond_5

    const/4 v2, 0x1

    move/from16 v0, v17

    if-ne v0, v2, :cond_4

    invoke-static {}, Lcom/mediatek/bluetooth/map/SmsMessageEntity;->getDefault()Lcom/mediatek/bluetooth/map/SmsMessageEntity;

    move-result-object v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/bluetooth/map/SmsController;->mSimId:I

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v2, v0, v1, v13, v4}, Lcom/mediatek/bluetooth/map/SmsMessageEntity;->getDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)[B

    move-result-object v11

    goto :goto_3

    :cond_4
    invoke-static {}, Lcom/mediatek/bluetooth/map/SmsMessageEntity;->getDefault()Lcom/mediatek/bluetooth/map/SmsMessageEntity;

    move-result-object v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/mediatek/bluetooth/map/SmsController;->mSimId:I

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v2, v0, v1, v13, v4}, Lcom/mediatek/bluetooth/map/SmsMessageEntity;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)[B

    move-result-object v11

    goto :goto_3

    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unknown charset type:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v10, 0x0

    goto/16 :goto_0

    :cond_6
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public getMessageList(Lcom/mediatek/bluetooth/map/cache/MessageListRequest;)Lcom/mediatek/bluetooth/map/cache/MessageListObject;
    .locals 24
    .param p1    # Lcom/mediatek/bluetooth/map/cache/MessageListRequest;

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getListSize()I

    move-result v14

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getListOffset()I

    move-result v19

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getFolder()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/SmsController;->convertMailboxType(Ljava/lang/String;)I

    move-result v15

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getOrignator()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getRecipient()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->getMaxSubjectLen()I

    move-result v16

    const/4 v12, 0x0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/mediatek/bluetooth/map/SmsController;->DEFAULT_PROJECTION:[Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getMessageList(): listsize is "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ",listoffset is"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", maxSubjectLen is "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ",mailbox is "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/16 v4, 0x64

    if-eq v15, v4, :cond_0

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/mediatek/bluetooth/map/SmsController;->getMailboxUri(I)Landroid/net/Uri;

    move-result-object v5

    if-nez v5, :cond_1

    const-string v4, "unrecognized mailbox uri"

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v13, 0x0

    :goto_0
    return-object v13

    :cond_0
    sget-object v5, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "type=?"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v15}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    move-object/from16 v3, v23

    invoke-direct {v0, v1, v2, v3}, Lcom/mediatek/bluetooth/map/SmsController;->convertFilterToSelection(Lcom/mediatek/bluetooth/map/cache/MessageListRequest;Ljava/lang/StringBuilder;Ljava/util/ArrayList;)[Ljava/lang/String;

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v8, v8, [Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    const-string v9, "date DESC"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v17

    if-nez v17, :cond_2

    const/4 v13, 0x0

    goto :goto_0

    :catch_0
    move-exception v11

    invoke-virtual {v11}, Ljava/lang/Throwable;->printStackTrace()V

    const-string v4, "fail to query"

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v13, 0x0

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/map/SmsController;->mInstance:Lcom/mediatek/bluetooth/map/Instance;

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/map/Instance;->getMsgListRspCache()Lcom/mediatek/bluetooth/map/cache/MessageListObject;

    move-result-object v13

    if-lez v14, :cond_3

    if-lez v19, :cond_3

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->move(I)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "list size is zero or no cursor"

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v13, 0x0

    goto :goto_0

    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mSimId:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v7, v0, Lcom/mediatek/bluetooth/map/SmsController;->mSimId:I

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/16 v18, 0x0

    :cond_4
    :goto_1
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_a

    if-eqz v14, :cond_5

    invoke-virtual {v13}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;->getCurrentSize()I

    move-result v4

    if-ge v4, v14, :cond_a

    :cond_5
    const/4 v4, 0x5

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v7, 0x1

    if-ne v4, v7, :cond_6

    const/16 v18, 0x1

    :cond_6
    const/4 v4, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    if-eqz v21, :cond_7

    if-eqz v10, :cond_7

    const/4 v4, 0x1

    if-eq v15, v4, :cond_7

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v7, -0x1

    if-eq v4, v7, :cond_4

    :cond_7
    if-eqz v20, :cond_8

    if-eqz v10, :cond_8

    const/4 v4, 0x1

    if-ne v15, v4, :cond_8

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v7, -0x1

    if-eq v4, v7, :cond_4

    :cond_8
    if-lez v14, :cond_9

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v15}, Lcom/mediatek/bluetooth/map/SmsController;->composeMessageItem(Landroid/database/Cursor;I)Lcom/mediatek/bluetooth/map/cache/MessageItem;

    move-result-object v4

    invoke-virtual {v13, v4}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;->addMessageItem(Lcom/mediatek/bluetooth/map/cache/MessageItem;)Z

    :cond_9
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    :cond_a
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    if-eqz v18, :cond_b

    invoke-virtual {v13}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;->setNewMessage()Z

    :cond_b
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->declineListOffset(I)V

    invoke-virtual {v13, v12}, Lcom/mediatek/bluetooth/map/cache/MessageListObject;->addSize(I)Z

    goto/16 :goto_0
.end method

.method public onStart()V
    .locals 3

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.mediatek.bluetooth.map.SmsController.action.SENT_RESULT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.mediatek.bluetooth.map.SmsController.action.DELIVER_RESULT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/SmsController;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onStop()V
    .locals 2

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/SmsController;->clearDeletedMessage()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/SmsController;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/SmsController;->deregisterListener()V

    return-void
.end method

.method public pushMessage(Lcom/mediatek/bluetooth/map/cache/BMessageObject;)Z
    .locals 40
    .param p1    # Lcom/mediatek/bluetooth/map/cache/BMessageObject;

    const/16 v37, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getContentSize()J

    move-result-wide v5

    long-to-int v0, v5

    move/from16 v35, v0

    move/from16 v0, v35

    new-array v12, v0, [B

    const/16 v36, 0x0

    const-wide/16 v25, -0x1

    const/16 v31, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getCharset()I

    move-result v11

    new-instance v39, Lcom/mediatek/bluetooth/map/VCard;

    invoke-direct/range {v39 .. v39}, Lcom/mediatek/bluetooth/map/VCard;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getFolder()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/bluetooth/map/SmsController;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/mediatek/bluetooth/map/SmsController;->mSimId:I

    invoke-static {v3, v5}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->getSimIdBySlotId(Landroid/content/Context;I)J

    move-result-wide v33

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pushMessage():"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v35

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const-wide/16 v5, 0x0

    cmp-long v3, v33, v5

    if-gez v3, :cond_0

    const-string v3, "no sim card in current sim slot"

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    if-nez v17, :cond_1

    const/16 v23, -0x1

    :goto_1
    const/4 v3, -0x1

    move/from16 v0, v23

    if-ne v0, v3, :cond_2

    const-string v3, "invalid folder"

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/SmsController;->convertMailboxType(Ljava/lang/String;)I

    move-result v23

    goto :goto_1

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->isTransparent()Z

    move-result v3

    if-nez v3, :cond_7

    const/16 v22, 0x1

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getContent([B)Z

    move-result v31

    const/4 v3, 0x1

    if-ne v11, v3, :cond_8

    new-instance v37, Ljava/lang/String;

    move-object/from16 v0, v37

    invoke-direct {v0, v12}, Ljava/lang/String;-><init>([B)V

    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getFinalRecipient()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v3}, Lcom/mediatek/bluetooth/map/VCard;->parse(Ljava/lang/String;)V

    invoke-virtual/range {v39 .. v39}, Lcom/mediatek/bluetooth/map/VCard;->getTelephone()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/mediatek/bluetooth/map/SmsController;->normalizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v39 .. v39}, Lcom/mediatek/bluetooth/map/VCard;->reset()V

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getOrignator()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v39

    invoke-virtual {v0, v3}, Lcom/mediatek/bluetooth/map/VCard;->parse(Ljava/lang/String;)V

    invoke-virtual/range {v39 .. v39}, Lcom/mediatek/bluetooth/map/VCard;->getTelephone()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/mediatek/bluetooth/map/SmsController;->normalizeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    if-eqz v22, :cond_10

    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "type"

    const/4 v3, 0x4

    move/from16 v0, v23

    if-ne v0, v3, :cond_d

    const/4 v3, 0x2

    :goto_4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v14, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "m_size"

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "date"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v14, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v3, 0x1

    move/from16 v0, v23

    if-ne v0, v3, :cond_e

    const-string v3, "address"

    move-object/from16 v0, v28

    invoke-virtual {v14, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_5
    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getReadStatus()I

    move-result v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/mediatek/bluetooth/map/SmsController;->convertReadStatus(I)I

    move-result v29

    const/4 v3, -0x1

    move/from16 v0, v29

    if-eq v0, v3, :cond_3

    const-string v3, "read"

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_3
    const-string v3, "body"

    move-object/from16 v0, v37

    invoke-virtual {v14, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "status"

    const/16 v5, 0x20

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "seen"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "sim_id"

    invoke-static/range {v33 .. v34}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v14, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    if-eqz v36, :cond_5

    const-string v3, "protocol"

    invoke-virtual/range {v36 .. v36}, Landroid/telephony/SmsMessage;->getProtocolIdentifier()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual/range {v36 .. v36}, Landroid/telephony/SmsMessage;->getPseudoSubject()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_4

    const-string v3, "subject"

    invoke-virtual/range {v36 .. v36}, Landroid/telephony/SmsMessage;->getPseudoSubject()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const-string v5, "reply_path_present"

    invoke-virtual/range {v36 .. v36}, Landroid/telephony/SmsMessage;->isReplyPathPresent()Z

    move-result v3

    if-eqz v3, :cond_f

    const/4 v3, 0x1

    :goto_6
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v14, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "service_center"

    invoke-virtual/range {v36 .. v36}, Landroid/telephony/SmsMessage;->getServiceCenterAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v5, v14}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v8, "_id"

    aput-object v8, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    if-eqz v13, :cond_6

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x0

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v25

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_6
    :goto_7
    const/4 v3, 0x4

    move/from16 v0, v23

    if-ne v0, v3, :cond_14

    if-eqz v30, :cond_14

    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v24

    const/4 v7, 0x0

    if-nez v37, :cond_11

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_7
    const/16 v22, 0x0

    goto/16 :goto_2

    :cond_8
    if-nez v11, :cond_c

    const/16 v21, 0x0

    const/16 v27, 0x0

    const/16 v19, 0x0

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    :cond_9
    :goto_8
    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->getContentSize(I)I

    move-result v19

    if-lez v19, :cond_b

    move/from16 v0, v19

    new-array v0, v0, [B

    move-object/from16 v18, v0

    const/4 v3, 0x0

    move/from16 v0, v27

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v12, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int v27, v27, v19

    add-int/lit8 v21, v21, 0x1

    if-eqz v12, :cond_a

    if-eqz v31, :cond_a

    :try_start_0
    invoke-static/range {v18 .. v18}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v36

    if-eqz v36, :cond_9

    invoke-virtual/range {v36 .. v36}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v38

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_8

    :catch_0
    move-exception v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_a
    :try_start_1
    const-string v3, "fail to send SMS message, the content is null"

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_b
    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    goto/16 :goto_3

    :cond_c
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unknown charset:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_d
    move/from16 v3, v23

    goto/16 :goto_4

    :cond_e
    const-string v3, "address"

    move-object/from16 v0, v30

    invoke-virtual {v14, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_f
    const/4 v3, 0x0

    goto/16 :goto_6

    :cond_10
    const-wide/16 v25, -0x1

    goto/16 :goto_7

    :cond_11
    move-object/from16 v0, v24

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Landroid/telephony/SmsManager;->divideMessage(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    new-instance v10, Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v10, v3}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v9, Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v9, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/16 v20, 0x0

    :goto_9
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    move/from16 v0, v20

    if-ge v0, v3, :cond_13

    new-instance v15, Landroid/content/Intent;

    const-string v3, "com.mediatek.bluetooth.map.SmsController.action.DELIVER_RESULT"

    invoke-direct {v15, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v32, Landroid/content/Intent;

    const-string v3, "com.mediatek.bluetooth.map.SmsController.action.SENT_RESULT"

    move-object/from16 v0, v32

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "com.mediatek.bluetooth.map.SmsController.extra.MESSAGE_ID"

    move-object/from16 v0, v32

    move-wide/from16 v1, v25

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v3, "com.mediatek.bluetooth.map.SmsController.extra.MESSAGE_ID"

    move-wide/from16 v0, v25

    invoke-virtual {v15, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move/from16 v0, v20

    if-ne v0, v3, :cond_12

    const-string v3, "com.mediatek.bluetooth.map.SmsController.extra.FINAL_MESSAGE"

    const/4 v5, 0x1

    move-object/from16 v0, v32

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v3, "com.mediatek.bluetooth.map.SmsController.extra.FINAL_MESSAGE"

    const/4 v5, 0x1

    invoke-virtual {v15, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/bluetooth/map/SmsController;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v3, v5, v15, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/bluetooth/map/SmsController;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v32

    invoke-static {v3, v0, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v20, v20, 0x1

    goto :goto_9

    :cond_13
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/mediatek/bluetooth/map/SmsController;->mSimId:I

    move-object/from16 v5, v30

    invoke-static/range {v5 .. v10}, Landroid/telephony/gemini/GeminiSmsManager;->sendMultipartTextMessageGemini(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    :cond_14
    const/4 v3, 0x1

    goto/16 :goto_0
.end method

.method public queryMessage(Ljava/util/HashMap;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v2, v7

    const-string v4, "type"

    aput-object v4, v2, v8

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_0

    :goto_0
    return-void

    :cond_0
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public registerListener(Lcom/mediatek/bluetooth/map/ControllerListener;)V
    .locals 4
    .param p1    # Lcom/mediatek/bluetooth/map/ControllerListener;

    const-string v0, "registerListener()"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/mediatek/bluetooth/map/Controller;->registerListener(Lcom/mediatek/bluetooth/map/ControllerListener;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/bluetooth/map/MessageObserver;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/Controller;->mListener:Lcom/mediatek/bluetooth/map/ControllerListener;

    iget v2, p0, Lcom/mediatek/bluetooth/map/SmsController;->mType:I

    invoke-direct {v0, p0, v1, v2}, Lcom/mediatek/bluetooth/map/MessageObserver;-><init>(Lcom/mediatek/bluetooth/map/MessageObserver$ControllerHelper;Lcom/mediatek/bluetooth/map/ControllerListener;I)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/SmsController;->mMessageObserver:Lcom/mediatek/bluetooth/map/MessageObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public restoreMessage(J)Z
    .locals 12
    .param p1    # J

    const/4 v11, 0x0

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteMessage():id is "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "type"

    aput-object v0, v2, v11

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const/16 v0, 0x64

    if-ne v9, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/Controller;->mDeleteFolder:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    if-eqz v10, :cond_1

    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "type"

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1, v7, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_0
    :goto_0
    const/4 v8, 0x1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :goto_1
    return v8

    :cond_1
    const-string v0, "no record in delete folder"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, "the message does not exist in MMS provider"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    const/4 v8, 0x0

    goto :goto_1
.end method

.method public revertMailboxType(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    const-string v0, "inbox"

    goto :goto_0

    :sswitch_1
    const-string v0, "outbox"

    goto :goto_0

    :sswitch_2
    const-string v0, "sent"

    goto :goto_0

    :sswitch_3
    const-string v0, "draft"

    goto :goto_0

    :sswitch_4
    const-string v0, "deleted"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_1
        0x64 -> :sswitch_4
    .end sparse-switch
.end method

.method public setMessageStatus(JI)Z
    .locals 12
    .param p1    # J
    .param p3    # I

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setMessageStatus():id is "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", state is "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    new-array v9, v11, [Ljava/lang/String;

    const-string v0, "read"

    aput-object v0, v9, v10

    invoke-direct {p0, p3}, Lcom/mediatek/bluetooth/map/SmsController;->convertReadStatus(I)I

    move-result v8

    const/4 v0, -0x1

    if-ne v8, v0, :cond_0

    const-string v0, "the status to be set is invalid"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    move v0, v10

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v8, :cond_2

    const-string v0, "state is same, no need to update"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsController;->log(Ljava/lang/String;)V

    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v11

    goto :goto_0

    :cond_2
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "read"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/SmsController;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v0, v1, v7, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method

.method public updateInbox()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
