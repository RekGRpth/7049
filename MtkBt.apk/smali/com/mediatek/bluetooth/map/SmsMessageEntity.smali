.class public Lcom/mediatek/bluetooth/map/SmsMessageEntity;
.super Ljava/lang/Object;
.source "SmsMessageEntity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SmsMessageEntity"

.field private static mEntity:Lcom/mediatek/bluetooth/map/SmsMessageEntity;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getCdmaDeliverPdu(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 13
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v6, 0x0

    invoke-static {p1}, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->parse(Ljava/lang/String;)Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-object v9

    :cond_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v10, 0x64

    invoke-direct {v0, v10}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    new-instance v1, Lcom/android/internal/telephony/cdma/sms/BearerData;

    invoke-direct {v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;-><init>()V

    const/4 v10, 0x1

    iput v10, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->messageType:I

    iput-boolean v11, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->deliveryAckReq:Z

    iput-boolean v11, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->userAckReq:Z

    iput-boolean v11, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->readAckReq:Z

    iput-boolean v11, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->reportReq:Z

    new-instance v8, Lcom/android/internal/telephony/cdma/sms/UserData;

    invoke-direct {v8}, Lcom/android/internal/telephony/cdma/sms/UserData;-><init>()V

    iput-object p2, v8, Lcom/android/internal/telephony/cdma/sms/UserData;->payloadStr:Ljava/lang/String;

    iput-object v9, v8, Lcom/android/internal/telephony/cdma/sms/UserData;->userDataHeader:Lcom/android/internal/telephony/SmsHeader;

    iput-object v8, v1, Lcom/android/internal/telephony/cdma/sms/BearerData;->userData:Lcom/android/internal/telephony/cdma/sms/UserData;

    invoke-static {v1}, Lcom/android/internal/telephony/cdma/sms/BearerData;->encode(Lcom/android/internal/telephony/cdma/sms/BearerData;)[B

    move-result-object v5

    const/4 v10, 0x0

    :try_start_0
    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->writeInt(I)V

    const v7, 0x10040200

    invoke-virtual {v3, v7}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget v10, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->digitMode:I

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->write(I)V

    iget v10, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberMode:I

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->write(I)V

    iget v10, v2, Lcom/android/internal/telephony/SmsAddress;->ton:I

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->write(I)V

    iget v10, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberPlan:I

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->write(I)V

    iget v10, v2, Lcom/android/internal/telephony/cdma/sms/CdmaSmsAddress;->numberOfDigits:I

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->write(I)V

    iget-object v10, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    const/4 v11, 0x0

    iget-object v12, v2, Lcom/android/internal/telephony/SmsAddress;->origBytes:[B

    array-length v12, v12

    invoke-virtual {v3, v10, v11, v12}, Ljava/io/DataOutputStream;->write([BII)V

    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->writeInt(I)V

    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->write(I)V

    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->write(I)V

    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->write(I)V

    if-eqz v5, :cond_1

    array-length v10, v5

    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->write(I)V

    const/4 v10, 0x0

    array-length v11, v5

    invoke-virtual {v3, v5, v10, v11}, Ljava/io/DataOutputStream;->write([BII)V

    :cond_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    array-length v6, v10

    invoke-virtual {v3}, Ljava/io/FilterOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    goto/16 :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public static getDefault()Lcom/mediatek/bluetooth/map/SmsMessageEntity;
    .locals 1

    sget-object v0, Lcom/mediatek/bluetooth/map/SmsMessageEntity;->mEntity:Lcom/mediatek/bluetooth/map/SmsMessageEntity;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/bluetooth/map/SmsMessageEntity;

    invoke-direct {v0}, Lcom/mediatek/bluetooth/map/SmsMessageEntity;-><init>()V

    sput-object v0, Lcom/mediatek/bluetooth/map/SmsMessageEntity;->mEntity:Lcom/mediatek/bluetooth/map/SmsMessageEntity;

    :cond_0
    sget-object v0, Lcom/mediatek/bluetooth/map/SmsMessageEntity;->mEntity:Lcom/mediatek/bluetooth/map/SmsMessageEntity;

    return-object v0
.end method

.method private getGsmDeliverPdu(Ljava/lang/String;Ljava/lang/String;I)[B
    .locals 12
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    new-instance v8, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0xb4

    invoke-direct {v8, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    const/4 v6, 0x0

    const/4 v7, -0x1

    const/4 v10, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "message body is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsMessageEntity;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/android/internal/telephony/gsm/SmsMessage;->calculateLength(Ljava/lang/CharSequence;Z)Lcom/android/internal/telephony/SmsMessageBase$TextEncodingDetails;

    move-result-object v9

    iget v6, v9, Lcom/android/internal/telephony/SmsMessageBase$TextEncodingDetails;->codeUnitSize:I

    iget v7, v9, Lcom/android/internal/telephony/SmsMessageBase$TextEncodingDetails;->shiftLangId:I

    const/4 v0, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v7}, Lcom/android/internal/telephony/gsm/SmsMessage;->getDeliverPduWithLang(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BJII)Lcom/android/internal/telephony/gsm/SmsMessage$DeliverPdu;

    move-result-object v11

    if-nez v11, :cond_0

    const-string v0, "fail to get deliver pdu"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/SmsMessageEntity;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v8, p3}, Lcom/mediatek/bluetooth/map/SmsMessageEntity;->setScaAddress(Ljava/io/ByteArrayOutputStream;I)V

    iget-object v0, v11, Lcom/android/internal/telephony/SmsMessageBase$PduBase;->encodedMessage:[B

    const/4 v1, 0x0

    iget-object v2, v11, Lcom/android/internal/telephony/SmsMessageBase$PduBase;->encodedMessage:[B

    array-length v2, v2

    invoke-virtual {v8, v0, v1, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "SmsMessageEntity"

    invoke-static {v0, p1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private setScaAddress(Ljava/io/ByteArrayOutputStream;I)V
    .locals 4
    .param p1    # Ljava/io/ByteArrayOutputStream;
    .param p2    # I

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-static {}, Lcom/mediatek/telephony/TelephonyManagerEx;->getDefault()Lcom/mediatek/telephony/TelephonyManagerEx;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/mediatek/telephony/TelephonyManagerEx;->getScAddress(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->networkPortionToCalledPartyBCDWithLength(Ljava/lang/String;)[B

    move-result-object v1

    :cond_0
    if-eqz v1, :cond_1

    array-length v2, v1

    invoke-virtual {p1, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0
.end method


# virtual methods
.method public getDeliverPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)[B
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I

    const/4 v1, 0x0

    invoke-static {}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->isGeminiSupport()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p4}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->getGeminiNetworkType(I)I

    move-result v0

    :goto_0
    const/4 v2, 0x2

    if-ne v2, v0, :cond_1

    invoke-direct {p0, p1, p3}, Lcom/mediatek/bluetooth/map/SmsMessageEntity;->getCdmaDeliverPdu(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v2

    :goto_1
    return-object v2

    :cond_0
    invoke-static {}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->getNetworkType()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    if-ne v2, v0, :cond_2

    invoke-direct {p0, p1, p3, p4}, Lcom/mediatek/bluetooth/map/SmsMessageEntity;->getGsmDeliverPdu(Ljava/lang/String;Ljava/lang/String;I)[B

    move-result-object v2

    goto :goto_1

    :cond_2
    const-string v2, "SmsMessageEntity"

    const-string v3, "unkown net type"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)[B
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I

    const/4 v5, 0x0

    const/4 v1, 0x0

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0xb4

    invoke-direct {v0, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static {p1, p2, p3, v5}, Landroid/telephony/SmsMessage;->getSubmitPdu(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/telephony/SmsMessage$SubmitPdu;

    move-result-object v2

    invoke-direct {p0, v0, p4}, Lcom/mediatek/bluetooth/map/SmsMessageEntity;->setScaAddress(Ljava/io/ByteArrayOutputStream;I)V

    iget-object v3, v2, Landroid/telephony/SmsMessage$SubmitPdu;->encodedMessage:[B

    iget-object v4, v2, Landroid/telephony/SmsMessage$SubmitPdu;->encodedMessage:[B

    array-length v4, v4

    invoke-virtual {v0, v3, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3
.end method
