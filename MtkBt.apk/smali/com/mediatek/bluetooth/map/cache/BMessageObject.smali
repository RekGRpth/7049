.class public Lcom/mediatek/bluetooth/map/cache/BMessageObject;
.super Ljava/lang/Object;
.source "BMessageObject.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mCharset:I

.field private mContentSize:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mEncoding:I

.field private mFile:Ljava/io/File;

.field private mFileName:Ljava/lang/String;

.field private mFolderPath:Ljava/lang/String;

.field private mLanguage:I

.field private mMsgType:I

.field private mName:Ljava/lang/String;

.field private mOrignator:Ljava/lang/String;

.field private mOrignatorSize:I

.field private mPartId:I

.field private mReadStatus:I

.field private mRecipient:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRecipientSize:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mRetry:Z

.field private mTransparent:Z

.field private mVersion:I

.field private mWholeSize:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v2, "BMessageObject"

    iput-object v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->TAG:Ljava/lang/String;

    iput v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mVersion:I

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContext:Landroid/content/Context;

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-virtual {v2, p2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p2}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFileName:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->initCache()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file path"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    const-string v2, "fail to create file"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v3, "BMessageObject"

    iput-object v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->TAG:Ljava/lang/String;

    const/4 v3, 0x1

    iput v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mVersion:I

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    const-string v3, "create dir"

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V

    :cond_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFileName:Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    move-result v2

    if-nez v2, :cond_2

    const-string v3, "the file exists"

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->initCache()V

    return-void

    :cond_2
    :try_start_1
    const-string v3, "create file succeed"

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_3
    :try_start_2
    iget-object v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private initCache()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mRecipient:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mRecipientSize:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContentSize:Ljava/util/ArrayList;

    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "BMessageObject"

    invoke-static {v0, p1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public addContent([B)Z
    .locals 7
    .param p1    # [B

    const/4 v2, 0x1

    if-eqz p1, :cond_0

    array-length v3, p1

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContext:Landroid/content/Context;

    if-nez v3, :cond_2

    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    :goto_1
    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContentSize:Ljava/util/ArrayList;

    array-length v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-wide v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mWholeSize:J

    array-length v5, p1

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mWholeSize:J
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFileName:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mName:Ljava/lang/String;

    const v5, 0x8001

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    goto :goto_1

    :cond_3
    const-string v3, "fail to get content"

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v2, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public addRecipient(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x1

    if-nez p1, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mRecipientSize:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mRecipient:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getCharset()I
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mCharset:I

    return v0
.end method

.method public getContent([B)Z
    .locals 4
    .param p1    # [B

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    new-instance v1, Ljava/io/FileInputStream;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    :goto_0
    invoke-virtual {v1, p1}, Ljava/io/InputStream;->read([B)I

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    :goto_1
    const/4 v2, 0x1

    :goto_2
    return v2

    :cond_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFileName:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1

    goto :goto_0

    :cond_1
    const-string v2, "fail to get content"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v2, "fail to find file"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getContentSize(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContentSize:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContentSize:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getContentSize()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mWholeSize:J

    return-wide v0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    return-object v0
.end method

.method public getFinalRecipient()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mRecipient:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mRecipient:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mRecipient:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFolder()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFolderPath:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFolderPath:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFolderPath:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFolderPath:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFolderPath:Ljava/lang/String;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getMessageType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mMsgType:I

    return v0
.end method

.method public getOrignator()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mOrignator:Ljava/lang/String;

    return-object v0
.end method

.method public getReadStatus()I
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mReadStatus:I

    return v0
.end method

.method public getRecipient()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mRecipient:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isRetry()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mRetry:Z

    return v0
.end method

.method public isTransparent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mTransparent:Z

    return v0
.end method

.method public releaseResource()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public reset()V
    .locals 5

    const/4 v4, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x0

    iput v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mReadStatus:I

    iput v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mMsgType:I

    iput-object v4, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFolderPath:Ljava/lang/String;

    iput v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mOrignatorSize:I

    iput-object v4, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mOrignator:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mRecipientSize:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mRecipient:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iput v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mPartId:I

    iput v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mEncoding:I

    iput v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mCharset:I

    iput v3, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mLanguage:I

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContentSize:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mWholeSize:J

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setCharset(I)Z
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const-string v0, "error, invalid charset"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mCharset:I

    :goto_0
    const/4 v0, 0x1

    return v0

    :pswitch_0
    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mCharset:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setContent([B)Z
    .locals 7
    .param p1    # [B

    const/4 v3, 0x1

    const/4 v2, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContext:Landroid/content/Context;

    if-nez v4, :cond_0

    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFile:Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    :goto_0
    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContentSize:Ljava/util/ArrayList;

    array-length v5, p1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    move v2, v3

    :goto_2
    return v2

    :cond_0
    iget-object v4, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFileName:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mName:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    goto :goto_0

    :cond_1
    const-string v4, "fail to get content"

    invoke-direct {p0, v4}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setContentSize(I)Z
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    iput-wide v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mWholeSize:J

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContentSize:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public setContentSize(Ljava/io/File;)Z
    .locals 6
    .param p1    # Ljava/io/File;

    const/4 v3, 0x1

    if-nez p1, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Ljava/io/FileInputStream;->available()I

    move-result v1

    int-to-long v4, v1

    iput-wide v4, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mWholeSize:J

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mContentSize:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setEncoding(I)Z
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mEncoding:I

    const/4 v0, 0x1

    return v0
.end method

.method public setFolderPath(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mFolderPath:Ljava/lang/String;

    const/4 v0, 0x1

    return v0
.end method

.method public setLang(I)Z
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mLanguage:I

    const/4 v0, 0x1

    return v0
.end method

.method public setMessageType(I)Z
    .locals 2
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mMsgType:I

    iget v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mMsgType:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "error, invalid message type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mMsgType:I

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setOrignator(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mOrignator:Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mOrignatorSize:I

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mOrignatorSize:I

    goto :goto_0
.end method

.method public setPartId(I)Z
    .locals 1
    .param p1    # I

    if-ltz p1, :cond_0

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mPartId:I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setReadStatus(I)V
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "error, invalid read status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->log(Ljava/lang/String;)V

    const/4 v0, 0x2

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mReadStatus:I

    :goto_0
    return-void

    :pswitch_0
    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/BMessageObject;->mReadStatus:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
