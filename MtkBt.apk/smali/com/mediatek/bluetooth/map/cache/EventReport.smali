.class public Lcom/mediatek/bluetooth/map/cache/EventReport;
.super Ljava/lang/Object;
.source "EventReport.java"


# instance fields
.field private mEventType:I

.field private mFolder:Ljava/lang/String;

.field private mHandle:J

.field private mMasId:I

.field private mMsgType:I

.field private mOldFolder:Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mMasId:I

    return-void
.end method


# virtual methods
.method public notifyDeliverResult(JILjava/lang/String;I)Z
    .locals 3
    .param p1    # J
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # I

    const/4 v0, 0x1

    if-eqz p4, :cond_0

    invoke-static {p3}, Lcom/mediatek/bluetooth/map/MAP;->isMessageTypeValid(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    if-ne p5, v0, :cond_2

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mEventType:I

    :goto_1
    invoke-static {p3, p1, p2}, Lcom/mediatek/bluetooth/map/util/HandleUtil;->getHandle(IJ)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mHandle:J

    iput p3, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mMsgType:I

    iput-object p4, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mFolder:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mEventType:I

    goto :goto_1
.end method

.method public notifyMemoryStatus(I)Z
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    const/4 v0, 0x5

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mEventType:I

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x6

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mEventType:I

    goto :goto_0
.end method

.method public notifyMessageDeleted(JILjava/lang/String;)Z
    .locals 2
    .param p1    # J
    .param p3    # I
    .param p4    # Ljava/lang/String;

    invoke-static {p3}, Lcom/mediatek/bluetooth/map/MAP;->isMessageTypeValid(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x7

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mEventType:I

    invoke-static {p3, p1, p2}, Lcom/mediatek/bluetooth/map/util/HandleUtil;->getHandle(IJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mHandle:J

    iput p3, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mMsgType:I

    iput-object p4, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mFolder:Ljava/lang/String;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public notifyMessageShifted(JILjava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1    # J
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    invoke-static {p3}, Lcom/mediatek/bluetooth/map/MAP;->isMessageTypeValid(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x8

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mEventType:I

    iput p3, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mMsgType:I

    iput-object p5, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mFolder:Ljava/lang/String;

    iput-object p4, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mOldFolder:Ljava/lang/String;

    invoke-static {p3, p1, p2}, Lcom/mediatek/bluetooth/map/util/HandleUtil;->getHandle(IJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mHandle:J

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public notifyNewMessageEvent(JILjava/lang/String;)Z
    .locals 2
    .param p1    # J
    .param p3    # I
    .param p4    # Ljava/lang/String;

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    invoke-static {p3}, Lcom/mediatek/bluetooth/map/MAP;->isMessageTypeValid(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mEventType:I

    invoke-static {p3, p1, p2}, Lcom/mediatek/bluetooth/map/util/HandleUtil;->getHandle(IJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mHandle:J

    iput p3, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mMsgType:I

    iput-object p4, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mFolder:Ljava/lang/String;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public notifySendResult(JILjava/lang/String;I)Z
    .locals 3
    .param p1    # J
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # I

    const/4 v0, 0x1

    if-eqz p4, :cond_0

    invoke-static {p3}, Lcom/mediatek/bluetooth/map/MAP;->isMessageTypeValid(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    if-ne p5, v0, :cond_2

    const/4 v1, 0x3

    iput v1, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mEventType:I

    :goto_1
    invoke-static {p3, p1, p2}, Lcom/mediatek/bluetooth/map/util/HandleUtil;->getHandle(IJ)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mHandle:J

    iput p3, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mMsgType:I

    iput-object p4, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mFolder:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const/4 v1, 0x4

    iput v1, p0, Lcom/mediatek/bluetooth/map/cache/EventReport;->mEventType:I

    goto :goto_1
.end method
