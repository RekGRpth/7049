.class public Lcom/mediatek/bluetooth/map/cache/FolderListObject;
.super Ljava/lang/Object;
.source "FolderListObject.java"


# instance fields
.field private mName:Ljava/lang/String;

.field private mSize:I

.field private mTime:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/mediatek/bluetooth/map/util/UtcUtil;->getCurrentTime()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/cache/FolderListObject;->mTime:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/cache/FolderListObject;->mName:Ljava/lang/String;

    return-void
.end method

.method public setSize(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/FolderListObject;->mSize:I

    return-void
.end method
