.class public Lcom/mediatek/bluetooth/map/cache/FolderListRequest;
.super Ljava/lang/Object;
.source "FolderListRequest.java"


# instance fields
.field private isOccupied:Z

.field private mAddr:Ljava/lang/String;

.field private mMasId:I

.field private mOffset:I

.field private mSize:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;->isOccupied:Z

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;->isOccupied:Z

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;->mMasId:I

    return-void
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;->mAddr:Ljava/lang/String;

    return-object v0
.end method

.method public getOffset()I
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;->mOffset:I

    return v0
.end method

.method public getSize()I
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;->mSize:I

    return v0
.end method

.method public isOccupied(Ljava/lang/Boolean;)Z
    .locals 1
    .param p1    # Ljava/lang/Boolean;

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;->isOccupied:Z

    return v0
.end method

.method public declared-synchronized setOccupied(Ljava/lang/Boolean;)Z
    .locals 1
    .param p1    # Ljava/lang/Boolean;

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;->isOccupied:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;->isOccupied:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    const/4 v0, 0x1

    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/map/cache/FolderListRequest;->isOccupied:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
