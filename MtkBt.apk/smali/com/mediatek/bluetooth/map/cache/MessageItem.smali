.class public Lcom/mediatek/bluetooth/map/cache/MessageItem;
.super Ljava/lang/Object;
.source "MessageItem.java"


# instance fields
.field private AttachSize:I

.field private DateTime:Ljava/lang/String;

.field private MsgHandle:J

.field private MsgType:I

.field private OrignalMsgSize:I

.field private RecipientAddr:Ljava/lang/String;

.field private RecipientName:Ljava/lang/String;

.field private RecipientStatus:I

.field private ReplyToAddr:Ljava/lang/String;

.field private SenderAddr:Ljava/lang/String;

.field private SenderName:Ljava/lang/String;

.field private Subject:Ljava/lang/String;

.field private bPriority:Z

.field private bProtected:Z

.field private bSent:Z

.field private bText:Z

.field private mMaxSubjectLength:I

.field private read:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->bSent:Z

    const/16 v0, 0x100

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->mMaxSubjectLength:I

    return-void
.end method


# virtual methods
.method public resetSubjectLength()V
    .locals 1

    const/16 v0, 0x100

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->mMaxSubjectLength:I

    return-void
.end method

.method public declared-synchronized set(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZIIIZ)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # I
    .param p10    # I
    .param p11    # Z
    .param p12    # I
    .param p13    # I
    .param p14    # I
    .param p15    # Z

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setSubject(Ljava/lang/String;)V

    invoke-virtual {p0, p2, p3}, Lcom/mediatek/bluetooth/map/cache/MessageItem;->setDatetime(J)V

    iput-object p4, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->SenderAddr:Ljava/lang/String;

    iput-object p5, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->SenderName:Ljava/lang/String;

    iput-object p6, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->ReplyToAddr:Ljava/lang/String;

    iput-object p7, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->RecipientName:Ljava/lang/String;

    iput-object p8, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->RecipientAddr:Ljava/lang/String;

    iput p12, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->RecipientStatus:I

    iput p9, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->MsgType:I

    iput p10, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->OrignalMsgSize:I

    iput-boolean p11, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->bText:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->bPriority:Z

    move/from16 v0, p14

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->read:I

    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->bProtected:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public setAttachSize(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->AttachSize:I

    return-void
.end method

.method public setDatetime(J)V
    .locals 1
    .param p1    # J

    invoke-static {p1, p2}, Lcom/mediatek/bluetooth/map/util/UtcUtil;->convertMillisToUtc(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->DateTime:Ljava/lang/String;

    return-void
.end method

.method public setHandle(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->MsgHandle:J

    return-void
.end method

.method public setMsgType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->MsgType:I

    return-void
.end method

.method public setPriority(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->bPriority:Z

    return-void
.end method

.method public setProtected(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->bProtected:Z

    return-void
.end method

.method public setReadStatus(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->read:I

    return-void
.end method

.method public setRecipientAddr(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->RecipientAddr:Ljava/lang/String;

    return-void
.end method

.method public setRecipientName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->RecipientName:Ljava/lang/String;

    return-void
.end method

.method public setRecipientStatus(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->RecipientStatus:I

    return-void
.end method

.method public setReplyAddr(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->ReplyToAddr:Ljava/lang/String;

    return-void
.end method

.method public setSenderAddr(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->SenderAddr:Ljava/lang/String;

    return-void
.end method

.method public setSenderName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->SenderName:Ljava/lang/String;

    return-void
.end method

.method public setSize(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->OrignalMsgSize:I

    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->mMaxSubjectLength:I

    if-le v0, v1, :cond_1

    const/4 v0, 0x0

    iget v1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->mMaxSubjectLength:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->Subject:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iput-object p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->Subject:Ljava/lang/String;

    goto :goto_0
.end method

.method public setSubjectLength(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->mMaxSubjectLength:I

    return-void
.end method

.method public setText(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageItem;->bText:Z

    return-void
.end method
