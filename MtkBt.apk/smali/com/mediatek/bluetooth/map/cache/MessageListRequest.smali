.class public Lcom/mediatek/bluetooth/map/cache/MessageListRequest;
.super Ljava/lang/Object;
.source "MessageListRequest.java"


# instance fields
.field private isOccupied:Z

.field private mAddr:Ljava/lang/String;

.field private mChildFolder:Ljava/lang/String;

.field private mEndTime:Ljava/lang/String;

.field private mFolder:Ljava/lang/String;

.field private mListOffset:I

.field private mListSize:I

.field private mMasId:I

.field private mMask:I

.field private mMaxSubjectLen:I

.field private mOrignator:Ljava/lang/String;

.field private mPriority:I

.field private mReadStatus:I

.field private mRecipient:Ljava/lang/String;

.field private mStartTime:Ljava/lang/String;

.field private mType:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->isOccupied:Z

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mMasId:I

    return-void
.end method


# virtual methods
.method public declineListOffset(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mListOffset:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mListOffset:I

    iget v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mListOffset:I

    if-gez v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mListOffset:I

    :cond_0
    return-void
.end method

.method public geMask()I
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mMask:I

    return v0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mAddr:Ljava/lang/String;

    return-object v0
.end method

.method public getEndTime()J
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mEndTime:Ljava/lang/String;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/util/UtcUtil;->revertUtcToMillis(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getFolder()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mFolder:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mFolder:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mFolder:Ljava/lang/String;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getListOffset()I
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mListOffset:I

    return v0
.end method

.method public getListSize()I
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mListSize:I

    return v0
.end method

.method public getMaxSubjectLen()I
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mListSize:I

    if-nez v0, :cond_0

    const/16 v0, 0x100

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mMaxSubjectLen:I

    :cond_0
    iget v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mMaxSubjectLen:I

    return v0
.end method

.method public getMessageType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mType:I

    return v0
.end method

.method public getOrignator()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mOrignator:Ljava/lang/String;

    return-object v0
.end method

.method public getReadStatus()I
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mReadStatus:I

    return v0
.end method

.method public getRecipient()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mRecipient:Ljava/lang/String;

    return-object v0
.end method

.method public getStartTime()J
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mStartTime:Ljava/lang/String;

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/util/UtcUtil;->revertUtcToMillis(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public isOccupied()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->isOccupied:Z

    return v0
.end method

.method public setDefaultMask()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mMask:I

    return-void
.end method

.method public setMask(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mMask:I

    return-void
.end method

.method public setMessageType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mType:I

    return-void
.end method

.method public declared-synchronized setOccupied(Z)Z
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->isOccupied:Z

    if-nez v0, :cond_0

    iput-boolean p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->isOccupied:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    const/4 v0, 0x1

    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    if-nez p1, :cond_1

    :try_start_1
    iput-boolean p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->isOccupied:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setSize(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/map/cache/MessageListRequest;->mListSize:I

    return-void
.end method
