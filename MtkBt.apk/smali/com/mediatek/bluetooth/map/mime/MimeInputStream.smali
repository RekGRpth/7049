.class public Lcom/mediatek/bluetooth/map/mime/MimeInputStream;
.super Ljava/lang/Object;
.source "MimeInputStream.java"


# static fields
.field private static final LINE_BREAK:Ljava/lang/String; = "\r\n"

.field public static final MAXIUM_LINE_LENGTH:I = 0x3e6

.field private static final TAG:Ljava/lang/String; = "MimeInputStream"


# instance fields
.field private currentPosition:I

.field private mCurrentLine:[B

.field private mInput:Ljava/io/PushbackInputStream;


# direct methods
.method constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    new-instance v0, Ljava/io/PushbackInputStream;

    invoke-direct {v0, p1}, Ljava/io/PushbackInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->mInput:Ljava/io/PushbackInputStream;

    const/16 v0, 0x3e6

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->mCurrentLine:[B

    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "MimeInputStream"

    invoke-static {v0, p1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method


# virtual methods
.method public readByteWithoutMark()I
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->mInput:Ljava/io/PushbackInputStream;

    invoke-virtual {v1}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->mInput:Ljava/io/PushbackInputStream;

    invoke-virtual {v1, v0}, Ljava/io/PushbackInputStream;->unread(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public readLine([B)I
    .locals 7
    .param p1    # [B

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x0

    const-string v5, "readLine()"

    invoke-direct {p0, v5}, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->log(Ljava/lang/String;)V

    iput v4, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    if-nez p1, :cond_0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->mInput:Ljava/io/PushbackInputStream;

    invoke-virtual {v5}, Ljava/io/PushbackInputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v5

    int-to-byte v2, v5

    move v0, v2

    :goto_1
    if-eq v0, v4, :cond_1

    iget v5, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    const/16 v6, 0x3e6

    if-ge v5, v6, :cond_1

    iget-object v5, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->mCurrentLine:[B

    iget v6, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    aput-byte v0, v5, v6

    const/16 v5, 0xd

    if-ne v2, v5, :cond_3

    const/16 v5, 0xa

    if-ne v0, v5, :cond_3

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->mCurrentLine:[B

    iget v5, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    add-int/lit8 v6, v5, -0x1

    iput v6, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    aput-byte v3, v4, v5

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->mCurrentLine:[B

    iget v5, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    aput-byte v3, v4, v5

    :cond_1
    iget v4, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    if-lez v4, :cond_2

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->mCurrentLine:[B

    iget v5, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    invoke-static {v4, v3, p1, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v4, Ljava/lang/String;

    iget v5, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    invoke-direct {v4, p1, v3, v5}, Ljava/lang/String;-><init>([BII)V

    invoke-direct {p0, v4}, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->log(Ljava/lang/String;)V

    :cond_2
    iget v3, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    goto :goto_0

    :cond_3
    move v2, v0

    :try_start_1
    iget-object v5, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->mInput:Ljava/io/PushbackInputStream;

    invoke-virtual {v5}, Ljava/io/PushbackInputStream;->read()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v5

    int-to-byte v0, v5

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v3, "fail to read byte"

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->log(Ljava/lang/String;)V

    move v3, v4

    goto :goto_0

    :catch_1
    move-exception v5

    goto :goto_1
.end method

.method public transferData(Ljava/io/OutputStream;II)Z
    .locals 6
    .param p1    # Ljava/io/OutputStream;
    .param p2    # I
    .param p3    # I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "transferData():offset is"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", length is"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->log(Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v1, -0x1

    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    :cond_0
    const/4 v2, 0x0

    move v4, v2

    :goto_0
    return v4

    :cond_1
    add-int/lit8 v1, v1, 0x1

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->mInput:Ljava/io/PushbackInputStream;

    invoke-virtual {v4}, Ljava/io/PushbackInputStream;->read()I

    move-result v3

    if-ge v1, p2, :cond_3

    :goto_1
    if-lt v1, p3, :cond_1

    :cond_2
    :goto_2
    move v4, v2

    goto :goto_0

    :cond_3
    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->log(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public unreadLine()V
    .locals 6

    const/4 v5, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unreadLine():currentPosition is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->log(Ljava/lang/String;)V

    iget v1, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    if-gtz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->mInput:Ljava/io/PushbackInputStream;

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->mCurrentLine:[B

    const/4 v3, 0x0

    iget v4, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/PushbackInputStream;->unread([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iput v5, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->currentPosition:I

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public unreadLine([BII)V
    .locals 3
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unreadLine()offset:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", len:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->log(Ljava/lang/String;)V

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->mInput:Ljava/io/PushbackInputStream;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/PushbackInputStream;->unread([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MimeInputStream;->log(Ljava/lang/String;)V

    goto :goto_0
.end method
