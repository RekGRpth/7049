.class public interface abstract Lcom/mediatek/bluetooth/map/mime/MimeListener;
.super Ljava/lang/Object;
.source "MimeListener.java"


# virtual methods
.method public abstract onBodyEnd()V
.end method

.method public abstract onBodyFail()V
.end method

.method public abstract onBodyStart()V
.end method

.method public abstract onContentAdd([B)V
.end method

.method public abstract onHeaderEnd()V
.end method

.method public abstract onHeaderFail()V
.end method

.method public abstract onHeaderFieldAdd(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onHeaderStart()V
.end method

.method public abstract onMessageEnd()V
.end method

.method public abstract onMessageStart()V
.end method

.method public abstract onRequestBoundary()Ljava/lang/String;
.end method

.method public abstract onRequestSize()I
.end method
