.class public Lcom/mediatek/bluetooth/map/mime/MimeParser;
.super Ljava/lang/Object;
.source "MimeParser.java"

# interfaces
.implements Lcom/mediatek/bluetooth/map/mime/MimeListener;


# static fields
.field private static final DATE_FORMAT:Ljava/text/SimpleDateFormat;

.field private static final TAG:Ljava/lang/String; = "MimeParser"


# instance fields
.field private attachList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private bodyCache:Ljava/lang/StringBuilder;

.field private isMessageHeaderEnd:Z

.field private mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

.field private mBody:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeBody;

.field private mBoundary:Ljava/lang/String;

.field private mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

.field private mMime:Lcom/mediatek/bluetooth/map/mime/MimeBase;

.field private mSeparator:Lcom/mediatek/bluetooth/map/mime/MimeInputSeparator;

.field private mSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE, dd MMM yyyy HH:mm:ss Z"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->DATE_FORMAT:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/mediatek/bluetooth/map/mime/MimeBase;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .param p2    # Lcom/mediatek/bluetooth/map/mime/MimeBase;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mSize:I

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->isMessageHeaderEnd:Z

    new-instance v0, Lcom/mediatek/bluetooth/map/mime/MimeInputSeparator;

    invoke-direct {v0, p1, p0}, Lcom/mediatek/bluetooth/map/mime/MimeInputSeparator;-><init>(Ljava/io/InputStream;Lcom/mediatek/bluetooth/map/mime/MimeListener;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mSeparator:Lcom/mediatek/bluetooth/map/mime/MimeInputSeparator;

    iput-object p2, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mMime:Lcom/mediatek/bluetooth/map/mime/MimeBase;

    invoke-virtual {p2}, Lcom/mediatek/bluetooth/map/mime/MimeBase;->getHeader()Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p2}, Lcom/mediatek/bluetooth/map/mime/MimeBase;->getBody()Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeBody;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mBody:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeBody;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->attachList:Ljava/util/ArrayList;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->bodyCache:Ljava/lang/StringBuilder;

    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "MimeParser"

    invoke-static {v0, p1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private processAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/mediatek/bluetooth/map/Address;->getFormatAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private processContentDisposition(Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v8, -0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    iget-object v6, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v6, ";"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x0

    aget-object v6, v1, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    :goto_1
    array-length v6, v1

    if-ge v2, v6, :cond_5

    aget-object v6, v1, v2

    const-string v7, "="

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const/4 v4, 0x0

    :goto_2
    if-eqz v5, :cond_4

    array-length v6, v5

    if-ge v4, v6, :cond_4

    aget-object v6, v5, v4

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, "filename"

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-eq v6, v8, :cond_3

    add-int/lit8 v6, v4, 0x1

    aget-object v6, v5, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    :cond_2
    :goto_3
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_3
    aget-object v6, v5, v4

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, "size"

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-eq v6, v8, :cond_2

    add-int/lit8 v6, v4, 0x1

    aget-object v6, v5, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mSize:I

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mSize is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mSize:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    iget-boolean v6, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->isMessageHeaderEnd:Z

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    iput-object v0, v6, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mLocation:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    iput-object v3, v6, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mFileName:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private processContentEncoding(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->isMessageHeaderEnd:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    iput-object p1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mEncoding:Ljava/lang/String;

    goto :goto_0
.end method

.method private processContentID(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->isMessageHeaderEnd:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    iput-object p1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentId:Ljava/lang/String;

    goto :goto_0
.end method

.method private processContentLocation(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->isMessageHeaderEnd:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    iput-object p1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentLocation:Ljava/lang/String;

    goto :goto_0
.end method

.method private processContentType(Ljava/lang/String;)V
    .locals 12
    .param p1    # Ljava/lang/String;

    const/4 v11, -0x1

    const/4 v3, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v2, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v9, ";"

    invoke-virtual {p1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v9, 0x0

    aget-object v3, v4, v9

    const/4 v5, 0x1

    :goto_1
    array-length v9, v4

    if-ge v5, v9, :cond_6

    aget-object v9, v4, v5

    const-string v10, "="

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x0

    :goto_2
    if-eqz v1, :cond_5

    array-length v9, v1

    if-ge v6, v9, :cond_5

    aget-object v9, v1, v6

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    const-string v10, "boundary"

    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-eq v9, v11, :cond_2

    add-int/lit8 v9, v6, 0x1

    aget-object v0, v1, v9

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    const/4 v10, 0x2

    if-le v9, v10, :cond_1

    const/4 v9, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mBoundary:Ljava/lang/String;

    :cond_1
    :goto_3
    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_2
    aget-object v9, v1, v6

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    const-string v10, "name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-eq v9, v11, :cond_3

    add-int/lit8 v9, v6, 0x1

    aget-object v9, v1, v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    :cond_3
    aget-object v9, v1, v6

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    const-string v10, "charset"

    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-eq v9, v11, :cond_4

    add-int/lit8 v9, v6, 0x1

    aget-object v9, v1, v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "unknown type:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v1, v6

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    :cond_6
    iget-boolean v9, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->isMessageHeaderEnd:Z

    if-nez v9, :cond_7

    iget-object v9, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iput-object v3, v9, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mMiltipartType:Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    iget-object v9, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    iput-object v3, v9, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mMimeType:Ljava/lang/String;

    iget-object v9, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    iput-object v7, v9, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mName:Ljava/lang/String;

    iget-object v9, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    iput-object v2, v9, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mCharset:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private processTimeStamp(Ljava/lang/String;)J
    .locals 5
    .param p1    # Ljava/lang/String;

    const-wide/16 v2, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-wide v2

    :cond_0
    :try_start_0
    sget-object v4, Lcom/mediatek/bluetooth/map/mime/MimeParser;->DATE_FORMAT:Ljava/text/SimpleDateFormat;

    invoke-virtual {v4, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v2

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onBodyEnd()V
    .locals 2

    const-string v0, "onBodyEnd"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->attachList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    return-void
.end method

.method public onBodyFail()V
    .locals 0

    return-void
.end method

.method public onBodyStart()V
    .locals 2

    const-string v0, "onBodyStart"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    new-instance v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mMime:Lcom/mediatek/bluetooth/map/mime/MimeBase;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;-><init>(Lcom/mediatek/bluetooth/map/mime/MimeBase;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mSize:I

    return-void
.end method

.method public onContentAdd([B)V
    .locals 2
    .param p1    # [B

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onContentAdd:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mAttachment:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    iput-object p1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentBytes:[B

    return-void

    :cond_0
    array-length v0, p1

    goto :goto_0
.end method

.method public onHeaderEnd()V
    .locals 1

    const-string v0, "onHeaderEnd"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->isMessageHeaderEnd:Z

    return-void
.end method

.method public onHeaderFail()V
    .locals 1

    const-string v0, "onHeaderFail"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    return-void
.end method

.method public onHeaderFieldAdd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onHeaderFieldAdd:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    if-nez p1, :cond_0

    const-string v0, "error, the header name is null"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Date"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->processTimeStamp(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTimeStamp:J

    goto :goto_0

    :cond_1
    const-string v0, "Subject"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iput-object p2, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mSubject:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, "Message-ID"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iput-object p2, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mMsgId:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "From"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->processAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mFrom:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v0, "Cc"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->processAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mCc:Ljava/lang/String;

    goto :goto_0

    :cond_5
    const-string v0, "To"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->processAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "value is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", to is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v1, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    const-string v0, "Bcc"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->processAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mBcc:Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    const-string v0, "Reply-To"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->processAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mReplyTo:Ljava/lang/String;

    goto/16 :goto_0

    :cond_8
    const-string v0, "MIME-Version"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iput-object p2, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mVersion:Ljava/lang/String;

    goto/16 :goto_0

    :cond_9
    const-string v0, "Content-Type"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->processContentType(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    const-string v0, "Content-ID"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->processContentID(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    const-string v0, "Content-Disposition"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->processContentDisposition(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    const-string v0, "Content-Transfer-Encoding"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->processContentEncoding(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    const-string v0, "Content-Location"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->processContentLocation(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unsupported header field:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onHeaderStart()V
    .locals 1

    const-string v0, "onHeaderStart"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    return-void
.end method

.method public onMessageEnd()V
    .locals 3

    const-string v0, "onMessageEnd"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mMime:Lcom/mediatek/bluetooth/map/mime/MimeBase;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->attachList:Ljava/util/ArrayList;

    const/4 v2, 0x0

    new-array v2, v2, [Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    iput-object v0, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mAttachment:[Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    return-void
.end method

.method public onMessageStart()V
    .locals 1

    const-string v0, "onMessageStart"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    return-void
.end method

.method public onRequestBoundary()Ljava/lang/String;
    .locals 1

    const-string v0, "onRequestBoundary"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mBoundary:Ljava/lang/String;

    return-object v0
.end method

.method public onRequestSize()I
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onRequestSize():"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/mime/MimeParser;->log(Ljava/lang/String;)V

    iget v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mSize:I

    return v0
.end method

.method public parse()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeParser;->mSeparator:Lcom/mediatek/bluetooth/map/mime/MimeInputSeparator;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/map/mime/MimeInputSeparator;->separate()V

    return-void
.end method
