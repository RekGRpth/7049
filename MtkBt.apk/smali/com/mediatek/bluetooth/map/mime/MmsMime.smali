.class public Lcom/mediatek/bluetooth/map/mime/MmsMime;
.super Lcom/mediatek/bluetooth/map/mime/MimeBase;
.source "MmsMime.java"


# static fields
.field private static final DEFAULT_DELIVERY_REPORT_MODE:Z = false

.field private static final DEFAULT_EXPIRY_TIME:J = 0x93a80L

.field private static final DEFAULT_READ_REPORT_MODE:Z = false

.field private static final TAG:Ljava/lang/String; = "MmsMime"


# instance fields
.field private mMmsCenter:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1
    .param p1    # Landroid/content/ContentResolver;

    sget v0, Lcom/mediatek/bluetooth/map/mime/MmsMime;->MSG_TYPE_MMS:I

    invoke-direct {p0, p1, v0}, Lcom/mediatek/bluetooth/map/mime/MimeBase;-><init>(Landroid/content/ContentResolver;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Lcom/google/android/mms/pdu/GenericPdu;)V
    .locals 1
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Lcom/google/android/mms/pdu/GenericPdu;

    sget v0, Lcom/mediatek/bluetooth/map/mime/MmsMime;->MSG_TYPE_MMS:I

    invoke-direct {p0, p1, v0}, Lcom/mediatek/bluetooth/map/mime/MimeBase;-><init>(Landroid/content/ContentResolver;I)V

    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadMime(Lcom/google/android/mms/pdu/GenericPdu;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Lcom/google/android/mms/pdu/GenericPdu;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Lcom/google/android/mms/pdu/GenericPdu;
    .param p3    # Ljava/lang/String;

    sget v0, Lcom/mediatek/bluetooth/map/mime/MmsMime;->MSG_TYPE_MMS:I

    invoke-direct {p0, p1, v0}, Lcom/mediatek/bluetooth/map/mime/MimeBase;-><init>(Landroid/content/ContentResolver;I)V

    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadMime(Lcom/google/android/mms/pdu/GenericPdu;)V

    iput-object p3, p0, Lcom/mediatek/bluetooth/map/mime/MmsMime;->mMmsCenter:Ljava/lang/String;

    return-void
.end method

.method private getAddress(Lcom/google/android/mms/pdu/EncodedStringValue;)Ljava/lang/String;
    .locals 3
    .param p1    # Lcom/google/android/mms/pdu/EncodedStringValue;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/mms/pdu/EncodedStringValue;->getString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/Address;->isValidAddress(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/mime/MmsMime;->mMmsCenter:Ljava/lang/String;

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/mime/MmsMime;->mMmsCenter:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get address:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v1, "MMSCenter is null"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getAddress([Lcom/google/android/mms/pdu/EncodedStringValue;)Ljava/lang/String;
    .locals 8
    .param p1    # [Lcom/google/android/mms/pdu/EncodedStringValue;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-nez p1, :cond_0

    const/4 v6, 0x0

    :goto_0
    return-object v6

    :cond_0
    move-object v2, p1

    array-length v4, v2

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v5, v2, v3

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_1

    const-string v6, ";"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v5}, Lcom/google/android/mms/pdu/EncodedStringValue;->getString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get address([]):"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->log(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method private getIntValue([B)I
    .locals 1
    .param p1    # [B

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private getStringValue([B)Ljava/lang/String;
    .locals 1
    .param p1    # [B

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method private loadFromAcknowledgeInd(Lcom/google/android/mms/pdu/AcknowledgeInd;)V
    .locals 0
    .param p1    # Lcom/google/android/mms/pdu/AcknowledgeInd;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromGenericPdu(Lcom/google/android/mms/pdu/GenericPdu;)V

    return-void
.end method

.method private loadFromDeliveryInd(Lcom/google/android/mms/pdu/DeliveryInd;)V
    .locals 3
    .param p1    # Lcom/google/android/mms/pdu/DeliveryInd;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromGenericPdu(Lcom/google/android/mms/pdu/GenericPdu;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/DeliveryInd;->getDate()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTimeStamp:J

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/DeliveryInd;->getMessageId()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->getStringValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mMsgId:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/DeliveryInd;->getTo()[Lcom/google/android/mms/pdu/EncodedStringValue;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->getAddress([Lcom/google/android/mms/pdu/EncodedStringValue;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    return-void
.end method

.method private loadFromGenericPdu(Lcom/google/android/mms/pdu/GenericPdu;)V
    .locals 3
    .param p1    # Lcom/google/android/mms/pdu/GenericPdu;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/GenericPdu;->getFrom()Lcom/google/android/mms/pdu/EncodedStringValue;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->getAddress(Lcom/google/android/mms/pdu/EncodedStringValue;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mFrom:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private loadFromMultimediaMessagePdu(Lcom/google/android/mms/pdu/MultimediaMessagePdu;)V
    .locals 13
    .param p1    # Lcom/google/android/mms/pdu/MultimediaMessagePdu;

    const/4 v8, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v4, "inline"

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromGenericPdu(Lcom/google/android/mms/pdu/GenericPdu;)V

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->getSubject()Lcom/google/android/mms/pdu/EncodedStringValue;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v10, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {v9}, Lcom/google/android/mms/pdu/EncodedStringValue;->getString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mSubject:Ljava/lang/String;

    :cond_0
    iget-object v10, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->getTo()[Lcom/google/android/mms/pdu/EncodedStringValue;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->getAddress([Lcom/google/android/mms/pdu/EncodedStringValue;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    iget-object v10, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->getDate()J

    move-result-wide v11

    iput-wide v11, v10, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTimeStamp:J

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->getBody()Lcom/google/android/mms/pdu/PduBody;

    move-result-object v2

    const/4 v6, 0x0

    :goto_0
    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lcom/google/android/mms/pdu/PduBody;->getPartsNum()I

    move-result v10

    if-ge v6, v10, :cond_9

    invoke-virtual {v2, v6}, Lcom/google/android/mms/pdu/PduBody;->getPart(I)Lcom/google/android/mms/pdu/PduPart;

    move-result-object v8

    new-instance v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;-><init>(Lcom/mediatek/bluetooth/map/mime/MimeBase;)V

    invoke-virtual {v8}, Lcom/google/android/mms/pdu/PduPart;->getContentId()[B

    move-result-object v3

    if-eqz v3, :cond_1

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v3}, Ljava/lang/String;-><init>([B)V

    iput-object v10, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentId:Ljava/lang/String;

    :cond_1
    invoke-virtual {v8}, Lcom/google/android/mms/pdu/PduPart;->getDataUri()Landroid/net/Uri;

    move-result-object v10

    iput-object v10, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentUri:Landroid/net/Uri;

    invoke-virtual {v8}, Lcom/google/android/mms/pdu/PduPart;->getData()[B

    move-result-object v10

    iput-object v10, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentBytes:[B

    invoke-virtual {v8}, Lcom/google/android/mms/pdu/PduPart;->getFilename()[B

    move-result-object v3

    if-eqz v3, :cond_2

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v3}, Ljava/lang/String;-><init>([B)V

    iput-object v10, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mFileName:Ljava/lang/String;

    :cond_2
    invoke-virtual {v8}, Lcom/google/android/mms/pdu/PduPart;->getName()[B

    move-result-object v3

    if-eqz v3, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v3}, Ljava/lang/String;-><init>([B)V

    iput-object v10, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mName:Ljava/lang/String;

    :cond_3
    invoke-virtual {v8}, Lcom/google/android/mms/pdu/PduPart;->getContentDisposition()[B

    move-result-object v10

    if-eqz v10, :cond_4

    new-instance v4, Ljava/lang/String;

    invoke-virtual {v8}, Lcom/google/android/mms/pdu/PduPart;->getContentDisposition()[B

    move-result-object v10

    invoke-direct {v4, v10}, Ljava/lang/String;-><init>([B)V

    :cond_4
    iput-object v4, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mLocation:Ljava/lang/String;

    invoke-virtual {v8}, Lcom/google/android/mms/pdu/PduPart;->getContentLocation()[B

    move-result-object v3

    if-eqz v3, :cond_5

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v3}, Ljava/lang/String;-><init>([B)V

    iput-object v10, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentLocation:Ljava/lang/String;

    :cond_5
    invoke-virtual {v8}, Lcom/google/android/mms/pdu/PduPart;->getContentType()[B

    move-result-object v3

    if-eqz v3, :cond_6

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v3}, Ljava/lang/String;-><init>([B)V

    iput-object v10, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mMimeType:Ljava/lang/String;

    :cond_6
    iget-object v10, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentBytes:[B

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentBytes:[B

    array-length v10, v10

    int-to-long v10, v10

    iput-wide v10, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mSize:J

    :cond_7
    :goto_1
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_8
    iget-object v10, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentUri:Landroid/net/Uri;

    if-eqz v10, :cond_7

    :try_start_0
    iget-object v10, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v11, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentUri:Landroid/net/Uri;

    invoke-virtual {v10, v11}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v7

    instance-of v10, v7, Ljava/io/FileInputStream;

    if-eqz v10, :cond_7

    check-cast v7, Ljava/io/FileInputStream;

    invoke-virtual {v7}, Ljava/io/FileInputStream;->available()I

    move-result v10

    int-to-long v10, v10

    iput-wide v10, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mSize:J
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v5

    invoke-virtual {v5}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->log(Ljava/lang/String;)V

    goto :goto_2

    :catch_1
    move-exception v5

    invoke-virtual {v5}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->log(Ljava/lang/String;)V

    goto :goto_2

    :cond_9
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v10, v10, [Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    iput-object v10, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mAttachment:[Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    return-void
.end method

.method private loadFromNotificationInd(Lcom/google/android/mms/pdu/NotificationInd;)V
    .locals 3
    .param p1    # Lcom/google/android/mms/pdu/NotificationInd;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromGenericPdu(Lcom/google/android/mms/pdu/GenericPdu;)V

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/NotificationInd;->getSubject()Lcom/google/android/mms/pdu/EncodedStringValue;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {v0}, Lcom/google/android/mms/pdu/EncodedStringValue;->getString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mSubject:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private loadFromNotifyRespInd(Lcom/google/android/mms/pdu/NotifyRespInd;)V
    .locals 0
    .param p1    # Lcom/google/android/mms/pdu/NotifyRespInd;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromGenericPdu(Lcom/google/android/mms/pdu/GenericPdu;)V

    return-void
.end method

.method private loadFromReadOrigInd(Lcom/google/android/mms/pdu/ReadOrigInd;)V
    .locals 3
    .param p1    # Lcom/google/android/mms/pdu/ReadOrigInd;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromGenericPdu(Lcom/google/android/mms/pdu/GenericPdu;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/ReadOrigInd;->getDate()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTimeStamp:J

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/ReadOrigInd;->getMessageId()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->getStringValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mMsgId:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/ReadOrigInd;->getTo()[Lcom/google/android/mms/pdu/EncodedStringValue;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->getAddress([Lcom/google/android/mms/pdu/EncodedStringValue;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    return-void
.end method

.method private loadFromReadRecInd(Lcom/google/android/mms/pdu/ReadRecInd;)V
    .locals 3
    .param p1    # Lcom/google/android/mms/pdu/ReadRecInd;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromGenericPdu(Lcom/google/android/mms/pdu/GenericPdu;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/ReadRecInd;->getDate()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTimeStamp:J

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/ReadRecInd;->getMessageId()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->getStringValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mMsgId:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/ReadRecInd;->getTo()[Lcom/google/android/mms/pdu/EncodedStringValue;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->getAddress([Lcom/google/android/mms/pdu/EncodedStringValue;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    return-void
.end method

.method private loadFromRetrieveConf(Lcom/google/android/mms/pdu/RetrieveConf;)V
    .locals 2
    .param p1    # Lcom/google/android/mms/pdu/RetrieveConf;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromMultimediaMessagePdu(Lcom/google/android/mms/pdu/MultimediaMessagePdu;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/RetrieveConf;->getCc()[Lcom/google/android/mms/pdu/EncodedStringValue;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->getAddress([Lcom/google/android/mms/pdu/EncodedStringValue;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mCc:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/RetrieveConf;->getMessageId()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->getStringValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mMsgId:Ljava/lang/String;

    return-void
.end method

.method private loadFromSendConf(Lcom/google/android/mms/pdu/SendConf;)V
    .locals 2
    .param p1    # Lcom/google/android/mms/pdu/SendConf;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromGenericPdu(Lcom/google/android/mms/pdu/GenericPdu;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/SendConf;->getMessageId()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->getStringValue([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mMsgId:Ljava/lang/String;

    return-void
.end method

.method private loadFromSendReq(Lcom/google/android/mms/pdu/SendReq;)V
    .locals 2
    .param p1    # Lcom/google/android/mms/pdu/SendReq;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromMultimediaMessagePdu(Lcom/google/android/mms/pdu/MultimediaMessagePdu;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/SendReq;->getBcc()[Lcom/google/android/mms/pdu/EncodedStringValue;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->getAddress([Lcom/google/android/mms/pdu/EncodedStringValue;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mBcc:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    invoke-virtual {p1}, Lcom/google/android/mms/pdu/SendReq;->getCc()[Lcom/google/android/mms/pdu/EncodedStringValue;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->getAddress([Lcom/google/android/mms/pdu/EncodedStringValue;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mCc:Ljava/lang/String;

    return-void
.end method

.method private loadMime(Lcom/google/android/mms/pdu/GenericPdu;)V
    .locals 3
    .param p1    # Lcom/google/android/mms/pdu/GenericPdu;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/mms/pdu/GenericPdu;->getMessageType()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->log(Ljava/lang/String;)V

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unsupported type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->log(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    check-cast p1, Lcom/google/android/mms/pdu/SendReq;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromSendReq(Lcom/google/android/mms/pdu/SendReq;)V

    goto :goto_0

    :pswitch_1
    check-cast p1, Lcom/google/android/mms/pdu/SendConf;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromSendConf(Lcom/google/android/mms/pdu/SendConf;)V

    goto :goto_0

    :pswitch_2
    check-cast p1, Lcom/google/android/mms/pdu/NotificationInd;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromNotificationInd(Lcom/google/android/mms/pdu/NotificationInd;)V

    goto :goto_0

    :pswitch_3
    check-cast p1, Lcom/google/android/mms/pdu/NotifyRespInd;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromNotifyRespInd(Lcom/google/android/mms/pdu/NotifyRespInd;)V

    goto :goto_0

    :pswitch_4
    check-cast p1, Lcom/google/android/mms/pdu/RetrieveConf;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromRetrieveConf(Lcom/google/android/mms/pdu/RetrieveConf;)V

    goto :goto_0

    :pswitch_5
    check-cast p1, Lcom/google/android/mms/pdu/DeliveryInd;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromDeliveryInd(Lcom/google/android/mms/pdu/DeliveryInd;)V

    goto :goto_0

    :pswitch_6
    check-cast p1, Lcom/google/android/mms/pdu/AcknowledgeInd;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromAcknowledgeInd(Lcom/google/android/mms/pdu/AcknowledgeInd;)V

    goto :goto_0

    :pswitch_7
    check-cast p1, Lcom/google/android/mms/pdu/ReadOrigInd;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromReadOrigInd(Lcom/google/android/mms/pdu/ReadOrigInd;)V

    goto :goto_0

    :pswitch_8
    check-cast p1, Lcom/google/android/mms/pdu/ReadRecInd;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->loadFromReadRecInd(Lcom/google/android/mms/pdu/ReadRecInd;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x80
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "MmsMime"

    invoke-static {v0, p1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private reverseAddress(Ljava/lang/String;)[Lcom/google/android/mms/pdu/EncodedStringValue;
    .locals 7
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_0

    const-string v6, ";"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    array-length v5, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v1, v4

    new-instance v6, Lcom/google/android/mms/pdu/EncodedStringValue;

    invoke-direct {v6, v2}, Lcom/google/android/mms/pdu/EncodedStringValue;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Lcom/google/android/mms/pdu/EncodedStringValue;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lcom/google/android/mms/pdu/EncodedStringValue;

    return-object v6
.end method


# virtual methods
.method public addToField(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v2, v2, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v2, v2, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iput-object p1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    goto :goto_0
.end method

.method public generatePdu(I)Lcom/google/android/mms/pdu/MultimediaMessagePdu;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    sparse-switch p1, :sswitch_data_0

    :goto_0
    return-object v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->generateSendReqPdu()Lcom/google/android/mms/pdu/SendReq;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->generateRetrieveConfPdu()Lcom/google/android/mms/pdu/RetrieveConf;

    move-result-object v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x80 -> :sswitch_0
        0x84 -> :sswitch_1
    .end sparse-switch
.end method

.method public generatePduBody()Lcom/google/android/mms/pdu/PduBody;
    .locals 10

    new-instance v2, Lcom/google/android/mms/pdu/PduBody;

    invoke-direct {v2}, Lcom/google/android/mms/pdu/PduBody;-><init>()V

    iget-object v8, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mAttachment:[Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mAttachment:[Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    array-length v8, v8

    if-nez v8, :cond_2

    :cond_0
    const-string v8, "attachment is null"

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->log(Ljava/lang/String;)V

    const/4 v2, 0x0

    :cond_1
    return-object v2

    :cond_2
    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mAttachment:[Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;

    array-length v6, v0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v1, v0, v5

    new-instance v7, Lcom/google/android/mms/pdu/PduPart;

    invoke-direct {v7}, Lcom/google/android/mms/pdu/PduPart;-><init>()V

    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentBytes:[B

    if-eqz v8, :cond_a

    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentBytes:[B

    array-length v8, v8

    if-lez v8, :cond_a

    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentBytes:[B

    invoke-virtual {v7, v8}, Lcom/google/android/mms/pdu/PduPart;->setData([B)V

    :goto_1
    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentId:Ljava/lang/String;

    if-eqz v8, :cond_3

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mContentId is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->log(Ljava/lang/String;)V

    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentId:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/mms/pdu/PduPart;->setContentId([B)V

    :cond_3
    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mLocation:Ljava/lang/String;

    if-eqz v8, :cond_4

    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mLocation:Ljava/lang/String;

    const-string v9, "inline"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mLocation:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/mms/pdu/PduPart;->setContentDisposition([B)V

    :cond_4
    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentLocation:Ljava/lang/String;

    if-eqz v8, :cond_5

    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentLocation:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/mms/pdu/PduPart;->setContentLocation([B)V

    :cond_5
    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mMimeType:Ljava/lang/String;

    if-eqz v8, :cond_c

    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mMimeType:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/mms/pdu/PduPart;->setContentType([B)V

    :goto_2
    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mCharset:Ljava/lang/String;

    if-eqz v8, :cond_6

    :try_start_0
    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mCharset:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/mms/pdu/CharacterSets;->getMibEnumValue(Ljava/lang/String;)I

    move-result v3

    const/4 v8, -0x1

    if-eq v3, v8, :cond_6

    invoke-virtual {v7, v3}, Lcom/google/android/mms/pdu/PduPart;->setCharset(I)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_6
    :goto_3
    const-string v8, "8bit"

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/mms/pdu/PduPart;->setContentTransferEncoding([B)V

    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mFileName:Ljava/lang/String;

    if-eqz v8, :cond_7

    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mFileName:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/mms/pdu/PduPart;->setFilename([B)V

    :cond_7
    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mLocation:Ljava/lang/String;

    if-nez v8, :cond_8

    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mFileName:Ljava/lang/String;

    if-nez v8, :cond_8

    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentId:Ljava/lang/String;

    if-nez v8, :cond_8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toOctalString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/mms/pdu/PduPart;->setContentLocation([B)V

    :cond_8
    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mName:Ljava/lang/String;

    if-eqz v8, :cond_9

    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mName:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/mms/pdu/PduPart;->setName([B)V

    :cond_9
    invoke-virtual {v2, v7}, Lcom/google/android/mms/pdu/PduBody;->addPart(Lcom/google/android/mms/pdu/PduPart;)Z

    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    :cond_a
    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentUri:Landroid/net/Uri;

    if-eqz v8, :cond_b

    iget-object v8, v1, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeAttachment;->mContentUri:Landroid/net/Uri;

    invoke-virtual {v7, v8}, Lcom/google/android/mms/pdu/PduPart;->setDataUri(Landroid/net/Uri;)V

    goto/16 :goto_1

    :cond_b
    const-string v8, "data is null"

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->log(Ljava/lang/String;)V

    goto :goto_4

    :cond_c
    const-string v8, "text/plain"

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/mms/pdu/PduPart;->setContentType([B)V

    goto :goto_2

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->log(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public generateRetrieveConfPdu()Lcom/google/android/mms/pdu/RetrieveConf;
    .locals 12

    const-string v8, "generateRetrieveConfPdu()"

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->log(Ljava/lang/String;)V

    :try_start_0
    new-instance v6, Lcom/google/android/mms/pdu/RetrieveConf;

    invoke-direct {v6}, Lcom/google/android/mms/pdu/RetrieveConf;-><init>()V

    iget-object v8, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v8, v8, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mMiltipartType:Ljava/lang/String;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v8, v8, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mMiltipartType:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/google/android/mms/pdu/RetrieveConf;->setContentType([B)V

    :goto_0
    const/16 v8, 0x81

    invoke-virtual {v6, v8}, Lcom/google/android/mms/pdu/RetrieveConf;->setDeliveryReport(I)V

    const/16 v8, 0x81

    invoke-virtual {v6, v8}, Lcom/google/android/mms/pdu/RetrieveConf;->setReadReport(I)V

    iget-object v8, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v8, v8, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mFrom:Ljava/lang/String;

    if-eqz v8, :cond_0

    new-instance v8, Lcom/google/android/mms/pdu/EncodedStringValue;

    iget-object v9, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v9, v9, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mFrom:Ljava/lang/String;

    invoke-direct {v8, v9}, Lcom/google/android/mms/pdu/EncodedStringValue;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v8}, Lcom/google/android/mms/pdu/RetrieveConf;->setFrom(Lcom/google/android/mms/pdu/EncodedStringValue;)V

    :cond_0
    iget-object v8, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v8, v8, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v8, v8, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->reverseAddress(Ljava/lang/String;)[Lcom/google/android/mms/pdu/EncodedStringValue;

    move-result-object v7

    move-object v0, v7

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_3

    aget-object v3, v0, v4

    invoke-virtual {v6, v3}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->addTo(Lcom/google/android/mms/pdu/EncodedStringValue;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    const-string v8, "text/plain"

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/google/android/mms/pdu/RetrieveConf;->setContentType([B)V
    :try_end_0
    .catch Lcom/google/android/mms/InvalidHeaderValueException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->log(Ljava/lang/String;)V

    const/4 v6, 0x0

    :cond_2
    :goto_2
    return-object v6

    :cond_3
    :try_start_1
    iget-object v8, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v8, v8, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mSubject:Ljava/lang/String;

    if-eqz v8, :cond_4

    new-instance v8, Lcom/google/android/mms/pdu/EncodedStringValue;

    iget-object v9, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v9, v9, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mSubject:Ljava/lang/String;

    invoke-direct {v8, v9}, Lcom/google/android/mms/pdu/EncodedStringValue;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v8}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->setSubject(Lcom/google/android/mms/pdu/EncodedStringValue;)V

    :cond_4
    iget-object v8, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-wide v8, v8, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTimeStamp:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-wide v8, v8, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTimeStamp:J

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->setDate(J)V

    :goto_3
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->generatePduBody()Lcom/google/android/mms/pdu/PduBody;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v6, v1}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->setBody(Lcom/google/android/mms/pdu/PduBody;)V

    goto :goto_2

    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->setDate(J)V
    :try_end_1
    .catch Lcom/google/android/mms/InvalidHeaderValueException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method

.method public generateSendReqPdu()Lcom/google/android/mms/pdu/SendReq;
    .locals 7

    const-string v3, "generateSendReqPdu()"

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->log(Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/mms/pdu/SendReq;

    invoke-direct {v2}, Lcom/google/android/mms/pdu/SendReq;-><init>()V

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v3, v3, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mFrom:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_0
    const/16 v3, 0x81

    invoke-virtual {v2, v3}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->setPriority(I)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->setDate(J)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v3, v3, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mSubject:Ljava/lang/String;

    if-eqz v3, :cond_1

    new-instance v3, Lcom/google/android/mms/pdu/EncodedStringValue;

    iget-object v4, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v4, v4, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mSubject:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/google/android/mms/pdu/EncodedStringValue;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->setSubject(Lcom/google/android/mms/pdu/EncodedStringValue;)V

    :cond_1
    iget-object v3, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v3, v3, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mMiltipartType:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v3, v3, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mMiltipartType:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/mms/pdu/SendReq;->setContentType([B)V

    :goto_0
    const-wide/32 v3, 0x93a80

    invoke-virtual {v2, v3, v4}, Lcom/google/android/mms/pdu/SendReq;->setExpiry(J)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-wide v3, v3, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mSize:J

    invoke-virtual {v2, v3, v4}, Lcom/google/android/mms/pdu/SendReq;->setMessageSize(J)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v3, v3, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v3, v3, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->reverseAddress(Ljava/lang/String;)[Lcom/google/android/mms/pdu/EncodedStringValue;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/mms/pdu/SendReq;->setTo([Lcom/google/android/mms/pdu/EncodedStringValue;)V

    :cond_2
    const/16 v3, 0x81

    invoke-virtual {v2, v3}, Lcom/google/android/mms/pdu/SendReq;->setDeliveryReport(I)V

    const/16 v3, 0x81

    invoke-virtual {v2, v3}, Lcom/google/android/mms/pdu/SendReq;->setReadReport(I)V
    :try_end_0
    .catch Lcom/google/android/mms/InvalidHeaderValueException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->generatePduBody()Lcom/google/android/mms/pdu/PduBody;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v2, v0}, Lcom/google/android/mms/pdu/MultimediaMessagePdu;->setBody(Lcom/google/android/mms/pdu/PduBody;)V

    :cond_3
    return-object v2

    :cond_4
    :try_start_1
    const-string v3, "text/plain"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/mms/pdu/SendReq;->setContentType([B)V
    :try_end_1
    .catch Lcom/google/android/mms/InvalidHeaderValueException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/map/mime/MmsMime;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public isHeaderComplete()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mFrom:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iget-object v0, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mTo:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFromField(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/bluetooth/map/mime/MimeBase;->mHeaders:Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;

    iput-object p1, v0, Lcom/mediatek/bluetooth/map/mime/MimeBase$MimeHeaders;->mFrom:Ljava/lang/String;

    return-void
.end method
