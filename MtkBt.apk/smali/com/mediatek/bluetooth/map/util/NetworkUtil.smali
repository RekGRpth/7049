.class public Lcom/mediatek/bluetooth/map/util/NetworkUtil;
.super Ljava/lang/Object;
.source "NetworkUtil.java"


# static fields
.field public static MAX_SLOT_NUM:I

.field public static SIM1:I

.field public static SIM2:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->SIM1:I

    const/4 v0, 0x1

    sput v0, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->SIM2:I

    const/4 v0, 0x2

    sput v0, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->MAX_SLOT_NUM:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getGeminiNetworkType(I)I
    .locals 1
    .param p0    # I

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/telephony/TelephonyManager;->getPhoneTypeGemini(I)I

    move-result v0

    return v0
.end method

.method public static getGeminiSmsType(I)I
    .locals 2
    .param p0    # I

    invoke-static {p0}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->getGeminiNetworkType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getNetworkType()I
    .locals 1

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    return v0
.end method

.method public static getPhoneNumber(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    invoke-static {}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->isGeminiSupport()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/telephony/TelephonyManager;->getLine1NumberGemini(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getSimIdBySlotId(Landroid/content/Context;I)J
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const-wide/16 v1, -0x1

    invoke-static {p0, p1}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-wide v1, v0, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    :cond_0
    return-wide v1
.end method

.method public static getSlotBySimId(Landroid/content/Context;J)I
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # J

    invoke-static {p0, p1, p2}, Landroid/provider/Telephony$SIMInfo;->getSlotById(Landroid/content/Context;J)I

    move-result v0

    return v0
.end method

.method public static getSmsType()I
    .locals 2

    invoke-static {}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->getNetworkType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static isGeminiSupport()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static isPhoneRadioReady()Z
    .locals 2

    const/4 v0, 0x0

    :goto_0
    sget v1, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->MAX_SLOT_NUM:I

    if-ge v0, v1, :cond_1

    invoke-static {v0}, Lcom/mediatek/bluetooth/map/util/NetworkUtil;->getGeminiNetworkType(I)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method
