.class Lcom/mediatek/bluetooth/opp/adp/OppManager$LooperThread;
.super Ljava/lang/Thread;
.source "OppManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/opp/adp/OppManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LooperThread"
.end annotation


# instance fields
.field private callback:Landroid/os/Handler$Callback;

.field public mHandler:Landroid/os/Handler;

.field private threadPriority:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILandroid/os/Handler$Callback;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/os/Handler$Callback;

    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    iput p2, p0, Lcom/mediatek/bluetooth/opp/adp/OppManager$LooperThread;->threadPriority:I

    iput-object p3, p0, Lcom/mediatek/bluetooth/opp/adp/OppManager$LooperThread;->callback:Landroid/os/Handler$Callback;

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppManager$LooperThread;->callback:Landroid/os/Handler$Callback;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppManager$LooperThread;->mHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    iget v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppManager$LooperThread;->threadPriority:I

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-static {}, Landroid/os/Looper;->prepare()V

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void
.end method
