.class Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$MessageListener;
.super Ljava/lang/Thread;
.source "OppServiceNative.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MessageListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;


# direct methods
.method public constructor <init>(Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;)V
    .locals 1

    iput-object p1, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$MessageListener;->this$0:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;

    const-string v0, "BtOppMessageListener"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    const-string v0, "BtTask MessageListener thread starting..."

    invoke-static {v0}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->i(Ljava/lang/String;)V

    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$MessageListener;->this$0:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->startListenNative()V

    const-string v0, "BtTask MessageListener thread stopped."

    invoke-static {v0}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->i(Ljava/lang/String;)V

    return-void
.end method

.method public shutdown()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$MessageListener;->this$0:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->stopListenNative()V

    return-void
.end method

.method public startup()V
    .locals 0

    invoke-virtual {p0}, Ljava/lang/Thread;->start()V

    return-void
.end method
