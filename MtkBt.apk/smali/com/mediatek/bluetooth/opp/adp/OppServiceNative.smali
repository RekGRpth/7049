.class public Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;
.super Landroid/app/Service;
.source "OppServiceNative.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;,
        Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$MessageListener;
    }
.end annotation


# static fields
.field private static final NATIVE_LIB:Ljava/lang/String; = "extopp_jni"

.field private static messageListener:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$MessageListener;


# instance fields
.field isAuthorized:Z

.field isListenDisconnect:Z

.field protected isServiceNativeEnabled:Z

.field private mNativeData:I

.field private oppcCurrentTask:Landroid/net/Uri;

.field private oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/mediatek/bluetooth/opp/adp/EventQueue",
            "<",
            "Lcom/mediatek/bluetooth/opp/adp/OppEvent;",
            ">;"
        }
    .end annotation
.end field

.field private oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

.field private oppsCurrentTask:Landroid/net/Uri;

.field private oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/mediatek/bluetooth/opp/adp/EventQueue",
            "<",
            "Lcom/mediatek/bluetooth/opp/adp/OppEvent;",
            ">;"
        }
    .end annotation
.end field

.field private oppsTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->messageListener:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$MessageListener;

    const-string v0, "extopp_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->classInitNative()V

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/16 v3, 0x4e20

    const/4 v2, 0x4

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    sget-object v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Idle:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    new-instance v0, Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {v0, v3, v2}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;-><init>(II)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    sget-object v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Idle:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    new-instance v0, Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {v0, v3, v2}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;-><init>(II)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isServiceNativeEnabled:Z

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isAuthorized:Z

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isListenDisconnect:Z

    return-void
.end method

.method private checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/bluetooth/opp/adp/EventQueue",
            "<",
            "Lcom/mediatek/bluetooth/opp/adp/OppEvent;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->size()I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "queue size["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->getPrintableString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected static native classInitNative()V
.end method

.method private jniCallback(I[Ljava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # [Ljava/lang/String;

    new-instance v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    invoke-direct {v0, p1, p2}, Lcom/mediatek/bluetooth/opp/adp/OppEvent;-><init>(I[Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "jni cb event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->d(Ljava/lang/String;)V

    if-lez p1, :cond_0

    const/16 v1, 0x1e

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v1, v0}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->notifyNewEvent(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0x64

    if-ge v1, p1, :cond_1

    const/16 v1, 0x82

    if-ge p1, v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v1, v0}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->notifyNewEvent(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid jni cb event["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private oppcFinalEvent(Lcom/mediatek/bluetooth/share/BluetoothShareTask;Z)Z
    .locals 2
    .param p1    # Lcom/mediatek/bluetooth/share/BluetoothShareTask;
    .param p2    # Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    sget-object v1, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Running:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    :cond_0
    :goto_0
    sget-object v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Idle:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    return p2

    :cond_1
    if-nez p2, :cond_0

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    goto :goto_0
.end method

.method private oppsFinalEvent(Lcom/mediatek/bluetooth/share/BluetoothShareTask;Z)Z
    .locals 2
    .param p1    # Lcom/mediatek/bluetooth/share/BluetoothShareTask;
    .param p2    # Z

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    :cond_0
    :goto_0
    sget-object v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Idle:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    return p2

    :cond_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    sget-object v1, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Running:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    if-eq v0, v1, :cond_2

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    goto :goto_0

    :cond_2
    if-nez p2, :cond_0

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    goto :goto_0
.end method


# virtual methods
.method protected native disableServiceNative()V
.end method

.method protected native enableServiceNative()Z
.end method

.method protected native objectDeinitNative()V
.end method

.method protected native objectInitNative()V
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const-string v0, "unsupported function: OppServiceNative.onBind()"

    invoke-static {v0}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->objectInitNative()V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->enableServiceNative()Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isServiceNativeEnabled:Z

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isServiceNativeEnabled:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->messageListener:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$MessageListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$MessageListener;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$MessageListener;-><init>(Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;)V

    sput-object v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->messageListener:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$MessageListener;

    sget-object v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->messageListener:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$MessageListener;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$MessageListener;->startup()V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isServiceNativeEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isServiceNativeEnabled:Z

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->disableServiceNative()V

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->objectDeinitNative()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public oppcAbort(Landroid/net/Uri;)Z
    .locals 2
    .param p1    # Landroid/net/Uri;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcCurrentTask:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    sget-object v1, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Aborted:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    if-eq v0, v1, :cond_2

    sget-object v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Aborting:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected native oppcAbortNative()Z
.end method

.method public oppcConnect(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->clear()V

    invoke-virtual {p0, p1}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcConnectNative(Ljava/lang/String;)Z

    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->waitNewEvent()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    if-nez v0, :cond_0

    const-string v3, "oppcConnect get NULL event (no available event and return-threshold is reach)"

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V

    :goto_1
    return v2

    :cond_0
    iget v3, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    iget v3, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    const/16 v4, 0x10

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v3, "oppcConnect thread interrupted"

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid oppcConnect event: ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected native oppcConnectNative(Ljava/lang/String;)Z
.end method

.method public oppcDisable()Z
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->clear()V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcDisableNative()Z

    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->waitNewEvent()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    if-nez v0, :cond_0

    const-string v3, "oppcDisable get NULL event (no available event and return-threshold is reach)"

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V

    :goto_1
    return v2

    :cond_0
    iget v3, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    iget v3, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v3, "oppcDisable thread interrupted"

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid oppcDisable event: ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected native oppcDisableNative()Z
.end method

.method public oppcDisconnect()Z
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->clear()V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcDisconnectNative()Z

    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->waitNewEvent()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    if-nez v0, :cond_0

    const-string v3, "oppcDisconnect get NULL event (no available event and return-threshold is reach)"

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V

    :goto_1
    return v2

    :cond_0
    iget v3, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    const/16 v4, 0x10

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid oppcDisconnect event: ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "oppcDisconnect thread interrupted"

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected native oppcDisconnectNative()Z
.end method

.method public oppcEnable()Z
    .locals 6

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v4}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    iget-object v4, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->clear()V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEnableNative()Z

    :goto_0
    :try_start_0
    iget-object v4, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v4}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->waitNewEvent()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    if-nez v0, :cond_0

    const-string v3, "oppcEnable get NULL event (no available event and return-threshold is reach)"

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V

    :goto_1
    return v2

    :cond_0
    iget v4, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    if-ne v4, v3, :cond_1

    iget-object v4, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v4}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    move v2, v3

    goto :goto_1

    :cond_1
    iget v4, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v3, "oppcEnable thread interrupted"

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "invalid oppcEnable event: ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected native oppcEnableNative()Z
.end method

.method protected native oppcExchangeObjectNative(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public oppcGetCurrentTask()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcCurrentTask:Landroid/net/Uri;

    return-object v0
.end method

.method protected native oppcPullObjectNative()Z
.end method

.method public oppcPush(Lcom/mediatek/bluetooth/share/BluetoothShareTask;Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;)Z
    .locals 12
    .param p1    # Lcom/mediatek/bluetooth/share/BluetoothShareTask;
    .param p2    # Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v8, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "oppc push object: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getPrintableString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->d(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v8}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->clear()V

    iget-object v8, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    sget-object v9, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Idle:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    if-ne v8, v9, :cond_0

    sget-object v8, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Running:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v8, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    :cond_0
    invoke-virtual {p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getMimeType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getObjectName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getData()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v8, v9, v10}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcPushNative(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    :cond_1
    :goto_0
    iget-object v8, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    sget-object v9, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Aborting:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    if-ne v8, v9, :cond_2

    sget-object v8, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Aborted:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v8, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcAbortNative()Z

    :cond_2
    iget-object v8, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v8}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->waitNewEvent()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    if-nez v1, :cond_3

    const-string v8, "oppcPush get NULL event (no available event and return-threshold is reach)"

    invoke-static {v8}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcDisconnectNative()Z

    goto :goto_0

    :cond_3
    iget v8, v1, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    packed-switch v8, :pswitch_data_0

    :pswitch_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "oppcPushObject invalid event: ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v1, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v8, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    sget-object v9, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Running:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    if-ne v8, v9, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    sub-long v8, v5, v3

    const-wide/16 v10, 0x320

    cmp-long v8, v8, v10

    if-lez v8, :cond_1

    move-wide v3, v5

    iget-object v7, v1, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->parameters:[Ljava/lang/String;

    const/4 v8, 0x4

    invoke-virtual {p1, v8}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    const/4 v8, 0x0

    aget-object v8, v7, v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {p1, v8, v9}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setDoneBytes(J)V

    const/4 v8, 0x1

    aget-object v8, v7, v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {p1, v8, v9}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setTotalBytes(J)V

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getDoneBytes()J

    move-result-wide v8

    const-wide/16 v10, 0x64

    mul-long/2addr v8, v10

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getTotalBytes()J

    move-result-wide v10

    div-long/2addr v8, v10

    long-to-int v0, v8

    if-ge v2, v0, :cond_1

    move v2, v0

    invoke-interface {p2, p1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v8, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    sget-object v9, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Running:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    if-ne v8, v9, :cond_1

    const/4 v8, 0x4

    invoke-virtual {p1, v8}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    const-wide/16 v8, 0x0

    invoke-virtual {p1, v8, v9}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setDoneBytes(J)V

    invoke-interface {p2, p1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    goto/16 :goto_0

    :pswitch_3
    sget-object v8, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Idle:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v8, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    const/16 v8, 0x8

    invoke-virtual {p1, v8}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    invoke-interface {p2, p1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    const/4 v8, 0x1

    :goto_1
    return v8

    :pswitch_4
    sget-object v8, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Idle:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v8, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    const/4 v8, 0x6

    invoke-virtual {p1, v8}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    invoke-interface {p2, p1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    const/4 v8, 0x1

    goto :goto_1

    :pswitch_5
    sget-object v8, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Idle:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v8, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iget-object v7, v1, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->parameters:[Ljava/lang/String;

    const/4 v8, 0x7

    invoke-virtual {p1, v8}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "oppcPushObject - push response: GOEP RSP["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget-object v9, v7, v9

    invoke-static {v9}, Lcom/mediatek/bluetooth/opp/mmi/Utils;->getGoepResponseCodeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    const/4 v8, 0x1

    goto :goto_1

    :pswitch_6
    sget-object v8, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Idle:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v8, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    const/4 v8, 0x7

    invoke-virtual {p1, v8}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    invoke-interface {p2, p1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    const/4 v8, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method protected native oppcPushNative(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public oppcPushObject(Lcom/mediatek/bluetooth/share/BluetoothShareTask;Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;)Z
    .locals 13
    .param p1    # Lcom/mediatek/bluetooth/share/BluetoothShareTask;
    .param p2    # Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v9, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v9}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "oppc push object: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getPrintableString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->d(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v9}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->clear()V

    iget-object v9, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    sget-object v10, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Idle:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    if-ne v9, v10, :cond_0

    sget-object v9, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Running:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v9, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    :cond_0
    invoke-virtual {p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getPeerAddr()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getMimeType()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getObjectName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getData()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v9, v10, v11, v12}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcPushObjectNative(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v8, 0x0

    :cond_1
    :goto_0
    iget-object v9, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    sget-object v10, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Aborting:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    if-ne v9, v10, :cond_2

    sget-object v9, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Aborted:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v9, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iget-object v9, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    new-instance v10, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    const/16 v11, 0x10

    const/4 v12, 0x0

    invoke-direct {v10, v11, v12}, Lcom/mediatek/bluetooth/opp/adp/OppEvent;-><init>(I[Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcDisconnectNative()Z

    :cond_2
    iget-object v9, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v9}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->waitNewEvent()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    if-nez v1, :cond_3

    const-string v9, "oppcPushObject get NULL event (no available event and return-threshold is reach)"

    invoke-static {v9}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcDisconnectNative()Z

    goto :goto_0

    :cond_3
    iget v9, v1, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    packed-switch v9, :pswitch_data_0

    :pswitch_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "oppcPushObject invalid event: ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v1, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v9, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    sget-object v10, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Running:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    if-ne v9, v10, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    sub-long v9, v5, v3

    const-wide/16 v11, 0x320

    cmp-long v9, v9, v11

    if-lez v9, :cond_1

    move-wide v3, v5

    iget-object v7, v1, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->parameters:[Ljava/lang/String;

    const/4 v9, 0x4

    invoke-virtual {p1, v9}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    const/4 v9, 0x0

    aget-object v9, v7, v9

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-virtual {p1, v9, v10}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setDoneBytes(J)V

    const/4 v9, 0x1

    aget-object v9, v7, v9

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-virtual {p1, v9, v10}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setTotalBytes(J)V

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getDoneBytes()J

    move-result-wide v9

    const-wide/16 v11, 0x64

    mul-long/2addr v9, v11

    invoke-virtual {p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getTotalBytes()J

    move-result-wide v11

    div-long/2addr v9, v11

    long-to-int v0, v9

    if-ge v2, v0, :cond_1

    move v2, v0

    invoke-interface {p2, p1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v9, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    sget-object v10, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Running:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    if-ne v9, v10, :cond_1

    const/4 v9, 0x4

    invoke-virtual {p1, v9}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    const-wide/16 v9, 0x0

    invoke-virtual {p1, v9, v10}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setDoneBytes(J)V

    invoke-interface {p2, p1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    goto/16 :goto_0

    :pswitch_3
    sget-object v9, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Running:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v9, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    const/16 v9, 0x8

    invoke-virtual {p1, v9}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    invoke-interface {p2, p1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    const/4 v8, 0x1

    goto/16 :goto_0

    :pswitch_4
    iget-object v9, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    sget-object v10, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Running:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    if-ne v9, v10, :cond_1

    iget-object v7, v1, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->parameters:[Ljava/lang/String;

    const/4 v9, 0x7

    invoke-virtual {p1, v9}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "oppcPushObject - push response: GOEP RSP["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x0

    aget-object v10, v7, v10

    invoke-static {v10}, Lcom/mediatek/bluetooth/opp/mmi/Utils;->getGoepResponseCodeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    const/4 v8, 0x0

    goto/16 :goto_0

    :pswitch_5
    invoke-direct {p0, p1, v8}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcFinalEvent(Lcom/mediatek/bluetooth/share/BluetoothShareTask;Z)Z

    move-result v8

    invoke-interface {p2, p1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    return v8

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method protected native oppcPushObjectNative(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public oppcSetCurrentTask(Landroid/net/Uri;)V
    .locals 0
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppcCurrentTask:Landroid/net/Uri;

    return-void
.end method

.method public oppsAbort(Landroid/net/Uri;)Z
    .locals 2
    .param p1    # Landroid/net/Uri;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsCurrentTask:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    sget-object v1, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Aborted:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    if-eq v0, v1, :cond_2

    sget-object v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Aborting:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->cancelWaitNewEvent()V

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public oppsAccessResponse(Lcom/mediatek/bluetooth/share/BluetoothShareTask;Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;)Z
    .locals 21
    .param p1    # Lcom/mediatek/bluetooth/share/BluetoothShareTask;
    .param p2    # Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getState()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    const/4 v6, 0x0

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getData()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_0

    const-string v5, ""

    :cond_0
    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v13, v0, [Ljava/lang/String;

    const/16 v17, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getTotalBytes()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v13, v17

    const/16 v17, 0x1

    aput-object v5, v13, v17

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "oppsAccessResponse: goep["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "], size["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const/16 v18, 0x0

    aget-object v18, v13, v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "], file["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const/16 v18, 0x1

    aget-object v18, v13, v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "]"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->d(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v13}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsAccessResponseNative(I[Ljava/lang/String;)Z

    if-eqz v6, :cond_2

    sget-object v17, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Idle:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    const/16 v17, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    const/4 v14, 0x1

    :goto_1
    return v14

    :cond_1
    const/16 v6, 0x43

    goto :goto_0

    :cond_2
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isAuthorized:Z

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/4 v14, 0x0

    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    move-object/from16 v17, v0

    sget-object v18, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Aborting:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_4

    sget-object v17, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Aborted:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    move-object/from16 v17, v0

    new-instance v18, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    const/16 v19, 0x70

    const/16 v20, 0x0

    invoke-direct/range {v18 .. v20}, Lcom/mediatek/bluetooth/opp/adp/OppEvent;-><init>(I[Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v18}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsDisconnectNative()Z

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->waitNewEvent()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    if-nez v4, :cond_5

    const-string v17, "oppsAccessResponse get NULL event (be canceled or no available event => return-threshold is reach)"

    invoke-static/range {v17 .. v17}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsDisconnectNative()Z

    goto :goto_2

    :cond_5
    iget v0, v4, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    move/from16 v17, v0

    packed-switch v17, :pswitch_data_0

    :pswitch_0
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "invalid oppsAccessResponse event: ["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    iget v0, v4, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "]"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V

    goto :goto_2

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    move-object/from16 v17, v0

    sget-object v18, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Running:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_3

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    sub-long v17, v10, v8

    const-wide/16 v19, 0x320

    cmp-long v17, v17, v19

    if-lez v17, :cond_3

    move-wide v8, v10

    iget-object v12, v4, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->parameters:[Ljava/lang/String;

    const/16 v17, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    const/16 v17, 0x0

    aget-object v17, v12, v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v17

    move-object/from16 v0, p1

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setDoneBytes(J)V

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getTotalBytes()J

    move-result-wide v15

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getDoneBytes()J

    move-result-wide v17

    cmp-long v17, v17, v15

    if-lez v17, :cond_6

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getDoneBytes()J

    move-result-wide v15

    :cond_6
    const/16 v3, 0x64

    const-wide/16 v17, 0x0

    cmp-long v17, v15, v17

    if-lez v17, :cond_7

    const-wide/16 v17, 0x64

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getDoneBytes()J

    move-result-wide v19

    mul-long v17, v17, v19

    div-long v17, v17, v15

    move-wide/from16 v0, v17

    long-to-int v3, v0

    :cond_7
    if-ge v7, v3, :cond_3

    move v7, v3

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    goto/16 :goto_2

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    move-object/from16 v17, v0

    sget-object v18, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Running:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getState()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    const/16 v17, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    const-wide/16 v17, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setDoneBytes(J)V

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    goto/16 :goto_2

    :pswitch_3
    sget-object v17, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Running:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    const/16 v17, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    const/16 v17, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsFinalEvent(Lcom/mediatek/bluetooth/share/BluetoothShareTask;Z)Z

    move-result v14

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    goto/16 :goto_1

    :pswitch_4
    iget-object v12, v4, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->parameters:[Ljava/lang/String;

    const/16 v17, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->setState(I)V

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "oppsAccessResponse get fail response :GOEP RSP["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const/16 v18, 0x0

    aget-object v18, v12, v18

    invoke-static/range {v18 .. v18}, Lcom/mediatek/bluetooth/opp/mmi/Utils;->getGoepResponseCodeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "]"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsFinalEvent(Lcom/mediatek/bluetooth/share/BluetoothShareTask;Z)Z

    move-result v14

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    goto/16 :goto_1

    :pswitch_5
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "oppsAccessResponse push disconnect - state["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {p1 .. p1}, Lcom/mediatek/bluetooth/share/BluetoothShareTask;->getState()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "]"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->d(Ljava/lang/String;)V

    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isAuthorized:Z

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsFinalEvent(Lcom/mediatek/bluetooth/share/BluetoothShareTask;Z)Z

    move-result v14

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/mediatek/bluetooth/opp/adp/OppTaskHandler;->onObjectChange(Lcom/mediatek/bluetooth/share/BluetoothShareTask;)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x69
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method protected native oppsAccessResponseNative(I[Ljava/lang/String;)Z
.end method

.method public oppsDisable()Z
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->clear()V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsDisableNative()Z

    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->waitNewEvent()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    if-nez v0, :cond_0

    const-string v3, "oppsDisable get NULL event (no available event and return-threshold is reach)"

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V

    :goto_1
    return v2

    :cond_0
    iget v3, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    const/16 v4, 0x67

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    iget v3, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    const/16 v4, 0x68

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v3, "oppsDisable thread interrupted"

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid oppsDisable event: ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected native oppsDisableNative()Z
.end method

.method protected native oppsDisconnectNative()Z
.end method

.method public oppsEnable()Z
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isAuthorized:Z

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->clear()V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEnableNative()Z

    :cond_0
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->waitNewEvent()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    if-nez v0, :cond_1

    const-string v3, "oppsEnable get NULL event (no available event and return-threshold is reach)"

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V

    :goto_1
    return v2

    :cond_1
    iget v3, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    const/16 v4, 0x65

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V

    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    iget v3, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    const/16 v4, 0x66

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->checkQueue(Lcom/mediatek/bluetooth/opp/adp/EventQueue;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v3, "oppsEnable thread interrupted"

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    :try_start_1
    iget v3, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    const/16 v4, 0x67

    if-eq v3, v4, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid oppsEnable event: ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected native oppsEnableNative()Z
.end method

.method public oppsGetCurrentTask()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsCurrentTask:Landroid/net/Uri;

    return-object v0
.end method

.method public oppsIsAuthorized()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isAuthorized:Z

    return v0
.end method

.method public oppsListenDisconnect()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v1, 0x1

    const/4 v2, 0x0

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isListenDisconnect:Z

    iget-object v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v3, v2}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->waitNewEvent(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isListenDisconnect:Z

    if-eqz v0, :cond_0

    iget v3, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    const/16 v4, 0x70

    if-ne v3, v4, :cond_0

    const-string v3, "oppsListenDisconnect(): disconnect event happened!"

    invoke-static {v3}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->d(Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isAuthorized:Z

    :goto_0
    return v1

    :cond_0
    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "oppsListenDisconnect() - unexpected event:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->w(Ljava/lang/String;)V

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public oppsSetCurrentTask(Landroid/net/Uri;)V
    .locals 0
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsCurrentTask:Landroid/net/Uri;

    return-void
.end method

.method public oppsStopListenDisconnect()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "oppsStopListenDisconnect(): is listening=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isListenDisconnect:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->d(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isListenDisconnect:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->cancelWaitNewEvent()V

    :cond_0
    return-void
.end method

.method public oppsWaitForAccessRequest()Lcom/mediatek/bluetooth/opp/adp/OppEvent;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v3, 0x0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsEventQueue:Lcom/mediatek/bluetooth/opp/adp/EventQueue;

    invoke-virtual {v1, v3}, Lcom/mediatek/bluetooth/opp/adp/EventQueue;->waitNewEvent(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;

    if-eqz v0, :cond_0

    iget v1, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid oppsWaitForAccessRequest event: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/mediatek/bluetooth/opp/adp/OppEvent;->event:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/bluetooth/opp/mmi/OppLog;->e(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    sget-object v2, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Idle:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    if-ne v1, v2, :cond_1

    sget-object v1, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;->Running:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    iput-object v1, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->oppsTaskState:Lcom/mediatek/bluetooth/opp/adp/OppServiceNative$TaskState;

    :cond_1
    return-object v0

    :pswitch_1
    iput-boolean v3, p0, Lcom/mediatek/bluetooth/opp/adp/OppServiceNative;->isAuthorized:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x70
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected native startListenNative()V
.end method

.method protected native stopListenNative()V
.end method

.method protected native testJNI(I[Ljava/lang/String;)Z
.end method
