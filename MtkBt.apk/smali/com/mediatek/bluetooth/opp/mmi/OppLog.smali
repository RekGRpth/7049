.class public Lcom/mediatek/bluetooth/opp/mmi/OppLog;
.super Ljava/lang/Object;
.source "OppLog.java"


# static fields
.field private static final DEVELOPMENT:Z = false

.field private static final TAG:Ljava/lang/String; = "Bluetooth.OPP"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static d(Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x3

    const-string v1, "Bluetooth.OPP"

    invoke-static {v0, v1, p0}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static e(Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x6

    const-string v1, "Bluetooth.OPP"

    invoke-static {v0, v1, p0}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static i(Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x4

    const-string v1, "Bluetooth.OPP"

    invoke-static {v0, v1, p0}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static w(Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x5

    const-string v1, "Bluetooth.OPP"

    invoke-static {v0, v1, p0}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    return-void
.end method
