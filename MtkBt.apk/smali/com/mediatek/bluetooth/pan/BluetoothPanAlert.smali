.class public Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;
.super Lcom/android/internal/app/AlertActivity;
.source "BluetoothPanAlert.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final BluetoothNotifyAlert:I = 0x1

.field private static final DEBUG:Z = true

.field private static onlyOnce:Z


# instance fields
.field private final TAG:Ljava/lang/String;

.field action:Ljava/lang/String;

.field deviceAddr:Ljava/lang/String;

.field deviceName:Ljava/lang/String;

.field private mContentView:Landroid/widget/TextView;

.field private mPanAction:Landroid/content/ServiceConnection;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mServerNotify:Lcom/mediatek/bluetooth/pan/IBluetoothPanAction;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->onlyOnce:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    const-string v0, "[BT][PAN][BluetoothPanAlert]"

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->deviceName:Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->deviceAddr:Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->mServerNotify:Lcom/mediatek/bluetooth/pan/IBluetoothPanAction;

    new-instance v0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert$1;-><init>(Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->mPanAction:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert$2;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert$2;-><init>(Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$002(Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;Lcom/mediatek/bluetooth/pan/IBluetoothPanAction;)Lcom/mediatek/bluetooth/pan/IBluetoothPanAction;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;
    .param p1    # Lcom/mediatek/bluetooth/pan/IBluetoothPanAction;

    iput-object p1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->mServerNotify:Lcom/mediatek/bluetooth/pan/IBluetoothPanAction;

    return-object p1
.end method

.method private createView()Landroid/view/View;
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03001c

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    const v2, 0x7f080002

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->mContentView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->mContentView:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v3, "bt_pan_GN_device_authorize"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const v2, 0x7f0600ff

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->deviceName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-object v1

    :cond_2
    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v3, "bt_pan_NAP_device_authorize"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const v2, 0x7f060100

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->deviceName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v3, "bt_pan_GN_device_connected"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const v2, 0x7f060103

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->deviceName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v3, "bt_pan_NAP_device_connected"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f060104

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->deviceName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getDeviceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v4, 0x1

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    sput-boolean v4, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->onlyOnce:Z

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :pswitch_0
    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v2, "bt_pan_GN_device_authorize"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v2, "bt_pan_NAP_device_authorize"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->mServerNotify:Lcom/mediatek/bluetooth/pan/IBluetoothPanAction;

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->deviceAddr:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lcom/mediatek/bluetooth/pan/IBluetoothPanAction;->authorizeRspAction(Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v2, "bt_pan_GN_device_connected"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v2, "bt_pan_NAP_device_connected"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    iget-object v1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->mServerNotify:Lcom/mediatek/bluetooth/pan/IBluetoothPanAction;

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->deviceAddr:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/mediatek/bluetooth/pan/IBluetoothPanAction;->disconnectPanDeviceAction(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const-string v1, "[BT][PAN][BluetoothPanAlert]"

    const-string v2, "onClick:BUTTON_NEGATIVE"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v2, "bt_pan_GN_device_authorize"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v2, "bt_pan_NAP_device_authorize"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_4
    iget-object v1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->mServerNotify:Lcom/mediatek/bluetooth/pan/IBluetoothPanAction;

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->deviceAddr:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/mediatek/bluetooth/pan/IBluetoothPanAction;->authorizeRspAction(Ljava/lang/String;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v2, "[BT][PAN][BluetoothPanAlert]"

    const-string v4, "onCreate"

    invoke-static {v2, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v2, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->onlyOnce:Z

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v2, "device_addr"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->deviceAddr:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v2, "action"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iput-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v2, "[BT][PAN][BluetoothPanAlert]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bluetoothPanAlert "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->deviceAddr:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->deviceAddr:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->deviceAddr:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->getDeviceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->deviceName:Ljava/lang/String;

    :cond_1
    iget-object v1, p0, Lcom/android/internal/app/AlertActivity;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    const v2, 0x108009b

    iput v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mIconId:I

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v3, "bt_pan_GN_device_authorize"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const v2, 0x7f0600fd

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->createView()Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    :cond_3
    const v2, 0x7f060105

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    iput-object p0, v1, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    const v2, 0x7f060106

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    iput-object p0, v1, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    const/4 v2, 0x0

    sput-boolean v2, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->onlyOnce:Z

    invoke-virtual {p0}, Lcom/android/internal/app/AlertActivity;->setupAlert()V

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.bluetooth.pan.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :goto_2
    return-void

    :cond_4
    move-object v2, v3

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v3, "bt_pan_NAP_device_authorize"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const v2, 0x7f0600fe

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v3, "bt_pan_GN_device_connected"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const v2, 0x7f060101

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_7
    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v3, "bt_pan_NAP_device_connected"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const v2, 0x7f060102

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/android/internal/app/AlertActivity;->dismiss()V

    invoke-virtual {p0}, Lcom/android/internal/app/AlertActivity;->cancel()V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "[BT][PAN][BluetoothPanAlert]"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x4

    if-eq p1, v1, :cond_0

    const/4 v1, 0x3

    if-ne p1, v1, :cond_3

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v2, "bt_pan_GN_device_authorize"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->action:Ljava/lang/String;

    const-string v2, "bt_pan_NAP_device_authorize"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->mServerNotify:Lcom/mediatek/bluetooth/pan/IBluetoothPanAction;

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->deviceAddr:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/mediatek/bluetooth/pan/IBluetoothPanAction;->authorizeRspAction(Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    const/4 v1, 0x1

    sput-boolean v1, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->onlyOnce:Z

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_3
    invoke-super {p0, p1, p2}, Lcom/android/internal/app/AlertActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "[BT][PAN][BluetoothPanAlert]"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onStart()V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const-string v1, "[BT][PAN][BluetoothPanAlert]"

    const-string v2, "onStart"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "Bluetooth is not available"

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v1

    const/16 v2, 0xd

    if-eq v1, v2, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/bluetooth/pan/BluetoothPanService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->mPanAction:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v1, v2, v3}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const-string v0, "[BT][PAN][BluetoothPanAlert]"

    const-string v1, "onStop:unbind pan service"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->onlyOnce:Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;->mPanAction:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    return-void
.end method
