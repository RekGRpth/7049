.class public Lcom/mediatek/bluetooth/pan/BluetoothPanService;
.super Landroid/app/Service;
.source "BluetoothPanService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;,
        Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;
    }
.end annotation


# static fields
.field private static final BLUETOOTH_IFACE_ADDR_START:Ljava/lang/String; = "192.168.44.1"

.field private static final BLUETOOTH_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH"

.field private static final BLUETOOTH_PREFIX_LENGTH:I = 0x18

.field private static final DEBUG:Z = true

.field private static final DHCP_DEFAULT_RANGE:[Ljava/lang/String;

.field private static final DNS_DEFAULT_SERVER:[Ljava/lang/String;

.field private static final Message_Display:Ljava/lang/String; = "show toast"

.field private static final Message_Display_ID:I = 0x0

.field private static final PAN_ID_START:I

.field private static final TAG:Ljava/lang/String; = "[BT][PAN][BluetoothPANService]"

.field private static mServerState:Z

.field private static pan_authorize_notify:I


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBluetoothService:Landroid/bluetooth/IBluetooth;

.field private mContext:Landroid/content/Context;

.field private mDhcpRange:[Ljava/lang/String;

.field private mDnsServers:[Ljava/lang/String;

.field private mHasInitiated:Z

.field private mNM:Landroid/app/NotificationManager;

.field private mNativeData:I

.field private mNetworkManagementService:Landroid/os/INetworkManagementService;

.field private final mPan:Landroid/bluetooth/IBluetoothPan$Stub;

.field private final mPanAction:Lcom/mediatek/bluetooth/pan/IBluetoothPanAction$Stub;

.field private mPanDevices:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            "Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mServiceHandler:Landroid/os/Handler;

.field private mSocketListener:Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;

.field private mTetheringOn:Z

.field private mTetheringReceiver:Landroid/content/BroadcastReceiver;

.field private noti:Landroid/app/Notification;

.field notifyMap:Ljava/util/Map;

.field notify_id:I

.field private update_GN_state_intent:Landroid/content/Intent;

.field private update_NAP_state_intent:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "extpan_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/mediatek/bluetooth/BluetoothProfile;->getProfileStart(I)I

    move-result v0

    sput v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->PAN_ID_START:I

    sget v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->PAN_ID_START:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->pan_authorize_notify:I

    sput-boolean v3, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mServerState:Z

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "192.168.44.2"

    aput-object v1, v0, v3

    const-string v1, "192.168.44.254"

    aput-object v1, v0, v4

    const-string v1, "192.168.45.2"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "192.168.45.254"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->DHCP_DEFAULT_RANGE:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "8.8.8.8"

    aput-object v1, v0, v3

    const-string v1, "8.8.4.4"

    aput-object v1, v0, v4

    sput-object v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->DNS_DEFAULT_SERVER:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.bluetooth.profilemanager.action.PROFILE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->update_GN_state_intent:Landroid/content/Intent;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.bluetooth.profilemanager.action.PROFILE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->update_NAP_state_intent:Landroid/content/Intent;

    iput-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mSocketListener:Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    iput-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNM:Landroid/app/NotificationManager;

    iput-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->noti:Landroid/app/Notification;

    iput v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    iput-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mTetheringReceiver:Landroid/content/BroadcastReceiver;

    iput-boolean v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mHasInitiated:Z

    new-instance v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$1;-><init>(Lcom/mediatek/bluetooth/pan/BluetoothPanService;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService$2;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$2;-><init>(Lcom/mediatek/bluetooth/pan/BluetoothPanService;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanAction:Lcom/mediatek/bluetooth/pan/IBluetoothPanAction$Stub;

    new-instance v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService$3;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$3;-><init>(Lcom/mediatek/bluetooth/pan/BluetoothPanService;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPan:Landroid/bluetooth/IBluetoothPan$Stub;

    new-instance v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService$4;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$4;-><init>(Lcom/mediatek/bluetooth/pan/BluetoothPanService;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mServiceHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/pan/BluetoothPanService;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/pan/BluetoothPanService;ILandroid/bluetooth/BluetoothDevice;Z)Landroid/app/Notification;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanService;
    .param p1    # I
    .param p2    # Landroid/bluetooth/BluetoothDevice;
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->genPanNotification(ILandroid/bluetooth/BluetoothDevice;Z)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/mediatek/bluetooth/pan/BluetoothPanService;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/mediatek/bluetooth/pan/BluetoothPanService;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mTetheringReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/mediatek/bluetooth/pan/BluetoothPanService;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanService;
    .param p1    # Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mTetheringReceiver:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/mediatek/bluetooth/pan/BluetoothPanService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanService;

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mTetheringOn:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/mediatek/bluetooth/pan/BluetoothPanService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mTetheringOn:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/mediatek/bluetooth/pan/BluetoothPanService;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/bluetooth/pan/BluetoothPanService;)Landroid/app/NotificationManager;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNM:Landroid/app/NotificationManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/bluetooth/pan/BluetoothPanService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanService;

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mHasInitiated:Z

    return v0
.end method

.method static synthetic access$302(Lcom/mediatek/bluetooth/pan/BluetoothPanService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mHasInitiated:Z

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/bluetooth/pan/BluetoothPanService;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/bluetooth/pan/BluetoothPanService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->listentoSocketNative()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/mediatek/bluetooth/pan/BluetoothPanService;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->wakeupListenerNative()V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/bluetooth/pan/BluetoothPanService;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/pan/BluetoothPanService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->disconnectPanServerDevices()V

    return-void
.end method

.method private callback_pan_handle_activate_cnf(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mServerState:Z

    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->updateProfileState(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->updateProfileState(I)V

    goto :goto_0
.end method

.method private callback_pan_handle_connect_cnf(ZLjava/lang/String;I)V
    .locals 10
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const/4 v7, 0x1

    const/4 v9, 0x0

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5}, Ljava/lang/String;-><init>()V

    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v6, p2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    if-nez v3, :cond_0

    const-string v6, "[BT][PAN][BluetoothPANService]"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "unknow device when callback_pan_handle_connect_cnf:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-static {v3}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$700(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v4

    if-eqz p1, :cond_5

    invoke-static {v3}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v6

    if-nez v6, :cond_4

    const v6, 0x7f060115

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v9

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    :cond_1
    :goto_1
    :try_start_0
    invoke-static {v3}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v6

    invoke-direct {p0, p3, v6}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->enableTethering(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v6, 0x2

    invoke-static {v3, v6}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$702(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;I)I

    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v6, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    sget v7, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->pan_authorize_notify:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, p2, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v6, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->pan_authorize_notify:I

    add-int/lit8 v6, v6, 0x1

    sput v6, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->pan_authorize_notify:I

    :cond_2
    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v6, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    iget v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    invoke-direct {p0, v6, v0, v9}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->genPanNotification(ILandroid/bluetooth/BluetoothDevice;Z)Landroid/app/Notification;

    move-result-object v6

    iput-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->noti:Landroid/app/Notification;

    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNM:Landroid/app/NotificationManager;

    iget v7, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    iget-object v8, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->noti:Landroid/app/Notification;

    invoke-virtual {v6, v7, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    :cond_3
    :goto_2
    invoke-direct {p0, v9, v5}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->sendServiceMsg(ILjava/lang/String;)V

    invoke-static {v3}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$700(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v6

    invoke-static {v3}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v7

    invoke-direct {p0, v4, v6, v0, v7}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->updateSettingsState(IILandroid/bluetooth/BluetoothDevice;I)V

    goto :goto_0

    :cond_4
    invoke-static {v3}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v6

    if-ne v6, v7, :cond_1

    const v6, 0x7f060113

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v9

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v6, "[BT][PAN][BluetoothPANService]"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error enableTethering :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    invoke-static {v3}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v6

    if-nez v6, :cond_6

    const v6, 0x7f060116

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v9

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    :goto_3
    invoke-static {v3, v9}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$702(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;I)I

    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v6, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v6, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNM:Landroid/app/NotificationManager;

    iget v7, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    invoke-virtual {v6, v7}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_2

    :cond_6
    const v6, 0x7f060114

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v9

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_3
.end method

.method private callback_pan_handle_connect_ind(ILjava/lang/String;I)V
    .locals 8
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I

    if-nez p1, :cond_0

    iget-boolean v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mTetheringOn:Z

    if-nez v5, :cond_0

    invoke-virtual {p0, p2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->disconnectPanDevice(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v4, 0x0

    const/4 v1, 0x2

    iget-object v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v5, p2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iget-object v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;

    if-nez v3, :cond_2

    new-instance v3, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;

    invoke-direct {v3, p0, v1, p1}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;-><init>(Lcom/mediatek/bluetooth/pan/BluetoothPanService;II)V

    iget-object v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v5, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    :try_start_0
    invoke-direct {p0, p3, p1}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->enableTethering(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    invoke-direct {p0, v4, v1, v0, p1}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->updateSettingsState(IILandroid/bluetooth/BluetoothDevice;I)V

    iget-object v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v5, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    sget v6, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->pan_authorize_notify:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, p2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v5, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->pan_authorize_notify:I

    add-int/lit8 v5, v5, 0x1

    sput v5, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->pan_authorize_notify:I

    :cond_1
    iget-object v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v5, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    iget v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    const/4 v6, 0x0

    invoke-direct {p0, v5, v0, v6}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->genPanNotification(ILandroid/bluetooth/BluetoothDevice;Z)Landroid/app/Notification;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->noti:Landroid/app/Notification;

    iget-object v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNM:Landroid/app/NotificationManager;

    iget v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    iget-object v7, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->noti:Landroid/app/Notification;

    invoke-virtual {v5, v6, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    :cond_2
    invoke-static {v3}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$700(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v4

    invoke-static {v3, v1}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$702(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;I)I

    invoke-static {v3, p1}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$802(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;I)I

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v5, "[BT][PAN][BluetoothPANService]"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error enableTethering :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private callback_pan_handle_connection_authorize_ind(ILjava/lang/String;)V
    .locals 8
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-nez p1, :cond_0

    iget-boolean v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mTetheringOn:Z

    if-nez v4, :cond_0

    const v4, 0x7f060117

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v6, p2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v7, v3}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->sendServiceMsg(ILjava/lang/String;)V

    invoke-virtual {p0, p2, v7}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->authorizeRsp(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4, p2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;

    if-nez v2, :cond_3

    new-instance v2, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;

    invoke-direct {v2, p0, v1, p1}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;-><init>(Lcom/mediatek/bluetooth/pan/BluetoothPanService;II)V

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v4, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-direct {p0, v7, v1, v0, p1}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->updateSettingsState(IILandroid/bluetooth/BluetoothDevice;I)V

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v4, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    sget v5, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->pan_authorize_notify:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, p2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v4, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->pan_authorize_notify:I

    add-int/lit8 v4, v4, 0x1

    sput v4, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->pan_authorize_notify:I

    :cond_2
    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v4, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    iget v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    invoke-direct {p0, v4, v0, v6}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->genPanNotification(ILandroid/bluetooth/BluetoothDevice;Z)Landroid/app/Notification;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->noti:Landroid/app/Notification;

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNM:Landroid/app/NotificationManager;

    iget v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->noti:Landroid/app/Notification;

    invoke-virtual {v4, v5, v6}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    :cond_3
    invoke-static {v2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$700(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0, p2, v7}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->authorizeRsp(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private callback_pan_handle_deactivate_cnf(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mServerState:Z

    if-eqz p1, :cond_0

    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->updateProfileState(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->updateProfileState(I)V

    goto :goto_0
.end method

.method private callback_pan_handle_disconnect_cnf(ZLjava/lang/String;)V
    .locals 9
    .param p1    # Z
    .param p2    # Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5}, Ljava/lang/String;-><init>()V

    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v6, p2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    if-nez v2, :cond_0

    const-string v6, "[BT][PAN][BluetoothPANService]"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "unknow device when callback_pan_handle_disconnect_cnf:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-static {v2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$700(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v3

    if-eqz p1, :cond_4

    invoke-static {v2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v6

    if-nez v6, :cond_3

    const v6, 0x7f06011a

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v8

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v6, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v6, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    iget-object v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNM:Landroid/app/NotificationManager;

    iget v7, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    invoke-virtual {v6, v7}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_2
    :goto_2
    invoke-direct {p0, v8, v5}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->sendServiceMsg(ILjava/lang/String;)V

    invoke-static {v2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v6

    invoke-direct {p0, v3, v4, v0, v6}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->updateSettingsState(IILandroid/bluetooth/BluetoothDevice;I)V

    goto :goto_0

    :cond_3
    invoke-static {v2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v6

    if-ne v6, v7, :cond_1

    const v6, 0x7f060118

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v8

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->stopNetworkConfig()Z

    goto :goto_1

    :cond_4
    invoke-static {v2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v6

    if-nez v6, :cond_6

    const v6, 0x7f060119

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v8

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    :cond_5
    :goto_3
    const/4 v6, 0x2

    invoke-static {v2, v6}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$702(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;I)I

    invoke-static {v2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$700(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v4

    goto :goto_2

    :cond_6
    invoke-static {v2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v6

    if-ne v6, v7, :cond_5

    const v6, 0x7f06011b

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v8

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_3
.end method

.method private callback_pan_handle_disconnect_ind(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    if-nez v2, :cond_0

    const-string v4, "[BT][PAN][BluetoothPANService]"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unknow device when callback_pan_handle_disconnect_ind:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-static {v2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v4

    if-nez v4, :cond_3

    const v4, 0x7f06011d

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-direct {p0, v6, v3}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->sendServiceMsg(ILjava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    iget-object v4, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNM:Landroid/app/NotificationManager;

    iget v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    invoke-virtual {v4, v5}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_2
    invoke-static {v2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$700(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v4

    invoke-static {v2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v5

    invoke-direct {p0, v4, v6, v0, v5}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->updateSettingsState(IILandroid/bluetooth/BluetoothDevice;I)V

    goto :goto_0

    :cond_3
    invoke-static {v2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v4

    if-ne v4, v5, :cond_1

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->stopNetworkConfig()Z

    const v4, 0x7f06011c

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method private native cleanServiceNative()V
.end method

.method private createNewTetheringAddressLocked(I)Ljava/lang/String;
    .locals 5
    .param p1    # I

    const/4 v4, 0x2

    const-string v1, "192.168.44.1"

    const-string v3, "\\."

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v3, v0, v4

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    add-int/2addr v3, p1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aget-object v3, v0, v4

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private disconnectPanServerDevices()V
    .locals 7

    const-string v5, "[BT][PAN][BluetoothPANService]"

    const-string v6, "disconnect all remote panu devices"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    iget-object v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;

    if-nez v3, :cond_2

    const-string v5, "[BT][PAN][BluetoothPANService]"

    const-string v6, "disconnectPanServerDevices invalid device!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void

    :cond_2
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    iget-object v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNM:Landroid/app/NotificationManager;

    iget v6, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    invoke-virtual {v5, v6}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_3
    invoke-static {v3}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$700(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    invoke-static {v3}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->disconnectPanDevice(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private enableTethering(II)Ljava/lang/String;
    .locals 21
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "updateTetherState:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->printLog(Ljava/lang/String;)V

    const-string v18, "network_management"

    invoke-static/range {v18 .. v18}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const-string v19, "connectivity"

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/ConnectivityManager;

    invoke-virtual {v7}, Landroid/net/ConnectivityManager;->getTetherableBluetoothRegexs()[Ljava/lang/String;

    move-result-object v6

    const/16 v18, 0x0

    aget-object v18, v6, v18

    const-string v19, "\\d"

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v13

    const-string v18, "[BT][PAN][BluetoothPANService]"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "enableTethering interface name: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v9, v0, [Ljava/lang/String;

    :try_start_0
    invoke-interface/range {v17 .. v17}, Landroid/os/INetworkManagementService;->listInterfaces()[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    const/4 v11, 0x0

    move-object v4, v9

    array-length v15, v4

    const/4 v12, 0x0

    :goto_0
    if-ge v12, v15, :cond_0

    aget-object v8, v4, v12

    invoke-virtual {v8, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    const/4 v11, 0x1

    :cond_0
    if-nez v11, :cond_3

    const/4 v3, 0x0

    :cond_1
    :goto_1
    return-object v3

    :catch_0
    move-exception v10

    const-string v18, "[BT][PAN][BluetoothPANService]"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Error listing Interfaces :"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :cond_3
    invoke-direct/range {p0 .. p1}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->createNewTetheringAddressLocked(I)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_4

    const/4 v3, 0x0

    goto :goto_1

    :cond_4
    const/4 v14, 0x0

    :try_start_1
    move-object/from16 v0, v17

    invoke-interface {v0, v13}, Landroid/os/INetworkManagementService;->getInterfaceConfig(Ljava/lang/String;)Landroid/net/InterfaceConfiguration;

    move-result-object v14

    if-eqz v14, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v14}, Landroid/net/InterfaceConfiguration;->getLinkAddress()Landroid/net/LinkAddress;

    move-result-object v16

    if-eqz v16, :cond_5

    invoke-virtual/range {v16 .. v16}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v2

    if-eqz v2, :cond_5

    const-string v18, "0.0.0.0"

    invoke-static/range {v18 .. v18}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_5

    const-string v18, "::0"

    invoke-static/range {v18 .. v18}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_6

    :cond_5
    invoke-static {v3}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    :cond_6
    invoke-virtual {v14}, Landroid/net/InterfaceConfiguration;->setInterfaceUp()V

    const-string v18, "running"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/net/InterfaceConfiguration;->clearFlag(Ljava/lang/String;)V

    new-instance v18, Landroid/net/LinkAddress;

    const/16 v19, 0x18

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v2, v1}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/net/InterfaceConfiguration;->setLinkAddress(Landroid/net/LinkAddress;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v13, v14}, Landroid/os/INetworkManagementService;->setInterfaceConfig(Ljava/lang/String;Landroid/net/InterfaceConfiguration;)V

    if-nez p2, :cond_7

    invoke-virtual {v7, v13}, Landroid/net/ConnectivityManager;->tether(Ljava/lang/String;)I

    move-result v18

    if-eqz v18, :cond_1

    const-string v18, "[BT][PAN][BluetoothPANService]"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Error tethering "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v10

    const-string v18, "[BT][PAN][BluetoothPANService]"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Error configuring interface "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", :"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_7
    const/16 v18, 0x1

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    :try_start_2
    invoke-direct/range {p0 .. p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->startNetworkConfig()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_1
.end method

.method private native forceClearServerNative()V
.end method

.method private genPanNotification(ILandroid/bluetooth/BluetoothDevice;Z)Landroid/app/Notification;
    .locals 16
    .param p1    # I
    .param p2    # Landroid/bluetooth/BluetoothDevice;
    .param p3    # Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;

    if-nez v7, :cond_0

    const-string v13, "[BT][PAN][BluetoothPANService]"

    const-string v14, "genPanNotification invalid device!"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v12, 0x0

    :goto_0
    return-object v12

    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    new-instance v11, Landroid/content/Intent;

    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    const/4 v12, 0x0

    const/4 v10, 0x0

    const/4 v4, -0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v1, 0x0

    const-string v13, "[BT][PAN][BluetoothPANService]"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "genPanNotification "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p2 .. p2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ";device_state "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {v7}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$700(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v4, 0x1080080

    const-class v13, Lcom/mediatek/bluetooth/pan/BluetoothPanAlert;

    invoke-virtual {v13}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v13

    const-string v14, "device_addr"

    invoke-virtual/range {p2 .. p2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6}, Ljava/lang/String;-><init>()V

    const/4 v5, 0x0

    invoke-static {v7}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$700(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v13

    const/4 v14, 0x2

    if-ne v13, v14, :cond_4

    const/4 v5, 0x2

    invoke-static {v7}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v13

    if-nez v13, :cond_3

    const v13, 0x7f060112

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v3, v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const v13, 0x7f060110

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v13, 0x7f060111

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v1, "bt_pan_NAP_device_connected"

    :cond_1
    :goto_1
    new-instance v12, Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    invoke-direct {v12, v4, v8, v13, v14}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iput v5, v12, Landroid/app/Notification;->flags:I

    if-eqz p3, :cond_2

    iget v13, v12, Landroid/app/Notification;->defaults:I

    or-int/lit8 v13, v13, 0x1

    iput v13, v12, Landroid/app/Notification;->defaults:I

    iget v13, v12, Landroid/app/Notification;->defaults:I

    or-int/lit8 v13, v13, 0x2

    iput v13, v12, Landroid/app/Notification;->defaults:I

    :cond_2
    const-string v13, "action"

    invoke-virtual {v11, v13, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    const/high16 v14, 0x10000000

    move/from16 v0, p1

    invoke-static {v13, v0, v11, v14}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mContext:Landroid/content/Context;

    invoke-virtual {v12, v13, v9, v6, v10}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    goto/16 :goto_0

    :cond_3
    invoke-static {v7}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_1

    const v13, 0x7f06010f

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v3, v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const v13, 0x7f06010d

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v13, 0x7f06010e

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v1, "bt_pan_GN_device_connected"

    goto :goto_1

    :cond_4
    invoke-static {v7}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$700(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_1

    const/16 v5, 0x8

    invoke-static {v7}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v13

    if-nez v13, :cond_5

    const v13, 0x7f06010c

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v3, v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const v13, 0x7f06010a

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v13, 0x7f06010b

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v1, "bt_pan_NAP_device_authorize"

    goto/16 :goto_1

    :cond_5
    invoke-static {v7}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_1

    const v13, 0x7f060109

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v3, v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const v13, 0x7f060107

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v13, 0x7f060108

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v1, "bt_pan_GN_device_authorize"

    goto/16 :goto_1
.end method

.method private native initServiceNative()Z
.end method

.method private native listentoSocketNative()Z
.end method

.method private printLog(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "[BT][PAN][BluetoothPANService]"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private sendServiceMsg(ILjava/lang/String;)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    const-string v2, "[BT][PAN][BluetoothPANService]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendServiceMsg status="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "message="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput p1, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "show toast"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mServiceHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private native serverActivateReqNative()V
.end method

.method private static native serverAuthorizeRspNative(Ljava/lang/String;Z)V
.end method

.method private native serverConnectReqNative(ILjava/lang/String;)V
.end method

.method private native serverDeactivateReqNative()V
.end method

.method private static native serverDisconnectReqNative(Ljava/lang/String;)V
.end method

.method private startNetworkConfig()Z
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "[BT][PAN][BluetoothPANService]"

    const-string v3, "start Tethering mDhcp Range"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNetworkManagementService:Landroid/os/INetworkManagementService;

    iget-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mDhcpRange:[Ljava/lang/String;

    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->startTethering([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v2, "[BT][PAN][BluetoothPANService]"

    const-string v3, "set Dns Forwarders"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNetworkManagementService:Landroid/os/INetworkManagementService;

    iget-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mDnsServers:[Ljava/lang/String;

    invoke-interface {v2, v3}, Landroid/os/INetworkManagementService;->setDnsForwarders([Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v2, "[BT][PAN][BluetoothPANService]"

    const-string v3, "startNetworkConfig error when startTethering"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "[BT][PAN][BluetoothPANService]"

    const-string v3, "startNetworkConfig error when setDnsForwarders"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private native stopListentoSocketNative()V
.end method

.method private stopNetworkConfig()Z
    .locals 3

    :try_start_0
    const-string v1, "[BT][PAN][BluetoothPANService]"

    const-string v2, "stopNetworkConfig"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNetworkManagementService:Landroid/os/INetworkManagementService;

    invoke-interface {v1}, Landroid/os/INetworkManagementService;->stopTethering()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v1, "[BT][PAN][BluetoothPANService]"

    const-string v2, "error when stopTethering"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateProfileState(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method private updateSettingsState(IILandroid/bluetooth/BluetoothDevice;I)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/bluetooth/BluetoothDevice;
    .param p4    # I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateSettingsState("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->printLog(Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.bluetooth.pan.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "android.bluetooth.pan.extra.LOCAL_ROLE"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mContext:Landroid/content/Context;

    const-string v3, "android.permission.BLUETOOTH"

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Pan Device state : device: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " State:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "->"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->printLog(Ljava/lang/String;)V

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mBluetoothService:Landroid/bluetooth/IBluetooth;

    const/4 v3, 0x5

    invoke-interface {v2, p3, v3, p2, p1}, Landroid/bluetooth/IBluetooth;->sendConnectionStateChange(Landroid/bluetooth/BluetoothDevice;III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "[BT][PAN][BluetoothPANService]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendConnectionStateChange Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private native wakeupListenerNative()V
.end method


# virtual methods
.method public authorizeRsp(Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->serverAuthorizeRspNative(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public connectPanDevice(Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;

    invoke-direct {v1, p0, v3, p2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;-><init>(Lcom/mediatek/bluetooth/pan/BluetoothPanService;II)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-direct {p0, v2, v3, v0, p2}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->updateSettingsState(IILandroid/bluetooth/BluetoothDevice;I)V

    invoke-direct {p0, p2, p1}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->serverConnectReqNative(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public disconnectPanDevice(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iget-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;

    if-nez v1, :cond_0

    const-string v3, "[BT][PAN][BluetoothPANService]"

    const-string v4, "unknown device"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-static {v1}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$700(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v2

    const/4 v3, 0x3

    invoke-static {v1, v3}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$702(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;I)I

    invoke-static {v1}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$700(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v3

    invoke-static {v1}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v4

    invoke-direct {p0, v2, v3, v0, v4}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->updateSettingsState(IILandroid/bluetooth/BluetoothDevice;I)V

    invoke-static {p1}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->serverDisconnectReqNative(Ljava/lang/String;)V

    goto :goto_0
.end method

.method localClearService()V
    .locals 11

    const/4 v10, 0x0

    const/4 v7, 0x0

    const/4 v0, 0x0

    sget-boolean v8, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mServerState:Z

    if-eqz v8, :cond_4

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->serverDeactivateReqNative()V

    iget-object v8, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    iget-object v8, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v8, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;

    if-eqz v5, :cond_0

    invoke-static {v5}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$700(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    iget-object v8, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v8, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notifyMap:Ljava/util/Map;

    invoke-interface {v8, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    iput v8, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    iget-object v8, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNM:Landroid/app/NotificationManager;

    iget v9, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->notify_id:I

    invoke-virtual {v8, v9}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_1
    invoke-static {v5}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;->access$800(Lcom/mediatek/bluetooth/pan/BluetoothPanService$BluetoothPanDevice;)I

    move-result v8

    invoke-direct {p0, v6, v10, v1, v8}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->updateSettingsState(IILandroid/bluetooth/BluetoothDevice;I)V

    goto :goto_0

    :cond_2
    const-wide/16 v8, 0x64

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x64

    :cond_3
    sget-boolean v8, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mServerState:Z

    if-eqz v8, :cond_4

    const/16 v8, 0x1388

    if-lt v0, v8, :cond_2

    const/4 v7, 0x1

    :cond_4
    if-eqz v7, :cond_5

    const-string v8, "[BT][PAN][BluetoothPANService]"

    const-string v9, "Waiting DEREGISTER_SERVER_CNF time-out. Force clear server context."

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sput-boolean v10, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mServerState:Z

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->forceClearServerNative()V

    :cond_5
    iget-object v8, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mSocketListener:Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;

    if-eqz v8, :cond_6

    :try_start_1
    const-string v8, "mSocketListener close."

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->printLog(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mSocketListener:Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;

    invoke-virtual {v8}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;->shutdown()V

    iget-object v8, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mSocketListener:Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;

    const-wide/16 v9, 0x3e8

    invoke-virtual {v8, v9, v10}, Ljava/lang/Thread;->join(J)V

    const/4 v8, 0x0

    iput-object v8, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mSocketListener:Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;

    const-string v8, "mSocketListener close OK."

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->printLog(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_6
    :goto_2
    iget-object v8, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->clear()V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->stopListentoSocketNative()V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->cleanServiceNative()V

    return-void

    :catch_0
    move-exception v3

    const-string v8, "[BT][PAN][BluetoothPANService]"

    const-string v9, "Waiting for server deregister-cnf was interrupted."

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_1
    move-exception v3

    const-string v8, "[BT][PAN][BluetoothPANService]"

    const-string v9, "mSocketListener close error."

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method localCreateService()V
    .locals 2

    sget-boolean v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mServerState:Z

    if-nez v0, :cond_1

    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->updateProfileState(I)V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->initServiceNative()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->serverActivateReqNative()V

    const-string v0, "Succeed to init BluetoothPanService."

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->printLog(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mSocketListener:Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;-><init>(Lcom/mediatek/bluetooth/pan/BluetoothPanService;Lcom/mediatek/bluetooth/pan/BluetoothPanService$1;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mSocketListener:Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;

    iget-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mSocketListener:Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;

    const-string v1, "BTPanSocketListener"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mSocketListener:Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;->stopped:Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mSocketListener:Lcom/mediatek/bluetooth/pan/BluetoothPanService$SocketListenerThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const-string v0, "SocketListener started."

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->printLog(Ljava/lang/String;)V

    :cond_0
    const-string v0, "[BT][PAN][BluetoothPANService]"

    const-string v1, "Pre-enable PAN Server"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "[BT][PAN][BluetoothPANService]"

    const-string v1, "Failed to init BluetoothPanService."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[BT][PAN][BluetoothPANService]"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Enter onBind(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-class v1, Landroid/bluetooth/IBluetoothPan;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPan:Landroid/bluetooth/IBluetoothPan$Stub;

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanAction:Lcom/mediatek/bluetooth/pan/IBluetoothPanAction$Stub;

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    const-string v0, "[BT][PAN][BluetoothPANService]"

    const-string v1, "Enter onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mPanDevices:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mTetheringOn:Z

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "[BT][PAN][BluetoothPANService]"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mHasInitiated:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->localClearService()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mHasInitiated:Z

    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v5, 0x1

    const-string v3, "[BT][PAN][BluetoothPANService]"

    const-string v4, "Enter onStartCommand()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mHasInitiated:Z

    if-nez v3, :cond_5

    const-string v3, "bluetooth"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/bluetooth/IBluetooth$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetooth;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mBluetoothService:Landroid/bluetooth/IBluetooth;

    const-string v3, "notification"

    invoke-virtual {p0, v3}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    iput-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNM:Landroid/app/NotificationManager;

    iget-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNM:Landroid/app/NotificationManager;

    if-nez v3, :cond_0

    const-string v3, "[BT][PAN][BluetoothPANService]"

    const-string v4, "Get Notification-Manager failed. Stop PAN service."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    :cond_0
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "android.bluetooth.device.action.NAME_CHANGED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05000e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mDnsServers:[Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mDnsServers:[Ljava/lang/String;

    array-length v3, v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->DNS_DEFAULT_SERVER:[Ljava/lang/String;

    iput-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mDnsServers:[Ljava/lang/String;

    :cond_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f05000d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mDhcpRange:[Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mDhcpRange:[Ljava/lang/String;

    array-length v3, v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mDhcpRange:[Ljava/lang/String;

    array-length v3, v3

    rem-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_3

    :cond_2
    sget-object v3, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->DHCP_DEFAULT_RANGE:[Ljava/lang/String;

    iput-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mDhcpRange:[Ljava/lang/String;

    :cond_3
    const-string v3, "network_management"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNetworkManagementService:Landroid/os/INetworkManagementService;

    iget-object v3, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mNetworkManagementService:Landroid/os/INetworkManagementService;

    if-nez v3, :cond_4

    const-string v3, "[BT][PAN][BluetoothPANService]"

    const-string v4, "Error get INetworkManagementService"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    :cond_4
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->localCreateService()V

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    iput-boolean v5, p0, Lcom/mediatek/bluetooth/pan/BluetoothPanService;->mHasInitiated:Z

    :goto_0
    return v5

    :cond_5
    const-string v3, "[BT][PAN][BluetoothPANService]"

    const-string v4, "Already started, just return!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
