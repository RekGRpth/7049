.class public Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;
.super Ljava/lang/Object;
.source "BluetoothPbapPath.java"


# static fields
.field private static final DEBUG:Z = true

.field public static final FOLDER_TYPE_CCH:I = 0x4

.field public static final FOLDER_TYPE_ICH:I = 0x1

.field public static final FOLDER_TYPE_MCH:I = 0x3

.field public static final FOLDER_TYPE_OCH:I = 0x2

.field public static final FOLDER_TYPE_PB:I = 0x0

.field public static final FOLDER_TYPE_SIM1_CCH:I = 0x9

.field public static final FOLDER_TYPE_SIM1_ICH:I = 0x6

.field public static final FOLDER_TYPE_SIM1_MCH:I = 0x8

.field public static final FOLDER_TYPE_SIM1_OCH:I = 0x7

.field public static final FOLDER_TYPE_SIM1_PB:I = 0x5

.field public static final FOLDER_TYPE_UNKNOWN:I = -0x1

.field public static final PBAP_PATH_BACKWARD:I = 0x1

.field public static final PBAP_PATH_FORWARD:I = 0x0

.field public static final PBAP_PATH_TO_ROOT:I = 0x2

.field private static final TAG:Ljava/lang/String; = "BluetoothPbapPath"

.field public static validPath:[Ljava/lang/String;


# instance fields
.field private mCurrentPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "telecom/pb"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "telecom/ich"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "telecom/och"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "telecom/mch"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "telecom/cch"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "SIM1/telecom/pb"

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->validPath:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->mCurrentPath:Ljava/lang/String;

    return-void
.end method

.method private errorLog(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "BluetoothPbapPath"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private formatPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :cond_0
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method private isValidPath(Ljava/lang/String;)Z
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    sget-object v5, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->validPath:[Ljava/lang/String;

    array-length v2, v5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->formatPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    sget-object v5, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->validPath:[Ljava/lang/String;

    aget-object v3, v5, v0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-gt v1, v5, :cond_1

    invoke-virtual {v3, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-eq v5, v1, :cond_0

    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2f

    if-ne v5, v6, :cond_1

    :cond_0
    const/4 v4, 0x1

    :goto_1
    return v4

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[ERR] invalid path : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->errorLog(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private printLog(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "BluetoothPbapPath"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public getCurrentPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->mCurrentPath:Ljava/lang/String;

    return-object v0
.end method

.method public getPathType(Ljava/lang/String;Z)I
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    sget-object v2, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->validPath:[Ljava/lang/String;

    array-length v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPathType("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->printLog(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->formatPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "path formatted="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->printLog(Ljava/lang/String;)V

    if-eqz p2, :cond_1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_1
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "absolute path is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->printLog(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_4

    sget-object v2, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->validPath:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_2
    return v0

    :cond_2
    iget-object p1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->mCurrentPath:Ljava/lang/String;

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, -0x1

    goto :goto_2
.end method

.method public setPath(ILjava/lang/String;)Z
    .locals 6
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[API] setPath("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->printLog(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    const-string v2, "Unknown set path operation"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->errorLog(Ljava/lang/String;)V

    :goto_0
    return v1

    :pswitch_0
    const-string v2, ""

    iput-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->mCurrentPath:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->mCurrentPath:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_0

    const-string v2, ""

    iput-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->mCurrentPath:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->mCurrentPath:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->mCurrentPath:Ljava/lang/String;

    const/16 v5, 0x2f

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->mCurrentPath:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->formatPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->mCurrentPath:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->mCurrentPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PBAP_PATH_FORWARD : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->printLog(Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->isValidPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iput-object p2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->mCurrentPath:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
