.class public Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;
.super Ljava/lang/Object;
.source "BluetoothPbapServer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$1;,
        Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$PbapSocketListener;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field public static final PBAP_AUTHORIZE_IND:I = 0x65

.field public static final PBAP_AUTH_CHALL_IND:I = 0x66

.field private static final PBAP_CNF_FAILED:I = 0x1

.field private static final PBAP_CNF_SUCCESS:I = 0x0

.field public static final PBAP_SESSION_DISCONNECTED:I = 0x68

.field public static final PBAP_SESSION_ESTABLISHED:I = 0x67

.field private static final TAG:Ljava/lang/String; = "BluetoothPbapServer"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mListener:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$PbapSocketListener;

.field private mLocalName:Ljava/lang/String;

.field private mLocalNumber:Ljava/lang/String;

.field private mNativeData:I

.field private mPath:Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;

.field private mResultPath:Ljava/lang/String;

.field private mServerState:I

.field private mServiceHandler:Landroid/os/Handler;

.field mSimAdn:Lcom/android/bluetooth/pbap/BluetoothPbapSimAdn;

.field private mVcardListing:Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "extpbap_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->classInitNative()V

    return-void
.end method

.method constructor <init>(Landroid/os/Handler;Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/os/Handler;
    .param p2    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mSimAdn:Lcom/android/bluetooth/pbap/BluetoothPbapSimAdn;

    iput-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mResultPath:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mServiceHandler:Landroid/os/Handler;

    iput-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mListener:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$PbapSocketListener;

    iput-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mPath:Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;

    iput-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalName:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mVcardListing:Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;

    iput-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mSimAdn:Lcom/android/bluetooth/pbap/BluetoothPbapSimAdn;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->initializeNativeDataNative()V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;Z)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->listenIndicationNative(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->errorLog(Ljava/lang/String;)V

    return-void
.end method

.method private native authChallengeRspNative(ZLjava/lang/String;Ljava/lang/String;)Z
.end method

.method private native authorizeRspNative(Z)Z
.end method

.method private static native classInitNative()V
.end method

.method private native cleanupNativeDataNative()V
.end method

.method private composeVCards(IZIIJJ)I
    .locals 15
    .param p1    # I
    .param p2    # Z
    .param p3    # I
    .param p4    # I
    .param p5    # J
    .param p7    # J

    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[API] composeVCards("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p5

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p7

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mResultPath:Ljava/lang/String;

    const/4 v3, -0x1

    move/from16 v0, p1

    if-ne v0, v3, :cond_0

    const-string v3, "[ERR] type is unknown"

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->errorLog(Ljava/lang/String;)V

    const/16 v3, 0xc0

    :goto_0
    return v3

    :cond_0
    if-ltz p3, :cond_1

    if-gez p4, :cond_2

    :cond_1
    const-string v3, "[ERR] listOffset or maxCount is negtive"

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->errorLog(Ljava/lang/String;)V

    const/16 v3, 0xc0

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    const/4 v3, 0x5

    move/from16 v0, p1

    if-ne v0, v3, :cond_4

    :cond_3
    const-string v10, "upper(display_name)"

    :goto_1
    packed-switch p1, :pswitch_data_0

    const-string v3, "Unsupported folder type"

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->errorLog(Ljava/lang/String;)V

    const/16 v3, 0xd1

    goto :goto_0

    :cond_4
    const-string v10, "date DESC"

    goto :goto_1

    :pswitch_0
    const-string v13, "type=1"

    :goto_2
    :pswitch_1
    invoke-direct {p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->getLocalNameAndNum()Z

    new-instance v2, Lcom/android/bluetooth/pbap/BluetoothVCardComposer;

    iget-object v3, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mContext:Landroid/content/Context;

    if-eqz p2, :cond_a

    const/high16 v4, -0x40000000

    :goto_3
    const/4 v5, 0x1

    if-eqz p1, :cond_b

    const/4 v6, 0x5

    move/from16 v0, p1

    if-eq v0, v6, :cond_b

    const/4 v6, 0x1

    :goto_4
    const/4 v7, 0x1

    invoke-direct/range {v2 .. v7}, Lcom/android/bluetooth/pbap/BluetoothVCardComposer;-><init>(Landroid/content/Context;IZZZ)V

    move-wide/from16 v0, p5

    invoke-virtual {v2, v0, v1}, Lcom/android/bluetooth/pbap/BluetoothVCardComposer;->setVCardAttribFilter(J)V

    if-eqz v12, :cond_5

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalName:Ljava/lang/String;

    iget-object v5, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalNumber:Ljava/lang/String;

    move/from16 v0, p2

    invoke-virtual {v2, v3, v4, v5, v0}, Lcom/android/bluetooth/pbap/BluetoothVCardComposer;->composeVCardForPhoneOwnNumber(ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v11

    :cond_5
    new-instance v8, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;

    invoke-direct {v8, v11}, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Lcom/android/bluetooth/pbap/BluetoothVCardComposer;->addHandler(Lcom/android/bluetooth/pbap/BluetoothVCardComposer$OneEntryHandler;)V

    const/4 v3, 0x0

    invoke-virtual {v2, v13, v3, v10}, Lcom/android/bluetooth/pbap/BluetoothVCardComposer;->init(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "composer init failed"

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->errorLog(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/android/bluetooth/pbap/BluetoothVCardComposer;->getErrorReason()Ljava/lang/String;

    move-result-object v3

    const-string v4, "There\'s no exportable in the database"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    const/16 v3, 0xd0

    goto :goto_0

    :pswitch_2
    const-string v13, "type=2"

    goto :goto_2

    :pswitch_3
    const-string v13, "type=3"

    goto :goto_2

    :pswitch_4
    const-string v13, "indicate_phone_or_sim_contact=-1"

    const-wide/16 v3, 0x0

    cmp-long v3, p7, v3

    if-ltz v3, :cond_6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND _id= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p7

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/16 p3, 0x0

    const/16 p4, 0x1

    goto :goto_2

    :cond_6
    if-nez p3, :cond_7

    const/4 v12, 0x1

    add-int/lit8 p4, p4, -0x1

    goto :goto_2

    :cond_7
    add-int/lit8 p3, p3, -0x1

    goto/16 :goto_2

    :pswitch_5
    const-string v13, "indicate_phone_or_sim_contact>-1"

    const-wide/16 v3, 0x0

    cmp-long v3, p7, v3

    if-ltz v3, :cond_8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND _id= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p7

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/16 p3, 0x0

    const/16 p4, 0x1

    goto/16 :goto_2

    :cond_8
    if-nez p3, :cond_9

    const/4 v12, 0x1

    add-int/lit8 p4, p4, -0x1

    goto/16 :goto_2

    :cond_9
    add-int/lit8 p3, p3, -0x1

    goto/16 :goto_2

    :cond_a
    const v4, -0x3fffffff

    goto/16 :goto_3

    :cond_b
    const/4 v6, 0x0

    goto/16 :goto_4

    :cond_c
    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/android/bluetooth/pbap/BluetoothVCardComposer;->movePosition(I)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_f

    invoke-virtual {v2}, Lcom/android/bluetooth/pbap/BluetoothVCardComposer;->getCount()I

    move-result v3

    sub-int v14, v3, p3

    move/from16 v0, p4

    if-ge v14, v0, :cond_d

    move/from16 p4, v14

    :cond_d
    const/4 v9, 0x0

    :goto_5
    move/from16 v0, p4

    if-ge v9, v0, :cond_10

    invoke-virtual {v2}, Lcom/android/bluetooth/pbap/BluetoothVCardComposer;->createOneEntry()Z

    move-result v3

    if-nez v3, :cond_e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CreateEntry "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " failed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->errorLog(Ljava/lang/String;)V

    const/16 v3, 0xd0

    goto/16 :goto_0

    :cond_e
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    :cond_f
    const/16 v3, 0xc4

    goto/16 :goto_0

    :cond_10
    invoke-virtual {v8}, Lcom/android/bluetooth/pbap/BluetoothVCardEntryHandler;->getPath()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mResultPath:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/android/bluetooth/pbap/BluetoothVCardComposer;->terminate()V

    const/16 v3, 0xa0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_5
    .end packed-switch
.end method

.method private native connectRspNative(IZ)Z
.end method

.method private native disableNative()V
.end method

.method private native disconnectNative()V
.end method

.method private native enableNative()Z
.end method

.method private errorLog(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "BluetoothPbapServer"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private getLocalNameAndNum()Z
    .locals 3

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalNumber:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1AlphaTag()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalName:Ljava/lang/String;

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getLocalNameAndNum : name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " num="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalNumber:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    iput-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalNumber:Ljava/lang/String;

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    iput-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalName:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private getNewMissedCallSize()I
    .locals 8

    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/String;

    const-string v0, "type=3 AND new=1"

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getNewMissedCallSize : selection="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-string v5, "date DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "newMissed calls="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    return v7

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private getPbSize(I)I
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[API] getPbSize("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return v2

    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "indicate_phone_or_sim_contact=-1"

    :goto_1
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-eqz p1, :cond_1

    const/4 v2, 0x5

    if-ne p1, v2, :cond_2

    :cond_1
    add-int/lit8 v7, v7, 0x1

    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v2, v7

    goto :goto_0

    :pswitch_1
    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "type=1"

    goto :goto_1

    :pswitch_2
    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "type=2"

    goto :goto_1

    :pswitch_3
    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "type=3"

    goto :goto_1

    :pswitch_4
    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    :pswitch_5
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "indicate_phone_or_sim_contact>-1"

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private native initializeNativeDataNative()V
.end method

.method private native listenIndicationNative(Z)Z
.end method

.method private onAbortInd()V
    .locals 0

    return-void
.end method

.method private onAuthChallInd(Ljava/lang/String;ZZ)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onAuthChallInd: name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isUserIdRequired="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isFullAccess="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    const/16 v0, 0x66

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->sendServiceMsg(ILjava/lang/Object;)V

    return-void
.end method

.method private onAuthorizeInd(Ljava/lang/String;)I
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[CBK] onAuthorizeInd("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    const/16 v1, 0x65

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->sendServiceMsg(ILjava/lang/Object;)V

    :goto_0
    const/4 v1, 0x1

    return v1

    :cond_0
    const-string v1, "Failed to get default adapter"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->errorLog(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->authorizeRspNative(Z)Z

    goto :goto_0
.end method

.method private onConnectInd(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const/4 v2, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[CBK] onConnectInd("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    invoke-direct {p0, p3, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->connectRspNative(IZ)Z

    move-result v0

    if-ne v2, v0, :cond_0

    new-instance v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;

    invoke-direct {v0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mPath:Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->getLocalNameAndNum()Z

    new-instance v0, Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalName:Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalNumber:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mVcardListing:Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;

    const/16 v0, 0x67

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->sendServiceMsg(ILjava/lang/Object;)V

    :cond_0
    const/16 v0, 0xa0

    return v0
.end method

.method private onDisconnectInd()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "[CBK] onDisconnectInd"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mPath:Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;

    iput-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mVcardListing:Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;

    iput-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mSimAdn:Lcom/android/bluetooth/pbap/BluetoothPbapSimAdn;

    const/16 v0, 0x68

    invoke-direct {p0, v0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->sendServiceMsg(ILjava/lang/Object;)V

    return-void
.end method

.method private onPullPhonebookInd(ILjava/lang/String;JZII)I
    .locals 16
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Z
    .param p6    # I
    .param p7    # I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[CBK] onPullPhonebookInd("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p3

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {p5 .. p5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p7

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ")"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    const/4 v11, 0x0

    const/4 v15, 0x0

    const v12, 0xffff

    const v13, 0xffff

    const/16 v14, 0xa0

    const-string v2, ".vcf"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_0

    const/4 v2, 0x0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x4

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mPath:Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v4}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->getPathType(Ljava/lang/String;Z)I

    move-result v3

    const/4 v2, -0x1

    if-ne v3, v2, :cond_1

    const/16 v14, 0xc0

    :goto_0
    const/16 v2, 0xa0

    if-ne v14, v2, :cond_4

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mResultPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v13, v12, v4}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->pullPhonebookRspNative(IIILjava/lang/String;)Z

    return v14

    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mResultPath:Ljava/lang/String;

    const/4 v2, 0x3

    if-ne v3, v2, :cond_2

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->getNewMissedCallSize()I

    move-result v12

    :cond_2
    if-nez p6, :cond_3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->getPbSize(I)I

    move-result v13

    goto :goto_0

    :cond_3
    const-wide/16 v9, -0x1

    move-object/from16 v2, p0

    move/from16 v4, p5

    move/from16 v5, p7

    move/from16 v6, p6

    move-wide/from16 v7, p3

    invoke-direct/range {v2 .. v10}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->composeVCards(IZIIJJ)I

    move-result v14

    goto :goto_0

    :cond_4
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private onPullVcardEntryInd(ILjava/lang/String;JZ)I
    .locals 17
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[CBK] onPullVcardEntryInd("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p3

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {p5 .. p5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ")"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    const/16 v16, 0xa0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mPath:Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->getPathType(Ljava/lang/String;Z)I

    move-result v3

    const-wide/16 v9, -0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/4 v2, -0x1

    if-eq v3, v2, :cond_8

    const-string v2, ".vcf"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x4

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    const/4 v2, 0x5

    if-eq v3, v2, :cond_0

    if-nez v3, :cond_5

    :cond_0
    const-wide/16 v4, 0x0

    cmp-long v2, v12, v4

    if-lez v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mVcardListing:Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;

    if-nez v2, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->getLocalNameAndNum()Z

    new-instance v2, Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mLocalNumber:Ljava/lang/String;

    invoke-direct {v2, v4, v5, v6}, Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mVcardListing:Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;

    :cond_1
    if-nez v3, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mVcardListing:Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;

    long-to-int v4, v12

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v4}, Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;->queryPbID(I)J

    move-result-wide v9

    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v2, v9, v4

    if-gez v2, :cond_2

    const/16 v16, 0xc4

    :cond_2
    :goto_1
    const/16 v2, 0xa0

    move/from16 v0, v16

    if-ne v0, v2, :cond_3

    long-to-int v5, v12

    const/4 v6, 0x1

    move-object/from16 v2, p0

    move/from16 v4, p5

    move-wide/from16 v7, p3

    invoke-direct/range {v2 .. v10}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->composeVCards(IZIIJJ)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v16

    :cond_3
    :goto_2
    const/16 v2, 0xa0

    move/from16 v0, v16

    if-ne v0, v2, :cond_9

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mResultPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->pullVcardEntryRspNative(ILjava/lang/String;)Z

    :goto_3
    return v16

    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mVcardListing:Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;

    long-to-int v4, v12

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v4}, Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;->querySimPbID(I)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v9

    goto :goto_0

    :cond_5
    const-wide/16 v4, 0x0

    cmp-long v2, v12, v4

    if-gtz v2, :cond_6

    const/16 v16, 0xc4

    goto :goto_1

    :cond_6
    const-wide/16 v4, 0x1

    sub-long/2addr v12, v4

    goto :goto_1

    :catch_0
    move-exception v11

    const/16 v16, 0xc0

    goto :goto_2

    :cond_7
    const/16 v16, 0xc0

    goto :goto_2

    :cond_8
    const/16 v16, 0xc0

    goto :goto_2

    :cond_9
    const/4 v2, 0x1

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->pullVcardEntryRspNative(ILjava/lang/String;)Z

    goto :goto_3
.end method

.method private onPullVcardListingInd(ILjava/lang/String;ILjava/lang/String;III)I
    .locals 12
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # I
    .param p7    # I

    const v9, 0xffff

    const v8, 0xffff

    const/4 v10, 0x0

    const/16 v11, 0xa0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[CBK] onPullVcardListingInd("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p5

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p6

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p7

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mPath:Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;

    if-nez v1, :cond_1

    const-string v1, "mPath is null"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->errorLog(Ljava/lang/String;)V

    const/16 v11, 0xd0

    :cond_0
    :goto_0
    const/16 v1, 0xa0

    if-ne v11, v1, :cond_7

    const/4 v1, 0x0

    :goto_1
    invoke-direct {p0, v1, v9, v8, v10}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->pullVcardListingRspNative(IIILjava/lang/String;)Z

    return v11

    :cond_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mPath:Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;

    const/4 v3, 0x1

    invoke-virtual {v1, p2, v3}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->getPathType(Ljava/lang/String;Z)I

    move-result v2

    const/4 v1, -0x1

    if-ne v2, v1, :cond_2

    const-string v1, "unknown folder type type"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->errorLog(Ljava/lang/String;)V

    const/16 v11, 0xc0

    goto :goto_0

    :cond_2
    const/4 v1, 0x3

    if-ne v2, v1, :cond_3

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->getNewMissedCallSize()I

    move-result v8

    :cond_3
    if-nez p6, :cond_4

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->getPbSize(I)I

    move-result v9

    goto :goto_0

    :cond_4
    const/4 v1, 0x2

    if-eq p3, v1, :cond_5

    const/4 v1, 0x2

    move/from16 v0, p5

    if-ne v0, v1, :cond_6

    :cond_5
    const-string v1, "order or search attrib is not supported"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    const/16 v11, 0xd1

    goto :goto_0

    :cond_6
    packed-switch v2, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown folder type : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->errorLog(Ljava/lang/String;)V

    const/16 v11, 0xc0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->getLocalNameAndNum()Z

    :pswitch_1
    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mVcardListing:Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;

    move v3, p3

    move/from16 v4, p5

    move-object/from16 v5, p4

    move/from16 v6, p7

    move/from16 v7, p6

    invoke-virtual/range {v1 .. v7}, Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;->list(IIILjava/lang/String;II)I

    move-result v11

    const/16 v1, 0xa0

    if-ne v11, v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mVcardListing:Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;

    invoke-virtual {v1}, Lcom/android/bluetooth/pbap/BluetoothPbapVCardListing;->getPath()Ljava/lang/String;

    move-result-object v10

    goto :goto_0

    :cond_7
    const/4 v1, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private onSetPathInd(Ljava/lang/String;I)I
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v3, 0x1

    const/16 v0, 0xa0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[CBK] onSetPathInd("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mPath:Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;

    invoke-virtual {v1, p2, p1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapPath;->setPath(ILjava/lang/String;)Z

    move-result v1

    if-eq v1, v3, :cond_0

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->setPathRspNative(I)Z

    const/16 v0, 0xc0

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->setPathRspNative(I)Z

    goto :goto_0
.end method

.method private printLog(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "BluetoothPbapServer"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private native pullPhonebookRspNative(IIILjava/lang/String;)Z
.end method

.method private native pullVcardEntryRspNative(ILjava/lang/String;)Z
.end method

.method private native pullVcardListingRspNative(IIILjava/lang/String;)Z
.end method

.method private sendServiceMsg(ILjava/lang/Object;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[API] sendServiceMsg("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mServiceHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mServiceHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mServiceHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    const-string v1, "mServiceHandler is null"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private native setPathRspNative(I)Z
.end method

.method private native wakeupListenerNative()V
.end method


# virtual methods
.method accept(Z)Z
    .locals 2
    .param p1    # Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[API] accept("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->authorizeRspNative(Z)Z

    move-result v0

    return v0
.end method

.method authChallRsp(ZLjava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[API] authChallRsp("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->authChallengeRspNative(ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public disable()V
    .locals 3

    const-string v1, "[API] disable"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mListener:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$PbapSocketListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mListener:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$PbapSocketListener;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$PbapSocketListener;->mStopListen:Z

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->wakeupListenerNative()V

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mListener:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$PbapSocketListener;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->disableNative()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mListener close error"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->errorLog(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v1, "Pbap server is not enabled yet"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->errorLog(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public enable()Z
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    const-string v1, "[API] enable"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->printLog(Ljava/lang/String;)V

    new-instance v1, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$PbapSocketListener;

    invoke-direct {v1, p0, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$PbapSocketListener;-><init>(Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$1;)V

    iput-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mListener:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$PbapSocketListener;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->enableNative()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mListener:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$PbapSocketListener;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$PbapSocketListener;->mStopListen:Z

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mListener:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$PbapSocketListener;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v1, "enableNative failed"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->errorLog(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->mListener:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer$PbapSocketListener;

    goto :goto_0
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->cleanupNativeDataNative()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method
