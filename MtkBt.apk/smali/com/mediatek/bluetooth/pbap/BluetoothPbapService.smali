.class public Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;
.super Landroid/app/Service;
.source "BluetoothPbapService.java"


# static fields
.field private static final DEBUG:Z = true

.field public static final PBAP_AUTHENTICATE_CANCEL_ACTION:Ljava/lang/String; = "com.android.bluetooth.pbap.authenticate.cancel"

.field public static final PBAP_AUTHENTICATE_RETURN_ACTION:Ljava/lang/String; = "com.android.bluetooth.pbap.authenticate.return"

.field public static final PBAP_AUTHENTICATE_TIMEOUT:I = 0x7530

.field public static final PBAP_AUTHORIZE_CANCEL_ACTION:Ljava/lang/String; = "com.android.bluetooth.pbap.authorize.cancel"

.field public static final PBAP_AUTHORIZE_RETURN_ACTION:Ljava/lang/String; = "com.android.bluetooth.pbap.authorize.return"

.field public static final PBAP_AUTHORIZE_TIMEOUT:I = 0x3a98

.field public static final PBAP_AUTH_TIMEOUT_IND:I = 0x3e9

.field public static final PBAP_NOTIFICATION_AUTHORIZE:I = 0x1

.field public static final PBAP_NOTIFICATION_AUTH_CHALL:I = 0x2

.field private static final TAG:Ljava/lang/String; = "BluetoothPbapService"


# instance fields
.field mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBluetoothService:Landroid/bluetooth/IBluetooth;

.field private mNM:Landroid/app/NotificationManager;

.field private mPbapServer:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;

.field private mPid:I

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

.field private final mServer:Landroid/bluetooth/IBluetoothPbap$Stub;

.field private mServerState:I

.field private mServiceHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    iput-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mNM:Landroid/app/NotificationManager;

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPid:I

    iput-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    new-instance v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService$1;-><init>(Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService$2;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService$2;-><init>(Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mServiceHandler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPbapServer:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;

    new-instance v0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService$3;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService$3;-><init>(Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mServer:Landroid/bluetooth/IBluetoothPbap$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->printLog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;)Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPbapServer:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->cancelServerNotification()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;I)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->setServerNotification(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/IBluetooth;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mBluetoothService:Landroid/bluetooth/IBluetooth;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;)I
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;

    iget v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPid:I

    return v0
.end method

.method static synthetic access$600(Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->errorLog(Ljava/lang/String;)V

    return-void
.end method

.method private broadcastPbapState(I)V
    .locals 3
    .param p1    # I

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.bluetooth.profilemanager.action.PROFILE_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v1, Landroid/bluetooth/BluetoothProfileManager$Profile;->Bluetooth_PBAP:Landroid/bluetooth/BluetoothProfileManager$Profile;

    const-string v2, "android.bluetooth.profilemanager.extra.PROFILE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "android.bluetooth.profilemanager.extra.EXTRA_NEW_STATE"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "android.permission.BLUETOOTH"

    invoke-virtual {p0, v0, v2}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method private cancelServerNotification()V
    .locals 4

    const/16 v3, 0xd

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mServiceHandler:Landroid/os/Handler;

    const/16 v2, 0x3e9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPid:I

    if-lez v1, :cond_0

    const-string v1, "unregisterReceiver"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->printLog(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPid:I

    const/4 v2, 0x1

    invoke-static {v3, v2}, Lcom/mediatek/bluetooth/util/NotificationFactory;->getProfileNotificationId(II)I

    move-result v2

    if-ne v1, v2, :cond_1

    const-string v1, "Send authorize cancel intent"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->printLog(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.bluetooth.pbap.authorize.cancel"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    :goto_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mNM:Landroid/app/NotificationManager;

    iget v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPid:I

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    const/4 v1, -0x1

    iput v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPid:I

    :cond_0
    return-void

    :cond_1
    iget v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPid:I

    const/4 v2, 0x2

    invoke-static {v3, v2}, Lcom/mediatek/bluetooth/util/NotificationFactory;->getProfileNotificationId(II)I

    move-result v2

    if-ne v1, v2, :cond_2

    const-string v1, "Send authenticate cancel intent"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->printLog(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.bluetooth.pbap.authenticate.cancel"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ERR] invalid pid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->errorLog(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private errorLog(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "BluetoothPbapService"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private printLog(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "BluetoothPbapService"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private sendServiceMsg(ILjava/lang/Object;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[API] sendServiceMsg("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->printLog(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mServiceHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mServiceHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mServiceHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    const-string v1, "mServiceHandler is null"

    invoke-direct {p0, v1}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->printLog(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setServerNotification(I)V
    .locals 13
    .param p1    # I

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v6, -0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setServerNotification("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->printLog(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const/4 v8, 0x1

    if-ne p1, v8, :cond_1

    new-instance v3, Landroid/app/Notification;

    const v8, 0x1080080

    const v9, 0x7f060124

    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v3, v8, v9, v10, v11}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const-class v9, Lcom/mediatek/bluetooth/pbap/BluetoothServerAuthorize;

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    const/high16 v9, 0x10000000

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Remote device name is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->printLog(Ljava/lang/String;)V

    const-string v8, "com.mediatek.bluetooth.extra.device_name"

    iget-object v9, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "com.mediatek.bluetooth.extra.action_cancel"

    const-string v10, "com.android.bluetooth.pbap.authorize.cancel"

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "com.mediatek.bluetooth.extra.action_return"

    const-string v10, "com.android.bluetooth.pbap.authorize.return"

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "com.android.bluetooth.pbap.authorize.return"

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {p0, v8, v2, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const v8, 0x7f06012c

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f060126

    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v1, v8, v9, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/16 v6, 0x3a98

    :goto_0
    const/4 v8, 0x4

    new-array v7, v8, [J

    fill-array-data v7, :array_0

    iput-object v7, v3, Landroid/app/Notification;->vibrate:[J

    iget v8, v3, Landroid/app/Notification;->defaults:I

    or-int/lit8 v8, v8, 0x2

    iput v8, v3, Landroid/app/Notification;->defaults:I

    iget v8, v3, Landroid/app/Notification;->defaults:I

    or-int/lit8 v8, v8, 0x1

    iput v8, v3, Landroid/app/Notification;->defaults:I

    iget v8, v3, Landroid/app/Notification;->flags:I

    or-int/lit8 v8, v8, 0x20

    iput v8, v3, Landroid/app/Notification;->flags:I

    const/16 v8, 0xd

    invoke-static {v8, p1}, Lcom/mediatek/bluetooth/util/NotificationFactory;->getProfileNotificationId(II)I

    move-result v8

    iput v8, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPid:I

    iget-object v8, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mNM:Landroid/app/NotificationManager;

    iget v9, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPid:I

    invoke-virtual {v8, v9, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    new-instance v5, Landroid/content/IntentFilter;

    invoke-direct {v5, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v8, "registerReceiver"

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->printLog(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v5}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    if-lez v6, :cond_0

    iget-object v8, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mServiceHandler:Landroid/os/Handler;

    iget-object v9, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mServiceHandler:Landroid/os/Handler;

    const/16 v10, 0x3e9

    invoke-virtual {v9, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v9

    int-to-long v10, v6

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v8, 0x2

    if-ne p1, v8, :cond_2

    new-instance v3, Landroid/app/Notification;

    const v8, 0x1080080

    const v9, 0x7f060125

    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-direct {v3, v8, v9, v10, v11}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const-class v9, Lcom/mediatek/bluetooth/pbap/BluetoothAuthenticating;

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    const/high16 v9, 0x10000000

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v8, "com.mediatek.bluetooth.extra.device_name"

    iget-object v9, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "com.mediatek.bluetooth.extra.action_cancel"

    const-string v10, "com.android.bluetooth.pbap.authenticate.cancel"

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    const-string v9, "com.mediatek.bluetooth.extra.action_return"

    const-string v10, "com.android.bluetooth.pbap.authenticate.return"

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "com.android.bluetooth.pbap.authenticate.return"

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {p0, v8, v2, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const v8, 0x7f06012e

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f06012f

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v12}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {p0, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v1, v8, v9, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/16 v6, 0x7530

    goto/16 :goto_0

    :cond_2
    const-string v8, "[ERR] unsupported mode"

    invoke-direct {p0, v8}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->errorLog(Ljava/lang/String;)V

    goto :goto_1

    :array_0
    .array-data 8
        0x0
        0x64
        0xc8
        0x12c
    .end array-data
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "BluetoothPbapService"

    const-string v1, "Enter onBind()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-class v0, Landroid/bluetooth/IBluetoothPbap;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mServer:Landroid/bluetooth/IBluetoothPbap$Stub;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 7

    const/4 v6, 0x0

    const/16 v5, 0xe

    const-string v2, "Enter onCreate()"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->printLog(Ljava/lang/String;)V

    const/16 v2, 0xa

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->broadcastPbapState(I)V

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    iput-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mNM:Landroid/app/NotificationManager;

    iget-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mNM:Landroid/app/NotificationManager;

    if-nez v2, :cond_0

    const-string v2, "BluetoothPbapService"

    const-string v3, "Get Notification-Manager failed, stop PBAP service."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    invoke-direct {p0, v5}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->broadcastPbapState(I)V

    :goto_0
    const-string v2, "bluetooth"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Bluetooth service not available"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;

    iget-object v3, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mServiceHandler:Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;-><init>(Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPbapServer:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;

    iget-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPbapServer:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;

    invoke-virtual {v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->enable()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "[ERR] Pbap server enable failed"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->errorLog(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPbapServer:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;

    iput-object v6, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    invoke-direct {p0, v5}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->broadcastPbapState(I)V

    goto :goto_0

    :cond_1
    const/16 v2, 0xb

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->broadcastPbapState(I)V

    goto :goto_0

    :cond_2
    iput-object v6, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    const-string v2, "[ERR] Pbap server enable failed"

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->errorLog(Ljava/lang/String;)V

    invoke-direct {p0, v5}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->broadcastPbapState(I)V

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    goto :goto_0

    :cond_3
    invoke-static {v0}, Landroid/bluetooth/IBluetooth$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetooth;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mBluetoothService:Landroid/bluetooth/IBluetooth;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "onDestroy()"

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->printLog(Ljava/lang/String;)V

    const-string v0, "Before stop listening to socket."

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->printLog(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->cancelServerNotification()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPbapServer:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPbapServer:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;

    invoke-virtual {v0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;->disable()V

    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->broadcastPbapState(I)V

    :cond_0
    iput-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mPbapServer:Lcom/mediatek/bluetooth/pbap/BluetoothPbapServer;

    iput-object v1, p0, Lcom/mediatek/bluetooth/pbap/BluetoothPbapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-void
.end method
