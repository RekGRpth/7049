.class public Lcom/mediatek/bluetooth/psm/ResultCode;
.super Ljava/lang/Object;
.source "ResultCode.java"


# static fields
.field public static final STATUS_FAILED:I = 0x3

.field public static final STATUS_PENDING:I = 0x2

.field public static final STATUS_SUCCESS:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(I)I
    .locals 1
    .param p0    # I

    shl-int/lit8 v0, p0, 0x10

    return v0
.end method

.method public static create(II)I
    .locals 1
    .param p0    # I
    .param p1    # I

    shl-int/lit8 v0, p0, 0x10

    or-int/2addr v0, p1

    return v0
.end method

.method public static rspcode(I)I
    .locals 1
    .param p0    # I

    const v0, 0xffff

    and-int/2addr v0, p0

    return v0
.end method

.method public static status(I)I
    .locals 1
    .param p0    # I

    shr-int/lit8 v0, p0, 0x10

    return v0
.end method
