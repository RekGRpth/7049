.class Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothSimapActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity$1;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.mediatek.bluetooth.simap.userconfirmtimeout"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity$1;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity;

    invoke-static {v1}, Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity;->access$000(Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "com.mediatek.bluetooth.simap.intent.action.BTSIMAP_DISCONNECTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity$1;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity;

    invoke-static {v1}, Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity;->access$100(Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity;)V

    goto :goto_0

    :cond_2
    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "android.bluetooth.adapter.extra.STATE"

    const/high16 v2, -0x80000000

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v1, "BluetoothSimapConfirmActivity"

    const-string v2, "Received BluetoothAdapter.STATE_TURNING_OFF."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity$1;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
    .end packed-switch
.end method
