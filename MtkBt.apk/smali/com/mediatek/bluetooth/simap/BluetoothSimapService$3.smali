.class Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothSimapService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/simap/BluetoothSimapService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mBluetoothReceiver:[Intent] action="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {v4}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$1100(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$2100(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$1100(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)I

    move-result v3

    if-ne v3, v6, :cond_1

    const-string v3, "android.bluetooth.device.action.ACL_DISCONNECT_REQUESTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$000(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$000(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$2200(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)Landroid/bluetooth/IBluetoothSimap$Stub;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/IBluetoothSimap$Stub;->disconnect()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "android.bluetooth.adapter.extra.STATE"

    const/high16 v4, -0x80000000

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$2300(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)V

    goto :goto_0

    :cond_2
    const-string v3, "com.mediatek.bluetooth.simap.accessallowed"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "BluetoothSimapService"

    const-string v4, "ACCESS_ALLOWED_ACTION"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "com.mediatek.bluetooth.simap.alwaysallowed"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$000(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/bluetooth/BluetoothDevice;->setTrust(Z)Z

    move-result v2

    const-string v3, "BluetoothSimapService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setTrust() result="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$500()I

    move-result v4

    invoke-static {v3, v4}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$600(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;I)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {v3, v6}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$200(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;Z)V

    goto :goto_0

    :cond_4
    const-string v3, "com.mediatek.bluetooth.simap.accessdisallowed"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$500()I

    move-result v4

    invoke-static {v3, v4}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$600(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;I)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {v3, v5}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$200(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;Z)V

    goto :goto_0

    :cond_5
    const-string v3, "simap_disconnect_request"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    iget-object v4, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {v4}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$2400(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$1300(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;I)V

    goto/16 :goto_0

    :cond_6
    const-string v3, "com.mediatek.bluetooth.simap.intent.action.SEND_SIMUNAVALIBLE_IND"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$2500(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)V

    goto/16 :goto_0

    :cond_7
    const-string v3, "android.bluetooth.device.action.NAME_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$000(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$000(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "BluetoothSimapService"

    const-string v4, "mRemoteDevice name changed!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->access$2600(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)V

    goto/16 :goto_0

    :catch_0
    move-exception v3

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
    .end packed-switch
.end method
