.class public Lcom/mediatek/bluetooth/simap/BluetoothSimapService;
.super Landroid/app/Service;
.source "BluetoothSimapService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;
    }
.end annotation


# static fields
.field public static final ACCESS_ALLOWED_ACTION:Ljava/lang/String; = "com.mediatek.bluetooth.simap.accessallowed"

.field public static final ACCESS_DISALLOWED_ACTION:Ljava/lang/String; = "com.mediatek.bluetooth.simap.accessdisallowed"

.field public static final ACCESS_REQUEST_ACTION:Ljava/lang/String; = "com.mediatek.bluetooth.simap.accessrequest"

.field public static final ACTION_CLEAR_AUTH_NOTIFICATION:Ljava/lang/String; = "com.mediatek.bluetooth.simap.intent.action.CLEAR_AUTH"

.field public static final ACTION_CLEAR_CONN_NOTIFICATION:Ljava/lang/String; = "com.mediatek.bluetooth.simap.intent.action.CLEAR_CONN"

.field private static final BLUETOOTH_ADMIN_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH_ADMIN"

.field private static final BLUETOOTH_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH"

.field public static final BTSIMAP_CONNECTED:Ljava/lang/String; = "com.mediatek.bluetooth.simap.intent.action.BTSIMAP_CONNECTED"

.field public static final BTSIMAP_DISCONNECTED:Ljava/lang/String; = "com.mediatek.bluetooth.simap.intent.action.BTSIMAP_DISCONNECTED"

.field public static final CONNECTED_NOTIFY_ACTION:Ljava/lang/String; = "com.mediatek.bluetooth.simap.connectednotify"

.field public static final DEBUG:Z = true

.field static final DISCONNECT_REQUEST:Ljava/lang/String; = "simap_disconnect_request"

.field public static final EXTRA_ALWAYS_ALLOWED:Ljava/lang/String; = "com.mediatek.bluetooth.simap.alwaysallowed"

.field private static final KEY_SIMAP_ENABLE:Ljava/lang/String; = "simap_server_enable"

.field private static final KEY_SIMAP_SETTINGS:Ljava/lang/String; = "simap_server_settings"

.field private static final KEY_SIMAP_SIM_INDEX:Ljava/lang/String; = "simap_server_sim_index"

.field private static final NOTIFICATION_ID_ACCESS:I

.field private static final NOTIFICATION_ID_CONNECTED:I

.field public static final RESULT_CANCELED:I = 0x2

.field public static final RESULT_FAILURE:I = 0x0

.field public static final RESULT_SUCCESS:I = 0x1

.field public static final SEND_SIMUNAVALIBLE_IND:Ljava/lang/String; = "com.mediatek.bluetooth.simap.intent.action.SEND_SIMUNAVALIBLE_IND"

.field public static final SIMAP_AUTHORIZE_IND:I = 0x69

.field public static final SIMAP_CONNECTED:I = 0x6a

.field public static final SIMAP_DISCONNECTED:I = 0x6b

.field private static final SIMAP_ID_START:I

.field private static final START_LISTENER:I = 0x1

.field private static final TAG:Ljava/lang/String; = "BluetoothSimapService"

.field public static final THIS_PACKAGE_NAME:Ljava/lang/String; = "com.mediatek.bluetooth"

.field public static final USER_CONFIRM_TIMEOUT_ACTION:Ljava/lang/String; = "com.mediatek.bluetooth.simap.userconfirmtimeout"

.field private static final USER_CONFIRM_TIMEOUT_VALUE:I = 0x186a0

.field private static final USER_TIMEOUT:I = 0x2

.field public static final VERBOSE:Z = true

.field private static mHasStarted:Z

.field private static sRemoteDeviceName:Ljava/lang/String;


# instance fields
.field private GRACEFUL_DISC_MODE:I

.field private IMMEDIATE_DISC_MODE:I

.field private enableAction:Z

.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mBinder:Landroid/bluetooth/IBluetoothSimap$Stub;

.field private mBluetoothReceiver:Landroid/content/BroadcastReceiver;

.field private volatile mInterrupted:Z

.field private mNativeData:I

.field private mPreferences:Landroid/content/SharedPreferences;

.field private mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

.field private mSIMIndex:I

.field private mServiceInitiated:Z

.field private final mSessionStatusHandler:Landroid/os/Handler;

.field private final mSimapCallback:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Landroid/bluetooth/IBluetoothSimapCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mSocketListener:Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;

.field private mStartId:I

.field private mState:I

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private savedEnableState:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xe

    invoke-static {v0}, Lcom/mediatek/bluetooth/BluetoothProfile;->getProfileStart(I)I

    move-result v0

    sput v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->SIMAP_ID_START:I

    sget v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->SIMAP_ID_START:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->NOTIFICATION_ID_ACCESS:I

    sget v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->SIMAP_ID_START:I

    add-int/lit8 v0, v0, 0x2

    sput v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->NOTIFICATION_ID_CONNECTED:I

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->sRemoteDeviceName:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mHasStarted:Z

    const-string v0, "extsimap_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iput-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mServiceInitiated:Z

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->savedEnableState:Z

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->enableAction:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mStartId:I

    iput v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSIMIndex:I

    iput v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->GRACEFUL_DISC_MODE:I

    iput v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->IMMEDIATE_DISC_MODE:I

    new-instance v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$1;-><init>(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSessionStatusHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$2;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$2;-><init>(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mBinder:Landroid/bluetooth/IBluetoothSimap$Stub;

    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSimapCallback:Landroid/os/RemoteCallbackList;

    new-instance v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$3;-><init>(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    iput-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSocketListener:Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)Landroid/bluetooth/BluetoothDevice;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method static synthetic access$002(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    iput-object p1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    return-object p1
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->sRemoteDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->enableAction:Z

    return p1
.end method

.method static synthetic access$102(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Ljava/lang/String;

    sput-object p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->sRemoteDeviceName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)I
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    iget v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    return v0
.end method

.method static synthetic access$1200(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)I
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    iget v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->IMMEDIATE_DISC_MODE:I

    return v0
.end method

.method static synthetic access$1300(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;I)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->disconnectClient(I)V

    return-void
.end method

.method static synthetic access$1400(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;I)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->selectSIMNative(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)I
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    iget v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSIMIndex:I

    return v0
.end method

.method static synthetic access$1502(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;I)I
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSIMIndex:I

    return p1
.end method

.method static synthetic access$1600(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)Landroid/os/RemoteCallbackList;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSimapCallback:Landroid/os/RemoteCallbackList;

    return-object v0
.end method

.method static synthetic access$1700()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mHasStarted:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->prepareListentoSocketNative()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)Z
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->startListenNative()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->authorizeRsp(Z)V

    return-void
.end method

.method static synthetic access$2000(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->stopListenNative()V

    return-void
.end method

.method static synthetic access$2100(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)Landroid/bluetooth/IBluetoothSimap$Stub;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mBinder:Landroid/bluetooth/IBluetoothSimap$Stub;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->clearService()V

    return-void
.end method

.method static synthetic access$2400(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)I
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    iget v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->GRACEFUL_DISC_MODE:I

    return v0
.end method

.method static synthetic access$2500(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->sendSIMUnaccessibleInd()V

    return-void
.end method

.method static synthetic access$2600(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->updateDeviceName()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->createSimapAuthNotification(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSessionStatusHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500()I
    .locals 1

    sget v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->NOTIFICATION_ID_ACCESS:I

    return v0
.end method

.method static synthetic access$600(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;I)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->removeSimapAuthNotification(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->createSimapConnNotification()V

    return-void
.end method

.method static synthetic access$800()I
    .locals 1

    sget v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->NOTIFICATION_ID_CONNECTED:I

    return v0
.end method

.method static synthetic access$900(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;I)V
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->removeSimapConnNotification(I)V

    return-void
.end method

.method private authorizeRsp(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x2

    const-string v0, "BluetoothSimapService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "authorizeRsp: accept="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSessionStatusHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const-string v0, "BluetoothSimapService"

    const-string v1, "mState!=STATE_AUTHORZING, ignore the authorizeRsp request"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->setState(I)V

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->authorizeRspNative(Z)V

    goto :goto_0
.end method

.method private native authorizeRspNative(Z)V
.end method

.method private static native classInitNative()Z
.end method

.method private native cleanupNativeDataNative()V
.end method

.method private clearService()V
    .locals 7

    const/4 v6, -0x1

    const-string v3, "BluetoothSimapService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "clearService(), mState = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v3, 0x0

    sput-boolean v3, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mHasStarted:Z

    iget v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    if-eq v3, v6, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->disable()V

    :goto_0
    iget v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    if-eq v3, v6, :cond_0

    const/16 v3, 0x7d0

    if-lt v0, v3, :cond_3

    const/4 v2, 0x1

    :cond_0
    if-eqz v2, :cond_1

    const-string v3, "BluetoothSimapService"

    const-string v4, "Waiting DEREGISTER_SERVER_CNF time-out. Force clear server context."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    :cond_1
    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSocketListener:Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;

    if-eqz v3, :cond_2

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSocketListener:Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;

    invoke-virtual {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;->shutdown()V

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSocketListener:Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;

    invoke-virtual {v3}, Ljava/lang/Thread;->join()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSocketListener:Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;

    const-string v3, "BluetoothSimapService"

    const-string v4, "mSocketListener finish shutdown!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    :goto_1
    const-string v3, "BluetoothSimapService"

    const-string v4, "clearService call storeSettings() "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->savedEnableState:Z

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->storeSettings(Z)V

    return-void

    :cond_3
    const-wide/16 v3, 0x64

    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    add-int/lit8 v0, v0, 0x64

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "BluetoothSimapService"

    const-string v4, "Waiting for server deregister-cnf was interrupted."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_1
    move-exception v1

    const-string v3, "BluetoothSimapService"

    const-string v4, "mSocketListener close error."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private final closeSimapService()V
    .locals 3

    const-string v1, "BluetoothSimapService"

    const-string v2, "Simap Service closeSimapService"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mStartId:I

    invoke-virtual {p0, v1}, Landroid/app/Service;->stopSelfResult(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "BluetoothSimapService"

    const-string v2, "successfully stopped simap service"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->stopListenNative()V

    iget-object v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSocketListener:Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSocketListener:Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSocketListener:Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->cleanupNativeDataNative()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "BluetoothSimapService"

    const-string v2, "mSocketListener close error."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private createSimapAuthNotification(Z)V
    .locals 13
    .param p1    # Z

    const v12, 0x7f060132

    const/4 v11, 0x1

    const/4 v10, 0x0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v6, "notification"

    invoke-virtual {v1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v6, Lcom/mediatek/bluetooth/simap/BluetoothSimapActivity;

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 v6, 0x10000000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v6, "com.mediatek.bluetooth.simap.accessrequest"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v2, Landroid/content/Intent;

    const-string v6, "com.mediatek.bluetooth.simap.intent.action.CLEAR_AUTH"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v6, Lcom/mediatek/bluetooth/simap/BluetoothSimapReceiver;

    invoke-virtual {v2, v1, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v5, 0x0

    invoke-static {}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->getRemoteDeviceName()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Landroid/app/Notification;

    const v6, 0x1080080

    invoke-virtual {v1, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    invoke-virtual {v1, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f060137

    new-array v8, v11, [Ljava/lang/Object;

    aput-object v3, v8, v10

    invoke-virtual {v1, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v10, v0, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-virtual {v5, v1, v6, v7, v8}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iget v6, v5, Landroid/app/Notification;->flags:I

    or-int/lit8 v6, v6, 0x20

    iput v6, v5, Landroid/app/Notification;->flags:I

    if-eqz p1, :cond_0

    iget v6, v5, Landroid/app/Notification;->flags:I

    or-int/lit8 v6, v6, 0x8

    iput v6, v5, Landroid/app/Notification;->flags:I

    iput v11, v5, Landroid/app/Notification;->defaults:I

    :cond_0
    invoke-static {v1, v10, v2, v10}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    iput-object v6, v5, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    sget v6, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->NOTIFICATION_ID_ACCESS:I

    invoke-virtual {v4, v6, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method private createSimapConnNotification()V
    .locals 13

    const/4 v12, 0x1

    const/4 v11, 0x0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v7, "BluetoothSimapService"

    const-string v8, "createSimapConnNotification..."

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "notification"

    invoke-virtual {v1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v7, Lcom/mediatek/bluetooth/simap/BluetoothSimapConnNotification;

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 v7, 0x10000000

    invoke-virtual {v0, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v7, "com.mediatek.bluetooth.simap.connectednotify"

    invoke-virtual {v0, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v2, Landroid/content/Intent;

    const-string v7, "com.mediatek.bluetooth.simap.intent.action.CLEAR_CONN"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v7, Lcom/mediatek/bluetooth/simap/BluetoothSimapReceiver;

    invoke-virtual {v2, v1, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v5, 0x0

    invoke-static {}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->getRemoteDeviceName()Ljava/lang/String;

    move-result-object v3

    const v7, 0x7f06013b

    new-array v8, v12, [Ljava/lang/Object;

    aput-object v3, v8, v11

    invoke-virtual {v1, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v5, Landroid/app/Notification;

    const v7, 0x1080080

    const v8, 0x7f06013a

    invoke-virtual {v1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-direct {v5, v7, v8, v9, v10}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    const v7, 0x7f06013c

    new-array v8, v12, [Ljava/lang/Object;

    aput-object v3, v8, v11

    invoke-virtual {v1, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v11, v0, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-virtual {v5, v1, v6, v7, v8}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iget v7, v5, Landroid/app/Notification;->flags:I

    or-int/lit8 v7, v7, 0x20

    iput v7, v5, Landroid/app/Notification;->flags:I

    invoke-static {v1, v11, v2, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    iput-object v7, v5, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    sget v7, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->NOTIFICATION_ID_CONNECTED:I

    invoke-virtual {v4, v7, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method private native disableNative()V
.end method

.method private disconnectClient(I)V
    .locals 3
    .param p1    # I

    const-string v0, "BluetoothSimapService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "disconnectClient...discMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->setState(I)V

    invoke-direct {p0, p1}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->disconnectNative(I)V

    return-void
.end method

.method private native disconnectNative(I)V
.end method

.method private native enableNative()Z
.end method

.method public static getRemoteDeviceName()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->sRemoteDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method private native initServiceNative()Z
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "BluetoothSimapService"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private onAuthorizeInd(Ljava/lang/String;)Z
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "BluetoothSimapService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Enter onAuthorizeInd(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v3, "BluetoothSimapService"

    const-string v4, "adapter == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->authorizeRspNative(Z)V

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v3, "BluetoothSimapService"

    const-string v4, "obj == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->authorizeRspNative(Z)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x3

    invoke-direct {p0, v2}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->setState(I)V

    const/16 v2, 0x69

    invoke-direct {p0, v2, v1}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->sendServiceMsg(ILjava/lang/Object;)V

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private onBtResetInd()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, -0x1

    const-string v3, "BluetoothSimapService"

    const-string v4, "Enter onBtResetInd() [+]"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    if-ne v3, v5, :cond_0

    const-string v3, "BluetoothSimapService"

    const-string v4, "disable: mState is already STATE_IDLE, just return!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    sget v3, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->NOTIFICATION_ID_ACCESS:I

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->removeSimapAuthNotification(I)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSessionStatusHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    :cond_1
    :goto_1
    invoke-direct {p0, v5}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->setState(I)V

    const/16 v2, 0xd

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSimapCallback:Landroid/os/RemoteCallbackList;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSimapCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Start broadcasting to callback. N="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->log(Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v0, :cond_3

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSimapCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/IBluetoothSimapCallback;

    const/4 v4, 0x0

    invoke-interface {v3, v2, v4}, Landroid/bluetooth/IBluetoothSimapCallback;->postEvent(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    iget v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    invoke-direct {p0, v6}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->setState(I)V

    sget v3, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->NOTIFICATION_ID_CONNECTED:I

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->removeSimapConnNotification(I)V

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSimapCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    const-string v3, "End broadcasting to callback."

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->log(Ljava/lang/String;)V

    :cond_4
    const-string v3, "BluetoothSimapService"

    const-string v4, "onBtResetInd() [-] "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_3
.end method

.method private onBtSimapCmd(IILjava/lang/String;)Lcom/mediatek/bluetooth/simap/AfAdapterResult;
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;

    new-instance v0, Lcom/mediatek/bluetooth/simap/AfAdapterResult;

    invoke-direct {v0}, Lcom/mediatek/bluetooth/simap/AfAdapterResult;-><init>()V

    invoke-virtual {v0, p1, p2, p3}, Lcom/mediatek/bluetooth/simap/AfAdapterResult;->onCommand(IILjava/lang/String;)V

    return-object v0
.end method

.method private onConnectedInd(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "BluetoothSimapService"

    const-string v1, "Enter onConnectedInd()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->setState(I)V

    const/16 v0, 0x6a

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->sendServiceMsg(ILjava/lang/Object;)V

    return-void
.end method

.method private onDisableCnf(Z)V
    .locals 7
    .param p1    # Z

    const/4 v6, 0x0

    const-string v3, "BluetoothSimapService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Enter onDisableCnf: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, -0x1

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->setState(I)V

    const/16 v2, 0xd

    const-string v3, "BluetoothSimapService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onDisableCnf call storeSettings(false) enableAction = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->enableAction:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->enableAction:Z

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    iput-boolean v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->savedEnableState:Z

    iput-boolean v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->enableAction:Z

    :cond_0
    invoke-direct {p0, v6}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->storeSettings(Z)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSimapCallback:Landroid/os/RemoteCallbackList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSimapCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Start broadcasting to callback. N="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->log(Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSimapCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/IBluetoothSimapCallback;

    const/4 v4, 0x0

    invoke-interface {v3, v2, v4}, Landroid/bluetooth/IBluetoothSimapCallback;->postEvent(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSimapCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    const-string v3, "End broadcasting to callback."

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->log(Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_2
    const-string v3, "For non-ui server enabling."

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->log(Ljava/lang/String;)V

    goto :goto_2

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method private onDisconnectCnf(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "BluetoothSimapService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enter onDisconnectCnf(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->setState(I)V

    const/16 v0, 0x6b

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->sendServiceMsg(ILjava/lang/Object;)V

    return-void
.end method

.method private onDisconnectedInd()V
    .locals 2

    const-string v0, "BluetoothSimapService"

    const-string v1, "Enter onDisconnectedInd()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->setState(I)V

    const/16 v0, 0x6b

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->sendServiceMsg(ILjava/lang/Object;)V

    return-void
.end method

.method private onEnableCnf(Z)V
    .locals 7
    .param p1    # Z

    const/4 v6, 0x1

    const-string v3, "BluetoothSimapService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Enter onEnableCnf: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x2

    const/4 v3, 0x2

    invoke-direct {p0, v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->setState(I)V

    const-string v3, "BluetoothSimapService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "enableAction: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->enableAction:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->enableAction:Z

    if-ne v3, v6, :cond_0

    iput-boolean v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->savedEnableState:Z

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->enableAction:Z

    :cond_0
    invoke-direct {p0, v6}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->storeSettings(Z)V

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSimapCallback:Landroid/os/RemoteCallbackList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSimapCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Start broadcasting to callback. N="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->log(Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSimapCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/IBluetoothSimapCallback;

    const/4 v4, 0x0

    invoke-interface {v3, v2, v4}, Landroid/bluetooth/IBluetoothSimapCallback;->postEvent(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSimapCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    const-string v3, "End broadcasting to callback."

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->log(Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_2
    const-string v3, "For non-ui server enabling."

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->log(Ljava/lang/String;)V

    goto :goto_2

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method private native prepareListentoSocketNative()Z
.end method

.method private removeSimapAuthNotification(I)V
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    invoke-virtual {v1, p1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method private removeSimapConnNotification(I)V
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    invoke-virtual {v1, p1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method private native selectSIMNative(I)Z
.end method

.method private sendSIMUnaccessibleInd()V
    .locals 3

    const-string v0, "BluetoothSimapService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendSIMUnaccessibleInd...mState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const-string v0, "BluetoothSimapService"

    const-string v1, "mState != BluetoothSimap.STATE_CONNECTED, just return"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->sendSIMUnaccessibleIndNative()V

    goto :goto_0
.end method

.method private native sendSIMUnaccessibleIndNative()V
.end method

.method private sendServiceMsg(ILjava/lang/Object;)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/Object;

    const-string v1, "BluetoothSimapService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendServiceMsg: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSessionStatusHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private setState(I)V
    .locals 3
    .param p1    # I

    const-string v0, "BluetoothSimapService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setState(state): Simap state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->setState(II)V

    return-void
.end method

.method private declared-synchronized setState(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    const/4 v7, 0x2

    const/16 v6, 0xa

    monitor-enter p0

    :try_start_0
    iget v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    if-eq p1, v3, :cond_3

    const-string v3, "BluetoothSimapService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Simap state "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", result = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eq p1, v6, :cond_0

    iget v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    if-eq v3, v6, :cond_0

    iget v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    const/16 v4, 0xc

    if-ne v3, v4, :cond_1

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.bluetooth.profilemanager.action.PROFILE_CHANGED"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "android.bluetooth.profilemanager.extra.PROFILE"

    sget-object v4, Landroid/bluetooth/BluetoothProfileManager$Profile;->Bluetooth_SIMAP:Landroid/bluetooth/BluetoothProfileManager$Profile;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    if-ne p1, v6, :cond_4

    const/16 v1, 0xa

    :goto_0
    const-string v3, "android.bluetooth.profilemanager.extra.EXTRA_NEW_STATE"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "BluetoothSimapService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "send broadcast: simap Enabling/Disabling status: bpm_state = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "android.permission.BLUETOOTH"

    invoke-virtual {p0, v0, v3}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_1
    const/4 v3, 0x1

    if-eq p1, v3, :cond_2

    if-eq p1, v7, :cond_2

    const/4 v3, 0x4

    if-ne p1, v3, :cond_8

    :cond_2
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.bluetooth.profilemanager.action.STATE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "android.bluetooth.profilemanager.extra.PROFILE"

    sget-object v4, Landroid/bluetooth/BluetoothProfileManager$Profile;->Bluetooth_SIMAP:Landroid/bluetooth/BluetoothProfileManager$Profile;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v3, "android.bluetooth.profilemanager.extra.EXTRA_PREVIOUS_STATE"

    iget v4, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iput p1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    const-string v3, "android.bluetooth.profilemanager.extra.EXTRA_NEW_STATE"

    iget v4, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "android.bluetooth.device.extra.DEVICE"

    iget-object v4, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v3, "BluetoothSimapService"

    const-string v4, "send broadcast: simap state changed"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "android.permission.BLUETOOTH"

    invoke-virtual {p0, v2, v3}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    :goto_1
    monitor-exit p0

    return-void

    :cond_4
    :try_start_1
    iget v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    if-ne v3, v6, :cond_6

    if-ne p1, v7, :cond_5

    const/16 v1, 0xb

    goto :goto_0

    :cond_5
    const/16 v1, 0xe

    goto :goto_0

    :cond_6
    const/4 v3, -0x1

    if-ne p1, v3, :cond_7

    const/16 v1, 0xd

    goto :goto_0

    :cond_7
    const/16 v1, 0xe

    goto :goto_0

    :cond_8
    iput p1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private native startListenNative()Z
.end method

.method private native stopListenNative()V
.end method

.method private storeSettings(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "BluetoothSimapService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "storeSettings: enabled="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mSIMIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSIMIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mPreferences:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "simap_server_enable"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "simap_server_sim_index"

    iget v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSIMIndex:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    return-void
.end method

.method private updateDeviceName()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->sRemoteDeviceName:Ljava/lang/String;

    sget-object v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->sRemoteDeviceName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BluetoothSimapService"

    const-string v1, "mRemoteDevice.getName()return empty, use the default name"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f060136

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->sRemoteDeviceName:Ljava/lang/String;

    :cond_0
    const-string v0, "BluetoothSimapService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateDeviceName: sRemoteDeviceName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->sRemoteDeviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mState= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->createSimapAuthNotification(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->createSimapConnNotification()V

    goto :goto_0
.end method


# virtual methods
.method public disable()V
    .locals 3

    const-string v0, "BluetoothSimapService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "disable, mState= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "BluetoothSimapService"

    const-string v1, "disable: mState is already STATE_IDLE, just return!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->authorizeRsp(Z)V

    sget v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->NOTIFICATION_ID_ACCESS:I

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->removeSimapAuthNotification(I)V

    :cond_1
    :goto_1
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->setState(I)V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->disableNative()V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->IMMEDIATE_DISC_MODE:I

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->disconnectClient(I)V

    goto :goto_1
.end method

.method public enable()Z
    .locals 3

    const-string v0, "BluetoothSimapService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enable, mState= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->setState(I)V

    invoke-direct {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->enableNative()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->setState(I)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1    # Landroid/content/Intent;

    const-string v0, "BluetoothSimapService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enter onBind(): mBinder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mBinder:Landroid/bluetooth/IBluetoothSimap$Stub;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mBinder:Landroid/bluetooth/IBluetoothSimap$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    const-string v1, "BluetoothSimapService"

    const-string v2, "SIMAP: onCreate..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mServiceInitiated:Z

    if-eqz v1, :cond_0

    const-string v1, "BluetoothSimapService"

    const-string v2, "Already initiated, just return!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mInterrupted:Z

    const/4 v1, -0x1

    iput v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mState:I

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-direct {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->initServiceNative()Z

    move-result v1

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mServiceInitiated:Z

    const-string v1, "BluetoothSimapService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Service initiated: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mServiceInitiated:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.device.action.ACL_DISCONNECT_REQUESTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.mediatek.bluetooth.simap.accessdisallowed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.mediatek.bluetooth.simap.accessallowed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "simap_disconnect_request"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.mediatek.bluetooth.simap.intent.action.SEND_SIMUNAVALIBLE_IND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.bluetooth.device.action.NAME_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "BluetoothSimapService"

    const-string v1, "onDestroy..."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->disable()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->closeSimapService()V

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSimapCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    sput-boolean v2, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mHasStarted:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mStartId:I

    iput-boolean v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mServiceInitiated:Z

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v0, "BluetoothSimapService"

    const-string v1, "onStartCommand [+]"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mHasStarted:Z

    if-eqz v0, :cond_0

    const-string v0, "BluetoothSimapService"

    const-string v1, "Already started, just return!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v3

    :cond_0
    iget-boolean v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mServiceInitiated:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSocketListener:Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;

    if-nez v0, :cond_1

    new-instance v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;-><init>(Lcom/mediatek/bluetooth/simap/BluetoothSimapService;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSocketListener:Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSocketListener:Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;

    const-string v1, "SimapSocketListener"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSocketListener:Lcom/mediatek/bluetooth/simap/BluetoothSimapService$SimapSocketListener;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const-string v0, "BluetoothSimapService"

    const-string v1, "SimapSocketListener started."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v0, "simap_server_settings"

    invoke-virtual {p0, v0, v2}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mPreferences:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "simap_server_enable"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->savedEnableState:Z

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->savedEnableState:Z

    if-eqz v0, :cond_2

    const-string v0, "Pre-enable SIMAP Server..."

    invoke-static {v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->enable()Z

    :cond_2
    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "simap_server_sim_index"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSIMIndex:I

    const-string v0, "BluetoothSimapService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SIM card: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSIMIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mSIMIndex:I

    invoke-direct {p0, v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->selectSIMNative(I)Z

    iput p3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mStartId:I

    sput-boolean v3, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->mHasStarted:Z

    :goto_1
    const-string v0, "BluetoothSimapService"

    const-string v1, "onStartCommand [-]"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v0, "Failed to init BluetoothSimapService. Stop SIMAP service."

    invoke-static {v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapService;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    goto :goto_1
.end method
