.class Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;
.super Ljava/lang/Object;
.source "BluetoothSimapSettings.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;


# direct methods
.method constructor <init>(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v2, "BluetoothSimapSettings"

    const-string v3, "SIMAP: onServiceConnected[+]"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    invoke-static {p2}, Landroid/bluetooth/IBluetoothSimap$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothSimap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->access$102(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;Landroid/bluetooth/IBluetoothSimap;)Landroid/bluetooth/IBluetoothSimap;

    const-string v2, "BluetoothSimapSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSimapService = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    invoke-static {v4}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->access$100(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/bluetooth/IBluetoothSimap;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    invoke-static {v2}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->access$100(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/bluetooth/IBluetoothSimap;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v2, "BluetoothSimapSettings"

    const-string v3, " mSimapService == null !"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    invoke-static {v2}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->access$100(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/bluetooth/IBluetoothSimap;

    move-result-object v2

    invoke-interface {v2}, Landroid/bluetooth/IBluetoothSimap;->isServiceStarted()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "BluetoothSimapSettings"

    const-string v3, "BluetoothSimapService is NOT started yet, start it now "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    invoke-static {v2}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->access$100(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/bluetooth/IBluetoothSimap;

    move-result-object v2

    invoke-interface {v2}, Landroid/bluetooth/IBluetoothSimap;->startSimapService()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    :try_start_1
    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    invoke-static {v2}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->access$100(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/bluetooth/IBluetoothSimap;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    invoke-static {v3}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->access$200(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/bluetooth/IBluetoothSimapCallback;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/bluetooth/IBluetoothSimap;->registerCallback(Landroid/bluetooth/IBluetoothSimapCallback;)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    invoke-static {v2}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->access$100(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/bluetooth/IBluetoothSimap;

    move-result-object v2

    invoke-interface {v2}, Landroid/bluetooth/IBluetoothSimap;->getState()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    const/16 v2, 0xa

    if-eq v1, v2, :cond_2

    const-string v2, "BluetoothSimapSettings"

    const-string v3, "SIMAP server: setChecked(true)..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    invoke-static {v2}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->access$300(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    invoke-static {v2}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->access$300(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    invoke-static {v2}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->access$000(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const-string v2, "BluetoothSimapSettings"

    const-string v3, "SIMAP: onServiceConnected[-]"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "BluetoothSimapSettings"

    const-string v3, "startSimapService error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    :try_start_2
    const-string v2, "BluetoothSimapSettings"

    const-string v3, "SIMAP server: setChecked(false)..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    invoke-static {v2}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->access$300(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v2, "BluetoothSimapSettings"

    const-string v3, "getState error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;

    const/4 v2, 0x0

    const-string v0, "BluetoothSimapSettings"

    const-string v1, "Unexpectedly disconnected with BluetoothSimapService"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    invoke-static {v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->access$300(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;->this$0:Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    invoke-static {v0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->access$400(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/preference/PreferenceActivity;

    move-result-object v0

    const-string v1, "SIMAP Service disconnected unexpectedly."

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
