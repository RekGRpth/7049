.class public Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;
.super Ljava/lang/Object;
.source "BluetoothSimapSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/mediatek/activity/AssembledPreferenceActivity$AssemblyPreference;


# static fields
.field private static final KEY_PROFILE:Ljava/lang/String; = "profile_key_for_dialog"

.field private static final KEY_SHOW_DIALOG:Ljava/lang/String; = "show_alert_dialog"

.field private static final KEY_SIMAP_SERVER_CATEGORY:Ljava/lang/String; = "simap_server_category"

.field private static final KEY_SIMAP_SERVER_ENABLE:Ljava/lang/String; = "simap_server_enable"

.field private static final KEY_SIMAP_SERVER_SIM_INDEX:Ljava/lang/String; = "simap_server_sim_index"

.field private static final MAX_SIM_NUM:I = 0x2

.field private static final MSG_SIM_STATE_CHECK:I = 0x64

.field private static final PROFILE_BASE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "BluetoothSimapSettings"


# instance fields
.field private mDialog:Landroid/app/AlertDialog;

.field private mProfileKey:I

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mSimapCategory:Landroid/preference/PreferenceCategory;

.field private mSimapHandler:Landroid/os/Handler;

.field private mSimapServerConn:Landroid/content/ServiceConnection;

.field private mSimapServerEnable:Landroid/preference/CheckBoxPreference;

.field private mSimapServerSimIndex:Landroid/preference/ListPreference;

.field private mSimapService:Landroid/bluetooth/IBluetoothSimap;

.field private mSimapUICallback:Landroid/bluetooth/IBluetoothSimapCallback;

.field private parentActivity:Landroid/preference/PreferenceActivity;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mProfileKey:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapService:Landroid/bluetooth/IBluetoothSimap;

    new-instance v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$1;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$1;-><init>(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapUICallback:Landroid/bluetooth/IBluetoothSimapCallback;

    new-instance v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$2;-><init>(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerConn:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$3;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$3;-><init>(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$4;

    invoke-direct {v0, p0}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings$4;-><init>(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)V

    iput-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/bluetooth/IBluetoothSimap;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapService:Landroid/bluetooth/IBluetoothSimap;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;Landroid/bluetooth/IBluetoothSimap;)Landroid/bluetooth/IBluetoothSimap;
    .locals 0
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;
    .param p1    # Landroid/bluetooth/IBluetoothSimap;

    iput-object p1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapService:Landroid/bluetooth/IBluetoothSimap;

    return-object p1
.end method

.method static synthetic access$200(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/bluetooth/IBluetoothSimapCallback;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapUICallback:Landroid/bluetooth/IBluetoothSimapCallback;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerEnable:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;)Landroid/preference/PreferenceActivity;
    .locals 1
    .param p0    # Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    return-object v0
.end method

.method private isBtEnabled()Z
    .locals 2

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public getPreferenceResourceId()I
    .locals 1

    const v0, 0x7f040002

    return v0
.end method

.method public handleCheckSimState()V
    .locals 14

    const v7, 0x7f060146

    const v6, 0x7f060145

    const/4 v13, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    new-array v5, v12, [I

    fill-array-data v5, :array_0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v12, :cond_1

    invoke-virtual {p0, v1}, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->isSimExist(I)Z

    move-result v8

    if-ne v8, v11, :cond_0

    add-int/lit8 v4, v4, 0x1

    aput v11, v5, v1

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    aput v13, v5, v1

    goto :goto_1

    :cond_1
    const-string v8, "BluetoothSimapSettings"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "simNum = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v4, v12, :cond_5

    const/4 v3, 0x1

    :try_start_0
    iget-object v8, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapService:Landroid/bluetooth/IBluetoothSimap;

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapService:Landroid/bluetooth/IBluetoothSimap;

    invoke-interface {v8}, Landroid/bluetooth/IBluetoothSimap;->getSIMIndex()I

    move-result v3

    :goto_2
    const-string v8, "BluetoothSimapSettings"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "simIndex = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    iget-object v8, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerSimIndex:Landroid/preference/ListPreference;

    add-int/lit8 v9, v3, -0x1

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v8, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerSimIndex:Landroid/preference/ListPreference;

    if-ne v3, v11, :cond_4

    :goto_4
    invoke-virtual {v8, v6}, Landroid/preference/Preference;->setSummary(I)V

    iget-object v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerSimIndex:Landroid/preference/ListPreference;

    invoke-virtual {v6}, Landroid/preference/Preference;->isEnabled()Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerSimIndex:Landroid/preference/ListPreference;

    invoke-virtual {v6, v11}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_2
    :goto_5
    return-void

    :cond_3
    :try_start_1
    const-string v8, "BluetoothSimapSettings"

    const-string v9, "handleCheckSimState mSimapService = null!"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v8, "BluetoothSimapSettings"

    const-string v9, "getSIMIndex error"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_4
    move v6, v7

    goto :goto_4

    :cond_5
    if-ne v4, v11, :cond_9

    const/4 v1, 0x0

    :goto_6
    if-ge v1, v12, :cond_2

    aget v8, v5, v1

    if-ne v8, v11, :cond_8

    :try_start_2
    iget-object v8, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapService:Landroid/bluetooth/IBluetoothSimap;

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapService:Landroid/bluetooth/IBluetoothSimap;

    add-int/lit8 v9, v1, 0x1

    invoke-interface {v8, v9}, Landroid/bluetooth/IBluetoothSimap;->selectSIM(I)Z

    :goto_7
    const-string v8, "BluetoothSimapSettings"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "simIndex = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v10, v1, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_8
    iget-object v8, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerSimIndex:Landroid/preference/ListPreference;

    invoke-virtual {v8, v1}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v8, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerSimIndex:Landroid/preference/ListPreference;

    if-nez v1, :cond_7

    :goto_9
    invoke-virtual {v8, v6}, Landroid/preference/Preference;->setSummary(I)V

    iget-object v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerSimIndex:Landroid/preference/ListPreference;

    invoke-virtual {v6, v13}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_5

    :cond_6
    :try_start_3
    const-string v8, "BluetoothSimapSettings"

    const-string v9, "handleCheckSimState mSimapService = null!"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_7

    :catch_1
    move-exception v0

    const-string v8, "BluetoothSimapSettings"

    const-string v9, "getSIMIndex error"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    :cond_7
    move v6, v7

    goto :goto_9

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_9
    if-nez v4, :cond_2

    iget-object v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapCategory:Landroid/preference/PreferenceCategory;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerSimIndex:Landroid/preference/ListPreference;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapCategory:Landroid/preference/PreferenceCategory;

    iget-object v7, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerSimIndex:Landroid/preference/ListPreference;

    invoke-virtual {v6, v7}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    move-result v2

    const-string v6, "BluetoothSimapSettings"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SIMAP: removePreference mSimapServerSimIndex...return: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public isSimExist(I)Z
    .locals 6
    .param p1    # I

    const/4 v2, 0x1

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v0

    const-string v3, "BluetoothSimapSettings"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sim current state = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v0, v2, :cond_0

    const/4 v2, 0x0

    :cond_0
    return v2
.end method

.method public onCreate(Landroid/preference/PreferenceActivity;)V
    .locals 6
    .param p1    # Landroid/preference/PreferenceActivity;

    const-string v2, "BluetoothSimapSettings"

    const-string v3, "onCreate..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    const-string v3, "simap_server_enable"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    iput-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerEnable:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    const-string v3, "simap_server_sim_index"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/ListPreference;

    iput-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerSimIndex:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    const-string v3, "simap_server_category"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceCategory;

    iput-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapCategory:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerEnable:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerSimIndex:Landroid/preference/ListPreference;

    if-nez v2, :cond_1

    :cond_0
    const-string v2, "BluetoothSimapSettings"

    const-string v3, "[BT][SIMAP] Can\'t find SIMAP preferences."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerSimIndex:Landroid/preference/ListPreference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v2, "BluetoothSimapSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SIMAP bindService: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-class v4, Landroid/bluetooth/IBluetoothSimap;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    new-instance v3, Landroid/content/Intent;

    const-class v4, Landroid/bluetooth/IBluetoothSimap;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerConn:Landroid/content/ServiceConnection;

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v2, "BluetoothSimapSettings"

    const-string v3, "SIMAP Service binding failed."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v2, "android.intent.action.SIM_INDICATOR_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 5

    const-string v2, "BluetoothSimapSettings"

    const-string v3, "onDestroy()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mDialog:Landroid/app/AlertDialog;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :try_start_0
    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapService:Landroid/bluetooth/IBluetoothSimap;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapService:Landroid/bluetooth/IBluetoothSimap;

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapUICallback:Landroid/bluetooth/IBluetoothSimapCallback;

    invoke-interface {v2, v3}, Landroid/bluetooth/IBluetoothSimap;->unregisterCallback(Landroid/bluetooth/IBluetoothSimapCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->parentActivity:Landroid/preference/PreferenceActivity;

    iget-object v3, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerConn:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v3}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-void

    :cond_1
    :try_start_2
    const-string v2, "BluetoothSimapSettings"

    const-string v3, "unregisterCallback(mSimapUICallback) failed: null mSimapService."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "BluetoothSimapSettings"

    const-string v3, "unregisterCallback(mSimapUICallback) error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "BluetoothSimapSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[BT][SIMAP] Exception triggered when unbinding service: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 10
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v7, "BluetoothSimapSettings"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onPreferenceChange(), key: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "simap_server_enable"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapService:Landroid/bluetooth/IBluetoothSimap;

    if-nez v6, :cond_0

    const-string v6, "BluetoothSimapSettings"

    const-string v7, "mSimapService is empty."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v5

    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerEnable:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6}, Landroid/preference/TwoStatePreference;->isChecked()Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapService:Landroid/bluetooth/IBluetoothSimap;

    invoke-interface {v6}, Landroid/bluetooth/IBluetoothSimap;->enableService()Z

    const-string v6, "BluetoothSimapSettings"

    const-string v7, "Enable the UI, waiting for SIMAP enable result."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerEnable:Landroid/preference/CheckBoxPreference;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setEnabled(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v6, "BluetoothSimapSettings"

    const-string v7, "Enable/disable SIMAP server failed."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapService:Landroid/bluetooth/IBluetoothSimap;

    invoke-interface {v6}, Landroid/bluetooth/IBluetoothSimap;->disableService()V

    const-string v6, "BluetoothSimapSettings"

    const-string v7, "Disable the UI, waiting for SIMAP disable result."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerEnable:Landroid/preference/CheckBoxPreference;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/preference/Preference;->setEnabled(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :cond_2
    const-string v5, "simap_server_sim_index"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    :try_start_2
    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const-string v5, "BluetoothSimapSettings"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SIMAP Server SIM index Changed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapService:Landroid/bluetooth/IBluetoothSimap;

    invoke-interface {v5, v4}, Landroid/bluetooth/IBluetoothSimap;->selectSIM(I)Z

    iget-object v7, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerSimIndex:Landroid/preference/ListPreference;

    if-ne v4, v6, :cond_4

    const v5, 0x7f060145

    :goto_1
    invoke-virtual {v7, v5}, Landroid/preference/Preference;->setSummary(I)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_3
    :goto_2
    move v5, v6

    goto :goto_0

    :cond_4
    const v5, 0x7f060146

    goto :goto_1

    :catch_1
    move-exception v1

    const-string v5, "BluetoothSimapSettings"

    const-string v7, "Could not parse SIMAP Server SIM index value."

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Landroid/preference/Preference;

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mSimapServerEnable:Landroid/preference/CheckBoxPreference;

    if-ne p2, v1, :cond_0

    const-string v1, "BluetoothSimapSettings"

    const-string v2, "SIMAP Server Checkbox is Clicked."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x0

    const-string v0, "profile_key_for_dialog"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mProfileKey:I

    const-string v0, "show_alert_dialog"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mProfileKey:I

    if-eqz v0, :cond_0

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    const-string v0, "BluetoothSimapSettings"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    const-string v0, "show_alert_dialog"

    iget-object v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "profile_key_for_dialog"

    iget v1, p0, Lcom/mediatek/bluetooth/simap/BluetoothSimapSettings;->mProfileKey:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method
