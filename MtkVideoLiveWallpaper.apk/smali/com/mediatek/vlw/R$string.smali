.class public final Lcom/mediatek/vlw/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/vlw/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final VideoEditor_error_button:I = 0x7f08000c

.field public static final VideoEditor_error_no_video_selected:I = 0x7f08000d

.field public static final VideoEditor_error_title:I = 0x7f08000b

.field public static final VideoEditor_warning_limited_duration:I = 0x7f08000e

.field public static final VideoScene_error_be_set:I = 0x7f08000a

.field public static final VideoScene_error_media_not_found:I = 0x7f080009

.field public static final VideoScene_error_media_removed:I = 0x7f080006

.field public static final VideoScene_error_sdcard_unmounted:I = 0x7f080007

.field public static final VideoScene_error_text_invalid_progressive_playback:I = 0x7f080004

.field public static final VideoScene_error_text_unknown:I = 0x7f080005

.field public static final VideoScene_info_wait_sdcard:I = 0x7f080008

.field public static final VideoScene_reload_after_mount:I = 0x7f08000f

.field public static final capture_video:I = 0x7f080018

.field public static final default_title:I = 0x7f080015

.field public static final default_video_path:I = 0x7f080016

.field public static final default_video_title:I = 0x7f080017

.field public static final folder_info:I = 0x7f08001c

.field public static final group_folder:I = 0x7f08001b

.field public static final group_video:I = 0x7f08001a

.field public static final menu_item_add_item:I = 0x7f080019

.field public static final select:I = 0x7f080012

.field public static final select_default:I = 0x7f080011

.field public static final select_video:I = 0x7f080010

.field public static final set_wallpaper:I = 0x7f080013

.field public static final title:I = 0x7f080014

.field public static final vlw:I = 0x7f080001

.field public static final vlw_author:I = 0x7f080002

.field public static final vlw_desc:I = 0x7f080003

.field public static final wallpapers:I = 0x7f080000


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
