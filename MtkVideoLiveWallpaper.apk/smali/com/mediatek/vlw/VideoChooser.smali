.class public Lcom/mediatek/vlw/VideoChooser;
.super Landroid/app/Activity;
.source "VideoChooser.java"


# static fields
.field private static final COVER_EXTEND_IMAGE:F = 0.5f

.field private static final COVER_MULTIPLE_IMAGE:F = 0.25f

.field private static final DEBUG:Z = false

.field static final DEFAULT_SCALE:F = 1.0f

.field private static final IMAGE_REFLECTION:F = 0.25f

.field private static final MAX_VIDEO_COUNT:I = 0x4

.field private static final MAX_ZOOM_OUT:F = 350.0f

.field private static final PATH:Ljava/lang/String; = "/raw/"

.field private static final SCHEMA:Ljava/lang/String; = "android.resource://"

.field private static final TAG:Ljava/lang/String; = "VideoChooser"


# instance fields
.field private mAdapter:Landroid/widget/BookmarkAdapter;

.field private mBookmark:Landroid/widget/BookmarkView;

.field private mBookmarkItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/BookmarkItem;",
            ">;"
        }
    .end annotation
.end field

.field private mImgHeight:I

.field private mImgWidth:I

.field private final mLock:Ljava/lang/Object;

.field private final mSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mSelectedPos:I

.field private final mTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mThumbIndex:I

.field private mUris:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mediatek/vlw/VideoChooser;->mLock:Ljava/lang/Object;

    new-instance v0, Lcom/mediatek/vlw/VideoChooser$1;

    invoke-direct {v0, p0}, Lcom/mediatek/vlw/VideoChooser$1;-><init>(Lcom/mediatek/vlw/VideoChooser;)V

    iput-object v0, p0, Lcom/mediatek/vlw/VideoChooser;->mTask:Landroid/os/AsyncTask;

    new-instance v0, Lcom/mediatek/vlw/VideoChooser$2;

    invoke-direct {v0, p0}, Lcom/mediatek/vlw/VideoChooser$2;-><init>(Lcom/mediatek/vlw/VideoChooser;)V

    iput-object v0, p0, Lcom/mediatek/vlw/VideoChooser;->mSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/vlw/VideoChooser;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoChooser;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoChooser;->mUris:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/vlw/VideoChooser;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoChooser;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoChooser;->mBookmarkItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/vlw/VideoChooser;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoChooser;

    iget v0, p0, Lcom/mediatek/vlw/VideoChooser;->mImgWidth:I

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/vlw/VideoChooser;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoChooser;

    iget v0, p0, Lcom/mediatek/vlw/VideoChooser;->mImgHeight:I

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/vlw/VideoChooser;Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoChooser;
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/vlw/VideoChooser;->createVideoThumbnail(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$502(Lcom/mediatek/vlw/VideoChooser;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoChooser;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoChooser;->mThumbIndex:I

    return p1
.end method

.method static synthetic access$600(Lcom/mediatek/vlw/VideoChooser;)Landroid/widget/BookmarkAdapter;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoChooser;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoChooser;->mAdapter:Landroid/widget/BookmarkAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/vlw/VideoChooser;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoChooser;

    iget v0, p0, Lcom/mediatek/vlw/VideoChooser;->mSelectedPos:I

    return v0
.end method

.method static synthetic access$702(Lcom/mediatek/vlw/VideoChooser;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoChooser;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoChooser;->mSelectedPos:I

    return p1
.end method

.method static synthetic access$800(Lcom/mediatek/vlw/VideoChooser;)Landroid/widget/BookmarkView;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoChooser;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoChooser;->mBookmark:Landroid/widget/BookmarkView;

    return-object v0
.end method

.method private addVideo(Landroid/content/res/Resources;Ljava/lang/String;I)V
    .locals 10
    .param p1    # Landroid/content/res/Resources;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "android.resource://"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/raw/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v5, 0x0

    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    if-eqz v1, :cond_0

    invoke-virtual {v7, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v8, p0, Lcom/mediatek/vlw/VideoChooser;->mUris:Ljava/util/ArrayList;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private createVideoThumbnail(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 10
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # I
    .param p3    # I

    const/4 v9, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    sget v7, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    invoke-virtual {p1, v7}, Landroid/graphics/Bitmap;->setDensity(I)V

    sget-object v7, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    sget v7, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    invoke-virtual {v3, v7}, Landroid/graphics/Bitmap;->setDensity(I)V

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    sget v7, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    invoke-virtual {v0, v7}, Landroid/graphics/Canvas;->setDensity(I)V

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    iput v9, v6, Landroid/graphics/Rect;->left:I

    iput v9, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    iput v7, v6, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    iput v7, v6, Landroid/graphics/Rect;->bottom:I

    iget v7, v6, Landroid/graphics/Rect;->right:I

    sub-int v2, p2, v7

    iget v7, v6, Landroid/graphics/Rect;->bottom:I

    sub-int v1, p3, v7

    const/high16 v5, 0x3f800000

    if-gtz v2, :cond_1

    if-lez v1, :cond_3

    :cond_1
    if-le v2, v1, :cond_2

    int-to-float v7, p2

    iget v8, v6, Landroid/graphics/Rect;->right:I

    int-to-float v8, v8

    div-float v5, v7, v8

    :goto_1
    iget v7, v6, Landroid/graphics/Rect;->right:I

    int-to-float v7, v7

    mul-float/2addr v7, v5

    float-to-int v7, v7

    iput v7, v6, Landroid/graphics/Rect;->right:I

    iget v7, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    mul-float/2addr v7, v5

    float-to-int v7, v7

    iput v7, v6, Landroid/graphics/Rect;->bottom:I

    iget v7, v6, Landroid/graphics/Rect;->right:I

    sub-int v2, p2, v7

    iget v7, v6, Landroid/graphics/Rect;->bottom:I

    sub-int v1, p3, v7

    div-int/lit8 v7, v2, 0x2

    div-int/lit8 v8, v1, 0x2

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->offset(II)V

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/graphics/Paint;->setDither(Z)V

    const/4 v7, 0x0

    invoke-virtual {v0, p1, v7, v6, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    move-object p1, v3

    goto :goto_0

    :cond_2
    int-to-float v7, p3

    iget v8, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v8, v8

    div-float v5, v7, v8

    goto :goto_1

    :cond_3
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v7

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v8

    if-ge v7, v8, :cond_4

    int-to-float v7, p2

    iget v8, v6, Landroid/graphics/Rect;->right:I

    int-to-float v8, v8

    div-float v5, v7, v8

    goto :goto_1

    :cond_4
    int-to-float v7, p3

    iget v8, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v8, v8

    div-float v5, v7, v8

    goto :goto_1
.end method

.method private findVideo()V
    .locals 10

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/high16 v8, 0x7f060000

    invoke-direct {p0, v5, v4, v8}, Lcom/mediatek/vlw/VideoChooser;->addVideo(Landroid/content/res/Resources;Ljava/lang/String;I)V

    const v8, 0x7f060001

    invoke-direct {p0, v5, v4, v8}, Lcom/mediatek/vlw/VideoChooser;->addVideo(Landroid/content/res/Resources;Ljava/lang/String;I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020009

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080017

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/vlw/VideoChooser;->mUris:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v6, :cond_0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v9, v1, 0x1

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/widget/BookmarkItem;

    invoke-direct {v3, v0, v7, v2}, Landroid/widget/BookmarkItem;-><init>(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/vlw/VideoChooser;->mBookmarkItems:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private initViews()V
    .locals 5

    const/high16 v4, 0x3e800000

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/mediatek/vlw/VideoChooser;->mUris:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/mediatek/vlw/VideoChooser;->mBookmarkItems:Ljava/util/ArrayList;

    const v1, 0x7f0a0002

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/BookmarkView;

    iput-object v1, p0, Lcom/mediatek/vlw/VideoChooser;->mBookmark:Landroid/widget/BookmarkView;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f070000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/vlw/VideoChooser;->mImgWidth:I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/mediatek/vlw/VideoChooser;->mImgHeight:I

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoChooser;->findVideo()V

    new-instance v1, Landroid/widget/BookmarkAdapter;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoChooser;->mBookmarkItems:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2}, Landroid/widget/BookmarkAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v1, p0, Lcom/mediatek/vlw/VideoChooser;->mAdapter:Landroid/widget/BookmarkAdapter;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoChooser;->mBookmark:Landroid/widget/BookmarkView;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoChooser;->mAdapter:Landroid/widget/BookmarkAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/BookmarkView;->setBookmarkAdapter(Landroid/widget/BookmarkAdapter;)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoChooser;->mBookmark:Landroid/widget/BookmarkView;

    invoke-virtual {v1}, Landroid/widget/BookmarkView;->getTitleView()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoChooser;->mBookmark:Landroid/widget/BookmarkView;

    iget v2, p0, Lcom/mediatek/vlw/VideoChooser;->mImgWidth:I

    iget v3, p0, Lcom/mediatek/vlw/VideoChooser;->mImgHeight:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/BookmarkView;->setImageDispSize(II)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoChooser;->mBookmark:Landroid/widget/BookmarkView;

    iget v2, p0, Lcom/mediatek/vlw/VideoChooser;->mImgWidth:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    const/high16 v3, 0x3f000000

    add-float/2addr v2, v3

    float-to-int v2, v2

    neg-int v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/BookmarkView;->setCoverFlowSpacing(I)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoChooser;->mBookmark:Landroid/widget/BookmarkView;

    invoke-virtual {v1, v4}, Landroid/widget/BookmarkView;->setImageReflection(F)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoChooser;->mBookmark:Landroid/widget/BookmarkView;

    invoke-virtual {v1}, Landroid/widget/BookmarkView;->getCoverFlow()Landroid/widget/BounceCoverFlow;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/vlw/VideoChooser;->mSelectedPos:I

    invoke-virtual {v0, v1}, Landroid/widget/AbsSpinner;->setSelection(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setEmptyView(Landroid/view/View;)V

    const/high16 v1, 0x43af0000

    invoke-virtual {v0, v1}, Landroid/widget/BounceCoverFlow;->setMaxZoomOut(F)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoChooser;->mSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoChooser;->mTask:Landroid/os/AsyncTask;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public getRetrievedThumbIndex()I
    .locals 1

    iget v0, p0, Lcom/mediatek/vlw/VideoChooser;->mThumbIndex:I

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoChooser;->initViews()V

    return-void
.end method

.method protected onDestroy()V
    .locals 5

    iget-object v3, p0, Lcom/mediatek/vlw/VideoChooser;->mTask:Landroid/os/AsyncTask;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/AsyncTask;->cancel(Z)Z

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/mediatek/vlw/VideoChooser;->mBookmarkItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/BookmarkItem;

    invoke-virtual {v2}, Landroid/widget/BookmarkItem;->getContentBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public selectVideo(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/mediatek/vlw/VideoChooser;->mUris:Ljava/util/ArrayList;

    iget v3, p0, Lcom/mediatek/vlw/VideoChooser;->mSelectedPos:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/4 v2, -0x1

    invoke-virtual {p0, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    iget-object v2, p0, Lcom/mediatek/vlw/VideoChooser;->mTask:Landroid/os/AsyncTask;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/AsyncTask;->cancel(Z)Z

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method
