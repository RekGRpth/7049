.class Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;
.super Landroid/service/wallpaper/WallpaperService$Engine;
.source "VideoLiveWallpaper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/vlw/VideoLiveWallpaper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VLWEngine"
.end annotation


# instance fields
.field private mRenderer:Lcom/mediatek/vlw/VideoScene;

.field private mVisible:Z

.field final synthetic this$0:Lcom/mediatek/vlw/VideoLiveWallpaper;


# direct methods
.method private constructor <init>(Lcom/mediatek/vlw/VideoLiveWallpaper;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->this$0:Lcom/mediatek/vlw/VideoLiveWallpaper;

    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;-><init>(Landroid/service/wallpaper/WallpaperService;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/vlw/VideoLiveWallpaper;Lcom/mediatek/vlw/VideoLiveWallpaper$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/vlw/VideoLiveWallpaper;
    .param p2    # Lcom/mediatek/vlw/VideoLiveWallpaper$1;

    invoke-direct {p0, p1}, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;-><init>(Lcom/mediatek/vlw/VideoLiveWallpaper;)V

    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/os/Bundle;
    .param p6    # Z

    const-string v0, "hide"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v0}, Lcom/mediatek/vlw/VideoScene;->pause()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/mediatek/vlw/VideoScene;->doCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    const-string v0, "show"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v0}, Lcom/mediatek/vlw/VideoScene;->start()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onCreate(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onCreate(Landroid/view/SurfaceHolder;)V

    const-string v0, "VideoLiveWallpaper"

    const-string v1, "create wallpaper engine"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->setTouchEventsEnabled(Z)V

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->setSizeFromLayout()V

    return-void
.end method

.method public onDesiredSizeChanged(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const-string v0, "VideoLiveWallpaper"

    const-string v1, "The desired size is: desiredWidth=%d, desiredHeight=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->onDestroy()V

    const-string v0, "VideoLiveWallpaper"

    const-string v1, "destroy wallpaper engine"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v0}, Lcom/mediatek/vlw/VideoScene;->destroy()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    return-void
.end method

.method public onSurfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 5
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceChanged(Landroid/view/SurfaceHolder;III)V

    const-string v0, "VideoLiveWallpaper"

    const-string v1, "surface changed width=%d, height=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v0, p1, p3, p4}, Lcom/mediatek/vlw/VideoScene;->resize(Landroid/view/SurfaceHolder;II)V

    :cond_0
    return-void
.end method

.method public onSurfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 4
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceCreated(Landroid/view/SurfaceHolder;)V

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v1, "VideoLiveWallpaper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSurfaceCreated(), surface: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    if-nez v1, :cond_1

    new-instance v1, Lcom/mediatek/vlw/VideoScene;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->this$0:Lcom/mediatek/vlw/VideoLiveWallpaper;

    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->isPreview()Z

    move-result v3

    invoke-direct {v1, v2, p1, v3}, Lcom/mediatek/vlw/VideoScene;-><init>(Landroid/content/Context;Landroid/view/SurfaceHolder;Z)V

    iput-object v1, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v1, p1}, Lcom/mediatek/vlw/VideoScene;->init(Landroid/view/SurfaceHolder;)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v1}, Lcom/mediatek/vlw/VideoScene;->start()V

    :cond_1
    return-void
.end method

.method public onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V

    const-string v0, "VideoLiveWallpaper"

    const-string v1, "onSurfaceDestroyed()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v0}, Lcom/mediatek/vlw/VideoScene;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    :cond_0
    return-void
.end method

.method public onVisibilityChanged(Z)V
    .locals 4
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onVisibilityChanged(Z)V

    iget-boolean v1, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mVisible:Z

    if-eq p1, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "VideoLiveWallpaper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "visibleChange="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", visible="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iput-boolean p1, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mVisible:Z

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v1}, Lcom/mediatek/vlw/VideoScene;->start()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/vlw/VideoLiveWallpaper$VLWEngine;->mRenderer:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v1}, Lcom/mediatek/vlw/VideoScene;->pause()V

    goto :goto_1
.end method
