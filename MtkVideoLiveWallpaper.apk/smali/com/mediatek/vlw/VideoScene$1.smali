.class Lcom/mediatek/vlw/VideoScene$1;
.super Landroid/os/Handler;
.source "VideoScene.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/vlw/VideoScene;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/vlw/VideoScene;


# direct methods
.method constructor <init>(Lcom/mediatek/vlw/VideoScene;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1    # Landroid/os/Message;

    const-wide/16 v11, 0x3e8

    const/4 v10, 0x5

    const/4 v7, 0x4

    const/4 v8, 0x0

    const/4 v9, 0x1

    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    const-string v6, "VideoScene"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "unknown message "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_1
    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v6}, Lcom/mediatek/vlw/VideoScene;->getCurrentPosition()I

    move-result v6

    int-to-long v3, v6

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v6}, Lcom/mediatek/vlw/VideoScene;->getDuration()I

    move-result v6

    int-to-long v0, v6

    const-wide/16 v6, -0x1

    cmp-long v6, v0, v6

    if-nez v6, :cond_1

    invoke-virtual {p0, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {p0, v2, v11, v12}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_1
    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6}, Lcom/mediatek/vlw/VideoScene;->access$000(Lcom/mediatek/vlw/VideoScene;)I

    move-result v6

    int-to-long v6, v6

    cmp-long v6, v6, v0

    if-gez v6, :cond_3

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6}, Lcom/mediatek/vlw/VideoScene;->access$000(Lcom/mediatek/vlw/VideoScene;)I

    move-result v6

    int-to-long v6, v6

    cmp-long v6, v3, v6

    if-lez v6, :cond_2

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v7}, Lcom/mediatek/vlw/VideoScene;->access$100(Lcom/mediatek/vlw/VideoScene;)I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {v6, v7, v8}, Lcom/mediatek/vlw/VideoScene;->seekTo(J)V

    :cond_2
    invoke-virtual {p0, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {p0, v2, v11, v12}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v9}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    :pswitch_2
    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6, v8}, Lcom/mediatek/vlw/VideoScene;->access$200(Lcom/mediatek/vlw/VideoScene;Z)V

    const/4 v6, 0x3

    invoke-virtual {p0, v6}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    :pswitch_3
    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {p0, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v6, 0x7d0

    invoke-virtual {p0, v2, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const-string v6, "VideoScene"

    const-string v7, "Cannot query video path, reload it in 2sec"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6, v8}, Lcom/mediatek/vlw/VideoScene;->access$402(Lcom/mediatek/vlw/VideoScene;Z)Z

    invoke-virtual {p0, v7}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0, v10}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6}, Lcom/mediatek/vlw/VideoScene;->access$500(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v7}, Lcom/mediatek/vlw/VideoScene;->access$700(Lcom/mediatek/vlw/VideoScene;)I

    move-result v7

    int-to-long v7, v7

    invoke-static {v6, v7, v8}, Lcom/mediatek/vlw/VideoScene;->access$602(Lcom/mediatek/vlw/VideoScene;J)J

    :cond_5
    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6}, Lcom/mediatek/vlw/VideoScene;->access$800(Lcom/mediatek/vlw/VideoScene;)V

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6}, Lcom/mediatek/vlw/VideoScene;->access$900(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v6}, Lcom/mediatek/vlw/VideoScene;->start()V

    goto/16 :goto_0

    :pswitch_4
    invoke-virtual {p0, v10}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0, v7}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6}, Lcom/mediatek/vlw/VideoScene;->access$400(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "VideoScene"

    const-string v7, "MSG_CLEAR sdcard removed, play default video"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6, v8}, Lcom/mediatek/vlw/VideoScene;->access$402(Lcom/mediatek/vlw/VideoScene;Z)Z

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6}, Lcom/mediatek/vlw/VideoScene;->access$1000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f080005

    invoke-static {v6, v7, v9}, Lcom/mediatek/vlw/Utils;->showInfo(Landroid/content/Context;IZ)V

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6, v8}, Lcom/mediatek/vlw/VideoScene;->access$200(Lcom/mediatek/vlw/VideoScene;Z)V

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6, v9, v9}, Lcom/mediatek/vlw/VideoScene;->access$1100(Lcom/mediatek/vlw/VideoScene;ZZ)V

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6}, Lcom/mediatek/vlw/VideoScene;->access$800(Lcom/mediatek/vlw/VideoScene;)V

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6}, Lcom/mediatek/vlw/VideoScene;->access$900(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v6}, Lcom/mediatek/vlw/VideoScene;->start()V

    goto/16 :goto_0

    :pswitch_5
    const-string v6, "VideoScene"

    const-string v7, "find the invalid video"

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$1;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6}, Lcom/mediatek/vlw/VideoScene;->access$1200(Lcom/mediatek/vlw/VideoScene;)V

    invoke-virtual {p0, v8}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
