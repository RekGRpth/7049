.class Lcom/mediatek/vlw/VideoScene$2;
.super Landroid/content/BroadcastReceiver;
.source "VideoScene.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/vlw/VideoScene;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/vlw/VideoScene;


# direct methods
.method constructor <init>(Lcom/mediatek/vlw/VideoScene;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene$2;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v4, 0x5

    const/4 v3, 0x1

    const-string v0, "VideoScene"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mShutdownReceiver intent = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "android.intent.action.REBOOT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "android.intent.action.ACTION_PRE_SHUTDOWN"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const-string v0, "VideoScene"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "shutdown: ACTION_SHUTDOWN, mUri = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene$2;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v2}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", intent.getAction() == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene$2;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v0}, Lcom/mediatek/vlw/VideoScene;->access$1300(Lcom/mediatek/vlw/VideoScene;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene$2;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v0, v3}, Lcom/mediatek/vlw/VideoScene;->access$1402(Lcom/mediatek/vlw/VideoScene;Z)Z

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene$2;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v0, v3}, Lcom/mediatek/vlw/VideoScene;->access$1502(Lcom/mediatek/vlw/VideoScene;Z)Z

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene$2;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v0}, Lcom/mediatek/vlw/VideoScene;->access$1600(Lcom/mediatek/vlw/VideoScene;)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene$2;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v0, v3}, Lcom/mediatek/vlw/VideoScene;->access$200(Lcom/mediatek/vlw/VideoScene;Z)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene$2;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v0}, Lcom/mediatek/vlw/VideoScene;->access$1700(Lcom/mediatek/vlw/VideoScene;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "android.intent.action.ACTION_BOOT_IPO"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene$2;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v0, v3}, Lcom/mediatek/vlw/VideoScene;->access$1502(Lcom/mediatek/vlw/VideoScene;Z)Z

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene$2;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/vlw/VideoScene;->access$1402(Lcom/mediatek/vlw/VideoScene;Z)Z

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene$2;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v0}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/vlw/Utils;->isDefaultVideo(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene$2;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v0}, Lcom/mediatek/vlw/VideoScene;->access$1800(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene$2;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v0}, Lcom/mediatek/vlw/VideoScene;->access$1300(Lcom/mediatek/vlw/VideoScene;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene$2;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v0}, Lcom/mediatek/vlw/VideoScene;->access$1300(Lcom/mediatek/vlw/VideoScene;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x7530

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
