.class Lcom/mediatek/vlw/VideoScene$3;
.super Landroid/content/BroadcastReceiver;
.source "VideoScene.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/vlw/VideoScene;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/vlw/VideoScene;


# direct methods
.method constructor <init>(Lcom/mediatek/vlw/VideoScene;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v10, "storage_volume"

    invoke-virtual {p2, v10}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/storage/StorageVolume;

    const/4 v4, 0x0

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v4

    :cond_0
    if-nez v4, :cond_1

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v10

    const-string v11, "file"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    :cond_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v10, "VideoScene"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Receive intent action="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " path="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v10, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10, v4}, Lcom/mediatek/vlw/VideoScene;->access$1900(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)V

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2000(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v10

    if-nez v10, :cond_2

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    new-instance v11, Ljava/util/ArrayList;

    const/4 v12, 0x2

    invoke-direct {v11, v12}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2002(Lcom/mediatek/vlw/VideoScene;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    :cond_2
    if-eqz v4, :cond_3

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2000(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2000(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2100(Lcom/mediatek/vlw/VideoScene;)Landroid/media/MediaPlayer;

    move-result-object v10

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2100(Lcom/mediatek/vlw/VideoScene;)Landroid/media/MediaPlayer;

    move-result-object v10

    invoke-virtual {v10}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v10

    if-nez v10, :cond_7

    :cond_4
    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$1500(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v10

    if-eqz v10, :cond_7

    if-eqz v4, :cond_7

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_7

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v10

    invoke-static {v10}, Lcom/mediatek/vlw/Utils;->isDefaultVideo(Landroid/net/Uri;)Z

    move-result v10

    if-nez v10, :cond_6

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$1300(Lcom/mediatek/vlw/VideoScene;)Landroid/os/Handler;

    move-result-object v10

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v10

    if-nez v10, :cond_5

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$1300(Lcom/mediatek/vlw/VideoScene;)Landroid/os/Handler;

    move-result-object v10

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_5
    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$402(Lcom/mediatek/vlw/VideoScene;Z)Z

    :cond_6
    :goto_0
    return-void

    :cond_7
    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2200(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v10

    if-eqz v10, :cond_6

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2200(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2200(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v10

    invoke-static {v10}, Lcom/mediatek/vlw/Utils;->swapSdcardUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2300(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_d

    const/4 v10, 0x0

    const-string v11, "/"

    invoke-virtual {v9, v11}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/4 v10, 0x0

    const-string v11, "/"

    invoke-virtual {v8, v11}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v8, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v1}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_8

    invoke-static {v7}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_8

    const-string v10, "VideoScene"

    const-string v11, "The video belonging sdcard unmounted"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_8
    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2300(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2402(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v9}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_b

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2200(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$302(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2500(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$102(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2600(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$002(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2700(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$702(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2900(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2802(Lcom/mediatek/vlw/VideoScene;I)I

    :goto_1
    const-string v10, "VideoScene"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Restore the video last time. mPrevUri="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v12}, Lcom/mediatek/vlw/VideoScene;->access$2200(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " mPrevBucketId="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v12}, Lcom/mediatek/vlw/VideoScene;->access$2300(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " mPrevStartTime="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v12}, Lcom/mediatek/vlw/VideoScene;->access$2500(Lcom/mediatek/vlw/VideoScene;)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " mPrevEndTime="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v12}, Lcom/mediatek/vlw/VideoScene;->access$2600(Lcom/mediatek/vlw/VideoScene;)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " mPrevCurrentPos="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v12}, Lcom/mediatek/vlw/VideoScene;->access$2700(Lcom/mediatek/vlw/VideoScene;)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2202(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2302(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)Ljava/lang/String;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$3000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/SharedPreferences;

    move-result-object v10

    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v10, "bucketId"

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v3, v10, v11}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v10, "uri"

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v3, v10, v11}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v10, "pos"

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$700(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    int-to-long v11, v11

    invoke-interface {v3, v10, v11, v12}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v10, "stereo"

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2800(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    int-to-long v11, v11

    invoke-interface {v3, v10, v11, v12}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_9

    const-string v10, "start"

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$100(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    int-to-long v11, v11

    invoke-interface {v3, v10, v11, v12}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v10, "end"

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$000(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    int-to-long v11, v11

    invoke-interface {v3, v10, v11, v12}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :cond_9
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$500(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v10

    if-nez v10, :cond_a

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$700(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    int-to-long v11, v11

    invoke-static {v10, v11, v12}, Lcom/mediatek/vlw/VideoScene;->access$602(Lcom/mediatek/vlw/VideoScene;J)J

    :cond_a
    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$800(Lcom/mediatek/vlw/VideoScene;)V

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$1402(Lcom/mediatek/vlw/VideoScene;Z)Z

    goto/16 :goto_0

    :cond_b
    invoke-static {v8}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_c

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v11}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$302(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2500(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$102(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2600(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$002(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2700(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$702(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2900(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2802(Lcom/mediatek/vlw/VideoScene;I)I

    goto/16 :goto_1

    :cond_c
    const-string v10, "VideoScene"

    const-string v11, "cannot reload videos selected last time"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_d
    invoke-static {v9}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_e

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2200(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$302(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2402(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)Ljava/lang/String;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2500(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$102(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2600(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$002(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2700(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$702(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2900(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2802(Lcom/mediatek/vlw/VideoScene;I)I

    goto/16 :goto_1

    :cond_e
    invoke-static {v8}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_f

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v11}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$302(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2402(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)Ljava/lang/String;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2500(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$102(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2600(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$002(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2700(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$702(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2900(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2802(Lcom/mediatek/vlw/VideoScene;I)I

    goto/16 :goto_1

    :cond_f
    const-string v10, "VideoScene"

    const-string v11, "video file selected last time does not exists"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_10
    const-string v10, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_11

    const-string v10, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_11

    const-string v10, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_11

    const-string v10, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_14

    :cond_11
    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$1400(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v10

    if-eqz v10, :cond_12

    const-string v10, "VideoScene"

    const-string v11, "Has been shutdown, Ignore"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_12
    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2000(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v10

    if-eqz v10, :cond_13

    if-eqz v4, :cond_13

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2000(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_13

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2000(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_13
    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_6

    if-eqz v4, :cond_6

    invoke-virtual {v9, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_6

    const-string v10, "VideoScene"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "action: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " revert to default video. sdcard path: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " absolute path: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " mUri: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v12}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$1000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/Context;

    move-result-object v10

    const v11, 0x7f080007

    const/4 v12, 0x1

    invoke-static {v10, v11, v12}, Lcom/mediatek/vlw/Utils;->showInfo(Landroid/content/Context;IZ)V

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2302(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)Ljava/lang/String;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2202(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$100(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2502(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$000(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2602(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v11}, Lcom/mediatek/vlw/VideoScene;->getCurrentPosition()I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2702(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2800(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$2902(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$1700(Lcom/mediatek/vlw/VideoScene;)V

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$200(Lcom/mediatek/vlw/VideoScene;Z)V

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v11, 0x1

    const/4 v12, 0x1

    invoke-static {v10, v11, v12}, Lcom/mediatek/vlw/VideoScene;->access$1100(Lcom/mediatek/vlw/VideoScene;ZZ)V

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$800(Lcom/mediatek/vlw/VideoScene;)V

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$900(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v10

    if-eqz v10, :cond_6

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v10}, Lcom/mediatek/vlw/VideoScene;->start()V

    goto/16 :goto_0

    :cond_14
    const-string v10, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_6

    invoke-virtual {v9, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_6

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_15

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$3100(Lcom/mediatek/vlw/VideoScene;)Z

    :cond_15
    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$1000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/Context;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/vlw/Utils;->queryStereoType(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v5

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$2800(Lcom/mediatek/vlw/VideoScene;)I

    move-result v10

    if-eq v5, v10, :cond_6

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10, v5}, Lcom/mediatek/vlw/VideoScene;->access$2802(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/mediatek/vlw/VideoScene;->access$3200(Lcom/mediatek/vlw/VideoScene;Z)V

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v10}, Lcom/mediatek/vlw/VideoScene;->access$3000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/SharedPreferences;

    move-result-object v10

    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v10, "stereo"

    iget-object v11, p0, Lcom/mediatek/vlw/VideoScene$3;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v11}, Lcom/mediatek/vlw/VideoScene;->access$2800(Lcom/mediatek/vlw/VideoScene;)I

    move-result v11

    int-to-long v11, v11

    invoke-interface {v3, v10, v11, v12}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0
.end method
