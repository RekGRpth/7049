.class Lcom/mediatek/vlw/VideoScene$5;
.super Ljava/lang/Object;
.source "VideoScene.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/vlw/VideoScene;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/vlw/VideoScene;


# direct methods
.method constructor <init>(Lcom/mediatek/vlw/VideoScene;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene$5;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene$5;->this$0:Lcom/mediatek/vlw/VideoScene;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/vlw/VideoScene;->access$3602(Lcom/mediatek/vlw/VideoScene;Z)Z

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene$5;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v0}, Lcom/mediatek/vlw/VideoScene;->access$3700(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene$5;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v0, p1}, Lcom/mediatek/vlw/VideoScene;->access$3800(Lcom/mediatek/vlw/VideoScene;Landroid/media/MediaPlayer;)V

    :cond_0
    const-string v0, "VideoScene"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPrepared, can get metadata:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene$5;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v2}, Lcom/mediatek/vlw/VideoScene;->access$3700(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
