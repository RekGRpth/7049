.class Lcom/mediatek/vlw/VideoScene$7;
.super Ljava/lang/Object;
.source "VideoScene.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/vlw/VideoScene;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/vlw/VideoScene;


# direct methods
.method constructor <init>(Lcom/mediatek/vlw/VideoScene;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 8
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v2, 0x5

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1, v2}, Lcom/mediatek/vlw/VideoScene;->access$4002(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1, v2}, Lcom/mediatek/vlw/VideoScene;->access$3902(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1}, Lcom/mediatek/vlw/VideoScene;->access$4100(Lcom/mediatek/vlw/VideoScene;)Landroid/media/MediaPlayer$OnCompletionListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1}, Lcom/mediatek/vlw/VideoScene;->access$4100(Lcom/mediatek/vlw/VideoScene;)Landroid/media/MediaPlayer$OnCompletionListener;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v2}, Lcom/mediatek/vlw/VideoScene;->access$2100(Lcom/mediatek/vlw/VideoScene;)Landroid/media/MediaPlayer;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    :cond_0
    const-string v1, "VideoScene"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCompletion() mLoopMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$4200(Lcom/mediatek/vlw/VideoScene;)Lcom/mediatek/vlw/Utils$LoopMode;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mBucketId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mUri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$4300(Lcom/mediatek/vlw/VideoScene;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v2}, Lcom/mediatek/vlw/VideoScene;->access$1000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/vlw/Utils;->queryUrisFromBucketId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/vlw/VideoScene;->access$4402(Lcom/mediatek/vlw/VideoScene;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1}, Lcom/mediatek/vlw/VideoScene;->access$4400(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1}, Lcom/mediatek/vlw/VideoScene;->access$4400(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1}, Lcom/mediatek/vlw/VideoScene;->access$3100(Lcom/mediatek/vlw/VideoScene;)Z

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v2}, Lcom/mediatek/vlw/VideoScene;->access$1000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/vlw/Utils;->queryUrisFromBucketId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/vlw/VideoScene;->access$4402(Lcom/mediatek/vlw/VideoScene;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    const-string v1, "VideoScene"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getUrisFromBucketId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$4400(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " videos"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1}, Lcom/mediatek/vlw/VideoScene;->access$4400(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v7, :cond_3

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1, v6, v6}, Lcom/mediatek/vlw/VideoScene;->access$1100(Lcom/mediatek/vlw/VideoScene;ZZ)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v2}, Lcom/mediatek/vlw/VideoScene;->access$4200(Lcom/mediatek/vlw/VideoScene;)Lcom/mediatek/vlw/Utils$LoopMode;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$4300(Lcom/mediatek/vlw/VideoScene;)I

    move-result v3

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v4}, Lcom/mediatek/vlw/VideoScene;->access$4400(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v5}, Lcom/mediatek/vlw/VideoScene;->access$4500(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/mediatek/vlw/Utils;->getLoopIndex(Lcom/mediatek/vlw/Utils$LoopMode;ILjava/util/ArrayList;Ljava/util/ArrayList;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/vlw/VideoScene;->access$4302(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1}, Lcom/mediatek/vlw/VideoScene;->access$4300(Lcom/mediatek/vlw/VideoScene;)I

    move-result v1

    if-ltz v1, :cond_2

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1}, Lcom/mediatek/vlw/VideoScene;->access$4400(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$4300(Lcom/mediatek/vlw/VideoScene;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-static {v2, v1}, Lcom/mediatek/vlw/VideoScene;->access$302(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v2}, Lcom/mediatek/vlw/VideoScene;->access$1000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/vlw/Utils;->queryStereoType(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/mediatek/vlw/VideoScene;->access$2802(Lcom/mediatek/vlw/VideoScene;I)I

    :goto_0
    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1}, Lcom/mediatek/vlw/VideoScene;->access$3000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "uri"

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v2}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1, v6}, Lcom/mediatek/vlw/VideoScene;->access$200(Lcom/mediatek/vlw/VideoScene;Z)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v1}, Lcom/mediatek/vlw/VideoScene;->startPlayback()V

    :goto_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1, v7, v7}, Lcom/mediatek/vlw/VideoScene;->access$1100(Lcom/mediatek/vlw/VideoScene;ZZ)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v1, v6}, Lcom/mediatek/vlw/VideoScene;->access$4302(Lcom/mediatek/vlw/VideoScene;I)I

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v2}, Lcom/mediatek/vlw/VideoScene;->access$100(Lcom/mediatek/vlw/VideoScene;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/vlw/VideoScene;->seekTo(J)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v1}, Lcom/mediatek/vlw/VideoScene;->start()V

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v2}, Lcom/mediatek/vlw/VideoScene;->access$100(Lcom/mediatek/vlw/VideoScene;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/vlw/VideoScene;->seekTo(J)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene$7;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v1}, Lcom/mediatek/vlw/VideoScene;->start()V

    goto :goto_1
.end method
