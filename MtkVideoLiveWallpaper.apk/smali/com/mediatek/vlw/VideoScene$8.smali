.class Lcom/mediatek/vlw/VideoScene$8;
.super Ljava/lang/Object;
.source "VideoScene.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/vlw/VideoScene;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/vlw/VideoScene;


# direct methods
.method constructor <init>(Lcom/mediatek/vlw/VideoScene;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 10
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v6, -0x1

    const/4 v9, 0x0

    const/4 v8, 0x1

    const-string v3, "VideoScene"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3, v6}, Lcom/mediatek/vlw/VideoScene;->access$4002(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3, v6}, Lcom/mediatek/vlw/VideoScene;->access$3902(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$4600(Lcom/mediatek/vlw/VideoScene;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$4600(Lcom/mediatek/vlw/VideoScene;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v4}, Lcom/mediatek/vlw/VideoScene;->access$2100(Lcom/mediatek/vlw/VideoScene;)Landroid/media/MediaPlayer;

    move-result-object v4

    invoke-interface {v3, v4, p2, p3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v8

    :cond_1
    const/4 v2, 0x0

    const/16 v3, 0xc8

    if-ne p2, v3, :cond_2

    const v2, 0x7f080004

    :goto_1
    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$1000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/Context;

    move-result-object v3

    if-nez v3, :cond_3

    const-string v3, "VideoScene"

    const-string v4, "mContext is null"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const v2, 0x7f080005

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$1500(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$400(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$1000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2, v8}, Lcom/mediatek/vlw/Utils;->showInfo(Landroid/content/Context;IZ)V

    :cond_4
    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lcom/mediatek/vlw/Utils;->isDefaultVideo(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3, v8}, Lcom/mediatek/vlw/VideoScene;->access$200(Lcom/mediatek/vlw/VideoScene;Z)V

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$1000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080005

    invoke-static {v3, v4, v8}, Lcom/mediatek/vlw/Utils;->showInfo(Landroid/content/Context;IZ)V

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$4700(Lcom/mediatek/vlw/VideoScene;)Landroid/app/WallpaperManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/WallpaperManager;->clear()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "VideoScene"

    const-string v4, "Failed to play default video, revert to static wallpaper"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$4800(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v1

    const-string v3, "VideoScene"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mStartFromBoot="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v5}, Lcom/mediatek/vlw/VideoScene;->access$1500(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", has sdcard: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mHasShutdown="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v5}, Lcom/mediatek/vlw/VideoScene;->access$1400(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mUri="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v5}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mBucketId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v5}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$1500(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v3

    if-eqz v3, :cond_6

    if-nez v1, :cond_7

    :cond_6
    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$1400(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_7
    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3, v9}, Lcom/mediatek/vlw/VideoScene;->access$200(Lcom/mediatek/vlw/VideoScene;Z)V

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$400(Lcom/mediatek/vlw/VideoScene;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$1000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f08000f

    invoke-static {v3, v4, v8}, Lcom/mediatek/vlw/Utils;->showInfo(Landroid/content/Context;IZ)V

    const-string v3, "VideoScene"

    const-string v4, "Start from boot and has sdcard, wait for its preparing"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3, v8}, Lcom/mediatek/vlw/VideoScene;->access$402(Lcom/mediatek/vlw/VideoScene;Z)Z

    goto/16 :goto_0

    :cond_8
    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_d

    if-eqz v1, :cond_d

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v4}, Lcom/mediatek/vlw/VideoScene;->access$1000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v5}, Lcom/mediatek/vlw/VideoScene;->access$2400(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/vlw/Utils;->queryUrisFromBucketId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/vlw/VideoScene;->access$4402(Lcom/mediatek/vlw/VideoScene;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$4400(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "VideoScene"

    const-string v4, "Invalid video folder, play the default video"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3, v9}, Lcom/mediatek/vlw/VideoScene;->access$200(Lcom/mediatek/vlw/VideoScene;Z)V

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3, v8, v8}, Lcom/mediatek/vlw/VideoScene;->access$1100(Lcom/mediatek/vlw/VideoScene;ZZ)V

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$800(Lcom/mediatek/vlw/VideoScene;)V

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v3}, Lcom/mediatek/vlw/VideoScene;->start()V

    goto/16 :goto_0

    :cond_9
    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$4500(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v3

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v3, v4}, Lcom/mediatek/vlw/VideoScene;->access$4502(Lcom/mediatek/vlw/VideoScene;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    :cond_a
    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$4500(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v4}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$4500(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v4}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_b
    const-string v3, "VideoScene"

    const-string v4, "Video playing is removed or invalid in selected folder, play another video"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3, v9}, Lcom/mediatek/vlw/VideoScene;->access$200(Lcom/mediatek/vlw/VideoScene;Z)V

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v4}, Lcom/mediatek/vlw/VideoScene;->access$4200(Lcom/mediatek/vlw/VideoScene;)Lcom/mediatek/vlw/Utils$LoopMode;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v5}, Lcom/mediatek/vlw/VideoScene;->access$4300(Lcom/mediatek/vlw/VideoScene;)I

    move-result v5

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v6}, Lcom/mediatek/vlw/VideoScene;->access$4400(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v6

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v7}, Lcom/mediatek/vlw/VideoScene;->access$4500(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/mediatek/vlw/Utils;->getLoopIndex(Lcom/mediatek/vlw/Utils$LoopMode;ILjava/util/ArrayList;Ljava/util/ArrayList;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/mediatek/vlw/VideoScene;->access$4302(Lcom/mediatek/vlw/VideoScene;I)I

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$4300(Lcom/mediatek/vlw/VideoScene;)I

    move-result v3

    if-ltz v3, :cond_c

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$4400(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v5}, Lcom/mediatek/vlw/VideoScene;->access$4300(Lcom/mediatek/vlw/VideoScene;)I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    invoke-static {v4, v3}, Lcom/mediatek/vlw/VideoScene;->access$302(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v4}, Lcom/mediatek/vlw/VideoScene;->access$1000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v5}, Lcom/mediatek/vlw/VideoScene;->access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/vlw/Utils;->queryStereoType(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/mediatek/vlw/VideoScene;->access$2802(Lcom/mediatek/vlw/VideoScene;I)I

    :goto_2
    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$1600(Lcom/mediatek/vlw/VideoScene;)V

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$800(Lcom/mediatek/vlw/VideoScene;)V

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v3}, Lcom/mediatek/vlw/VideoScene;->start()V

    goto/16 :goto_0

    :cond_c
    const-string v3, "VideoScene"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No valid video in this folder, play default video, size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v5}, Lcom/mediatek/vlw/VideoScene;->access$4400(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3, v8, v8}, Lcom/mediatek/vlw/VideoScene;->access$1100(Lcom/mediatek/vlw/VideoScene;ZZ)V

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3, v9}, Lcom/mediatek/vlw/VideoScene;->access$4302(Lcom/mediatek/vlw/VideoScene;I)I

    goto :goto_2

    :cond_d
    const-string v3, "VideoScene"

    const-string v4, "media file doesn\'t exist, play the default video"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3, v9}, Lcom/mediatek/vlw/VideoScene;->access$200(Lcom/mediatek/vlw/VideoScene;Z)V

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3, v8, v8}, Lcom/mediatek/vlw/VideoScene;->access$1100(Lcom/mediatek/vlw/VideoScene;ZZ)V

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-static {v3}, Lcom/mediatek/vlw/VideoScene;->access$800(Lcom/mediatek/vlw/VideoScene;)V

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene$8;->this$0:Lcom/mediatek/vlw/VideoScene;

    invoke-virtual {v3}, Lcom/mediatek/vlw/VideoScene;->start()V

    goto/16 :goto_0
.end method
