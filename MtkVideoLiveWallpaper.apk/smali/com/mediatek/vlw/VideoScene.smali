.class public Lcom/mediatek/vlw/VideoScene;
.super Ljava/lang/Object;
.source "VideoScene.java"


# static fields
.field private static final ACTION_BOOT_IPO:Ljava/lang/String; = "android.intent.action.ACTION_BOOT_IPO"

.field private static final ACTION_PRE_SHUTDOWN:Ljava/lang/String; = "android.intent.action.ACTION_PRE_SHUTDOWN"

.field static final BUCKET_ID:Ljava/lang/String; = "bucketId"

.field private static final BUCKET_ID_PREV:Ljava/lang/String; = "bucketId_prev"

.field private static final CLEAR_TIME_OUT:I = 0x7530

.field private static final COMPLETE_PRECENT:I = 0x64

.field static final CURRENT_POSITION:Ljava/lang/String; = "pos"

.field private static final CURRENT_POSITION_PREV:Ljava/lang/String; = "pos_prev"

.field private static final DEBUG:Z = true

.field static final DEFAULT_END:I = 0x927c0

.field static final DEFAULT_START:I = 0x0

.field static final DEFAULT_STEREO_TYPE:I = -0x1

.field static final END_TIME:Ljava/lang/String; = "end"

.field private static final END_TIME_PREV:Ljava/lang/String; = "end_prev"

.field private static final FILE:Ljava/lang/String; = "file"

.field private static final ICS_DEFAULT_SDCARD_PATH:Ljava/lang/String; = "/mnt/sdcard/"

.field private static final ICS_EXTERNAL_SDCARD_PATH:Ljava/lang/String; = "/mnt/sdcard2/"

.field private static final MEDIA_CANGETMETADATA:I = 0x323

.field private static final MSG_CLEAR:I = 0x5

.field private static final MSG_INVALIDVIDEO:I = 0x0

.field private static final MSG_MONITOR_POSITION:I = 0x1

.field private static final MSG_RELEASE_TIMER:I = 0x3

.field private static final MSG_RELOAD_VIDEO:I = 0x4

.field private static final RELEASE_TIME_OUT:I = 0x493e0

.field private static final RELOAD_TIME_OUT:I = 0x7d0

.field private static final SCHEMA_HTTP:Ljava/lang/String; = "http"

.field private static final SCHEMA_RTSP:Ljava/lang/String; = "rtsp"

.field static final SHARED_PREFS_FILE:Ljava/lang/String; = "vlw"

.field static final START_TIME:Ljava/lang/String; = "start"

.field private static final START_TIME_PREV:Ljava/lang/String; = "start_prev"

.field private static final STATE_ERROR:I = -0x1

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_PAUSED:I = 0x4

.field private static final STATE_PLAYBACK_COMPLETED:I = 0x5

.field private static final STATE_PLAYING:I = 0x3

.field private static final STATE_PREPARED:I = 0x2

.field private static final STATE_PREPARING:I = 0x1

.field static final STEREO_TYPE:Ljava/lang/String; = "stereo"

.field static final STEREO_TYPE_PREV:Ljava/lang/String; = "stereo_prev"

.field private static final STREAMING_HTTP:I = 0x1

.field private static final STREAMING_RTSP:I = 0x2

.field private static final STREAMING_SDP:I = 0x3

.field private static final TAG:Ljava/lang/String; = "VideoScene"

.field private static final TIME_OUT:I = 0x3e8

.field private static final URI_END:Ljava/lang/String; = "sdp"

.field private static final WALLPAPER_MAX_SCALE_PERCENT:F = 2.0f

.field private static final WALLPAPER_SCREENS_SPAN:F = 1.25f

.field static final WALLPAPER_URI:Ljava/lang/String; = "uri"

.field private static final WALLPAPER_URI_PREV:Ljava/lang/String; = "uri_prev"


# instance fields
.field private mBucketId:Ljava/lang/String;

.field private mBufferWaitTimes:I

.field private final mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private mCanGetMetaData:Z

.field private mCanPause:Z

.field private mCanSeekBack:Z

.field private mCanSeekForward:Z

.field private final mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private final mContext:Landroid/content/Context;

.field private mCurrentBufferPercentage:I

.field private mCurrentPos:I

.field private mCurrentState:I

.field private mDuration:I

.field private mEndTime:I

.field private final mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mFileObserver:Landroid/os/FileObserver;

.field private final mHandler:Landroid/os/Handler;

.field private mHasShutdown:Z

.field private mHaveGetPreparedCallBack:Z

.field private mHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mLoopMode:Lcom/mediatek/vlw/Utils$LoopMode;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mMode:I

.field private mMovingFile:Ljava/lang/String;

.field private mObserverPath:Ljava/lang/String;

.field private mOnBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mOriginWallpaperHeight:I

.field private mOriginWallpaperWidth:I

.field mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mPrevBucketId:Ljava/lang/String;

.field private mPrevCurrentPos:I

.field private mPrevEndTime:I

.field private mPrevStartTime:I

.field private mPrevStereoType:I

.field private mPrevUri:Landroid/net/Uri;

.field private final mPreview:Z

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mSeekWhenPrepared:J

.field private final mSharedPref:Landroid/content/SharedPreferences;

.field private final mShutdownReceiver:Landroid/content/BroadcastReceiver;

.field mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private mStartFromBoot:Z

.field private mStartTime:I

.field private mStateWhenSuspended:I

.field private mStereoType:I

.field private mStoragesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStreamingType:I

.field private mSurfaceHeight:I

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mSurfaceWidth:I

.field private mTargetState:I

.field private mTvOut:Lcom/mediatek/common/tvout/ITVOUTNative;

.field private mUri:Landroid/net/Uri;

.field private mUriInvalid:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mUriList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mVideoHeight:I

.field private mVideoWidth:I

.field private mVisible:Z

.field private mWaitingReload:Z

.field private final mWallpaperManager:Landroid/app/WallpaperManager;

.field private final mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/SurfaceHolder;Z)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/SurfaceHolder;
    .param p3    # Z

    const/4 v3, -0x1

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v5, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentState:I

    iput v5, p0, Lcom/mediatek/vlw/VideoScene;->mTargetState:I

    iput v3, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperWidth:I

    iput v3, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperHeight:I

    iput v3, p0, Lcom/mediatek/vlw/VideoScene;->mStreamingType:I

    iput v5, p0, Lcom/mediatek/vlw/VideoScene;->mBufferWaitTimes:I

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mediatek/vlw/VideoScene;->mStartFromBoot:Z

    sget-object v3, Lcom/mediatek/vlw/Utils$LoopMode;->ALL:Lcom/mediatek/vlw/Utils$LoopMode;

    iput-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mLoopMode:Lcom/mediatek/vlw/Utils$LoopMode;

    new-instance v3, Lcom/mediatek/vlw/VideoScene$1;

    invoke-direct {v3, p0}, Lcom/mediatek/vlw/VideoScene$1;-><init>(Lcom/mediatek/vlw/VideoScene;)V

    iput-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/mediatek/vlw/VideoScene$2;

    invoke-direct {v3, p0}, Lcom/mediatek/vlw/VideoScene$2;-><init>(Lcom/mediatek/vlw/VideoScene;)V

    iput-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Lcom/mediatek/vlw/VideoScene$3;

    invoke-direct {v3, p0}, Lcom/mediatek/vlw/VideoScene$3;-><init>(Lcom/mediatek/vlw/VideoScene;)V

    iput-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Lcom/mediatek/vlw/VideoScene$4;

    invoke-direct {v3, p0}, Lcom/mediatek/vlw/VideoScene$4;-><init>(Lcom/mediatek/vlw/VideoScene;)V

    iput-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    new-instance v3, Lcom/mediatek/vlw/VideoScene$5;

    invoke-direct {v3, p0}, Lcom/mediatek/vlw/VideoScene$5;-><init>(Lcom/mediatek/vlw/VideoScene;)V

    iput-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v3, Lcom/mediatek/vlw/VideoScene$6;

    invoke-direct {v3, p0}, Lcom/mediatek/vlw/VideoScene$6;-><init>(Lcom/mediatek/vlw/VideoScene;)V

    iput-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    new-instance v3, Lcom/mediatek/vlw/VideoScene$7;

    invoke-direct {v3, p0}, Lcom/mediatek/vlw/VideoScene$7;-><init>(Lcom/mediatek/vlw/VideoScene;)V

    iput-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v3, Lcom/mediatek/vlw/VideoScene$8;

    invoke-direct {v3, p0}, Lcom/mediatek/vlw/VideoScene$8;-><init>(Lcom/mediatek/vlw/VideoScene;)V

    iput-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v3, Lcom/mediatek/vlw/VideoScene$9;

    invoke-direct {v3, p0}, Lcom/mediatek/vlw/VideoScene$9;-><init>(Lcom/mediatek/vlw/VideoScene;)V

    iput-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    const-string v3, "vlw"

    invoke-virtual {p1, v3, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mTvOut:Lcom/mediatek/common/tvout/ITVOUTNative;

    if-nez v3, :cond_0

    const-class v3, Lcom/mediatek/common/tvout/ITVOUTNative;

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/mediatek/common/MediatekClassFactory;->createInstance(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/common/tvout/ITVOUTNative;

    iput-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mTvOut:Lcom/mediatek/common/tvout/ITVOUTNative;

    :cond_0
    iput-object p2, p0, Lcom/mediatek/vlw/VideoScene;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iput-boolean p3, p0, Lcom/mediatek/vlw/VideoScene;->mPreview:Z

    iput v5, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentState:I

    iput v5, p0, Lcom/mediatek/vlw/VideoScene;->mTargetState:I

    const-string v3, "wallpaper"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/WallpaperManager;

    iput-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mWallpaperManager:Landroid/app/WallpaperManager;

    const-string v3, "window"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    iput-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mWindowManager:Landroid/view/WindowManager;

    iget v3, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperWidth:I

    if-lez v3, :cond_1

    iget v3, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperHeight:I

    if-gtz v3, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v1

    if-le v1, v2, :cond_3

    mul-int/lit8 v3, v2, 0x2

    iput v3, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperWidth:I

    iput v1, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperHeight:I

    :cond_2
    :goto_0
    const-string v3, "VideoScene"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "original wallpaper width="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", height="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_3
    mul-int/lit8 v3, v1, 0x2

    iput v3, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperWidth:I

    iput v2, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperHeight:I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/mediatek/vlw/VideoScene;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mEndTime:I

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/vlw/VideoScene;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoScene;->mEndTime:I

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/vlw/VideoScene;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mStartTime:I

    return v0
.end method

.method static synthetic access$1000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/vlw/VideoScene;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoScene;->mStartTime:I

    return p1
.end method

.method static synthetic access$1100(Lcom/mediatek/vlw/VideoScene;ZZ)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Z
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/mediatek/vlw/VideoScene;->clear(ZZ)V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/vlw/VideoScene;)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->handleInvalid()V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/vlw/VideoScene;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mediatek/vlw/VideoScene;)Z
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mHasShutdown:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/mediatek/vlw/VideoScene;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/vlw/VideoScene;->mHasShutdown:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/mediatek/vlw/VideoScene;)Z
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mStartFromBoot:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/mediatek/vlw/VideoScene;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/vlw/VideoScene;->mStartFromBoot:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/mediatek/vlw/VideoScene;)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->saveSettings()V

    return-void
.end method

.method static synthetic access$1700(Lcom/mediatek/vlw/VideoScene;)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->stopAndReleaseVideoObserver()V

    return-void
.end method

.method static synthetic access$1800(Lcom/mediatek/vlw/VideoScene;)Z
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->isInPlaybackState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/vlw/VideoScene;->correctUriIfNeeded(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/mediatek/vlw/VideoScene;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/vlw/VideoScene;->release(Z)V

    return-void
.end method

.method static synthetic access$2000(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mStoragesList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/mediatek/vlw/VideoScene;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene;->mStoragesList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/mediatek/vlw/VideoScene;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mPrevUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene;->mPrevUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mPrevBucketId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene;->mPrevBucketId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/mediatek/vlw/VideoScene;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mPrevStartTime:I

    return v0
.end method

.method static synthetic access$2502(Lcom/mediatek/vlw/VideoScene;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoScene;->mPrevStartTime:I

    return p1
.end method

.method static synthetic access$2600(Lcom/mediatek/vlw/VideoScene;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mPrevEndTime:I

    return v0
.end method

.method static synthetic access$2602(Lcom/mediatek/vlw/VideoScene;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoScene;->mPrevEndTime:I

    return p1
.end method

.method static synthetic access$2700(Lcom/mediatek/vlw/VideoScene;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mPrevCurrentPos:I

    return v0
.end method

.method static synthetic access$2702(Lcom/mediatek/vlw/VideoScene;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoScene;->mPrevCurrentPos:I

    return p1
.end method

.method static synthetic access$2800(Lcom/mediatek/vlw/VideoScene;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    return v0
.end method

.method static synthetic access$2802(Lcom/mediatek/vlw/VideoScene;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    return p1
.end method

.method static synthetic access$2900(Lcom/mediatek/vlw/VideoScene;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mPrevStereoType:I

    return v0
.end method

.method static synthetic access$2902(Lcom/mediatek/vlw/VideoScene;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoScene;->mPrevStereoType:I

    return p1
.end method

.method static synthetic access$300(Lcom/mediatek/vlw/VideoScene;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/mediatek/vlw/VideoScene;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$302(Lcom/mediatek/vlw/VideoScene;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/mediatek/vlw/VideoScene;)Z
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->correctBucketIdIfNeeded()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3200(Lcom/mediatek/vlw/VideoScene;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/vlw/VideoScene;->setFlagsEx(Z)V

    return-void
.end method

.method static synthetic access$3300(Lcom/mediatek/vlw/VideoScene;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mVideoWidth:I

    return v0
.end method

.method static synthetic access$3302(Lcom/mediatek/vlw/VideoScene;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoScene;->mVideoWidth:I

    return p1
.end method

.method static synthetic access$3400(Lcom/mediatek/vlw/VideoScene;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mVideoHeight:I

    return v0
.end method

.method static synthetic access$3402(Lcom/mediatek/vlw/VideoScene;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoScene;->mVideoHeight:I

    return p1
.end method

.method static synthetic access$3500(Lcom/mediatek/vlw/VideoScene;II)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/vlw/VideoScene;->relayout(II)V

    return-void
.end method

.method static synthetic access$3600(Lcom/mediatek/vlw/VideoScene;)Z
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mHaveGetPreparedCallBack:Z

    return v0
.end method

.method static synthetic access$3602(Lcom/mediatek/vlw/VideoScene;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/vlw/VideoScene;->mHaveGetPreparedCallBack:Z

    return p1
.end method

.method static synthetic access$3700(Lcom/mediatek/vlw/VideoScene;)Z
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mCanGetMetaData:Z

    return v0
.end method

.method static synthetic access$3702(Lcom/mediatek/vlw/VideoScene;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/vlw/VideoScene;->mCanGetMetaData:Z

    return p1
.end method

.method static synthetic access$3800(Lcom/mediatek/vlw/VideoScene;Landroid/media/MediaPlayer;)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Landroid/media/MediaPlayer;

    invoke-direct {p0, p1}, Lcom/mediatek/vlw/VideoScene;->doPrepared(Landroid/media/MediaPlayer;)V

    return-void
.end method

.method static synthetic access$3900(Lcom/mediatek/vlw/VideoScene;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mTargetState:I

    return v0
.end method

.method static synthetic access$3902(Lcom/mediatek/vlw/VideoScene;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoScene;->mTargetState:I

    return p1
.end method

.method static synthetic access$400(Lcom/mediatek/vlw/VideoScene;)Z
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mWaitingReload:Z

    return v0
.end method

.method static synthetic access$4002(Lcom/mediatek/vlw/VideoScene;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentState:I

    return p1
.end method

.method static synthetic access$402(Lcom/mediatek/vlw/VideoScene;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/vlw/VideoScene;->mWaitingReload:Z

    return p1
.end method

.method static synthetic access$4100(Lcom/mediatek/vlw/VideoScene;)Landroid/media/MediaPlayer$OnCompletionListener;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/mediatek/vlw/VideoScene;)Lcom/mediatek/vlw/Utils$LoopMode;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mLoopMode:Lcom/mediatek/vlw/Utils$LoopMode;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/mediatek/vlw/VideoScene;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mMode:I

    return v0
.end method

.method static synthetic access$4302(Lcom/mediatek/vlw/VideoScene;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoScene;->mMode:I

    return p1
.end method

.method static synthetic access$4400(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$4402(Lcom/mediatek/vlw/VideoScene;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$4500(Lcom/mediatek/vlw/VideoScene;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUriInvalid:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$4502(Lcom/mediatek/vlw/VideoScene;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene;->mUriInvalid:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$4600(Lcom/mediatek/vlw/VideoScene;)Landroid/media/MediaPlayer$OnErrorListener;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/mediatek/vlw/VideoScene;)Landroid/app/WallpaperManager;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mWallpaperManager:Landroid/app/WallpaperManager;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/mediatek/vlw/VideoScene;)Z
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->checkMediaState()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4902(Lcom/mediatek/vlw/VideoScene;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentBufferPercentage:I

    return p1
.end method

.method static synthetic access$500(Lcom/mediatek/vlw/VideoScene;)Z
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mPreview:Z

    return v0
.end method

.method static synthetic access$5000(Lcom/mediatek/vlw/VideoScene;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mBufferWaitTimes:I

    return v0
.end method

.method static synthetic access$5020(Lcom/mediatek/vlw/VideoScene;I)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mBufferWaitTimes:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/mediatek/vlw/VideoScene;->mBufferWaitTimes:I

    return v0
.end method

.method static synthetic access$5100(Lcom/mediatek/vlw/VideoScene;I)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/vlw/VideoScene;->updateBufferState(I)V

    return-void
.end method

.method static synthetic access$5200(Lcom/mediatek/vlw/VideoScene;)Landroid/media/MediaPlayer$OnBufferingUpdateListener;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mOnBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    return-object v0
.end method

.method static synthetic access$5300(Lcom/mediatek/vlw/VideoScene;)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->playDefaultVideoOrNextVideo()V

    return-void
.end method

.method static synthetic access$5400(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mMovingFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5402(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene;->mMovingFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5500(Lcom/mediatek/vlw/VideoScene;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mObserverPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/mediatek/vlw/VideoScene;J)J
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # J

    iput-wide p1, p0, Lcom/mediatek/vlw/VideoScene;->mSeekWhenPrepared:J

    return-wide p1
.end method

.method static synthetic access$700(Lcom/mediatek/vlw/VideoScene;)I
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    return v0
.end method

.method static synthetic access$702(Lcom/mediatek/vlw/VideoScene;I)I
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    return p1
.end method

.method static synthetic access$800(Lcom/mediatek/vlw/VideoScene;)V
    .locals 0
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->openVideo()V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/vlw/VideoScene;)Z
    .locals 1
    .param p0    # Lcom/mediatek/vlw/VideoScene;

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mVisible:Z

    return v0
.end method

.method private addAndStartVideoObserver()V
    .locals 4

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-static {v1}, Lcom/mediatek/vlw/Utils;->isDefaultVideo(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mFileObserver:Landroid/os/FileObserver;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mObserverPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    iput-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mObserverPath:Ljava/lang/String;

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->stopAndReleaseVideoObserver()V

    const-string v1, "VideoScene"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "======FileObserver start to monitor "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mObserverPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/mediatek/vlw/VideoScene$10;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mObserverPath:Ljava/lang/String;

    const/16 v3, 0xec0

    invoke-direct {v1, p0, v2, v3}, Lcom/mediatek/vlw/VideoScene$10;-><init>(Lcom/mediatek/vlw/VideoScene;Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mFileObserver:Landroid/os/FileObserver;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mFileObserver:Landroid/os/FileObserver;

    invoke-virtual {v1}, Landroid/os/FileObserver;->startWatching()V

    goto :goto_0
.end method

.method private checkEnvironment()V
    .locals 13

    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v6, -0x1

    :try_start_0
    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v10, "uri"

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v10, "bucketId"

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v10, "stereo"

    const-wide/16 v11, -0x1

    invoke-interface {v9, v10, v11, v12}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v9

    long-to-int v6, v9

    :goto_0
    if-eqz v0, :cond_5

    const/4 v8, 0x1

    :goto_1
    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v9, :cond_6

    iget-boolean v9, p0, Lcom/mediatek/vlw/VideoScene;->mWaitingReload:Z

    if-nez v9, :cond_6

    iget-boolean v9, p0, Lcom/mediatek/vlw/VideoScene;->mPreview:Z

    if-nez v9, :cond_0

    iget v9, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    int-to-long v9, v9

    iput-wide v9, p0, Lcom/mediatek/vlw/VideoScene;->mSeekWhenPrepared:J

    :cond_0
    const-string v9, "VideoScene"

    const-string v10, "checkEnvironment() MediaPlayer released, open video"

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->openVideo()V

    :cond_1
    :goto_2
    iput-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v10, "start"

    iget v11, p0, Lcom/mediatek/vlw/VideoScene;->mStartTime:I

    int-to-long v11, v11

    invoke-interface {v9, v10, v11, v12}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v9

    long-to-int v4, v9

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v10, "end"

    iget v11, p0, Lcom/mediatek/vlw/VideoScene;->mEndTime:I

    int-to-long v11, v11

    invoke-interface {v9, v10, v11, v12}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v9

    long-to-int v3, v9

    if-nez v8, :cond_2

    iget v9, p0, Lcom/mediatek/vlw/VideoScene;->mStartTime:I

    if-eq v4, v9, :cond_2

    const-string v9, "VideoScene"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkEnvironment() change start from sharedpref: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mStartTime:I

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    iget v9, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    int-to-long v9, v9

    invoke-virtual {p0, v9, v10}, Lcom/mediatek/vlw/VideoScene;->seekTo(J)V

    :cond_2
    if-nez v8, :cond_3

    iget v9, p0, Lcom/mediatek/vlw/VideoScene;->mEndTime:I

    if-eq v3, v9, :cond_3

    const-string v9, "VideoScene"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkEnvironment() change end from sharedpref: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput v3, p0, Lcom/mediatek/vlw/VideoScene;->mEndTime:I

    iget v9, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    if-le v9, v3, :cond_3

    iget v9, p0, Lcom/mediatek/vlw/VideoScene;->mStartTime:I

    iput v9, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    iget v9, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    int-to-long v9, v9

    invoke-virtual {p0, v9, v10}, Lcom/mediatek/vlw/VideoScene;->seekTo(J)V

    :cond_3
    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-static {v9, v10}, Lcom/mediatek/vlw/Utils;->queryStereoType(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v5

    if-nez v8, :cond_4

    iget v9, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    if-eq v5, v9, :cond_4

    const/4 v9, -0x1

    if-eq v5, v9, :cond_4

    const-string v9, "VideoScene"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkEnvironment() User changes s3d type in Gallery: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput v5, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    const/4 v9, 0x0

    invoke-direct {p0, v9}, Lcom/mediatek/vlw/VideoScene;->setFlagsEx(Z)V

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v9, "stereo"

    iget v10, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    int-to-long v10, v10

    invoke-interface {v2, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_4
    return-void

    :catch_0
    move-exception v1

    const-string v9, "VideoScene"

    const-string v10, "Read in SharedPref failed"

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    const/4 v8, 0x0

    goto/16 :goto_1

    :cond_6
    if-eqz v0, :cond_7

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_7

    const-string v9, "VideoScene"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkEnvironment() select folder from sharedpref: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-direct {p0, v9, v10}, Lcom/mediatek/vlw/VideoScene;->clear(ZZ)V

    iput-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    iput-object v7, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    iput v6, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    const/4 v8, 0x1

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->openVideo()V

    goto/16 :goto_2

    :cond_7
    if-eqz v7, :cond_1

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v9, v7}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string v9, "VideoScene"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkEnvironment() change video from sharedpref: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-direct {p0, v9, v10}, Lcom/mediatek/vlw/VideoScene;->clear(ZZ)V

    iput-object v7, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    iput v6, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    const/4 v8, 0x1

    const/4 v9, 0x0

    iput-object v9, p0, Lcom/mediatek/vlw/VideoScene;->mPrevBucketId:Ljava/lang/String;

    const/4 v9, 0x0

    iput-object v9, p0, Lcom/mediatek/vlw/VideoScene;->mPrevUri:Landroid/net/Uri;

    iget-object v9, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v9, "uri_prev"

    const/4 v10, 0x0

    invoke-interface {v2, v9, v10}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->openVideo()V

    goto/16 :goto_2
.end method

.method private checkMediaState()Z
    .locals 5

    const/4 v0, 0x0

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    const-string v2, "removed"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "bad_removal"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "unmountable"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "nofs"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const-string v2, "VideoScene"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "check sdcard state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const-string v2, "unmounted"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "checking"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "mounted"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "mounted_ro"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "shared"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const-string v2, "VideoScene"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "check sdcard state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const-string v2, "VideoScene"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "check sdcard state, uncaught sdcard state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private clear(ZZ)V
    .locals 5
    .param p1    # Z
    .param p2    # Z

    const/4 v1, 0x0

    const/4 v4, 0x0

    if-eqz p2, :cond_1

    iput-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    :cond_0
    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mUriInvalid:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mUriInvalid:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    :cond_1
    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080016

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mStartTime:I

    const v2, 0x927c0

    iput v2, p0, Lcom/mediatek/vlw/VideoScene;->mEndTime:I

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    const/4 v2, -0x1

    iput v2, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    if-eqz p1, :cond_2

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    if-nez v2, :cond_3

    const-string v1, "VideoScene"

    const-string v2, "we lost the shared preferences"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "bucketId"

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "uri"

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "start"

    iget v3, p0, Lcom/mediatek/vlw/VideoScene;->mStartTime:I

    int-to-long v3, v3

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v2, "end"

    iget v3, p0, Lcom/mediatek/vlw/VideoScene;->mEndTime:I

    int-to-long v3, v3

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v2, "pos"

    iget v3, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    int-to-long v3, v3

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v2, "bucketId_prev"

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mPrevBucketId:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "uri_prev"

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mPrevUri:Landroid/net/Uri;

    if-eqz v3, :cond_4

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mPrevUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_4
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "start_prev"

    iget v2, p0, Lcom/mediatek/vlw/VideoScene;->mPrevStartTime:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v1, "end_prev"

    iget v2, p0, Lcom/mediatek/vlw/VideoScene;->mPrevEndTime:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v1, "pos_prev"

    iget v2, p0, Lcom/mediatek/vlw/VideoScene;->mPrevCurrentPos:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v1, "stereo"

    iget v2, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v1, "stereo_prev"

    iget v2, p0, Lcom/mediatek/vlw/VideoScene;->mPrevStereoType:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v1, "VideoScene"

    const-string v2, "clean(), reset the default state into shared_prefs"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private correctBucketIdIfNeeded()Z
    .locals 5

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-static {v2}, Lcom/mediatek/vlw/Utils;->isDefaultVideo(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_1
    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/vlw/Utils;->queryBucketId(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "VideoScene"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Correct invalid bucketId "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " --> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "bucketId"

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private correctUriIfNeeded(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "/mnt/sdcard2/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "/mnt/sdcard2/"

    invoke-virtual {v3, v4, p1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "VideoScene"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Correct invalid video path "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " --> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-static {v4, v5}, Lcom/mediatek/vlw/Utils;->queryStereoType(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v4

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v4, "uri"

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v4, "stereo"

    iget v5, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    int-to-long v5, v5

    invoke-interface {v1, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    return-void

    :cond_2
    const-string v4, "/mnt/sdcard/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "/mnt/sdcard/"

    invoke-virtual {v3, v4, p1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private doPrepared(Landroid/media/MediaPlayer;)V
    .locals 12
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x1

    const-string v6, "VideoScene"

    const-string v9, "do prepared."

    invoke-static {v6, v9}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput v10, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentState:I

    iput-boolean v7, p0, Lcom/mediatek/vlw/VideoScene;->mStartFromBoot:Z

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene;->mHandler:Landroid/os/Handler;

    const/4 v9, 0x5

    invoke-virtual {v6, v9}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p1, v7, v7}, Landroid/media/MediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {v0, v8}, Landroid/media/Metadata;->has(I)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0, v8}, Landroid/media/Metadata;->getBoolean(I)Z

    move-result v6

    if-eqz v6, :cond_8

    :cond_0
    move v6, v8

    :goto_0
    iput-boolean v6, p0, Lcom/mediatek/vlw/VideoScene;->mCanPause:Z

    invoke-virtual {v0, v10}, Landroid/media/Metadata;->has(I)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v0, v10}, Landroid/media/Metadata;->getBoolean(I)Z

    move-result v6

    if-eqz v6, :cond_9

    :cond_1
    move v6, v8

    :goto_1
    iput-boolean v6, p0, Lcom/mediatek/vlw/VideoScene;->mCanSeekBack:Z

    invoke-virtual {v0, v11}, Landroid/media/Metadata;->has(I)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v0, v11}, Landroid/media/Metadata;->getBoolean(I)Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_2
    move v7, v8

    :cond_3
    iput-boolean v7, p0, Lcom/mediatek/vlw/VideoScene;->mCanSeekForward:Z

    :goto_2
    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/mediatek/vlw/VideoScene;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    iget-object v7, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-interface {v6, v7}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V

    :cond_4
    iget-wide v3, p0, Lcom/mediatek/vlw/VideoScene;->mSeekWhenPrepared:J

    const-wide/16 v6, 0x0

    cmp-long v6, v3, v6

    if-eqz v6, :cond_5

    invoke-virtual {p0, v3, v4}, Lcom/mediatek/vlw/VideoScene;->seekTo(J)V

    :cond_5
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    iget v6, p0, Lcom/mediatek/vlw/VideoScene;->mEndTime:I

    const v7, 0x927c0

    if-ne v6, v7, :cond_b

    :goto_3
    iput v1, p0, Lcom/mediatek/vlw/VideoScene;->mEndTime:I

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v5

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v2

    if-eqz v5, :cond_c

    if-eqz v2, :cond_c

    const-string v6, "VideoScene"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "doPrepared, video size: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v6, p0, Lcom/mediatek/vlw/VideoScene;->mVideoWidth:I

    if-ne v6, v5, :cond_6

    iget v6, p0, Lcom/mediatek/vlw/VideoScene;->mVideoHeight:I

    if-eq v6, v2, :cond_7

    :cond_6
    const-string v6, "VideoScene"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Video size changed ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/mediatek/vlw/VideoScene;->mVideoWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/mediatek/vlw/VideoScene;->mVideoHeight:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")->("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "), relayout surface"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput v5, p0, Lcom/mediatek/vlw/VideoScene;->mVideoWidth:I

    iput v2, p0, Lcom/mediatek/vlw/VideoScene;->mVideoHeight:I

    iget v6, p0, Lcom/mediatek/vlw/VideoScene;->mVideoWidth:I

    iget v7, p0, Lcom/mediatek/vlw/VideoScene;->mVideoHeight:I

    invoke-direct {p0, v6, v7}, Lcom/mediatek/vlw/VideoScene;->relayout(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/mediatek/vlw/VideoScene;->start()V

    :goto_4
    return-void

    :cond_8
    move v6, v7

    goto/16 :goto_0

    :cond_9
    move v6, v7

    goto/16 :goto_1

    :cond_a
    iput-boolean v8, p0, Lcom/mediatek/vlw/VideoScene;->mCanPause:Z

    iput-boolean v8, p0, Lcom/mediatek/vlw/VideoScene;->mCanSeekBack:Z

    iput-boolean v8, p0, Lcom/mediatek/vlw/VideoScene;->mCanSeekForward:Z

    goto/16 :goto_2

    :cond_b
    iget v1, p0, Lcom/mediatek/vlw/VideoScene;->mEndTime:I

    goto/16 :goto_3

    :cond_c
    invoke-virtual {p0}, Lcom/mediatek/vlw/VideoScene;->start()V

    goto :goto_4
.end method

.method private handleInvalid()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/vlw/Utils;->queryUrisFromBucketId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "VideoScene"

    const-string v1, "invalid video folder, play the default video"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v5}, Lcom/mediatek/vlw/VideoScene;->release(Z)V

    invoke-direct {p0, v4, v4}, Lcom/mediatek/vlw/VideoScene;->clear(ZZ)V

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->openVideo()V

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mVisible:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/vlw/VideoScene;->start()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUriInvalid:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUriInvalid:Ljava/util/ArrayList;

    :cond_2
    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUriInvalid:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUriInvalid:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    const-string v0, "VideoScene"

    const-string v1, "video playing is removed or invalid in selected folder, play another video"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v5}, Lcom/mediatek/vlw/VideoScene;->release(Z)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mLoopMode:Lcom/mediatek/vlw/Utils$LoopMode;

    iget v1, p0, Lcom/mediatek/vlw/VideoScene;->mMode:I

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mUriInvalid:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2, v3}, Lcom/mediatek/vlw/Utils;->getLoopIndex(Lcom/mediatek/vlw/Utils$LoopMode;ILjava/util/ArrayList;Ljava/util/ArrayList;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/vlw/VideoScene;->mMode:I

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mMode:I

    if-ltz v0, :cond_4

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/mediatek/vlw/VideoScene;->mMode:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/mediatek/vlw/Utils;->queryStereoType(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    :goto_1
    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->saveSettings()V

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->openVideo()V

    goto :goto_0

    :cond_4
    const-string v0, "VideoScene"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No valid video in this folder, play default video, size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v4, v4}, Lcom/mediatek/vlw/VideoScene;->clear(ZZ)V

    iput v5, p0, Lcom/mediatek/vlw/VideoScene;->mMode:I

    goto :goto_1
.end method

.method private isInPlaybackState()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentState:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentState:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentState:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private judgeStreamingType()V
    .locals 5

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    if-nez v1, :cond_0

    const-string v1, "VideoScene"

    const-string v2, "mUri is null, cannot judge streaming type."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "VideoScene"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "judgeStreamingType, mUri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/vlw/VideoScene;->mCanGetMetaData:Z

    const-string v1, "rtsp"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "sdp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    iput v1, p0, Lcom/mediatek/vlw/VideoScene;->mStreamingType:I

    const-string v1, "VideoScene"

    const-string v2, "SDP streaming type."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    iput v1, p0, Lcom/mediatek/vlw/VideoScene;->mStreamingType:I

    const-string v1, "VideoScene"

    const-string v2, "RTSP streaming type."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v1, "http"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mStreamingType:I

    iput-boolean v4, p0, Lcom/mediatek/vlw/VideoScene;->mCanGetMetaData:Z

    const-string v1, "VideoScene"

    const-string v2, "HTTP streaming type."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iput-boolean v4, p0, Lcom/mediatek/vlw/VideoScene;->mCanGetMetaData:Z

    const-string v1, "VideoScene"

    const-string v2, "Local Video streaming type."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private loadSettings()V
    .locals 15

    const-wide/32 v13, 0x927c0

    const-wide/16 v11, -0x1

    const/4 v10, 0x0

    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080016

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    if-nez v4, :cond_1

    iput-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    iput v10, p0, Lcom/mediatek/vlw/VideoScene;->mStartTime:I

    const v4, 0x927c0

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mEndTime:I

    iput v10, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    const/4 v4, -0x1

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    :goto_0
    const-string v4, "VideoScene"

    const-string v5, "Restore shared_prefs, mStartTime=%d, mEndTime=%d, mCurrentPos=%d, mBucketId=%s, mUri=%s, mStereoType=%d"

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/Object;

    iget v7, p0, Lcom/mediatek/vlw/VideoScene;->mStartTime:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    const/4 v7, 0x1

    iget v8, p0, Lcom/mediatek/vlw/VideoScene;->mEndTime:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget v8, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    iget-object v8, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x4

    iget-object v8, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    aput-object v8, v6, v7

    const/4 v7, 0x5

    iget v8, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/mediatek/vlw/Utils;->queryUrisFromBucketId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iput v0, p0, Lcom/mediatek/vlw/VideoScene;->mMode:I

    :cond_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "bucketId"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "uri"

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "start"

    invoke-interface {v4, v5, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mStartTime:I

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "end"

    invoke-interface {v4, v5, v13, v14}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mEndTime:I

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "pos"

    invoke-interface {v4, v5, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "bucketId_prev"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mPrevBucketId:Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "uri_prev"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mPrevUri:Landroid/net/Uri;

    :goto_2
    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "start_prev"

    invoke-interface {v4, v5, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mPrevStartTime:I

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "end_prev"

    invoke-interface {v4, v5, v13, v14}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mPrevEndTime:I

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "pos_prev"

    invoke-interface {v4, v5, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mPrevCurrentPos:I

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "stereo"

    invoke-interface {v4, v5, v11, v12}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v5, "stereo_prev"

    invoke-interface {v4, v5, v11, v12}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    long-to-int v4, v4

    iput v4, p0, Lcom/mediatek/vlw/VideoScene;->mPrevStereoType:I

    goto/16 :goto_0

    :cond_2
    iput-object v7, p0, Lcom/mediatek/vlw/VideoScene;->mPrevUri:Landroid/net/Uri;

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1
.end method

.method private openVideo()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, -0x1

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->judgeStreamingType()V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v7}, Lcom/mediatek/vlw/VideoScene;->release(Z)V

    :try_start_0
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    const/4 v1, -0x1

    iput v1, p0, Lcom/mediatek/vlw/VideoScene;->mDuration:I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    const/4 v1, 0x0

    iput v1, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentBufferPercentage:I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mHeaders:Ljava/util/Map;

    invoke-virtual {v1, v2, v3, v4}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    const-string v1, "VideoScene"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "open video path: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "context: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepareAsync()V

    const/4 v1, 0x1

    iput v1, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentState:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v1, "VideoScene"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iput v5, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentState:I

    iput v5, p0, Lcom/mediatek/vlw/VideoScene;->mTargetState:I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-interface {v1, v2, v6, v7}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v1, "VideoScene"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iput v5, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentState:I

    iput v5, p0, Lcom/mediatek/vlw/VideoScene;->mTargetState:I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-interface {v1, v2, v6, v7}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_0
.end method

.method private playDefaultVideoOrNextVideo()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/mediatek/vlw/Utils;->queryUrisFromBucketId(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VideoScene"

    const-string v1, "playDefaultVideoOrNextVideo(), play the default video"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v5}, Lcom/mediatek/vlw/VideoScene;->release(Z)V

    invoke-direct {p0, v4, v4}, Lcom/mediatek/vlw/VideoScene;->clear(ZZ)V

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->openVideo()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "VideoScene"

    const-string v1, "playDefaultVideoOrNextVideo(),video playing is removed or invalid in selected folder, play another video"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v5}, Lcom/mediatek/vlw/VideoScene;->release(Z)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mLoopMode:Lcom/mediatek/vlw/Utils$LoopMode;

    iget v1, p0, Lcom/mediatek/vlw/VideoScene;->mMode:I

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mUriInvalid:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2, v3}, Lcom/mediatek/vlw/Utils;->getLoopIndex(Lcom/mediatek/vlw/Utils$LoopMode;ILjava/util/ArrayList;Ljava/util/ArrayList;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/vlw/VideoScene;->mMode:I

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mMode:I

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/mediatek/vlw/VideoScene;->mMode:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/mediatek/vlw/Utils;->queryStereoType(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    iput v0, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/mediatek/vlw/Utils;->isExternalFileExists(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v4, v4}, Lcom/mediatek/vlw/VideoScene;->clear(ZZ)V

    iput v5, p0, Lcom/mediatek/vlw/VideoScene;->mMode:I

    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->saveSettings()V

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->openVideo()V

    goto :goto_0

    :cond_2
    const-string v0, "VideoScene"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "playDefaultVideoOrNextVideo(),No valid video in this folder, play default video, size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v4, v4}, Lcom/mediatek/vlw/VideoScene;->clear(ZZ)V

    iput v5, p0, Lcom/mediatek/vlw/VideoScene;->mMode:I

    goto :goto_1
.end method

.method private relayout(II)V
    .locals 24
    .param p1    # I
    .param p2    # I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vlw/VideoScene;->mWindowManager:Landroid/view/WindowManager;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/view/Display;->getHeight()I

    move-result v5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperWidth:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperHeight:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vlw/VideoScene;->mWallpaperManager:Landroid/app/WallpaperManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/app/WallpaperManager;->getDesiredMinimumWidth()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vlw/VideoScene;->mWallpaperManager:Landroid/app/WallpaperManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/app/WallpaperManager;->getDesiredMinimumHeight()I

    move-result v2

    const-string v19, "VideoScene"

    const-string v20, "relayout, display: (%d,%d), video: (%d,%d), previous wallpaper: (%d,%d)"

    const/16 v21, 0x6

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x2

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x3

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v0, v17

    if-ne v0, v3, :cond_0

    move/from16 v0, v16

    if-eq v0, v2, :cond_1

    :cond_0
    const-string v19, "VideoScene"

    const-string v20, "Wallpaper size changed, restore it: (%d,%d)-->(%d,%d)"

    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperWidth:I

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperHeight:I

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vlw/VideoScene;->mWallpaperManager:Landroid/app/WallpaperManager;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperWidth:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperHeight:I

    move/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/app/WallpaperManager;->suggestDesiredDimensions(II)V

    :cond_1
    move/from16 v15, p1

    move/from16 v14, p2

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v13, v19, v20

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v12, v19, v20

    const/high16 v11, 0x3f800000

    const/4 v9, 0x0

    const/4 v10, 0x0

    move/from16 v0, v17

    move/from16 v1, p1

    if-ne v0, v1, :cond_2

    move/from16 v0, v16

    move/from16 v1, p2

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/vlw/VideoScene;->mSurfaceWidth:I

    move/from16 v19, v0

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/vlw/VideoScene;->mSurfaceHeight:I

    move/from16 v19, v0

    move/from16 v0, p2

    move/from16 v1, v19

    if-eq v0, v1, :cond_3

    :cond_2
    const/4 v9, 0x1

    cmpl-float v19, v13, v12

    if-lez v19, :cond_6

    move v11, v13

    :cond_3
    :goto_0
    int-to-float v0, v15

    move/from16 v19, v0

    mul-float v19, v19, v11

    move/from16 v0, v19

    float-to-int v15, v0

    int-to-float v0, v14

    move/from16 v19, v0

    mul-float v19, v19, v11

    move/from16 v0, v19

    float-to-int v14, v0

    sub-int v19, v15, v17

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v18, v19, v20

    sub-int v19, v14, v16

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v7, v19, v20

    cmpl-float v19, v7, v18

    if-lez v19, :cond_7

    move v8, v7

    :goto_1
    const/high16 v19, 0x3f800000

    cmpg-float v19, v8, v19

    if-gez v19, :cond_8

    const-string v19, "VideoScene"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "just scale video to fit wallpaper, percent="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", scale="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    move/from16 v15, p1

    move/from16 v14, p2

    int-to-float v0, v15

    move/from16 v19, v0

    mul-float v19, v19, v11

    move/from16 v0, v19

    float-to-int v15, v0

    int-to-float v0, v14

    move/from16 v19, v0

    mul-float v19, v19, v11

    move/from16 v0, v19

    float-to-int v14, v0

    if-eqz v9, :cond_4

    const-string v19, "VideoScene"

    const-string v20, "resize surface: (%d,%d)-->(%d,%d)"

    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/vlw/VideoScene;->mSurfaceWidth:I

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/vlw/VideoScene;->mSurfaceHeight:I

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x3

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iput v15, v0, Lcom/mediatek/vlw/VideoScene;->mSurfaceWidth:I

    move-object/from16 v0, p0

    iput v14, v0, Lcom/mediatek/vlw/VideoScene;->mSurfaceHeight:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vlw/VideoScene;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v15, v14}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    :cond_4
    if-eqz v10, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/vlw/VideoScene;->mWallpaperManager:Landroid/app/WallpaperManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v15, v14}, Landroid/app/WallpaperManager;->suggestDesiredDimensions(II)V

    const-string v19, "VideoScene"

    const-string v20, "resize wallpaper: (%d,%d)-->(%d,%d)"

    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x3

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    return-void

    :cond_6
    move v11, v12

    goto/16 :goto_0

    :cond_7
    move/from16 v8, v18

    goto/16 :goto_1

    :cond_8
    const/high16 v19, 0x40000000

    cmpg-float v19, v8, v19

    if-gez v19, :cond_c

    const-string v19, "VideoScene"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "big difference percent: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", scale="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", reset wallpaper width to dispW * "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const/high16 v21, 0x3fa00000

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    int-to-float v0, v6

    move/from16 v19, v0

    const/high16 v20, 0x3fa00000

    mul-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v20, v0

    div-float v13, v19, v20

    move/from16 v0, v17

    move/from16 v1, p1

    if-ne v0, v1, :cond_9

    move/from16 v0, v16

    move/from16 v1, p2

    if-ne v0, v1, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/vlw/VideoScene;->mSurfaceWidth:I

    move/from16 v19, v0

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/mediatek/vlw/VideoScene;->mSurfaceHeight:I

    move/from16 v19, v0

    move/from16 v0, p2

    move/from16 v1, v19

    if-eq v0, v1, :cond_a

    :cond_9
    const/4 v9, 0x1

    cmpl-float v19, v13, v12

    if-lez v19, :cond_b

    move v11, v13

    :cond_a
    :goto_3
    const/4 v10, 0x1

    goto/16 :goto_2

    :cond_b
    move v11, v12

    goto :goto_3

    :cond_c
    const-string v19, "VideoScene"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "TODO: need to rotate the video, percent="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", scale="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v11, 0x3f800000

    goto/16 :goto_2
.end method

.method private release(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput v2, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentState:I

    if-eqz p1, :cond_0

    iput v2, p0, Lcom/mediatek/vlw/VideoScene;->mTargetState:I

    :cond_0
    return-void
.end method

.method private saveSettings()V
    .locals 6

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mediatek/vlw/VideoScene;->getCurrentPosition()I

    move-result v1

    iput v1, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    const-string v3, "uri"

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "uri_prev"

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mPrevUri:Landroid/net/Uri;

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mPrevUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_0
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "pos"

    iget v2, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v1, "pos_prev"

    iget v2, p0, Lcom/mediatek/vlw/VideoScene;->mPrevCurrentPos:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v1, "stereo"

    iget v2, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v1, "stereo_prev"

    iget v2, p0, Lcom/mediatek/vlw/VideoScene;->mPrevStereoType:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v1, "VideoScene"

    const-string v2, "save mCurrentPos=%d, mUri=%s, mPrevUri=%s, mStereoType=%d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/mediatek/vlw/VideoScene;->mPrevUri:Landroid/net/Uri;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget v5, p0, Lcom/mediatek/vlw/VideoScene;->mStereoType:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method private setFlagsEx(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method private stopAndReleaseVideoObserver()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mFileObserver:Landroid/os/FileObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mFileObserver:Landroid/os/FileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->stopWatching()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mFileObserver:Landroid/os/FileObserver;

    return-void
.end method

.method private updateBufferState(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mStreamingType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/16 v0, 0x64

    if-lt p1, v0, :cond_1

    iput-boolean v3, p0, Lcom/mediatek/vlw/VideoScene;->mCanPause:Z

    iput-boolean v3, p0, Lcom/mediatek/vlw/VideoScene;->mCanSeekBack:Z

    iput-boolean v3, p0, Lcom/mediatek/vlw/VideoScene;->mCanSeekForward:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v2, p0, Lcom/mediatek/vlw/VideoScene;->mCanPause:Z

    iput-boolean v2, p0, Lcom/mediatek/vlw/VideoScene;->mCanSeekBack:Z

    iput-boolean v2, p0, Lcom/mediatek/vlw/VideoScene;->mCanSeekForward:Z

    goto :goto_0
.end method


# virtual methods
.method public canPause()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mCanPause:Z

    return v0
.end method

.method public canSeekBackward()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mCanSeekBack:Z

    return v0
.end method

.method public canSeekForward()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mCanSeekForward:Z

    return v0
.end method

.method public destroy()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    iput-boolean v7, p0, Lcom/mediatek/vlw/VideoScene;->mStartFromBoot:Z

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-direct {p0, v6}, Lcom/mediatek/vlw/VideoScene;->release(Z)V

    iget-boolean v2, p0, Lcom/mediatek/vlw/VideoScene;->mPreview:Z

    if-nez v2, :cond_0

    invoke-direct {p0, v6, v6}, Lcom/mediatek/vlw/VideoScene;->clear(ZZ)V

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->stopAndReleaseVideoObserver()V

    const-string v2, "VideoScene"

    const-string v3, "destroy VideoScene"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_1

    const-string v2, "VideoScene"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unregister receiver: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/vlw/VideoScene;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_2
    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mTvOut:Lcom/mediatek/common/tvout/ITVOUTNative;

    invoke-interface {v2, v7}, Lcom/mediatek/common/tvout/ITVOUTNative;->disableVideoMode(Z)Z

    const-string v2, "VideoScene"

    const-string v3, "exit vlw, enable video mode"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mWallpaperManager:Landroid/app/WallpaperManager;

    invoke-virtual {v2}, Landroid/app/WallpaperManager;->getDesiredMinimumWidth()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mWallpaperManager:Landroid/app/WallpaperManager;

    invoke-virtual {v2}, Landroid/app/WallpaperManager;->getDesiredMinimumHeight()I

    move-result v0

    iget v2, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperWidth:I

    if-ne v2, v1, :cond_3

    iget v2, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperHeight:I

    if-eq v2, v0, :cond_4

    :cond_3
    const-string v2, "VideoScene"

    const-string v3, "restore wallpaper dimensions: (%d,%d)-->(%d,%d)"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x2

    iget v6, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperWidth:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget v6, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperHeight:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mWallpaperManager:Landroid/app/WallpaperManager;

    iget v3, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperWidth:I

    iget v4, p0, Lcom/mediatek/vlw/VideoScene;->mOriginWallpaperHeight:I

    invoke-virtual {v2, v3, v4}, Landroid/app/WallpaperManager;->suggestDesiredDimensions(II)V

    :cond_4
    return-void
.end method

.method public doCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/os/Bundle;
    .param p6    # Z

    const/4 v0, 0x0

    return-object v0
.end method

.method public getBufferPercentage()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentBufferPercentage:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    :cond_0
    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    return v0
.end method

.method public getDuration()I
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mDuration:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mDuration:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/mediatek/vlw/VideoScene;->mDuration:I

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mDuration:I

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/vlw/VideoScene;->mDuration:I

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mDuration:I

    goto :goto_0
.end method

.method public getSurfaceHeight()I
    .locals 1

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mSurfaceHeight:I

    return v0
.end method

.method public getSurfaceWidth()I
    .locals 1

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mSurfaceWidth:I

    return v0
.end method

.method public init(Landroid/view/SurfaceHolder;)V
    .locals 6
    .param p1    # Landroid/view/SurfaceHolder;

    const/4 v5, 0x5

    const/4 v4, 0x1

    const-string v1, "VideoScene"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "init VideoScene, sh: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v4, p0, Lcom/mediatek/vlw/VideoScene;->mStartFromBoot:Z

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->loadSettings()V

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-boolean v1, p0, Lcom/mediatek/vlw/VideoScene;->mPreview:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/mediatek/vlw/VideoScene;->mStartTime:I

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/mediatek/vlw/VideoScene;->mSeekWhenPrepared:J

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->openVideo()V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    const-string v1, "VideoScene"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "register receiver: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/vlw/VideoScene;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_1
    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.ACTION_PRE_SHUTDOWN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.ACTION_BOOT_IPO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.REBOOT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/mediatek/vlw/VideoScene;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_2
    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mTvOut:Lcom/mediatek/common/tvout/ITVOUTNative;

    invoke-interface {v1, v4}, Lcom/mediatek/common/tvout/ITVOUTNative;->disableVideoMode(Z)Z

    const-string v1, "VideoScene"

    const-string v2, "start vlw, disable video mode"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    invoke-static {v1}, Lcom/mediatek/vlw/Utils;->isDefaultVideo(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->isInPlaybackState()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/vlw/VideoScene;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7530

    invoke-virtual {v1, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_3
    return-void
.end method

.method public isPlaying()Z
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPreview()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mPreview:Z

    return v0
.end method

.method public pause()V
    .locals 4

    const/4 v1, 0x4

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    iput v1, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentState:I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    :cond_0
    iput v1, p0, Lcom/mediatek/vlw/VideoScene;->mTargetState:I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->saveSettings()V

    return-void
.end method

.method public resize(Landroid/view/SurfaceHolder;II)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I

    const-string v0, "VideoScene"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSurfaceChanged("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), sh: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput p2, p0, Lcom/mediatek/vlw/VideoScene;->mSurfaceWidth:I

    iput p3, p0, Lcom/mediatek/vlw/VideoScene;->mSurfaceHeight:I

    return-void
.end method

.method public resume()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->openVideo()V

    return-void
.end method

.method public seekTo(J)V
    .locals 2
    .param p1    # J

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/vlw/VideoScene;->mSeekWhenPrepared:J

    :goto_0
    return-void

    :cond_0
    iput-wide p1, p0, Lcom/mediatek/vlw/VideoScene;->mSeekWhenPrepared:J

    goto :goto_0
.end method

.method public setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V
    .locals 0
    .param p1    # Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene;->mOnBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    return-void
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0
    .param p1    # Landroid/media/MediaPlayer$OnCompletionListener;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    return-void
.end method

.method public setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 0
    .param p1    # Landroid/media/MediaPlayer$OnErrorListener;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 0
    .param p1    # Landroid/media/MediaPlayer$OnPreparedListener;

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    return-void
.end method

.method public setVideoPath(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/vlw/VideoScene;->setVideoURI(Landroid/net/Uri;)V

    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/mediatek/vlw/VideoScene;->setVideoURI(Landroid/net/Uri;Ljava/util/Map;)V

    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;Ljava/util/Map;)V
    .locals 2
    .param p1    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/vlw/VideoScene;->mUri:Landroid/net/Uri;

    iput-object p2, p0, Lcom/mediatek/vlw/VideoScene;->mHeaders:Ljava/util/Map;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/mediatek/vlw/VideoScene;->mSeekWhenPrepared:J

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->openVideo()V

    return-void
.end method

.method public setVisibility(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x1

    const-string v0, "VideoScene"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVisibility("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mVisible:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lcom/mediatek/vlw/VideoScene;->mVisible:Z

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->checkEnvironment()V

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->addAndStartVideoObserver()V

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mPreview:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/mediatek/vlw/VideoScene;->mSeekWhenPrepared:J

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/vlw/VideoScene;->start()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mPreview:Z

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->saveSettings()V

    :cond_3
    invoke-virtual {p0}, Lcom/mediatek/vlw/VideoScene;->pause()V

    invoke-direct {p0, v3}, Lcom/mediatek/vlw/VideoScene;->release(Z)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method public start()V
    .locals 3

    const/4 v2, 0x3

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mHasShutdown:Z

    if-eqz v0, :cond_0

    const-string v0, "VideoScene"

    const-string v1, "shuting down, do not start to play"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->checkEnvironment()V

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->addAndStartVideoObserver()V

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    iput v2, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentState:I

    :cond_1
    iput v2, p0, Lcom/mediatek/vlw/VideoScene;->mTargetState:I

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_2
    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method public startPlayback()V
    .locals 2

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mPreview:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/mediatek/vlw/VideoScene;->mCurrentPos:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/mediatek/vlw/VideoScene;->mSeekWhenPrepared:J

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->openVideo()V

    invoke-virtual {p0}, Lcom/mediatek/vlw/VideoScene;->start()V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    return-void
.end method

.method public stopPlayback()V
    .locals 2

    iget-boolean v0, p0, Lcom/mediatek/vlw/VideoScene;->mPreview:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/vlw/VideoScene;->saveSettings()V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/vlw/VideoScene;->release(Z)V

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mBucketId:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vlw/VideoScene;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_1
    return-void
.end method

.method public suspend()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/vlw/VideoScene;->release(Z)V

    return-void
.end method
