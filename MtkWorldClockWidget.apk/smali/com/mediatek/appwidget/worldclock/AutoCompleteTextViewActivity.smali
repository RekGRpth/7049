.class public Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;
.super Landroid/app/Activity;
.source "AutoCompleteTextViewActivity.java"


# static fields
.field private static final ON_CLICK_APPWIDGETID:Ljava/lang/String; = "onClickAppWidgetId"

.field static final TAG:Ljava/lang/String; = "AutoCompleteTextViewActivity"

.field private static final TIMEZONE_ID:I = 0x0

.field private static final WEATHER_ID:I = 0x1


# instance fields
.field buttonListener:Landroid/view/View$OnClickListener;

.field private focusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private mAdapterCityArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAdapterLocalCityArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAppwidgetId:I

.field private mAutoComplete:Landroid/widget/AutoCompleteTextView;

.field private mCityInfo:Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

.field private mCityNameArrayBak:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCityNumberInXml:I

.field private mTimeZoneArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mWeatherIDArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private onkeyListener:Landroid/view/View$OnKeyListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mCityNameArrayBak:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mTimeZoneArray:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mWeatherIDArray:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mAdapterCityArray:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mAdapterLocalCityArray:Ljava/util/ArrayList;

    new-instance v0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    invoke-direct {v0}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;-><init>()V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mCityInfo:Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    new-instance v0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$1;-><init>(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;)V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->focusChangeListener:Landroid/view/View$OnFocusChangeListener;

    new-instance v0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$2;

    invoke-direct {v0, p0}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$2;-><init>(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;)V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->onkeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$3;

    invoke-direct {v0, p0}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$3;-><init>(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;)V

    iput-object v0, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->buttonListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;)Landroid/widget/AutoCompleteTextView;
    .locals 1
    .param p0    # Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;

    iget-object v0, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mAutoComplete:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;
    .locals 1
    .param p0    # Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;

    iget-object v0, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mCityInfo:Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;Lcom/mediatek/appwidget/worldclock/ClockCityInfo;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;
    .locals 0
    .param p0    # Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;
    .param p1    # Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    iput-object p1, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mCityInfo:Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    return-object p1
.end method

.method static synthetic access$200(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;Ljava/lang/String;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;
    .locals 1
    .param p0    # Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->setCityInfo(Ljava/lang/String;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    move-result-object v0

    return-object v0
.end method

.method private getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$6;

    invoke-direct {v0, p0}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$6;-><init>(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;)V

    return-object v0
.end method

.method private getLocalGMTString()Ljava/lang/String;
    .locals 10

    const/16 v9, 0x30

    const/16 v8, 0xa

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GMT"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-gez v3, :cond_1

    const/16 v6, 0x2d

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_0
    const v6, 0x36ee80

    div-int v0, v4, v6

    if-ge v0, v8, :cond_2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :goto_1
    const/16 v6, 0x3a

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const v6, 0xea60

    div-int v1, v4, v6

    rem-int/lit8 v1, v1, 0x3c

    if-ge v1, v8, :cond_0

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    :cond_1
    const/16 v6, 0x2b

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private setCityInfo(Ljava/lang/String;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    invoke-direct {v0}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;-><init>()V

    invoke-virtual {v0, p1}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->setCityName(Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mCityNumberInXml:I

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mCityNameArrayBak:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->setIndex(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mTimeZoneArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->setTimeZone(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mWeatherIDArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->setWeatherID(Ljava/lang/String;)V

    :cond_0
    return-object v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getZones()V
    .locals 13

    const/4 v10, 0x0

    invoke-direct {p0}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->getLocalGMTString()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f040001

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v10

    :cond_0
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v11

    const/4 v12, 0x2

    if-ne v11, v12, :cond_0

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->next()I

    const/4 v4, 0x0

    const/16 v11, 0x1f4

    new-array v5, v11, [Ljava/lang/String;

    const/16 v11, 0x1f4

    new-array v7, v11, [Ljava/lang/String;

    const/16 v11, 0x1f4

    new-array v6, v11, [Ljava/lang/String;

    :goto_0
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v11

    const/4 v12, 0x3

    if-eq v11, v12, :cond_8

    :goto_1
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v11

    const/4 v12, 0x2

    if-eq v11, v12, :cond_3

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->getEventType()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_2

    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_1
    :goto_2
    return-void

    :cond_2
    :try_start_1
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v9

    :try_start_2
    const-string v11, "AutoCompleteTextViewActivity"

    const-string v12, "Ill-formatted timezones.xml file"

    invoke-static {v11, v12}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->close()V

    goto :goto_2

    :cond_3
    :try_start_3
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "timezone"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    const/4 v11, 0x0

    invoke-interface {v10, v11}, Landroid/content/res/XmlResourceParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v1

    const/4 v11, 0x1

    invoke-interface {v10, v11}, Landroid/content/res/XmlResourceParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->nextText()Ljava/lang/String;

    move-result-object v0

    const/16 v11, 0x1f4

    if-ge v4, v11, :cond_5

    iget-object v11, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mCityNameArrayBak:Ljava/util/ArrayList;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v11, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mAdapterCityArray:Ljava/util/ArrayList;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v11, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mTimeZoneArray:Ljava/util/ArrayList;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    iget-object v11, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mAdapterLocalCityArray:Ljava/util/ArrayList;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v11, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mWeatherIDArray:Ljava/util/ArrayList;

    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    :cond_5
    :goto_3
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v11

    const/4 v12, 0x3

    if-eq v11, v12, :cond_6

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    :catch_1
    move-exception v2

    :try_start_4
    const-string v11, "AutoCompleteTextViewActivity"

    const-string v12, "Unable to read timezones.xml file"

    invoke-static {v11, v12}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->close()V

    goto :goto_2

    :cond_6
    :try_start_5
    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v11

    if-eqz v10, :cond_7

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_7
    throw v11

    :cond_8
    :try_start_6
    iput v4, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mCityNumberInXml:I

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_6
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/content/res/XmlResourceParser;->close()V

    goto/16 :goto_2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v5, "AutoCompleteTextViewActivity"

    const-string v6, "bundle != null"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "onClickAppWidgetId"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mAppwidgetId:I

    :cond_0
    iget-object v5, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mTimeZoneArray:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "AutoCompleteTextViewActivity"

    const-string v6, "mTimeZoneArray.isEmpty()"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->getZones()V

    :cond_1
    const-string v5, "AutoCompleteTextViewActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mAppwidgetId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mAppwidgetId:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v5, 0x7f030000

    invoke-virtual {p0, v5}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070006

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewArrayAdapter;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x109000a

    iget-object v7, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mAdapterCityArray:Ljava/util/ArrayList;

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    invoke-direct {v0, v5, v6, v7}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->getComparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewArrayAdapter;->sort(Ljava/util/Comparator;)V

    const/high16 v5, 0x7f090000

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/AutoCompleteTextView;

    iput-object v5, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mAutoComplete:Landroid/widget/AutoCompleteTextView;

    iget-object v5, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mAutoComplete:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v5, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v5, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mAutoComplete:Landroid/widget/AutoCompleteTextView;

    new-instance v6, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$4;

    invoke-direct {v6, p0}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$4;-><init>(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v5, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mAutoComplete:Landroid/widget/AutoCompleteTextView;

    iget-object v6, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->onkeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v5, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->mAutoComplete:Landroid/widget/AutoCompleteTextView;

    iget-object v6, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->focusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    new-instance v4, Ljava/util/Timer;

    invoke-direct {v4}, Ljava/util/Timer;-><init>()V

    new-instance v5, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$5;

    invoke-direct {v5, p0}, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity$5;-><init>(Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;)V

    const-wide/16 v6, 0x12c

    invoke-virtual {v4, v5, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    :cond_2
    const v5, 0x7f090001

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iget-object v5, p0, Lcom/mediatek/appwidget/worldclock/AutoCompleteTextViewActivity;->buttonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
