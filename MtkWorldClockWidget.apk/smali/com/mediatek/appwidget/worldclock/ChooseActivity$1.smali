.class Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;
.super Ljava/lang/Object;
.source "ChooseActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/appwidget/worldclock/ChooseActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/appwidget/worldclock/ChooseActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 11
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    move-object v3, p2

    check-cast v3, Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    const/4 v8, 0x2

    invoke-virtual {v4, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    iget-object v9, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-static {v9, v7}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->access$100(Lcom/mediatek/appwidget/worldclock/ChooseActivity;Ljava/lang/String;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->access$002(Lcom/mediatek/appwidget/worldclock/ChooseActivity;Lcom/mediatek/appwidget/worldclock/ClockCityInfo;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    const/4 v5, 0x0

    iget-object v8, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-static {v8}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->access$000(Lcom/mediatek/appwidget/worldclock/ChooseActivity;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mediatek/appwidget/worldclock/ClockCityInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    const v9, 0x7f070008

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-static {v2}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-static {v2}, Lcom/mediatek/appwidget/worldclock/ClockCityUtils;->initPreference(Landroid/content/Context;)V

    iget-object v8, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-static {v8}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->access$200(Lcom/mediatek/appwidget/worldclock/ChooseActivity;)I

    move-result v8

    iget-object v9, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-static {v9}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->access$000(Lcom/mediatek/appwidget/worldclock/ChooseActivity;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    move-result-object v9

    invoke-static {v0, v8, v9}, Lcom/mediatek/appwidget/worldclock/ClockCityUtils;->savePreferences(Landroid/appwidget/AppWidgetManager;ILcom/mediatek/appwidget/worldclock/ClockCityInfo;)V

    iget-object v8, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-static {v8}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->access$200(Lcom/mediatek/appwidget/worldclock/ChooseActivity;)I

    move-result v8

    iget-object v9, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-static {v9}, Lcom/mediatek/appwidget/worldclock/ChooseActivity;->access$000(Lcom/mediatek/appwidget/worldclock/ChooseActivity;)Lcom/mediatek/appwidget/worldclock/ClockCityInfo;

    move-result-object v9

    invoke-static {v2, v8, v9}, Lcom/mediatek/appwidget/worldclock/WorldClockWidgetProvider;->updateCity(Landroid/content/Context;ILcom/mediatek/appwidget/worldclock/ClockCityInfo;)V

    iget-object v8, p0, Lcom/mediatek/appwidget/worldclock/ChooseActivity$1;->this$0:Lcom/mediatek/appwidget/worldclock/ChooseActivity;

    invoke-virtual {v8}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
