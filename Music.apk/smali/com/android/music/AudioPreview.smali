.class public Lcom/android/music/AudioPreview;
.super Landroid/app/Activity;
.source "AudioPreview.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnDurationUpdateListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/music/AudioPreview$PreviewPlayer;,
        Lcom/android/music/AudioPreview$ProgressRefresher;
    }
.end annotation


# static fields
.field private static final OPEN_IN_MUSIC:I = 0x1

.field private static final TAG:Ljava/lang/String; = "AudioPreview"


# instance fields
.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mDuration:I

.field private mIsComplete:Z

.field private mLoadingText:Landroid/widget/TextView;

.field private mMediaCanSeek:Z

.field private mMediaId:J

.field private mPauseRefreshingProgressBar:Z

.field private mPausedByTransientLossOfFocus:Z

.field private mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

.field private mProgressRefresher:Landroid/os/Handler;

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mSeeking:Z

.field private mTextLine1:Landroid/widget/TextView;

.field private mTextLine2:Landroid/widget/TextView;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v2, p0, Lcom/android/music/AudioPreview;->mSeeking:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/music/AudioPreview;->mDuration:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/music/AudioPreview;->mMediaId:J

    iput-boolean v2, p0, Lcom/android/music/AudioPreview;->mPauseRefreshingProgressBar:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/music/AudioPreview;->mMediaCanSeek:Z

    iput-boolean v2, p0, Lcom/android/music/AudioPreview;->mIsComplete:Z

    new-instance v0, Lcom/android/music/AudioPreview$2;

    invoke-direct {v0, p0}, Lcom/android/music/AudioPreview$2;-><init>(Lcom/android/music/AudioPreview;)V

    iput-object v0, p0, Lcom/android/music/AudioPreview;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    new-instance v0, Lcom/android/music/AudioPreview$3;

    invoke-direct {v0, p0}, Lcom/android/music/AudioPreview$3;-><init>(Lcom/android/music/AudioPreview;)V

    iput-object v0, p0, Lcom/android/music/AudioPreview;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-void
.end method

.method static synthetic access$1000(Lcom/android/music/AudioPreview;)I
    .locals 1
    .param p0    # Lcom/android/music/AudioPreview;

    iget v0, p0, Lcom/android/music/AudioPreview;->mDuration:I

    return v0
.end method

.method static synthetic access$102(Lcom/android/music/AudioPreview;J)J
    .locals 0
    .param p0    # Lcom/android/music/AudioPreview;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/music/AudioPreview;->mMediaId:J

    return-wide p1
.end method

.method static synthetic access$1100(Lcom/android/music/AudioPreview;)Z
    .locals 1
    .param p0    # Lcom/android/music/AudioPreview;

    iget-boolean v0, p0, Lcom/android/music/AudioPreview;->mIsComplete:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/android/music/AudioPreview;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/AudioPreview;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/AudioPreview;->mIsComplete:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/android/music/AudioPreview;)Landroid/widget/SeekBar;
    .locals 1
    .param p0    # Lcom/android/music/AudioPreview;

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/music/AudioPreview;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/music/AudioPreview;

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/music/AudioPreview;)Z
    .locals 1
    .param p0    # Lcom/android/music/AudioPreview;

    iget-boolean v0, p0, Lcom/android/music/AudioPreview;->mPauseRefreshingProgressBar:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/android/music/AudioPreview;)Z
    .locals 1
    .param p0    # Lcom/android/music/AudioPreview;

    iget-boolean v0, p0, Lcom/android/music/AudioPreview;->mMediaCanSeek:Z

    return v0
.end method

.method static synthetic access$200(Lcom/android/music/AudioPreview;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/music/AudioPreview;

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mTextLine1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/music/AudioPreview;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/music/AudioPreview;

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mTextLine2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/music/AudioPreview;)Lcom/android/music/AudioPreview$PreviewPlayer;
    .locals 1
    .param p0    # Lcom/android/music/AudioPreview;

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/music/AudioPreview;)Landroid/media/AudioManager;
    .locals 1
    .param p0    # Lcom/android/music/AudioPreview;

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/music/AudioPreview;)Z
    .locals 1
    .param p0    # Lcom/android/music/AudioPreview;

    iget-boolean v0, p0, Lcom/android/music/AudioPreview;->mPausedByTransientLossOfFocus:Z

    return v0
.end method

.method static synthetic access$602(Lcom/android/music/AudioPreview;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/AudioPreview;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/AudioPreview;->mPausedByTransientLossOfFocus:Z

    return p1
.end method

.method static synthetic access$700(Lcom/android/music/AudioPreview;)V
    .locals 0
    .param p0    # Lcom/android/music/AudioPreview;

    invoke-direct {p0}, Lcom/android/music/AudioPreview;->start()V

    return-void
.end method

.method static synthetic access$800(Lcom/android/music/AudioPreview;)V
    .locals 0
    .param p0    # Lcom/android/music/AudioPreview;

    invoke-direct {p0}, Lcom/android/music/AudioPreview;->updatePlayPause()V

    return-void
.end method

.method static synthetic access$900(Lcom/android/music/AudioPreview;)Z
    .locals 1
    .param p0    # Lcom/android/music/AudioPreview;

    iget-boolean v0, p0, Lcom/android/music/AudioPreview;->mSeeking:Z

    return v0
.end method

.method static synthetic access$902(Lcom/android/music/AudioPreview;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/AudioPreview;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/AudioPreview;->mSeeking:Z

    return p1
.end method

.method private showPostPrepareUI()V
    .locals 12

    const/16 v11, 0x8

    const/4 v10, 0x0

    const-wide/32 v0, 0x7fffffff

    const v6, 0x7f0c0013

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    invoke-virtual {v4, v11}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v6, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v6}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v6

    iput v6, p0, Lcom/android/music/AudioPreview;->mDuration:I

    const-string v6, "AudioPreview"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mDuration:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/music/AudioPreview;->mDuration:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v6, "AudioPreview"

    invoke-static {v6, v3}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/android/music/AudioPreview;->mMediaCanSeek:Z

    const-string v2, ".imy"

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, ".imy"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget v6, p0, Lcom/android/music/AudioPreview;->mDuration:I

    int-to-long v6, v6

    const-wide/32 v8, 0x7fffffff

    cmp-long v6, v6, v8

    if-nez v6, :cond_0

    iput-boolean v10, p0, Lcom/android/music/AudioPreview;->mMediaCanSeek:Z

    :cond_0
    iget v6, p0, Lcom/android/music/AudioPreview;->mDuration:I

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    iget v7, p0, Lcom/android/music/AudioPreview;->mDuration:I

    invoke-virtual {v6, v7}, Landroid/widget/AbsSeekBar;->setMax(I)V

    iget-object v6, p0, Lcom/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v6, v10}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_1
    iget-object v6, p0, Lcom/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v7, p0, Lcom/android/music/AudioPreview;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v6, v7}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v6, p0, Lcom/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/view/View;->isInTouchMode()Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    :cond_2
    iget-object v6, p0, Lcom/android/music/AudioPreview;->mLoadingText:Landroid/widget/TextView;

    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    const v6, 0x7f0c0016

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Lcom/android/music/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    iget-object v7, p0, Lcom/android/music/AudioPreview;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v8, 0x3

    const/4 v9, 0x2

    invoke-virtual {v6, v7, v8, v9}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    iget-object v6, p0, Lcom/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    new-instance v7, Lcom/android/music/AudioPreview$ProgressRefresher;

    invoke-direct {v7, p0}, Lcom/android/music/AudioPreview$ProgressRefresher;-><init>(Lcom/android/music/AudioPreview;)V

    const-wide/16 v8, 0xc8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    invoke-direct {p0}, Lcom/android/music/AudioPreview;->updatePlayPause()V

    return-void
.end method

.method private start()V
    .locals 4

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    new-instance v1, Lcom/android/music/AudioPreview$ProgressRefresher;

    invoke-direct {v1, p0}, Lcom/android/music/AudioPreview$ProgressRefresher;-><init>(Lcom/android/music/AudioPreview;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private stopPlayback()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput-object v1, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    :cond_1
    return-void
.end method

.method private updatePlayPause()V
    .locals 3

    const v1, 0x7f0c0019

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f020011

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f020012

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;

    const-string v0, "AudioPreview"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCompletion Position:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/music/AudioPreview;->updatePlayPause()V

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getMax()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/music/AudioPreview;->mIsComplete:Z

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    if-nez v9, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v9}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    if-nez v1, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v13

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setVolumeControlStream(I)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v1, 0x7f030003

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/Window;->setCloseOnTouchOutside(Z)V

    const v1, 0x7f0c0017

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/music/AudioPreview;->mTextLine1:Landroid/widget/TextView;

    const v1, 0x7f0c0018

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/music/AudioPreview;->mTextLine2:Landroid/widget/TextView;

    const v1, 0x7f0c0014

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/music/AudioPreview;->mLoadingText:Landroid/widget/TextView;

    const-string v1, "http"

    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x7f070059

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mLoadingText:Landroid/widget/TextView;

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const v1, 0x7f0c0015

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    const-string v1, "audio"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/android/music/AudioPreview;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {p0}, Landroid/app/Activity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/music/AudioPreview$PreviewPlayer;

    if-nez v12, :cond_5

    new-instance v1, Lcom/android/music/AudioPreview$PreviewPlayer;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/android/music/AudioPreview$PreviewPlayer;-><init>(Lcom/android/music/AudioPreview$1;)V

    iput-object v1, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1, p0}, Lcom/android/music/AudioPreview$PreviewPlayer;->setActivity(Lcom/android/music/AudioPreview;)V

    :try_start_0
    iget-object v1, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    iget-object v2, p0, Lcom/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/android/music/AudioPreview$PreviewPlayer;->setDataSourceAndPrepare(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_2
    new-instance v0, Lcom/android/music/AudioPreview$1;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/music/AudioPreview$1;-><init>(Lcom/android/music/AudioPreview;Landroid/content/ContentResolver;)V

    const-string v1, "content"

    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    const-string v2, "media"

    if-ne v1, v2, :cond_6

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "title"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "artist"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/music/AudioPreview;->mLoadingText:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :catch_0
    move-exception v8

    const-string v1, "AudioPreview"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to open file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v1, 0x7f070056

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_5
    iput-object v12, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1, p0}, Lcom/android/music/AudioPreview$PreviewPlayer;->setActivity(Lcom/android/music/AudioPreview;)V

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1}, Lcom/android/music/AudioPreview$PreviewPlayer;->isPrepared()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/android/music/AudioPreview;->showPostPrepareUI()V

    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v1, "file"

    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v11

    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "title"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "artist"

    aput-object v6, v4, v5

    const-string v5, "_data=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v11, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    iget-object v1, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1}, Lcom/android/music/AudioPreview$PreviewPlayer;->isPrepared()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/music/AudioPreview;->setNames()V

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    const-string v0, "open in music"

    invoke-interface {p1, v1, v2, v1, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    return v2
.end method

.method public onDestroy()V
    .locals 0

    invoke-direct {p0}, Lcom/android/music/AudioPreview;->stopPlayback()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onDurationUpdate(Landroid/media/MediaPlayer;I)V
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I

    if-lez p2, :cond_0

    iput p2, p0, Lcom/android/music/AudioPreview;->mDuration:I

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/android/music/AudioPreview;->mDuration:I

    invoke-virtual {v0, v1}, Landroid/widget/AbsSeekBar;->setMax(I)V

    :cond_0
    const-string v0, "AudioPreview"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDurationUpdate("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/music/AudioPreview;->mDuration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const v0, 0x7f070056

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 4
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    const-string v1, "AudioPreview"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onInfo:: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x35e

    if-ne p2, v1, :cond_0

    const v1, 0x7f070056

    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    iget-object v1, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->pause()V

    :goto_1
    invoke-direct {p0}, Lcom/android/music/AudioPreview;->updatePlayPause()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/music/AudioPreview;->start()V

    goto :goto_1

    :sswitch_2
    invoke-direct {p0}, Lcom/android/music/AudioPreview;->start()V

    invoke-direct {p0}, Lcom/android/music/AudioPreview;->updatePlayPause()V

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->pause()V

    :cond_1
    invoke-direct {p0}, Lcom/android/music/AudioPreview;->updatePlayPause()V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lcom/android/music/AudioPreview;->stopPlayback()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_4
        0x4f -> :sswitch_1
        0x55 -> :sswitch_1
        0x56 -> :sswitch_4
        0x57 -> :sswitch_0
        0x58 -> :sswitch_0
        0x59 -> :sswitch_0
        0x5a -> :sswitch_0
        0x7e -> :sswitch_2
        0x7f -> :sswitch_3
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    const-string v0, "AudioPreview"

    const-string v1, "onPause for stop ProgressRefresher!"

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/music/AudioPreview;->mPauseRefreshingProgressBar:Z

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1    # Landroid/view/Menu;

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/music/AudioPreview;->mMediaId:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_0
    return v5

    :cond_0
    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    check-cast p1, Lcom/android/music/AudioPreview$PreviewPlayer;

    iput-object p1, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {p0}, Lcom/android/music/AudioPreview;->setNames()V

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    invoke-direct {p0}, Lcom/android/music/AudioPreview;->showPostPrepareUI()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/16 v0, 0xc8

    const-string v1, "AudioPreview"

    const-string v2, "onResume for start ProgressRefresher!"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/android/music/AudioPreview;->mPauseRefreshingProgressBar:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/music/AudioPreview;->mPauseRefreshingProgressBar:Z

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mProgressRefresher:Landroid/os/Handler;

    new-instance v2, Lcom/android/music/AudioPreview$ProgressRefresher;

    invoke-direct {v2, p0}, Lcom/android/music/AudioPreview$ProgressRefresher;-><init>(Lcom/android/music/AudioPreview;)V

    const-wide/16 v3, 0xc8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    return-object v0
.end method

.method public onUserLeaveHint()V
    .locals 0

    invoke-direct {p0}, Lcom/android/music/AudioPreview;->stopPlayback()V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    return-void
.end method

.method public playPauseClicked(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mPlayer:Lcom/android/music/AudioPreview$PreviewPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/music/AudioPreview;->mIsComplete:Z

    invoke-direct {p0}, Lcom/android/music/AudioPreview;->updatePlayPause()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/music/AudioPreview;->start()V

    goto :goto_0
.end method

.method public setNames()V
    .locals 2

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mTextLine1:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mTextLine1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/music/AudioPreview;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/android/music/AudioPreview;->mTextLine2:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/music/AudioPreview;->mTextLine2:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/music/AudioPreview;->mTextLine2:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
