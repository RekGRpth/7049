.class Lcom/android/music/CreatePlaylist$2;
.super Ljava/lang/Object;
.source "CreatePlaylist.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/music/CreatePlaylist;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/music/CreatePlaylist;


# direct methods
.method constructor <init>(Lcom/android/music/CreatePlaylist;)V
    .locals 0

    iput-object p1, p0, Lcom/android/music/CreatePlaylist$2;->this$0:Lcom/android/music/CreatePlaylist;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v8, -0x1

    if-ne p2, v8, :cond_2

    iget-object v5, p0, Lcom/android/music/CreatePlaylist$2;->this$0:Lcom/android/music/CreatePlaylist;

    invoke-static {v5}, Lcom/android/music/CreatePlaylist;->access$100(Lcom/android/music/CreatePlaylist;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    iget-object v5, p0, Lcom/android/music/CreatePlaylist$2;->this$0:Lcom/android/music/CreatePlaylist;

    invoke-virtual {v5}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v5, p0, Lcom/android/music/CreatePlaylist$2;->this$0:Lcom/android/music/CreatePlaylist;

    invoke-static {v5, v1}, Lcom/android/music/CreatePlaylist;->access$200(Lcom/android/music/CreatePlaylist;Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_1

    sget-object v5, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    int-to-long v6, v0

    invoke-static {v5, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    iget-object v5, p0, Lcom/android/music/CreatePlaylist$2;->this$0:Lcom/android/music/CreatePlaylist;

    invoke-static {v5, v0}, Lcom/android/music/MusicUtils;->clearPlaylist(Landroid/content/Context;I)V

    :goto_0
    iget-object v5, p0, Lcom/android/music/CreatePlaylist$2;->this$0:Lcom/android/music/CreatePlaylist;

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v6, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v5, v8, v6}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    iget-object v5, p0, Lcom/android/music/CreatePlaylist$2;->this$0:Lcom/android/music/CreatePlaylist;

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    new-instance v4, Landroid/content/ContentValues;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/content/ContentValues;-><init>(I)V

    const-string v5, "name"

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v5, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v5, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    goto :goto_0

    :cond_2
    const/4 v5, -0x3

    if-ne p2, v5, :cond_0

    iget-object v5, p0, Lcom/android/music/CreatePlaylist$2;->this$0:Lcom/android/music/CreatePlaylist;

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    goto :goto_1
.end method
