.class public Lcom/android/music/CreatePlaylist;
.super Landroid/app/Activity;
.source "CreatePlaylist.java"


# static fields
.field private static final ALERT_DIALOG_KEY:I = 0x0

.field private static final TAG:Ljava/lang/String; = "CreatePlaylist"


# instance fields
.field private mButtonClicked:Landroid/content/DialogInterface$OnClickListener;

.field private mDialog:Lcom/android/music/MusicDialog;

.field private mPlaylist:Landroid/widget/EditText;

.field private mPrompt:Ljava/lang/String;

.field private mSaveButton:Landroid/widget/Button;

.field private mScanListener:Landroid/content/BroadcastReceiver;

.field mTextWatcher:Landroid/text/TextWatcher;

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/music/CreatePlaylist$1;

    invoke-direct {v0, p0}, Lcom/android/music/CreatePlaylist$1;-><init>(Lcom/android/music/CreatePlaylist;)V

    iput-object v0, p0, Lcom/android/music/CreatePlaylist;->mTextWatcher:Landroid/text/TextWatcher;

    new-instance v0, Lcom/android/music/CreatePlaylist$2;

    invoke-direct {v0, p0}, Lcom/android/music/CreatePlaylist$2;-><init>(Lcom/android/music/CreatePlaylist;)V

    iput-object v0, p0, Lcom/android/music/CreatePlaylist;->mButtonClicked:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/android/music/CreatePlaylist$3;

    invoke-direct {v0, p0}, Lcom/android/music/CreatePlaylist$3;-><init>(Lcom/android/music/CreatePlaylist;)V

    iput-object v0, p0, Lcom/android/music/CreatePlaylist;->mScanListener:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/android/music/CreatePlaylist;)V
    .locals 0
    .param p0    # Lcom/android/music/CreatePlaylist;

    invoke-direct {p0}, Lcom/android/music/CreatePlaylist;->setSaveButton()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/music/CreatePlaylist;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/android/music/CreatePlaylist;

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mPlaylist:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/music/CreatePlaylist;Ljava/lang/String;)I
    .locals 1
    .param p0    # Lcom/android/music/CreatePlaylist;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/music/CreatePlaylist;->idForplaylist(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private idForplaylist(Ljava/lang/String;)I
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v8, 0x0

    sget-object v1, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v8

    const-string v3, "name=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v8

    const-string v5, "name"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/android/music/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, -0x1

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    return v7
.end method

.method private makePlaylistName()Ljava/lang/String;
    .locals 15

    const/4 v4, 0x0

    const/4 v14, 0x1

    const/4 v13, 0x0

    const v1, 0x7f070046

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v8, 0x1

    new-array v2, v14, [Ljava/lang/String;

    const-string v1, "name"

    aput-object v1, v2, v13

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "name != \'\'"

    sget-object v1, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "name"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_0

    :goto_0
    return-object v4

    :cond_0
    new-array v1, v14, [Ljava/lang/Object;

    add-int/lit8 v9, v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v13

    invoke-static {v12, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    const/4 v7, 0x0

    move v8, v9

    :cond_1
    if-nez v7, :cond_3

    const/4 v7, 0x1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v6, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v11}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    new-array v1, v14, [Ljava/lang/Object;

    add-int/lit8 v9, v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v13

    invoke-static {v12, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    const/4 v7, 0x0

    move v8, v9

    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v4, v11

    goto :goto_0
.end method

.method private setSaveButton()V
    .locals 4

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mPlaylist:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    if-nez v1, :cond_0

    const-string v1, "CreatePlaylist"

    const-string v2, "setSaveButton with dialog is null return!"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    invoke-virtual {v1}, Lcom/android/music/MusicDialog;->getPositiveButton()Landroid/widget/Button;

    move-result-object v1

    iput-object v1, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    :cond_1
    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_2
    :goto_1
    const-string v1, "CreatePlaylist"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSaveButton "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-direct {p0, v0}, Lcom/android/music/CreatePlaylist;->idForplaylist(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_4

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    const v2, 0x7f070049

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mSaveButton:Landroid/widget/Button;

    const v2, 0x7f070048

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setVolumeControlStream(I)V

    invoke-virtual {p0, v6}, Landroid/app/Activity;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030005

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/music/CreatePlaylist;->mView:Landroid/view/View;

    iget-object v3, p0, Lcom/android/music/CreatePlaylist;->mView:Landroid/view/View;

    const v4, 0x7f0c0021

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/android/music/CreatePlaylist;->mPlaylist:Landroid/widget/EditText;

    if-eqz p1, :cond_0

    const-string v3, "defaultname"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_1
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/music/CreatePlaylist;->makePlaylistName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v3, 0x7f070068

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/music/CreatePlaylist;->mPrompt:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/music/CreatePlaylist;->mPlaylist:Landroid/widget/EditText;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/music/CreatePlaylist;->mPlaylist:Landroid/widget/EditText;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v3, p0, Lcom/android/music/CreatePlaylist;->mPlaylist:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/android/music/CreatePlaylist;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "file"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/music/CreatePlaylist;->mScanListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1    # I

    if-nez p1, :cond_0

    new-instance v0, Lcom/android/music/MusicDialog;

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mButtonClicked:Landroid/content/DialogInterface$OnClickListener;

    iget-object v2, p0, Lcom/android/music/CreatePlaylist;->mView:Landroid/view/View;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/music/MusicDialog;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/view/View;)V

    iput-object v0, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mPrompt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/music/MusicDialog;->setPositiveButton(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/music/MusicDialog;->setNeutralButton(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/music/MusicDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mDialog:Lcom/android/music/MusicDialog;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/music/CreatePlaylist;->mScanListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->showDialog(I)V

    invoke-direct {p0}, Lcom/android/music/CreatePlaylist;->setSaveButton()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "defaultname"

    iget-object v1, p0, Lcom/android/music/CreatePlaylist;->mPlaylist:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
