.class public Lcom/android/music/DeleteItems;
.super Landroid/app/Activity;
.source "DeleteItems.java"


# static fields
.field private static final ALERT_DIALOG_KEY:I = 0x1

.field private static final FINISH:I = 0x1

.field private static final PROGRESS_DIALOG_KEY:I = 0x0

.field private static final START_DELETING:I = 0x0

.field private static final TAG:Ljava/lang/String; = "DeleteItems"


# instance fields
.field private mButtonClicked:Landroid/content/DialogInterface$OnClickListener;

.field mDesc:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mItemList:[J

.field private mScanListener:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/music/DeleteItems;->mDesc:Ljava/lang/String;

    new-instance v0, Lcom/android/music/DeleteItems$1;

    invoke-direct {v0, p0}, Lcom/android/music/DeleteItems$1;-><init>(Lcom/android/music/DeleteItems;)V

    iput-object v0, p0, Lcom/android/music/DeleteItems;->mButtonClicked:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/android/music/DeleteItems$2;

    invoke-direct {v0, p0}, Lcom/android/music/DeleteItems$2;-><init>(Lcom/android/music/DeleteItems;)V

    iput-object v0, p0, Lcom/android/music/DeleteItems;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/music/DeleteItems$3;

    invoke-direct {v0, p0}, Lcom/android/music/DeleteItems$3;-><init>(Lcom/android/music/DeleteItems;)V

    iput-object v0, p0, Lcom/android/music/DeleteItems;->mScanListener:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/android/music/DeleteItems;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/music/DeleteItems;

    iget-object v0, p0, Lcom/android/music/DeleteItems;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/music/DeleteItems;)V
    .locals 0
    .param p0    # Lcom/android/music/DeleteItems;

    invoke-direct {p0}, Lcom/android/music/DeleteItems;->doDeleteItems()V

    return-void
.end method

.method private doDeleteItems()V
    .locals 5

    iget-object v2, p0, Lcom/android/music/DeleteItems;->mItemList:[J

    invoke-static {p0, v2}, Lcom/android/music/MusicUtils;->deleteTracks(Landroid/content/Context;[J)I

    move-result v0

    iget-object v2, p0, Lcom/android/music/DeleteItems;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v0, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lcom/android/music/DeleteItems;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setVolumeControlStream(I)V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Landroid/app/Activity;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "description"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/music/DeleteItems;->mDesc:Ljava/lang/String;

    const-string v2, "items"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v2

    iput-object v2, p0, Lcom/android/music/DeleteItems;->mItemList:[J

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "file"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/music/DeleteItems;->mScanListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .param p1    # I

    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    const-string v2, "DeleteItems"

    const-string v3, "onCreateDialog with undefined id!"

    invoke-static {v2, v3}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const/high16 v2, 0x7f070000

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/android/music/MusicDialog;

    iget-object v2, p0, Lcom/android/music/DeleteItems;->mButtonClicked:Landroid/content/DialogInterface$OnClickListener;

    invoke-direct {v0, p0, v2, v1}, Lcom/android/music/MusicDialog;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/view/View;)V

    const v2, 0x7f07000f

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setTitle(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070015

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/music/MusicDialog;->setPositiveButton(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070057

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/music/MusicDialog;->setNeutralButton(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/music/DeleteItems;->mDesc:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {v0, v5}, Lcom/android/music/MusicDialog;->setCancelable(Z)V

    move-object v1, v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/music/DeleteItems;->mScanListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 2

    const-string v1, "statusbar"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->showDialog(I)V

    const-string v1, "statusbar"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    return-void
.end method
