.class public Lcom/android/music/MediaButtonIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MediaButtonIntentReceiver.java"


# static fields
.field private static final ACTION_TIME:I = 0x1f4

.field private static final LONG_PRESS_DELAY:I = 0x3e8

.field private static final MSG_LONGPRESS_TIMEOUT:I = 0x1

.field private static mDown:Z

.field private static mLastClickTime:J

.field private static mLaunched:Z

.field private static sHandler:Landroid/os/Handler;

.field private static sStartTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    sput-wide v1, Lcom/android/music/MediaButtonIntentReceiver;->mLastClickTime:J

    sput-boolean v0, Lcom/android/music/MediaButtonIntentReceiver;->mDown:Z

    sput-boolean v0, Lcom/android/music/MediaButtonIntentReceiver;->mLaunched:Z

    sput-wide v1, Lcom/android/music/MediaButtonIntentReceiver;->sStartTime:J

    new-instance v0, Lcom/android/music/MediaButtonIntentReceiver$1;

    invoke-direct {v0}, Lcom/android/music/MediaButtonIntentReceiver$1;-><init>()V

    sput-object v0, Lcom/android/music/MediaButtonIntentReceiver;->sHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    sget-boolean v0, Lcom/android/music/MediaButtonIntentReceiver;->mLaunched:Z

    return v0
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/android/music/MediaButtonIntentReceiver;->mLaunched:Z

    return p0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 15
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    const-string v11, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v11, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    new-instance v8, Landroid/content/Intent;

    const-class v11, Lcom/android/music/MediaPlaybackService;

    move-object/from16 v0, p1

    invoke-direct {v8, v0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v11, "com.android.music.musicservicecommand"

    invoke-virtual {v8, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v11, "command"

    const-string v12, "pause"

    invoke-virtual {v8, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v11, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {v11, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    const-string v11, "android.intent.extra.KEY_EVENT"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/view/KeyEvent;

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v10

    invoke-virtual {v5}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    invoke-virtual {v5}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v6

    sget-wide v11, Lcom/android/music/MediaButtonIntentReceiver;->mLastClickTime:J

    sub-long v3, v6, v11

    const/4 v2, 0x0

    sparse-switch v10, :sswitch_data_0

    :goto_1
    if-eqz v2, :cond_0

    if-nez v1, :cond_a

    sget-boolean v11, Lcom/android/music/MediaButtonIntentReceiver;->mDown:Z

    if-eqz v11, :cond_6

    const-string v11, "togglepause"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    const-string v11, "play"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    :cond_2
    sget-wide v11, Lcom/android/music/MediaButtonIntentReceiver;->mLastClickTime:J

    const-wide/16 v13, 0x0

    cmp-long v11, v11, v13

    if-eqz v11, :cond_3

    sget-wide v11, Lcom/android/music/MediaButtonIntentReceiver;->mLastClickTime:J

    sub-long v11, v6, v11

    const-wide/16 v13, 0x3e8

    cmp-long v11, v11, v13

    if-lez v11, :cond_3

    sget-object v11, Lcom/android/music/MediaButtonIntentReceiver;->sHandler:Landroid/os/Handler;

    sget-object v12, Lcom/android/music/MediaButtonIntentReceiver;->sHandler:Landroid/os/Handler;

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v12, v13, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_3
    const-string v11, "forward"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    const-string v11, "rewind"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    :cond_4
    sget-wide v11, Lcom/android/music/MediaButtonIntentReceiver;->mLastClickTime:J

    const-wide/16 v13, 0x0

    cmp-long v11, v11, v13

    if-eqz v11, :cond_5

    const-wide/16 v11, 0x1f4

    cmp-long v11, v3, v11

    if-lez v11, :cond_5

    move-object/from16 v0, p1

    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/android/music/MediaButtonIntentReceiver;->sendToStartService(Landroid/content/Context;Ljava/lang/String;J)V

    sput-wide v6, Lcom/android/music/MediaButtonIntentReceiver;->mLastClickTime:J

    :cond_5
    :goto_2
    invoke-virtual {p0}, Landroid/content/BroadcastReceiver;->isOrderedBroadcast()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-virtual {p0}, Landroid/content/BroadcastReceiver;->abortBroadcast()V

    goto/16 :goto_0

    :sswitch_0
    const-string v2, "stop"

    goto :goto_1

    :sswitch_1
    const-string v2, "togglepause"

    goto :goto_1

    :sswitch_2
    const-string v2, "next"

    goto :goto_1

    :sswitch_3
    const-string v2, "previous"

    goto :goto_1

    :sswitch_4
    const-string v2, "pause"

    goto :goto_1

    :sswitch_5
    const-string v2, "play"

    goto :goto_1

    :sswitch_6
    const-string v2, "forward"

    goto :goto_1

    :sswitch_7
    const-string v2, "rewind"

    goto :goto_1

    :cond_6
    invoke-virtual {v5}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v11

    if-nez v11, :cond_5

    new-instance v8, Landroid/content/Intent;

    const-class v11, Lcom/android/music/MediaPlaybackService;

    move-object/from16 v0, p1

    invoke-direct {v8, v0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v11, "com.android.music.musicservicecommand"

    invoke-virtual {v8, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/16 v11, 0x4f

    if-ne v10, v11, :cond_7

    sget-wide v11, Lcom/android/music/MediaButtonIntentReceiver;->mLastClickTime:J

    sub-long v11, v6, v11

    const-wide/16 v13, 0x12c

    cmp-long v11, v11, v13

    if-gez v11, :cond_7

    const-string v11, "command"

    const-string v12, "next"

    invoke-virtual {v8, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const-wide/16 v11, 0x0

    sput-wide v11, Lcom/android/music/MediaButtonIntentReceiver;->mLastClickTime:J

    :goto_3
    const/4 v11, 0x0

    sput-boolean v11, Lcom/android/music/MediaButtonIntentReceiver;->mLaunched:Z

    const/4 v11, 0x1

    sput-boolean v11, Lcom/android/music/MediaButtonIntentReceiver;->mDown:Z

    goto :goto_2

    :cond_7
    const-string v11, "forward"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_8

    const-string v11, "rewind"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    :cond_8
    sput-wide v6, Lcom/android/music/MediaButtonIntentReceiver;->mLastClickTime:J

    sput-wide v6, Lcom/android/music/MediaButtonIntentReceiver;->sStartTime:J

    goto :goto_3

    :cond_9
    const-string v11, "command"

    invoke-virtual {v8, v11, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    sput-wide v6, Lcom/android/music/MediaButtonIntentReceiver;->mLastClickTime:J

    goto :goto_3

    :cond_a
    const-string v11, "forward"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_b

    const-string v11, "rewind"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    :cond_b
    move-object/from16 v0, p1

    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/android/music/MediaButtonIntentReceiver;->sendToStartService(Landroid/content/Context;Ljava/lang/String;J)V

    const-wide/16 v11, 0x0

    sput-wide v11, Lcom/android/music/MediaButtonIntentReceiver;->mLastClickTime:J

    :cond_c
    sget-object v11, Lcom/android/music/MediaButtonIntentReceiver;->sHandler:Landroid/os/Handler;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v11, 0x0

    sput-boolean v11, Lcom/android/music/MediaButtonIntentReceiver;->mDown:Z

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_1
        0x55 -> :sswitch_1
        0x56 -> :sswitch_0
        0x57 -> :sswitch_2
        0x58 -> :sswitch_3
        0x59 -> :sswitch_7
        0x5a -> :sswitch_6
        0x7e -> :sswitch_5
        0x7f -> :sswitch_4
    .end sparse-switch
.end method

.method public sendToStartService(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # J

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/music/MediaPlaybackService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.android.music.musicservicecommand"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "command"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "deltatime"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
