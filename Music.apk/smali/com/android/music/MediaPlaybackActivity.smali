.class public Lcom/android/music/MediaPlaybackActivity;
.super Landroid/app/Activity;
.source "MediaPlaybackActivity.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/android/music/MusicUtils$Defs;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/music/MediaPlaybackActivity$Worker;,
        Lcom/android/music/MediaPlaybackActivity$AlbumArtHandler;,
        Lcom/android/music/MediaPlaybackActivity$AlbumSongIdWrapper;
    }
.end annotation


# static fields
.field private static final ALBUM_ART_DECODED:I = 0x4

.field private static final FM_TX_ACTIVITY:Ljava/lang/String; = "com.mediatek.FMTransmitter.FMTransmitterActivity"

.field private static final FM_TX_PACKAGE:Ljava/lang/String; = "com.mediatek.FMTransmitter"

.field private static final GET_ALBUM_ART:I = 0x3

.field private static final NEXT_BUTTON:I = 0x6

.field private static final NEXT_TEST:Ljava/lang/String; = "next song"

.field private static final PLAY_TEST:Ljava/lang/String; = "play song"

.field private static final PREV_BUTTON:I = 0x7

.field private static final PREV_TEST:Ljava/lang/String; = "prev song"

.field private static final QUIT:I = 0x2

.field private static final REFRESH:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MediaPlayback"

.field private static final USE_AS_RINGTONE:I = 0x10


# instance fields
.field private final keyboard:[[I

.field private lastX:I

.field private lastY:I

.field private mAddToPlaylistSubmenu:Landroid/view/SubMenu;

.field private mAlbum:Landroid/widget/ImageView;

.field private mAlbumArtHandler:Lcom/android/music/MediaPlaybackActivity$AlbumArtHandler;

.field private mAlbumArtWorker:Lcom/android/music/MediaPlaybackActivity$Worker;

.field private mAlbumName:Landroid/widget/TextView;

.field private mArtBitmap:Landroid/graphics/Bitmap;

.field private mArtSongId:J

.field private mArtistName:Landroid/widget/TextView;

.field private mCurrentTime:Landroid/widget/TextView;

.field private mDeviceHasDpad:Z

.field mDraggingLabel:Z

.field private mDuration:J

.field private mFfwdListener:Lcom/android/music/RepeatingImageButton$RepeatListener;

.field private mFromTouch:Z

.field private final mHandler:Landroid/os/Handler;

.field mInitialX:I

.field private mIsCallOnStop:Z

.field private mIsInBackgroud:Z

.field private mIsLandScape:Z

.field private mIsShowAlbumArt:Z

.field mLabelScroller:Landroid/os/Handler;

.field private mLastSeekEventTime:J

.field mLastX:I

.field private mNeedUpdateDuration:Z

.field private mNextButton:Lcom/android/music/RepeatingImageButton;

.field private mNextListener:Landroid/view/View$OnClickListener;

.field private mPauseButton:Landroid/widget/ImageButton;

.field private mPauseListener:Landroid/view/View$OnClickListener;

.field private mPerformanceTestString:Ljava/lang/String;

.field private mPosOverride:J

.field private mPrevButton:Lcom/android/music/RepeatingImageButton;

.field private mPrevListener:Landroid/view/View$OnClickListener;

.field private mProgress:Landroid/widget/ProgressBar;

.field mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

.field private mQueueButton:Landroid/widget/ImageButton;

.field private mQueueListener:Landroid/view/View$OnClickListener;

.field private mQueueMenuItem:Landroid/view/MenuItem;

.field private mRepeatButton:Landroid/widget/ImageButton;

.field private mRepeatCount:I

.field private mRepeatListener:Landroid/view/View$OnClickListener;

.field private mRepeatMenuItem:Landroid/view/MenuItem;

.field private mRewListener:Lcom/android/music/RepeatingImageButton$RepeatListener;

.field private mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mSeeking:Z

.field private mService:Lcom/android/music/IMediaPlaybackService;

.field private mShuffleButton:Landroid/widget/ImageButton;

.field private mShuffleListener:Landroid/view/View$OnClickListener;

.field private mShuffleMenuItem:Landroid/view/MenuItem;

.field private mStartSeekPos:J

.field private mStatusListener:Landroid/content/BroadcastReceiver;

.field mTextWidth:I

.field private mToast:Landroid/widget/Toast;

.field private mToken:Lcom/android/music/MusicUtils$ServiceToken;

.field private mTotalTime:Landroid/widget/TextView;

.field private mTouchSlop:I

.field private mTrackName:Landroid/widget/TextView;

.field mViewWidth:I

.field private osc:Landroid/content/ServiceConnection;

.field private paused:Z

.field private seekmethod:I


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v4, 0xa

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackActivity;->mSeeking:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/music/MediaPlaybackActivity;->mStartSeekPos:J

    iput-object v5, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackActivity;->mIsShowAlbumArt:Z

    iput-object v5, p0, Lcom/android/music/MediaPlaybackActivity;->mArtBitmap:Landroid/graphics/Bitmap;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/music/MediaPlaybackActivity;->mArtSongId:J

    iput-object v5, p0, Lcom/android/music/MediaPlaybackActivity;->mPerformanceTestString:Ljava/lang/String;

    iput v2, p0, Lcom/android/music/MediaPlaybackActivity;->mRepeatCount:I

    iput-boolean v6, p0, Lcom/android/music/MediaPlaybackActivity;->mNeedUpdateDuration:Z

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackActivity;->mIsInBackgroud:Z

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackActivity;->mIsCallOnStop:Z

    iput v2, p0, Lcom/android/music/MediaPlaybackActivity;->mInitialX:I

    iput v2, p0, Lcom/android/music/MediaPlaybackActivity;->mLastX:I

    iput v3, p0, Lcom/android/music/MediaPlaybackActivity;->mTextWidth:I

    iput v3, p0, Lcom/android/music/MediaPlaybackActivity;->mViewWidth:I

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackActivity;->mDraggingLabel:Z

    new-instance v0, Lcom/android/music/MediaPlaybackActivity$1;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackActivity$1;-><init>(Lcom/android/music/MediaPlaybackActivity;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mLabelScroller:Landroid/os/Handler;

    new-instance v0, Lcom/android/music/MediaPlaybackActivity$2;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackActivity$2;-><init>(Lcom/android/music/MediaPlaybackActivity;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/android/music/MediaPlaybackActivity$3;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackActivity$3;-><init>(Lcom/android/music/MediaPlaybackActivity;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mQueueListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/music/MediaPlaybackActivity$4;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackActivity$4;-><init>(Lcom/android/music/MediaPlaybackActivity;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mShuffleListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/music/MediaPlaybackActivity$5;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackActivity$5;-><init>(Lcom/android/music/MediaPlaybackActivity;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mRepeatListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/music/MediaPlaybackActivity$6;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackActivity$6;-><init>(Lcom/android/music/MediaPlaybackActivity;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mPauseListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/music/MediaPlaybackActivity$7;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackActivity$7;-><init>(Lcom/android/music/MediaPlaybackActivity;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mPrevListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/music/MediaPlaybackActivity$8;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackActivity$8;-><init>(Lcom/android/music/MediaPlaybackActivity;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mNextListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/android/music/MediaPlaybackActivity$9;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackActivity$9;-><init>(Lcom/android/music/MediaPlaybackActivity;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mRewListener:Lcom/android/music/RepeatingImageButton$RepeatListener;

    new-instance v0, Lcom/android/music/MediaPlaybackActivity$10;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackActivity$10;-><init>(Lcom/android/music/MediaPlaybackActivity;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mFfwdListener:Lcom/android/music/RepeatingImageButton$RepeatListener;

    const/4 v0, 0x3

    new-array v0, v0, [[I

    new-array v1, v4, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v3

    new-array v1, v4, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v6

    const/4 v1, 0x2

    new-array v2, v4, [I

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->keyboard:[[I

    new-instance v0, Lcom/android/music/MediaPlaybackActivity$11;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackActivity$11;-><init>(Lcom/android/music/MediaPlaybackActivity;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->osc:Landroid/content/ServiceConnection;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/music/MediaPlaybackActivity;->mPosOverride:J

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackActivity;->mFromTouch:Z

    new-instance v0, Lcom/android/music/MediaPlaybackActivity$12;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackActivity$12;-><init>(Lcom/android/music/MediaPlaybackActivity;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/music/MediaPlaybackActivity$13;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackActivity$13;-><init>(Lcom/android/music/MediaPlaybackActivity;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mStatusListener:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/music/MediaPlaybackActivity$14;

    invoke-direct {v0, p0}, Lcom/android/music/MediaPlaybackActivity$14;-><init>(Lcom/android/music/MediaPlaybackActivity;)V

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    return-void

    nop

    :array_0
    .array-data 4
        0x2d
        0x33
        0x21
        0x2e
        0x30
        0x35
        0x31
        0x25
        0x2b
        0x2c
    .end array-data

    :array_1
    .array-data 4
        0x1d
        0x2f
        0x20
        0x22
        0x23
        0x24
        0x26
        0x27
        0x28
        0x43
    .end array-data

    :array_2
    .array-data 4
        0x36
        0x34
        0x1f
        0x32
        0x1e
        0x2a
        0x29
        0x37
        0x38
        0x42
    .end array-data
.end method

.method static synthetic access$000(Lcom/android/music/MediaPlaybackActivity;)Z
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackActivity;->mFromTouch:Z

    return v0
.end method

.method static synthetic access$002(Lcom/android/music/MediaPlaybackActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/MediaPlaybackActivity;->mFromTouch:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/music/MediaPlaybackActivity;)Lcom/android/music/IMediaPlaybackService;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/android/music/MediaPlaybackActivity;I)I
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;
    .param p1    # I

    iput p1, p0, Lcom/android/music/MediaPlaybackActivity;->mRepeatCount:I

    return p1
.end method

.method static synthetic access$102(Lcom/android/music/MediaPlaybackActivity;Lcom/android/music/IMediaPlaybackService;)Lcom/android/music/IMediaPlaybackService;
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;
    .param p1    # Lcom/android/music/IMediaPlaybackService;

    iput-object p1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/music/MediaPlaybackActivity;IJ)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;
    .param p1    # I
    .param p2    # J

    invoke-direct {p0, p1, p2, p3}, Lcom/android/music/MediaPlaybackActivity;->scanBackward(IJ)V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/music/MediaPlaybackActivity;IJ)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;
    .param p1    # I
    .param p2    # J

    invoke-direct {p0, p1, p2, p3}, Lcom/android/music/MediaPlaybackActivity;->scanForward(IJ)V

    return-void
.end method

.method static synthetic access$1300(Lcom/android/music/MediaPlaybackActivity;)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->startPlayback()V

    return-void
.end method

.method static synthetic access$1400(Lcom/android/music/MediaPlaybackActivity;)Z
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackActivity;->mIsLandScape:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/android/music/MediaPlaybackActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mRepeatButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/music/MediaPlaybackActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mShuffleButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/music/MediaPlaybackActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mQueueButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/music/MediaPlaybackActivity;)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->setRepeatButtonImage()V

    return-void
.end method

.method static synthetic access$1900(Lcom/android/music/MediaPlaybackActivity;)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->setShuffleButtonImage()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/music/MediaPlaybackActivity;)J
    .locals 2
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-wide v0, p0, Lcom/android/music/MediaPlaybackActivity;->mPosOverride:J

    return-wide v0
.end method

.method static synthetic access$2000(Lcom/android/music/MediaPlaybackActivity;)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->setPauseButtonImage()V

    return-void
.end method

.method static synthetic access$202(Lcom/android/music/MediaPlaybackActivity;J)J
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/music/MediaPlaybackActivity;->mPosOverride:J

    return-wide p1
.end method

.method static synthetic access$2100(Lcom/android/music/MediaPlaybackActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbum:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/android/music/MediaPlaybackActivity;J)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/android/music/MediaPlaybackActivity;->queueNextRefresh(J)V

    return-void
.end method

.method static synthetic access$2300(Lcom/android/music/MediaPlaybackActivity;)Lcom/android/music/RepeatingImageButton;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mNextButton:Lcom/android/music/RepeatingImageButton;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/android/music/MediaPlaybackActivity;)Lcom/android/music/RepeatingImageButton;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mPrevButton:Lcom/android/music/RepeatingImageButton;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/android/music/MediaPlaybackActivity;)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->updateTrackInfo()V

    return-void
.end method

.method static synthetic access$2600(Lcom/android/music/MediaPlaybackActivity;)Z
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackActivity;->mIsShowAlbumArt:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/android/music/MediaPlaybackActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/MediaPlaybackActivity;->mIsShowAlbumArt:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/android/music/MediaPlaybackActivity;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mArtBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/android/music/MediaPlaybackActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/android/music/MediaPlaybackActivity;->mArtBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/android/music/MediaPlaybackActivity;)J
    .locals 2
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-wide v0, p0, Lcom/android/music/MediaPlaybackActivity;->mArtSongId:J

    return-wide v0
.end method

.method static synthetic access$2802(Lcom/android/music/MediaPlaybackActivity;J)J
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/music/MediaPlaybackActivity;->mArtSongId:J

    return-wide p1
.end method

.method static synthetic access$300(Lcom/android/music/MediaPlaybackActivity;)J
    .locals 2
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-wide v0, p0, Lcom/android/music/MediaPlaybackActivity;->mDuration:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/android/music/MediaPlaybackActivity;)J
    .locals 2
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->refreshNow()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$500(Lcom/android/music/MediaPlaybackActivity;)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->toggleShuffle()V

    return-void
.end method

.method static synthetic access$600(Lcom/android/music/MediaPlaybackActivity;)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->cycleRepeat()V

    return-void
.end method

.method static synthetic access$700(Lcom/android/music/MediaPlaybackActivity;)V
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->doPauseResume()V

    return-void
.end method

.method static synthetic access$800(Lcom/android/music/MediaPlaybackActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mPerformanceTestString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Lcom/android/music/MediaPlaybackActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/music/MediaPlaybackActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/music/MediaPlaybackActivity;->mPerformanceTestString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/android/music/MediaPlaybackActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/music/MediaPlaybackActivity;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private cycleRepeat()V
    .locals 3

    const/4 v2, 0x2

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v1}, Lcom/android/music/IMediaPlaybackService;->getRepeatMode()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Lcom/android/music/IMediaPlaybackService;->setRepeatMode(I)V

    const v1, 0x7f07003e

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackActivity;->showToast(I)V

    :goto_1
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->setRepeatButtonImage()V

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_1
    if-ne v0, v2, :cond_3

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/android/music/IMediaPlaybackService;->setRepeatMode(I)V

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v1}, Lcom/android/music/IMediaPlaybackService;->getShuffleMode()I

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/android/music/IMediaPlaybackService;->setShuffleMode(I)V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->setShuffleButtonImage()V

    :cond_2
    const v1, 0x7f07003d

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackActivity;->showToast(I)V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/android/music/IMediaPlaybackService;->setRepeatMode(I)V

    const v1, 0x7f07003c

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackActivity;->showToast(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method

.method private doPauseResume()V
    .locals 4

    :try_start_0
    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v1}, Lcom/android/music/IMediaPlaybackService;->isPlaying()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "MediaPlayback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doPauseResume: isPlaying="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/android/music/MediaPlaybackActivity;->mPosOverride:J

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v1}, Lcom/android/music/IMediaPlaybackService;->pause()V

    :goto_0
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->refreshNow()J

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->setPauseButtonImage()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v1}, Lcom/android/music/IMediaPlaybackService;->play()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method private getBackgroundColor()I
    .locals 3

    const v0, -0x33ff6634

    const v2, -0x33ff6634

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getThemeMainColor()I

    move-result v2

    if-nez v2, :cond_0

    const v2, -0x33ff6634

    :cond_0
    return v2
.end method

.method private queueNextRefresh(J)V
    .locals 3
    .param p1    # J

    const/4 v2, 0x1

    iget-boolean v1, p0, Lcom/android/music/MediaPlaybackActivity;->paused:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    return-void
.end method

.method private recordDurationUpdateStatus()V
    .locals 10

    const-string v6, "audio/mpeg"

    const-string v3, "audio/amr"

    const-string v4, "audio/amr-wb"

    const-string v2, "audio/aac"

    const-string v5, "audio/flac"

    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/android/music/MediaPlaybackActivity;->mNeedUpdateDuration:Z

    :try_start_0
    iget-object v7, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v7}, Lcom/android/music/IMediaPlaybackService;->getMIMEType()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    const-string v7, "MediaPlayback"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mimeType="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/music/MusicLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "audio/mpeg"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "audio/amr"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "audio/amr-wb"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "audio/aac"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "audio/flac"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/android/music/MediaPlaybackActivity;->mNeedUpdateDuration:Z

    :cond_1
    return-void

    :catch_0
    move-exception v0

    const-string v7, "MediaPlayback"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private refreshNow()J
    .locals 14

    const/16 v2, 0x12c

    iget-object v10, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-nez v10, :cond_1

    const-wide/16 v4, 0x1f4

    :cond_0
    :goto_0
    return-wide v4

    :cond_1
    :try_start_0
    iget-wide v10, p0, Lcom/android/music/MediaPlaybackActivity;->mPosOverride:J

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-gez v10, :cond_7

    iget-object v10, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v10}, Lcom/android/music/IMediaPlaybackService;->position()J

    move-result-wide v0

    :goto_1
    const-wide/16 v10, 0x12c

    add-long/2addr v10, v0

    iget-wide v12, p0, Lcom/android/music/MediaPlaybackActivity;->mDuration:J

    cmp-long v10, v10, v12

    if-lez v10, :cond_2

    const-string v10, "MediaPlayback"

    const-string v11, "refreshNow, do a workaround for position"

    invoke-static {v10, v11}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v0, p0, Lcom/android/music/MediaPlaybackActivity;->mDuration:J

    :cond_2
    const-wide/16 v10, 0x0

    cmp-long v10, v0, v10

    if-ltz v10, :cond_a

    iget-wide v10, p0, Lcom/android/music/MediaPlaybackActivity;->mDuration:J

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-lez v10, :cond_a

    iget-object v10, p0, Lcom/android/music/MediaPlaybackActivity;->mCurrentTime:Landroid/widget/TextView;

    const-wide/16 v11, 0x3e8

    div-long v11, v0, v11

    invoke-static {p0, v11, v12}, Lcom/android/music/MusicUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v10, p0, Lcom/android/music/MediaPlaybackActivity;->mFromTouch:Z

    if-nez v10, :cond_3

    const-wide/16 v10, 0x3e8

    mul-long/2addr v10, v0

    iget-wide v12, p0, Lcom/android/music/MediaPlaybackActivity;->mDuration:J

    div-long/2addr v10, v12

    long-to-int v3, v10

    iget-object v10, p0, Lcom/android/music/MediaPlaybackActivity;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v10, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    :cond_3
    iget-object v10, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v10}, Lcom/android/music/IMediaPlaybackService;->isPlaying()Z

    move-result v10

    if-nez v10, :cond_4

    iget v10, p0, Lcom/android/music/MediaPlaybackActivity;->mRepeatCount:I

    const/4 v11, -0x1

    if-le v10, v11, :cond_8

    :cond_4
    iget-object v10, p0, Lcom/android/music/MediaPlaybackActivity;->mCurrentTime:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    :goto_2
    invoke-direct {p0, v0, v1}, Lcom/android/music/MediaPlaybackActivity;->updateDuration(J)V

    const-wide/16 v10, 0x3e8

    const-wide/16 v12, 0x3e8

    rem-long v12, v0, v12

    sub-long v4, v10, v12

    iget-object v10, p0, Lcom/android/music/MediaPlaybackActivity;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v10}, Landroid/view/View;->getWidth()I

    move-result v9

    if-nez v9, :cond_6

    const/16 v9, 0x140

    :cond_6
    iget-wide v10, p0, Lcom/android/music/MediaPlaybackActivity;->mDuration:J

    int-to-long v12, v9

    div-long v6, v10, v12

    cmp-long v10, v6, v4

    if-gtz v10, :cond_0

    const-wide/16 v10, 0x14

    cmp-long v10, v6, v10

    if-gez v10, :cond_b

    const-wide/16 v4, 0x14

    goto/16 :goto_0

    :cond_7
    iget-wide v0, p0, Lcom/android/music/MediaPlaybackActivity;->mPosOverride:J

    goto :goto_1

    :cond_8
    iget-object v10, p0, Lcom/android/music/MediaPlaybackActivity;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/view/View;->getVisibility()I

    move-result v8

    iget-object v11, p0, Lcom/android/music/MediaPlaybackActivity;->mCurrentTime:Landroid/widget/TextView;

    const/4 v10, 0x4

    if-ne v8, v10, :cond_9

    const/4 v10, 0x0

    :goto_3
    invoke-virtual {v11, v10}, Landroid/view/View;->setVisibility(I)V

    const-wide/16 v4, 0x1f4

    goto/16 :goto_0

    :cond_9
    const/4 v10, 0x4

    goto :goto_3

    :cond_a
    iget-object v10, p0, Lcom/android/music/MediaPlaybackActivity;->mCurrentTime:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v10, p0, Lcom/android/music/MediaPlaybackActivity;->mCurrentTime:Landroid/widget/TextView;

    const-string v11, "0:00"

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v10, p0, Lcom/android/music/MediaPlaybackActivity;->mTotalTime:Landroid/widget/TextView;

    const-string v11, "--:--"

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v10, p0, Lcom/android/music/MediaPlaybackActivity;->mFromTouch:Z

    if-nez v10, :cond_5

    iget-object v10, p0, Lcom/android/music/MediaPlaybackActivity;->mProgress:Landroid/widget/ProgressBar;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ProgressBar;->setProgress(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v10

    const-wide/16 v4, 0x1f4

    goto/16 :goto_0

    :cond_b
    move-wide v4, v6

    goto/16 :goto_0
.end method

.method private scanBackward(IJ)V
    .locals 12
    .param p1    # I
    .param p2    # J

    const-wide/16 v6, 0x1388

    const-wide/16 v10, 0x0

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    :try_start_0
    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v4}, Lcom/android/music/IMediaPlaybackService;->position()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mStartSeekPos:J

    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mLastSeekEventTime:J

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/music/MediaPlaybackActivity;->mSeeking:Z

    goto :goto_0

    :catch_0
    move-exception v4

    goto :goto_0

    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/music/MediaPlaybackActivity;->mSeeking:Z

    cmp-long v4, p2, v6

    if-gez v4, :cond_5

    const-wide/16 v4, 0xa

    mul-long/2addr p2, v4

    :goto_1
    iget-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mStartSeekPos:J

    sub-long v2, v4, p2

    cmp-long v4, v2, v10

    if-gez v4, :cond_2

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v4}, Lcom/android/music/IMediaPlaybackService;->prev()V

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v4}, Lcom/android/music/IMediaPlaybackService;->duration()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mStartSeekPos:J

    add-long/2addr v4, v0

    iput-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mStartSeekPos:J

    add-long/2addr v2, v0

    :cond_2
    iget-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mLastSeekEventTime:J

    sub-long v4, p2, v4

    const-wide/16 v6, 0xfa

    cmp-long v4, v4, v6

    if-gtz v4, :cond_3

    if-gez p1, :cond_4

    :cond_3
    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v4, v2, v3}, Lcom/android/music/IMediaPlaybackService;->seek(J)J

    iput-wide p2, p0, Lcom/android/music/MediaPlaybackActivity;->mLastSeekEventTime:J

    :cond_4
    if-ltz p1, :cond_6

    iput-wide v2, p0, Lcom/android/music/MediaPlaybackActivity;->mPosOverride:J

    :goto_2
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->refreshNow()J

    goto :goto_0

    :cond_5
    const-wide/32 v4, 0xc350

    sub-long v6, p2, v6

    const-wide/16 v8, 0x28

    mul-long/2addr v6, v8

    add-long p2, v4, v6

    goto :goto_1

    :cond_6
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mPosOverride:J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2
.end method

.method private scanForward(IJ)V
    .locals 10
    .param p1    # I
    .param p2    # J

    const-wide/16 v6, 0x1388

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    :try_start_0
    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v4}, Lcom/android/music/IMediaPlaybackService;->position()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mStartSeekPos:J

    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mLastSeekEventTime:J

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/music/MediaPlaybackActivity;->mSeeking:Z

    goto :goto_0

    :catch_0
    move-exception v4

    goto :goto_0

    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/music/MediaPlaybackActivity;->mSeeking:Z

    cmp-long v4, p2, v6

    if-gez v4, :cond_5

    const-wide/16 v4, 0xa

    mul-long/2addr p2, v4

    :goto_1
    iget-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mStartSeekPos:J

    add-long v2, v4, p2

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v4}, Lcom/android/music/IMediaPlaybackService;->duration()J

    move-result-wide v0

    cmp-long v4, v2, v0

    if-ltz v4, :cond_2

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v4}, Lcom/android/music/IMediaPlaybackService;->next()V

    iget-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mStartSeekPos:J

    sub-long/2addr v4, v0

    iput-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mStartSeekPos:J

    sub-long/2addr v2, v0

    :cond_2
    iget-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mLastSeekEventTime:J

    sub-long v4, p2, v4

    const-wide/16 v6, 0xfa

    cmp-long v4, v4, v6

    if-gtz v4, :cond_3

    if-gez p1, :cond_4

    :cond_3
    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v4, v2, v3}, Lcom/android/music/IMediaPlaybackService;->seek(J)J

    iput-wide p2, p0, Lcom/android/music/MediaPlaybackActivity;->mLastSeekEventTime:J

    :cond_4
    if-ltz p1, :cond_6

    iput-wide v2, p0, Lcom/android/music/MediaPlaybackActivity;->mPosOverride:J

    :goto_2
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->refreshNow()J

    goto :goto_0

    :cond_5
    const-wide/32 v4, 0xc350

    sub-long v6, p2, v6

    const-wide/16 v8, 0x28

    mul-long/2addr v6, v8

    add-long p2, v4, v6

    goto :goto_1

    :cond_6
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mPosOverride:J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2
.end method

.method private seekMethod1(I)Z
    .locals 9
    .param p1    # I

    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v3, 0x0

    const/4 v6, -0x1

    const/4 v5, 0x2

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-nez v4, :cond_0

    :goto_0
    return v3

    :cond_0
    const/4 v1, 0x0

    :goto_1
    const/16 v4, 0xa

    if-ge v1, v4, :cond_c

    const/4 v2, 0x0

    :goto_2
    const/4 v4, 0x3

    if-ge v2, v4, :cond_b

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->keyboard:[[I

    aget-object v4, v4, v2

    aget v4, v4, v1

    if-ne v4, p1, :cond_a

    const/4 v0, 0x0

    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->lastX:I

    if-ne v1, v3, :cond_2

    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->lastY:I

    if-ne v2, v3, :cond_2

    const/4 v0, 0x0

    :cond_1
    :goto_3
    iput v1, p0, Lcom/android/music/MediaPlaybackActivity;->lastX:I

    iput v2, p0, Lcom/android/music/MediaPlaybackActivity;->lastY:I

    :try_start_0
    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v4}, Lcom/android/music/IMediaPlaybackService;->position()J

    move-result-wide v4

    mul-int/lit8 v6, v0, 0x5

    int-to-long v6, v6

    add-long/2addr v4, v6

    invoke-interface {v3, v4, v5}, Lcom/android/music/IMediaPlaybackService;->seek(J)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->refreshNow()J

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    if-nez v2, :cond_3

    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->lastY:I

    if-nez v3, :cond_3

    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->lastX:I

    if-le v1, v3, :cond_3

    const/4 v0, 0x1

    goto :goto_3

    :cond_3
    if-nez v2, :cond_4

    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->lastY:I

    if-nez v3, :cond_4

    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->lastX:I

    if-ge v1, v3, :cond_4

    const/4 v0, -0x1

    goto :goto_3

    :cond_4
    if-ne v2, v5, :cond_5

    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->lastY:I

    if-ne v3, v5, :cond_5

    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->lastX:I

    if-le v1, v3, :cond_5

    const/4 v0, -0x1

    goto :goto_3

    :cond_5
    if-ne v2, v5, :cond_6

    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->lastY:I

    if-ne v3, v5, :cond_6

    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->lastX:I

    if-ge v1, v3, :cond_6

    const/4 v0, 0x1

    goto :goto_3

    :cond_6
    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->lastY:I

    if-ge v2, v3, :cond_7

    if-gt v1, v7, :cond_7

    const/4 v0, 0x1

    goto :goto_3

    :cond_7
    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->lastY:I

    if-ge v2, v3, :cond_8

    if-lt v1, v8, :cond_8

    const/4 v0, -0x1

    goto :goto_3

    :cond_8
    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->lastY:I

    if-le v2, v3, :cond_9

    if-gt v1, v7, :cond_9

    const/4 v0, -0x1

    goto :goto_3

    :cond_9
    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->lastY:I

    if-le v2, v3, :cond_1

    if-lt v1, v8, :cond_1

    const/4 v0, 0x1

    goto :goto_3

    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    :cond_c
    iput v6, p0, Lcom/android/music/MediaPlaybackActivity;->lastX:I

    iput v6, p0, Lcom/android/music/MediaPlaybackActivity;->lastY:I

    goto/16 :goto_0

    :catch_0
    move-exception v3

    goto :goto_4
.end method

.method private seekMethod2(I)Z
    .locals 7
    .param p1    # I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    const/4 v0, 0x0

    :goto_1
    const/16 v3, 0xa

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->keyboard:[[I

    aget-object v3, v3, v2

    aget v3, v3, v0

    if-ne v3, p1, :cond_2

    mul-int/lit8 v2, v0, 0x64

    div-int/lit8 v1, v2, 0xa

    :try_start_0
    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v3}, Lcom/android/music/IMediaPlaybackService;->duration()J

    move-result-wide v3

    int-to-long v5, v1

    mul-long/2addr v3, v5

    const-wide/16 v5, 0x64

    div-long/2addr v3, v5

    invoke-interface {v2, v3, v4}, Lcom/android/music/IMediaPlaybackService;->seek(J)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->refreshNow()J

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v2

    goto :goto_2
.end method

.method private setPauseButtonImage()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v0}, Lcom/android/music/IMediaPlaybackService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mPauseButton:Landroid/widget/ImageButton;

    const v1, 0x1080023

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackActivity;->mSeeking:Z

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/music/MediaPlaybackActivity;->mPosOverride:J

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mPauseButton:Landroid/widget/ImageButton;

    const v1, 0x1080024

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private setRepeatButtonImage()V
    .locals 2

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v1}, Lcom/android/music/IMediaPlaybackService;->getRepeatMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const v0, 0x7f02002d

    :goto_1
    iget-boolean v1, p0, Lcom/android/music/MediaPlaybackActivity;->mIsLandScape:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mRepeatMenuItem:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :pswitch_0
    const v0, 0x7f02002c

    goto :goto_1

    :pswitch_1
    const v0, 0x7f02002e

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mRepeatButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private setShuffleButtonImage()V
    .locals 2

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v1}, Lcom/android/music/IMediaPlaybackService;->getShuffleMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f020031

    :goto_1
    iget-boolean v1, p0, Lcom/android/music/MediaPlaybackActivity;->mIsLandScape:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mShuffleMenuItem:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :pswitch_1
    const v0, 0x7f020030

    goto :goto_1

    :pswitch_2
    const v0, 0x7f020029

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mShuffleButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private showToast(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    const-string v0, ""

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mToast:Landroid/widget/Toast;

    :cond_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private startPlayback()V
    .locals 10

    iget-object v7, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-nez v7, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v1, ""

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_1

    invoke-virtual {v6}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    const-string v7, "file"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v6}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    :goto_1
    :try_start_0
    iget-object v7, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v7}, Lcom/android/music/IMediaPlaybackService;->stop()V

    iget-object v7, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v7, v1}, Lcom/android/music/IMediaPlaybackService;->openFile(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v7}, Lcom/android/music/IMediaPlaybackService;->play()V

    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0, v7}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_2
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->updateTrackInfo()V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->refreshNow()J

    move-result-wide v3

    invoke-direct {p0, v3, v4}, Lcom/android/music/MediaPlaybackActivity;->queueNextRefresh(J)V

    goto :goto_0

    :cond_2
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v7, "MediaPlayback"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "couldn\'t start playback: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private toggleShuffle()V
    .locals 4

    const/4 v2, 0x2

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v1}, Lcom/android/music/IMediaPlaybackService;->getShuffleMode()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/android/music/IMediaPlaybackService;->setShuffleMode(I)V

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v1}, Lcom/android/music/IMediaPlaybackService;->getRepeatMode()I

    move-result v1

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Lcom/android/music/IMediaPlaybackService;->setRepeatMode(I)V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->setRepeatButtonImage()V

    :cond_1
    const v1, 0x7f07003a

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackActivity;->showToast(I)V

    :goto_1
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->setShuffleButtonImage()V

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_2
    if-eq v0, v3, :cond_3

    if-ne v0, v2, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/android/music/IMediaPlaybackService;->setShuffleMode(I)V

    const v1, 0x7f07003b

    invoke-direct {p0, v1}, Lcom/android/music/MediaPlaybackActivity;->showToast(I)V

    goto :goto_1

    :cond_4
    const-string v1, "MediaPlayback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid shuffle mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method

.method private updateDuration(J)V
    .locals 9
    .param p1    # J

    const-wide/16 v5, 0x0

    const/16 v3, 0x3e8

    :try_start_0
    iget-boolean v4, p0, Lcom/android/music/MediaPlaybackActivity;->mNeedUpdateDuration:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v4}, Lcom/android/music/IMediaPlaybackService;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v4}, Lcom/android/music/IMediaPlaybackService;->duration()J

    move-result-wide v1

    cmp-long v4, v1, v5

    if-lez v4, :cond_0

    iget-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mDuration:J

    cmp-long v4, v1, v4

    if-eqz v4, :cond_0

    iput-wide v1, p0, Lcom/android/music/MediaPlaybackActivity;->mDuration:J

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/music/MediaPlaybackActivity;->mNeedUpdateDuration:Z

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mTotalTime:Landroid/widget/TextView;

    iget-wide v5, p0, Lcom/android/music/MediaPlaybackActivity;->mDuration:J

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-static {p0, v5, v6}, Lcom/android/music/MusicUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v4, "MediaPlayback"

    const-string v5, "new duration updated!!"

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    cmp-long v4, p1, v5

    if-ltz v4, :cond_2

    iget-wide v4, p0, Lcom/android/music/MediaPlaybackActivity;->mDuration:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_0

    :cond_2
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/music/MediaPlaybackActivity;->mNeedUpdateDuration:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "MediaPlayback"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateTrackInfo()V
    .locals 15

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-nez v8, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v8}, Lcom/android/music/IMediaPlaybackService;->getPath()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v8}, Lcom/android/music/IMediaPlaybackService;->getAudioId()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-gez v8, :cond_2

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    const-string v9, "http://"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mArtistName:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbumName:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbum:Landroid/widget/ImageView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mTrackName:Landroid/widget/TextView;

    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbumArtHandler:Lcom/android/music/MediaPlaybackActivity$AlbumArtHandler;

    const/4 v9, 0x3

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbumArtHandler:Lcom/android/music/MediaPlaybackActivity$AlbumArtHandler;

    const/4 v9, 0x3

    new-instance v10, Lcom/android/music/MediaPlaybackActivity$AlbumSongIdWrapper;

    const-wide/16 v11, -0x1

    const-wide/16 v13, -0x1

    invoke-direct {v10, v11, v12, v13, v14}, Lcom/android/music/MediaPlaybackActivity$AlbumSongIdWrapper;-><init>(JJ)V

    invoke-virtual {v8, v9, v10}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/Message;->sendToTarget()V

    :goto_1
    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v8}, Lcom/android/music/IMediaPlaybackService;->duration()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/android/music/MediaPlaybackActivity;->mDuration:J

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mTotalTime:Landroid/widget/TextView;

    iget-wide v9, p0, Lcom/android/music/MediaPlaybackActivity;->mDuration:J

    const-wide/16 v11, 0x3e8

    div-long/2addr v9, v11

    invoke-static {p0, v9, v10}, Lcom/android/music/MusicUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->recordDurationUpdateStatus()V

    goto :goto_0

    :cond_2
    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mArtistName:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbumName:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v8}, Lcom/android/music/IMediaPlaybackService;->getArtistName()Ljava/lang/String;

    move-result-object v3

    const-string v8, "<unknown>"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    const v8, 0x7f070038

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :cond_3
    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mArtistName:Landroid/widget/TextView;

    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v8}, Lcom/android/music/IMediaPlaybackService;->getAlbumName()Ljava/lang/String;

    move-result-object v0

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v8}, Lcom/android/music/IMediaPlaybackService;->getAlbumId()J

    move-result-wide v1

    const-string v8, "<unknown>"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    const v8, 0x7f070039

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, -0x1

    :cond_4
    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbumName:Landroid/widget/TextView;

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mTrackName:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v9}, Lcom/android/music/IMediaPlaybackService;->getTrackName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbumArtHandler:Lcom/android/music/MediaPlaybackActivity$AlbumArtHandler;

    const/4 v9, 0x3

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbumArtHandler:Lcom/android/music/MediaPlaybackActivity$AlbumArtHandler;

    const/4 v9, 0x3

    new-instance v10, Lcom/android/music/MediaPlaybackActivity$AlbumSongIdWrapper;

    invoke-direct {v10, v1, v2, v6, v7}, Lcom/android/music/MediaPlaybackActivity$AlbumSongIdWrapper;-><init>(JJ)V

    invoke-virtual {v8, v9, v10}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/Message;->sendToTarget()V

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbum:Landroid/widget/ImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private updateUI()V
    .locals 7

    const-wide/16 v5, 0x104

    const/4 v3, 0x1

    const v2, 0x7f030001

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    const v2, 0x7f0c000e

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mCurrentTime:Landroid/widget/TextView;

    const v2, 0x7f0c0012

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mTotalTime:Landroid/widget/TextView;

    const v2, 0x102000d

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mProgress:Landroid/widget/ProgressBar;

    const v2, 0x7f0c0006

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbum:Landroid/widget/ImageView;

    const v2, 0x7f0c000a

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mArtistName:Landroid/widget/TextView;

    const v2, 0x7f0c000b

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbumName:Landroid/widget/TextView;

    const v2, 0x7f0c000c

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mTrackName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mArtistName:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbumName:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mTrackName:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    const v2, 0x7f0c000f

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/music/RepeatingImageButton;

    iput-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mPrevButton:Lcom/android/music/RepeatingImageButton;

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mPrevButton:Lcom/android/music/RepeatingImageButton;

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mPrevListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mPrevButton:Lcom/android/music/RepeatingImageButton;

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mRewListener:Lcom/android/music/RepeatingImageButton$RepeatListener;

    invoke-virtual {v2, v4, v5, v6}, Lcom/android/music/RepeatingImageButton;->setRepeatListener(Lcom/android/music/RepeatingImageButton$RepeatListener;J)V

    const v2, 0x7f0c0010

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mPauseButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mPauseButton:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mPauseListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0c0011

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/music/RepeatingImageButton;

    iput-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mNextButton:Lcom/android/music/RepeatingImageButton;

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mNextButton:Lcom/android/music/RepeatingImageButton;

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mNextListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mNextButton:Lcom/android/music/RepeatingImageButton;

    iget-object v4, p0, Lcom/android/music/MediaPlaybackActivity;->mFfwdListener:Lcom/android/music/RepeatingImageButton$RepeatListener;

    invoke-virtual {v2, v4, v5, v6}, Lcom/android/music/RepeatingImageButton;->setRepeatListener(Lcom/android/music/RepeatingImageButton$RepeatListener;J)V

    iput v3, p0, Lcom/android/music/MediaPlaybackActivity;->seekmethod:I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->navigation:I

    const/4 v4, 0x2

    if-ne v2, v4, :cond_2

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/android/music/MediaPlaybackActivity;->mDeviceHasDpad:Z

    iget-boolean v2, p0, Lcom/android/music/MediaPlaybackActivity;->mIsLandScape:Z

    if-nez v2, :cond_0

    const v2, 0x7f0c0007

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mQueueButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mQueueButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mQueueListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0c0008

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mShuffleButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mShuffleButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mShuffleListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0c0009

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mRepeatButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mRepeatButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mRepeatListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mProgress:Landroid/widget/ProgressBar;

    instance-of v2, v2, Landroid/widget/SeekBar;

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mProgress:Landroid/widget/ProgressBar;

    check-cast v0, Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    :cond_1
    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mProgress:Landroid/widget/ProgressBar;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/android/music/MediaPlaybackActivity;->mTouchSlop:I

    return-void

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private useDpadMusicControl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackActivity;->mDeviceHasDpad:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mPrevButton:Lcom/android/music/RepeatingImageButton;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mNextButton:Lcom/android/music/RepeatingImageButton;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v3, -0x1

    if-eq p2, v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v3, 0x1

    new-array v0, v3, [J

    const/4 v3, 0x0

    invoke-static {}, Lcom/android/music/MusicUtils;->getCurrentAudioId()J

    move-result-wide v4

    aput-wide v4, v0, v3

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v3, v1

    invoke-static {p0, v0, v3, v4}, Lcom/android/music/MusicUtils;->addToPlaylist(Landroid/content/Context;[JJ)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1    # Landroid/content/res/Configuration;

    const/4 v3, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v2, v4, :cond_0

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/android/music/MediaPlaybackActivity;->mIsLandScape:Z

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackActivity;->mIsShowAlbumArt:Z

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->updateUI()V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->updateTrackInfo()V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->refreshNow()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/android/music/MediaPlaybackActivity;->queueNextRefresh(J)V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->setRepeatButtonImage()V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->setPauseButtonImage()V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->setShuffleButtonImage()V

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/music/MediaPlaybackActivity;->mPosOverride:J

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setVolumeControlStream(I)V

    new-instance v1, Lcom/android/music/MediaPlaybackActivity$Worker;

    const-string v3, "album art worker"

    invoke-direct {v1, v3}, Lcom/android/music/MediaPlaybackActivity$Worker;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbumArtWorker:Lcom/android/music/MediaPlaybackActivity$Worker;

    new-instance v1, Lcom/android/music/MediaPlaybackActivity$AlbumArtHandler;

    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbumArtWorker:Lcom/android/music/MediaPlaybackActivity$Worker;

    invoke-virtual {v3}, Lcom/android/music/MediaPlaybackActivity$Worker;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, p0, v3}, Lcom/android/music/MediaPlaybackActivity$AlbumArtHandler;-><init>(Lcom/android/music/MediaPlaybackActivity;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbumArtHandler:Lcom/android/music/MediaPlaybackActivity$AlbumArtHandler;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/android/music/MediaPlaybackActivity;->mIsLandScape:Z

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->updateUI()V

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 14
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-static {}, Lcom/android/music/MusicUtils;->getCurrentAudioId()J

    move-result-wide v7

    const-wide/16 v0, 0x0

    cmp-long v0, v7, v0

    if-ltz v0, :cond_4

    const/4 v0, 0x0

    const/16 v1, 0x8

    const/4 v2, 0x0

    const v3, 0x7f07000d

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const v3, 0x7f070043

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/view/Menu;->addSubMenu(IIII)Landroid/view/SubMenu;

    move-result-object v0

    const v1, 0x1080033

    invoke-interface {v0, v1}, Landroid/view/SubMenu;->setIcon(I)Landroid/view/SubMenu;

    move-result-object v0

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mAddToPlaylistSubmenu:Landroid/view/SubMenu;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "is_drm"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "drm_method"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v12, -0x1

    const/4 v9, -0x1

    const/4 v13, 0x0

    const/4 v10, 0x1

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    const/4 v0, 0x1

    if-ne v12, v0, :cond_2

    const/4 v0, 0x1

    if-ne v12, v0, :cond_3

    const/4 v0, 0x1

    if-ne v9, v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    const/16 v1, 0x10

    const/4 v2, 0x0

    const v3, 0x7f070040

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020025

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    :cond_3
    const/4 v0, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    const v3, 0x7f07000f

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02001d

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/4 v0, 0x0

    const/16 v1, 0xd

    const/4 v2, 0x0

    const v3, 0x7f070005

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02001e

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/4 v0, 0x0

    const/4 v1, 0x6

    const/4 v2, 0x0

    const v3, 0x7f07000c

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020020

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    invoke-virtual {p0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v11

    const/high16 v0, 0x7f0b0000

    invoke-virtual {v11, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f0c003d

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mQueueMenuItem:Landroid/view/MenuItem;

    const v0, 0x7f0c003e

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mShuffleMenuItem:Landroid/view/MenuItem;

    const v0, 0x7f0c003f

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mRepeatMenuItem:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    invoke-static {p0, p1, v0}, Lcom/android/music/MusicUtils;->addSearchView(Landroid/app/Activity;Landroid/view/Menu;Landroid/widget/SearchView$OnQueryTextListener;)Landroid/widget/SearchView;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mAlbumArtWorker:Lcom/android/music/MediaPlaybackActivity$Worker;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackActivity$Worker;->quit()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v2, 0x1

    const/4 v0, -0x1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->seekmethod:I

    if-nez v3, :cond_1

    invoke-direct {p0, p1}, Lcom/android/music/MediaPlaybackActivity;->seekMethod1(I)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    :goto_0
    :sswitch_0
    return v2

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/music/MediaPlaybackActivity;->seekMethod2(I)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    sparse-switch p1, :sswitch_data_0

    :cond_3
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0

    :sswitch_1
    iget v3, p0, Lcom/android/music/MediaPlaybackActivity;->seekmethod:I

    rsub-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/android/music/MediaPlaybackActivity;->seekmethod:I

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->useDpadMusicControl()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mPrevButton:Lcom/android/music/RepeatingImageButton;

    invoke-virtual {v3}, Landroid/view/View;->hasFocus()Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mPrevButton:Lcom/android/music/RepeatingImageButton;

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    :cond_4
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-direct {p0, v1, v3, v4}, Lcom/android/music/MediaPlaybackActivity;->scanBackward(IJ)V

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->useDpadMusicControl()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mNextButton:Lcom/android/music/RepeatingImageButton;

    invoke-virtual {v3}, Landroid/view/View;->hasFocus()Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mNextButton:Lcom/android/music/RepeatingImageButton;

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    :cond_5
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-direct {p0, v1, v3, v4}, Lcom/android/music/MediaPlaybackActivity;->scanForward(IJ)V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->toggleShuffle()V

    goto :goto_0

    :sswitch_5
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->doPauseResume()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x17 -> :sswitch_0
        0x2f -> :sswitch_4
        0x3e -> :sswitch_5
        0x4c -> :sswitch_1
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    :cond_1
    :goto_1
    return v1

    :pswitch_0
    :try_start_0
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->useDpadMusicControl()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/android/music/MediaPlaybackActivity;->mSeeking:Z

    if-nez v2, :cond_4

    iget-wide v2, p0, Lcom/android/music/MediaPlaybackActivity;->mStartSeekPos:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_4

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    iget-wide v2, p0, Lcom/android/music/MediaPlaybackActivity;->mStartSeekPos:J

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v2}, Lcom/android/music/IMediaPlaybackService;->prev()V

    :cond_2
    :goto_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/music/MediaPlaybackActivity;->mSeeking:Z

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/music/MediaPlaybackActivity;->mPosOverride:J

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    const-wide/16 v3, 0x0

    invoke-interface {v2, v3, v4}, Lcom/android/music/IMediaPlaybackService;->seek(J)J

    goto :goto_2

    :cond_4
    const/4 v2, -0x1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-direct {p0, v2, v3, v4}, Lcom/android/music/MediaPlaybackActivity;->scanBackward(IJ)V

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/music/MediaPlaybackActivity;->mStartSeekPos:J

    goto :goto_2

    :pswitch_1
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->useDpadMusicControl()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lcom/android/music/MediaPlaybackActivity;->mSeeking:Z

    if-nez v2, :cond_6

    iget-wide v2, p0, Lcom/android/music/MediaPlaybackActivity;->mStartSeekPos:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_6

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v2}, Lcom/android/music/IMediaPlaybackService;->next()V

    :cond_5
    :goto_3
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/music/MediaPlaybackActivity;->mSeeking:Z

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/music/MediaPlaybackActivity;->mPosOverride:J

    goto :goto_1

    :cond_6
    const/4 v2, -0x1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v3

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-direct {p0, v2, v3, v4}, Lcom/android/music/MediaPlaybackActivity;->scanForward(IJ)V

    iget-object v2, p0, Lcom/android/music/MediaPlaybackActivity;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/music/MediaPlaybackActivity;->mStartSeekPos:J

    goto :goto_3

    :pswitch_2
    invoke-virtual {p0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_7

    const v2, 0x7f0c0010

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v3

    if-eq v2, v3, :cond_8

    :cond_7
    if-nez v0, :cond_1

    :cond_8
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->doPauseResume()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 21
    .param p1    # Landroid/view/View;

    const/16 v20, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v1}, Lcom/android/music/IMediaPlaybackService;->getArtistName()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v1}, Lcom/android/music/IMediaPlaybackService;->getAlbumName()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v1}, Lcom/android/music/IMediaPlaybackService;->getTrackName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v1}, Lcom/android/music/IMediaPlaybackService;->getAudioId()J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v9

    const-string v1, "<unknown>"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "<unknown>"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v19, :cond_0

    const-string v1, "recording"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :catch_0
    move-exception v12

    const/4 v1, 0x1

    goto :goto_0

    :catch_1
    move-exception v12

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    cmp-long v1, v9, v1

    if-gez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v9, v10}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "is_music"

    aput-object v4, v3, v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    invoke-static/range {v1 .. v6}, Lcom/android/music/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    const/4 v14, 0x1

    if-eqz v11, :cond_3

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_4

    const/4 v14, 0x1

    :cond_2
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_3
    if-nez v14, :cond_5

    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    const/4 v14, 0x0

    goto :goto_1

    :cond_5
    if-eqz v8, :cond_8

    const-string v1, "<unknown>"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const/16 v16, 0x1

    :goto_2
    if-eqz v7, :cond_9

    const-string v1, "<unknown>"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const/4 v15, 0x1

    :goto_3
    if-eqz v16, :cond_a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/music/MediaPlaybackActivity;->mArtistName:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    move-object/from16 v20, v8

    move-object/from16 v18, v8

    const-string v17, "vnd.android.cursor.item/artist"

    :goto_4
    const v1, 0x7f07005a

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v20, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    new-instance v13, Landroid/content/Intent;

    invoke-direct {v13}, Landroid/content/Intent;-><init>()V

    const/high16 v1, 0x10000000

    invoke-virtual {v13, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_SEARCH"

    invoke-virtual {v13, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "query"

    move-object/from16 v0, v18

    invoke-virtual {v13, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz v16, :cond_6

    const-string v1, "android.intent.extra.artist"

    invoke-virtual {v13, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_6
    if-eqz v15, :cond_7

    const-string v1, "android.intent.extra.album"

    invoke-virtual {v13, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_7
    const-string v1, "android.intent.extra.title"

    move-object/from16 v0, v19

    invoke-virtual {v13, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.focus"

    move-object/from16 v0, v17

    invoke-virtual {v13, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, v20

    invoke-static {v13, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_8
    const/16 v16, 0x0

    goto :goto_2

    :cond_9
    const/4 v15, 0x0

    goto :goto_3

    :cond_a
    if-eqz v15, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/music/MediaPlaybackActivity;->mAlbumName:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    move-object/from16 v20, v7

    if-eqz v16, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    :goto_5
    const-string v17, "vnd.android.cursor.item/album"

    goto/16 :goto_4

    :cond_b
    move-object/from16 v18, v7

    goto :goto_5

    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/music/MediaPlaybackActivity;->mTrackName:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    if-eqz v16, :cond_d

    if-nez v15, :cond_11

    :cond_d
    if-eqz v19, :cond_e

    const-string v1, "<unknown>"

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    :cond_e
    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_f
    move-object/from16 v20, v19

    if-eqz v16, :cond_10

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    :goto_6
    const-string v17, "audio/*"

    goto/16 :goto_4

    :cond_10
    move-object/from16 v18, v19

    goto :goto_6

    :cond_11
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "shouldn\'t be here"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 13
    .param p1    # Landroid/view/MenuItem;

    const/4 v8, 0x1

    :try_start_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_0
    :goto_0
    :sswitch_0
    return v8

    :sswitch_1
    iget-boolean v8, p0, Lcom/android/music/MediaPlaybackActivity;->mIsInBackgroud:Z

    if-nez v8, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->onBackPressed()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v8

    goto :goto_0

    :sswitch_2
    :try_start_1
    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mQueueListener:Landroid/view/View$OnClickListener;

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v8, "MediaPlayback"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onOptionsItemSelected with RemoteException "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :sswitch_3
    :try_start_2
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->toggleShuffle()V

    goto :goto_1

    :sswitch_4
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->cycleRepeat()V

    goto :goto_1

    :sswitch_5
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-class v8, Lcom/android/music/MusicBrowserActivity;

    invoke-virtual {v4, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 v8, 0x14000000

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v4}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :sswitch_6
    iget-object v9, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v9}, Lcom/android/music/IMediaPlaybackService;->getAudioId()J

    move-result-wide v9

    invoke-static {p0, v9, v10}, Lcom/android/music/MusicUtils;->setRingtone(Landroid/content/Context;J)V

    goto :goto_0

    :sswitch_7
    invoke-static {}, Lcom/android/music/MusicUtils;->togglePartyShuffle()V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->setShuffleButtonImage()V

    goto :goto_1

    :sswitch_8
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-class v9, Lcom/android/music/CreatePlaylist;

    invoke-virtual {v4, p0, v9}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v9, 0x4

    invoke-virtual {p0, v4, v9}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :sswitch_9
    const/4 v9, 0x1

    new-array v5, v9, [J

    const/4 v9, 0x0

    invoke-static {}, Lcom/android/music/MusicUtils;->getCurrentAudioId()J

    move-result-wide v10

    aput-wide v10, v5, v9

    invoke-interface {p1}, Landroid/view/MenuItem;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "playlist"

    const-wide/16 v11, 0x0

    invoke-virtual {v9, v10, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {p0, v5, v6, v7}, Lcom/android/music/MusicUtils;->addToPlaylist(Landroid/content/Context;[JJ)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v9, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v9, :cond_0

    const/4 v9, 0x1

    new-array v5, v9, [J

    const/4 v9, 0x0

    invoke-static {}, Lcom/android/music/MusicUtils;->getCurrentAudioId()J

    move-result-wide v10

    aput-wide v10, v5, v9

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v9, 0x7f070014

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v12}, Lcom/android/music/IMediaPlaybackService;->getTrackName()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {p0, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v9, "description"

    invoke-virtual {v0, v9, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v9, "items"

    invoke-virtual {v0, v9, v5}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-class v9, Lcom/android/music/DeleteItems;

    invoke-virtual {v4, p0, v9}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {v4, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const/4 v9, -0x1

    invoke-virtual {p0, v4, v9}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :sswitch_b
    new-instance v3, Landroid/content/Intent;

    const-string v9, "android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v9, "android.media.extra.AUDIO_SESSION"

    iget-object v10, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v10}, Lcom/android/music/IMediaPlaybackService;->getAudioSessionId()I

    move-result v10

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v9, 0xd

    invoke-virtual {p0, v3, v9}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p0}, Landroid/app/Activity;->onSearchRequested()Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_9
        0x4 -> :sswitch_8
        0x6 -> :sswitch_5
        0x8 -> :sswitch_7
        0xa -> :sswitch_a
        0xd -> :sswitch_b
        0xe -> :sswitch_0
        0x10 -> :sswitch_6
        0x102002c -> :sswitch_1
        0x7f0c003d -> :sswitch_2
        0x7f0c003e -> :sswitch_3
        0x7f0c003f -> :sswitch_4
        0x7f0c0040 -> :sswitch_c
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/music/MediaPlaybackActivity;->mIsInBackgroud:Z

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 9
    .param p1    # Landroid/view/Menu;

    const v8, 0x7f020022

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-nez v7, :cond_0

    :goto_0
    return v5

    :cond_0
    const/16 v7, 0x8

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/android/music/MusicUtils;->getCurrentShuffleMode()I

    move-result v4

    const/4 v7, 0x2

    if-ne v4, v7, :cond_4

    invoke-interface {v2, v8}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const v7, 0x7f07000e

    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    :cond_1
    :goto_1
    const/4 v1, 0x1

    new-instance v0, Landroid/content/Intent;

    const-string v7, "android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL"

    invoke-direct {v0, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {v7, v0, v5}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v7

    if-nez v7, :cond_2

    const/4 v1, 0x0

    :cond_2
    const/16 v7, 0xd

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    invoke-interface {v7, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v7, p0, Lcom/android/music/MediaPlaybackActivity;->mAddToPlaylistSubmenu:Landroid/view/SubMenu;

    invoke-static {p0, v7}, Lcom/android/music/MusicUtils;->makePlaylistMenu(Landroid/content/Context;Landroid/view/SubMenu;)V

    iget-object v7, p0, Lcom/android/music/MediaPlaybackActivity;->mAddToPlaylistSubmenu:Landroid/view/SubMenu;

    const/16 v8, 0xc

    invoke-interface {v7, v8}, Landroid/view/SubMenu;->removeItem(I)V

    const-string v7, "keyguard"

    invoke-virtual {p0, v7}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/KeyguardManager;

    invoke-virtual {v3}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v7

    if-nez v7, :cond_3

    move v5, v6

    :cond_3
    invoke-interface {p1, v6, v5}, Landroid/view/Menu;->setGroupVisible(IZ)V

    iget-object v5, p0, Lcom/android/music/MediaPlaybackActivity;->mQueueMenuItem:Landroid/view/MenuItem;

    iget-boolean v7, p0, Lcom/android/music/MediaPlaybackActivity;->mIsLandScape:Z

    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v5, p0, Lcom/android/music/MediaPlaybackActivity;->mShuffleMenuItem:Landroid/view/MenuItem;

    iget-boolean v7, p0, Lcom/android/music/MediaPlaybackActivity;->mIsLandScape:Z

    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v5, p0, Lcom/android/music/MediaPlaybackActivity;->mRepeatMenuItem:Landroid/view/MenuItem;

    iget-boolean v7, p0, Lcom/android/music/MediaPlaybackActivity;->mIsLandScape:Z

    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->setRepeatButtonImage()V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->setShuffleButtonImage()V

    move v5, v6

    goto :goto_0

    :cond_4
    invoke-interface {v2, v8}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const v7, 0x7f07000d

    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public onResume()V
    .locals 7

    const/4 v6, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "collapse_statusbar"

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v3, "MediaPlayback"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onResume: collapseStatusBar="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    const-string v3, "statusbar"

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/StatusBarManager;

    invoke-virtual {v2}, Landroid/app/StatusBarManager;->collapse()V

    :cond_0
    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->updateTrackInfo()V

    iget-boolean v3, p0, Lcom/android/music/MediaPlaybackActivity;->mIsCallOnStop:Z

    if-nez v3, :cond_1

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->setPauseButtonImage()V

    :cond_1
    iput-boolean v6, p0, Lcom/android/music/MediaPlaybackActivity;->mIsCallOnStop:Z

    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lcom/android/music/MediaPlaybackActivity;->mPosOverride:J

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    const-string v3, "play song"

    iput-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mPerformanceTestString:Ljava/lang/String;

    iput-boolean v6, p0, Lcom/android/music/MediaPlaybackActivity;->mIsInBackgroud:Z

    return-void
.end method

.method public onStart()V
    .locals 5

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/music/MediaPlaybackActivity;->paused:Z

    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->osc:Landroid/content/ServiceConnection;

    invoke-static {p0, v3}, Lcom/android/music/MusicUtils;->bindToService(Landroid/app/Activity;Landroid/content/ServiceConnection;)Lcom/android/music/MusicUtils$ServiceToken;

    move-result-object v3

    iput-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mToken:Lcom/android/music/MusicUtils$ServiceToken;

    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mToken:Lcom/android/music/MusicUtils$ServiceToken;

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "com.android.music.playstatechanged"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.android.music.metachanged"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.android.music.quitplayback"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/music/MediaPlaybackActivity;->mStatusListener:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4, v0}, Landroid/content/IntentFilter;-><init>(Landroid/content/IntentFilter;)V

    invoke-virtual {p0, v3, v4}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->updateTrackInfo()V

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->refreshNow()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/android/music/MediaPlaybackActivity;->queueNextRefresh(J)V

    return-void
.end method

.method public onStop()V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/music/MediaPlaybackActivity;->paused:Z

    iput-boolean v1, p0, Lcom/android/music/MediaPlaybackActivity;->mIsCallOnStop:Z

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mStatusListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mToken:Lcom/android/music/MusicUtils$ServiceToken;

    invoke-static {v0}, Lcom/android/music/MusicUtils;->unbindFromService(Lcom/android/music/MusicUtils$ServiceToken;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/music/MediaPlaybackActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/android/music/MediaPlaybackActivity;->textViewForContainer(Landroid/view/View;)Landroid/widget/TextView;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return v7

    :cond_1
    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/music/MediaPlaybackActivity;->getBackgroundColor()I

    move-result v8

    invoke-virtual {p1, v8}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/android/music/MediaPlaybackActivity;->mLastX:I

    iput v8, p0, Lcom/android/music/MediaPlaybackActivity;->mInitialX:I

    iput-boolean v7, p0, Lcom/android/music/MediaPlaybackActivity;->mDraggingLabel:Z

    goto :goto_0

    :cond_2
    if-eq v0, v8, :cond_3

    const/4 v9, 0x3

    if-ne v0, v9, :cond_4

    :cond_3
    invoke-virtual {p1, v7}, Landroid/view/View;->setBackgroundColor(I)V

    iget-boolean v8, p0, Lcom/android/music/MediaPlaybackActivity;->mDraggingLabel:Z

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mLabelScroller:Landroid/os/Handler;

    invoke-virtual {v8, v7, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    iget-object v8, p0, Lcom/android/music/MediaPlaybackActivity;->mLabelScroller:Landroid/os/Handler;

    const-wide/16 v9, 0x3e8

    invoke-virtual {v8, v3, v9, v10}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_4
    const/4 v9, 0x2

    if-ne v0, v9, :cond_0

    iget-boolean v9, p0, Lcom/android/music/MediaPlaybackActivity;->mDraggingLabel:Z

    if-eqz v9, :cond_8

    invoke-virtual {v5}, Landroid/view/View;->getScrollX()I

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    float-to-int v6, v9

    iget v9, p0, Lcom/android/music/MediaPlaybackActivity;->mLastX:I

    sub-int v1, v9, v6

    if-eqz v1, :cond_7

    iput v6, p0, Lcom/android/music/MediaPlaybackActivity;->mLastX:I

    add-int/2addr v4, v1

    iget v9, p0, Lcom/android/music/MediaPlaybackActivity;->mTextWidth:I

    if-le v4, v9, :cond_5

    iget v9, p0, Lcom/android/music/MediaPlaybackActivity;->mTextWidth:I

    sub-int/2addr v4, v9

    iget v9, p0, Lcom/android/music/MediaPlaybackActivity;->mViewWidth:I

    sub-int/2addr v4, v9

    :cond_5
    iget v9, p0, Lcom/android/music/MediaPlaybackActivity;->mViewWidth:I

    neg-int v9, v9

    if-ge v4, v9, :cond_6

    iget v9, p0, Lcom/android/music/MediaPlaybackActivity;->mViewWidth:I

    add-int/2addr v4, v9

    iget v9, p0, Lcom/android/music/MediaPlaybackActivity;->mTextWidth:I

    add-int/2addr v4, v9

    :cond_6
    invoke-virtual {v5, v4, v7}, Landroid/view/View;->scrollTo(II)V

    :cond_7
    move v7, v8

    goto :goto_0

    :cond_8
    iget v9, p0, Lcom/android/music/MediaPlaybackActivity;->mInitialX:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    float-to-int v10, v10

    sub-int v1, v9, v10

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v9

    iget v10, p0, Lcom/android/music/MediaPlaybackActivity;->mTouchSlop:I

    if-le v9, v10, :cond_0

    iget-object v9, p0, Lcom/android/music/MediaPlaybackActivity;->mLabelScroller:Landroid/os/Handler;

    invoke-virtual {v9, v7, v5}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    invoke-virtual {v5}, Landroid/widget/TextView;->getEllipsize()Landroid/text/TextUtils$TruncateAt;

    move-result-object v9

    if-eqz v9, :cond_9

    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    :cond_9
    invoke-virtual {v5}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v5}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v9

    float-to-int v9, v9

    iput v9, p0, Lcom/android/music/MediaPlaybackActivity;->mTextWidth:I

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v9

    iput v9, p0, Lcom/android/music/MediaPlaybackActivity;->mViewWidth:I

    iget v9, p0, Lcom/android/music/MediaPlaybackActivity;->mViewWidth:I

    iget v10, p0, Lcom/android/music/MediaPlaybackActivity;->mTextWidth:I

    if-le v9, v10, :cond_a

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {p1}, Landroid/view/View;->cancelLongPress()V

    goto/16 :goto_0

    :cond_a
    iput-boolean v8, p0, Lcom/android/music/MediaPlaybackActivity;->mDraggingLabel:Z

    invoke-virtual {v5, v8}, Landroid/view/View;->setHorizontalFadingEdgeEnabled(Z)V

    invoke-virtual {p1}, Landroid/view/View;->cancelLongPress()V

    move v7, v8

    goto/16 :goto_0
.end method

.method textViewForContainer(Landroid/view/View;)Landroid/widget/TextView;
    .locals 2
    .param p1    # Landroid/view/View;

    const v1, 0x7f0c000a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v1, v0

    check-cast v1, Landroid/widget/TextView;

    :goto_0
    return-object v1

    :cond_0
    const v1, 0x7f0c000b

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v1, v0

    check-cast v1, Landroid/widget/TextView;

    goto :goto_0

    :cond_1
    const v1, 0x7f0c000c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    move-object v1, v0

    check-cast v1, Landroid/widget/TextView;

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
