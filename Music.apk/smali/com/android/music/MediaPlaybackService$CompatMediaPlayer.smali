.class Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;
.super Landroid/media/MediaPlayer;
.source "MediaPlaybackService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/music/MediaPlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CompatMediaPlayer"
.end annotation


# instance fields
.field private mCompatMode:Z

.field private mCompletion:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mNextPlayer:Landroid/media/MediaPlayer;


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v6, 0x1

    invoke-direct {p0}, Landroid/media/MediaPlayer;-><init>()V

    iput-boolean v6, p0, Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;->mCompatMode:Z

    :try_start_0
    const-class v1, Landroid/media/MediaPlayer;

    const-string v2, "setNextMediaPlayer"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/media/MediaPlayer;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;->mCompatMode:Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iput-boolean v6, p0, Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;->mCompatMode:Z

    invoke-super {p0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    goto :goto_0
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;->mNextPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x32

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    iget-object v0, p0, Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;->mNextPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    :cond_0
    iget-object v0, p0, Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;->mCompletion:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-interface {v0, p0}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    return-void
.end method

.method public setNextMediaPlayer(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;->mCompatMode:Z

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;->mNextPlayer:Landroid/media/MediaPlayer;

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/media/MediaPlayer;->setNextMediaPlayer(Landroid/media/MediaPlayer;)V

    goto :goto_0
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 1
    .param p1    # Landroid/media/MediaPlayer$OnCompletionListener;

    iget-boolean v0, p0, Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;->mCompatMode:Z

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;->mCompletion:Landroid/media/MediaPlayer$OnCompletionListener;

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    goto :goto_0
.end method
