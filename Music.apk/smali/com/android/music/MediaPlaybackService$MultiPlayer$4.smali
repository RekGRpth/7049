.class Lcom/android/music/MediaPlaybackService$MultiPlayer$4;
.super Ljava/lang/Object;
.source "MediaPlaybackService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnDurationUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/music/MediaPlaybackService$MultiPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;


# direct methods
.method constructor <init>(Lcom/android/music/MediaPlaybackService$MultiPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDurationUpdate(Landroid/media/MediaPlayer;I)V
    .locals 11
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I

    const/4 v10, 0x0

    if-eqz p1, :cond_0

    if-gtz p2, :cond_2

    :cond_0
    const-string v6, "MusicService"

    const-string v7, "onDurationUpdate with null media player or 0 duration!"

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v6, "MusicService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onDurationUpdate("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService;->access$4800(Lcom/android/music/MediaPlaybackService;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getDuration()I

    :cond_3
    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->access$4000(Lcom/android/music/MediaPlaybackService$MultiPlayer;)Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService;->access$900(Lcom/android/music/MediaPlaybackService;)I

    move-result v4

    :goto_1
    if-ltz v4, :cond_6

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService;->access$1200(Lcom/android/music/MediaPlaybackService;)[J

    move-result-object v6

    aget-wide v1, v6, v4

    :goto_2
    const-wide/16 v6, 0x0

    cmp-long v6, v1, v6

    if-ltz v6, :cond_4

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService;->access$1000(Lcom/android/music/MediaPlaybackService;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_7

    :cond_4
    const-string v6, "MusicService"

    const-string v7, "onDurationUpdate with unknown track!"

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService;->access$3900(Lcom/android/music/MediaPlaybackService;)I

    move-result v4

    goto :goto_1

    :cond_6
    const-wide/16 v1, -0x1

    goto :goto_2

    :cond_7
    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService;->access$1000(Lcom/android/music/MediaPlaybackService;)Landroid/database/Cursor;

    move-result-object v6

    iget-object v7, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v7, v7, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v7}, Lcom/android/music/MediaPlaybackService;->access$1000(Lcom/android/music/MediaPlaybackService;)Landroid/database/Cursor;

    move-result-object v7

    const-string v8, "duration"

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->access$4000(Lcom/android/music/MediaPlaybackService$MultiPlayer;)Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    iget-object v7, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v7, v7, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v7}, Lcom/android/music/MediaPlaybackService;->access$1200(Lcom/android/music/MediaPlaybackService;)[J

    move-result-object v7

    aget-wide v7, v7, v4

    invoke-static {v6, v7, v8}, Lcom/android/music/MediaPlaybackService;->access$1300(Lcom/android/music/MediaPlaybackService;J)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_8

    const-string v6, "MusicService"

    const-string v7, "onDurationUpdate: Next Player track not found!"

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    const-string v6, "duration"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_9
    if-eq v3, p2, :cond_1

    new-instance v5, Landroid/content/ContentValues;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/content/ContentValues;-><init>(I)V

    const-string v6, "MusicService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Old Duration is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "duration"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v6}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "_id = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v5, v8, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v6}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "external"

    invoke-static {v7}, Landroid/provider/MediaStore$Audio$Playlists;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    const-string v6, "MusicService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "duration updated to DB with new duration "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->access$4000(Lcom/android/music/MediaPlaybackService$MultiPlayer;)Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService;->access$1000(Lcom/android/music/MediaPlaybackService;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService;->access$1000(Lcom/android/music/MediaPlaybackService;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v6, v10}, Lcom/android/music/MediaPlaybackService;->access$1002(Lcom/android/music/MediaPlaybackService;Landroid/database/Cursor;)Landroid/database/Cursor;

    :cond_a
    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v7, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    monitor-enter v7

    :try_start_0
    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    iget-object v8, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v8, v8, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    iget-object v9, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$4;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v9, v9, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v9}, Lcom/android/music/MediaPlaybackService;->access$1200(Lcom/android/music/MediaPlaybackService;)[J

    move-result-object v9

    aget-wide v9, v9, v4

    invoke-static {v8, v9, v10}, Lcom/android/music/MediaPlaybackService;->access$1300(Lcom/android/music/MediaPlaybackService;J)Landroid/database/Cursor;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/android/music/MediaPlaybackService;->access$1002(Lcom/android/music/MediaPlaybackService;Landroid/database/Cursor;)Landroid/database/Cursor;

    monitor-exit v7

    goto/16 :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method
