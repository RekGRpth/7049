.class Lcom/android/music/MediaPlaybackService$MultiPlayer$5;
.super Ljava/lang/Object;
.source "MediaPlaybackService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/music/MediaPlaybackService$MultiPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;


# direct methods
.method constructor <init>(Lcom/android/music/MediaPlaybackService$MultiPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 10
    .param p1    # Landroid/media/MediaPlayer;

    const-wide/16 v3, 0x0

    iget-object v5, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v5, v5, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    monitor-enter v5

    :try_start_0
    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->access$4000(Lcom/android/music/MediaPlaybackService$MultiPlayer;)Lcom/android/music/MediaPlaybackService$CompatMediaPlayer;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v3, "MusicService"

    const-string v4, "preparedlistener finish for next player!"

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v5

    :goto_0
    return-void

    :cond_0
    const-string v6, "MusicService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ">> onPrepared: doseek="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v8, v8, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v8}, Lcom/android/music/MediaPlaybackService;->access$600(Lcom/android/music/MediaPlaybackService;)Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mediaseekable="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v8, v8, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v8}, Lcom/android/music/MediaPlaybackService;->access$1400(Lcom/android/music/MediaPlaybackService;)Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", quietmode="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v8, v8, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v8}, Lcom/android/music/MediaPlaybackService;->access$700(Lcom/android/music/MediaPlaybackService;)Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/android/music/MediaPlaybackService;->access$4802(Lcom/android/music/MediaPlaybackService;Z)Z

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v6}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->duration()J

    move-result-wide v6

    cmp-long v6, v6, v3

    if-nez v6, :cond_2

    const-string v3, "MusicService"

    const-string v4, "onPrepared, bad media: duration is 0"

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v3}, Lcom/android/music/MediaPlaybackService;->access$700(Lcom/android/music/MediaPlaybackService;)Z

    move-result v0

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v3}, Lcom/android/music/MediaPlaybackService;->access$4900(Lcom/android/music/MediaPlaybackService;)I

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v3}, Lcom/android/music/MediaPlaybackService;->access$2200(Lcom/android/music/MediaPlaybackService;)I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v3}, Lcom/android/music/MediaPlaybackService;->access$600(Lcom/android/music/MediaPlaybackService;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v3}, Lcom/android/music/MediaPlaybackService;->access$900(Lcom/android/music/MediaPlaybackService;)I

    move-result v3

    iget-object v4, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v4, v4, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v4}, Lcom/android/music/MediaPlaybackService;->access$1100(Lcom/android/music/MediaPlaybackService;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-lt v3, v4, :cond_1

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    const v4, 0x7f070003

    const/4 v6, 0x0

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :cond_1
    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/android/music/MediaPlaybackService;->access$702(Lcom/android/music/MediaPlaybackService;Z)Z

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->errorListener:Landroid/media/MediaPlayer$OnErrorListener;

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-interface {v3, p1, v4, v6}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v3, v0}, Lcom/android/music/MediaPlaybackService;->access$702(Lcom/android/music/MediaPlaybackService;Z)Z

    const-string v3, "MusicService"

    const-string v4, "<< onPrepared, bad media.."

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v5

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_2
    :try_start_1
    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService;->access$600(Lcom/android/music/MediaPlaybackService;)Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService;->access$1400(Lcom/android/music/MediaPlaybackService;)Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService;->access$5000(Lcom/android/music/MediaPlaybackService;)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "seekpos"

    const-wide/16 v8, 0x0

    invoke-interface {v6, v7, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService;->access$5100(Lcom/android/music/MediaPlaybackService;)J

    move-result-wide v6

    cmp-long v6, v6, v3

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v6}, Lcom/android/music/MediaPlaybackService;->access$5100(Lcom/android/music/MediaPlaybackService;)J

    move-result-wide v1

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v6, v6, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    const-wide/16 v7, 0x0

    invoke-static {v6, v7, v8}, Lcom/android/music/MediaPlaybackService;->access$5102(Lcom/android/music/MediaPlaybackService;J)J

    :cond_3
    const-string v6, "MusicService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "seekpos="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    cmp-long v7, v1, v3

    if-ltz v7, :cond_4

    iget-object v7, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v7}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->duration()J

    move-result-wide v7

    cmp-long v7, v1, v7

    if-gtz v7, :cond_4

    move-wide v3, v1

    :cond_4
    invoke-virtual {v6, v3, v4}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->seek(J)J

    const-string v3, "MusicService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "restored queue, currently at position "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v6}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->position()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    invoke-virtual {v6}, Lcom/android/music/MediaPlaybackService$MultiPlayer;->duration()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " (requested "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ")"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/music/MediaPlaybackService;->access$602(Lcom/android/music/MediaPlaybackService;Z)Z

    :cond_5
    :goto_1
    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v3}, Lcom/android/music/MediaPlaybackService;->access$700(Lcom/android/music/MediaPlaybackService;)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v3}, Lcom/android/music/MediaPlaybackService;->access$5200(Lcom/android/music/MediaPlaybackService;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v3}, Lcom/android/music/MediaPlaybackService;->play()V

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    const-string v4, "com.android.music.metachanged"

    invoke-static {v3, v4}, Lcom/android/music/MediaPlaybackService;->access$1600(Lcom/android/music/MediaPlaybackService;Ljava/lang/String;)V

    :cond_6
    :goto_2
    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v3}, Lcom/android/music/MediaPlaybackService;->access$2100(Lcom/android/music/MediaPlaybackService;)V

    const-string v3, "MusicService"

    const-string v4, "<< onPrepared"

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v5

    goto/16 :goto_0

    :cond_7
    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v3}, Lcom/android/music/MediaPlaybackService;->access$1400(Lcom/android/music/MediaPlaybackService;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "MusicService"

    const-string v4, "onPrepared: media NOT seekable, so skip seek!"

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/music/MediaPlaybackService;->access$602(Lcom/android/music/MediaPlaybackService;Z)Z

    goto :goto_1

    :cond_8
    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    invoke-static {v3}, Lcom/android/music/MediaPlaybackService;->access$5200(Lcom/android/music/MediaPlaybackService;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/android/music/MediaPlaybackService$MultiPlayer$5;->this$1:Lcom/android/music/MediaPlaybackService$MultiPlayer;

    iget-object v3, v3, Lcom/android/music/MediaPlaybackService$MultiPlayer;->this$0:Lcom/android/music/MediaPlaybackService;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/android/music/MediaPlaybackService;->access$5202(Lcom/android/music/MediaPlaybackService;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method
