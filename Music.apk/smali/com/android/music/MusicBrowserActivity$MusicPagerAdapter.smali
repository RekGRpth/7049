.class Lcom/android/music/MusicBrowserActivity$MusicPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "MusicBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/music/MusicBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MusicPagerAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/music/MusicBrowserActivity;


# direct methods
.method private constructor <init>(Lcom/android/music/MusicBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/music/MusicBrowserActivity$MusicPagerAdapter;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/music/MusicBrowserActivity;Lcom/android/music/MusicBrowserActivity$1;)V
    .locals 0
    .param p1    # Lcom/android/music/MusicBrowserActivity;
    .param p2    # Lcom/android/music/MusicBrowserActivity$1;

    invoke-direct {p0, p1}, Lcom/android/music/MusicBrowserActivity$MusicPagerAdapter;-><init>(Lcom/android/music/MusicBrowserActivity;)V

    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/View;ILjava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    move-object v0, p1

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity$MusicPagerAdapter;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-static {v1}, Lcom/android/music/MusicBrowserActivity;->access$900(Lcom/android/music/MusicBrowserActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity$MusicPagerAdapter;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-static {v0}, Lcom/android/music/MusicBrowserActivity;->access$900(Lcom/android/music/MusicBrowserActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public instantiateItem(Landroid/view/View;I)Ljava/lang/Object;
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # I

    move-object v1, p1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/android/music/MusicBrowserActivity$MusicPagerAdapter;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-static {v2}, Lcom/android/music/MusicBrowserActivity;->access$900(Lcom/android/music/MusicBrowserActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const-string v2, "MusicBrowser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "instantiateItem-position:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/android/music/MusicBrowserActivity$MusicPagerAdapter;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-static {v2, p2}, Lcom/android/music/MusicBrowserActivity;->access$1000(Lcom/android/music/MusicBrowserActivity;I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/android/music/MusicBrowserActivity$MusicPagerAdapter;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-static {v2}, Lcom/android/music/MusicBrowserActivity;->access$900(Lcom/android/music/MusicBrowserActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/music/MusicBrowserActivity$MusicPagerAdapter;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-static {v2}, Lcom/android/music/MusicBrowserActivity;->access$900(Lcom/android/music/MusicBrowserActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v2, p0, Lcom/android/music/MusicBrowserActivity$MusicPagerAdapter;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-static {v2}, Lcom/android/music/MusicBrowserActivity;->access$1100(Lcom/android/music/MusicBrowserActivity;)Landroid/app/LocalActivityManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/LocalActivityManager;->dispatchResume()V

    :cond_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/android/music/MusicBrowserActivity$MusicPagerAdapter;->this$0:Lcom/android/music/MusicBrowserActivity;

    invoke-static {v2}, Lcom/android/music/MusicBrowserActivity;->access$900(Lcom/android/music/MusicBrowserActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    return-object v2
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/Object;

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
