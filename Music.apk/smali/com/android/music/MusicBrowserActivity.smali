.class public Lcom/android/music/MusicBrowserActivity;
.super Landroid/app/TabActivity;
.source "MusicBrowserActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Landroid/widget/TabHost$OnTabChangeListener;
.implements Lcom/android/music/MusicUtils$Defs;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/music/MusicBrowserActivity$MusicPagerAdapter;
    }
.end annotation


# static fields
.field private static final ALBUM:Ljava/lang/String; = "Album"

.field private static final ALBUM_INDEX:I = 0x1

.field private static final ARTIST:Ljava/lang/String; = "Artist"

.field private static final ARTIST_INDEX:I = 0x0

.field private static final PLAYBACK:Ljava/lang/String; = "Playback"

.field private static final PLAYBACK_INDEX:I = 0x4

.field private static final PLAYLIST:Ljava/lang/String; = "Playlist"

.field private static final PLAYLIST_INDEX:I = 0x3

.field private static final PLAY_ALL:I = 0x13

.field private static final SAVE_TAB:Ljava/lang/String; = "activetab"

.field private static final SONG:Ljava/lang/String; = "Song"

.field private static final SONG_INDEX:I = 0x2

.field private static final TAB_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "MusicBrowser"


# instance fields
.field private mActivityManager:Landroid/app/LocalActivityManager;

.field private mCurrentTab:I

.field private mHasMenukey:Ljava/lang/Boolean;

.field private mIsSdcardMounted:Z

.field private mOrientaiton:I

.field private mOverflowMenuButton:Landroid/view/View;

.field private mOverflowMenuButtonId:I

.field private mPagers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mPopupMenu:Landroid/widget/PopupMenu;

.field private mPopupMenuShowing:Ljava/lang/Boolean;

.field mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

.field private mSdcardstatustListener:Landroid/content/BroadcastReceiver;

.field private mSearchItem:Landroid/view/MenuItem;

.field private mSearchView:Landroid/widget/SearchView;

.field private mService:Lcom/android/music/IMediaPlaybackService;

.field private mTabCount:I

.field private mTabHost:Landroid/widget/TabHost;

.field private mToken:Lcom/android/music/MusicUtils$ServiceToken;

.field private mTrackListListener:Landroid/content/BroadcastReceiver;

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/android/music/MusicBrowserActivity;->TAB_MAP:Ljava/util/HashMap;

    sget-object v0, Lcom/android/music/MusicBrowserActivity;->TAB_MAP:Ljava/util/HashMap;

    const-string v1, "Artist"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/music/MusicBrowserActivity;->TAB_MAP:Ljava/util/HashMap;

    const-string v1, "Album"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/music/MusicBrowserActivity;->TAB_MAP:Ljava/util/HashMap;

    const-string v1, "Song"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/music/MusicBrowserActivity;->TAB_MAP:Ljava/util/HashMap;

    const-string v1, "Playlist"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/music/MusicBrowserActivity;->TAB_MAP:Ljava/util/HashMap;

    const-string v1, "Playback"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Landroid/app/TabActivity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mPagers:Ljava/util/ArrayList;

    iput-object v3, p0, Lcom/android/music/MusicBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    iput-object v3, p0, Lcom/android/music/MusicBrowserActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mPopupMenuShowing:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mHasMenukey:Ljava/lang/Boolean;

    iput-boolean v2, p0, Lcom/android/music/MusicBrowserActivity;->mIsSdcardMounted:Z

    new-instance v0, Lcom/android/music/MusicBrowserActivity$2;

    invoke-direct {v0, p0}, Lcom/android/music/MusicBrowserActivity$2;-><init>(Lcom/android/music/MusicBrowserActivity;)V

    iput-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    new-instance v0, Lcom/android/music/MusicBrowserActivity$3;

    invoke-direct {v0, p0}, Lcom/android/music/MusicBrowserActivity$3;-><init>(Lcom/android/music/MusicBrowserActivity;)V

    iput-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mTrackListListener:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/music/MusicBrowserActivity$4;

    invoke-direct {v0, p0}, Lcom/android/music/MusicBrowserActivity$4;-><init>(Lcom/android/music/MusicBrowserActivity;)V

    iput-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mSdcardstatustListener:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$100(Lcom/android/music/MusicBrowserActivity;)I
    .locals 1
    .param p0    # Lcom/android/music/MusicBrowserActivity;

    iget v0, p0, Lcom/android/music/MusicBrowserActivity;->mOverflowMenuButtonId:I

    return v0
.end method

.method static synthetic access$1000(Lcom/android/music/MusicBrowserActivity;I)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/music/MusicBrowserActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/music/MusicBrowserActivity;->getView(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/music/MusicBrowserActivity;)Landroid/app/LocalActivityManager;
    .locals 1
    .param p0    # Lcom/android/music/MusicBrowserActivity;

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mActivityManager:Landroid/app/LocalActivityManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/music/MusicBrowserActivity;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/music/MusicBrowserActivity;

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mOverflowMenuButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/music/MusicBrowserActivity;Landroid/widget/PopupMenu;)Landroid/widget/PopupMenu;
    .locals 0
    .param p0    # Lcom/android/music/MusicBrowserActivity;
    .param p1    # Landroid/widget/PopupMenu;

    iput-object p1, p0, Lcom/android/music/MusicBrowserActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    return-object p1
.end method

.method static synthetic access$402(Lcom/android/music/MusicBrowserActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0    # Lcom/android/music/MusicBrowserActivity;
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/android/music/MusicBrowserActivity;->mPopupMenuShowing:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$500(Lcom/android/music/MusicBrowserActivity;)Lcom/android/music/IMediaPlaybackService;
    .locals 1
    .param p0    # Lcom/android/music/MusicBrowserActivity;

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/music/MusicBrowserActivity;)I
    .locals 1
    .param p0    # Lcom/android/music/MusicBrowserActivity;

    iget v0, p0, Lcom/android/music/MusicBrowserActivity;->mOrientaiton:I

    return v0
.end method

.method static synthetic access$700(Lcom/android/music/MusicBrowserActivity;)V
    .locals 0
    .param p0    # Lcom/android/music/MusicBrowserActivity;

    invoke-direct {p0}, Lcom/android/music/MusicBrowserActivity;->updatePlaybackTab()V

    return-void
.end method

.method static synthetic access$800(Lcom/android/music/MusicBrowserActivity;)Z
    .locals 1
    .param p0    # Lcom/android/music/MusicBrowserActivity;

    iget-boolean v0, p0, Lcom/android/music/MusicBrowserActivity;->mIsSdcardMounted:Z

    return v0
.end method

.method static synthetic access$802(Lcom/android/music/MusicBrowserActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/MusicBrowserActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/MusicBrowserActivity;->mIsSdcardMounted:Z

    return p1
.end method

.method static synthetic access$900(Lcom/android/music/MusicBrowserActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/android/music/MusicBrowserActivity;

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mPagers:Ljava/util/ArrayList;

    return-object v0
.end method

.method private createFakeMenu()V
    .locals 6

    const v5, 0x7f0c0037

    const v4, 0x7f0c0020

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mHasMenukey:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "MusicBrowser"

    const-string v2, "createFakeMenu Quit when there has Menu Key"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lcom/android/music/MusicBrowserActivity;->mOrientaiton:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iput v4, p0, Lcom/android/music/MusicBrowserActivity;->mOverflowMenuButtonId:I

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mOverflowMenuButton:Landroid/view/View;

    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mOverflowMenuButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mOverflowMenuButton:Landroid/view/View;

    new-instance v2, Lcom/android/music/MusicBrowserActivity$1;

    invoke-direct {v2, p0}, Lcom/android/music/MusicBrowserActivity$1;-><init>(Lcom/android/music/MusicBrowserActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_2
    iput v5, p0, Lcom/android/music/MusicBrowserActivity;->mOverflowMenuButtonId:I

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mOverflowMenuButton:Landroid/view/View;

    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mOverflowMenuButton:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private getStringId(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    const-string v0, "Artist"

    packed-switch p1, :pswitch_data_0

    const-string v1, "MusicBrowser"

    const-string v2, "ARTIST_INDEX or default"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "Album"

    goto :goto_0

    :pswitch_1
    const-string v0, "Song"

    goto :goto_0

    :pswitch_2
    const-string v0, "Playlist"

    goto :goto_0

    :pswitch_3
    const-string v0, "Playback"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getView(I)Landroid/view/View;
    .locals 5
    .param p1    # I

    const-string v2, "MusicBrowser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getView>>>index = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.PICK"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    const-string v2, "MusicBrowser"

    const-string v3, "default"

    invoke-static {v2, v3}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :pswitch_0
    sget-object v2, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    const-string v3, "vnd.android.cursor.dir/artistalbum"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    :goto_1
    const-string v2, "withtabs"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v2, 0x4000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/music/MusicBrowserActivity;->mActivityManager:Landroid/app/LocalActivityManager;

    invoke-direct {p0, p1}, Lcom/android/music/MusicBrowserActivity;->getStringId(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/app/LocalActivityManager;->startActivity(Ljava/lang/String;Landroid/content/Intent;)Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const-string v2, "MusicBrowser"

    const-string v3, "getView<<<"

    invoke-static {v2, v3}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v1

    goto :goto_0

    :pswitch_1
    sget-object v2, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    const-string v3, "vnd.android.cursor.dir/album"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :pswitch_2
    sget-object v2, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    const-string v3, "vnd.android.cursor.dir/track"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :pswitch_3
    sget-object v2, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    const-string v3, "vnd.android.cursor.dir/playlist"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private initPager()V
    .locals 3

    iget-object v2, p0, Lcom/android/music/MusicBrowserActivity;->mPagers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x3

    if-gt v0, v2, :cond_1

    iget v2, p0, Lcom/android/music/MusicBrowserActivity;->mCurrentTab:I

    if-ne v0, v2, :cond_0

    invoke-direct {p0, v0}, Lcom/android/music/MusicBrowserActivity;->getView(I)Landroid/view/View;

    move-result-object v1

    :goto_1
    iget-object v2, p0, Lcom/android/music/MusicBrowserActivity;->mPagers:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    return-void
.end method

.method private initTab()V
    .locals 7

    const-string v4, "MusicBrowser"

    const-string v5, "initTab>>"

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030004

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TabWidget;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    iput v4, p0, Lcom/android/music/MusicBrowserActivity;->mOrientaiton:I

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    iput v4, p0, Lcom/android/music/MusicBrowserActivity;->mTabCount:I

    iget-object v4, p0, Lcom/android/music/MusicBrowserActivity;->mHasMenukey:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/android/music/MusicBrowserActivity;->mTabCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/android/music/MusicBrowserActivity;->mTabCount:I

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget v4, p0, Lcom/android/music/MusicBrowserActivity;->mTabCount:I

    if-ge v0, v4, :cond_2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_1
    const-string v4, "MusicBrowser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "addTab:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/music/MusicBrowserActivity;->mTabHost:Landroid/widget/TabHost;

    iget-object v5, p0, Lcom/android/music/MusicBrowserActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-direct {p0, v0}, Lcom/android/music/MusicBrowserActivity;->getStringId(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    move-result-object v5

    const v6, 0x1020011

    invoke-virtual {v5, v6}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget v4, p0, Lcom/android/music/MusicBrowserActivity;->mOrientaiton:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/android/music/MusicBrowserActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v4}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v3

    const/4 v0, 0x4

    :goto_1
    iget v4, p0, Lcom/android/music/MusicBrowserActivity;->mTabCount:I

    if-ge v0, v4, :cond_4

    invoke-virtual {v3, v0}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    const-string v4, "MusicBrowser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "set tab gone:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    const-string v4, "MusicBrowser"

    const-string v5, "initTab<<"

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private updatePlaybackTab()V
    .locals 14

    const/4 v0, 0x1

    const/16 v1, 0xff

    const/16 v2, 0x80

    iget-object v11, p0, Lcom/android/music/MusicBrowserActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v11}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v10

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    const/4 v6, 0x1

    const-wide/16 v7, -0x1

    const/4 v3, 0x0

    const/16 v4, 0xff

    if-nez v9, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v11, p0, Lcom/android/music/MusicBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v11, :cond_1

    iget-object v11, p0, Lcom/android/music/MusicBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v11}, Lcom/android/music/IMediaPlaybackService;->getAudioId()J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v7

    :cond_1
    :goto_1
    const-wide/16 v11, -0x1

    cmp-long v11, v7, v11

    if-nez v11, :cond_2

    const/4 v6, 0x0

    const/16 v4, 0x80

    :cond_2
    invoke-virtual {v9, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v9}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v5

    const/4 v11, 0x1

    aget-object v3, v5, v11

    if-eqz v3, :cond_3

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_3
    const-string v11, "MusicBrowser"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "updatePlaybackTab:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v11

    goto :goto_1
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mActivityManager:Landroid/app/LocalActivityManager;

    iget v2, p0, Lcom/android/music/MusicBrowserActivity;->mCurrentTab:I

    invoke-direct {p0, v2}, Lcom/android/music/MusicBrowserActivity;->getStringId(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/LocalActivityManager;->getActivity(Ljava/lang/String;)Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lcom/android/music/MusicBrowserActivity;->mCurrentTab:I

    packed-switch v1, :pswitch_data_0

    const-string v1, "MusicBrowser"

    const-string v2, "default"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_0
    check-cast v0, Lcom/android/music/ArtistAlbumBrowserActivity;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/music/ArtistAlbumBrowserActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    :pswitch_1
    check-cast v0, Lcom/android/music/AlbumBrowserActivity;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/music/AlbumBrowserActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    check-cast v0, Lcom/android/music/TrackBrowserActivity;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/music/TrackBrowserActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    :pswitch_3
    check-cast v0, Lcom/android/music/PlaylistBrowserActivity;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/music/PlaylistBrowserActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 9
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    const-string v6, "MusicBrowser"

    const-string v7, "onConfigurationChanged>>"

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/music/MusicBrowserActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v6}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v4

    const/16 v5, 0x8

    iget v6, p1, Landroid/content/res/Configuration;->orientation:I

    iput v6, p0, Lcom/android/music/MusicBrowserActivity;->mOrientaiton:I

    iget v6, p0, Lcom/android/music/MusicBrowserActivity;->mOrientaiton:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    const-string v6, "MusicBrowser"

    const-string v7, "onConfigurationChanged--LandScape"

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    :cond_0
    iget-object v6, p0, Lcom/android/music/MusicBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v6, :cond_1

    iget v6, p0, Lcom/android/music/MusicBrowserActivity;->mOrientaiton:I

    invoke-static {p0, v6}, Lcom/android/music/MusicUtils;->updateNowPlaying(Landroid/app/Activity;I)V

    invoke-direct {p0}, Lcom/android/music/MusicBrowserActivity;->updatePlaybackTab()V

    :cond_1
    const/4 v1, 0x4

    :goto_0
    iget v6, p0, Lcom/android/music/MusicBrowserActivity;->mTabCount:I

    if-ge v1, v6, :cond_3

    invoke-virtual {v4, v1}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    :goto_1
    const/4 v6, 0x4

    if-ge v1, v6, :cond_5

    iget-object v6, p0, Lcom/android/music/MusicBrowserActivity;->mActivityManager:Landroid/app/LocalActivityManager;

    invoke-direct {p0, v1}, Lcom/android/music/MusicBrowserActivity;->getStringId(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/LocalActivityManager;->getActivity(Ljava/lang/String;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    iget-object v6, p0, Lcom/android/music/MusicBrowserActivity;->mHasMenukey:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_7

    iget-object v2, p0, Lcom/android/music/MusicBrowserActivity;->mPopupMenuShowing:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/android/music/MusicBrowserActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/android/music/MusicBrowserActivity;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v6}, Landroid/widget/PopupMenu;->dismiss()V

    const-string v6, "MusicBrowser"

    const-string v7, "changeFakeMenu:mPopupMenu.dismiss()"

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const-string v6, "MusicBrowser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "changeFakeMenu:popupMenuShowing="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/music/MusicBrowserActivity;->createFakeMenu()V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_7

    const-string v6, "MusicBrowser"

    const-string v7, "changeFakeMenu:performClick()"

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/music/MusicBrowserActivity;->mOverflowMenuButton:Landroid/view/View;

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/android/music/MusicBrowserActivity;->mOverflowMenuButton:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->performClick()Z

    :cond_7
    const-string v6, "MusicBrowser"

    const-string v7, "onConfigurationChanged<<"

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/app/ActivityGroup;->onCreate(Landroid/os/Bundle;)V

    const-string v1, "MusicBrowser"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v1, 0x7f030007

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setVolumeControlStream(I)V

    invoke-static {p0, p0}, Lcom/android/music/MusicUtils;->bindToService(Landroid/app/Activity;Landroid/content/ServiceConnection;)Lcom/android/music/MusicUtils$ServiceToken;

    move-result-object v1

    iput-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mToken:Lcom/android/music/MusicUtils$ServiceToken;

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mHasMenukey:Ljava/lang/Boolean;

    new-instance v1, Landroid/app/LocalActivityManager;

    invoke-direct {v1, p0, v4}, Landroid/app/LocalActivityManager;-><init>(Landroid/app/Activity;Z)V

    iput-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mActivityManager:Landroid/app/LocalActivityManager;

    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mActivityManager:Landroid/app/LocalActivityManager;

    invoke-virtual {v1, p1}, Landroid/app/LocalActivityManager;->dispatchCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/TabActivity;->getTabHost()Landroid/widget/TabHost;

    move-result-object v1

    iput-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-direct {p0}, Lcom/android/music/MusicBrowserActivity;->initTab()V

    const-string v1, "activetab"

    invoke-static {p0, v1, v4}, Lcom/android/music/MusicUtils;->getIntPref(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/music/MusicBrowserActivity;->mCurrentTab:I

    const-string v1, "MusicBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate mCurrentTab:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/music/MusicBrowserActivity;->mCurrentTab:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/android/music/MusicBrowserActivity;->mCurrentTab:I

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/android/music/MusicBrowserActivity;->mCurrentTab:I

    iget v2, p0, Lcom/android/music/MusicBrowserActivity;->mTabCount:I

    if-lt v1, v2, :cond_1

    :cond_0
    iput v4, p0, Lcom/android/music/MusicBrowserActivity;->mCurrentTab:I

    :cond_1
    iget v1, p0, Lcom/android/music/MusicBrowserActivity;->mCurrentTab:I

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mTabHost:Landroid/widget/TabHost;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->setCurrentTab(I)V

    :cond_2
    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v1, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    invoke-direct {p0}, Lcom/android/music/MusicBrowserActivity;->initPager()V

    const v1, 0x7f0c0028

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    iput-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v2, Lcom/android/music/MusicBrowserActivity$MusicPagerAdapter;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/music/MusicBrowserActivity$MusicPagerAdapter;-><init>(Lcom/android/music/MusicBrowserActivity;Lcom/android/music/MusicBrowserActivity$1;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.android.music.sdcardstatusupdate"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mSdcardstatustListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/android/music/MusicBrowserActivity;->createFakeMenu()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    const/16 v0, 0x13

    const v1, 0x7f070011

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    const/16 v0, 0x8

    const v1, 0x7f07000d

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    const/16 v0, 0x9

    const v1, 0x7f070010

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    const/16 v0, 0xd

    const v1, 0x7f070005

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    invoke-static {p0, p1, v0}, Lcom/android/music/MusicUtils;->addSearchView(Landroid/app/Activity;Landroid/view/Menu;Landroid/widget/SearchView$OnQueryTextListener;)Landroid/widget/SearchView;

    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "MusicBrowser"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mToken:Lcom/android/music/MusicUtils$ServiceToken;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mToken:Lcom/android/music/MusicUtils$ServiceToken;

    invoke-static {v0}, Lcom/android/music/MusicUtils;->unbindFromService(Lcom/android/music/MusicUtils$ServiceToken;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    :cond_0
    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mSdcardstatustListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mActivityManager:Landroid/app/LocalActivityManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/LocalActivityManager;->dispatchDestroy(Z)V

    invoke-super {p0}, Landroid/app/ActivityGroup;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1    # Landroid/view/MenuItem;

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v3

    const-string v3, "is_music=1"

    const-string v5, "title_key"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/android/music/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {p0, v6}, Lcom/android/music/MusicUtils;->playAll(Landroid/content/Context;Landroid/database/Cursor;)V

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    move v0, v7

    goto :goto_0

    :sswitch_1
    invoke-static {}, Lcom/android/music/MusicUtils;->togglePartyShuffle()V

    move v0, v7

    goto :goto_0

    :sswitch_2
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v7, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v3

    const-string v3, "is_music=1"

    const-string v5, "title_key"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/android/music/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-static {p0, v6}, Lcom/android/music/MusicUtils;->shuffleAll(Landroid/content/Context;Landroid/database/Cursor;)V

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v7

    goto :goto_0

    :sswitch_3
    invoke-static {p0}, Lcom/android/music/MusicUtils;->startEffectPanel(Landroid/app/Activity;)Z

    move-result v0

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0}, Landroid/app/Activity;->onSearchRequested()Z

    move v0, v7

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0x9 -> :sswitch_2
        0xd -> :sswitch_3
        0x13 -> :sswitch_0
        0x7f0c0040 -> :sswitch_4
    .end sparse-switch
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1    # I
    .param p2    # F
    .param p3    # I

    return-void
.end method

.method public onPageSelected(I)V
    .locals 3
    .param p1    # I

    const-string v0, "MusicBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPageSelected-position:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0, p1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    return-void
.end method

.method public onPause()V
    .locals 2

    const-string v0, "MusicBrowser"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mTrackListListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mActivityManager:Landroid/app/LocalActivityManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/LocalActivityManager;->dispatchPause(Z)V

    const-string v0, "activetab"

    iget v1, p0, Lcom/android/music/MusicBrowserActivity;->mCurrentTab:I

    invoke-static {p0, v0, v1}, Lcom/android/music/MusicUtils;->setIntPref(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-super {p0}, Landroid/app/ActivityGroup;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1    # Landroid/view/Menu;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/android/music/MusicUtils;->setPartyShuffleMenuIcon(Landroid/view/Menu;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    iget-boolean v4, p0, Lcom/android/music/MusicBrowserActivity;->mIsSdcardMounted:Z

    if-nez v4, :cond_0

    const-string v3, "MusicBrowser"

    const-string v4, "Sdcard is not mounted, don\'t show option menu!"

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v2

    :cond_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget v4, p0, Lcom/android/music/MusicBrowserActivity;->mCurrentTab:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :cond_1
    const/16 v4, 0x13

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget v4, p0, Lcom/android/music/MusicBrowserActivity;->mCurrentTab:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :cond_2
    const/16 v4, 0x9

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    if-nez v4, :cond_3

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :cond_3
    const/16 v2, 0xd

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move v2, v3

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Landroid/app/ActivityGroup;->onResume()V

    const-string v1, "MusicBrowser"

    const-string v2, "onResume>>>"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mTrackListListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mTabHost:Landroid/widget/TabHost;

    iget v2, p0, Lcom/android/music/MusicBrowserActivity;->mCurrentTab:I

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->setCurrentTab(I)V

    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mActivityManager:Landroid/app/LocalActivityManager;

    invoke-virtual {v1}, Landroid/app/LocalActivityManager;->dispatchResume()V

    const-string v1, "MusicBrowser"

    const-string v2, "onResume<<<"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    invoke-static {p2}, Lcom/android/music/IMediaPlaybackService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/music/IMediaPlaybackService;

    move-result-object v1

    iput-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "autoshuffle"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v1, :cond_1

    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/music/MusicBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Lcom/android/music/IMediaPlaybackService;->setShuffleMode(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget v1, p0, Lcom/android/music/MusicBrowserActivity;->mOrientaiton:I

    invoke-static {p0, v1}, Lcom/android/music/MusicUtils;->updateNowPlaying(Landroid/app/Activity;I)V

    invoke-direct {p0}, Lcom/android/music/MusicBrowserActivity;->updatePlaybackTab()V

    :cond_1
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1    # Landroid/content/ComponentName;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/android/music/MusicBrowserActivity;->mActivityManager:Landroid/app/LocalActivityManager;

    invoke-virtual {v0}, Landroid/app/LocalActivityManager;->dispatchStop()V

    invoke-super {p0}, Landroid/app/ActivityGroup;->onStop()V

    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    sget-object v2, Lcom/android/music/MusicBrowserActivity;->TAB_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string v2, "MusicBrowser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onTabChanged-tabId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ltz v1, :cond_1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_1

    iget-object v2, p0, Lcom/android/music/MusicBrowserActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    iput v1, p0, Lcom/android/music/MusicBrowserActivity;->mCurrentTab:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/android/music/MediaPlaybackActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
