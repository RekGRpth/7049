.class Lcom/android/music/MusicDialog;
.super Landroid/app/AlertDialog;
.source "MusicDialog.java"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private final mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field private final mListener:Landroid/content/DialogInterface$OnClickListener;

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/DialogInterface$OnClickListener;
    .param p3    # Landroid/view/View;

    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/music/MusicDialog$1;

    invoke-direct {v0, p0}, Lcom/android/music/MusicDialog$1;-><init>(Lcom/android/music/MusicDialog;)V

    iput-object v0, p0, Lcom/android/music/MusicDialog;->mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/android/music/MusicDialog;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/android/music/MusicDialog;->mListener:Landroid/content/DialogInterface$OnClickListener;

    iput-object p3, p0, Lcom/android/music/MusicDialog;->mView:Landroid/view/View;

    return-void
.end method

.method static synthetic access$000(Lcom/android/music/MusicDialog;)Landroid/app/Activity;
    .locals 1
    .param p0    # Lcom/android/music/MusicDialog;

    iget-object v0, p0, Lcom/android/music/MusicDialog;->mActivity:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public getNeutralButton()Landroid/widget/Button;
    .locals 1

    const/4 v0, -0x3

    invoke-virtual {p0, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    return-object v0
.end method

.method public getPositiveButton()Landroid/widget/Button;
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/android/music/MusicDialog;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/music/MusicDialog;->mView:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public setCancelable(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/music/MusicDialog;->mCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    return-void
.end method

.method public setNeutralButton(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;

    const/4 v0, -0x3

    iget-object v1, p0, Lcom/android/music/MusicDialog;->mListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v0, p1, v1}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method public setPositiveButton(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/android/music/MusicDialog;->mListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p0, v0, p1, v1}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method
