.class final Lcom/android/music/MusicPicker$QueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "MusicPicker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/music/MusicPicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "QueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/music/MusicPicker;


# direct methods
.method public constructor <init>(Lcom/android/music/MusicPicker;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .param p3    # Landroid/database/Cursor;

    const/4 v2, 0x0

    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget-object v0, v0, Lcom/android/music/MusicPicker;->mAdapter:Lcom/android/music/MusicPicker$TrackListAdapter;

    invoke-virtual {v0, v2}, Lcom/android/music/MusicPicker$TrackListAdapter;->setLoading(Z)V

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget-object v0, v0, Lcom/android/music/MusicPicker;->mAdapter:Lcom/android/music/MusicPicker$TrackListAdapter;

    invoke-virtual {v0, p3}, Lcom/android/music/MusicPicker$TrackListAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget-object v0, v0, Lcom/android/music/MusicPicker;->mListState:Landroid/os/Parcelable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    invoke-virtual {v0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget-object v1, v1, Lcom/android/music/MusicPicker;->mListState:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget-boolean v0, v0, Lcom/android/music/MusicPicker;->mListHasFocus:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    invoke-virtual {v0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_0
    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iput-boolean v2, v0, Lcom/android/music/MusicPicker;->mListHasFocus:Z

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/music/MusicPicker;->mListState:Landroid/os/Parcelable;

    :cond_1
    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget-object v0, v0, Lcom/android/music/MusicPicker;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget v0, v0, Lcom/android/music/MusicPicker;->mPrevSelectedPos:I

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget v0, v0, Lcom/android/music/MusicPicker;->mPrevSelectedPos:I

    iget-object v1, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget-object v1, v1, Lcom/android/music/MusicPicker;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget-object v0, v0, Lcom/android/music/MusicPicker;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget v1, v1, Lcom/android/music/MusicPicker;->mPrevSelectedPos:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget-object v1, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget v1, v1, Lcom/android/music/MusicPicker;->mPrevSelectedPos:I

    iput v1, v0, Lcom/android/music/MusicPicker;->mSelectedPos:I

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget-object v1, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget-object v1, v1, Lcom/android/music/MusicPicker;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget-object v2, v2, Lcom/android/music/MusicPicker;->mCursor:Landroid/database/Cursor;

    const-string v3, "_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/android/music/MusicPicker;->mSelectedId:J

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    iget-wide v2, v2, Lcom/android/music/MusicPicker;->mSelectedId:J

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lcom/android/music/MusicPicker;->mSelectedUri:Landroid/net/Uri;

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    invoke-virtual {v0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidateViews()V

    iget-object v0, p0, Lcom/android/music/MusicPicker$QueryHandler;->this$0:Lcom/android/music/MusicPicker;

    const/4 v1, -0x1

    iput v1, v0, Lcom/android/music/MusicPicker;->mPrevSelectedPos:I

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method
