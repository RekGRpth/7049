.class public final Lcom/android/music/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/music/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final album_border_large:I = 0x7f020000

.field public static final albumart_mp_unknown:I = 0x7f020001

.field public static final albumart_mp_unknown_list:I = 0x7f020002

.field public static final app_music:I = 0x7f020003

.field public static final app_video:I = 0x7f020004

.field public static final appwidget_bg_holo:I = 0x7f020005

.field public static final appwidget_button_center:I = 0x7f020006

.field public static final appwidget_button_left:I = 0x7f020007

.field public static final appwidget_button_right:I = 0x7f020008

.field public static final appwidget_divider_holo:I = 0x7f020009

.field public static final appwidget_inner_focus_c_holo:I = 0x7f02000a

.field public static final appwidget_inner_focus_l_holo:I = 0x7f02000b

.field public static final appwidget_inner_focus_r_holo:I = 0x7f02000c

.field public static final appwidget_inner_press_c_holo:I = 0x7f02000d

.field public static final appwidget_inner_press_l_holo:I = 0x7f02000e

.field public static final appwidget_inner_press_r_holo:I = 0x7f02000f

.field public static final btn_nowplaying_background_small:I = 0x7f020010

.field public static final btn_playback_ic_pause_small:I = 0x7f020011

.field public static final btn_playback_ic_play_small:I = 0x7f020012

.field public static final btn_playback_small_pressed:I = 0x7f020013

.field public static final btn_playback_small_selected:I = 0x7f020014

.field public static final buttonbarbackground:I = 0x7f020015

.field public static final ic_appwidget_music_next:I = 0x7f020016

.field public static final ic_appwidget_music_pause:I = 0x7f020017

.field public static final ic_appwidget_music_play:I = 0x7f020018

.field public static final ic_dialog_alert:I = 0x7f020019

.field public static final ic_launcher_shortcut_music_playlist:I = 0x7f02001a

.field public static final ic_menu_add:I = 0x7f02001b

.field public static final ic_menu_clear_playlist:I = 0x7f02001c

.field public static final ic_menu_delete:I = 0x7f02001d

.field public static final ic_menu_eq:I = 0x7f02001e

.field public static final ic_menu_fmtransmitter:I = 0x7f02001f

.field public static final ic_menu_music_library:I = 0x7f020020

.field public static final ic_menu_overflow:I = 0x7f020021

.field public static final ic_menu_party_shuffle:I = 0x7f020022

.field public static final ic_menu_play_clip:I = 0x7f020023

.field public static final ic_menu_save:I = 0x7f020024

.field public static final ic_menu_set_as_ringtone:I = 0x7f020025

.field public static final ic_menu_shuffle:I = 0x7f020026

.field public static final ic_mp_artist_list:I = 0x7f020027

.field public static final ic_mp_current_playlist_btn:I = 0x7f020028

.field public static final ic_mp_partyshuffle_on_btn:I = 0x7f020029

.field public static final ic_mp_playlist_list:I = 0x7f02002a

.field public static final ic_mp_playlist_recently_added_list:I = 0x7f02002b

.field public static final ic_mp_repeat_all_btn:I = 0x7f02002c

.field public static final ic_mp_repeat_off_btn:I = 0x7f02002d

.field public static final ic_mp_repeat_once_btn:I = 0x7f02002e

.field public static final ic_mp_sd_card:I = 0x7f02002f

.field public static final ic_mp_shuffle_off_btn:I = 0x7f020030

.field public static final ic_mp_shuffle_on_btn:I = 0x7f020031

.field public static final ic_mp_song_list:I = 0x7f020032

.field public static final ic_playlist_move:I = 0x7f020033

.field public static final ic_search_category_music_song:I = 0x7f020034

.field public static final ic_slide_keyboard:I = 0x7f020035

.field public static final ic_tab_albums:I = 0x7f020036

.field public static final ic_tab_artists:I = 0x7f020037

.field public static final ic_tab_playback:I = 0x7f020038

.field public static final ic_tab_playlists:I = 0x7f020039

.field public static final ic_tab_selected_focused_holo:I = 0x7f02003a

.field public static final ic_tab_selected_holo:I = 0x7f02003b

.field public static final ic_tab_selected_pressed_holo:I = 0x7f02003c

.field public static final ic_tab_songs:I = 0x7f02003d

.field public static final ic_tab_unselected_focused_holo:I = 0x7f02003e

.field public static final ic_tab_unselected_holo:I = 0x7f02003f

.field public static final ic_tab_unselected_pressed_holo:I = 0x7f020040

.field public static final indicator_ic_mp_playing_list:I = 0x7f020041

.field public static final lyrics_mode_multiple:I = 0x7f020042

.field public static final lyrics_mode_single:I = 0x7f020043

.field public static final lyrics_switch_multiple_mode:I = 0x7f020044

.field public static final lyrics_switch_single_mode:I = 0x7f020045

.field public static final midi:I = 0x7f020046

.field public static final movie:I = 0x7f020047

.field public static final music_bottom_playback_bg:I = 0x7f020048

.field public static final playlist_tile_drag:I = 0x7f020049

.field public static final stat_notif_next:I = 0x7f02004a

.field public static final stat_notif_pause:I = 0x7f02004b

.field public static final stat_notif_prev:I = 0x7f02004c

.field public static final stat_notify_albumart_frame:I = 0x7f02004d

.field public static final stat_notify_musicplayer:I = 0x7f02004e

.field public static final stat_notify_next_nor:I = 0x7f02004f

.field public static final stat_notify_next_pre:I = 0x7f020050

.field public static final stat_notify_pause_nor:I = 0x7f020051

.field public static final stat_notify_pause_pre:I = 0x7f020052

.field public static final stat_notify_prev_nor:I = 0x7f020053

.field public static final stat_notify_prev_pre:I = 0x7f020054


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
