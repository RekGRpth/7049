.class public final Lcom/android/music/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/music/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final album:I = 0x7f0c0006

.field public static final album_appwidget:I = 0x7f0c0000

.field public static final album_info:I = 0x7f0c0005

.field public static final albumname:I = 0x7f0c000b

.field public static final albumtab:I = 0x7f0c001c

.field public static final artist:I = 0x7f0c0002

.field public static final artistalbum:I = 0x7f0c003b

.field public static final artistname:I = 0x7f0c000a

.field public static final artisttab:I = 0x7f0c001b

.field public static final btn_next:I = 0x7f0c0035

.field public static final btn_pause:I = 0x7f0c0034

.field public static final btn_prev:I = 0x7f0c0033

.field public static final buttonbar:I = 0x7f0c001a

.field public static final cancelButton:I = 0x7f0c002d

.field public static final control_next:I = 0x7f0c0004

.field public static final control_play:I = 0x7f0c0003

.field public static final curplaylist:I = 0x7f0c0007

.field public static final current_playlist_menu_item:I = 0x7f0c003d

.field public static final currenttime:I = 0x7f0c000e

.field public static final drm_lock:I = 0x7f0c0025

.field public static final duration:I = 0x7f0c0023

.field public static final icon:I = 0x7f0c0022

.field public static final iv_cover:I = 0x7f0c0031

.field public static final line1:I = 0x7f0c0017

.field public static final line2:I = 0x7f0c0018

.field public static final listContainer:I = 0x7f0c002c

.field public static final loading:I = 0x7f0c0014

.field public static final mainLayout:I = 0x7f0c002a

.field public static final media_button:I = 0x7f0c000d

.field public static final message:I = 0x7f0c0038

.field public static final next:I = 0x7f0c0011

.field public static final normal_view:I = 0x7f0c0027

.field public static final nowplaying:I = 0x7f0c0036

.field public static final nowplayinglinear:I = 0x7f0c0029

.field public static final nowplayingtab:I = 0x7f0c001f

.field public static final okayButton:I = 0x7f0c002e

.field public static final overflow_menu:I = 0x7f0c0020

.field public static final overflow_menu_nowplaying:I = 0x7f0c0037

.field public static final pause:I = 0x7f0c0010

.field public static final play_indicator:I = 0x7f0c0024

.field public static final playlist:I = 0x7f0c0021

.field public static final playlisttab:I = 0x7f0c001e

.field public static final playpause:I = 0x7f0c0019

.field public static final prev:I = 0x7f0c000f

.field public static final progress:I = 0x7f0c0015

.field public static final progressContainer:I = 0x7f0c002b

.field public static final radio:I = 0x7f0c002f

.field public static final repeat:I = 0x7f0c0009

.field public static final repeat_menu_item:I = 0x7f0c003f

.field public static final rl_newstatus:I = 0x7f0c0030

.field public static final sd_error:I = 0x7f0c0026

.field public static final sd_icon:I = 0x7f0c0039

.field public static final sd_message:I = 0x7f0c003a

.field public static final search:I = 0x7f0c0040

.field public static final shuffle:I = 0x7f0c0008

.field public static final shuffle_menu_item:I = 0x7f0c003e

.field public static final songtab:I = 0x7f0c001d

.field public static final spinner:I = 0x7f0c0013

.field public static final title:I = 0x7f0c0001

.field public static final titleandbuttons:I = 0x7f0c0016

.field public static final totaltime:I = 0x7f0c0012

.field public static final trackname:I = 0x7f0c000c

.field public static final txt_trackinfo:I = 0x7f0c0032

.field public static final viewpage:I = 0x7f0c0028

.field public static final weeks:I = 0x7f0c003c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
