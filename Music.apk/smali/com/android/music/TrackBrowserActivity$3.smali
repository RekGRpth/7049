.class Lcom/android/music/TrackBrowserActivity$3;
.super Ljava/lang/Object;
.source "TrackBrowserActivity.java"

# interfaces
.implements Lcom/android/music/TouchInterceptor$DropListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/music/TrackBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/music/TrackBrowserActivity;


# direct methods
.method constructor <init>(Lcom/android/music/TrackBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/music/TrackBrowserActivity$3;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public drop(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity$3;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v3}, Lcom/android/music/TrackBrowserActivity;->access$700(Lcom/android/music/TrackBrowserActivity;)Landroid/database/Cursor;

    move-result-object v3

    instance-of v3, v3, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity$3;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v3}, Lcom/android/music/TrackBrowserActivity;->access$700(Lcom/android/music/TrackBrowserActivity;)Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;

    invoke-virtual {v0, p1, p2}, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;->moveItem(II)V

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity$3;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-virtual {v3}, Landroid/app/ListActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    check-cast v3, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    invoke-virtual {v3}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity$3;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-virtual {v3}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/AbsListView;->invalidateViews()V

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity$3;->this$0:Lcom/android/music/TrackBrowserActivity;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/android/music/TrackBrowserActivity;->access$802(Lcom/android/music/TrackBrowserActivity;Z)Z

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity$3;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v3}, Lcom/android/music/TrackBrowserActivity;->access$700(Lcom/android/music/TrackBrowserActivity;)Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v3, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity$3;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v3}, Lcom/android/music/TrackBrowserActivity;->access$700(Lcom/android/music/TrackBrowserActivity;)Landroid/database/Cursor;

    move-result-object v3

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity$3;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v4}, Lcom/android/music/TrackBrowserActivity;->access$700(Lcom/android/music/TrackBrowserActivity;)Landroid/database/Cursor;

    move-result-object v4

    const-string v5, "play_order"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity$3;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v3}, Lcom/android/music/TrackBrowserActivity;->access$700(Lcom/android/music/TrackBrowserActivity;)Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v3, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity$3;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v3}, Lcom/android/music/TrackBrowserActivity;->access$700(Lcom/android/music/TrackBrowserActivity;)Landroid/database/Cursor;

    move-result-object v3

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity$3;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v4}, Lcom/android/music/TrackBrowserActivity;->access$700(Lcom/android/music/TrackBrowserActivity;)Landroid/database/Cursor;

    move-result-object v4

    const-string v5, "play_order"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity$3;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-virtual {v3}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity$3;->this$0:Lcom/android/music/TrackBrowserActivity;

    invoke-static {v4}, Lcom/android/music/TrackBrowserActivity;->access$900(Lcom/android/music/TrackBrowserActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5, v1, v2}, Landroid/provider/MediaStore$Audio$Playlists$Members;->moveItem(Landroid/content/ContentResolver;JII)Z

    goto :goto_0
.end method
