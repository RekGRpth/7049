.class public Lcom/android/music/TrackBrowserActivity;
.super Landroid/app/ListActivity;
.source "TrackBrowserActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/ServiceConnection;
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Lcom/android/music/MusicUtils$Defs;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/music/TrackBrowserActivity$AlbumArtFetcher;,
        Lcom/android/music/TrackBrowserActivity$TrackListAdapter;,
        Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;
    }
.end annotation


# static fields
.field private static final ADD_SONG:I = 0x17

.field private static final CLEAR_PLAYLIST:I = 0x14

.field private static final PLAY_ALL:I = 0x13

.field private static final Q_ALL:I = 0x11

.field private static final Q_SELECTED:I = 0x10

.field private static final REMOVE:I = 0x15

.field private static final SAVE_AS_PLAYLIST:I = 0x12

.field private static final SEARCH:I = 0x16

.field private static final TAG:Ljava/lang/String; = "TrackBrowser"

.field private static mLastListPosCourse:I

.field private static mLastListPosFine:I


# instance fields
.field private mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

.field private mAdapterSent:Z

.field private mAlbumArtBitmap:Landroid/graphics/Bitmap;

.field private mAlbumId:Ljava/lang/String;

.field private mArtistId:Ljava/lang/String;

.field private mAsyncAlbumArtFetcher:Lcom/android/music/TrackBrowserActivity$AlbumArtFetcher;

.field private mCurTrackPos:I

.field private mCurrentAlbumName:Ljava/lang/String;

.field private mCurrentArtistNameForAlbum:Ljava/lang/String;

.field private mCurrentTrackName:Ljava/lang/String;

.field private mCursorCols:[Ljava/lang/String;

.field private mDeletedOneRow:Z

.field private mDrmClient:Landroid/drm/DrmManagerClient;

.field private mDropListener:Lcom/android/music/TouchInterceptor$DropListener;

.field private mEditMode:Z

.field private mEnableClearPlaylistMenu:Z

.field private mGenre:Ljava/lang/String;

.field private mIsInBackgroud:Z

.field private mIsMounted:Z

.field private mMusicTrackBrowserPlugin:Lcom/android/music/ext/IMusicTrackBrowser;

.field private mNowPlayingListener:Landroid/content/BroadcastReceiver;

.field private mOrientation:I

.field private mPlaylist:Ljava/lang/String;

.field private mPlaylistMemberCols:[Ljava/lang/String;

.field mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

.field private mReScanHandler:Landroid/os/Handler;

.field private mRemoveListener:Lcom/android/music/TouchInterceptor$RemoveListener;

.field private mResetSdStatus:Z

.field private mScanListener:Landroid/content/BroadcastReceiver;

.field private mSelectedId:J

.field private mSelectedPosition:I

.field private mService:Lcom/android/music/IMediaPlaybackService;

.field private mSortOrder:Ljava/lang/String;

.field private mSubMenu:Landroid/view/SubMenu;

.field private mToken:Lcom/android/music/MusicUtils$ServiceToken;

.field private mTrackCursor:Landroid/database/Cursor;

.field private mTrackList:Landroid/widget/ListView;

.field private mTrackListListener:Landroid/content/BroadcastReceiver;

.field private mWithtabs:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/android/music/TrackBrowserActivity;->mLastListPosCourse:I

    sput v0, Lcom/android/music/TrackBrowserActivity;->mLastListPosFine:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    iput-boolean v1, p0, Lcom/android/music/TrackBrowserActivity;->mDeletedOneRow:Z

    iput-boolean v1, p0, Lcom/android/music/TrackBrowserActivity;->mEditMode:Z

    iput-boolean v1, p0, Lcom/android/music/TrackBrowserActivity;->mAdapterSent:Z

    iput-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mAlbumArtBitmap:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mAsyncAlbumArtFetcher:Lcom/android/music/TrackBrowserActivity$AlbumArtFetcher;

    iput-boolean v1, p0, Lcom/android/music/TrackBrowserActivity;->mResetSdStatus:Z

    iput-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    iput-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/music/TrackBrowserActivity;->mCurTrackPos:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/music/TrackBrowserActivity;->mIsMounted:Z

    iput-boolean v1, p0, Lcom/android/music/TrackBrowserActivity;->mWithtabs:Z

    iput-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mMusicTrackBrowserPlugin:Lcom/android/music/ext/IMusicTrackBrowser;

    iput-boolean v1, p0, Lcom/android/music/TrackBrowserActivity;->mEnableClearPlaylistMenu:Z

    iput-boolean v1, p0, Lcom/android/music/TrackBrowserActivity;->mIsInBackgroud:Z

    iput-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mSubMenu:Landroid/view/SubMenu;

    new-instance v0, Lcom/android/music/TrackBrowserActivity$1;

    invoke-direct {v0, p0}, Lcom/android/music/TrackBrowserActivity$1;-><init>(Lcom/android/music/TrackBrowserActivity;)V

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mScanListener:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/music/TrackBrowserActivity$2;

    invoke-direct {v0, p0}, Lcom/android/music/TrackBrowserActivity$2;-><init>(Lcom/android/music/TrackBrowserActivity;)V

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mReScanHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/music/TrackBrowserActivity$3;

    invoke-direct {v0, p0}, Lcom/android/music/TrackBrowserActivity$3;-><init>(Lcom/android/music/TrackBrowserActivity;)V

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mDropListener:Lcom/android/music/TouchInterceptor$DropListener;

    new-instance v0, Lcom/android/music/TrackBrowserActivity$4;

    invoke-direct {v0, p0}, Lcom/android/music/TrackBrowserActivity$4;-><init>(Lcom/android/music/TrackBrowserActivity;)V

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mRemoveListener:Lcom/android/music/TouchInterceptor$RemoveListener;

    new-instance v0, Lcom/android/music/TrackBrowserActivity$5;

    invoke-direct {v0, p0}, Lcom/android/music/TrackBrowserActivity$5;-><init>(Lcom/android/music/TrackBrowserActivity;)V

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackListListener:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/music/TrackBrowserActivity$6;

    invoke-direct {v0, p0}, Lcom/android/music/TrackBrowserActivity$6;-><init>(Lcom/android/music/TrackBrowserActivity;)V

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mNowPlayingListener:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/music/TrackBrowserActivity$7;

    invoke-direct {v0, p0}, Lcom/android/music/TrackBrowserActivity$7;-><init>(Lcom/android/music/TrackBrowserActivity;)V

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    return-void
.end method

.method static synthetic access$000(Lcom/android/music/TrackBrowserActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mReScanHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/music/TrackBrowserActivity;I)V
    .locals 0
    .param p0    # Lcom/android/music/TrackBrowserActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/music/TrackBrowserActivity;->removePlaylistItem(I)V

    return-void
.end method

.method static synthetic access$102(Lcom/android/music/TrackBrowserActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/TrackBrowserActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/TrackBrowserActivity;->mIsMounted:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/android/music/TrackBrowserActivity;)Lcom/android/music/IMediaPlaybackService;
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/music/TrackBrowserActivity;)I
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity;

    iget v0, p0, Lcom/android/music/TrackBrowserActivity;->mOrientation:I

    return v0
.end method

.method static synthetic access$1300(Lcom/android/music/TrackBrowserActivity;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mCursorCols:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/music/TrackBrowserActivity;)Z
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity;

    iget-boolean v0, p0, Lcom/android/music/TrackBrowserActivity;->mEditMode:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/android/music/TrackBrowserActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAlbumId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/music/TrackBrowserActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/music/TrackBrowserActivity;Landroid/database/Cursor;Ljava/lang/Boolean;)I
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity;
    .param p1    # Landroid/database/Cursor;
    .param p2    # Ljava/lang/Boolean;

    invoke-direct {p0, p1, p2}, Lcom/android/music/TrackBrowserActivity;->getDrmRightsStatus(Landroid/database/Cursor;Ljava/lang/Boolean;)I

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/android/music/TrackBrowserActivity;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAlbumArtBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/android/music/TrackBrowserActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0    # Lcom/android/music/TrackBrowserActivity;
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/android/music/TrackBrowserActivity;->mAlbumArtBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$202(Lcom/android/music/TrackBrowserActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/TrackBrowserActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/TrackBrowserActivity;->mResetSdStatus:Z

    return p1
.end method

.method static synthetic access$300(Lcom/android/music/TrackBrowserActivity;)Landroid/view/SubMenu;
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mSubMenu:Landroid/view/SubMenu;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/music/TrackBrowserActivity;)Lcom/android/music/TrackBrowserActivity$TrackListAdapter;
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/music/TrackBrowserActivity;Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity;
    .param p1    # Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-direct {p0, p1, p2, p3}, Lcom/android/music/TrackBrowserActivity;->getTrackCursor(Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/music/TrackBrowserActivity;)Landroid/database/Cursor;
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$702(Lcom/android/music/TrackBrowserActivity;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0    # Lcom/android/music/TrackBrowserActivity;
    .param p1    # Landroid/database/Cursor;

    iput-object p1, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$800(Lcom/android/music/TrackBrowserActivity;)Z
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity;

    iget-boolean v0, p0, Lcom/android/music/TrackBrowserActivity;->mDeletedOneRow:Z

    return v0
.end method

.method static synthetic access$802(Lcom/android/music/TrackBrowserActivity;Z)Z
    .locals 0
    .param p0    # Lcom/android/music/TrackBrowserActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/music/TrackBrowserActivity;->mDeletedOneRow:Z

    return p1
.end method

.method static synthetic access$900(Lcom/android/music/TrackBrowserActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/music/TrackBrowserActivity;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    return-object v0
.end method

.method private addSongToPlaylist(Landroid/net/Uri;)V
    .locals 13
    .param p1    # Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "TrackBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addSongToPlaylist: data="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "\'"

    const-string v1, "\'\'"

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_data LIKE \'%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "file:///"

    const-string v2, ""

    invoke-virtual {v8, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v0, "TrackBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addSongToPlaylist: where="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v0, "TrackBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addSongToPlaylist: track id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    new-array v9, v0, [J

    const/4 v0, 0x0

    int-to-long v1, v12

    aput-wide v1, v9, v0

    const-string v0, "TrackBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addSongToPlaylist: mPlaylist="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v1, "nowplaying"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0, v9}, Lcom/android/music/MusicUtils;->addToCurrentPlaylist(Landroid/content/Context;[J)V

    :goto_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v6, 0x0

    :cond_3
    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mMusicTrackBrowserPlugin:Lcom/android/music/ext/IMusicTrackBrowser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mMusicTrackBrowserPlugin:Lcom/android/music/ext/IMusicTrackBrowser;

    invoke-interface {v0}, Lcom/android/music/ext/IMusicTrackBrowser;->getNoneAudioString()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_0

    const/4 v0, 0x0

    invoke-static {p0, v11, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    invoke-static {p0, v9, v0, v1}, Lcom/android/music/MusicUtils;->addToPlaylist(Landroid/content/Context;[JJ)V

    goto :goto_1
.end method

.method private checkDrmRightsForPlay(Landroid/database/Cursor;ILjava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 12
    .param p1    # Landroid/database/Cursor;
    .param p2    # I
    .param p3    # Ljava/lang/Boolean;

    const/4 v11, 0x1

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    invoke-interface {p1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    const-string v8, "is_drm"

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const-string v8, "TrackBrowser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkDrmRightsForPlay: isDrm="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v3, :cond_0

    const/4 v8, 0x1

    :try_start_0
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    invoke-interface {p1, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_0
    return-object v8

    :cond_0
    :try_start_1
    iput p2, p0, Lcom/android/music/TrackBrowserActivity;->mCurTrackPos:I

    const-string v8, "drm_method"

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const-string v8, "TrackBrowser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkDrmRightsForPlay: drmMethod="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v2, v11, :cond_1

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    invoke-interface {p1, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_0

    :cond_1
    const/4 v8, 0x1

    :try_start_2
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-direct {p0, p1, v8}, Lcom/android/music/TrackBrowserActivity;->getDrmRightsStatus(Landroid/database/Cursor;Ljava/lang/Boolean;)I

    move-result v7

    packed-switch v7, :pswitch_data_0

    :pswitch_0
    const-string v8, "TrackBrowser"

    const-string v9, "No such rights status for current DRM file!!"

    invoke-static {v8, v9}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v8

    invoke-interface {p1, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_0

    :pswitch_1
    :try_start_3
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_2

    sget-object v8, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v8, :cond_2

    invoke-static {p1}, Lcom/android/music/MusicUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v4

    sget-object v8, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v8}, Lcom/android/music/IMediaPlaybackService;->getQueue()[J

    move-result-object v0

    sget-object v8, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v8}, Lcom/android/music/IMediaPlaybackService;->getQueuePosition()I

    move-result v1

    const-string v8, "TrackBrowser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "curPos="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", position="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v4, v0}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v8

    if-eqz v8, :cond_2

    if-ne p2, v1, :cond_2

    sget-boolean v8, Lcom/android/music/MediaPlaybackService;->mTrackCompleted:Z

    if-nez v8, :cond_2

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v8

    invoke-interface {p1, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto/16 :goto_0

    :cond_2
    :try_start_4
    iget-object v8, p0, Lcom/android/music/TrackBrowserActivity;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v8, 0x0

    invoke-static {p0, p0, v8}, Landroid/drm/DrmManagerClient;->showConsumeDialog(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/Dialog;
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v6

    :try_start_5
    const-string v8, "TrackBrowser"

    const-string v9, "RemoteException in service call!"

    invoke-static {v8, v9}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v8

    invoke-interface {p1, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto/16 :goto_0

    :pswitch_2
    :try_start_6
    iget-object v8, p0, Lcom/android/music/TrackBrowserActivity;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-direct {p0, p1}, Lcom/android/music/TrackBrowserActivity;->getUri(Landroid/database/Cursor;)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, p0, v9, v10}, Landroid/drm/DrmManagerClient;->showLicenseAcquisitionDialog(Landroid/content/Context;Landroid/net/Uri;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/Dialog;
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v8

    invoke-interface {p1, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    throw v8

    :pswitch_3
    :try_start_7
    iget-object v8, p0, Lcom/android/music/TrackBrowserActivity;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {p0, v8, v9}, Landroid/drm/DrmManagerClient;->showSecureTimerInvalidDialog(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/Dialog;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getDrmRightsStatus(Landroid/database/Cursor;Ljava/lang/Boolean;)I
    .locals 5
    .param p1    # Landroid/database/Cursor;
    .param p2    # Ljava/lang/Boolean;

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/android/music/TrackBrowserActivity;->getUri(Landroid/database/Cursor;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v2, v1, v3}, Landroid/drm/DrmManagerClient;->checkRightsStatusForTap(Landroid/net/Uri;I)I

    move-result v0

    :goto_0
    const-string v2, "TrackBrowser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDrmRightsStatus: rightsStatus="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v2, v1, v3}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Landroid/net/Uri;I)I

    move-result v0

    goto :goto_0
.end method

.method private getTrackCursor(Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 10
    .param p1    # Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v8, 0x0

    const-string v0, "title_pinyin_key"

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mSortOrder:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "title != \'\'"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mGenre:Ljava/lang/String;

    if-eqz v0, :cond_4

    const-string v0, "external"

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mGenre:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v0, v2, v3}, Landroid/provider/MediaStore$Audio$Genres$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "filter"

    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    :cond_1
    const-string v0, "title_key"

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mSortOrder:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mCursorCols:[Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mSortOrder:Ljava/lang/String;

    move-object v0, p1

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;->doQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v8

    :cond_2
    :goto_0
    if-eqz v8, :cond_3

    if-eqz p3, :cond_3

    const/4 v0, 0x0

    invoke-virtual {p0, v8, v0}, Lcom/android/music/TrackBrowserActivity;->init(Landroid/database/Cursor;Z)V

    invoke-direct {p0}, Lcom/android/music/TrackBrowserActivity;->setTitle()V

    :cond_3
    return-object v8

    :cond_4
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v2, "nowplaying"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v0, :cond_2

    new-instance v8, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;

    sget-object v0, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mCursorCols:[Ljava/lang/String;

    invoke-direct {v8, p0, v0, v2}, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;-><init>(Lcom/android/music/TrackBrowserActivity;Lcom/android/music/IMediaPlaybackService;[Ljava/lang/String;)V

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v2, "podcasts"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, " AND is_podcast=1"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "filter"

    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    :cond_6
    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mCursorCols:[Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "title_key"

    move-object v0, p1

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;->doQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v8

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v2, "recentlyadded"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "filter"

    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    :cond_8
    const-string v0, "numweeks"

    const/4 v2, 0x2

    invoke-static {p0, v0, v2}, Lcom/android/music/MusicUtils;->getIntPref(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    const v2, 0x93a80

    mul-int v7, v0, v2

    const-string v0, " AND date_added>"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v5, 0x3e8

    div-long/2addr v2, v5

    int-to-long v5, v7

    sub-long/2addr v2, v5

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mCursorCols:[Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "title_key"

    move-object v0, p1

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;->doQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v8

    goto/16 :goto_0

    :cond_9
    const-string v0, "external"

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "filter"

    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    :cond_a
    const-string v0, "play_order"

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mSortOrder:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylistMemberCols:[Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mSortOrder:Ljava/lang/String;

    move-object v0, p1

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;->doQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v8

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAlbumId:Ljava/lang/String;

    if-eqz v0, :cond_c

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " AND album_id="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mAlbumId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "track, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mSortOrder:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mSortOrder:Ljava/lang/String;

    :cond_c
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mArtistId:Ljava/lang/String;

    if-eqz v0, :cond_d

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " AND artist_id="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mArtistId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    const-string v0, " AND is_music=1"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "filter"

    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    :cond_e
    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mCursorCols:[Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mSortOrder:Ljava/lang/String;

    move-object v0, p1

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;->doQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v8

    goto/16 :goto_0
.end method

.method private getUri(Landroid/database/Cursor;)Landroid/net/Uri;
    .locals 5
    .param p1    # Landroid/database/Cursor;

    const/4 v0, -0x1

    :try_start_0
    const-string v2, "audio_id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    return-object v2

    :catch_0
    move-exception v1

    const-string v2, "_id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private initAdapter()V
    .locals 12

    const v11, 0x7f07005d

    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    if-nez v0, :cond_3

    const-string v0, "TrackBrowser"

    const-string v1, "starting query"

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/music/TrackBrowserActivity;->mEditMode:Z

    if-eqz v2, :cond_2

    const v3, 0x7f030006

    :goto_0
    new-array v5, v8, [Ljava/lang/String;

    new-array v6, v8, [I

    const-string v2, "nowplaying"

    iget-object v7, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v10, "podcasts"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v10, "recentlyadded"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v8, v9

    :cond_0
    move-object v2, p0

    invoke-direct/range {v0 .. v8}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;-><init>(Landroid/content/Context;Lcom/android/music/TrackBrowserActivity;ILandroid/database/Cursor;[Ljava/lang/String;[IZZ)V

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    invoke-virtual {p0, v0}, Landroid/app/ListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    iget-boolean v0, p0, Lcom/android/music/TrackBrowserActivity;->mWithtabs:Z

    if-nez v0, :cond_1

    invoke-virtual {p0, v11}, Landroid/app/Activity;->setTitle(I)V

    :cond_1
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    invoke-virtual {v0}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->getQueryHandler()Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;

    move-result-object v0

    invoke-direct {p0, v0, v4, v9}, Lcom/android/music/TrackBrowserActivity;->getTrackCursor(Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;Ljava/lang/String;Z)Landroid/database/Cursor;

    :goto_1
    return-void

    :cond_2
    const v3, 0x7f030014

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    invoke-virtual {v0, p0}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->setActivity(Lcom/android/music/TrackBrowserActivity;)V

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    invoke-virtual {v0}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->reloadStringOnLocaleChanges()V

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    invoke-virtual {p0, v0}, Landroid/app/ListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v0, v8}, Lcom/android/music/TrackBrowserActivity;->init(Landroid/database/Cursor;Z)V

    goto :goto_1

    :cond_4
    iget-boolean v0, p0, Lcom/android/music/TrackBrowserActivity;->mWithtabs:Z

    if-nez v0, :cond_5

    invoke-virtual {p0, v11}, Landroid/app/Activity;->setTitle(I)V

    :cond_5
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    invoke-virtual {v0}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->getQueryHandler()Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;

    move-result-object v0

    invoke-direct {p0, v0, v4, v9}, Lcom/android/music/TrackBrowserActivity;->getTrackCursor(Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;Ljava/lang/String;Z)Landroid/database/Cursor;

    goto :goto_1
.end method

.method private isMusic(Landroid/database/Cursor;)Z
    .locals 10
    .param p1    # Landroid/database/Cursor;

    const/4 v8, 0x0

    const-string v9, "title"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    const-string v9, "album"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    const-string v9, "artist"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v9, "<unknown>"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "<unknown>"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    if-eqz v6, :cond_0

    const-string v9, "recording"

    invoke-virtual {v6, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    :goto_0
    return v8

    :cond_0
    const-string v9, "is_music"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    const/4 v4, 0x1

    if-ltz v5, :cond_1

    iget-object v9, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    if-eqz v9, :cond_2

    const/4 v4, 0x1

    :cond_1
    :goto_1
    move v8, v4

    goto :goto_0

    :cond_2
    move v4, v8

    goto :goto_1
.end method

.method private moveItem(Z)V
    .locals 13
    .param p1    # Z

    iget-object v10, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v3

    iget-object v10, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    invoke-virtual {v10}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v4

    if-eqz p1, :cond_0

    const/4 v10, 0x1

    if-lt v4, v10, :cond_1

    :cond_0
    if-nez p1, :cond_2

    add-int/lit8 v10, v3, -0x1

    if-lt v4, v10, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v10, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    instance-of v10, v10, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;

    if-eqz v10, :cond_5

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    check-cast v1, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;

    if-eqz p1, :cond_3

    add-int/lit8 v10, v4, -0x1

    :goto_1
    invoke-virtual {v1, v4, v10}, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;->moveItem(II)V

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v10

    check-cast v10, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    invoke-virtual {v10}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/AbsListView;->invalidateViews()V

    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/android/music/TrackBrowserActivity;->mDeletedOneRow:Z

    if-eqz p1, :cond_4

    iget-object v10, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    add-int/lit8 v11, v4, -0x1

    invoke-virtual {v10, v11}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :cond_3
    add-int/lit8 v10, v4, 0x1

    goto :goto_1

    :cond_4
    iget-object v10, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    add-int/lit8 v11, v4, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :cond_5
    iget-object v10, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const-string v11, "play_order"

    invoke-interface {v10, v11}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    iget-object v10, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v10, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v10, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const-string v10, "external"

    iget-object v11, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    invoke-static {v10, v11, v12}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v0

    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    const-string v8, "_id=?"

    const/4 v10, 0x1

    new-array v9, v10, [Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    if-eqz p1, :cond_6

    const-string v10, "play_order"

    add-int/lit8 v11, v5, -0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const/4 v12, 0x0

    invoke-interface {v11, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v6, v0, v7, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v10}, Landroid/database/Cursor;->moveToPrevious()Z

    :goto_2
    const-string v10, "play_order"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const/4 v12, 0x0

    invoke-interface {v11, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v6, v0, v7, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    const-string v10, "play_order"

    add-int/lit8 v11, v5, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const/4 v12, 0x0

    invoke-interface {v11, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v6, v0, v7, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_2
.end method

.method private removeItem()V
    .locals 11

    const/4 v10, 0x0

    iget-object v7, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iget-object v7, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    invoke-virtual {v7}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v2

    if-eqz v1, :cond_0

    if-gez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v7, "nowplaying"

    iget-object v8, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    :try_start_0
    sget-object v7, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v7}, Lcom/android/music/IMediaPlaybackService;->getQueuePosition()I

    move-result v7

    if-eq v2, v7, :cond_2

    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/android/music/TrackBrowserActivity;->mDeletedOneRow:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    iget-object v7, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    invoke-virtual {v7}, Landroid/widget/AbsListView;->getSelectedView()Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v7, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    invoke-virtual {v7}, Landroid/widget/AbsListView;->invalidateViews()V

    iget-object v7, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    check-cast v7, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;

    invoke-virtual {v7, v2}, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;->removeItem(I)Z

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v7, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    invoke-virtual {v7}, Landroid/widget/AbsListView;->invalidateViews()V

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const-string v8, "_id"

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iget-object v7, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v7, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v7, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-string v7, "external"

    iget-object v8, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-static {v5, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8, v10, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    add-int/lit8 v1, v1, -0x1

    if-nez v1, :cond_4

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_4
    iget-object v7, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    if-ge v2, v1, :cond_5

    :goto_2
    invoke-virtual {v7, v2}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :cond_5
    move v2, v1

    goto :goto_2

    :catch_0
    move-exception v7

    goto :goto_1
.end method

.method private removePlaylistItem(I)V
    .locals 10
    .param p1    # I

    const/4 v9, 0x0

    const/4 v8, 0x1

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    iget-object v7, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    invoke-virtual {v7}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v7

    sub-int v7, p1, v7

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    if-nez v5, :cond_0

    const-string v6, "TrackBrowser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "No view when removing playlist item "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    sget-object v6, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v6, :cond_1

    sget-object v6, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v6}, Lcom/android/music/IMediaPlaybackService;->getQueuePosition()I

    move-result v6

    if-eq p1, v6, :cond_1

    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/android/music/TrackBrowserActivity;->mDeletedOneRow:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    invoke-virtual {v6}, Landroid/widget/AbsListView;->invalidateViews()V

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    instance-of v6, v6, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    check-cast v6, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;

    invoke-virtual {v6, p1}, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;->removeItem(I)Z

    :goto_2
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    invoke-virtual {v6}, Landroid/widget/AbsListView;->invalidateViews()V

    goto :goto_0

    :catch_0
    move-exception v1

    iput-boolean v8, p0, Lcom/android/music/TrackBrowserActivity;->mDeletedOneRow:Z

    goto :goto_1

    :cond_2
    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const-string v7, "_id"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v6, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-string v6, "external"

    iget-object v7, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {v6, v7, v8}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v4, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7, v9, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_2
.end method

.method private setAlbumArtBackground()V
    .locals 5

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mAsyncAlbumArtFetcher:Lcom/android/music/TrackBrowserActivity$AlbumArtFetcher;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mAsyncAlbumArtFetcher:Lcom/android/music/TrackBrowserActivity$AlbumArtFetcher;

    invoke-virtual {v1}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mAsyncAlbumArtFetcher:Lcom/android/music/TrackBrowserActivity$AlbumArtFetcher;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    new-instance v1, Lcom/android/music/TrackBrowserActivity$AlbumArtFetcher;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/music/TrackBrowserActivity$AlbumArtFetcher;-><init>(Lcom/android/music/TrackBrowserActivity;Lcom/android/music/TrackBrowserActivity$1;)V

    iput-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mAsyncAlbumArtFetcher:Lcom/android/music/TrackBrowserActivity$AlbumArtFetcher;

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mAlbumId:Ljava/lang/String;

    if-nez v1, :cond_1

    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mAsyncAlbumArtFetcher:Lcom/android/music/TrackBrowserActivity$AlbumArtFetcher;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Long;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mAlbumId:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "TrackBrowser"

    const-string v2, "Exception while fetching album art!!"

    invoke-static {v1, v2, v0}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private setTitle()V
    .locals 15

    const/4 v12, 0x0

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAlbumId:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v14

    :goto_0
    if-lez v14, :cond_3

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const-string v1, "album"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const-string v3, "album_id=?  AND artist_id=? "

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mAlbumId:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const-string v5, "artist_id"

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v5, "album"

    aput-object v5, v2, v0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/android/music/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    if-eqz v11, :cond_1

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eq v0, v14, :cond_0

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    :cond_0
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_1
    if-eqz v12, :cond_2

    const-string v0, "<unknown>"

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const v0, 0x7f070039

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    :cond_3
    :goto_1
    iget-boolean v0, p0, Lcom/android/music/TrackBrowserActivity;->mWithtabs:Z

    if-nez v0, :cond_4

    if-eqz v12, :cond_e

    invoke-virtual {p0, v12}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_4
    :goto_2
    return-void

    :cond_5
    const/4 v14, 0x0

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v1, "nowplaying"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {}, Lcom/android/music/MusicUtils;->getCurrentShuffleMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    const v0, 0x7f070019

    invoke-virtual {p0, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v12

    goto :goto_1

    :cond_7
    const v0, 0x7f070018

    invoke-virtual {p0, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v12

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v1, "podcasts"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const v0, 0x7f070030

    invoke-virtual {p0, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v12

    goto :goto_1

    :cond_9
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v1, "recentlyadded"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const v0, 0x7f07002e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v12

    goto :goto_1

    :cond_a
    const/4 v0, 0x1

    new-array v7, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "name"

    aput-object v1, v7, v0

    sget-object v0, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v5, p0

    invoke-static/range {v5 .. v10}, Lcom/android/music/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    if-eqz v11, :cond_3

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v0, 0x0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    :cond_b
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    :cond_c
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mGenre:Ljava/lang/String;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    new-array v7, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "name"

    aput-object v1, v7, v0

    sget-object v0, Landroid/provider/MediaStore$Audio$Genres;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mGenre:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v5, p0

    invoke-static/range {v5 .. v10}, Lcom/android/music/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    if-eqz v11, :cond_3

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v0, 0x0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    :cond_d
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    :cond_e
    const v0, 0x7f07001e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTitle(I)V

    goto/16 :goto_2
.end method

.method private unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V
    .locals 1
    .param p1    # Landroid/content/BroadcastReceiver;

    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-direct {p0, v0}, Lcom/android/music/TrackBrowserActivity;->moveItem(Z)V

    goto :goto_0

    :sswitch_1
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/music/TrackBrowserActivity;->moveItem(Z)V

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/android/music/TrackBrowserActivity;->removeItem()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x43 -> :sswitch_2
    .end sparse-switch
.end method

.method doSearch()V
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.intent.action.MEDIA_SEARCH"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v3, 0x10000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mCurrentTrackName:Ljava/lang/String;

    const-string v3, "<unknown>"

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mCurrentArtistNameForAlbum:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mCurrentTrackName:Ljava/lang/String;

    :goto_0
    const-string v3, "<unknown>"

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mCurrentAlbumName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "android.intent.extra.album"

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mCurrentAlbumName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    const-string v3, "android.intent.extra.focus"

    const-string v4, "audio/*"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const v3, 0x7f07005a

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "query"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mCurrentArtistNameForAlbum:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mCurrentTrackName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "android.intent.extra.artist"

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mCurrentArtistNameForAlbum:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public init(Landroid/database/Cursor;Z)V
    .locals 10
    .param p1    # Landroid/database/Cursor;
    .param p2    # Z

    const/4 v9, 0x0

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    if-nez v6, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    invoke-virtual {v6, p1}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    if-nez v6, :cond_2

    iget-boolean v6, p0, Lcom/android/music/TrackBrowserActivity;->mResetSdStatus:Z

    if-eqz v6, :cond_1

    invoke-static {}, Lcom/android/music/MusicUtils;->resetSdStatus()V

    iput-boolean v9, p0, Lcom/android/music/TrackBrowserActivity;->mResetSdStatus:Z

    :cond_1
    iget-boolean v6, p0, Lcom/android/music/TrackBrowserActivity;->mIsMounted:Z

    invoke-static {p0, v6}, Lcom/android/music/MusicUtils;->displayDatabaseError(Landroid/app/Activity;Z)V

    invoke-virtual {p0}, Landroid/app/Activity;->closeContextMenu()V

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mReScanHandler:Landroid/os/Handler;

    const-wide/16 v7, 0x3e8

    invoke-virtual {v6, v9, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lcom/android/music/MusicUtils;->hideDatabaseError(Landroid/app/Activity;)V

    invoke-direct {p0}, Lcom/android/music/TrackBrowserActivity;->setTitle()V

    sget v6, Lcom/android/music/TrackBrowserActivity;->mLastListPosCourse:I

    if-ltz v6, :cond_3

    iget-boolean v6, p0, Lcom/android/music/TrackBrowserActivity;->mWithtabs:Z

    if-eqz v6, :cond_3

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget v6, Lcom/android/music/TrackBrowserActivity;->mLastListPosCourse:I

    sget v7, Lcom/android/music/TrackBrowserActivity;->mLastListPosFine:I

    invoke-virtual {v5, v6, v7}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    if-nez p2, :cond_3

    const/4 v6, -0x1

    sput v6, Lcom/android/music/TrackBrowserActivity;->mLastListPosCourse:I

    :cond_3
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string v6, "com.android.music.metachanged"

    invoke-virtual {v2, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v6, "com.android.music.queuechanged"

    invoke-virtual {v2, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v6, "nowplaying"

    iget-object v7, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    :try_start_0
    sget-object v6, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v6}, Lcom/android/music/IMediaPlaybackService;->getQueuePosition()I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/app/ListActivity;->setSelection(I)V

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mNowPlayingListener:Landroid/content/BroadcastReceiver;

    new-instance v7, Landroid/content/IntentFilter;

    invoke-direct {v7, v2}, Landroid/content/IntentFilter;-><init>(Landroid/content/IntentFilter;)V

    invoke-virtual {p0, v6, v7}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mNowPlayingListener:Landroid/content/BroadcastReceiver;

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.android.music.metachanged"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0, v7}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "artist"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const-string v7, "artist_id"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-virtual {p0, v6}, Landroid/app/ListActivity;->setSelection(I)V

    :cond_5
    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackListListener:Landroid/content/BroadcastReceiver;

    new-instance v7, Landroid/content/IntentFilter;

    invoke-direct {v7, v2}, Landroid/content/IntentFilter;-><init>(Landroid/content/IntentFilter;)V

    invoke-virtual {p0, v6, v7}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackListListener:Landroid/content/BroadcastReceiver;

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.android.music.metachanged"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0, v7}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_6
    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v5, 0x1

    const/4 v3, -0x1

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    if-nez p2, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    invoke-virtual {v3}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->getQueryHandler()Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4, v5}, Lcom/android/music/TrackBrowserActivity;->getTrackCursor(Lcom/android/music/TrackBrowserActivity$TrackListAdapter$TrackQueryHandler;Ljava/lang/String;Z)Landroid/database/Cursor;

    goto :goto_0

    :sswitch_1
    if-ne p2, v3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    new-array v0, v5, [J

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedId:J

    aput-wide v4, v0, v3

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v3, v3

    invoke-static {p0, v0, v3, v4}, Lcom/android/music/MusicUtils;->addToPlaylist(Landroid/content/Context;[JJ)V

    goto :goto_0

    :sswitch_2
    if-ne p2, v3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-static {v3}, Lcom/android/music/MusicUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v0

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v3, v1

    invoke-static {p0, v0, v3, v4}, Lcom/android/music/MusicUtils;->addToPlaylist(Landroid/content/Context;[JJ)V

    goto :goto_0

    :sswitch_3
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/music/TrackBrowserActivity;->addSongToPlaylist(Landroid/net/Uri;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0xb -> :sswitch_0
        0x12 -> :sswitch_2
        0x17 -> :sswitch_3
    .end sparse-switch
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    instance-of v1, v1, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v1, :cond_0

    :try_start_0
    sget-object v1, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    iget v2, p0, Lcom/android/music/TrackBrowserActivity;->mCurTrackPos:I

    invoke-interface {v1, v2}, Lcom/android/music/IMediaPlaybackService;->setQueuePosition(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "TrackBrowser"

    const-string v2, "RemoteException when setQueuePosition: "

    invoke-static {v1, v2, v0}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    iget v2, p0, Lcom/android/music/TrackBrowserActivity;->mCurTrackPos:I

    invoke-static {p0, v1, v2}, Lcom/android/music/MusicUtils;->playAll(Landroid/content/Context;Landroid/database/Cursor;I)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/music/TrackBrowserActivity;->mOrientation:I

    const v0, 0x7f0c0039

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "TrackBrowser"

    const-string v1, "Configuration Changed at database error, return!"

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/music/TrackBrowserActivity;->mOrientation:I

    invoke-static {p0, v0}, Lcom/android/music/MusicUtils;->updateNowPlaying(Landroid/app/Activity;I)V

    :cond_1
    invoke-direct {p0}, Lcom/android/music/TrackBrowserActivity;->setAlbumArtBackground()V

    goto :goto_0
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 14
    .param p1    # Landroid/view/MenuItem;

    const/4 v11, 0x4

    const/4 v12, 0x0

    const/4 v9, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :pswitch_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v9

    :cond_0
    :goto_0
    return v9

    :pswitch_1
    iget v8, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedPosition:I

    iget-object v10, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-direct {p0, v10, v8, v11}, Lcom/android/music/TrackBrowserActivity;->checkDrmRightsForPlay(Landroid/database/Cursor;ILjava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-static {p0, v10, v8}, Lcom/android/music/MusicUtils;->playAll(Landroid/content/Context;Landroid/database/Cursor;I)V

    goto :goto_0

    :pswitch_2
    new-array v4, v9, [J

    iget-wide v10, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedId:J

    aput-wide v10, v4, v12

    invoke-static {p0, v4}, Lcom/android/music/MusicUtils;->addToCurrentPlaylist(Landroid/content/Context;[J)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object v5

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-class v10, Lcom/android/music/CreatePlaylist;

    invoke-virtual {v3, p0, v10}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    if-nez v5, :cond_1

    invoke-virtual {p0, v3, v11}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v5, v3, v11}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_4
    new-array v4, v9, [J

    iget-wide v10, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedId:J

    aput-wide v10, v4, v12

    invoke-interface {p1}, Landroid/view/MenuItem;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "playlist"

    const-wide/16 v12, 0x0

    invoke-virtual {v10, v11, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {p0, v4, v6, v7}, Lcom/android/music/MusicUtils;->addToPlaylist(Landroid/content/Context;[JJ)V

    goto :goto_0

    :pswitch_5
    iget-wide v10, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedId:J

    invoke-static {p0, v10, v11}, Lcom/android/music/MusicUtils;->setRingtone(Landroid/content/Context;J)V

    goto :goto_0

    :pswitch_6
    new-array v4, v9, [J

    iget-wide v10, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedId:J

    long-to-int v10, v10

    int-to-long v10, v10

    aput-wide v10, v4, v12

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v10, 0x7f070014

    invoke-virtual {p0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v10, v9, [Ljava/lang/Object;

    iget-object v11, p0, Lcom/android/music/TrackBrowserActivity;->mCurrentTrackName:Ljava/lang/String;

    aput-object v11, v10, v12

    invoke-static {v2, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v10, "description"

    invoke-virtual {v0, v10, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "items"

    invoke-virtual {v0, v10, v4}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-class v10, Lcom/android/music/DeleteItems;

    invoke-virtual {v3, p0, v10}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const/4 v10, -0x1

    invoke-virtual {p0, v3, v10}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :pswitch_7
    iget v10, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedPosition:I

    invoke-direct {p0, v10}, Lcom/android/music/TrackBrowserActivity;->removePlaylistItem(I)V

    goto/16 :goto_0

    :pswitch_8
    invoke-virtual {p0}, Lcom/android/music/TrackBrowserActivity;->doSearch()V

    goto/16 :goto_0

    :pswitch_9
    iget-object v10, p0, Lcom/android/music/TrackBrowserActivity;->mDrmClient:Landroid/drm/DrmManagerClient;

    sget-object v11, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-wide v12, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedId:J

    invoke-static {v11, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v10, p0, v11}, Landroid/drm/DrmManagerClient;->showProtectionInfoDialog(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/Dialog;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const/4 v10, 0x2

    const/4 v9, 0x5

    const/4 v8, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v3, "TrackBrowser"

    const-string v4, "onCreate"

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v9}, Landroid/app/Activity;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "withtabs"

    invoke-virtual {v2, v3, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/music/TrackBrowserActivity;->mWithtabs:Z

    iget-boolean v3, p0, Lcom/android/music/TrackBrowserActivity;->mWithtabs:Z

    if-eqz v3, :cond_3

    invoke-virtual {p0, v6}, Landroid/app/Activity;->requestWindowFeature(I)Z

    :cond_0
    :goto_0
    invoke-virtual {p0, v8}, Landroid/app/Activity;->setVolumeControlStream(I)V

    if-eqz p1, :cond_4

    const-string v3, "selectedtrack"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedId:J

    const-string v3, "album"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mAlbumId:Ljava/lang/String;

    const-string v3, "artist"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mArtistId:Ljava/lang/String;

    const-string v3, "playlist"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v3, "genre"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mGenre:Ljava/lang/String;

    const-string v3, "editmode"

    invoke-virtual {p1, v3, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/music/TrackBrowserActivity;->mEditMode:Z

    const-string v3, "curtrackpos"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/android/music/TrackBrowserActivity;->mCurTrackPos:I

    :goto_1
    const/16 v3, 0xa

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v3, v7

    const-string v4, "title"

    aput-object v4, v3, v6

    const-string v4, "_data"

    aput-object v4, v3, v10

    const-string v4, "album"

    aput-object v4, v3, v8

    const/4 v4, 0x4

    const-string v5, "artist"

    aput-object v5, v3, v4

    const-string v4, "artist_id"

    aput-object v4, v3, v9

    const/4 v4, 0x6

    const-string v5, "duration"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "is_drm"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "drm_method"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "title_pinyin_key"

    aput-object v5, v3, v4

    iput-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mCursorCols:[Ljava/lang/String;

    const/16 v3, 0xd

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v3, v7

    const-string v4, "title"

    aput-object v4, v3, v6

    const-string v4, "_data"

    aput-object v4, v3, v10

    const-string v4, "album"

    aput-object v4, v3, v8

    const/4 v4, 0x4

    const-string v5, "artist"

    aput-object v5, v3, v4

    const-string v4, "artist_id"

    aput-object v4, v3, v9

    const/4 v4, 0x6

    const-string v5, "duration"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "play_order"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "audio_id"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "is_music"

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "is_drm"

    aput-object v5, v3, v4

    const/16 v4, 0xb

    const-string v5, "drm_method"

    aput-object v5, v3, v4

    const/16 v4, 0xc

    const-string v5, "title_pinyin_key"

    aput-object v5, v3, v4

    iput-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylistMemberCols:[Ljava/lang/String;

    iget-boolean v3, p0, Lcom/android/music/TrackBrowserActivity;->mWithtabs:Z

    if-nez v3, :cond_1

    iget-boolean v3, p0, Lcom/android/music/TrackBrowserActivity;->mEditMode:Z

    if-eqz v3, :cond_5

    :cond_1
    const v3, 0x7f030008

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    :goto_2
    iget-boolean v3, p0, Lcom/android/music/TrackBrowserActivity;->mWithtabs:Z

    if-nez v3, :cond_2

    invoke-static {p0, p0}, Lcom/android/music/MusicUtils;->bindToService(Landroid/app/Activity;Landroid/content/ServiceConnection;)Lcom/android/music/MusicUtils$ServiceToken;

    move-result-object v3

    iput-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mToken:Lcom/android/music/MusicUtils$ServiceToken;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v3

    invoke-static {v3}, Lcom/android/music/ext/Extensions;->createPluginObject(Landroid/content/Context;)Lcom/android/music/ext/IMusicTrackBrowser;

    move-result-object v3

    iput-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mMusicTrackBrowserPlugin:Lcom/android/music/ext/IMusicTrackBrowser;

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mMusicTrackBrowserPlugin:Lcom/android/music/ext/IMusicTrackBrowser;

    if-eqz v3, :cond_2

    const-string v3, "TrackBrowser"

    const-string v4, "Get operator succeed"

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mMusicTrackBrowserPlugin:Lcom/android/music/ext/IMusicTrackBrowser;

    invoke-interface {v3}, Lcom/android/music/ext/IMusicTrackBrowser;->enableClearPlaylistMenu()Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/music/TrackBrowserActivity;->mEnableClearPlaylistMenu:Z

    :cond_2
    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v3

    iput-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    invoke-virtual {v3, v7}, Landroid/widget/ListView;->setCacheColorHint(I)V

    iget-boolean v3, p0, Lcom/android/music/TrackBrowserActivity;->mEditMode:Z

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    check-cast v3, Lcom/android/music/TouchInterceptor;

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mDropListener:Lcom/android/music/TouchInterceptor$DropListener;

    invoke-virtual {v3, v4}, Lcom/android/music/TouchInterceptor;->setDropListener(Lcom/android/music/TouchInterceptor$DropListener;)V

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    check-cast v3, Lcom/android/music/TouchInterceptor;

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mRemoveListener:Lcom/android/music/TouchInterceptor$RemoveListener;

    invoke-virtual {v3, v4}, Lcom/android/music/TouchInterceptor;->setRemoveListener(Lcom/android/music/TouchInterceptor$RemoveListener;)V

    :goto_3
    new-instance v3, Landroid/drm/DrmManagerClient;

    invoke-direct {v3, p0}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-direct {p0}, Lcom/android/music/TrackBrowserActivity;->initAdapter()V

    invoke-direct {p0}, Lcom/android/music/TrackBrowserActivity;->setAlbumArtBackground()V

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "file"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mScanListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    goto/16 :goto_0

    :cond_4
    const-string v3, "selectedtrack"

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedId:J

    const-string v3, "album"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mAlbumId:Ljava/lang/String;

    const-string v3, "artist"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mArtistId:Ljava/lang/String;

    const-string v3, "playlist"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v3, "genre"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mGenre:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.intent.action.EDIT"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/music/TrackBrowserActivity;->mEditMode:Z

    goto/16 :goto_1

    :cond_5
    const v3, 0x7f03000a

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    iput v3, p0, Lcom/android/music/TrackBrowserActivity;->mOrientation:I

    goto/16 :goto_2

    :cond_6
    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    invoke-virtual {v3, v6}, Landroid/widget/AbsListView;->setTextFilterEnabled(Z)V

    goto/16 :goto_3
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 10
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x5

    const v6, 0x7f070042

    invoke-interface {p1, v8, v5, v8, v6}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    const v5, 0x7f070043

    invoke-interface {p1, v8, v9, v8, v5}, Landroid/view/ContextMenu;->addSubMenu(IIII)Landroid/view/SubMenu;

    move-result-object v5

    iput-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mSubMenu:Landroid/view/SubMenu;

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mSubMenu:Landroid/view/SubMenu;

    invoke-static {p0, v5}, Lcom/android/music/MusicUtils;->makePlaylistMenu(Landroid/content/Context;Landroid/view/SubMenu;)V

    iget-boolean v5, p0, Lcom/android/music/TrackBrowserActivity;->mEditMode:Z

    if-eqz v5, :cond_0

    const/16 v5, 0x15

    const v6, 0x7f070058

    invoke-interface {p1, v8, v5, v8, v6}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :cond_0
    move-object v4, p3

    check-cast v4, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    iget v5, v4, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    iput v5, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedPosition:I

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    iget v6, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedPosition:I

    invoke-interface {v5, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    :try_start_0
    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const-string v6, "audio_id"

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v5, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedId:J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v3, 0x0

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const-string v7, "is_drm"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const-string v7, "drm_method"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v3, v9, :cond_1

    if-ne v3, v9, :cond_2

    if-ne v0, v9, :cond_2

    :cond_1
    const/4 v5, 0x2

    const v6, 0x7f07003f

    invoke-interface {p1, v8, v5, v8, v6}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :cond_2
    const/16 v5, 0xa

    const v6, 0x7f07000f

    invoke-interface {p1, v8, v5, v8, v6}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-direct {p0, v5}, Lcom/android/music/TrackBrowserActivity;->isMusic(Landroid/database/Cursor;)Z

    move-result v5

    if-eqz v5, :cond_3

    const/16 v5, 0x16

    const v6, 0x7f070024

    invoke-interface {p1, v8, v5, v8, v6}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :cond_3
    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const-string v7, "album"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mCurrentAlbumName:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const-string v7, "artist"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mCurrentArtistNameForAlbum:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    iget-object v6, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const-string v7, "title"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mCurrentTrackName:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mCurrentTrackName:Ljava/lang/String;

    invoke-interface {p1, v5}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    if-ne v3, v9, :cond_4

    const/16 v5, 0xf

    const v6, 0x2050062

    invoke-interface {p1, v8, v5, v8, v6}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :cond_4
    return-void

    :catch_0
    move-exception v1

    iget-wide v5, v4, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    iput-wide v5, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedId:J

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    iget-boolean v4, p0, Lcom/android/music/TrackBrowserActivity;->mWithtabs:Z

    if-eqz v4, :cond_0

    :goto_0
    return v1

    :cond_0
    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    if-nez v4, :cond_1

    const/16 v4, 0x13

    const v5, 0x7f070011

    invoke-interface {p1, v3, v4, v3, v5}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v4

    const v5, 0x7f020023

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    :cond_1
    const/16 v4, 0x8

    const v5, 0x7f07000d

    invoke-interface {p1, v3, v4, v3, v5}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    const/16 v4, 0x9

    const v5, 0x7f070010

    invoke-interface {p1, v3, v4, v3, v5}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v4

    const v5, 0x7f020026

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    if-eqz v4, :cond_5

    const/16 v4, 0x12

    const v5, 0x7f07004f

    invoke-interface {p1, v3, v4, v3, v5}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v4

    const v5, 0x7f020024

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v5, "recentlyadded"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v5, "podcasts"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    move v0, v2

    :goto_1
    if-eqz v0, :cond_2

    iget-boolean v4, p0, Lcom/android/music/TrackBrowserActivity;->mEnableClearPlaylistMenu:Z

    if-nez v4, :cond_3

    :cond_2
    iget-boolean v4, p0, Lcom/android/music/TrackBrowserActivity;->mEnableClearPlaylistMenu:Z

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v5, "nowplaying"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    const/16 v4, 0x14

    const v5, 0x7f070050

    invoke-interface {p1, v3, v4, v3, v5}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v4

    const v5, 0x7f02001c

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    :cond_4
    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mMusicTrackBrowserPlugin:Lcom/android/music/ext/IMusicTrackBrowser;

    if-eqz v4, :cond_5

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mMusicTrackBrowserPlugin:Lcom/android/music/ext/IMusicTrackBrowser;

    const/16 v5, 0x17

    invoke-interface {v4, p1, v5}, Lcom/android/music/ext/IMusicTrackBrowser;->createAddSongMenu(Landroid/view/Menu;I)V

    :cond_5
    const/16 v4, 0xd

    const v5, 0x7f070005

    invoke-interface {p1, v3, v4, v3, v5}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v3

    const v4, 0x7f02001e

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    invoke-static {p0, p1, v3}, Lcom/android/music/MusicUtils;->addSearchView(Landroid/app/Activity;Landroid/view/Menu;Landroid/widget/SearchView$OnQueryTextListener;)Landroid/widget/SearchView;

    move v1, v2

    goto/16 :goto_0

    :cond_6
    move v0, v3

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 5

    const/4 v4, 0x0

    const-string v2, "TrackBrowser"

    const-string v3, "onDestroy"

    invoke-static {v2, v3}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mAsyncAlbumArtFetcher:Lcom/android/music/TrackBrowserActivity$AlbumArtFetcher;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mAsyncAlbumArtFetcher:Lcom/android/music/TrackBrowserActivity$AlbumArtFetcher;

    invoke-virtual {v2}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mAsyncAlbumArtFetcher:Lcom/android/music/TrackBrowserActivity$AlbumArtFetcher;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-boolean v2, p0, Lcom/android/music/TrackBrowserActivity;->mWithtabs:Z

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v2

    sput v2, Lcom/android/music/TrackBrowserActivity;->mLastListPosCourse:I

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    sput v2, Lcom/android/music/TrackBrowserActivity;->mLastListPosFine:I

    :cond_1
    iget-boolean v2, p0, Lcom/android/music/TrackBrowserActivity;->mEditMode:Z

    if-eqz v2, :cond_2

    move-object v2, v1

    check-cast v2, Lcom/android/music/TouchInterceptor;

    invoke-virtual {v2, v4}, Lcom/android/music/TouchInterceptor;->setDropListener(Lcom/android/music/TouchInterceptor$DropListener;)V

    check-cast v1, Lcom/android/music/TouchInterceptor;

    invoke-virtual {v1, v4}, Lcom/android/music/TouchInterceptor;->setRemoveListener(Lcom/android/music/TouchInterceptor$RemoveListener;)V

    :cond_2
    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mToken:Lcom/android/music/MusicUtils$ServiceToken;

    invoke-static {v2}, Lcom/android/music/MusicUtils;->unbindFromService(Lcom/android/music/MusicUtils$ServiceToken;)V

    iput-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    :try_start_0
    const-string v2, "nowplaying"

    iget-object v3, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mNowPlayingListener:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v2}, Lcom/android/music/TrackBrowserActivity;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-boolean v2, p0, Lcom/android/music/TrackBrowserActivity;->mAdapterSent:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    invoke-virtual {v2, v4}, Lcom/android/music/TrackBrowserActivity$TrackListAdapter;->changeCursor(Landroid/database/Cursor;)V

    :cond_3
    invoke-virtual {p0, v4}, Landroid/app/ListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    iput-object v4, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mScanListener:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v2}, Lcom/android/music/TrackBrowserActivity;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    return-void

    :cond_4
    :try_start_1
    iget-object v2, p0, Lcom/android/music/TrackBrowserActivity;->mTrackListListener:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v2}, Lcom/android/music/TrackBrowserActivity;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    const-string v0, "MusicPerformanceTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Performance test][Music] play song start ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    instance-of v0, v0, Lcom/android/music/TrackBrowserActivity$NowPlayingCursor;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, p3, v1}, Lcom/android/music/TrackBrowserActivity;->checkDrmRightsForPlay(Landroid/database/Cursor;ILjava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/music/MusicUtils;->sService:Lcom/android/music/IMediaPlaybackService;

    invoke-interface {v0, p3}, Lcom/android/music/IMediaPlaybackService;->setQueuePosition(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_2
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, p3, v1}, Lcom/android/music/TrackBrowserActivity;->checkDrmRightsForPlay(Landroid/database/Cursor;ILjava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-static {p0, v0, p3}, Lcom/android/music/MusicUtils;->playAll(Landroid/content/Context;Landroid/database/Cursor;I)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 10
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x0

    const/4 v9, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v8

    iget-boolean v1, p0, Lcom/android/music/TrackBrowserActivity;->mWithtabs:Z

    if-eqz v1, :cond_0

    :goto_0
    return v8

    :cond_0
    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mMusicTrackBrowserPlugin:Lcom/android/music/ext/IMusicTrackBrowser;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mMusicTrackBrowserPlugin:Lcom/android/music/ext/IMusicTrackBrowser;

    invoke-interface {v1, p1, p0}, Lcom/android/music/ext/IMusicTrackBrowser;->handleAddSongMenu(Landroid/view/MenuItem;Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v8, v9

    goto :goto_0

    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    move v8, v0

    goto :goto_0

    :sswitch_0
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    invoke-static {p0, v0}, Lcom/android/music/MusicUtils;->playAll(Landroid/content/Context;Landroid/database/Cursor;)V

    move v8, v9

    goto :goto_0

    :sswitch_1
    invoke-static {}, Lcom/android/music/MusicUtils;->togglePartyShuffle()V

    goto :goto_0

    :sswitch_2
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v0

    const-string v3, "is_music=1"

    const/4 v4, 0x0

    const-string v5, "title_key"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/android/music/MusicUtils;->query(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-static {p0, v6}, Lcom/android/music/MusicUtils;->shuffleAll(Landroid/content/Context;Landroid/database/Cursor;)V

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move v8, v9

    goto :goto_0

    :sswitch_3
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    const-class v0, Lcom/android/music/CreatePlaylist;

    invoke-virtual {v7, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/16 v0, 0x12

    invoke-virtual {p0, v7, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    move v8, v9

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v1, "nowplaying"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/android/music/MusicUtils;->clearQueue()V

    :goto_1
    move v8, v9

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/android/music/MusicUtils;->clearPlaylist(Landroid/content/Context;I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :sswitch_5
    invoke-static {p0}, Lcom/android/music/MusicUtils;->startEffectPanel(Landroid/app/Activity;)Z

    move-result v8

    goto :goto_0

    :sswitch_6
    iget-boolean v0, p0, Lcom/android/music/TrackBrowserActivity;->mIsInBackgroud:Z

    if-nez v0, :cond_4

    invoke-virtual {p0}, Landroid/app/Activity;->onBackPressed()V

    :cond_4
    move v8, v9

    goto :goto_0

    :sswitch_7
    invoke-virtual {p0}, Landroid/app/Activity;->onSearchRequested()Z

    move v8, v9

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0x9 -> :sswitch_2
        0xd -> :sswitch_5
        0x12 -> :sswitch_3
        0x13 -> :sswitch_0
        0x14 -> :sswitch_4
        0x102002c -> :sswitch_6
        0x7f0c0040 -> :sswitch_7
    .end sparse-switch
.end method

.method public onPause()V
    .locals 4

    const-string v1, "TrackBrowser"

    const-string v2, "onPause"

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mReScanHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v1, "selectedtrack"

    iget-wide v2, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedId:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "curtrackpos"

    iget v2, p0, Lcom/android/music/TrackBrowserActivity;->mCurTrackPos:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/music/TrackBrowserActivity;->mIsInBackgroud:Z

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 8
    .param p1    # Landroid/view/Menu;

    const/4 v4, 0x1

    const/16 v7, 0x14

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v2

    iget-boolean v5, p0, Lcom/android/music/TrackBrowserActivity;->mWithtabs:Z

    if-eqz v5, :cond_0

    :goto_0
    return v2

    :cond_0
    invoke-static {p1}, Lcom/android/music/MusicUtils;->setPartyShuffleMenuIcon(Landroid/view/Menu;)V

    iget-boolean v5, p0, Lcom/android/music/TrackBrowserActivity;->mEnableClearPlaylistMenu:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    if-eqz v5, :cond_1

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mTrackList:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/AdapterView;->getCount()I

    move-result v5

    if-gtz v5, :cond_3

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    :goto_1
    const/4 v1, 0x1

    new-instance v0, Landroid/content/Intent;

    const-string v5, "android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5, v0, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v5

    if-nez v5, :cond_2

    const/4 v1, 0x0

    :cond_2
    const/16 v5, 0xd

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    if-nez v5, :cond_4

    const-string v4, "TrackBrowser"

    const-string v5, "Track cursor is null, disable option menu!"

    invoke-static {v4, v5}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v6, "recentlyadded"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    const-string v6, "podcasts"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_4
    move v2, v4

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "TrackBrowser"

    const-string v1, "onResume>>>"

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mTrackCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidateViews()V

    :cond_0
    invoke-static {p0}, Lcom/android/music/MusicUtils;->setSpinnerState(Landroid/app/Activity;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/music/TrackBrowserActivity;->mIsInBackgroud:Z

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/music/TrackBrowserActivity;->mOrientation:I

    invoke-static {p0, v0}, Lcom/android/music/MusicUtils;->updateNowPlaying(Landroid/app/Activity;I)V

    :cond_1
    const-string v0, "TrackBrowser"

    const-string v1, "onResume<<<"

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mAdapter:Lcom/android/music/TrackBrowserActivity$TrackListAdapter;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/music/TrackBrowserActivity;->mAdapterSent:Z

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v0, "selectedtrack"

    iget-wide v1, p0, Lcom/android/music/TrackBrowserActivity;->mSelectedId:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "artist"

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mArtistId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "album"

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mAlbumId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "playlist"

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mPlaylist:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "genre"

    iget-object v1, p0, Lcom/android/music/TrackBrowserActivity;->mGenre:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "editmode"

    iget-boolean v1, p0, Lcom/android/music/TrackBrowserActivity;->mEditMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "curtrackpos"

    iget v1, p0, Lcom/android/music/TrackBrowserActivity;->mCurTrackPos:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    invoke-static {p2}, Lcom/android/music/IMediaPlaybackService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/music/IMediaPlaybackService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    iget v0, p0, Lcom/android/music/TrackBrowserActivity;->mOrientation:I

    invoke-static {p0, v0}, Lcom/android/music/MusicUtils;->updateNowPlaying(Landroid/app/Activity;I)V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1    # Landroid/content/ComponentName;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/music/TrackBrowserActivity;->mService:Lcom/android/music/IMediaPlaybackService;

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method
