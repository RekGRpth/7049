.class public Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;
.super Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic$Stub;
.source "ServiceAvrcpStub.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SERVICE_AVRCP_STUB"


# instance fields
.field protected bPlaybackFlag:Z

.field protected bTrackAppSettingChangedFlag:Z

.field protected bTrackNowPlayingChangedFlag:Z

.field protected bTrackPosChangedFlag:Z

.field protected bTrackReachEndFlag:Z

.field protected bTrackReachStartFlag:Z

.field protected bTrackchangeFlag:Z

.field final mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mRepeatMode:I

.field private mService:Lcom/android/music/MediaPlaybackService;

.field private mShuffleMode:I


# direct methods
.method public constructor <init>(Lcom/android/music/MediaPlaybackService;)V
    .locals 2
    .param p1    # Lcom/android/music/MediaPlaybackService;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusic$Stub;-><init>()V

    iput v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mRepeatMode:I

    iput v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mShuffleMode:I

    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bPlaybackFlag:Z

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackchangeFlag:Z

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackReachStartFlag:Z

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackReachEndFlag:Z

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackPosChangedFlag:Z

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackAppSettingChangedFlag:Z

    iput-boolean v1, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackNowPlayingChangedFlag:Z

    iput-object p1, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    return-void
.end method


# virtual methods
.method public duration()J
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->duration()J

    move-result-wide v0

    return-wide v0
.end method

.method public enqueue([JI)V
    .locals 1
    .param p1    # [J
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/android/music/MediaPlaybackService;->enqueue([JI)V

    return-void
.end method

.method public getAlbumId()J
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->getAlbumId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAlbumName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->getAlbumName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getArtistName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->getArtistName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAudioId()J
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->getAudioId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getCapabilities()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEqualizeMode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getNowPlaying()[J
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->getQueue()[J

    move-result-object v0

    return-object v0
.end method

.method public getNowPlayingItemName(J)Ljava/lang/String;
    .locals 1
    .param p1    # J

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlayStatus()B
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v1}, Lcom/android/music/MediaPlaybackService;->isPlaying()Z

    move-result v1

    if-ne v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->isCursorNull()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getQueuePosition()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->getQueuePosition()I

    move-result v0

    return v0
.end method

.method public getRepeatMode()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->getRepeatMode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mRepeatMode:I

    iget v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mRepeatMode:I

    return v0
.end method

.method public getScanMode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getShuffleMode()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->getShuffleMode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mShuffleMode:I

    iget v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mShuffleMode:I

    return v0
.end method

.method public getTrackName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->getTrackName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public informBatteryStatusOfCT()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public informDisplayableCharacterSet(I)Z
    .locals 1
    .param p1    # I

    const/16 v0, 0x6a

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/music/MediaPlaybackService;->gotoNext(Z)V

    return-void
.end method

.method public nextGroup()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/music/MediaPlaybackService;->gotoNext(Z)V

    return-void
.end method

.method protected notifyAppSettingChanged()V
    .locals 5

    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackAppSettingChangedFlag:Z

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackAppSettingChangedFlag:Z

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_2

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;

    :try_start_0
    invoke-interface {v2}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;->notifyAppSettingChanged()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_2
.end method

.method public notifyBTAvrcp(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "SERVICE_AVRCP_STUB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[AVRCP] notifyBTAvrcp "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.android.music.playstatechanged"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->getPlayStatus()B

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->notifyPlaybackStatus(B)V

    :cond_0
    const-string v0, "com.android.music.playbackcomplete"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->notifyTrackChanged()V

    :cond_1
    const-string v0, "com.android.music.queuechanged"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->notifyTrackChanged()V

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->notifyNowPlayingContentChanged()V

    :cond_2
    const-string v0, "com.android.music.metachanged"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->notifyTrackChanged()V

    :cond_3
    return-void
.end method

.method protected notifyNowPlayingContentChanged()V
    .locals 6

    const-string v3, "SERVICE_AVRCP_STUB"

    const-string v4, "[AVRCP] notifyNowPlayingContentChanged "

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackNowPlayingChangedFlag:Z

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    const-string v3, "SERVICE_AVRCP_STUB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[AVRCP] notifyNowPlayingContentChanged  N= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_1

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;

    :try_start_0
    invoke-interface {v2}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;->notifyNowPlayingContentChanged()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_2
.end method

.method protected notifyPlaybackPosChanged()V
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackPosChangedFlag:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    if-nez v0, :cond_0

    :cond_0
    return-void
.end method

.method protected notifyPlaybackStatus(B)V
    .locals 6
    .param p1    # B

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bPlaybackFlag:Z

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    const-string v3, "SERVICE_AVRCP_STUB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[AVRCP] notifyPlaybackStatus "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " N= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;

    :try_start_0
    invoke-interface {v2, p1}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;->notifyPlaybackStatus(B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    return-void

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method protected notifyTrackChanged()V
    .locals 6

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackchangeFlag:Z

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    const-string v3, "SERVICE_AVRCP_STUB"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[AVRCP] notifyTrackChanged  N= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;

    :try_start_0
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->getAudioId()J

    move-result-wide v3

    invoke-interface {v2, v3, v4}, Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;->notifyTrackChanged(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    return-void

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method protected notifyTrackReachEnd()V
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackReachEndFlag:Z

    if-eq v0, v1, :cond_0

    :cond_0
    return-void
.end method

.method protected notifyTrackReachStart()V
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackReachStartFlag:Z

    if-eq v0, v1, :cond_0

    :cond_0
    return-void
.end method

.method protected notifyVolumehanged(B)V
    .locals 3
    .param p1    # B

    const-string v0, "SERVICE_AVRCP_STUB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[AVRCP] notifyVolumehanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public open([JI)V
    .locals 1
    .param p1    # [J
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/android/music/MediaPlaybackService;->open([JI)V

    return-void
.end method

.method public pause()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->pause()V

    return-void
.end method

.method public play()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->play()V

    return-void
.end method

.method public position()J
    .locals 2

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->position()J

    move-result-wide v0

    return-wide v0
.end method

.method public prev()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->prev()V

    return-void
.end method

.method public prevGroup()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->prev()V

    return-void
.end method

.method public regNotificationEvent(BI)Z
    .locals 4
    .param p1    # B
    .param p2    # I

    const/4 v0, 0x1

    sparse-switch p1, :sswitch_data_0

    const-string v0, "SERVICE_AVRCP_STUB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[AVRCP] MusicApp doesn\'t support eventId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    iput-boolean v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bPlaybackFlag:Z

    const-string v1, "SERVICE_AVRCP_STUB"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[AVRCP] bPlaybackFlag flag is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bPlaybackFlag:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_1
    iput-boolean v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackchangeFlag:Z

    const-string v0, "SERVICE_AVRCP_STUB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[AVRCP] bTrackchange flag is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackchangeFlag:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackchangeFlag:Z

    goto :goto_0

    :sswitch_2
    iput-boolean v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->bTrackNowPlayingChangedFlag:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x9 -> :sswitch_2
    .end sparse-switch
.end method

.method public registerCallback(Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;)V
    .locals 2
    .param p1    # Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;

    const-string v0, "SERVICE_AVRCP_STUB"

    const-string v1, "[AVRCP] ServiceAvrcpStub. registerCallback"

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->getRepeatMode()I

    invoke-virtual {p0}, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->getShuffleMode()I

    return-void
.end method

.method public resume()V
    .locals 0

    return-void
.end method

.method public setEqualizeMode(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public setPlayerApplicationSettingValue(BB)Z
    .locals 1
    .param p1    # B
    .param p2    # B

    const/4 v0, 0x0

    return v0
.end method

.method public setQueuePosition(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0, p1}, Lcom/android/music/MediaPlaybackService;->setQueuePosition(I)V

    return-void
.end method

.method public setRepeatMode(I)Z
    .locals 7
    .param p1    # I

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return v1

    :pswitch_0
    const/4 v0, 0x0

    :goto_1
    const-string v3, "SERVICE_AVRCP_STUB"

    const-string v4, "[AVRCP] setRepeatMode musid_mode:%d"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v1, v0}, Lcom/android/music/MediaPlaybackService;->setRepeatMode(I)V

    move v1, v2

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setScanMode(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public setShuffleMode(I)Z
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :pswitch_0
    const/4 v0, 0x0

    :goto_1
    const-string v1, "SERVICE_AVRCP_STUB"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[AVRCP] setShuffleMode music_mode:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v1, v0}, Lcom/android/music/MediaPlaybackService;->setShuffleMode(I)V

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public stop()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mService:Lcom/android/music/MediaPlaybackService;

    invoke-virtual {v0}, Lcom/android/music/MediaPlaybackService;->stop()V

    return-void
.end method

.method public unregisterCallback(Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;)V
    .locals 2
    .param p1    # Lcom/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback;

    const-string v0, "SERVICE_AVRCP_STUB"

    const-string v1, "[AVRCP] ServiceAvrcpStub. unregisterCallback"

    invoke-static {v0, v1}, Lcom/android/music/MusicLogUtils;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/bluetooth/avrcp/ServiceAvrcpStub;->mAvrcpCallbacksList:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    :cond_0
    return-void
.end method
