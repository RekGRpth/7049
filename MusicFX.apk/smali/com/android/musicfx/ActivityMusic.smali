.class public Lcom/android/musicfx/ActivityMusic;
.super Landroid/app/Activity;
.source "ActivityMusic.java"

# interfaces
.implements Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;


# static fields
.field public static final ATTACH_AUX_AUDIO_EFFECT:Ljava/lang/String; = "com.android.music.attachauxaudioeffect"

.field private static final AUX_AUDIO_EFFECT_ID:Ljava/lang/String; = "auxaudioeffectid"

.field public static final DETACH_AUX_AUDIO_EFFECT:Ljava/lang/String; = "com.android.music.detachauxaudioeffect"

.field private static final EQUALIZER_MAX_BANDS:I = 0x20

.field private static final EQViewElementIds:[[I

.field private static final PRESETREVERBPRESETSTRINGS:[Ljava/lang/String;

.field private static final PRESET_REVERB_NONE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MusicFXActivityMusic"


# instance fields
.field private mAudioSession:I

.field private mBassBoostSupported:Z

.field private mCallingPackageName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mEQPreset:I

.field private mEQPresetNames:[Ljava/lang/String;

.field private mEQPresetPrevious:I

.field private mEQPresetUserBandLevelsPrev:[I

.field private mEQPresetUserPos:I

.field private mEqualizerMinBandLevel:I

.field private final mEqualizerSeekBar:[Lcom/android/musicfx/seekbar/SeekBar;

.field private mEqualizerSupported:Z

.field private mFormatBuilder:Ljava/lang/StringBuilder;

.field private mFormatter:Ljava/util/Formatter;

.field private mIsHeadsetOn:Z

.field private mNumberEqualizerBands:I

.field private mPRPreset:I

.field private mPRPresetPrevious:I

.field private mPresetReverbSupported:Z

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mToast:Landroid/widget/Toast;

.field private mToggleSwitch:Landroid/widget/CompoundButton;

.field private mVirtualizerSupported:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x20

    new-array v0, v0, [[I

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    new-array v1, v3, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v5

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    new-array v1, v3, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v6

    new-array v1, v3, [I

    fill-array-data v1, :array_4

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v3, [I

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v3, [I

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v3, [I

    fill-array-data v2, :array_a

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v3, [I

    fill-array-data v2, :array_b

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v3, [I

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v3, [I

    fill-array-data v2, :array_d

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v3, [I

    fill-array-data v2, :array_e

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v3, [I

    fill-array-data v2, :array_f

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v3, [I

    fill-array-data v2, :array_10

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v3, [I

    fill-array-data v2, :array_11

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v3, [I

    fill-array-data v2, :array_12

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v3, [I

    fill-array-data v2, :array_13

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v3, [I

    fill-array-data v2, :array_14

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v3, [I

    fill-array-data v2, :array_15

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v3, [I

    fill-array-data v2, :array_16

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v3, [I

    fill-array-data v2, :array_17

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v3, [I

    fill-array-data v2, :array_18

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-array v2, v3, [I

    fill-array-data v2, :array_19

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v3, [I

    fill-array-data v2, :array_1a

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-array v2, v3, [I

    fill-array-data v2, :array_1b

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v3, [I

    fill-array-data v2, :array_1c

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-array v2, v3, [I

    fill-array-data v2, :array_1d

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    new-array v2, v3, [I

    fill-array-data v2, :array_1e

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    new-array v2, v3, [I

    fill-array-data v2, :array_1f

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/musicfx/ActivityMusic;->EQViewElementIds:[[I

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v4

    const-string v1, "SmallRoom"

    aput-object v1, v0, v5

    const-string v1, "MediumRoom"

    aput-object v1, v0, v3

    const-string v1, "LargeRoom"

    aput-object v1, v0, v6

    const-string v1, "MediumHall"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "LargeHall"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Plate"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/musicfx/ActivityMusic;->PRESETREVERBPRESETSTRINGS:[Ljava/lang/String;

    return-void

    :array_0
    .array-data 4
        0x7f090005
        0x7f090004
    .end array-data

    :array_1
    .array-data 4
        0x7f090007
        0x7f090006
    .end array-data

    :array_2
    .array-data 4
        0x7f090009
        0x7f090008
    .end array-data

    :array_3
    .array-data 4
        0x7f09000b
        0x7f09000a
    .end array-data

    :array_4
    .array-data 4
        0x7f09000d
        0x7f09000c
    .end array-data

    :array_5
    .array-data 4
        0x7f09000f
        0x7f09000e
    .end array-data

    :array_6
    .array-data 4
        0x7f090011
        0x7f090010
    .end array-data

    :array_7
    .array-data 4
        0x7f090013
        0x7f090012
    .end array-data

    :array_8
    .array-data 4
        0x7f090015
        0x7f090014
    .end array-data

    :array_9
    .array-data 4
        0x7f090017
        0x7f090016
    .end array-data

    :array_a
    .array-data 4
        0x7f090019
        0x7f090018
    .end array-data

    :array_b
    .array-data 4
        0x7f09001b
        0x7f09001a
    .end array-data

    :array_c
    .array-data 4
        0x7f09001d
        0x7f09001c
    .end array-data

    :array_d
    .array-data 4
        0x7f09001f
        0x7f09001e
    .end array-data

    :array_e
    .array-data 4
        0x7f090021
        0x7f090020
    .end array-data

    :array_f
    .array-data 4
        0x7f090023
        0x7f090022
    .end array-data

    :array_10
    .array-data 4
        0x7f090025
        0x7f090024
    .end array-data

    :array_11
    .array-data 4
        0x7f090027
        0x7f090026
    .end array-data

    :array_12
    .array-data 4
        0x7f090029
        0x7f090028
    .end array-data

    :array_13
    .array-data 4
        0x7f09002b
        0x7f09002a
    .end array-data

    :array_14
    .array-data 4
        0x7f09002d
        0x7f09002c
    .end array-data

    :array_15
    .array-data 4
        0x7f09002f
        0x7f09002e
    .end array-data

    :array_16
    .array-data 4
        0x7f090031
        0x7f090030
    .end array-data

    :array_17
    .array-data 4
        0x7f090033
        0x7f090032
    .end array-data

    :array_18
    .array-data 4
        0x7f090035
        0x7f090034
    .end array-data

    :array_19
    .array-data 4
        0x7f090037
        0x7f090036
    .end array-data

    :array_1a
    .array-data 4
        0x7f090039
        0x7f090038
    .end array-data

    :array_1b
    .array-data 4
        0x7f09003b
        0x7f09003a
    .end array-data

    :array_1c
    .array-data 4
        0x7f09003d
        0x7f09003c
    .end array-data

    :array_1d
    .array-data 4
        0x7f09003f
        0x7f09003e
    .end array-data

    :array_1e
    .array-data 4
        0x7f090041
        0x7f090040
    .end array-data

    :array_1f
    .array-data 4
        0x7f090043
        0x7f090042
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0x20

    new-array v0, v0, [Lcom/android/musicfx/seekbar/SeekBar;

    iput-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mEqualizerSeekBar:[Lcom/android/musicfx/seekbar/SeekBar;

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetUserPos:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/musicfx/ActivityMusic;->mIsHeadsetOn:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mFormatBuilder:Ljava/lang/StringBuilder;

    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/android/musicfx/ActivityMusic;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mFormatter:Ljava/util/Formatter;

    const-string v0, "empty"

    iput-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    const/4 v0, -0x4

    iput v0, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mToast:Landroid/widget/Toast;

    new-instance v0, Lcom/android/musicfx/ActivityMusic$1;

    invoke-direct {v0, p0}, Lcom/android/musicfx/ActivityMusic$1;-><init>(Lcom/android/musicfx/ActivityMusic;)V

    iput-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/android/musicfx/ActivityMusic;)Z
    .locals 1
    .param p0    # Lcom/android/musicfx/ActivityMusic;

    iget-boolean v0, p0, Lcom/android/musicfx/ActivityMusic;->mIsHeadsetOn:Z

    return v0
.end method

.method static synthetic access$002(Lcom/android/musicfx/ActivityMusic;Z)Z
    .locals 0
    .param p0    # Lcom/android/musicfx/ActivityMusic;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/musicfx/ActivityMusic;->mIsHeadsetOn:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/musicfx/ActivityMusic;)V
    .locals 0
    .param p0    # Lcom/android/musicfx/ActivityMusic;

    invoke-direct {p0}, Lcom/android/musicfx/ActivityMusic;->updateUIHeadset()V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/musicfx/ActivityMusic;)I
    .locals 1
    .param p0    # Lcom/android/musicfx/ActivityMusic;

    iget v0, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetPrevious:I

    return v0
.end method

.method static synthetic access$1002(Lcom/android/musicfx/ActivityMusic;I)I
    .locals 0
    .param p0    # Lcom/android/musicfx/ActivityMusic;
    .param p1    # I

    iput p1, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetPrevious:I

    return p1
.end method

.method static synthetic access$1100(Lcom/android/musicfx/ActivityMusic;I)V
    .locals 0
    .param p0    # Lcom/android/musicfx/ActivityMusic;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/musicfx/ActivityMusic;->equalizerSetPreset(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/musicfx/ActivityMusic;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/musicfx/ActivityMusic;

    iget-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/musicfx/ActivityMusic;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/musicfx/ActivityMusic;

    iget-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/musicfx/ActivityMusic;)I
    .locals 1
    .param p0    # Lcom/android/musicfx/ActivityMusic;

    iget v0, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    return v0
.end method

.method static synthetic access$500(Lcom/android/musicfx/ActivityMusic;Landroid/view/ViewGroup;Z)V
    .locals 0
    .param p0    # Lcom/android/musicfx/ActivityMusic;
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/android/musicfx/ActivityMusic;->setEnabledAllChildren(Landroid/view/ViewGroup;Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/android/musicfx/ActivityMusic;Z)V
    .locals 0
    .param p0    # Lcom/android/musicfx/ActivityMusic;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/android/musicfx/ActivityMusic;->sendMessageToAttachDetachEffect(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/android/musicfx/ActivityMusic;)V
    .locals 0
    .param p0    # Lcom/android/musicfx/ActivityMusic;

    invoke-direct {p0}, Lcom/android/musicfx/ActivityMusic;->showHeadsetMsg()V

    return-void
.end method

.method static synthetic access$800(Lcom/android/musicfx/ActivityMusic;)I
    .locals 1
    .param p0    # Lcom/android/musicfx/ActivityMusic;

    iget v0, p0, Lcom/android/musicfx/ActivityMusic;->mPRPresetPrevious:I

    return v0
.end method

.method static synthetic access$802(Lcom/android/musicfx/ActivityMusic;I)I
    .locals 0
    .param p0    # Lcom/android/musicfx/ActivityMusic;
    .param p1    # I

    iput p1, p0, Lcom/android/musicfx/ActivityMusic;->mPRPresetPrevious:I

    return p1
.end method

.method static synthetic access$900(Lcom/android/musicfx/ActivityMusic;I)V
    .locals 0
    .param p0    # Lcom/android/musicfx/ActivityMusic;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/musicfx/ActivityMusic;->presetReverbSetPreset(I)V

    return-void
.end method

.method private equalizerBandUpdate(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v2, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v3, Lcom/android/musicfx/ControlPanelEffect$Key;->eq_band_level:Lcom/android/musicfx/ControlPanelEffect$Key;

    move v4, p2

    move v5, p1

    invoke-static/range {v0 .. v5}, Lcom/android/musicfx/ControlPanelEffect;->setParameterInt(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;II)V

    return-void
.end method

.method private equalizerBandsInit(Landroid/view/View;)V
    .locals 14
    .param p1    # Landroid/view/View;

    iget-object v8, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v10, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v11, Lcom/android/musicfx/ControlPanelEffect$Key;->eq_num_bands:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static {v8, v9, v10, v11}, Lcom/android/musicfx/ControlPanelEffect;->getParameterInt(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;)I

    move-result v8

    iput v8, p0, Lcom/android/musicfx/ActivityMusic;->mNumberEqualizerBands:I

    iget-object v8, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v10, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v11, Lcom/android/musicfx/ControlPanelEffect$Key;->eq_preset_user_band_level:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static {v8, v9, v10, v11}, Lcom/android/musicfx/ControlPanelEffect;->getParameterIntArray(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;)[I

    move-result-object v8

    iput-object v8, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetUserBandLevelsPrev:[I

    iget-object v8, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v10, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v11, Lcom/android/musicfx/ControlPanelEffect$Key;->eq_center_freq:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static {v8, v9, v10, v11}, Lcom/android/musicfx/ControlPanelEffect;->getParameterIntArray(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;)[I

    move-result-object v4

    iget-object v8, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v10, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v11, Lcom/android/musicfx/ControlPanelEffect$Key;->eq_level_range:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static {v8, v9, v10, v11}, Lcom/android/musicfx/ControlPanelEffect;->getParameterIntArray(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;)[I

    move-result-object v1

    const/4 v8, 0x0

    aget v8, v1, v8

    iput v8, p0, Lcom/android/musicfx/ActivityMusic;->mEqualizerMinBandLevel:I

    const/4 v8, 0x1

    aget v5, v1, v8

    const/4 v0, 0x0

    :goto_0
    iget v8, p0, Lcom/android/musicfx/ActivityMusic;->mNumberEqualizerBands:I

    if-ge v0, v8, :cond_1

    aget v8, v4, v0

    div-int/lit16 v2, v8, 0x3e8

    int-to-float v3, v2

    const-string v7, ""

    const/high16 v8, 0x447a0000

    cmpl-float v8, v3, v8

    if-ltz v8, :cond_0

    const/high16 v8, 0x447a0000

    div-float/2addr v3, v8

    const-string v7, "k"

    :cond_0
    sget-object v8, Lcom/android/musicfx/ActivityMusic;->EQViewElementIds:[[I

    aget-object v8, v8, v0

    const/4 v9, 0x0

    aget v8, v8, v9

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%.0f "

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-direct {p0, v10, v11}, Lcom/android/musicfx/ActivityMusic;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Hz"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/android/musicfx/ActivityMusic;->mEqualizerSeekBar:[Lcom/android/musicfx/seekbar/SeekBar;

    sget-object v8, Lcom/android/musicfx/ActivityMusic;->EQViewElementIds:[[I

    aget-object v8, v8, v0

    const/4 v10, 0x1

    aget v8, v8, v10

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/android/musicfx/seekbar/SeekBar;

    aput-object v8, v9, v0

    iget-object v8, p0, Lcom/android/musicfx/ActivityMusic;->mEqualizerSeekBar:[Lcom/android/musicfx/seekbar/SeekBar;

    aget-object v8, v8, v0

    iget v9, p0, Lcom/android/musicfx/ActivityMusic;->mEqualizerMinBandLevel:I

    sub-int v9, v5, v9

    invoke-virtual {v8, v9}, Lcom/android/musicfx/seekbar/AbsSeekBar;->setMax(I)V

    iget-object v8, p0, Lcom/android/musicfx/ActivityMusic;->mEqualizerSeekBar:[Lcom/android/musicfx/seekbar/SeekBar;

    aget-object v8, v8, v0

    invoke-virtual {v8, p0}, Lcom/android/musicfx/seekbar/SeekBar;->setOnSeekBarChangeListener(Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/android/musicfx/ActivityMusic;->mNumberEqualizerBands:I

    :goto_1
    const/16 v8, 0x20

    if-ge v0, v8, :cond_2

    sget-object v8, Lcom/android/musicfx/ActivityMusic;->EQViewElementIds:[[I

    aget-object v8, v8, v0

    const/4 v9, 0x0

    aget v8, v8, v9

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    sget-object v8, Lcom/android/musicfx/ActivityMusic;->EQViewElementIds:[[I

    aget-object v8, v8, v0

    const/4 v9, 0x1

    aget v8, v8, v9

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const v8, 0x7f090001

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const-string v8, "+15 dB"

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v8, 0x7f090002

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const-string v8, "0 dB"

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v8, 0x7f090003

    invoke-virtual {p0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const-string v8, "-15 dB"

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/musicfx/ActivityMusic;->equalizerUpdateDisplay()V

    return-void
.end method

.method private equalizerSetPreset(I)V
    .locals 4
    .param p1    # I

    iget-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v2, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v3, Lcom/android/musicfx/ControlPanelEffect$Key;->eq_current_preset:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/android/musicfx/ControlPanelEffect;->setParameterInt(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;I)V

    invoke-direct {p0}, Lcom/android/musicfx/ActivityMusic;->equalizerUpdateDisplay()V

    return-void
.end method

.method private equalizerSpinnerInit(Landroid/widget/Spinner;)V
    .locals 4
    .param p1    # Landroid/widget/Spinner;

    const/high16 v3, 0x7f030000

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f040000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x1090008

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :goto_0
    new-instance v1, Lcom/android/musicfx/ActivityMusic$8;

    invoke-direct {v1, p0}, Lcom/android/musicfx/ActivityMusic$8;-><init>(Lcom/android/musicfx/ActivityMusic;)V

    invoke-virtual {p1, v1}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget v1, p0, Lcom/android/musicfx/ActivityMusic;->mEQPreset:I

    invoke-virtual {p1, v1}, Landroid/widget/AbsSpinner;->setSelection(I)V

    return-void

    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    invoke-direct {v0, p0, v3, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto :goto_0
.end method

.method private equalizerUpdateDisplay()V
    .locals 8

    iget-object v4, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v6, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v7, Lcom/android/musicfx/ControlPanelEffect$Key;->eq_band_level:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static {v4, v5, v6, v7}, Lcom/android/musicfx/ControlPanelEffect;->getParameterIntArray(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;)[I

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    iget v4, p0, Lcom/android/musicfx/ActivityMusic;->mNumberEqualizerBands:I

    if-ge v0, v4, :cond_0

    aget v2, v1, v0

    iget v4, p0, Lcom/android/musicfx/ActivityMusic;->mEqualizerMinBandLevel:I

    sub-int v3, v2, v4

    iget-object v4, p0, Lcom/android/musicfx/ActivityMusic;->mEqualizerSeekBar:[Lcom/android/musicfx/seekbar/SeekBar;

    aget-object v4, v4, v0

    invoke-virtual {v4, v3}, Lcom/android/musicfx/seekbar/ProgressBar;->setProgress(I)V

    add-int/lit8 v4, v0, 0x1

    int-to-short v0, v4

    goto :goto_0

    :cond_0
    return-void
.end method

.method private varargs format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mFormatBuilder:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mFormatter:Ljava/util/Formatter;

    invoke-virtual {v0, p1, p2}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    iget-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getStringForEq(I)V
    .locals 6
    .param p1    # I

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_a

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v4, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v5, Lcom/android/musicfx/ControlPanelEffect$Key;->eq_preset_name:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static {v2, v3, v4, v5, v0}, Lcom/android/musicfx/ControlPanelEffect;->getParameterString(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Normal"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    const v3, 0x7f060001

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    :goto_1
    add-int/lit8 v2, v0, 0x1

    int-to-short v0, v2

    goto :goto_0

    :cond_0
    const-string v2, "Classical"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    const v3, 0x7f060002

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    :cond_1
    const-string v2, "Dance"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    const v3, 0x7f060003

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    :cond_2
    const-string v2, "Flat"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    const v3, 0x7f060004

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    :cond_3
    const-string v2, "Folk"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    const v3, 0x7f060005

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    :cond_4
    const-string v2, "Heavy Metal"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    const v3, 0x7f060006

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    :cond_5
    const-string v2, "Hip Hop"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    const v3, 0x7f060007

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    :cond_6
    const-string v2, "Jazz"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    const v3, 0x7f060008

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_1

    :cond_7
    const-string v2, "Pop"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    const v3, 0x7f060009

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_1

    :cond_8
    const-string v2, "Rock"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    const v3, 0x7f06000a

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_1

    :cond_9
    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    aput-object v1, v2, v0

    goto/16 :goto_1

    :cond_a
    return-void
.end method

.method private presetReverbSetPreset(I)V
    .locals 6
    .param p1    # I

    iget-object v1, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v3, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v4, Lcom/android/musicfx/ControlPanelEffect$Key;->pr_current_preset:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static {v1, v2, v3, v4, p1}, Lcom/android/musicfx/ControlPanelEffect;->setParameterInt(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;I)V

    if-nez p1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.detachauxaudioeffect"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "auxaudioeffectid"

    iget v2, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    invoke-static {v2}, Lcom/android/musicfx/ControlPanelEffect;->getAuxiliaryEffectId(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v3, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v4, Lcom/android/musicfx/ControlPanelEffect$Key;->pr_enabled:Lcom/android/musicfx/ControlPanelEffect$Key;

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Lcom/android/musicfx/ControlPanelEffect;->setParameterBoolean(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;Z)V

    const-string v1, "MusicFXActivityMusic"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "presetReverbSetPreset ReverbEnabled = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    invoke-static {v3}, Lcom/android/musicfx/ControlPanelEffect;->getReverbEnabled(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v1, "MusicFXActivityMusic"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "presetReverb = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ,enabled = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    invoke-static {v3}, Lcom/android/musicfx/ControlPanelEffect;->getReverbEnabled(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.attachauxaudioeffect"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "auxaudioeffectid"

    iget v2, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    invoke-static {v2}, Lcom/android/musicfx/ControlPanelEffect;->getAuxiliaryEffectId(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    iget v1, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    invoke-static {v1}, Lcom/android/musicfx/ControlPanelEffect;->getReverbEnabled(I)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v3, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v4, Lcom/android/musicfx/ControlPanelEffect$Key;->pr_enabled:Lcom/android/musicfx/ControlPanelEffect$Key;

    const/4 v5, 0x1

    invoke-static {v1, v2, v3, v4, v5}, Lcom/android/musicfx/ControlPanelEffect;->setParameterBoolean(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;Z)V

    :cond_1
    const-string v1, "MusicFXActivityMusic"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "presetReverbSetPreset ReverbEnabled = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    invoke-static {v3}, Lcom/android/musicfx/ControlPanelEffect;->getReverbEnabled(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private reverbSpinnerInit(Landroid/widget/Spinner;)V
    .locals 5
    .param p1    # Landroid/widget/Spinner;

    const/high16 v4, 0x7f030000

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f070000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f040000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v2, 0x7f030003

    invoke-direct {v0, p0, v2, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const v2, 0x1090009

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :goto_0
    new-instance v2, Lcom/android/musicfx/ActivityMusic$7;

    invoke-direct {v2, p0}, Lcom/android/musicfx/ActivityMusic$7;-><init>(Lcom/android/musicfx/ActivityMusic;)V

    invoke-virtual {p1, v2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget v2, p0, Lcom/android/musicfx/ActivityMusic;->mPRPreset:I

    invoke-virtual {p1, v2}, Landroid/widget/AbsSpinner;->setSelection(I)V

    return-void

    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-direct {v0, p0, v4, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto :goto_0
.end method

.method private sendMessageToAttachDetachEffect(Z)V
    .locals 4
    .param p1    # Z

    if-eqz p1, :cond_0

    iget v1, p0, Lcom/android/musicfx/ActivityMusic;->mPRPresetPrevious:I

    if-nez v1, :cond_1

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.detachauxaudioeffect"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "auxaudioeffectid"

    iget v2, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    invoke-static {v2}, Lcom/android/musicfx/ControlPanelEffect;->getAuxiliaryEffectId(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "MusicFXActivityMusic"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "presetReverbSetPreset ReverbEnabled = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    invoke-static {v3}, Lcom/android/musicfx/ControlPanelEffect;->getReverbEnabled(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.attachauxaudioeffect"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "auxaudioeffectid"

    iget v2, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    invoke-static {v2}, Lcom/android/musicfx/ControlPanelEffect;->getAuxiliaryEffectId(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "MusicFXActivityMusic"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "presetReverbSetPreset ReverbEnabled = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    invoke-static {v3}, Lcom/android/musicfx/ControlPanelEffect;->getReverbEnabled(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setEnabledAllChildren(Landroid/view/ViewGroup;Z)V
    .locals 5
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    instance-of v4, v3, Landroid/widget/LinearLayout;

    if-nez v4, :cond_0

    instance-of v4, v3, Landroid/widget/RelativeLayout;

    if-eqz v4, :cond_1

    :cond_0
    move-object v2, v3

    check-cast v2, Landroid/view/ViewGroup;

    invoke-direct {p0, v2, p2}, Lcom/android/musicfx/ActivityMusic;->setEnabledAllChildren(Landroid/view/ViewGroup;Z)V

    :cond_1
    invoke-virtual {v3, p2}, Landroid/view/View;->setEnabled(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private showHeadsetMsg()V
    .locals 6

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mToast:Landroid/widget/Toast;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x7f06000e

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    iput-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mToast:Landroid/widget/Toast;

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mToast:Landroid/widget/Toast;

    const/16 v3, 0x11

    iget-object v4, p0, Lcom/android/musicfx/ActivityMusic;->mToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->getXOffset()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    iget-object v5, p0, Lcom/android/musicfx/ActivityMusic;->mToast:Landroid/widget/Toast;

    invoke-virtual {v5}, Landroid/widget/Toast;->getYOffset()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {v2, v3, v4, v5}, Landroid/widget/Toast;->setGravity(III)V

    :cond_0
    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private updateUI()V
    .locals 7

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v4, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v5, Lcom/android/musicfx/ControlPanelEffect$Key;->global_enabled:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static {v2, v3, v4, v5}, Lcom/android/musicfx/ControlPanelEffect;->getParameterBoolean(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mToggleSwitch:Landroid/widget/CompoundButton;

    invoke-virtual {v2, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const v2, 0x7f090045

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-direct {p0, v2, v0}, Lcom/android/musicfx/ActivityMusic;->setEnabledAllChildren(Landroid/view/ViewGroup;Z)V

    invoke-direct {p0}, Lcom/android/musicfx/ActivityMusic;->updateUIHeadset()V

    iget-boolean v2, p0, Lcom/android/musicfx/ActivityMusic;->mVirtualizerSupported:Z

    if-eqz v2, :cond_0

    const v2, 0x7f09004c

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/musicfx/seekbar/SeekBar;

    iget-object v3, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v5, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v6, Lcom/android/musicfx/ControlPanelEffect$Key;->virt_strength:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static {v3, v4, v5, v6}, Lcom/android/musicfx/ControlPanelEffect;->getParameterInt(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/musicfx/seekbar/ProgressBar;->setProgress(I)V

    :cond_0
    iget-boolean v2, p0, Lcom/android/musicfx/ActivityMusic;->mBassBoostSupported:Z

    if-eqz v2, :cond_1

    const v2, 0x7f090049

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/musicfx/seekbar/SeekBar;

    iget-object v3, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v5, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v6, Lcom/android/musicfx/ControlPanelEffect$Key;->bb_strength:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static {v3, v4, v5, v6}, Lcom/android/musicfx/ControlPanelEffect;->getParameterInt(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/musicfx/seekbar/ProgressBar;->setProgress(I)V

    :cond_1
    iget-boolean v2, p0, Lcom/android/musicfx/ActivityMusic;->mEqualizerSupported:Z

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/android/musicfx/ActivityMusic;->equalizerUpdateDisplay()V

    :cond_2
    iget-boolean v2, p0, Lcom/android/musicfx/ActivityMusic;->mPresetReverbSupported:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v4, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v5, Lcom/android/musicfx/ControlPanelEffect$Key;->pr_current_preset:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static {v2, v3, v4, v5}, Lcom/android/musicfx/ControlPanelEffect;->getParameterInt(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;)I

    move-result v1

    const v2, 0x7f09004f

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    invoke-virtual {v2, v1}, Landroid/widget/AbsSpinner;->setSelection(I)V

    :cond_3
    return-void
.end method

.method private updateUIHeadset()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mToggleSwitch:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f09004b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-boolean v3, p0, Lcom/android/musicfx/ActivityMusic;->mIsHeadsetOn:Z

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    const v0, 0x7f09004c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/musicfx/seekbar/SeekBar;

    iget-boolean v3, p0, Lcom/android/musicfx/ActivityMusic;->mIsHeadsetOn:Z

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    const v0, 0x7f09004a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-boolean v0, p0, Lcom/android/musicfx/ActivityMusic;->mIsHeadsetOn:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    const v0, 0x7f090048

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-boolean v3, p0, Lcom/android/musicfx/ActivityMusic;->mIsHeadsetOn:Z

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    const v0, 0x7f090049

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/musicfx/seekbar/SeekBar;

    iget-boolean v3, p0, Lcom/android/musicfx/ActivityMusic;->mIsHeadsetOn:Z

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    const v0, 0x7f090047

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-boolean v3, p0, Lcom/android/musicfx/ActivityMusic;->mIsHeadsetOn:Z

    if-nez v3, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 18
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v13, "android.media.extra.AUDIO_SESSION"

    const/4 v14, -0x4

    invoke-virtual {v7, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    const-string v13, "MusicFXActivityMusic"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "audio session: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    if-nez v13, :cond_0

    const-string v13, "MusicFXActivityMusic"

    const-string v14, "Package name is null"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    const/4 v13, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->setResult(I)V

    const-string v13, "MusicFXActivityMusic"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " ("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    invoke-static {v13, v14, v15}, Lcom/android/musicfx/ControlPanelEffect;->initEffectsPreferences(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-static {}, Landroid/media/audiofx/AudioEffect;->queryEffects()[Landroid/media/audiofx/AudioEffect$Descriptor;

    move-result-object v5

    const-string v13, "MusicFXActivityMusic"

    const-string v14, "Available effects:"

    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v5

    array-length v8, v3

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v8, :cond_5

    aget-object v4, v3, v6

    const-string v13, "MusicFXActivityMusic"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v15, v4, Landroid/media/audiofx/AudioEffect$Descriptor;->name:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", type: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v4, Landroid/media/audiofx/AudioEffect$Descriptor;->type:Ljava/util/UUID;

    invoke-virtual {v15}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v13, v4, Landroid/media/audiofx/AudioEffect$Descriptor;->type:Ljava/util/UUID;

    sget-object v14, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_VIRTUALIZER:Ljava/util/UUID;

    invoke-virtual {v13, v14}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/android/musicfx/ActivityMusic;->mVirtualizerSupported:Z

    :cond_1
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    iget-object v13, v4, Landroid/media/audiofx/AudioEffect$Descriptor;->type:Ljava/util/UUID;

    sget-object v14, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_BASS_BOOST:Ljava/util/UUID;

    invoke-virtual {v13, v14}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/android/musicfx/ActivityMusic;->mBassBoostSupported:Z

    goto :goto_2

    :cond_3
    iget-object v13, v4, Landroid/media/audiofx/AudioEffect$Descriptor;->type:Ljava/util/UUID;

    sget-object v14, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_EQUALIZER:Ljava/util/UUID;

    invoke-virtual {v13, v14}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/android/musicfx/ActivityMusic;->mEqualizerSupported:Z

    goto :goto_2

    :cond_4
    iget-object v13, v4, Landroid/media/audiofx/AudioEffect$Descriptor;->type:Ljava/util/UUID;

    sget-object v14, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_PRESET_REVERB:Ljava/util/UUID;

    invoke-virtual {v13, v14}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/android/musicfx/ActivityMusic;->mPresetReverbSupported:Z

    goto :goto_2

    :cond_5
    const v13, 0x7f030002

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->setContentView(I)V

    const v13, 0x7f090045

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v16, Lcom/android/musicfx/ControlPanelEffect$Key;->eq_num_presets:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static/range {v13 .. v16}, Lcom/android/musicfx/ControlPanelEffect;->getParameterInt(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;)I

    move-result v9

    add-int/lit8 v13, v9, 0x2

    new-array v13, v13, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/android/musicfx/ActivityMusic;->getStringForEq(I)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    const v14, 0x7f060011

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v13, v9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    add-int/lit8 v14, v9, 0x1

    const v15, 0x7f060012

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    add-int/lit8 v13, v9, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/musicfx/ActivityMusic;->mEQPresetUserPos:I

    new-instance v13, Landroid/widget/Switch;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/android/musicfx/ActivityMusic;->mToggleSwitch:Landroid/widget/CompoundButton;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/musicfx/ActivityMusic;->mToggleSwitch:Landroid/widget/CompoundButton;

    new-instance v14, Lcom/android/musicfx/ActivityMusic$2;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v12}, Lcom/android/musicfx/ActivityMusic$2;-><init>(Lcom/android/musicfx/ActivityMusic;Landroid/view/ViewGroup;)V

    invoke-virtual {v13, v14}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/musicfx/ActivityMusic;->mVirtualizerSupported:Z

    if-nez v13, :cond_6

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/musicfx/ActivityMusic;->mBassBoostSupported:Z

    if-nez v13, :cond_6

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/musicfx/ActivityMusic;->mEqualizerSupported:Z

    if-nez v13, :cond_6

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/musicfx/ActivityMusic;->mPresetReverbSupported:Z

    if-eqz v13, :cond_c

    :cond_6
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/musicfx/ActivityMusic;->mVirtualizerSupported:Z

    if-eqz v13, :cond_7

    const v13, 0x7f09004a

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    new-instance v14, Lcom/android/musicfx/ActivityMusic$3;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/android/musicfx/ActivityMusic$3;-><init>(Lcom/android/musicfx/ActivityMusic;)V

    invoke-virtual {v13, v14}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v13, 0x7f09004c

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/android/musicfx/seekbar/SeekBar;

    const/16 v13, 0x3e8

    invoke-virtual {v11, v13}, Lcom/android/musicfx/seekbar/AbsSeekBar;->setMax(I)V

    new-instance v13, Lcom/android/musicfx/ActivityMusic$4;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/android/musicfx/ActivityMusic$4;-><init>(Lcom/android/musicfx/ActivityMusic;)V

    invoke-virtual {v11, v13}, Lcom/android/musicfx/seekbar/SeekBar;->setOnSeekBarChangeListener(Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;)V

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/musicfx/ActivityMusic;->mBassBoostSupported:Z

    if-eqz v13, :cond_8

    const v13, 0x7f090047

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    new-instance v14, Lcom/android/musicfx/ActivityMusic$5;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/android/musicfx/ActivityMusic$5;-><init>(Lcom/android/musicfx/ActivityMusic;)V

    invoke-virtual {v13, v14}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v13, 0x7f090049

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/android/musicfx/seekbar/SeekBar;

    const/16 v13, 0x3e8

    invoke-virtual {v11, v13}, Lcom/android/musicfx/seekbar/AbsSeekBar;->setMax(I)V

    new-instance v13, Lcom/android/musicfx/ActivityMusic$6;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/android/musicfx/ActivityMusic$6;-><init>(Lcom/android/musicfx/ActivityMusic;)V

    invoke-virtual {v11, v13}, Lcom/android/musicfx/seekbar/SeekBar;->setOnSeekBarChangeListener(Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;)V

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/musicfx/ActivityMusic;->mEqualizerSupported:Z

    if-eqz v13, :cond_a

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v16, Lcom/android/musicfx/ControlPanelEffect$Key;->eq_current_preset:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static/range {v13 .. v16}, Lcom/android/musicfx/ControlPanelEffect;->getParameterInt(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/musicfx/ActivityMusic;->mEQPreset:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/musicfx/ActivityMusic;->mEQPreset:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/musicfx/ActivityMusic;->mEQPresetNames:[Ljava/lang/String;

    array-length v14, v14

    if-lt v13, v14, :cond_9

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/musicfx/ActivityMusic;->mEQPreset:I

    :cond_9
    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/musicfx/ActivityMusic;->mEQPreset:I

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/musicfx/ActivityMusic;->mEQPresetPrevious:I

    const v13, 0x7f090046

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/musicfx/ActivityMusic;->equalizerSpinnerInit(Landroid/widget/Spinner;)V

    const/high16 v13, 0x7f090000

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/musicfx/ActivityMusic;->equalizerBandsInit(Landroid/view/View;)V

    :cond_a
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/musicfx/ActivityMusic;->mPresetReverbSupported:Z

    if-eqz v13, :cond_b

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v16, Lcom/android/musicfx/ControlPanelEffect$Key;->pr_current_preset:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static/range {v13 .. v16}, Lcom/android/musicfx/ControlPanelEffect;->getParameterInt(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/musicfx/ActivityMusic;->mPRPreset:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/musicfx/ActivityMusic;->mPRPreset:I

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/musicfx/ActivityMusic;->mPRPresetPrevious:I

    const v13, 0x7f09004f

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/musicfx/ActivityMusic;->reverbSpinnerInit(Landroid/widget/Spinner;)V

    :cond_b
    :goto_3
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f050002

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/musicfx/ActivityMusic;->mToggleSwitch:Landroid/widget/CompoundButton;

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v13, v14, v15, v10, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/musicfx/ActivityMusic;->mToggleSwitch:Landroid/widget/CompoundButton;

    new-instance v14, Landroid/app/ActionBar$LayoutParams;

    const/4 v15, -0x2

    const/16 v16, -0x2

    const/16 v17, 0x15

    invoke-direct/range {v14 .. v17}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v2, v13, v14}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    const/16 v13, 0x18

    invoke-virtual {v2, v13}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    const v13, 0x7f090048

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setSelected(Z)V

    const v13, 0x7f09004b

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setSelected(Z)V

    const v13, 0x7f09004e

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setSelected(Z)V

    goto/16 :goto_0

    :cond_c
    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    const v13, 0x7f090044

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/musicfx/ControlPanelEffect;->saveToSharePreference(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public onProgressChanged(Lcom/android/musicfx/seekbar/SeekBar;IZ)V
    .locals 5
    .param p1    # Lcom/android/musicfx/seekbar/SeekBar;
    .param p2    # I
    .param p3    # Z

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    iget v3, p0, Lcom/android/musicfx/ActivityMusic;->mNumberEqualizerBands:I

    if-ge v0, v3, :cond_0

    sget-object v3, Lcom/android/musicfx/ActivityMusic;->EQViewElementIds:[[I

    aget-object v3, v3, v0

    const/4 v4, 0x1

    aget v3, v3, v4

    if-ne v1, v3, :cond_1

    iget v3, p0, Lcom/android/musicfx/ActivityMusic;->mEqualizerMinBandLevel:I

    add-int/2addr v3, p2

    int-to-short v2, v3

    if-eqz p3, :cond_0

    invoke-direct {p0, v0, v2}, Lcom/android/musicfx/ActivityMusic;->equalizerBandUpdate(II)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v3, v0, 0x1

    int-to-short v0, v3

    goto :goto_0
.end method

.method protected onResume()V
    .locals 5

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-boolean v2, p0, Lcom/android/musicfx/ActivityMusic;->mVirtualizerSupported:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/musicfx/ActivityMusic;->mBassBoostSupported:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/musicfx/ActivityMusic;->mEqualizerSupported:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/musicfx/ActivityMusic;->mPresetReverbSupported:Z

    if-eqz v2, :cond_2

    :cond_0
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v2, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v2, "audio"

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/android/musicfx/ActivityMusic;->mIsHeadsetOn:Z

    const-string v2, "MusicFXActivityMusic"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onResume: mIsHeadsetOn : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/musicfx/ActivityMusic;->mIsHeadsetOn:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/musicfx/ActivityMusic;->updateUI()V

    :cond_2
    return-void

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onStartTrackingTouch(Lcom/android/musicfx/seekbar/SeekBar;)V
    .locals 6
    .param p1    # Lcom/android/musicfx/seekbar/SeekBar;

    iget-object v2, p0, Lcom/android/musicfx/ActivityMusic;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/musicfx/ActivityMusic;->mCallingPackageName:Ljava/lang/String;

    iget v4, p0, Lcom/android/musicfx/ActivityMusic;->mAudioSession:I

    sget-object v5, Lcom/android/musicfx/ControlPanelEffect$Key;->eq_band_level:Lcom/android/musicfx/ControlPanelEffect$Key;

    invoke-static {v2, v3, v4, v5}, Lcom/android/musicfx/ControlPanelEffect;->getParameterIntArray(Landroid/content/Context;Ljava/lang/String;ILcom/android/musicfx/ControlPanelEffect$Key;)[I

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/android/musicfx/ActivityMusic;->mNumberEqualizerBands:I

    if-ge v0, v2, :cond_0

    aget v2, v1, v0

    invoke-direct {p0, v0, v2}, Lcom/android/musicfx/ActivityMusic;->equalizerBandUpdate(II)V

    add-int/lit8 v2, v0, 0x1

    int-to-short v0, v2

    goto :goto_0

    :cond_0
    iget v2, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetUserPos:I

    invoke-direct {p0, v2}, Lcom/android/musicfx/ActivityMusic;->equalizerSetPreset(I)V

    const v2, 0x7f090046

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iget v3, p0, Lcom/android/musicfx/ActivityMusic;->mEQPresetUserPos:I

    invoke-virtual {v2, v3}, Landroid/widget/AbsSpinner;->setSelection(I)V

    return-void
.end method

.method public onStopTrackingTouch(Lcom/android/musicfx/seekbar/SeekBar;)V
    .locals 0
    .param p1    # Lcom/android/musicfx/seekbar/SeekBar;

    invoke-direct {p0}, Lcom/android/musicfx/ActivityMusic;->equalizerUpdateDisplay()V

    return-void
.end method
