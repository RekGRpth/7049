.class public final Lcom/android/musicfx/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/musicfx/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final EQBand0SeekBar:I = 0x7f090004

.field public static final EQBand0TextView:I = 0x7f090005

.field public static final EQBand10SeekBar:I = 0x7f090018

.field public static final EQBand10TextView:I = 0x7f090019

.field public static final EQBand11SeekBar:I = 0x7f09001a

.field public static final EQBand11TextView:I = 0x7f09001b

.field public static final EQBand12SeekBar:I = 0x7f09001c

.field public static final EQBand12TextView:I = 0x7f09001d

.field public static final EQBand13SeekBar:I = 0x7f09001e

.field public static final EQBand13TextView:I = 0x7f09001f

.field public static final EQBand14SeekBar:I = 0x7f090020

.field public static final EQBand14TextView:I = 0x7f090021

.field public static final EQBand15SeekBar:I = 0x7f090022

.field public static final EQBand15TextView:I = 0x7f090023

.field public static final EQBand16SeekBar:I = 0x7f090024

.field public static final EQBand16TextView:I = 0x7f090025

.field public static final EQBand17SeekBar:I = 0x7f090026

.field public static final EQBand17TextView:I = 0x7f090027

.field public static final EQBand18SeekBar:I = 0x7f090028

.field public static final EQBand18TextView:I = 0x7f090029

.field public static final EQBand19SeekBar:I = 0x7f09002a

.field public static final EQBand19TextView:I = 0x7f09002b

.field public static final EQBand1SeekBar:I = 0x7f090006

.field public static final EQBand1TextView:I = 0x7f090007

.field public static final EQBand20SeekBar:I = 0x7f09002c

.field public static final EQBand20TextView:I = 0x7f09002d

.field public static final EQBand21SeekBar:I = 0x7f09002e

.field public static final EQBand21TextView:I = 0x7f09002f

.field public static final EQBand22SeekBar:I = 0x7f090030

.field public static final EQBand22TextView:I = 0x7f090031

.field public static final EQBand23SeekBar:I = 0x7f090032

.field public static final EQBand23TextView:I = 0x7f090033

.field public static final EQBand24SeekBar:I = 0x7f090034

.field public static final EQBand24TextView:I = 0x7f090035

.field public static final EQBand25SeekBar:I = 0x7f090036

.field public static final EQBand25TextView:I = 0x7f090037

.field public static final EQBand26SeekBar:I = 0x7f090038

.field public static final EQBand26TextView:I = 0x7f090039

.field public static final EQBand27SeekBar:I = 0x7f09003a

.field public static final EQBand27TextView:I = 0x7f09003b

.field public static final EQBand28SeekBar:I = 0x7f09003c

.field public static final EQBand28TextView:I = 0x7f09003d

.field public static final EQBand29SeekBar:I = 0x7f09003e

.field public static final EQBand29TextView:I = 0x7f09003f

.field public static final EQBand2SeekBar:I = 0x7f090008

.field public static final EQBand2TextView:I = 0x7f090009

.field public static final EQBand30SeekBar:I = 0x7f090040

.field public static final EQBand30TextView:I = 0x7f090041

.field public static final EQBand31SeekBar:I = 0x7f090042

.field public static final EQBand31TextView:I = 0x7f090043

.field public static final EQBand3SeekBar:I = 0x7f09000a

.field public static final EQBand3TextView:I = 0x7f09000b

.field public static final EQBand4SeekBar:I = 0x7f09000c

.field public static final EQBand4TextView:I = 0x7f09000d

.field public static final EQBand5SeekBar:I = 0x7f09000e

.field public static final EQBand5TextView:I = 0x7f09000f

.field public static final EQBand6SeekBar:I = 0x7f090010

.field public static final EQBand6TextView:I = 0x7f090011

.field public static final EQBand7SeekBar:I = 0x7f090012

.field public static final EQBand7TextView:I = 0x7f090013

.field public static final EQBand8SeekBar:I = 0x7f090014

.field public static final EQBand8TextView:I = 0x7f090015

.field public static final EQBand9SeekBar:I = 0x7f090016

.field public static final EQBand9TextView:I = 0x7f090017

.field public static final bBLayout:I = 0x7f090047

.field public static final bBStrengthSeekBar:I = 0x7f090049

.field public static final bBStrengthText:I = 0x7f090048

.field public static final centerLevelText:I = 0x7f090002

.field public static final contentSoundEffects:I = 0x7f090045

.field public static final eqSpinner:I = 0x7f090046

.field public static final eqcontainer:I = 0x7f090000

.field public static final maxLevelText:I = 0x7f090001

.field public static final minLevelText:I = 0x7f090003

.field public static final noEffectsTextView:I = 0x7f090044

.field public static final pRLayout:I = 0x7f09004d

.field public static final pRText:I = 0x7f09004e

.field public static final prSpinner:I = 0x7f09004f

.field public static final spinner_text:I = 0x7f090050

.field public static final vILayout:I = 0x7f09004a

.field public static final vIStrengthSeekBar:I = 0x7f09004c

.field public static final vIStrengthText:I = 0x7f09004b


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
