.class public interface abstract Lcom/android/musicfx/seekbar/SeekBar$OnSeekBarChangeListener;
.super Ljava/lang/Object;
.source "SeekBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/musicfx/seekbar/SeekBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnSeekBarChangeListener"
.end annotation


# virtual methods
.method public abstract onProgressChanged(Lcom/android/musicfx/seekbar/SeekBar;IZ)V
.end method

.method public abstract onStartTrackingTouch(Lcom/android/musicfx/seekbar/SeekBar;)V
.end method

.method public abstract onStopTrackingTouch(Lcom/android/musicfx/seekbar/SeekBar;)V
.end method
