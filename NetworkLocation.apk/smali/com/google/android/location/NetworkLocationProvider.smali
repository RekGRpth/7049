.class Lcom/google/android/location/NetworkLocationProvider;
.super Lcom/android/location/provider/LocationProvider;
.source "NetworkLocationProvider.java"

# interfaces
.implements Landroid/location/LocationListener;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/NetworkLocationProvider$1;,
        Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;
    }
.end annotation


# static fields
.field private static context:Landroid/content/Context;

.field private static instance:Lcom/google/android/location/NetworkLocationProvider;


# instance fields
.field private final client:Lcom/google/android/location/internal/client/NetworkLocationClient;

.field private final lock:Ljava/lang/Object;

.field private looper:Landroid/os/Looper;

.field private mProviderHandler:Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

.field private mStarted:Z

.field private final mThread:Ljava/lang/Thread;

.field private minTimeSeconds:I

.field private networkState:I

.field private status:I

.field private statusUpdateTime:J


# direct methods
.method private constructor <init>()V
    .locals 9

    const v7, 0x7fffffff

    const/4 v4, 0x0

    const/4 v8, -0x1

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/android/location/provider/LocationProvider;-><init>()V

    new-instance v5, Ljava/lang/Object;

    invoke-direct {v5}, Ljava/lang/Object;-><init>()V

    iput-object v5, p0, Lcom/google/android/location/NetworkLocationProvider;->lock:Ljava/lang/Object;

    const/4 v5, 0x2

    iput v5, p0, Lcom/google/android/location/NetworkLocationProvider;->status:I

    const-wide/16 v5, 0x0

    iput-wide v5, p0, Lcom/google/android/location/NetworkLocationProvider;->statusUpdateTime:J

    iput v7, p0, Lcom/google/android/location/NetworkLocationProvider;->minTimeSeconds:I

    iput v3, p0, Lcom/google/android/location/NetworkLocationProvider;->networkState:I

    sget-object v5, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    if-nez v5, :cond_0

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "must call init"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    iput-boolean v4, p0, Lcom/google/android/location/NetworkLocationProvider;->mStarted:Z

    new-instance v5, Ljava/lang/Thread;

    const/4 v6, 0x0

    const-string v7, "NetworkLocationProvider"

    invoke-direct {v5, v6, p0, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/google/android/location/NetworkLocationProvider;->mThread:Ljava/lang/Thread;

    iget-object v5, p0, Lcom/google/android/location/NetworkLocationProvider;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    sget-object v5, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-static {v5}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v5, "user-confirmed"

    invoke-interface {v2, v5, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v8, :cond_1

    if-ne v1, v3, :cond_2

    :goto_0
    invoke-direct {p0, v3}, Lcom/google/android/location/NetworkLocationProvider;->setUserConfirmedPreference(Z)V

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    iget-object v4, p0, Lcom/google/android/location/NetworkLocationProvider;->mThread:Ljava/lang/Thread;

    monitor-enter v4

    :goto_1
    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/location/NetworkLocationProvider;->mStarted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_3

    :try_start_1
    iget-object v3, p0, Lcom/google/android/location/NetworkLocationProvider;->mThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v3

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_0

    :cond_3
    :try_start_2
    new-instance v3, Lcom/google/android/location/internal/client/NetworkLocationClient;

    sget-object v5, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    const v6, 0x7fffffff

    iget-object v7, p0, Lcom/google/android/location/NetworkLocationProvider;->looper:Landroid/os/Looper;

    invoke-direct {v3, v5, v6, p0, v7}, Lcom/google/android/location/internal/client/NetworkLocationClient;-><init>(Landroid/content/Context;ILandroid/location/LocationListener;Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/google/android/location/NetworkLocationProvider;->client:Lcom/google/android/location/internal/client/NetworkLocationClient;

    monitor-exit v4

    return-void

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method static synthetic access$000(Lcom/google/android/location/NetworkLocationProvider;)V
    .locals 0
    .param p0    # Lcom/google/android/location/NetworkLocationProvider;

    invoke-direct {p0}, Lcom/google/android/location/NetworkLocationProvider;->handleEnable()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/location/NetworkLocationProvider;)V
    .locals 0
    .param p0    # Lcom/google/android/location/NetworkLocationProvider;

    invoke-direct {p0}, Lcom/google/android/location/NetworkLocationProvider;->handleDisable()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/location/NetworkLocationProvider;)V
    .locals 0
    .param p0    # Lcom/google/android/location/NetworkLocationProvider;

    invoke-direct {p0}, Lcom/google/android/location/NetworkLocationProvider;->handleSetMinTime()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/google/android/location/NetworkLocationProvider;
    .locals 2

    const-class v0, Lcom/google/android/location/NetworkLocationProvider;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/android/location/NetworkLocationProvider;->instance:Lcom/google/android/location/NetworkLocationProvider;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private handleDisable()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/NetworkLocationProvider;->setUserConfirmedPreference(Z)V

    return-void
.end method

.method private handleEnable()V
    .locals 7

    const/4 v2, 0x0

    const/4 v6, 0x0

    sget-object v0, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "(name=?)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "network_location_opt_in"

    aput-object v5, v4, v6

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "NetworkLocationProvider"

    const-string v1, "handleEnable: provider not available"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    sget-object v0, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network_location_opt_in"

    invoke-static {v0, v1, v6}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    const-class v2, Lcom/google/android/location/ConfirmAlertActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget-object v1, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private handleSetMinTime()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/location/NetworkLocationProvider;->lock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget v0, p0, Lcom/google/android/location/NetworkLocationProvider;->minTimeSeconds:I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/google/android/location/NetworkLocationProvider;->client:Lcom/google/android/location/internal/client/NetworkLocationClient;

    invoke-virtual {v1, v0}, Lcom/google/android/location/internal/client/NetworkLocationClient;->changePeriod(I)V

    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static declared-synchronized init(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/location/NetworkLocationProvider;

    monitor-enter v1

    :try_start_0
    sput-object p0, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    sget-object v0, Lcom/google/android/location/NetworkLocationProvider;->instance:Lcom/google/android/location/NetworkLocationProvider;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/location/NetworkLocationProvider;

    invoke-direct {v0}, Lcom/google/android/location/NetworkLocationProvider;-><init>()V

    sput-object v0, Lcom/google/android/location/NetworkLocationProvider;->instance:Lcom/google/android/location/NetworkLocationProvider;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private setUserConfirmedPreference(Z)V
    .locals 4
    .param p1    # Z

    sget-object v1, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "network_location_opt_in"

    if-eqz p1, :cond_1

    const-string v1, "1"

    :goto_0
    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    if-nez p1, :cond_0

    sget-object v1, Lcom/google/android/location/internal/NlpVersionInfo$NlpApk;->ANDROID:Lcom/google/android/location/internal/NlpVersionInfo$NlpApk;

    sget-object v2, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/google/android/location/internal/NlpVersionInfo;->getNlpVersionInfo(Lcom/google/android/location/internal/NlpVersionInfo$NlpApk;Landroid/content/Context;)Lcom/google/android/location/internal/NlpVersionInfo;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/location/internal/NlpVersionInfo;->intent:Landroid/content/Intent;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/location/internal/NlpVersionInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void

    :cond_1
    const-string v1, "0"

    goto :goto_0
.end method

.method private updateStatusLocked(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/google/android/location/NetworkLocationProvider;->status:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/location/NetworkLocationProvider;->status:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/NetworkLocationProvider;->statusUpdateTime:J

    :cond_0
    return-void
.end method


# virtual methods
.method public onAddListener(ILandroid/os/WorkSource;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/WorkSource;

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    return-void
.end method

.method public onDisable()V
    .locals 3

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v0, p0, Lcom/google/android/location/NetworkLocationProvider;->mProviderHandler:Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

    iget-object v1, p0, Lcom/google/android/location/NetworkLocationProvider;->mProviderHandler:Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public onEnable()V
    .locals 3

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v0, p0, Lcom/google/android/location/NetworkLocationProvider;->mProviderHandler:Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

    iget-object v1, p0, Lcom/google/android/location/NetworkLocationProvider;->mProviderHandler:Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public onEnableLocationTracking(Z)V
    .locals 3
    .param p1    # Z

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v1, p0, Lcom/google/android/location/NetworkLocationProvider;->lock:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/NetworkLocationProvider;->client:Lcom/google/android/location/internal/client/NetworkLocationClient;

    iget v2, p0, Lcom/google/android/location/NetworkLocationProvider;->minTimeSeconds:I

    invoke-virtual {v0, v2}, Lcom/google/android/location/internal/client/NetworkLocationClient;->changePeriod(I)V

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/NetworkLocationProvider;->client:Lcom/google/android/location/internal/client/NetworkLocationClient;

    const v2, 0x7fffffff

    invoke-virtual {v0, v2}, Lcom/google/android/location/internal/client/NetworkLocationClient;->changePeriod(I)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/NetworkLocationProvider;->updateStatusLocked(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onGetAccuracy()I
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    const/4 v0, 0x2

    return v0
.end method

.method public onGetInternalState()Ljava/lang/String;
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v0, p0, Lcom/google/android/location/NetworkLocationProvider;->client:Lcom/google/android/location/internal/client/NetworkLocationClient;

    invoke-virtual {v0}, Lcom/google/android/location/internal/client/NetworkLocationClient;->getDebugDump()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onGetPowerRequirement()I
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    const/4 v0, 0x1

    return v0
.end method

.method public onGetStatus(Landroid/os/Bundle;)I
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v1, p0, Lcom/google/android/location/NetworkLocationProvider;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/location/NetworkLocationProvider;->status:I

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onGetStatusUpdateTime()J
    .locals 4

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v1, p0, Lcom/google/android/location/NetworkLocationProvider;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, p0, Lcom/google/android/location/NetworkLocationProvider;->statusUpdateTime:J

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onHasMonetaryCost()Z
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    const/4 v0, 0x1

    return v0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .param p1    # Landroid/location/Location;

    iget-object v1, p0, Lcom/google/android/location/NetworkLocationProvider;->lock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x2

    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/location/NetworkLocationProvider;->updateStatusLocked(I)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, p1}, Lcom/google/android/location/NetworkLocationProvider;->reportLocation(Landroid/location/Location;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onMeetsCriteria(Landroid/location/Criteria;)Z
    .locals 3
    .param p1    # Landroid/location/Criteria;

    const/4 v0, 0x0

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    invoke-virtual {p1}, Landroid/location/Criteria;->getAccuracy()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/location/Criteria;->getAccuracy()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/location/Criteria;->isAltitudeRequired()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/location/Criteria;->isSpeedRequired()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/location/Criteria;->isBearingRequired()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onRemoveListener(ILandroid/os/WorkSource;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/WorkSource;

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    return-void
.end method

.method public onRequiresCell()Z
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    const/4 v0, 0x1

    return v0
.end method

.method public onRequiresNetwork()Z
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    const/4 v0, 0x1

    return v0
.end method

.method public onRequiresSatellite()Z
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    const/4 v0, 0x0

    return v0
.end method

.method public onSendExtraCommand(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/os/Bundle;

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    const/4 v0, 0x0

    return v0
.end method

.method public onSetMinTime(JLandroid/os/WorkSource;)V
    .locals 5
    .param p1    # J
    .param p3    # Landroid/os/WorkSource;

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    const-wide/16 v2, 0x3e8

    div-long/2addr p1, v2

    long-to-int v1, p1

    int-to-long v2, v1

    cmp-long v2, p1, v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "minTime is too big "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    iget-object v3, p0, Lcom/google/android/location/NetworkLocationProvider;->lock:Ljava/lang/Object;

    monitor-enter v3

    const/16 v2, 0x14

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/location/NetworkLocationProvider;->minTimeSeconds:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lcom/google/android/location/NetworkLocationProvider;->mProviderHandler:Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/location/NetworkLocationProvider;->mProviderHandler:Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

    invoke-virtual {v2, v0}, Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    return-void
.end method

.method public onSupportsAltitude()Z
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    const/4 v0, 0x0

    return v0
.end method

.method public onSupportsBearing()Z
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    const/4 v0, 0x0

    return v0
.end method

.method public onSupportsSpeed()Z
    .locals 1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    const/4 v0, 0x0

    return v0
.end method

.method public onUpdateLocation(Landroid/location/Location;)V
    .locals 0
    .param p1    # Landroid/location/Location;

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    return-void
.end method

.method public onUpdateNetworkState(ILandroid/net/NetworkInfo;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/net/NetworkInfo;

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v1, p0, Lcom/google/android/location/NetworkLocationProvider;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput p1, p0, Lcom/google/android/location/NetworkLocationProvider;->networkState:I

    iget v0, p0, Lcom/google/android/location/NetworkLocationProvider;->networkState:I

    invoke-direct {p0, v0}, Lcom/google/android/location/NetworkLocationProvider;->updateStatusLocked(I)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 2

    const/4 v0, -0x2

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;-><init>(Lcom/google/android/location/NetworkLocationProvider;Lcom/google/android/location/NetworkLocationProvider$1;)V

    iput-object v0, p0, Lcom/google/android/location/NetworkLocationProvider;->mProviderHandler:Lcom/google/android/location/NetworkLocationProvider$ProviderHandler;

    iget-object v1, p0, Lcom/google/android/location/NetworkLocationProvider;->mThread:Ljava/lang/Thread;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/NetworkLocationProvider;->looper:Landroid/os/Looper;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/NetworkLocationProvider;->mStarted:Z

    iget-object v0, p0, Lcom/google/android/location/NetworkLocationProvider;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method userConfirmedEnable(Z)V
    .locals 2
    .param p1    # Z

    sget-object v0, Lcom/google/android/location/NetworkLocationProvider;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/NetworkLocationProvider;->setUserConfirmedPreference(Z)V

    :cond_0
    return-void
.end method
