.class public Lcom/google/android/location/NetworkLocator;
.super Ljava/lang/Object;
.source "NetworkLocator.java"


# static fields
.field private static final EMPTY_CELL_STATUS:Lcom/google/android/location/data/CellStatus;


# instance fields
.field cellEnabled:Z

.field cellStatus:Lcom/google/android/location/data/CellStatus;

.field glsQuery:Lcom/google/gmm/common/io/protocol/ProtoBuf;

.field glsQueryTicket:Z

.field glsQueryTime:J

.field glsQueryWifiScan:Lcom/google/android/location/data/WifiScan;

.field lastCachePurge:J

.field lastCellScanRequestTime:J

.field private lastGps:Lcom/google/android/location/os/LocationInterface;

.field lastNetworkLocation:Lcom/google/android/location/data/NetworkLocation;

.field lastStateCheckpointTime:J

.field lastWakeLockAcquireTime:J

.field lastWakeLockReleaseTime:J

.field lastWifiScanRequestTime:J

.field final locator:Lcom/google/android/location/localizer/LocatorManager;

.field locatorTicket:Z

.field modelStateManager:Lcom/google/android/location/localizer/ModelStateManager;

.field nextTriggerTime:J

.field final os:Lcom/google/android/location/os/Os;

.field period:J

.field final seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

.field final state:Lcom/google/android/location/cache/PersistentState;

.field final stats:Lcom/google/android/location/Stats;

.field final tracker:Lcom/google/android/location/data/CellScreenTracker;

.field final travelDetectionManager:Lcom/google/android/location/activity/TravelDetectionManager;

.field wifiEnabled:Z

.field wifiScan:Lcom/google/android/location/data/WifiScan;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/location/data/CellStatus;

    invoke-direct {v0}, Lcom/google/android/location/data/CellStatus;-><init>()V

    sput-object v0, Lcom/google/android/location/NetworkLocator;->EMPTY_CELL_STATUS:Lcom/google/android/location/data/CellStatus;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/cache/PersistentState;Lcom/google/android/location/cache/SeenDevicesCache;Lcom/google/android/location/localizer/ModelStateManager;Lcom/google/android/location/Stats;Lcom/google/android/location/data/CellScreenTracker;)V
    .locals 8
    .param p1    # Lcom/google/android/location/os/Os;
    .param p2    # Lcom/google/android/location/cache/PersistentState;
    .param p3    # Lcom/google/android/location/cache/SeenDevicesCache;
    .param p4    # Lcom/google/android/location/localizer/ModelStateManager;
    .param p5    # Lcom/google/android/location/Stats;
    .param p6    # Lcom/google/android/location/data/CellScreenTracker;

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const-wide/16 v2, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide v0, 0x1f3fffffc18L

    iput-wide v0, p0, Lcom/google/android/location/NetworkLocator;->period:J

    iput-wide v6, p0, Lcom/google/android/location/NetworkLocator;->nextTriggerTime:J

    new-instance v0, Lcom/google/android/location/data/NetworkLocation;

    invoke-direct {v0, v5, v5, v5, v5}, Lcom/google/android/location/data/NetworkLocation;-><init>(Lcom/google/android/location/data/LocatorResult;Lcom/google/android/location/data/WifiLocatorResult;Lcom/google/android/location/data/CellLocatorResult;Lcom/google/android/location/data/GlsLocatorResult;)V

    iput-object v0, p0, Lcom/google/android/location/NetworkLocator;->lastNetworkLocation:Lcom/google/android/location/data/NetworkLocation;

    iput-boolean v4, p0, Lcom/google/android/location/NetworkLocator;->locatorTicket:Z

    iput-wide v2, p0, Lcom/google/android/location/NetworkLocator;->glsQueryTime:J

    iput-boolean v4, p0, Lcom/google/android/location/NetworkLocator;->glsQueryTicket:Z

    iput-wide v2, p0, Lcom/google/android/location/NetworkLocator;->lastWakeLockAcquireTime:J

    iput-wide v2, p0, Lcom/google/android/location/NetworkLocator;->lastWakeLockReleaseTime:J

    new-instance v0, Lcom/google/android/location/data/CellStatus;

    invoke-direct {v0}, Lcom/google/android/location/data/CellStatus;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/NetworkLocator;->cellStatus:Lcom/google/android/location/data/CellStatus;

    iput-wide v2, p0, Lcom/google/android/location/NetworkLocator;->lastCellScanRequestTime:J

    iput-boolean v4, p0, Lcom/google/android/location/NetworkLocator;->cellEnabled:Z

    iput-boolean v4, p0, Lcom/google/android/location/NetworkLocator;->wifiEnabled:Z

    iput-wide v2, p0, Lcom/google/android/location/NetworkLocator;->lastWifiScanRequestTime:J

    iput-wide v6, p0, Lcom/google/android/location/NetworkLocator;->lastStateCheckpointTime:J

    iput-wide v6, p0, Lcom/google/android/location/NetworkLocator;->lastCachePurge:J

    iput-object p1, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    iput-object p2, p0, Lcom/google/android/location/NetworkLocator;->state:Lcom/google/android/location/cache/PersistentState;

    iput-object p3, p0, Lcom/google/android/location/NetworkLocator;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    iput-object p4, p0, Lcom/google/android/location/NetworkLocator;->modelStateManager:Lcom/google/android/location/localizer/ModelStateManager;

    new-instance v0, Lcom/google/android/location/localizer/LocatorManager;

    iget-object v1, p2, Lcom/google/android/location/cache/PersistentState;->cellCache:Lcom/google/android/location/cache/TemporalCache;

    iget-object v2, p2, Lcom/google/android/location/cache/PersistentState;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-direct {v0, v1, v2, p4, p1}, Lcom/google/android/location/localizer/LocatorManager;-><init>(Lcom/google/android/location/cache/TemporalCache;Lcom/google/android/location/cache/TemporalCache;Lcom/google/android/location/localizer/ModelStateManager;Lcom/google/android/location/os/Os;)V

    iput-object v0, p0, Lcom/google/android/location/NetworkLocator;->locator:Lcom/google/android/location/localizer/LocatorManager;

    iput-object p5, p0, Lcom/google/android/location/NetworkLocator;->stats:Lcom/google/android/location/Stats;

    iput-object p6, p0, Lcom/google/android/location/NetworkLocator;->tracker:Lcom/google/android/location/data/CellScreenTracker;

    new-instance v0, Lcom/google/android/location/activity/TravelDetectionManager;

    iget-object v1, p2, Lcom/google/android/location/cache/PersistentState;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-direct {v0, v1, p1}, Lcom/google/android/location/activity/TravelDetectionManager;-><init>(Lcom/google/android/location/cache/TemporalCache;Lcom/google/android/location/os/Clock;)V

    iput-object v0, p0, Lcom/google/android/location/NetworkLocator;->travelDetectionManager:Lcom/google/android/location/activity/TravelDetectionManager;

    return-void
.end method

.method private acquireWakeLock()J
    .locals 4

    iget-object v2, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/google/android/location/os/Os;->wakeLockAcquire(I)V

    iget-object v2, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v2}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/location/NetworkLocator;->lastWakeLockReleaseTime:J

    iput-wide v0, p0, Lcom/google/android/location/NetworkLocator;->lastWakeLockAcquireTime:J

    return-wide v0
.end method

.method static computeAperture(JLcom/google/android/location/os/LocationInterface;Lcom/google/android/location/os/LocationInterface;)Lcom/google/android/location/data/Position;
    .locals 15
    .param p0    # J
    .param p2    # Lcom/google/android/location/os/LocationInterface;
    .param p3    # Lcom/google/android/location/os/LocationInterface;

    const-wide/32 v7, 0x15f90

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    invoke-interface/range {p2 .. p2}, Lcom/google/android/location/os/LocationInterface;->getTimeSinceBoot()J

    move-result-wide v9

    sub-long v9, p0, v9

    const-wide/32 v11, 0x15f90

    cmp-long v9, v9, v11

    if-gez v9, :cond_0

    move-object/from16 v1, p2

    :cond_0
    if-eqz p3, :cond_1

    invoke-interface/range {p3 .. p3}, Lcom/google/android/location/os/LocationInterface;->getTimeSinceBoot()J

    move-result-wide v9

    sub-long v4, p0, v9

    const-wide/32 v9, 0x15f90

    cmp-long v9, v4, v9

    if-gez v9, :cond_1

    invoke-interface/range {p3 .. p3}, Lcom/google/android/location/os/LocationInterface;->getAccuracy()F

    move-result v3

    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcom/google/android/location/os/LocationInterface;->getAccuracy()F

    move-result v2

    float-to-double v9, v3

    float-to-double v11, v2

    const-wide v13, 0x3feccccccccccccdL

    mul-double/2addr v11, v13

    cmpg-double v9, v9, v11

    if-gez v9, :cond_2

    const-string v9, "NetworkLocator"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Preferring non-GPS reading ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ") vs ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v1, p3

    :cond_1
    :goto_0
    if-nez v1, :cond_4

    const/4 v6, 0x0

    :goto_1
    return-object v6

    :cond_2
    const-string v9, "NetworkLocator"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Preferring GPS reading ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ") vs ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v9, "NetworkLocator"

    const-string v10, "No GPS position, using recently-computed position"

    invoke-static {v9, v10}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v1, p3

    goto :goto_0

    :cond_4
    invoke-interface {v1}, Lcom/google/android/location/os/LocationInterface;->getAccuracy()F

    move-result v0

    float-to-double v9, v0

    const-wide v11, 0x408f400000000000L

    cmpg-double v9, v9, v11

    if-gez v9, :cond_5

    const/high16 v0, 0x457a0000

    :goto_2
    new-instance v6, Lcom/google/android/location/data/Position;

    invoke-interface {v1}, Lcom/google/android/location/os/LocationInterface;->getLat()D

    move-result-wide v9

    const-wide v11, 0x416312d000000000L

    mul-double/2addr v9, v11

    double-to-int v9, v9

    invoke-interface {v1}, Lcom/google/android/location/os/LocationInterface;->getLng()D

    move-result-wide v10

    const-wide v12, 0x416312d000000000L

    mul-double/2addr v10, v12

    double-to-int v10, v10

    const/high16 v11, 0x447a0000

    mul-float/2addr v11, v0

    float-to-int v11, v11

    invoke-direct {v6, v9, v10, v11}, Lcom/google/android/location/data/Position;-><init>(III)V

    const-string v9, "NetworkLocator"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Returning aperture "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    float-to-double v9, v0

    const-wide v11, 0x40c3880000000000L

    cmpg-double v9, v9, v11

    if-gez v9, :cond_6

    const v0, 0x47435000

    goto :goto_2

    :cond_6
    const v0, 0x47c35000

    goto :goto_2
.end method

.method private createGlsQueryRequest(Lcom/google/android/location/data/CellStatus;Lcom/google/android/location/data/WifiScan;)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 9
    .param p1    # Lcom/google/android/location/data/CellStatus;
    .param p2    # Lcom/google/android/location/data/WifiScan;

    const/4 v8, 0x2

    const/4 v7, 0x1

    new-instance v4, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v6, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST_ELEMENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    iget-object v6, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v6}, Lcom/google/android/location/os/Os;->bootTime()J

    move-result-wide v0

    if-eqz p1, :cond_0

    invoke-virtual {p1, v4, v0, v1, v7}, Lcom/google/android/location/data/CellStatus;->addToRequestElement(Lcom/google/gmm/common/io/protocol/ProtoBuf;JZ)V

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2, v0, v1, v7}, Lcom/google/android/location/data/WifiScan;->createWifiProfile(JZ)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v5, v6, v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-virtual {v4, v8, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    new-instance v2, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v6, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSIGNAL_FINGERPRINT_MODEL_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v2, v7, v7}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/16 v6, 0xc

    invoke-virtual {v4, v6, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    :cond_1
    const/16 v6, 0xa

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    new-instance v3, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v6, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/4 v6, 0x4

    invoke-virtual {v3, v6, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    iget-object v6, p0, Lcom/google/android/location/NetworkLocator;->stats:Lcom/google/android/location/Stats;

    iget-object v7, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-virtual {v6, v7, v3}, Lcom/google/android/location/Stats;->maybeAddClientStats(Lcom/google/android/location/os/Os;Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    return-object v3
.end method

.method private releaseWakeLock(J)V
    .locals 4
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/location/NetworkLocator;->lastWakeLockReleaseTime:J

    iget-wide v0, p0, Lcom/google/android/location/NetworkLocator;->lastWakeLockAcquireTime:J

    const-wide/16 v2, 0x1

    sub-long v2, p1, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/NetworkLocator;->lastWakeLockAcquireTime:J

    iget-object v0, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/location/os/Os;->wakeLockRelease(I)V

    return-void
.end method

.method private setCellWifiEnabled(JZZ)V
    .locals 9
    .param p1    # J
    .param p3    # Z
    .param p4    # Z

    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v5, 0x0

    iget-boolean v1, p0, Lcom/google/android/location/NetworkLocator;->cellEnabled:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/location/NetworkLocator;->wifiEnabled:Z

    if-eqz v1, :cond_3

    :cond_0
    move v8, v0

    :goto_0
    iput-boolean p3, p0, Lcom/google/android/location/NetworkLocator;->cellEnabled:Z

    iput-boolean p4, p0, Lcom/google/android/location/NetworkLocator;->wifiEnabled:Z

    iget-boolean v1, p0, Lcom/google/android/location/NetworkLocator;->cellEnabled:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/location/NetworkLocator;->wifiEnabled:Z

    if-eqz v1, :cond_4

    :cond_1
    move v7, v0

    :goto_1
    if-eq v8, v7, :cond_2

    if-eqz v7, :cond_5

    const-wide/16 v0, 0x1

    sub-long v0, p1, v0

    iput-wide v0, p0, Lcom/google/android/location/NetworkLocator;->nextTriggerTime:J

    :cond_2
    :goto_2
    move-object v0, p0

    move-wide v1, p1

    move-object v4, v3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/NetworkLocator;->updateState(JLcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/GlsLocatorResult;ZZ)V

    return-void

    :cond_3
    move v8, v5

    goto :goto_0

    :cond_4
    move v7, v5

    goto :goto_1

    :cond_5
    const-wide/32 v0, 0x7fffffff

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/location/NetworkLocator;->nextTriggerTime:J

    goto :goto_2
.end method

.method private shouldReportLocation(Lcom/google/android/location/data/LocatorResult;J)Z
    .locals 9
    .param p1    # Lcom/google/android/location/data/LocatorResult;
    .param p2    # J

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v3}, Lcom/google/android/location/os/Os;->getLastKnownLocation()Lcom/google/android/location/os/LocationInterface;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v3, p1, Lcom/google/android/location/data/LocatorResult;->position:Lcom/google/android/location/data/Position;

    iget v3, v3, Lcom/google/android/location/data/Position;->accuracyMm:I

    const v4, 0x30d40

    if-ge v3, v4, :cond_1

    const-string v2, "NetworkLocator"

    const-string v3, "Reporting available location since we\'ve never reported one before."

    invoke-static {v2, v3}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v1

    :cond_1
    const-string v1, "NetworkLocator"

    const-string v3, "Not reporting first fix as it may be significantly improved by going to the server."

    invoke-static {v1, v3}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    goto :goto_0

    :cond_2
    iget-wide v3, p1, Lcom/google/android/location/data/LocatorResult;->reportTime:J

    invoke-interface {v0}, Lcom/google/android/location/os/LocationInterface;->getTimeSinceBoot()J

    move-result-wide v5

    sub-long/2addr v3, v5

    iget-wide v5, p0, Lcom/google/android/location/NetworkLocator;->period:J

    const-wide/32 v7, 0xdbba0

    add-long/2addr v5, v7

    cmp-long v3, v3, v5

    if-ltz v3, :cond_3

    const-string v2, "NetworkLocator"

    const-string v3, "New location significantly newer than previous. Reporting it."

    invoke-static {v2, v3}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v3, p1, Lcom/google/android/location/data/LocatorResult;->position:Lcom/google/android/location/data/Position;

    iget v3, v3, Lcom/google/android/location/data/Position;->accuracyMm:I

    int-to-float v3, v3

    invoke-interface {v0}, Lcom/google/android/location/os/LocationInterface;->getAccuracy()F

    move-result v4

    const/high16 v5, 0x447a0000

    mul-float/2addr v4, v5

    const v5, 0x47c35000

    add-float/2addr v4, v5

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    const-string v1, "NetworkLocator"

    const-string v3, "Not reporting location since the new location has worse accuracy than the previous one."

    invoke-static {v1, v3}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    goto :goto_0
.end method

.method private updateState(JLcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/GlsLocatorResult;ZZ)V
    .locals 55
    .param p1    # J
    .param p3    # Lcom/google/android/location/data/WifiScan;
    .param p4    # Lcom/google/android/location/data/GlsLocatorResult;
    .param p5    # Z
    .param p6    # Z

    if-eqz p3, :cond_0

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/NetworkLocator;->wifiScan:Lcom/google/android/location/data/WifiScan;

    :cond_0
    if-eqz p5, :cond_1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/location/NetworkLocator;->glsQuery:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    :cond_1
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/NetworkLocator;->lastCachePurge:J

    sub-long v3, p1, v3

    const-wide/32 v5, 0x36ee80

    cmp-long v3, v3, v5

    if-lez v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->state:Lcom/google/android/location/cache/PersistentState;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v4}, Lcom/google/android/location/os/Os;->millisSinceEpoch()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/location/cache/PersistentState;->discardOldCacheEntries(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->modelStateManager:Lcom/google/android/location/localizer/ModelStateManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v4}, Lcom/google/android/location/os/Os;->millisSinceEpoch()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/location/localizer/ModelStateManager;->discardOldCacheEntries(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    invoke-virtual {v3}, Lcom/google/android/location/cache/SeenDevicesCache;->discardOldEntries()V

    :cond_2
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/NetworkLocator;->lastCachePurge:J

    :cond_3
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/NetworkLocator;->lastWakeLockAcquireTime:J

    sub-long v42, p1, v3

    const-wide/32 v27, 0xafc8

    const-wide/16 v12, 0xc8

    const-wide/32 v29, 0xafc8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->wifiScan:Lcom/google/android/location/data/WifiScan;

    if-nez v3, :cond_28

    const-wide/16 v52, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->cellStatus:Lcom/google/android/location/data/CellStatus;

    invoke-virtual {v3}, Lcom/google/android/location/data/CellStatus;->getPrimary()Lcom/google/android/location/data/CellState;

    move-result-object v10

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/NetworkLocator;->nextTriggerTime:J

    cmp-long v3, p1, v3

    if-ltz v3, :cond_29

    const/16 v47, 0x1

    :goto_1
    if-eqz v10, :cond_2a

    invoke-virtual {v10}, Lcom/google/android/location/data/CellState;->isValid()Z

    move-result v3

    if-eqz v3, :cond_2a

    invoke-virtual {v10}, Lcom/google/android/location/data/CellState;->getTime()J

    move-result-wide v3

    sub-long v3, p1, v3

    const-wide/32 v5, 0xafc8

    cmp-long v3, v3, v5

    if-gez v3, :cond_2a

    const/16 v21, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->wifiScan:Lcom/google/android/location/data/WifiScan;

    if-eqz v3, :cond_2b

    const-wide/32 v3, 0xafc8

    cmp-long v3, v52, v3

    if-gez v3, :cond_2b

    const/16 v23, 0x1

    :goto_3
    if-eqz v21, :cond_2c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->tracker:Lcom/google/android/location/data/CellScreenTracker;

    invoke-virtual {v3}, Lcom/google/android/location/data/CellScreenTracker;->isOk()Z

    move-result v3

    if-eqz v3, :cond_2c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->lastNetworkLocation:Lcom/google/android/location/data/NetworkLocation;

    invoke-virtual {v3}, Lcom/google/android/location/data/NetworkLocation;->getCellResultPrimary()Lcom/google/android/location/data/CellState;

    move-result-object v3

    if-eq v10, v3, :cond_2c

    const/4 v14, 0x1

    :goto_4
    if-eqz v23, :cond_2d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->wifiScan:Lcom/google/android/location/data/WifiScan;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/NetworkLocator;->lastNetworkLocation:Lcom/google/android/location/data/NetworkLocation;

    invoke-virtual {v4}, Lcom/google/android/location/data/NetworkLocation;->getWifiResultScan()Lcom/google/android/location/data/WifiScan;

    move-result-object v4

    if-eq v3, v4, :cond_2d

    const/4 v15, 0x1

    :goto_5
    if-eqz v47, :cond_2e

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/location/NetworkLocator;->cellEnabled:Z

    if-eqz v3, :cond_2e

    const/16 v38, 0x1

    :goto_6
    if-eqz v47, :cond_2f

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/location/NetworkLocator;->wifiEnabled:Z

    if-eqz v3, :cond_2f

    if-eqz v15, :cond_4

    const-wide/32 v3, 0xafc8

    sub-long v3, v3, v52

    const-wide/16 v5, 0xc8

    cmp-long v3, v3, v5

    if-gez v3, :cond_2f

    :cond_4
    const/16 v39, 0x1

    :goto_7
    if-nez v38, :cond_5

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/NetworkLocator;->lastCellScanRequestTime:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-eqz v3, :cond_30

    if-eqz v10, :cond_5

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/NetworkLocator;->lastCellScanRequestTime:J

    invoke-virtual {v10}, Lcom/google/android/location/data/CellState;->getTime()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-lez v3, :cond_30

    :cond_5
    const/4 v11, 0x1

    :goto_8
    if-nez v39, :cond_6

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/NetworkLocator;->lastWifiScanRequestTime:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-eqz v3, :cond_31

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/location/NetworkLocator;->wifiEnabled:Z

    if-eqz v3, :cond_31

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->wifiScan:Lcom/google/android/location/data/WifiScan;

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/NetworkLocator;->lastWifiScanRequestTime:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/NetworkLocator;->wifiScan:Lcom/google/android/location/data/WifiScan;

    iget-wide v5, v5, Lcom/google/android/location/data/WifiScan;->deliveryTime:J

    cmp-long v3, v3, v5

    if-lez v3, :cond_31

    :cond_6
    const/16 v54, 0x1

    :goto_9
    if-eqz v11, :cond_32

    if-nez v38, :cond_7

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/NetworkLocator;->lastCellScanRequestTime:J

    sub-long v3, p1, v3

    const-wide/16 v5, 0x1388

    cmp-long v3, v3, v5

    if-gez v3, :cond_32

    :cond_7
    const/16 v48, 0x1

    :goto_a
    if-eqz v54, :cond_33

    if-nez v39, :cond_8

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/NetworkLocator;->lastWifiScanRequestTime:J

    sub-long v3, p1, v3

    const-wide/16 v5, 0x1388

    cmp-long v3, v3, v5

    if-gez v3, :cond_33

    :cond_8
    const/16 v50, 0x1

    :goto_b
    if-nez v48, :cond_9

    if-eqz v50, :cond_34

    :cond_9
    const/16 v49, 0x1

    :goto_c
    if-nez v14, :cond_a

    if-eqz v15, :cond_b

    :cond_a
    if-eqz v49, :cond_c

    :cond_b
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/location/NetworkLocator;->wifiEnabled:Z

    if-nez v3, :cond_35

    if-eqz v21, :cond_35

    if-nez v48, :cond_35

    :cond_c
    const/16 v41, 0x1

    :goto_d
    if-nez p5, :cond_e

    if-nez p6, :cond_e

    if-nez v47, :cond_d

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/location/NetworkLocator;->locatorTicket:Z

    if-eqz v3, :cond_36

    :cond_d
    if-eqz v41, :cond_36

    :cond_e
    const/16 v40, 0x1

    :goto_e
    const/16 v31, 0x0

    if-eqz v40, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->lastGps:Lcom/google/android/location/os/LocationInterface;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v4}, Lcom/google/android/location/os/Os;->getLastKnownLocation()Lcom/google/android/location/os/LocationInterface;

    move-result-object v4

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v3, v4}, Lcom/google/android/location/NetworkLocator;->computeAperture(JLcom/google/android/location/os/LocationInterface;Lcom/google/android/location/os/LocationInterface;)Lcom/google/android/location/data/Position;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->locator:Lcom/google/android/location/localizer/LocatorManager;

    if-eqz v14, :cond_37

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/NetworkLocator;->cellStatus:Lcom/google/android/location/data/CellStatus;

    invoke-virtual {v4}, Lcom/google/android/location/data/CellStatus;->copy()Lcom/google/android/location/data/CellStatus;

    move-result-object v4

    :goto_f
    if-eqz v23, :cond_38

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/NetworkLocator;->wifiScan:Lcom/google/android/location/data/WifiScan;

    :goto_10
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/location/NetworkLocator;->period:J

    move-object/from16 v6, p4

    invoke-virtual/range {v3 .. v9}, Lcom/google/android/location/localizer/LocatorManager;->computeLocation(Lcom/google/android/location/data/CellStatus;Lcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/GlsLocatorResult;Lcom/google/android/location/data/Position;J)Lcom/google/android/location/data/NetworkLocation;

    move-result-object v31

    :cond_f
    if-eqz v31, :cond_39

    move-object/from16 v0, v31

    iget-object v3, v0, Lcom/google/android/location/data/NetworkLocation;->bestResult:Lcom/google/android/location/data/LocatorResult;

    if-eqz v3, :cond_39

    const/16 v22, 0x1

    :goto_11
    if-eqz v31, :cond_3a

    move-object/from16 v0, v31

    iget-object v3, v0, Lcom/google/android/location/data/NetworkLocation;->cellResult:Lcom/google/android/location/data/CellLocatorResult;

    iget-object v3, v3, Lcom/google/android/location/data/CellLocatorResult;->status:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    sget-object v4, Lcom/google/android/location/data/LocatorResult$ResultStatus;->CACHE_MISS:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    if-ne v3, v4, :cond_3a

    const/16 v16, 0x1

    :goto_12
    if-eqz v31, :cond_3b

    move-object/from16 v0, v31

    iget-object v3, v0, Lcom/google/android/location/data/NetworkLocation;->wifiResult:Lcom/google/android/location/data/WifiLocatorResult;

    iget-object v3, v3, Lcom/google/android/location/data/WifiLocatorResult;->status:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    sget-object v4, Lcom/google/android/location/data/LocatorResult$ResultStatus;->CACHE_MISS:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    if-ne v3, v4, :cond_3b

    const/16 v17, 0x1

    :goto_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->glsQuery:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    if-nez v3, :cond_3c

    if-nez v47, :cond_10

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/location/NetworkLocator;->glsQueryTicket:Z

    if-eqz v3, :cond_3c

    :cond_10
    if-eqz v16, :cond_11

    if-nez v14, :cond_12

    :cond_11
    if-eqz v17, :cond_3c

    if-eqz v15, :cond_3c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->wifiScan:Lcom/google/android/location/data/WifiScan;

    invoke-virtual {v3}, Lcom/google/android/location/data/WifiScan;->numDevices()I

    move-result v3

    if-lez v3, :cond_3c

    :cond_12
    const/16 v34, 0x1

    :goto_14
    if-nez v34, :cond_13

    if-eqz v22, :cond_3d

    :cond_13
    const/16 v26, 0x1

    :goto_15
    if-eqz v22, :cond_3e

    if-eqz v34, :cond_14

    move-object/from16 v0, v31

    iget-object v3, v0, Lcom/google/android/location/data/NetworkLocation;->bestResult:Lcom/google/android/location/data/LocatorResult;

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/location/NetworkLocator;->shouldReportLocation(Lcom/google/android/location/data/LocatorResult;J)Z

    move-result v3

    if-eqz v3, :cond_3e

    :cond_14
    const/16 v37, 0x1

    :goto_16
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/location/NetworkLocator;->isWakeLocked()Z

    move-result v24

    if-nez v24, :cond_3f

    if-eqz v47, :cond_3f

    const/16 v20, 0x1

    :goto_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->glsQuery:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    if-eqz v3, :cond_15

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/NetworkLocator;->glsQueryTime:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/location/NetworkLocator;->lastWakeLockAcquireTime:J

    cmp-long v3, v3, v5

    if-gez v3, :cond_16

    :cond_15
    if-eqz v34, :cond_40

    :cond_16
    const/16 v18, 0x1

    :goto_18
    if-nez v11, :cond_17

    if-nez v54, :cond_17

    if-eqz v18, :cond_41

    :cond_17
    const/16 v51, 0x1

    :goto_19
    if-eqz v24, :cond_42

    if-nez v47, :cond_42

    const-wide/16 v3, 0x1388

    cmp-long v3, v42, v3

    if-gez v3, :cond_18

    if-nez v51, :cond_42

    :cond_18
    const/16 v35, 0x1

    :goto_1a
    if-eqz v24, :cond_43

    if-eqz v47, :cond_43

    if-nez v35, :cond_43

    const/16 v36, 0x1

    :goto_1b
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/location/NetworkLocator;->locatorTicket:Z

    if-nez v3, :cond_19

    if-eqz v47, :cond_44

    :cond_19
    if-nez v40, :cond_44

    const/4 v3, 0x1

    :goto_1c
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/location/NetworkLocator;->locatorTicket:Z

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/location/NetworkLocator;->glsQueryTicket:Z

    if-nez v3, :cond_1a

    if-eqz v47, :cond_45

    :cond_1a
    if-nez v26, :cond_45

    const/4 v3, 0x1

    :goto_1d
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/location/NetworkLocator;->glsQueryTicket:Z

    if-eqz v20, :cond_1b

    invoke-direct/range {p0 .. p0}, Lcom/google/android/location/NetworkLocator;->acquireWakeLock()J

    move-result-wide p1

    :cond_1b
    if-eqz v36, :cond_1c

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/NetworkLocator;->lastWakeLockAcquireTime:J

    :cond_1c
    if-eqz v47, :cond_1d

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/NetworkLocator;->period:J

    add-long v3, v3, p1

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/google/android/location/NetworkLocator;->nextTriggerTime:J

    :cond_1d
    if-eqz v38, :cond_1e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v3}, Lcom/google/android/location/os/Os;->cellRequestScan()V

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/NetworkLocator;->lastCellScanRequestTime:J

    :cond_1e
    if-eqz v39, :cond_1f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v3}, Lcom/google/android/location/os/Os;->wifiRequestScan()V

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/NetworkLocator;->lastWifiScanRequestTime:J

    :cond_1f
    if-eqz v34, :cond_20

    if-eqz v14, :cond_46

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/NetworkLocator;->cellStatus:Lcom/google/android/location/data/CellStatus;

    move-object/from16 v25, v0

    :goto_1e
    if-eqz v15, :cond_47

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->wifiScan:Lcom/google/android/location/data/WifiScan;

    invoke-virtual {v3}, Lcom/google/android/location/data/WifiScan;->numDevices()I

    move-result v3

    if-lez v3, :cond_47

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/NetworkLocator;->wifiScan:Lcom/google/android/location/data/WifiScan;

    move-object/from16 v19, v0

    :goto_1f
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/NetworkLocator;->createGlsQueryRequest(Lcom/google/android/location/data/CellStatus;Lcom/google/android/location/data/WifiScan;)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/location/NetworkLocator;->glsQuery:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/NetworkLocator;->glsQueryTime:J

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/NetworkLocator;->glsQueryWifiScan:Lcom/google/android/location/data/WifiScan;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/NetworkLocator;->glsQuery:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-static/range {v25 .. v25}, Lcom/google/android/location/data/CellStatus;->createCellularPlatformProfile(Lcom/google/android/location/data/CellStatus;)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/google/android/location/os/Os;->glsQuery(Lcom/google/gmm/common/io/protocol/ProtoBuf;Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    :cond_20
    if-eqz v37, :cond_21

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/NetworkLocator;->travelDetectionManager:Lcom/google/android/location/activity/TravelDetectionManager;

    if-eqz v23, :cond_48

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->wifiScan:Lcom/google/android/location/data/WifiScan;

    :goto_20
    move-object/from16 v0, v31

    invoke-virtual {v4, v0, v3}, Lcom/google/android/location/activity/TravelDetectionManager;->detectTravelType(Lcom/google/android/location/data/NetworkLocation;Lcom/google/android/location/data/WifiScan;)Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    move-result-object v46

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    move-object/from16 v0, v46

    invoke-interface {v3, v0}, Lcom/google/android/location/os/Os;->onTravelModeDetected(Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/NetworkLocator;->lastNetworkLocation:Lcom/google/android/location/data/NetworkLocation;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->state:Lcom/google/android/location/cache/PersistentState;

    move-object/from16 v0, v31

    iget-object v4, v0, Lcom/google/android/location/data/NetworkLocation;->bestResult:Lcom/google/android/location/data/LocatorResult;

    iput-object v4, v3, Lcom/google/android/location/cache/PersistentState;->lastKnown:Lcom/google/android/location/data/LocatorResult;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/NetworkLocator;->lastNetworkLocation:Lcom/google/android/location/data/NetworkLocation;

    if-nez v46, :cond_49

    const/4 v3, 0x0

    :goto_21
    invoke-interface {v4, v5, v3}, Lcom/google/android/location/os/Os;->locationReport(Lcom/google/android/location/data/NetworkLocation;Lcom/google/android/location/data/TravelDetectionType;)V

    :cond_21
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/location/NetworkLocator;->nextTriggerTime:J

    move-wide/from16 v32, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/location/NetworkLocator;->isWakeLocked()Z

    move-result v3

    if-eqz v3, :cond_22

    if-nez v35, :cond_22

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/NetworkLocator;->lastWakeLockAcquireTime:J

    const-wide/16 v5, 0x1388

    add-long/2addr v3, v5

    move-wide/from16 v0, v32

    invoke-static {v0, v1, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v32

    :cond_22
    const-wide v3, 0x7fffffffffffffffL

    cmp-long v3, v32, v3

    if-gez v3, :cond_23

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    const/4 v4, 0x0

    move-wide/from16 v0, v32

    invoke-interface {v3, v4, v0, v1}, Lcom/google/android/location/os/Os;->alarmReset(IJ)V

    :cond_23
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/location/NetworkLocator;->lastStateCheckpointTime:J

    sub-long v44, p1, v3

    const-wide/32 v3, 0x5265c0

    cmp-long v3, v44, v3

    if-gtz v3, :cond_24

    const-wide/32 v3, 0x2932e0

    cmp-long v3, v44, v3

    if-lez v3, :cond_26

    sub-long v3, v32, p1

    const-wide/16 v5, 0x2710

    cmp-long v3, v3, v5

    if-lez v3, :cond_26

    if-nez v11, :cond_26

    if-nez v54, :cond_26

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->glsQuery:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    if-nez v3, :cond_26

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/location/NetworkLocator;->isWakeLocked()Z

    move-result v3

    if-eqz v3, :cond_26

    :cond_24
    const-string v3, "NetworkLocator"

    const-string v4, "NlpState checkpointing..."

    invoke-static {v3, v4}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->state:Lcom/google/android/location/cache/PersistentState;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-virtual {v3, v4}, Lcom/google/android/location/cache/PersistentState;->save(Lcom/google/android/location/os/Os;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->modelStateManager:Lcom/google/android/location/localizer/ModelStateManager;

    invoke-virtual {v3}, Lcom/google/android/location/localizer/ModelStateManager;->saveState()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    if-eqz v3, :cond_25

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    invoke-virtual {v3}, Lcom/google/android/location/cache/SeenDevicesCache;->save()V

    :cond_25
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/NetworkLocator;->lastStateCheckpointTime:J

    :cond_26
    if-eqz v35, :cond_27

    invoke-direct/range {p0 .. p2}, Lcom/google/android/location/NetworkLocator;->releaseWakeLock(J)V

    :cond_27
    return-void

    :cond_28
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/NetworkLocator;->wifiScan:Lcom/google/android/location/data/WifiScan;

    iget-wide v3, v3, Lcom/google/android/location/data/WifiScan;->deliveryTime:J

    sub-long v52, p1, v3

    goto/16 :goto_0

    :cond_29
    const/16 v47, 0x0

    goto/16 :goto_1

    :cond_2a
    const/16 v21, 0x0

    goto/16 :goto_2

    :cond_2b
    const/16 v23, 0x0

    goto/16 :goto_3

    :cond_2c
    const/4 v14, 0x0

    goto/16 :goto_4

    :cond_2d
    const/4 v15, 0x0

    goto/16 :goto_5

    :cond_2e
    const/16 v38, 0x0

    goto/16 :goto_6

    :cond_2f
    const/16 v39, 0x0

    goto/16 :goto_7

    :cond_30
    const/4 v11, 0x0

    goto/16 :goto_8

    :cond_31
    const/16 v54, 0x0

    goto/16 :goto_9

    :cond_32
    const/16 v48, 0x0

    goto/16 :goto_a

    :cond_33
    const/16 v50, 0x0

    goto/16 :goto_b

    :cond_34
    const/16 v49, 0x0

    goto/16 :goto_c

    :cond_35
    const/16 v41, 0x0

    goto/16 :goto_d

    :cond_36
    const/16 v40, 0x0

    goto/16 :goto_e

    :cond_37
    sget-object v4, Lcom/google/android/location/NetworkLocator;->EMPTY_CELL_STATUS:Lcom/google/android/location/data/CellStatus;

    goto/16 :goto_f

    :cond_38
    const/4 v5, 0x0

    goto/16 :goto_10

    :cond_39
    const/16 v22, 0x0

    goto/16 :goto_11

    :cond_3a
    const/16 v16, 0x0

    goto/16 :goto_12

    :cond_3b
    const/16 v17, 0x0

    goto/16 :goto_13

    :cond_3c
    const/16 v34, 0x0

    goto/16 :goto_14

    :cond_3d
    const/16 v26, 0x0

    goto/16 :goto_15

    :cond_3e
    const/16 v37, 0x0

    goto/16 :goto_16

    :cond_3f
    const/16 v20, 0x0

    goto/16 :goto_17

    :cond_40
    const/16 v18, 0x0

    goto/16 :goto_18

    :cond_41
    const/16 v51, 0x0

    goto/16 :goto_19

    :cond_42
    const/16 v35, 0x0

    goto/16 :goto_1a

    :cond_43
    const/16 v36, 0x0

    goto/16 :goto_1b

    :cond_44
    const/4 v3, 0x0

    goto/16 :goto_1c

    :cond_45
    const/4 v3, 0x0

    goto/16 :goto_1d

    :cond_46
    const/16 v25, 0x0

    goto/16 :goto_1e

    :cond_47
    const/16 v19, 0x0

    goto/16 :goto_1f

    :cond_48
    const/4 v3, 0x0

    goto/16 :goto_20

    :cond_49
    invoke-virtual/range {v46 .. v46}, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;->getType()Lcom/google/android/location/data/TravelDetectionType;

    move-result-object v3

    goto/16 :goto_21
.end method


# virtual methods
.method public airplaneModeChanged(Z)V
    .locals 4
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v0}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-boolean v3, p0, Lcom/google/android/location/NetworkLocator;->wifiEnabled:Z

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/google/android/location/NetworkLocator;->setCellWifiEnabled(JZZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public alarmRing()V
    .locals 7

    const/4 v3, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v0}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v1

    move-object v0, p0

    move-object v4, v3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/NetworkLocator;->updateState(JLcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/GlsLocatorResult;ZZ)V

    return-void
.end method

.method public cellScanResults(Lcom/google/android/location/data/CellState;)V
    .locals 7
    .param p1    # Lcom/google/android/location/data/CellState;

    const/4 v3, 0x0

    const/4 v5, 0x0

    if-nez p1, :cond_0

    const-string v0, "NetworkLocator"

    const-string v1, "null cell state delivered"

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/NetworkLocator;->lastCellScanRequestTime:J

    :goto_0
    iget-object v0, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v0}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v1

    move-object v0, p0

    move-object v4, v3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/NetworkLocator;->updateState(JLcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/GlsLocatorResult;ZZ)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/NetworkLocator;->cellStatus:Lcom/google/android/location/data/CellStatus;

    invoke-virtual {v0, p1}, Lcom/google/android/location/data/CellStatus;->setPrimary(Lcom/google/android/location/data/CellState;)V

    goto :goto_0
.end method

.method public cellSignalStrength(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/location/NetworkLocator;->cellStatus:Lcom/google/android/location/data/CellStatus;

    invoke-virtual {v0, p1}, Lcom/google/android/location/data/CellStatus;->setSignalStrength(I)V

    return-void
.end method

.method public glsModelQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 8
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/location/NetworkLocator;->modelStateManager:Lcom/google/android/location/localizer/ModelStateManager;

    iget-object v1, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v1}, Lcom/google/android/location/os/Os;->millisSinceEpoch()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/location/localizer/ModelStateManager;->glsModelQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;J)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v0, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v0}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v1

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/NetworkLocator;->updateState(JLcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/GlsLocatorResult;ZZ)V

    :cond_0
    return-void
.end method

.method public glsQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 8
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v0}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v1

    iget-object v0, p0, Lcom/google/android/location/NetworkLocator;->glsQueryWifiScan:Lcom/google/android/location/data/WifiScan;

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/location/data/GlsLocatorResult;->fromGLocReply(Lcom/google/gmm/common/io/protocol/ProtoBuf;Lcom/google/android/location/data/WifiScan;J)Lcom/google/android/location/data/GlsLocatorResult;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/location/NetworkLocator;->state:Lcom/google/android/location/cache/PersistentState;

    iget-object v3, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v3}, Lcom/google/android/location/os/Os;->millisSinceEpoch()J

    move-result-wide v6

    invoke-virtual {v0, p1, v5, v6, v7}, Lcom/google/android/location/cache/PersistentState;->updateCachesFromGlsQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;ZJ)V

    iget-object v0, p0, Lcom/google/android/location/NetworkLocator;->modelStateManager:Lcom/google/android/location/localizer/ModelStateManager;

    iget-object v3, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v3}, Lcom/google/android/location/os/Os;->millisSinceEpoch()J

    move-result-wide v6

    invoke-virtual {v0, p1, v6, v7}, Lcom/google/android/location/localizer/ModelStateManager;->glsQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;J)Z

    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/NetworkLocator;->updateState(JLcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/GlsLocatorResult;ZZ)V

    return-void
.end method

.method isWakeLocked()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/location/NetworkLocator;->lastWakeLockAcquireTime:J

    iget-wide v2, p0, Lcom/google/android/location/NetworkLocator;->lastWakeLockReleaseTime:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPeriod(IZ)V
    .locals 10
    .param p1    # I
    .param p2    # Z

    int-to-long v3, p1

    const-wide/16 v5, 0x3e8

    mul-long/2addr v3, v5

    iput-wide v3, p0, Lcom/google/android/location/NetworkLocator;->period:J

    iget-object v0, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v0}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v1

    iget-boolean v0, p0, Lcom/google/android/location/NetworkLocator;->cellEnabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/NetworkLocator;->wifiEnabled:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v7, 0x1

    :goto_0
    if-eqz v7, :cond_1

    if-eqz p2, :cond_3

    const-wide/16 v8, 0x0

    :goto_1
    iget-wide v3, p0, Lcom/google/android/location/NetworkLocator;->nextTriggerTime:J

    invoke-static {v3, v4, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/location/NetworkLocator;->nextTriggerTime:J

    :cond_1
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/NetworkLocator;->updateState(JLcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/GlsLocatorResult;ZZ)V

    return-void

    :cond_2
    const/4 v7, 0x0

    goto :goto_0

    :cond_3
    iget-wide v3, p0, Lcom/google/android/location/NetworkLocator;->period:J

    add-long v8, v1, v3

    goto :goto_1
.end method

.method public wifiScanResults(Lcom/google/android/location/data/WifiScan;)V
    .locals 7
    .param p1    # Lcom/google/android/location/data/WifiScan;

    const/4 v5, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v0}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v1

    const/4 v4, 0x0

    move-object v0, p0

    move-object v3, p1

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/NetworkLocator;->updateState(JLcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/GlsLocatorResult;ZZ)V

    goto :goto_0
.end method

.method public wifiStateChanged(Z)V
    .locals 3
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/location/NetworkLocator;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v0}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v0

    iget-boolean v2, p0, Lcom/google/android/location/NetworkLocator;->cellEnabled:Z

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/google/android/location/NetworkLocator;->setCellWifiEnabled(JZZ)V

    return-void
.end method
