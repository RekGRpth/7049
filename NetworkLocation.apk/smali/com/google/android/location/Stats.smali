.class public Lcom/google/android/location/Stats;
.super Ljava/lang/Object;
.source "Stats.java"


# instance fields
.field private final os:Lcom/google/android/location/os/Os;

.field private periodStart:J

.field private final persistentState:Lcom/google/android/location/cache/PersistentState;

.field private final seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/cache/PersistentState;Lcom/google/android/location/cache/SeenDevicesCache;)V
    .locals 2
    .param p1    # Lcom/google/android/location/os/Os;
    .param p2    # Lcom/google/android/location/cache/PersistentState;
    .param p3    # Lcom/google/android/location/cache/SeenDevicesCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/location/Stats;->periodStart:J

    iput-object p1, p0, Lcom/google/android/location/Stats;->os:Lcom/google/android/location/os/Os;

    iput-object p2, p0, Lcom/google/android/location/Stats;->persistentState:Lcom/google/android/location/cache/PersistentState;

    iput-object p3, p0, Lcom/google/android/location/Stats;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    return-void
.end method

.method private createClientStats(JJ)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 8
    .param p1    # J
    .param p3    # J

    const-wide/16 v6, 0x3e8

    new-instance v2, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/4 v3, 0x1

    div-long v4, p3, v6

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setLong(IJ)V

    const/4 v3, 0x2

    iget-wide v4, p0, Lcom/google/android/location/Stats;->periodStart:J

    sub-long v4, p1, v4

    div-long/2addr v4, v6

    long-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/location/Stats;->persistentState:Lcom/google/android/location/cache/PersistentState;

    iget-object v4, v4, Lcom/google/android/location/cache/PersistentState;->cellCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-virtual {v4}, Lcom/google/android/location/cache/TemporalCache;->extractStatsAndReset()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/location/Stats;->persistentState:Lcom/google/android/location/cache/PersistentState;

    iget-object v4, v4, Lcom/google/android/location/cache/PersistentState;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-virtual {v4}, Lcom/google/android/location/cache/TemporalCache;->extractStatsAndReset()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    sub-long v0, p3, p1

    iget-object v3, p0, Lcom/google/android/location/Stats;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    if-eqz v3, :cond_0

    const/16 v3, 0xb

    iget-object v4, p0, Lcom/google/android/location/Stats;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    invoke-virtual {v4}, Lcom/google/android/location/cache/SeenDevicesCache;->extractStats()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    :cond_0
    const/16 v3, 0xe

    iget-object v4, p0, Lcom/google/android/location/Stats;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v4}, Lcom/google/android/location/os/Os;->getNlpMemoryKb()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/16 v3, 0xf

    iget-object v4, p0, Lcom/google/android/location/Stats;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v4}, Lcom/google/android/location/os/Os;->getFreeMemoryKb()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    return-object v2
.end method


# virtual methods
.method public maybeAddClientStats(Lcom/google/android/location/os/Os;Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 6
    .param p1    # Lcom/google/android/location/os/Os;
    .param p2    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-interface {p1}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/Stats;->periodStart:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/Stats;->reset(J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v2, p0, Lcom/google/android/location/Stats;->periodStart:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x6ddd00

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    const/4 v2, 0x5

    invoke-interface {p1}, Lcom/google/android/location/os/Os;->millisSinceEpoch()J

    move-result-wide v3

    invoke-direct {p0, v0, v1, v3, v4}, Lcom/google/android/location/Stats;->createClientStats(JJ)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    iput-wide v0, p0, Lcom/google/android/location/Stats;->periodStart:J

    goto :goto_0
.end method

.method public reset(J)V
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/location/Stats;->persistentState:Lcom/google/android/location/cache/PersistentState;

    iget-object v0, v0, Lcom/google/android/location/cache/PersistentState;->cellCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-virtual {v0}, Lcom/google/android/location/cache/TemporalCache;->extractStatsAndReset()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/Stats;->persistentState:Lcom/google/android/location/cache/PersistentState;

    iget-object v0, v0, Lcom/google/android/location/cache/PersistentState;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-virtual {v0}, Lcom/google/android/location/cache/TemporalCache;->extractStatsAndReset()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iput-wide p1, p0, Lcom/google/android/location/Stats;->periodStart:J

    return-void
.end method
