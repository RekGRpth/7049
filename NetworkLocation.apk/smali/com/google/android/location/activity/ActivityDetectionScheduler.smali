.class public Lcom/google/android/location/activity/ActivityDetectionScheduler;
.super Ljava/lang/Object;
.source "ActivityDetectionScheduler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/activity/ActivityDetectionScheduler$1;,
        Lcom/google/android/location/activity/ActivityDetectionScheduler$State;,
        Lcom/google/android/location/activity/ActivityDetectionScheduler$OffState;
    }
.end annotation


# instance fields
.field private final hasAccelerometer:Z

.field private lastAlarmTime:J

.field private lastReportedActivity:Lcom/google/android/location/clientlib/NlpActivity;

.field private lastReportedActivityTime:J

.field private lastTravelMode:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

.field private final os:Lcom/google/android/location/os/Os;

.field private screenOn:Z

.field private final sensorRateUtil:Lcom/google/android/location/activity/SensorRateUtil;

.field private state:Lcom/google/android/location/activity/ActivityDetectionScheduler$State;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/activity/SensorRateUtil;)V
    .locals 4
    .param p1    # Lcom/google/android/location/os/Os;
    .param p2    # Lcom/google/android/location/activity/SensorRateUtil;

    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/activity/ActivityDetectionScheduler$OffState;

    invoke-direct {v0, p0, v1}, Lcom/google/android/location/activity/ActivityDetectionScheduler$OffState;-><init>(Lcom/google/android/location/activity/ActivityDetectionScheduler;Lcom/google/android/location/activity/ActivityDetectionScheduler$1;)V

    iput-object v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->state:Lcom/google/android/location/activity/ActivityDetectionScheduler$State;

    iput-wide v2, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->lastAlarmTime:J

    iput-object v1, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->lastReportedActivity:Lcom/google/android/location/clientlib/NlpActivity;

    iput-wide v2, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->lastReportedActivityTime:J

    iput-object v1, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->lastTravelMode:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->screenOn:Z

    const-string v0, "ActivityScheduler"

    const-string v1, "ActivityDetectionScheduler started in state off"

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->os:Lcom/google/android/location/os/Os;

    iput-object p2, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->sensorRateUtil:Lcom/google/android/location/activity/SensorRateUtil;

    invoke-interface {p1}, Lcom/google/android/location/os/Os;->hasAccelerometer()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->hasAccelerometer:Z

    iget-boolean v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->hasAccelerometer:Z

    if-nez v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "No accelerometer detected. Activity detection will be disabled."

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "ActivityScheduler"

    const-string v1, "Activity detection will be disabled due to client flag."

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/location/activity/ActivityDetectionScheduler;)Z
    .locals 1
    .param p0    # Lcom/google/android/location/activity/ActivityDetectionScheduler;

    iget-boolean v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->hasAccelerometer:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/location/activity/ActivityDetectionScheduler;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/location/activity/ActivityDetectionScheduler;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->screenOn:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/location/activity/ActivityDetectionScheduler;Lcom/google/android/location/activity/ActivityDetectionScheduler$State;)V
    .locals 0
    .param p0    # Lcom/google/android/location/activity/ActivityDetectionScheduler;
    .param p1    # Lcom/google/android/location/activity/ActivityDetectionScheduler$State;

    invoke-direct {p0, p1}, Lcom/google/android/location/activity/ActivityDetectionScheduler;->changeState(Lcom/google/android/location/activity/ActivityDetectionScheduler$State;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/location/activity/ActivityDetectionScheduler;)V
    .locals 0
    .param p0    # Lcom/google/android/location/activity/ActivityDetectionScheduler;

    invoke-direct {p0}, Lcom/google/android/location/activity/ActivityDetectionScheduler;->cancelAlarm()V

    return-void
.end method

.method private cancelAlarm()V
    .locals 4

    const-wide/16 v2, -0x1

    iget-wide v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->lastAlarmTime:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    const-string v1, "Alarm canceled"

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->os:Lcom/google/android/location/os/Os;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/location/os/Os;->alarmCancel(I)V

    iput-wide v2, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->lastAlarmTime:J

    :cond_0
    return-void
.end method

.method private changeState(Lcom/google/android/location/activity/ActivityDetectionScheduler$State;)V
    .locals 3
    .param p1    # Lcom/google/android/location/activity/ActivityDetectionScheduler$State;

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Leaving state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->state:Lcom/google/android/location/activity/ActivityDetectionScheduler$State;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->state:Lcom/google/android/location/activity/ActivityDetectionScheduler$State;

    invoke-virtual {v0}, Lcom/google/android/location/activity/ActivityDetectionScheduler$State;->stateExit()V

    iput-object p1, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->state:Lcom/google/android/location/activity/ActivityDetectionScheduler$State;

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->state:Lcom/google/android/location/activity/ActivityDetectionScheduler$State;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->state:Lcom/google/android/location/activity/ActivityDetectionScheduler$State;

    invoke-virtual {v0}, Lcom/google/android/location/activity/ActivityDetectionScheduler$State;->stateEntered()V

    return-void
.end method

.method private resetAlarm(J)V
    .locals 5
    .param p1    # J

    iget-wide v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->lastAlarmTime:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->lastAlarmTime:J

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Alarm set to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    iget-object v3, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v3}, Lcom/google/android/location/os/Os;->bootTime()J

    move-result-wide v3

    add-long/2addr v3, p1

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->os:Lcom/google/android/location/os/Os;

    const/4 v1, 0x2

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/location/os/Os;->alarmReset(IJ)V

    :cond_0
    return-void
.end method


# virtual methods
.method public activityDetectionEnabledChanged(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ActivityDetectionEnabledChanged: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->state:Lcom/google/android/location/activity/ActivityDetectionScheduler$State;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/ActivityDetectionScheduler$State;->activityDetectionEnabledChanged(Z)V

    return-void
.end method

.method public alarmRing(I)V
    .locals 8
    .param p1    # I

    const-wide/16 v6, -0x1

    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->lastAlarmTime:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_1

    const-string v2, "ActivityScheduler"

    const-string v3, "Alarm received when no alarm set"

    invoke-static {v2, v3}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v2}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->lastAlarmTime:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    iget-wide v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->lastAlarmTime:J

    iput-wide v6, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->lastAlarmTime:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/location/activity/ActivityDetectionScheduler;->resetAlarm(J)V

    goto :goto_0

    :cond_2
    iput-wide v6, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->lastAlarmTime:J

    iget-object v2, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->state:Lcom/google/android/location/activity/ActivityDetectionScheduler$State;

    invoke-virtual {v2}, Lcom/google/android/location/activity/ActivityDetectionScheduler$State;->alarmRing()V

    goto :goto_0
.end method

.method public screenStateChanged(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->state:Lcom/google/android/location/activity/ActivityDetectionScheduler$State;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/ActivityDetectionScheduler$State;->screenStateChanged(Z)V

    return-void
.end method

.method public travelModeDetected(Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;)V
    .locals 1
    .param p1    # Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    iget-object v0, p0, Lcom/google/android/location/activity/ActivityDetectionScheduler;->state:Lcom/google/android/location/activity/ActivityDetectionScheduler$State;

    invoke-virtual {v0, p1}, Lcom/google/android/location/activity/ActivityDetectionScheduler$State;->travelModeDetected(Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;)V

    return-void
.end method
