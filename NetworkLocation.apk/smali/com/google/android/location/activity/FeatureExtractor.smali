.class Lcom/google/android/location/activity/FeatureExtractor;
.super Ljava/lang/Object;
.source "FeatureExtractor.java"


# instance fields
.field private final wifiCache:Lcom/google/android/location/cache/TemporalCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/TemporalCache",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/location/cache/TemporalCache;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/cache/TemporalCache",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/activity/FeatureExtractor;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    return-void
.end method

.method private addDistanceFeatures(Ljava/util/List;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/activity/TravelClassifier$TravelFeature;",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v2, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->WIFI_LOC_DISTANCE_0_1:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/location/activity/FeatureExtractor;->computeDistance(Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;Z)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p2, v2, v0}, Lcom/google/android/location/activity/FeatureExtractor;->putIfNotNull(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;Ljava/lang/Double;)V

    sget-object v2, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->WIFI_LOC_DISTANCE_0_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/location/activity/FeatureExtractor;->computeDistance(Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;Z)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p2, v2, v0}, Lcom/google/android/location/activity/FeatureExtractor;->putIfNotNull(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;Ljava/lang/Double;)V

    sget-object v2, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->WIFI_LOC_DISTANCE_1_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/location/activity/FeatureExtractor;->computeDistance(Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;Z)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p2, v2, v0}, Lcom/google/android/location/activity/FeatureExtractor;->putIfNotNull(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;Ljava/lang/Double;)V

    sget-object v2, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->CELL_LOC_DISTANCE_0_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;

    invoke-direct {p0, v0, v1, v4}, Lcom/google/android/location/activity/FeatureExtractor;->computeDistance(Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;Z)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {p0, p2, v2, v0}, Lcom/google/android/location/activity/FeatureExtractor;->putIfNotNull(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;Ljava/lang/Double;)V

    return-void
.end method

.method private addWifiFeatures(Ljava/util/List;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/activity/TravelClassifier$TravelFeature;",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    const/4 v4, 0x0

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;

    invoke-virtual {v4}, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;->getWifiScan()Lcom/google/android/location/data/WifiScan;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/location/activity/FeatureExtractor;->getMacsFromScan(Lcom/google/android/location/data/WifiScan;)Ljava/util/Set;

    move-result-object v1

    const/4 v4, 0x1

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;

    invoke-virtual {v4}, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;->getWifiScan()Lcom/google/android/location/data/WifiScan;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/location/activity/FeatureExtractor;->getMacsFromScan(Lcom/google/android/location/data/WifiScan;)Ljava/util/Set;

    move-result-object v2

    const/4 v4, 0x2

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;

    invoke-virtual {v4}, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;->getWifiScan()Lcom/google/android/location/data/WifiScan;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/location/activity/FeatureExtractor;->getMacsFromScan(Lcom/google/android/location/data/WifiScan;)Ljava/util/Set;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/google/android/location/activity/FeatureExtractor;->computeScanIntersection(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sget-object v4, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->SCAN_OVERLAP_RATIO_0_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-direct {p0, v1, v3, v0}, Lcom/google/android/location/activity/FeatureExtractor;->computeIntersectionRatio(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Ljava/lang/Double;

    move-result-object v5

    invoke-direct {p0, p2, v4, v5}, Lcom/google/android/location/activity/FeatureExtractor;->putIfNotNull(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;Ljava/lang/Double;)V

    sget-object v4, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->SCAN_OVERLAP_COUNT_0_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v5

    int-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-interface {p2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->SCAN_OVERLAP_MIN_RADIUS_0_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-direct {p0, v0}, Lcom/google/android/location/activity/FeatureExtractor;->computeMinWifiRadius(Ljava/util/Set;)Ljava/lang/Double;

    move-result-object v5

    invoke-direct {p0, p2, v4, v5}, Lcom/google/android/location/activity/FeatureExtractor;->putIfNotNull(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;Ljava/lang/Double;)V

    invoke-direct {p0, v2, v3}, Lcom/google/android/location/activity/FeatureExtractor;->computeScanIntersection(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sget-object v4, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->SCAN_OVERLAP_RATIO_1_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/location/activity/FeatureExtractor;->computeIntersectionRatio(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Ljava/lang/Double;

    move-result-object v5

    invoke-direct {p0, p2, v4, v5}, Lcom/google/android/location/activity/FeatureExtractor;->putIfNotNull(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;Ljava/lang/Double;)V

    return-void
.end method

.method private computeDistance(Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;Z)Ljava/lang/Double;
    .locals 6
    .param p1    # Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;
    .param p2    # Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;
    .param p3    # Z

    invoke-direct {p0, p1, p3}, Lcom/google/android/location/activity/FeatureExtractor;->getPosition(Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;Z)Lcom/google/android/location/data/Position;

    move-result-object v2

    invoke-direct {p0, p2, p3}, Lcom/google/android/location/activity/FeatureExtractor;->getPosition(Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;Z)Lcom/google/android/location/data/Position;

    move-result-object v3

    if-eqz v2, :cond_0

    if-nez v3, :cond_1

    :cond_0
    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_1
    invoke-static {v2, v3}, Lcom/google/android/location/localizer/LocalizerUtil;->computeDistance(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/Position;)I

    move-result v0

    if-eqz p3, :cond_2

    const/16 v1, 0x1388

    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    goto :goto_0

    :cond_2
    const/16 v1, 0x3e8

    goto :goto_1
.end method

.method private computeIntersectionRatio(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Ljava/lang/Double;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/Double;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v2

    add-int/2addr v1, v2

    invoke-interface {p3}, Ljava/util/Set;->size()I

    move-result v2

    sub-int v0, v1, v2

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-interface {p3}, Ljava/util/Set;->size()I

    move-result v1

    int-to-double v1, v1

    int-to-double v3, v0

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    goto :goto_0
.end method

.method private computeMinWifiRadius(Ljava/util/Set;)Ljava/lang/Double;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/Double;"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    iget-object v5, p0, Lcom/google/android/location/activity/FeatureExtractor;->wifiCache:Lcom/google/android/location/cache/TemporalCache;

    invoke-virtual {v5, v2}, Lcom/google/android/location/cache/TemporalCache;->lookupWithoutUpdatingLastSeen(Ljava/lang/Object;)Lcom/google/android/location/cache/CacheResult;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/location/cache/CacheResult;->getPosition()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/location/data/WifiApPosition;

    iget v5, v5, Lcom/google/android/location/data/WifiApPosition;->accuracyMm:I

    div-int/lit16 v4, v5, 0x3e8

    if-eqz v3, :cond_1

    int-to-double v5, v4

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    cmpg-double v5, v5, v7

    if-gez v5, :cond_0

    :cond_1
    int-to-double v5, v4

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    goto :goto_0

    :cond_2
    return-object v3
.end method

.method private computeScanIntersection(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, p2}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method private getMacsFromScan(Lcom/google/android/location/data/WifiScan;)Ljava/util/Set;
    .locals 3
    .param p1    # Lcom/google/android/location/data/WifiScan;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/data/WifiScan;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/location/data/WifiScan;->numDevices()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/android/location/data/WifiScan;->getDevice(I)Lcom/google/android/location/data/WifiScan$Device;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/location/data/WifiScan$Device;->mac:Ljava/lang/Long;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private getPosition(Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;Z)Lcom/google/android/location/data/Position;
    .locals 3
    .param p1    # Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;
    .param p2    # Z

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;->getNetworkLocation()Lcom/google/android/location/data/NetworkLocation;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    if-eqz p2, :cond_2

    iget-object v1, v0, Lcom/google/android/location/data/NetworkLocation;->cellResult:Lcom/google/android/location/data/CellLocatorResult;

    :goto_1
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/google/android/location/data/LocatorResult;->position:Lcom/google/android/location/data/Position;

    goto :goto_0

    :cond_2
    iget-object v1, v0, Lcom/google/android/location/data/NetworkLocation;->wifiResult:Lcom/google/android/location/data/WifiLocatorResult;

    goto :goto_1
.end method

.method private putIfNotNull(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;Ljava/lang/Double;)V
    .locals 0
    .param p2    # Lcom/google/android/location/activity/TravelClassifier$TravelFeature;
    .param p3    # Ljava/lang/Double;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/activity/TravelClassifier$TravelFeature;",
            "Ljava/lang/Double;",
            ">;",
            "Lcom/google/android/location/activity/TravelClassifier$TravelFeature;",
            "Ljava/lang/Double;",
            ")V"
        }
    .end annotation

    if-eqz p3, :cond_0

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method


# virtual methods
.method public computeFeatures(Ljava/util/List;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/activity/TravelClassifier$TravelFeature;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t compute features for history of size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/activity/FeatureExtractor;->addWifiFeatures(Ljava/util/List;Ljava/util/Map;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/activity/FeatureExtractor;->addDistanceFeatures(Ljava/util/List;Ljava/util/Map;)V

    return-object v0
.end method
