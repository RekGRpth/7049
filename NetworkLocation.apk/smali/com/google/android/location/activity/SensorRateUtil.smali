.class public Lcom/google/android/location/activity/SensorRateUtil;
.super Lcom/google/android/location/collectionlib/SimpleCollectorListener;
.source "SensorRateUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/activity/SensorRateUtil$SensorRateData;
    }
.end annotation


# static fields
.field private static SENSOR_RATE_FILENAME:Ljava/lang/String;


# instance fields
.field private activeAnalysisSensorDelay:I

.field private activeAnalysisSensorType:Lcom/google/android/location/collectionlib/ScannerType;

.field private final lastAnalysisEpochTimes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/collectionlib/ScannerType;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final os:Lcom/google/android/location/os/Os;

.field private final sensorRates:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/collectionlib/ScannerType;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/activity/SensorRateUtil$SensorRateData;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "rate"

    sput-object v0, Lcom/google/android/location/activity/SensorRateUtil;->SENSOR_RATE_FILENAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/Os;)V
    .locals 2
    .param p1    # Lcom/google/android/location/os/Os;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/location/activity/SensorRateUtil;-><init>(Lcom/google/android/location/os/Os;Ljava/util/Map;Ljava/util/Map;)V

    invoke-direct {p0}, Lcom/google/android/location/activity/SensorRateUtil;->loadSensorRates()V

    return-void
.end method

.method constructor <init>(Lcom/google/android/location/os/Os;Ljava/util/Map;Ljava/util/Map;)V
    .locals 1
    .param p1    # Lcom/google/android/location/os/Os;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/os/Os;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/collectionlib/ScannerType;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/activity/SensorRateUtil$SensorRateData;",
            ">;>;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/collectionlib/ScannerType;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/location/collectionlib/SimpleCollectorListener;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/activity/SensorRateUtil;->activeAnalysisSensorDelay:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/activity/SensorRateUtil;->activeAnalysisSensorType:Lcom/google/android/location/collectionlib/ScannerType;

    iput-object p1, p0, Lcom/google/android/location/activity/SensorRateUtil;->os:Lcom/google/android/location/os/Os;

    iput-object p2, p0, Lcom/google/android/location/activity/SensorRateUtil;->sensorRates:Ljava/util/Map;

    iput-object p3, p0, Lcom/google/android/location/activity/SensorRateUtil;->lastAnalysisEpochTimes:Ljava/util/Map;

    return-void
.end method

.method private loadSensorRates()V
    .locals 14

    const/4 v13, 0x2

    iget-object v10, p0, Lcom/google/android/location/activity/SensorRateUtil;->sensorRates:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->clear()V

    iget-object v10, p0, Lcom/google/android/location/activity/SensorRateUtil;->lastAnalysisEpochTimes:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->clear()V

    iget-object v10, p0, Lcom/google/android/location/activity/SensorRateUtil;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v10}, Lcom/google/android/location/os/Os;->getSensorCacheDir()Ljava/io/File;

    move-result-object v8

    new-instance v2, Ljava/io/File;

    sget-object v10, Lcom/google/android/location/activity/SensorRateUtil;->SENSOR_RATE_FILENAME:Ljava/lang/String;

    invoke-direct {v2, v8, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/FileReader;

    invoke-direct {v10, v2}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v5, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const/4 v3, 0x1

    :cond_1
    :goto_1
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    const-string v10, ","

    invoke-virtual {v4, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v10, 0x0

    aget-object v10, v1, v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Lcom/google/android/location/collectionlib/ScannerType;->parse(I)Lcom/google/android/location/collectionlib/ScannerType;

    move-result-object v6

    array-length v10, v1

    if-ne v10, v13, :cond_3

    iget-object v10, p0, Lcom/google/android/location/activity/SensorRateUtil;->lastAnalysisEpochTimes:Ljava/util/Map;

    const/4 v11, 0x1

    aget-object v11, v1, v11

    invoke-static {v11}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v10, v6, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v10, p0, Lcom/google/android/location/activity/SensorRateUtil;->lastAnalysisEpochTimes:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->clear()V

    iget-object v10, p0, Lcom/google/android/location/activity/SensorRateUtil;->sensorRates:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->clear()V

    const-string v10, "SensorRateUtil"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11, v0}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    :try_start_1
    array-length v10, v1

    const/4 v11, 0x3

    if-ne v10, v11, :cond_1

    iget-object v10, p0, Lcom/google/android/location/activity/SensorRateUtil;->sensorRates:Ljava/util/Map;

    invoke-interface {v10, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    if-nez v9, :cond_4

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iget-object v10, p0, Lcom/google/android/location/activity/SensorRateUtil;->sensorRates:Ljava/util/Map;

    invoke-interface {v10, v6, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    new-instance v10, Lcom/google/android/location/activity/SensorRateUtil$SensorRateData;

    const/4 v11, 0x1

    aget-object v11, v1, v11

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    const/4 v12, 0x2

    aget-object v12, v1, v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    invoke-direct {v10, v11, v12}, Lcom/google/android/location/activity/SensorRateUtil$SensorRateData;-><init>(II)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
