.class Lcom/google/android/location/activity/TravelClassifier;
.super Ljava/lang/Object;
.source "TravelClassifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/activity/TravelClassifier$TravelFeature;
    }
.end annotation


# instance fields
.field private final defaultFeatureValues:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/activity/TravelClassifier$TravelFeature;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sget-object v1, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->SCAN_OVERLAP_MIN_RADIUS_0_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    const-wide v2, 0x4061c4fdf3b645a2L

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->WIFI_LOC_DISTANCE_0_1:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    const-wide v2, 0x40591cfdf3b645a2L

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->WIFI_LOC_DISTANCE_0_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    const-wide v2, 0x4065e7e76c8b4396L

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->WIFI_LOC_DISTANCE_1_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    const-wide v2, 0x405b5428f5c28f5cL

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->CELL_LOC_DISTANCE_0_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    const-wide v2, 0x40709e9374bc6a7fL

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/activity/TravelClassifier;->defaultFeatureValues:Ljava/util/Map;

    return-void
.end method

.method private computeProbability(Ljava/util/Map;)D
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/activity/TravelClassifier$TravelFeature;",
            "Ljava/lang/Double;",
            ">;)D"
        }
    .end annotation

    const-wide/high16 v8, 0x3ff0000000000000L

    const-wide v2, -0x3fff4504816f0069L

    const-wide v4, 0x3ffffe5c91d14e3cL

    sget-object v6, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->SCAN_OVERLAP_RATIO_0_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-direct {p0, p1, v6}, Lcom/google/android/location/activity/TravelClassifier;->get(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide v4, 0x3fd1013a92a30553L

    sget-object v6, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->SCAN_OVERLAP_COUNT_0_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-direct {p0, p1, v6}, Lcom/google/android/location/activity/TravelClassifier;->get(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide v4, -0x409c56d5cfaacd9fL

    sget-object v6, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->SCAN_OVERLAP_MIN_RADIUS_0_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-direct {p0, p1, v6}, Lcom/google/android/location/activity/TravelClassifier;->get(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide v4, 0x40158b0f27bb2fecL

    sget-object v6, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->SCAN_OVERLAP_RATIO_1_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-direct {p0, p1, v6}, Lcom/google/android/location/activity/TravelClassifier;->get(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide v4, -0x40876c8b43958106L

    sget-object v6, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->WIFI_LOC_DISTANCE_0_1:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-direct {p0, p1, v6}, Lcom/google/android/location/activity/TravelClassifier;->get(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide v4, -0x408353f7ced91687L

    sget-object v6, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->WIFI_LOC_DISTANCE_0_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-direct {p0, p1, v6}, Lcom/google/android/location/activity/TravelClassifier;->get(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide v4, -0x406978d4fdf3b646L

    sget-object v6, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->WIFI_LOC_DISTANCE_1_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-direct {p0, p1, v6}, Lcom/google/android/location/activity/TravelClassifier;->get(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    const-wide v4, -0x40bf9db22d0e5604L

    sget-object v6, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->CELL_LOC_DISTANCE_0_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-direct {p0, p1, v6}, Lcom/google/android/location/activity/TravelClassifier;->get(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double v0, v2, v4

    const-wide v2, 0x4005bf0a8b145769L

    neg-double v4, v0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v2, v8

    div-double v2, v8, v2

    return-wide v2
.end method

.method private get(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;)D
    .locals 3
    .param p2    # Lcom/google/android/location/activity/TravelClassifier$TravelFeature;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/activity/TravelClassifier$TravelFeature;",
            "Ljava/lang/Double;",
            ">;",
            "Lcom/google/android/location/activity/TravelClassifier$TravelFeature;",
            ")D"
        }
    .end annotation

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/location/activity/TravelClassifier;->defaultFeatureValues:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    return-wide v1
.end method

.method private isDataUnclear(Ljava/util/Map;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/activity/TravelClassifier$TravelFeature;",
            "Ljava/lang/Double;",
            ">;)Z"
        }
    .end annotation

    sget-object v0, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->SCAN_OVERLAP_RATIO_1_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/location/activity/TravelClassifier$TravelFeature;->SCAN_OVERLAP_MIN_RADIUS_0_2:Lcom/google/android/location/activity/TravelClassifier$TravelFeature;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/activity/TravelClassifier;->get(Ljava/util/Map;Lcom/google/android/location/activity/TravelClassifier$TravelFeature;)D

    move-result-wide v0

    const-wide/high16 v2, 0x4079000000000000L

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public classify(Ljava/util/Map;)Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/location/activity/TravelClassifier$TravelFeature;",
            "Ljava/lang/Double;",
            ">;)",
            "Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/location/activity/TravelClassifier;->isDataUnclear(Ljava/util/Map;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    :goto_0
    return-object v2

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/location/activity/TravelClassifier;->computeProbability(Ljava/util/Map;)D

    move-result-wide v0

    const-wide/high16 v2, 0x3fe0000000000000L

    cmpg-double v2, v0, v2

    if-gez v2, :cond_1

    new-instance v2, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    sget-object v3, Lcom/google/android/location/data/TravelDetectionType;->MOVING:Lcom/google/android/location/data/TravelDetectionType;

    const-wide/high16 v4, 0x3ff0000000000000L

    sub-double/2addr v4, v0

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;-><init>(Lcom/google/android/location/data/TravelDetectionType;D)V

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    sget-object v3, Lcom/google/android/location/data/TravelDetectionType;->STATIONARY:Lcom/google/android/location/data/TravelDetectionType;

    invoke-direct {v2, v3, v0, v1}, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;-><init>(Lcom/google/android/location/data/TravelDetectionType;D)V

    goto :goto_0
.end method
