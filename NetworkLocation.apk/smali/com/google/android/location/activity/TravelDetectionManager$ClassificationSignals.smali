.class Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;
.super Ljava/lang/Object;
.source "TravelDetectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/activity/TravelDetectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ClassificationSignals"
.end annotation


# instance fields
.field private final networkLocation:Lcom/google/android/location/data/NetworkLocation;

.field private final wifiScan:Lcom/google/android/location/data/WifiScan;


# direct methods
.method public constructor <init>(Lcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/NetworkLocation;)V
    .locals 0
    .param p1    # Lcom/google/android/location/data/WifiScan;
    .param p2    # Lcom/google/android/location/data/NetworkLocation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;->wifiScan:Lcom/google/android/location/data/WifiScan;

    iput-object p2, p0, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;->networkLocation:Lcom/google/android/location/data/NetworkLocation;

    return-void
.end method


# virtual methods
.method public getNetworkLocation()Lcom/google/android/location/data/NetworkLocation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;->networkLocation:Lcom/google/android/location/data/NetworkLocation;

    return-object v0
.end method

.method public getWifiScan()Lcom/google/android/location/data/WifiScan;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;->wifiScan:Lcom/google/android/location/data/WifiScan;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;->networkLocation:Lcom/google/android/location/data/NetworkLocation;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ClassificationSignals [wifiScan="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;->wifiScan:Lcom/google/android/location/data/WifiScan;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", networkLocation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;->networkLocation:Lcom/google/android/location/data/NetworkLocation;

    invoke-virtual {v1}, Lcom/google/android/location/data/NetworkLocation;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\n"

    const-string v3, "_"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
