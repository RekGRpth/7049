.class public Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;
.super Ljava/lang/Object;
.source "TravelDetectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/activity/TravelDetectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TravelDetectionResult"
.end annotation


# instance fields
.field private final confidence:D

.field private final type:Lcom/google/android/location/data/TravelDetectionType;


# direct methods
.method constructor <init>(Lcom/google/android/location/data/TravelDetectionType;D)V
    .locals 0
    .param p1    # Lcom/google/android/location/data/TravelDetectionType;
    .param p2    # D

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;->type:Lcom/google/android/location/data/TravelDetectionType;

    iput-wide p2, p0, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;->confidence:D

    return-void
.end method


# virtual methods
.method public getType()Lcom/google/android/location/data/TravelDetectionType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;->type:Lcom/google/android/location/data/TravelDetectionType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TravelDetectionResult [type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;->type:Lcom/google/android/location/data/TravelDetectionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", confidence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;->confidence:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
