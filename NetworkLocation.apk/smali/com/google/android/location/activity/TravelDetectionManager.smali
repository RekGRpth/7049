.class public Lcom/google/android/location/activity/TravelDetectionManager;
.super Ljava/lang/Object;
.source "TravelDetectionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;,
        Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;
    }
.end annotation


# static fields
.field static final UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;


# instance fields
.field private final cellDetector:Lcom/google/android/location/activity/CellTravelDetector;

.field private final classifier:Lcom/google/android/location/activity/TravelClassifier;

.field private final clock:Lcom/google/android/location/os/Clock;

.field private final featureExtractor:Lcom/google/android/location/activity/FeatureExtractor;

.field private isLastTravelResultFromAllSignalsProvider:Z

.field private lastKnownTravelType:Lcom/google/android/location/data/TravelDetectionType;

.field private lastTransitionTime:J

.field private lastTravelDetectionResult:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

.field private lastTravelDetectionTime:J

.field private final signalHistory:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    sget-object v1, Lcom/google/android/location/data/TravelDetectionType;->UNKNOWN:Lcom/google/android/location/data/TravelDetectionType;

    const-wide/high16 v2, -0x4010000000000000L

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;-><init>(Lcom/google/android/location/data/TravelDetectionType;D)V

    sput-object v0, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/cache/TemporalCache;Lcom/google/android/location/os/Clock;)V
    .locals 3
    .param p2    # Lcom/google/android/location/os/Clock;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/cache/TemporalCache",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;",
            "Lcom/google/android/location/os/Clock;",
            ")V"
        }
    .end annotation

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/activity/TravelClassifier;

    invoke-direct {v0}, Lcom/google/android/location/activity/TravelClassifier;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager;->classifier:Lcom/google/android/location/activity/TravelClassifier;

    new-instance v0, Lcom/google/android/location/activity/CellTravelDetector;

    invoke-direct {v0}, Lcom/google/android/location/activity/CellTravelDetector;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager;->cellDetector:Lcom/google/android/location/activity/CellTravelDetector;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager;->signalHistory:Ljava/util/List;

    sget-object v0, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    iput-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager;->lastTravelDetectionResult:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    iput-wide v1, p0, Lcom/google/android/location/activity/TravelDetectionManager;->lastTravelDetectionTime:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/activity/TravelDetectionManager;->isLastTravelResultFromAllSignalsProvider:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager;->lastKnownTravelType:Lcom/google/android/location/data/TravelDetectionType;

    iput-wide v1, p0, Lcom/google/android/location/activity/TravelDetectionManager;->lastTransitionTime:J

    iput-object p2, p0, Lcom/google/android/location/activity/TravelDetectionManager;->clock:Lcom/google/android/location/os/Clock;

    new-instance v0, Lcom/google/android/location/activity/FeatureExtractor;

    invoke-direct {v0, p1}, Lcom/google/android/location/activity/FeatureExtractor;-><init>(Lcom/google/android/location/cache/TemporalCache;)V

    iput-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager;->featureExtractor:Lcom/google/android/location/activity/FeatureExtractor;

    return-void
.end method

.method private detectTravelTypeUsingAllSignals(Lcom/google/android/location/data/NetworkLocation;Lcom/google/android/location/data/WifiScan;)Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;
    .locals 7
    .param p1    # Lcom/google/android/location/data/NetworkLocation;
    .param p2    # Lcom/google/android/location/data/WifiScan;

    if-nez p2, :cond_0

    const-string v4, "TravelC"

    const-string v5, "Null WiFi scan. Not enough signals to determine travel type"

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    :goto_0
    const-string v4, "TravelC"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Travel detection result AllSignals: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-object v3

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/location/data/WifiScan;->numDevices()I

    move-result v4

    if-gtz v4, :cond_1

    const-string v4, "TravelC"

    const-string v5, "Empty WiFi scan. Not enough signals to determine travel type"

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;

    invoke-direct {v1, p2, p1}, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;-><init>(Lcom/google/android/location/data/WifiScan;Lcom/google/android/location/data/NetworkLocation;)V

    invoke-direct {p0, v1}, Lcom/google/android/location/activity/TravelDetectionManager;->maybeUpdateHistory(Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;)V

    invoke-direct {p0, v1}, Lcom/google/android/location/activity/TravelDetectionManager;->getSignalsToUse(Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x3

    if-ge v4, v5, :cond_2

    const-string v4, "TravelC"

    const-string v5, "Not enough signals in history to determine travel type"

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/google/android/location/activity/TravelDetectionManager;->featureExtractor:Lcom/google/android/location/activity/FeatureExtractor;

    invoke-virtual {v4, v2}, Lcom/google/android/location/activity/FeatureExtractor;->computeFeatures(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/location/activity/TravelDetectionManager;->classifier:Lcom/google/android/location/activity/TravelClassifier;

    invoke-virtual {v4, v0}, Lcom/google/android/location/activity/TravelClassifier;->classify(Ljava/util/Map;)Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    move-result-object v3

    goto :goto_0
.end method

.method private getSignalsToUse(Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;)Ljava/util/List;
    .locals 3
    .param p1    # Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/location/activity/TravelDetectionManager;->signalHistory:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/location/activity/TravelDetectionManager;->signalHistory:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v1
.end method

.method private isLastResultUsable()Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/location/activity/TravelDetectionManager;->lastDetectedTravelDetectionTypeIsStale()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager;->lastTravelDetectionResult:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    sget-object v1, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private lastDetectedTravelDetectionTypeIsStale()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v0}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/location/activity/TravelDetectionManager;->lastTravelDetectionTime:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x57e40

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeUpdateHistory(Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;)V
    .locals 2
    .param p1    # Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;

    iget-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager;->signalHistory:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager;->signalHistory:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/google/android/location/activity/TravelDetectionManager;->signalIsSignificantlyNewer(Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager;->signalHistory:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager;->signalHistory:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/location/activity/TravelDetectionManager;->signalHistory:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method private signalIsSignificantlyNewer(Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;Ljava/util/List;)Z
    .locals 8
    .param p1    # Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;",
            ">;)Z"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;->getWifiScan()Lcom/google/android/location/data/WifiScan;

    move-result-object v4

    iget-wide v0, v4, Lcom/google/android/location/data/WifiScan;->deliveryTime:J

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;

    invoke-virtual {v4}, Lcom/google/android/location/activity/TravelDetectionManager$ClassificationSignals;->getWifiScan()Lcom/google/android/location/data/WifiScan;

    move-result-object v4

    iget-wide v2, v4, Lcom/google/android/location/data/WifiScan;->deliveryTime:J

    sub-long v4, v0, v2

    const-wide/32 v6, 0xd6d8

    cmp-long v4, v4, v6

    if-ltz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public detectTravelType(Lcom/google/android/location/data/NetworkLocation;Lcom/google/android/location/data/WifiScan;)Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;
    .locals 9
    .param p1    # Lcom/google/android/location/data/NetworkLocation;
    .param p2    # Lcom/google/android/location/data/WifiScan;

    invoke-direct {p0, p1, p2}, Lcom/google/android/location/activity/TravelDetectionManager;->detectTravelTypeUsingAllSignals(Lcom/google/android/location/data/NetworkLocation;Lcom/google/android/location/data/WifiScan;)Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/location/activity/TravelDetectionManager;->cellDetector:Lcom/google/android/location/activity/CellTravelDetector;

    if-nez p1, :cond_2

    const/4 v5, 0x0

    :goto_0
    invoke-virtual {v6, v5}, Lcom/google/android/location/activity/CellTravelDetector;->detectTravelType(Lcom/google/android/location/data/CellLocatorResult;)Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    move-result-object v1

    sget-object v5, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    if-ne v0, v5, :cond_3

    sget-object v5, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    if-eq v1, v5, :cond_3

    move-object v4, v1

    :goto_1
    iget-object v5, p0, Lcom/google/android/location/activity/TravelDetectionManager;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v5}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v2

    sget-object v5, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    if-ne v4, v5, :cond_4

    invoke-direct {p0}, Lcom/google/android/location/activity/TravelDetectionManager;->isLastResultUsable()Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "TravelC"

    const-string v6, "Using previous result since current is unknown"

    invoke-static {v5, v6}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/location/activity/TravelDetectionManager;->lastTravelDetectionResult:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    :goto_2
    const-string v5, "TravelC"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Travel detection result: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/location/activity/TravelDetectionManager;->lastKnownTravelType:Lcom/google/android/location/data/TravelDetectionType;

    if-eqz v5, :cond_0

    sget-object v5, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    if-eq v4, v5, :cond_0

    iget-object v5, p0, Lcom/google/android/location/activity/TravelDetectionManager;->lastKnownTravelType:Lcom/google/android/location/data/TravelDetectionType;

    invoke-virtual {v4}, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;->getType()Lcom/google/android/location/data/TravelDetectionType;

    move-result-object v6

    if-eq v5, v6, :cond_0

    iget-wide v5, p0, Lcom/google/android/location/activity/TravelDetectionManager;->lastTransitionTime:J

    sub-long v5, v2, v5

    const-wide/32 v7, 0xc350

    cmp-long v5, v5, v7

    if-lez v5, :cond_7

    const-string v5, "TravelC"

    const-string v6, "State transition detected"

    invoke-static {v5, v6}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput-wide v2, p0, Lcom/google/android/location/activity/TravelDetectionManager;->lastTransitionTime:J

    :cond_0
    :goto_3
    sget-object v5, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    if-eq v4, v5, :cond_1

    invoke-virtual {v4}, Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;->getType()Lcom/google/android/location/data/TravelDetectionType;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/location/activity/TravelDetectionManager;->lastKnownTravelType:Lcom/google/android/location/data/TravelDetectionType;

    :cond_1
    return-object v4

    :cond_2
    iget-object v5, p1, Lcom/google/android/location/data/NetworkLocation;->cellResult:Lcom/google/android/location/data/CellLocatorResult;

    goto :goto_0

    :cond_3
    move-object v4, v0

    goto :goto_1

    :cond_4
    sget-object v5, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    if-ne v0, v5, :cond_5

    iget-boolean v5, p0, Lcom/google/android/location/activity/TravelDetectionManager;->isLastTravelResultFromAllSignalsProvider:Z

    if-eqz v5, :cond_5

    invoke-direct {p0}, Lcom/google/android/location/activity/TravelDetectionManager;->isLastResultUsable()Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v5, "TravelC"

    const-string v6, "Using previous result since it\'s from a better provider."

    invoke-static {v5, v6}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/location/activity/TravelDetectionManager;->lastTravelDetectionResult:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    goto :goto_2

    :cond_5
    if-ne v4, v0, :cond_6

    const/4 v5, 0x1

    :goto_4
    iput-boolean v5, p0, Lcom/google/android/location/activity/TravelDetectionManager;->isLastTravelResultFromAllSignalsProvider:Z

    iput-object v4, p0, Lcom/google/android/location/activity/TravelDetectionManager;->lastTravelDetectionResult:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    iput-wide v2, p0, Lcom/google/android/location/activity/TravelDetectionManager;->lastTravelDetectionTime:J

    goto :goto_2

    :cond_6
    const/4 v5, 0x0

    goto :goto_4

    :cond_7
    const-string v5, "TravelC"

    const-string v6, "Ignoring travel detection for now as a state transition just happened."

    invoke-static {v5, v6}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/location/activity/TravelDetectionManager;->UNKNOWN_RESULT:Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    goto :goto_3
.end method
