.class public Lcom/google/android/location/cache/CacheUpdater;
.super Ljava/lang/Object;
.source "CacheUpdater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/cache/CacheUpdater$1;,
        Lcom/google/android/location/cache/CacheUpdater$State;
    }
.end annotation


# static fields
.field static final DUMMY_PRIMARY_CELL:Lcom/google/gmm/common/io/protocol/ProtoBuf;

.field static final REFRESH_RETRY_INTERVAL_MILLIS:J


# instance fields
.field final cellToUpdate:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/gmm/common/io/protocol/ProtoBuf;",
            ">;"
        }
    .end annotation
.end field

.field maxDevicesPerRefill:I

.field final nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

.field numSuccessRpcs:I

.field numTotalRpcs:I

.field final os:Lcom/google/android/location/os/Os;

.field final persistentState:Lcom/google/android/location/cache/PersistentState;

.field final seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

.field state:Lcom/google/android/location/cache/CacheUpdater$State;

.field wakeLockAcquired:Z

.field final wifiToUpdate:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/gmm/common/io/protocol/ProtoBuf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const-wide/32 v2, 0x2932e00

    const/4 v4, -0x1

    const-wide/32 v0, 0x5265c00

    invoke-static {v2, v3, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/location/cache/CacheUpdater;->REFRESH_RETRY_INTERVAL_MILLIS:J

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCELL:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    sput-object v0, Lcom/google/android/location/cache/CacheUpdater;->DUMMY_PRIMARY_CELL:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/android/location/cache/CacheUpdater;->DUMMY_PRIMARY_CELL:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    sget-object v0, Lcom/google/android/location/cache/CacheUpdater;->DUMMY_PRIMARY_CELL:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/cache/PersistentState;Lcom/google/android/location/cache/SeenDevicesCache;Lcom/google/android/location/os/NlpParametersState;)V
    .locals 2
    .param p1    # Lcom/google/android/location/os/Os;
    .param p2    # Lcom/google/android/location/cache/PersistentState;
    .param p3    # Lcom/google/android/location/cache/SeenDevicesCache;
    .param p4    # Lcom/google/android/location/os/NlpParametersState;

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/cache/CacheUpdater;->cellToUpdate:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/cache/CacheUpdater;->wifiToUpdate:Ljava/util/List;

    iput v1, p0, Lcom/google/android/location/cache/CacheUpdater;->maxDevicesPerRefill:I

    iput v1, p0, Lcom/google/android/location/cache/CacheUpdater;->numTotalRpcs:I

    iput v1, p0, Lcom/google/android/location/cache/CacheUpdater;->numSuccessRpcs:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/cache/CacheUpdater;->wakeLockAcquired:Z

    sget-object v0, Lcom/google/android/location/cache/CacheUpdater$State;->IDLE:Lcom/google/android/location/cache/CacheUpdater$State;

    iput-object v0, p0, Lcom/google/android/location/cache/CacheUpdater;->state:Lcom/google/android/location/cache/CacheUpdater$State;

    iput-object p1, p0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    iput-object p2, p0, Lcom/google/android/location/cache/CacheUpdater;->persistentState:Lcom/google/android/location/cache/PersistentState;

    iput-object p3, p0, Lcom/google/android/location/cache/CacheUpdater;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    iput-object p4, p0, Lcom/google/android/location/cache/CacheUpdater;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    return-void
.end method

.method private acquireWakeLock()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/location/cache/CacheUpdater;->wakeLockAcquired:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v0, v1}, Lcom/google/android/location/os/Os;->wakeLockAcquire(I)V

    iput-boolean v1, p0, Lcom/google/android/location/cache/CacheUpdater;->wakeLockAcquired:Z

    :cond_0
    return-void
.end method

.method private createWifiDeviceProtoBuf(JI)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 3
    .param p1    # J
    .param p3    # I

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/protocol/LocserverMessageTypes;->GWIFI_DEVICE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setLong(IJ)V

    const/4 v1, 0x1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    if-lez p3, :cond_0

    const/16 v1, 0xa

    invoke-virtual {v0, v1, p3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_0
    return-object v0
.end method

.method private finishCurrentRefresh()V
    .locals 7

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget v3, p0, Lcom/google/android/location/cache/CacheUpdater;->numTotalRpcs:I

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/google/android/location/cache/CacheUpdater;->numSuccessRpcs:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/location/cache/CacheUpdater;->numTotalRpcs:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    const v4, 0x3f333333

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    iget v3, p0, Lcom/google/android/location/cache/CacheUpdater;->numTotalRpcs:I

    if-nez v3, :cond_2

    const-string v3, "CacheUpdater"

    const-string v4, "Cache up-to-date. Wifi database: %d, Cell database: %d"

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/location/cache/CacheUpdater;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    invoke-virtual {v6}, Lcom/google/android/location/os/NlpParametersState;->getWifiDatabaseVersion()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    iget-object v1, p0, Lcom/google/android/location/cache/CacheUpdater;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    invoke-virtual {v1}, Lcom/google/android/location/os/NlpParametersState;->getCellDatabaseVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/location/cache/CacheUpdater;->setLastRefreshTime(Z)V

    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->prepareNextRefresh()V

    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->releaseWakeLock()V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const-string v3, "CacheUpdater"

    const-string v4, "Cache refreshed successfully. Wifi database: %d, Cell database: %d"

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/location/cache/CacheUpdater;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    invoke-virtual {v6}, Lcom/google/android/location/os/NlpParametersState;->getWifiDatabaseVersion()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    iget-object v1, p0, Lcom/google/android/location/cache/CacheUpdater;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    invoke-virtual {v1}, Lcom/google/android/location/os/NlpParametersState;->getCellDatabaseVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const-string v3, "CacheUpdater"

    const-string v4, "Failed to refrehs cache. Total RPCs: %d, successful RPC: %d"

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lcom/google/android/location/cache/CacheUpdater;->numTotalRpcs:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    iget v1, p0, Lcom/google/android/location/cache/CacheUpdater;->numSuccessRpcs:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private mightStartRefresh()V
    .locals 2

    const-string v0, "CacheUpdater"

    const-string v1, "Starting a new refresh."

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/cache/CacheUpdater;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    invoke-virtual {v0}, Lcom/google/android/location/os/NlpParametersState;->isDefault()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->startRefreshNlpParams()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->startRefreshCache()V

    goto :goto_0
.end method

.method private onFailedToGetNlpParams()V
    .locals 2

    const-string v0, "CacheUpdater"

    const-string v1, "Failed to get latest NlpParameters."

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/location/cache/CacheUpdater$State;->IDLE:Lcom/google/android/location/cache/CacheUpdater$State;

    iput-object v0, p0, Lcom/google/android/location/cache/CacheUpdater;->state:Lcom/google/android/location/cache/CacheUpdater$State;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/cache/CacheUpdater;->setLastRefreshTime(Z)V

    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->prepareNextRefresh()V

    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->releaseWakeLock()V

    return-void
.end method

.method private prepareNextRefresh()V
    .locals 5

    const/4 v3, -0x1

    iget-object v2, p0, Lcom/google/android/location/cache/CacheUpdater;->cellToUpdate:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/google/android/location/cache/CacheUpdater;->wifiToUpdate:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iput v3, p0, Lcom/google/android/location/cache/CacheUpdater;->maxDevicesPerRefill:I

    iput v3, p0, Lcom/google/android/location/cache/CacheUpdater;->numTotalRpcs:I

    iput v3, p0, Lcom/google/android/location/cache/CacheUpdater;->numSuccessRpcs:I

    sget-object v2, Lcom/google/android/location/cache/CacheUpdater$State;->IDLE:Lcom/google/android/location/cache/CacheUpdater$State;

    iput-object v2, p0, Lcom/google/android/location/cache/CacheUpdater;->state:Lcom/google/android/location/cache/CacheUpdater$State;

    invoke-virtual {p0}, Lcom/google/android/location/cache/CacheUpdater;->getNextAlarmTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    const/4 v3, 0x1

    invoke-interface {v2, v3, v0, v1}, Lcom/google/android/location/os/Os;->alarmReset(IJ)V

    const-string v2, "CacheUpdater"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cache refresh scheduled at "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private releaseWakeLock()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/location/cache/CacheUpdater;->wakeLockAcquired:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/location/os/Os;->wakeLockRelease(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/cache/CacheUpdater;->wakeLockAcquired:Z

    :cond_0
    return-void
.end method

.method private setLastRefreshTime(Z)V
    .locals 3
    .param p1    # Z

    iget-object v2, p0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v2}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/location/cache/CacheUpdater;->persistentState:Lcom/google/android/location/cache/PersistentState;

    invoke-virtual {v2, v0, v1, p1}, Lcom/google/android/location/cache/PersistentState;->setCacheRefreshMillisSinceBoot(JZ)V

    iget-object v2, p0, Lcom/google/android/location/cache/CacheUpdater;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    invoke-virtual {v2, v0, v1, p1}, Lcom/google/android/location/cache/SeenDevicesCache;->setLastRefreshMillisSinceBoot(JZ)V

    return-void
.end method

.method private startRefreshCache()V
    .locals 9

    iget-object v1, p0, Lcom/google/android/location/cache/CacheUpdater;->persistentState:Lcom/google/android/location/cache/PersistentState;

    invoke-virtual {v1}, Lcom/google/android/location/cache/PersistentState;->getCellCache()Lcom/google/android/location/cache/TemporalCache;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/cache/CacheUpdater;->persistentState:Lcom/google/android/location/cache/PersistentState;

    invoke-virtual {v2}, Lcom/google/android/location/cache/PersistentState;->getWifiCache()Lcom/google/android/location/cache/TemporalCache;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/location/cache/CacheUpdater;->fillUpdateList(Lcom/google/android/location/cache/TemporalCache;Lcom/google/android/location/cache/TemporalCache;)V

    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->updateCacheOrFinishCurrentRefresh()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->acquireWakeLock()V

    iget-object v1, p0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v3}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v3

    iget v5, p0, Lcom/google/android/location/cache/CacheUpdater;->numTotalRpcs:I

    int-to-long v5, v5

    const-wide/16 v7, 0x2710

    mul-long/2addr v5, v7

    add-long/2addr v3, v5

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/location/os/Os;->alarmReset(IJ)V

    sget-object v1, Lcom/google/android/location/cache/CacheUpdater$State;->REFRESHING_CACHE:Lcom/google/android/location/cache/CacheUpdater$State;

    iput-object v1, p0, Lcom/google/android/location/cache/CacheUpdater;->state:Lcom/google/android/location/cache/CacheUpdater$State;

    :cond_0
    return-void
.end method

.method private startRefreshNlpParams()V
    .locals 7

    const-string v1, "CacheUpdater"

    const-string v2, "NlpParameters expired, need to sync with server."

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    iget-object v1, p0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v1, v0}, Lcom/google/android/location/os/Os;->glsDeviceLocationQuery(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->acquireWakeLock()V

    iget-object v1, p0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v3}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v3

    const-wide/16 v5, 0x2710

    add-long/2addr v3, v5

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/location/os/Os;->alarmReset(IJ)V

    sget-object v1, Lcom/google/android/location/cache/CacheUpdater$State;->REFRESHING_NLP_PARAMS:Lcom/google/android/location/cache/CacheUpdater$State;

    iput-object v1, p0, Lcom/google/android/location/cache/CacheUpdater;->state:Lcom/google/android/location/cache/CacheUpdater$State;

    return-void
.end method

.method private updateCache()Z
    .locals 14

    const/4 v13, 0x4

    const/4 v11, 0x0

    const/4 v12, 0x2

    const/4 v10, 0x1

    const/4 v4, 0x0

    iget-object v9, p0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v9}, Lcom/google/android/location/os/Os;->millisSinceEpoch()J

    move-result-wide v2

    const/4 v0, 0x0

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/location/cache/CacheUpdater;->cellToUpdate:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_0

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v9, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCELLULAR_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    sget-object v9, Lcom/google/android/location/cache/CacheUpdater;->DUMMY_PRIMARY_CELL:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v10, v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v12, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setLong(IJ)V

    iget-object v9, p0, Lcom/google/android/location/cache/CacheUpdater;->cellToUpdate:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    iget v9, p0, Lcom/google/android/location/cache/CacheUpdater;->maxDevicesPerRefill:I

    if-ge v4, v9, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v13, v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    iget v9, p0, Lcom/google/android/location/cache/CacheUpdater;->maxDevicesPerRefill:I

    if-ge v4, v9, :cond_1

    iget-object v9, p0, Lcom/google/android/location/cache/CacheUpdater;->wifiToUpdate:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_1

    new-instance v8, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v9, Lcom/google/android/location/protocol/LocserverMessageTypes;->GWIFI_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v8, v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v8, v10, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setLong(IJ)V

    iget-object v9, p0, Lcom/google/android/location/cache/CacheUpdater;->wifiToUpdate:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    iget v9, p0, Lcom/google/android/location/cache/CacheUpdater;->maxDevicesPerRefill:I

    if-ge v4, v9, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {v8, v12, v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    if-lez v4, :cond_5

    move v7, v10

    :goto_2
    if-eqz v7, :cond_4

    new-instance v5, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v9, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v5, v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    new-instance v6, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v9, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST_ELEMENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v6, v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    if-eqz v0, :cond_2

    invoke-virtual {v6, v10, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    :cond_2
    if-eqz v8, :cond_3

    invoke-virtual {v6, v12, v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    :cond_3
    invoke-virtual {v5, v13, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    iget-object v9, p0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v9, v5}, Lcom/google/android/location/os/Os;->glsDeviceLocationQuery(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    const-string v9, "CacheUpdater"

    const-string v12, "Sent %d cache items for refresh."

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v10, v11

    invoke-static {v12, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return v7

    :cond_5
    move v7, v11

    goto :goto_2
.end method

.method private updateCacheOrFinishCurrentRefresh()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->updateCache()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->finishCurrentRefresh()V

    :cond_0
    return v0
.end method


# virtual methods
.method public alarmRing(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/location/cache/CacheUpdater$1;->$SwitchMap$com$google$android$location$cache$CacheUpdater$State:[I

    iget-object v1, p0, Lcom/google/android/location/cache/CacheUpdater;->state:Lcom/google/android/location/cache/CacheUpdater$State;

    invoke-virtual {v1}, Lcom/google/android/location/cache/CacheUpdater$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->mightStartRefresh()V

    goto :goto_0

    :pswitch_1
    const-string v0, "CacheUpdater"

    const-string v1, "CacheUpdater terminated early when refreshing parameters."

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->onFailedToGetNlpParams()V

    goto :goto_0

    :pswitch_2
    const-string v0, "CacheUpdater"

    const-string v1, "CacheUpdater terminated early when refreshing cache."

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->finishCurrentRefresh()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method fillUpdateList(Lcom/google/android/location/cache/TemporalCache;Lcom/google/android/location/cache/TemporalCache;)V
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/cache/TemporalCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/location/data/Position;",
            ">;",
            "Lcom/google/android/location/cache/TemporalCache",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->cellToUpdate:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->clear()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->wifiToUpdate:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->clear()V

    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/location/cache/CacheUpdater;->numSuccessRpcs:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Lcom/google/android/location/os/Os;->millisSinceEpoch()J

    move-result-wide v19

    if-eqz p1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/location/os/NlpParametersState;->getCellDatabaseVersion()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/location/cache/TemporalCache;->entrySet()Ljava/util/Set;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/location/cache/CacheResult;

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/location/cache/CacheResult;->getDatabaseVersion()I

    move-result v22

    move/from16 v0, v22

    if-ge v0, v11, :cond_1

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/google/android/location/cache/PersistentState;->cellCacheKeyToGCell(Ljava/lang/String;)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v9

    if-eqz v9, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->cellToUpdate:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/location/cache/CacheResult;

    move-object/from16 v0, v22

    move-wide/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/cache/CacheResult;->setReadingTime(J)V

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/location/os/NlpParametersState;->getWifiDatabaseVersion()I

    move-result v12

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    if-eqz p2, :cond_5

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/location/cache/TemporalCache;->entrySet()Ljava/util/Set;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/cache/CacheResult;

    invoke-virtual {v4}, Lcom/google/android/location/cache/CacheResult;->getPosition()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/android/location/data/WifiApPosition;

    invoke-virtual {v4}, Lcom/google/android/location/cache/CacheResult;->getDatabaseVersion()I

    move-result v22

    move/from16 v0, v22

    if-lt v0, v12, :cond_3

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/location/data/WifiApPosition;->isOutlier()Z

    move-result v22

    if-eqz v22, :cond_4

    :cond_3
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Long;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/location/data/WifiApPosition;->getOutlierCount()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->wifiToUpdate:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v13, v14, v1}, Lcom/google/android/location/cache/CacheUpdater;->createWifiDeviceProtoBuf(JI)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v23

    invoke-interface/range {v22 .. v23}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/location/cache/CacheResult;

    move-object/from16 v0, v22

    move-wide/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/cache/CacheResult;->setReadingTime(J)V

    goto :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    move-object/from16 v22, v0

    if-eqz v22, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/location/cache/SeenDevicesCache;->entrySet()Ljava/util/Set;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_6
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Long;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->getDatabaseVersion()I

    move-result v22

    move/from16 v0, v22

    if-ge v0, v12, :cond_7

    const/4 v8, 0x1

    :goto_3
    if-eqz v8, :cond_8

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->wifiToUpdate:Ljava/util/List;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v13, v14, v1}, Lcom/google/android/location/cache/CacheUpdater;->createWifiDeviceProtoBuf(JI)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v23

    invoke-interface/range {v22 .. v23}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    const/4 v8, 0x0

    goto :goto_3

    :cond_8
    if-nez v8, :cond_6

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;

    move-object/from16 v0, v22

    move-wide v1, v15

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/cache/SeenDevicesCache$SeenWifiAp;->setReadingTime(J)V

    goto :goto_2

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/location/os/NlpParametersState;->getMaxCacheRefillEntrys()I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/location/cache/CacheUpdater;->maxDevicesPerRefill:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->cellToUpdate:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->wifiToUpdate:Ljava/util/List;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v23

    add-int v21, v22, v23

    move/from16 v0, v21

    int-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide/high16 v24, 0x3ff0000000000000L

    mul-double v22, v22, v24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/location/cache/CacheUpdater;->maxDevicesPerRefill:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-double v0, v0

    move-wide/from16 v24, v0

    div-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/location/cache/CacheUpdater;->numTotalRpcs:I

    if-lez v21, :cond_a

    const-string v22, "CacheUpdater"

    const-string v23, "Will refresh %d cell and %d WIFI."

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->cellToUpdate:Ljava/util/List;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/location/cache/CacheUpdater;->wifiToUpdate:Ljava/util/List;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    return-void
.end method

.method getNextAlarmTime()J
    .locals 7

    iget-object v4, p0, Lcom/google/android/location/cache/CacheUpdater;->persistentState:Lcom/google/android/location/cache/PersistentState;

    invoke-virtual {v4}, Lcom/google/android/location/cache/PersistentState;->getCacheRefreshMillisSinceBoot()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/location/cache/CacheUpdater;->persistentState:Lcom/google/android/location/cache/PersistentState;

    invoke-virtual {v6}, Lcom/google/android/location/cache/PersistentState;->getLastRefreshIsSuccess()Z

    move-result v6

    invoke-virtual {p0, v4, v5, v6}, Lcom/google/android/location/cache/CacheUpdater;->getNextAlarmTime(JZ)J

    move-result-wide v0

    iget-object v4, p0, Lcom/google/android/location/cache/CacheUpdater;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    invoke-virtual {v4}, Lcom/google/android/location/cache/SeenDevicesCache;->getLastRefreshMillisSinceBoot()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/location/cache/CacheUpdater;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    invoke-virtual {v6}, Lcom/google/android/location/cache/SeenDevicesCache;->getLastRefreshIsSuccess()Z

    move-result v6

    invoke-virtual {p0, v4, v5, v6}, Lcom/google/android/location/cache/CacheUpdater;->getNextAlarmTime(JZ)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    return-wide v4
.end method

.method getNextAlarmTime(JZ)J
    .locals 10
    .param p1    # J
    .param p3    # Z

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v6

    const-wide/high16 v8, 0x3fe0000000000000L

    sub-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L

    mul-double/2addr v6, v8

    const-wide v8, 0x41224f8000000000L

    mul-double/2addr v6, v8

    double-to-long v4, v6

    if-eqz p3, :cond_0

    const-wide/32 v2, 0x5265c00

    :goto_0
    add-long v6, p1, v2

    add-long v0, v6, v4

    return-wide v0

    :cond_0
    sget-wide v2, Lcom/google/android/location/cache/CacheUpdater;->REFRESH_RETRY_INTERVAL_MILLIS:J

    goto :goto_0
.end method

.method public glsDeviceLocationResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 4
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v2, 0x5

    iget-object v0, p0, Lcom/google/android/location/cache/CacheUpdater;->state:Lcom/google/android/location/cache/CacheUpdater$State;

    sget-object v1, Lcom/google/android/location/cache/CacheUpdater$State;->REFRESHING_NLP_PARAMS:Lcom/google/android/location/cache/CacheUpdater$State;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/location/os/Os;->alarmCancel(I)V

    invoke-static {p1}, Lcom/google/android/location/utils/Utils;->isSuccessfulRpc(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/cache/CacheUpdater;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    invoke-virtual {p1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/NlpParametersState;->setParameters(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->startRefreshCache()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->onFailedToGetNlpParams()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/cache/CacheUpdater;->state:Lcom/google/android/location/cache/CacheUpdater$State;

    sget-object v1, Lcom/google/android/location/cache/CacheUpdater$State;->REFRESHING_CACHE:Lcom/google/android/location/cache/CacheUpdater$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/location/cache/CacheUpdater;->persistentState:Lcom/google/android/location/cache/PersistentState;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/location/cache/CacheUpdater;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v2}, Lcom/google/android/location/os/Os;->millisSinceEpoch()J

    move-result-wide v2

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/location/cache/PersistentState;->updateCachesFromGlsQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;ZJ)V

    iget-object v0, p0, Lcom/google/android/location/cache/CacheUpdater;->seenDevicesCache:Lcom/google/android/location/cache/SeenDevicesCache;

    invoke-virtual {v0, p1}, Lcom/google/android/location/cache/SeenDevicesCache;->updateCachesFromGlsQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    invoke-static {p1}, Lcom/google/android/location/utils/Utils;->isSuccessfulRpc(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/location/cache/CacheUpdater;->numSuccessRpcs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/location/cache/CacheUpdater;->numSuccessRpcs:I

    :cond_3
    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->updateCacheOrFinishCurrentRefresh()Z

    goto :goto_0
.end method

.method public initialize()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/cache/CacheUpdater;->prepareNextRefresh()V

    return-void
.end method
