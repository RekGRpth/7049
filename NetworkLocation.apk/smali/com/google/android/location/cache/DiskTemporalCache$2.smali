.class Lcom/google/android/location/cache/DiskTemporalCache$2;
.super Ljava/lang/Object;
.source "DiskTemporalCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/cache/DiskTemporalCache;->insertValue(Ljava/lang/Object;Lcom/google/gmm/common/io/protocol/ProtoBuf;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/location/cache/DiskTemporalCache;

.field final synthetic val$fileName:Lcom/google/android/location/data/TemporalObject;

.field final synthetic val$key:Ljava/lang/Object;

.field final synthetic val$proto:Lcom/google/gmm/common/io/protocol/ProtoBuf;


# direct methods
.method constructor <init>(Lcom/google/android/location/cache/DiskTemporalCache;Lcom/google/android/location/data/TemporalObject;Lcom/google/gmm/common/io/protocol/ProtoBuf;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    iput-object p2, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->val$fileName:Lcom/google/android/location/data/TemporalObject;

    iput-object p3, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->val$proto:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iput-object p4, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->val$key:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    new-instance v1, Ljava/io/File;

    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->cacheDir:Ljava/io/File;
    invoke-static {v4}, Lcom/google/android/location/cache/DiskTemporalCache;->access$200(Lcom/google/android/location/cache/DiskTemporalCache;)Ljava/io/File;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->val$fileName:Lcom/google/android/location/data/TemporalObject;

    invoke-virtual {v4}, Lcom/google/android/location/data/TemporalObject;->getWithoutUpdatingLastUsedTime()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v1, v5, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->fileSystem:Lcom/google/android/location/os/FileSystem;
    invoke-static {v4}, Lcom/google/android/location/cache/DiskTemporalCache;->access$700(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/os/FileSystem;

    move-result-object v4

    invoke-interface {v4, v1}, Lcom/google/android/location/os/FileSystem;->makeFilePrivate(Ljava/io/File;)V

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->secretKey:Ljavax/crypto/SecretKey;
    invoke-static {v4}, Lcom/google/android/location/cache/DiskTemporalCache;->access$300(Lcom/google/android/location/cache/DiskTemporalCache;)Ljavax/crypto/SecretKey;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/location/os/CipherStreams;->newBufferedCipherOutputStream(Ljava/io/OutputStream;Ljavax/crypto/SecretKey;)Ljava/io/OutputStream;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->val$proto:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {v4, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    const-string v4, "DiskTemporalCache"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Successfully wrote element "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->val$key:Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to disk"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    # invokes: Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V
    invoke-static {v3}, Lcom/google/android/location/cache/DiskTemporalCache;->access$600(Ljava/io/Closeable;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v4, "DiskTemporalCache"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "The file that we were trying to write was not found: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    monitor-enter p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;
    invoke-static {v4}, Lcom/google/android/location/cache/DiskTemporalCache;->access$100(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/cache/TemporalLRUCache;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->val$key:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Lcom/google/android/location/cache/TemporalLRUCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;
    invoke-static {v4}, Lcom/google/android/location/cache/DiskTemporalCache;->access$500(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->val$key:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    # invokes: Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V
    invoke-static {v3}, Lcom/google/android/location/cache/DiskTemporalCache;->access$600(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v4

    # invokes: Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V
    invoke-static {v3}, Lcom/google/android/location/cache/DiskTemporalCache;->access$600(Ljava/io/Closeable;)V

    throw v4

    :catch_1
    move-exception v0

    :try_start_5
    const-string v4, "DiskTemporalCache"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException while writing proto to disk: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    monitor-enter p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;
    invoke-static {v4}, Lcom/google/android/location/cache/DiskTemporalCache;->access$100(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/cache/TemporalLRUCache;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->val$key:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Lcom/google/android/location/cache/TemporalLRUCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->this$0:Lcom/google/android/location/cache/DiskTemporalCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;
    invoke-static {v4}, Lcom/google/android/location/cache/DiskTemporalCache;->access$500(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/location/cache/DiskTemporalCache$2;->val$key:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    # invokes: Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V
    invoke-static {v3}, Lcom/google/android/location/cache/DiskTemporalCache;->access$600(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_2
    move-exception v4

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1
.end method
