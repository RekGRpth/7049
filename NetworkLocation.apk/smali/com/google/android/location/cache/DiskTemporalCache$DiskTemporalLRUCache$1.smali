.class Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache$1;
.super Ljava/lang/Object;
.source "DiskTemporalCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->onRemoveEntry(Ljava/util/Map$Entry;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

.field final synthetic val$fileName:Lcom/google/android/location/data/TemporalObject;


# direct methods
.method constructor <init>(Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;Lcom/google/android/location/data/TemporalObject;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache$1;->this$0:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    iput-object p2, p0, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache$1;->val$fileName:Lcom/google/android/location/data/TemporalObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const-string v1, ""

    iget-object v2, p0, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache$1;->val$fileName:Lcom/google/android/location/data/TemporalObject;

    invoke-virtual {v2}, Lcom/google/android/location/data/TemporalObject;->getWithoutUpdatingLastUsedTime()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache$1;->this$0:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    # getter for: Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->cacheDir:Ljava/io/File;
    invoke-static {v1}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->access$000(Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;)Ljava/io/File;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache$1;->val$fileName:Lcom/google/android/location/data/TemporalObject;

    invoke-virtual {v1}, Lcom/google/android/location/data/TemporalObject;->getWithoutUpdatingLastUsedTime()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v0, v2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method
