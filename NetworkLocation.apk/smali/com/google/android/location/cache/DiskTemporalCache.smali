.class public Lcom/google/android/location/cache/DiskTemporalCache;
.super Ljava/lang/Object;
.source "DiskTemporalCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final cacheDir:Ljava/io/File;

.field private executor:Ljava/util/concurrent/ExecutorService;

.field private final fileSystem:Lcom/google/android/location/os/FileSystem;

.field private final inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/TemporalLRUCache",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final protoFactory:Lcom/google/android/location/data/ProtoFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/data/ProtoFactory",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final saver:Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver",
            "<TK;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final secretKey:Ljavax/crypto/SecretKey;


# direct methods
.method public constructor <init>(IILjava/util/concurrent/ExecutorService;Lcom/google/android/location/data/ProtoFactory;Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;Ljava/io/File;Ljavax/crypto/SecretKey;Lcom/google/android/location/os/FileSystem;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/util/concurrent/ExecutorService;
    .param p6    # Ljava/io/File;
    .param p7    # Ljavax/crypto/SecretKey;
    .param p8    # Lcom/google/android/location/os/FileSystem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/google/android/location/data/ProtoFactory",
            "<TV;>;",
            "Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver",
            "<TK;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            "Ljavax/crypto/SecretKey;",
            "Lcom/google/android/location/os/FileSystem;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-lt p1, p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Memory capacity is expected to be larger than disk capacity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p4, p0, Lcom/google/android/location/cache/DiskTemporalCache;->protoFactory:Lcom/google/android/location/data/ProtoFactory;

    iput-object p6, p0, Lcom/google/android/location/cache/DiskTemporalCache;->cacheDir:Ljava/io/File;

    iput-object p7, p0, Lcom/google/android/location/cache/DiskTemporalCache;->secretKey:Ljavax/crypto/SecretKey;

    iput-object p3, p0, Lcom/google/android/location/cache/DiskTemporalCache;->executor:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-direct {v0, p1}, Lcom/google/android/location/cache/TemporalLRUCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;

    if-eqz p7, :cond_1

    new-instance v0, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    invoke-direct {v0, p2, p3, p6}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;-><init>(ILjava/util/concurrent/ExecutorService;Ljava/io/File;)V

    iput-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    :goto_0
    iput-object p5, p0, Lcom/google/android/location/cache/DiskTemporalCache;->saver:Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;

    iput-object p8, p0, Lcom/google/android/location/cache/DiskTemporalCache;->fileSystem:Lcom/google/android/location/os/FileSystem;

    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/cache/TemporalLRUCache;
    .locals 1
    .param p0    # Lcom/google/android/location/cache/DiskTemporalCache;

    iget-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/location/cache/DiskTemporalCache;)Ljava/io/File;
    .locals 1
    .param p0    # Lcom/google/android/location/cache/DiskTemporalCache;

    iget-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->cacheDir:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/location/cache/DiskTemporalCache;)Ljavax/crypto/SecretKey;
    .locals 1
    .param p0    # Lcom/google/android/location/cache/DiskTemporalCache;

    iget-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->secretKey:Ljavax/crypto/SecretKey;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/data/ProtoFactory;
    .locals 1
    .param p0    # Lcom/google/android/location/cache/DiskTemporalCache;

    iget-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->protoFactory:Lcom/google/android/location/data/ProtoFactory;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;
    .locals 1
    .param p0    # Lcom/google/android/location/cache/DiskTemporalCache;

    iget-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    return-object v0
.end method

.method static synthetic access$600(Ljava/io/Closeable;)V
    .locals 0
    .param p0    # Ljava/io/Closeable;

    invoke-static {p0}, Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/location/cache/DiskTemporalCache;)Lcom/google/android/location/os/FileSystem;
    .locals 1
    .param p0    # Lcom/google/android/location/cache/DiskTemporalCache;

    iget-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->fileSystem:Lcom/google/android/location/os/FileSystem;

    return-object v0
.end method

.method private static close(Ljava/io/Closeable;)V
    .locals 4
    .param p0    # Ljava/io/Closeable;

    if-eqz p0, :cond_0

    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "DiskTemporalCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while closing: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private deleteFilesInCacheDir()V
    .locals 6

    iget-object v5, p0, Lcom/google/android/location/cache/DiskTemporalCache;->cacheDir:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v1, v0

    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v2, v1, v3

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public deleteCacheFilesAndDir()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/cache/DiskTemporalCache;->deleteFilesInCacheDir()V

    iget-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->cacheDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    return-void
.end method

.method public declared-synchronized discardOldEntries(JJ)V
    .locals 1
    .param p1    # J
    .param p3    # J

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/cache/TemporalLRUCache;->discardOldEntries(JJ)V

    iget-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->secretKey:Ljavax/crypto/SecretKey;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->discardOldEntries(JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasValue(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)Z"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->secretKey:Ljavax/crypto/SecretKey;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    invoke-virtual {v0, p1}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-virtual {v0, p1}, Lcom/google/android/location/cache/TemporalLRUCache;->containsKey(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public insertValue(Ljava/lang/Object;Lcom/google/gmm/common/io/protocol/ProtoBuf;J)V
    .locals 4
    .param p2    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/google/gmm/common/io/protocol/ProtoBuf;",
            "J)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/location/cache/DiskTemporalCache;->secretKey:Ljavax/crypto/SecretKey;

    if-nez v2, :cond_1

    new-instance v1, Lcom/google/android/location/data/TemporalObject;

    iget-object v2, p0, Lcom/google/android/location/cache/DiskTemporalCache;->protoFactory:Lcom/google/android/location/data/ProtoFactory;

    invoke-virtual {v2, p2}, Lcom/google/android/location/data/ProtoFactory;->create(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v1, v2, p3, p4}, Lcom/google/android/location/data/TemporalObject;-><init>(Ljava/lang/Object;J)V

    iget-object v2, p0, Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-virtual {v2, p1, v1}, Lcom/google/android/location/cache/TemporalLRUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    invoke-virtual {v2, p1}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v1, Lcom/google/android/location/data/TemporalObject;

    iget-object v2, p0, Lcom/google/android/location/cache/DiskTemporalCache;->protoFactory:Lcom/google/android/location/data/ProtoFactory;

    invoke-virtual {v2, p2}, Lcom/google/android/location/data/ProtoFactory;->create(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v1, v2, p3, p4}, Lcom/google/android/location/data/TemporalObject;-><init>(Ljava/lang/Object;J)V

    new-instance v0, Lcom/google/android/location/data/TemporalObject;

    if-nez p2, :cond_2

    const-string v2, ""

    :goto_1
    invoke-direct {v0, v2, p3, p4}, Lcom/google/android/location/data/TemporalObject;-><init>(Ljava/lang/Object;J)V

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-virtual {v2, p1, v1}, Lcom/google/android/location/cache/TemporalLRUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    invoke-virtual {v2, p1, v0}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p2, :cond_3

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p3, p4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".cache"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/google/android/location/cache/DiskTemporalCache;->executor:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/google/android/location/cache/DiskTemporalCache$2;

    invoke-direct {v3, p0, v0, p2, p1}, Lcom/google/android/location/cache/DiskTemporalCache$2;-><init>(Lcom/google/android/location/cache/DiskTemporalCache;Lcom/google/android/location/data/TemporalObject;Lcom/google/gmm/common/io/protocol/ProtoBuf;Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized load()V
    .locals 14

    monitor-enter p0

    :try_start_0
    iget-object v11, p0, Lcom/google/android/location/cache/DiskTemporalCache;->secretKey:Ljavax/crypto/SecretKey;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v11, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v2, Ljava/io/File;

    iget-object v11, p0, Lcom/google/android/location/cache/DiskTemporalCache;->cacheDir:Ljava/io/File;

    const-string v12, "lru.cache"

    invoke-direct {v2, v11, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    :try_start_2
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iget-object v11, p0, Lcom/google/android/location/cache/DiskTemporalCache;->secretKey:Ljavax/crypto/SecretKey;

    invoke-static {v3, v11}, Lcom/google/android/location/os/CipherStreams;->newBufferedCipherInputStream(Ljava/io/InputStream;Ljavax/crypto/SecretKey;)Ljava/io/InputStream;

    move-result-object v0

    iget-object v11, p0, Lcom/google/android/location/cache/DiskTemporalCache;->saver:Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;

    iget-object v12, p0, Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    invoke-interface {v11, v12, v0}, Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;->load(Lcom/google/android/location/cache/TemporalLRUCache;Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-static {v0}, Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V

    :goto_0
    iget-object v11, p0, Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-virtual {v11}, Lcom/google/android/location/cache/TemporalLRUCache;->size()I

    move-result v11

    iget-object v12, p0, Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    invoke-virtual {v12}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->size()I

    move-result v12

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v9

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v9}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    invoke-virtual {v12}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->size()I

    move-result v12

    iget-object v13, p0, Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-virtual {v13}, Lcom/google/android/location/cache/TemporalLRUCache;->getCapacity()I

    move-result v13

    sub-int/2addr v12, v13

    invoke-static {v11, v12}, Ljava/lang/Math;->max(II)I

    move-result v10

    const/4 v8, 0x0

    iget-object v11, p0, Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    invoke-virtual {v11}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->keySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v5

    if-ge v8, v10, :cond_2

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_4
    const-string v11, "DiskTemporalCache"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "On disk LRU cache file not found: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/location/cache/DiskTemporalCache;->deleteFilesInCacheDir()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    invoke-static {v0}, Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v11

    monitor-exit p0

    throw v11

    :catch_1
    move-exception v1

    :try_start_6
    const-string v11, "DiskTemporalCache"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "IOException while reading LRU cache file: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v11, p0, Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    invoke-virtual {v11}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->clear()V

    invoke-direct {p0}, Lcom/google/android/location/cache/DiskTemporalCache;->deleteFilesInCacheDir()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    invoke-static {v0}, Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catchall_1
    move-exception v11

    invoke-static {v0}, Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V

    throw v11

    :cond_2
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    const-wide/16 v11, 0x0

    invoke-virtual {p0, v5, v11, v12}, Lcom/google/android/location/cache/DiskTemporalCache;->lookupValue(Ljava/lang/Object;J)Ljava/lang/Object;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2
.end method

.method public lookupValue(Ljava/lang/Object;J)Ljava/lang/Object;
    .locals 7
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;J)TV;"
        }
    .end annotation

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-virtual {v4, p1}, Lcom/google/android/location/cache/TemporalLRUCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/data/TemporalObject;

    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache;->secretKey:Ljavax/crypto/SecretKey;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    invoke-virtual {v4, p1}, Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/data/TemporalObject;

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/google/android/location/cache/DiskTemporalCache;->secretKey:Ljavax/crypto/SecretKey;

    if-eqz v3, :cond_0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/location/data/TemporalObject;->updateLastUsedTime(J)V

    :cond_0
    invoke-virtual {v2, p2, p3}, Lcom/google/android/location/data/TemporalObject;->get(J)Ljava/lang/Object;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    :cond_2
    if-eqz v0, :cond_4

    const-string v4, ""

    invoke-virtual {v0}, Lcom/google/android/location/data/TemporalObject;->getWithoutUpdatingLastUsedTime()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v1, Lcom/google/android/location/data/TemporalObject;

    invoke-virtual {v0}, Lcom/google/android/location/data/TemporalObject;->getCreationTimeInMillisSinceEpoch()J

    move-result-wide v4

    invoke-direct {v1, v3, v4, v5}, Lcom/google/android/location/data/TemporalObject;-><init>(Ljava/lang/Object;J)V

    monitor-enter p0

    :try_start_2
    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache;->inMemoryContents:Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-virtual {v4, p1, v1}, Lcom/google/android/location/cache/TemporalLRUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0

    goto :goto_1

    :catchall_1
    move-exception v3

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3

    :cond_3
    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache;->executor:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/google/android/location/cache/DiskTemporalCache$1;

    invoke-direct {v5, p0, p1, v0}, Lcom/google/android/location/cache/DiskTemporalCache$1;-><init>(Lcom/google/android/location/cache/DiskTemporalCache;Ljava/lang/Object;Lcom/google/android/location/data/TemporalObject;)V

    invoke-interface {v4, v5}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_1

    :cond_4
    const-string v4, "DiskTemporalCache"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Element for key "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " not in memory or on disk."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public save()V
    .locals 7

    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache;->secretKey:Ljavax/crypto/SecretKey;

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Ljava/io/File;

    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache;->cacheDir:Ljava/io/File;

    const-string v5, "lru.cache"

    invoke-direct {v2, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache;->fileSystem:Lcom/google/android/location/os/FileSystem;

    invoke-interface {v4, v2}, Lcom/google/android/location/os/FileSystem;->makeFilePrivate(Ljava/io/File;)V

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache;->secretKey:Ljavax/crypto/SecretKey;

    invoke-static {v3, v4}, Lcom/google/android/location/os/CipherStreams;->newBufferedCipherOutputStream(Ljava/io/OutputStream;Ljavax/crypto/SecretKey;)Ljava/io/OutputStream;

    move-result-object v0

    monitor-enter p0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v4, p0, Lcom/google/android/location/cache/DiskTemporalCache;->saver:Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;

    iget-object v5, p0, Lcom/google/android/location/cache/DiskTemporalCache;->onDiskContentsFiles:Lcom/google/android/location/cache/DiskTemporalCache$DiskTemporalLRUCache;

    invoke-interface {v4, v5, v0}, Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;->save(Lcom/google/android/location/cache/TemporalLRUCache;Ljava/io/OutputStream;)V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v0}, Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v4
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_0
    move-exception v1

    :try_start_4
    const-string v4, "DiskTemporalCache"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not find LRU cache file to write to: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    invoke-static {v0}, Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    :try_start_5
    const-string v4, "DiskTemporalCache"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException while writing LRU cache file: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    invoke-static {v0}, Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_1
    move-exception v4

    invoke-static {v0}, Lcom/google/android/location/cache/DiskTemporalCache;->close(Ljava/io/Closeable;)V

    throw v4
.end method
