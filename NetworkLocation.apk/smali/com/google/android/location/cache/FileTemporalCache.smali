.class public Lcom/google/android/location/cache/FileTemporalCache;
.super Ljava/lang/Object;
.source "FileTemporalCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final cacheFile:Ljava/io/File;

.field private final contents:Lcom/google/android/location/cache/TemporalLRUCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/TemporalLRUCache",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final fileSystem:Lcom/google/android/location/os/FileSystem;

.field private final saver:Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final secretKey:Ljavax/crypto/SecretKey;


# direct methods
.method public constructor <init>(ILcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;Ljava/io/File;Ljavax/crypto/SecretKey;Lcom/google/android/location/os/FileSystem;)V
    .locals 1
    .param p1    # I
    .param p3    # Ljava/io/File;
    .param p4    # Ljavax/crypto/SecretKey;
    .param p5    # Lcom/google/android/location/os/FileSystem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver",
            "<TK;TV;>;",
            "Ljava/io/File;",
            "Ljavax/crypto/SecretKey;",
            "Lcom/google/android/location/os/FileSystem;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-direct {v0, p1}, Lcom/google/android/location/cache/TemporalLRUCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/cache/FileTemporalCache;->contents:Lcom/google/android/location/cache/TemporalLRUCache;

    iput-object p2, p0, Lcom/google/android/location/cache/FileTemporalCache;->saver:Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;

    iput-object p3, p0, Lcom/google/android/location/cache/FileTemporalCache;->cacheFile:Ljava/io/File;

    iput-object p4, p0, Lcom/google/android/location/cache/FileTemporalCache;->secretKey:Ljavax/crypto/SecretKey;

    iput-object p5, p0, Lcom/google/android/location/cache/FileTemporalCache;->fileSystem:Lcom/google/android/location/os/FileSystem;

    return-void
.end method

.method private static close(Ljava/io/Closeable;)V
    .locals 4
    .param p0    # Ljava/io/Closeable;

    if-eqz p0, :cond_0

    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "FileTemporalCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while closing: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public deleteCacheFile()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/cache/FileTemporalCache;->cacheFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    return-void
.end method

.method public declared-synchronized discardOldEntries(JJ)V
    .locals 1
    .param p1    # J
    .param p3    # J

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/cache/FileTemporalCache;->contents:Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/cache/TemporalLRUCache;->discardOldEntries(JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized insertValue(Ljava/lang/Object;Ljava/lang/Object;J)V
    .locals 2
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;J)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/location/data/TemporalObject;

    invoke-direct {v0, p2, p3, p4}, Lcom/google/android/location/data/TemporalObject;-><init>(Ljava/lang/Object;J)V

    iget-object v1, p0, Lcom/google/android/location/cache/FileTemporalCache;->contents:Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/location/cache/TemporalLRUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized load()V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/location/cache/FileTemporalCache;->secretKey:Ljavax/crypto/SecretKey;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/google/android/location/cache/FileTemporalCache;->cacheFile:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iget-object v3, p0, Lcom/google/android/location/cache/FileTemporalCache;->secretKey:Ljavax/crypto/SecretKey;

    invoke-static {v2, v3}, Lcom/google/android/location/os/CipherStreams;->newBufferedCipherInputStream(Ljava/io/InputStream;Ljavax/crypto/SecretKey;)Ljava/io/InputStream;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/location/cache/FileTemporalCache;->saver:Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;

    iget-object v4, p0, Lcom/google/android/location/cache/FileTemporalCache;->contents:Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-interface {v3, v4, v0}, Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;->load(Lcom/google/android/location/cache/TemporalLRUCache;Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-static {v0}, Lcom/google/android/location/cache/FileTemporalCache;->close(Ljava/io/Closeable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :catch_0
    move-exception v1

    :try_start_3
    const-string v3, "FileTemporalCache"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "LRU cache file not found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-static {v0}, Lcom/google/android/location/cache/FileTemporalCache;->close(Ljava/io/Closeable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v1

    :try_start_5
    const-string v3, "FileTemporalCache"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException while reading LRU cache file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/location/cache/FileTemporalCache;->contents:Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-virtual {v3}, Lcom/google/android/location/cache/TemporalLRUCache;->clear()V

    iget-object v3, p0, Lcom/google/android/location/cache/FileTemporalCache;->cacheFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    invoke-static {v0}, Lcom/google/android/location/cache/FileTemporalCache;->close(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_1
    move-exception v3

    invoke-static {v0}, Lcom/google/android/location/cache/FileTemporalCache;->close(Ljava/io/Closeable;)V

    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public declared-synchronized lookupValue(Ljava/lang/Object;J)Ljava/lang/Object;
    .locals 2
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;J)TV;"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/cache/FileTemporalCache;->contents:Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-virtual {v1, p1}, Lcom/google/android/location/cache/TemporalLRUCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/data/TemporalObject;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/location/data/TemporalObject;->get(J)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :goto_0
    monitor-exit p0

    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public save()V
    .locals 6

    iget-object v3, p0, Lcom/google/android/location/cache/FileTemporalCache;->secretKey:Ljavax/crypto/SecretKey;

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/location/cache/FileTemporalCache;->cacheFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    iget-object v3, p0, Lcom/google/android/location/cache/FileTemporalCache;->fileSystem:Lcom/google/android/location/os/FileSystem;

    iget-object v4, p0, Lcom/google/android/location/cache/FileTemporalCache;->cacheFile:Ljava/io/File;

    invoke-interface {v3, v4}, Lcom/google/android/location/os/FileSystem;->makeFilePrivate(Ljava/io/File;)V

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, Lcom/google/android/location/cache/FileTemporalCache;->cacheFile:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iget-object v3, p0, Lcom/google/android/location/cache/FileTemporalCache;->secretKey:Ljavax/crypto/SecretKey;

    invoke-static {v2, v3}, Lcom/google/android/location/os/CipherStreams;->newBufferedCipherOutputStream(Ljava/io/OutputStream;Ljavax/crypto/SecretKey;)Ljava/io/OutputStream;

    move-result-object v0

    monitor-enter p0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v3, p0, Lcom/google/android/location/cache/FileTemporalCache;->saver:Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;

    iget-object v4, p0, Lcom/google/android/location/cache/FileTemporalCache;->contents:Lcom/google/android/location/cache/TemporalLRUCache;

    invoke-interface {v3, v4, v0}, Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;->save(Lcom/google/android/location/cache/TemporalLRUCache;Ljava/io/OutputStream;)V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v0}, Lcom/google/android/location/cache/FileTemporalCache;->close(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v3
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_0
    move-exception v1

    :try_start_4
    const-string v3, "FileTemporalCache"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not find LRU cache file to write to: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    invoke-static {v0}, Lcom/google/android/location/cache/FileTemporalCache;->close(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    :try_start_5
    const-string v3, "FileTemporalCache"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException while writing LRU cache file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/location/cache/FileTemporalCache;->cacheFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    invoke-static {v0}, Lcom/google/android/location/cache/FileTemporalCache;->close(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_1
    move-exception v3

    invoke-static {v0}, Lcom/google/android/location/cache/FileTemporalCache;->close(Ljava/io/Closeable;)V

    throw v3
.end method
