.class Lcom/google/android/location/cache/Stats;
.super Ljava/lang/Object;
.source "Stats.java"


# instance fields
.field private protoBuf:Lcom/google/gmm/common/io/protocol/ProtoBuf;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/location/cache/Stats;->zeroCounts()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/cache/Stats;->protoBuf:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    return-void
.end method

.method private increment(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/location/cache/Stats;->protoBuf:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/android/location/cache/Stats;->protoBuf:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, p1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    return-void
.end method

.method private static zeroCounts()Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCLIENT_CACHE_STATS:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    return-object v0
.end method


# virtual methods
.method public addCapacityEviction()V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/location/cache/Stats;->increment(I)V

    return-void
.end method

.method public addExpirationEviction()V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/location/cache/Stats;->increment(I)V

    return-void
.end method

.method public addInsertion()V
    .locals 1

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/location/cache/Stats;->increment(I)V

    return-void
.end method

.method public addLookup(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/android/location/cache/Stats;->increment(I)V

    if-eqz p1, :cond_0

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/location/cache/Stats;->increment(I)V

    :cond_0
    return-void
.end method

.method public extractAndReset(II)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/location/cache/Stats;->protoBuf:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    invoke-static {}, Lcom/google/android/location/cache/Stats;->zeroCounts()Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/cache/Stats;->protoBuf:Lcom/google/gmm/common/io/protocol/ProtoBuf;

    return-object v0
.end method
