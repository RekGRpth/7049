.class public Lcom/google/android/location/cache/TemporalCache;
.super Ljava/lang/Object;
.source "TemporalCache.java"

# interfaces
.implements Lcom/google/android/location/data/Persistent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/cache/TemporalCache$Contents;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "P:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/location/data/Persistent",
        "<",
        "Lcom/google/android/location/cache/TemporalCache",
        "<TK;TP;>;>;"
    }
.end annotation


# instance fields
.field final contents:Lcom/google/android/location/cache/TemporalCache$Contents;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/TemporalCache$Contents",
            "<TK;",
            "Lcom/google/android/location/cache/CacheResult",
            "<TP;>;>;"
        }
    .end annotation
.end field

.field private final keySaver:Lcom/google/android/location/data/Persistent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/data/Persistent",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final stats:Lcom/google/android/location/cache/Stats;

.field private final valueSaver:Lcom/google/android/location/data/Persistent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/data/Persistent",
            "<",
            "Lcom/google/android/location/cache/CacheResult",
            "<TP;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILcom/google/android/location/data/Persistent;Lcom/google/android/location/data/Persistent;)V
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/location/data/Persistent",
            "<TK;>;",
            "Lcom/google/android/location/data/Persistent",
            "<",
            "Lcom/google/android/location/cache/CacheResult",
            "<TP;>;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/location/cache/TemporalCache;->keySaver:Lcom/google/android/location/data/Persistent;

    iput-object p3, p0, Lcom/google/android/location/cache/TemporalCache;->valueSaver:Lcom/google/android/location/data/Persistent;

    new-instance v0, Lcom/google/android/location/cache/Stats;

    invoke-direct {v0}, Lcom/google/android/location/cache/Stats;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/cache/TemporalCache;->stats:Lcom/google/android/location/cache/Stats;

    new-instance v0, Lcom/google/android/location/cache/TemporalCache$Contents;

    iget-object v1, p0, Lcom/google/android/location/cache/TemporalCache;->stats:Lcom/google/android/location/cache/Stats;

    invoke-direct {v0, p1, v1}, Lcom/google/android/location/cache/TemporalCache$Contents;-><init>(ILcom/google/android/location/cache/Stats;)V

    iput-object v0, p0, Lcom/google/android/location/cache/TemporalCache;->contents:Lcom/google/android/location/cache/TemporalCache$Contents;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/cache/TemporalCache;->contents:Lcom/google/android/location/cache/TemporalCache$Contents;

    invoke-virtual {v0}, Lcom/google/android/location/cache/TemporalCache$Contents;->clear()V

    return-void
.end method

.method public discardOldEntries(JJ)V
    .locals 9
    .param p1    # J
    .param p3    # J

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v3, p0, Lcom/google/android/location/cache/TemporalCache;->contents:Lcom/google/android/location/cache/TemporalCache$Contents;

    invoke-virtual {v3}, Lcom/google/android/location/cache/TemporalCache$Contents;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/cache/CacheResult;

    invoke-virtual {v2}, Lcom/google/android/location/cache/CacheResult;->getLastSeenTime()J

    move-result-wide v3

    cmp-long v3, v3, p3

    if-gez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/location/cache/TemporalCache;->stats:Lcom/google/android/location/cache/Stats;

    invoke-virtual {v3}, Lcom/google/android/location/cache/Stats;->addExpirationEviction()V

    const-string v3, "TemporalCache"

    const-string v4, "Discarding %s because never seen recently."

    new-array v5, v8, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/google/android/location/cache/CacheResult;->getReadingTime()J

    move-result-wide v3

    cmp-long v3, v3, p1

    if-gez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/location/cache/TemporalCache;->stats:Lcom/google/android/location/cache/Stats;

    invoke-virtual {v3}, Lcom/google/android/location/cache/Stats;->addExpirationEviction()V

    const-string v3, "TemporalCache"

    const-string v4, "Discarding %s because result too old."

    new-array v5, v8, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Lcom/google/android/location/cache/CacheResult",
            "<TP;>;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/cache/TemporalCache;->contents:Lcom/google/android/location/cache/TemporalCache$Contents;

    invoke-virtual {v0}, Lcom/google/android/location/cache/TemporalCache$Contents;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public extractStatsAndReset()Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 3

    iget-object v0, p0, Lcom/google/android/location/cache/TemporalCache;->stats:Lcom/google/android/location/cache/Stats;

    iget-object v1, p0, Lcom/google/android/location/cache/TemporalCache;->contents:Lcom/google/android/location/cache/TemporalCache$Contents;

    iget v1, v1, Lcom/google/android/location/cache/TemporalCache$Contents;->capacity:I

    iget-object v2, p0, Lcom/google/android/location/cache/TemporalCache;->contents:Lcom/google/android/location/cache/TemporalCache$Contents;

    invoke-virtual {v2}, Lcom/google/android/location/cache/TemporalCache$Contents;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/cache/Stats;->extractAndReset(II)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public insertPosition(ZLjava/lang/Object;ILjava/lang/Object;J)V
    .locals 2
    .param p1    # Z
    .param p3    # I
    .param p5    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTK;ITP;J)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/location/cache/TemporalCache;->contents:Lcom/google/android/location/cache/TemporalCache$Contents;

    invoke-virtual {v1, p2}, Lcom/google/android/location/cache/TemporalCache$Contents;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/cache/CacheResult;

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/android/location/cache/CacheResult;

    invoke-direct {v0, p3, p4, p5, p6}, Lcom/google/android/location/cache/CacheResult;-><init>(ILjava/lang/Object;J)V

    iget-object v1, p0, Lcom/google/android/location/cache/TemporalCache;->contents:Lcom/google/android/location/cache/TemporalCache$Contents;

    invoke-virtual {v1, p2, v0}, Lcom/google/android/location/cache/TemporalCache$Contents;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/location/cache/TemporalCache;->stats:Lcom/google/android/location/cache/Stats;

    invoke-virtual {v1}, Lcom/google/android/location/cache/Stats;->addInsertion()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, p3, p4, p5, p6}, Lcom/google/android/location/cache/CacheResult;->setGlsResult(ILjava/lang/Object;J)V

    goto :goto_0
.end method

.method public load(Ljava/io/DataInput;)Lcom/google/android/location/cache/TemporalCache;
    .locals 9
    .param p1    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInput;",
            ")",
            "Lcom/google/android/location/cache/TemporalCache",
            "<TK;TP;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/location/cache/TemporalCache;->clear()V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v5, p0, Lcom/google/android/location/cache/TemporalCache;->keySaver:Lcom/google/android/location/data/Persistent;

    invoke-interface {v5, p1}, Lcom/google/android/location/data/Persistent;->load(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/location/cache/TemporalCache;->valueSaver:Lcom/google/android/location/data/Persistent;

    invoke-interface {v5, p1}, Lcom/google/android/location/data/Persistent;->load(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/location/cache/CacheResult;

    iget-object v5, p0, Lcom/google/android/location/cache/TemporalCache;->contents:Lcom/google/android/location/cache/TemporalCache$Contents;

    invoke-virtual {v5, v2, v4}, Lcom/google/android/location/cache/TemporalCache$Contents;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "TemporalCache"

    const-string v6, "Loaded entry, key=[%s], value=%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    const/4 v8, 0x1

    aput-object v4, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/location/cache/TemporalCache;->clear()V

    throw v0

    :cond_0
    return-object p0
.end method

.method public bridge synthetic load(Ljava/io/DataInput;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/location/cache/TemporalCache;->load(Ljava/io/DataInput;)Lcom/google/android/location/cache/TemporalCache;

    move-result-object v0

    return-object v0
.end method

.method public lookup(Ljava/lang/Object;J)Lcom/google/android/location/cache/CacheResult;
    .locals 3
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;J)",
            "Lcom/google/android/location/cache/CacheResult",
            "<TP;>;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/location/cache/TemporalCache;->contents:Lcom/google/android/location/cache/TemporalCache$Contents;

    invoke-virtual {v1, p1}, Lcom/google/android/location/cache/TemporalCache$Contents;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/cache/CacheResult;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/location/cache/CacheResult;->setLastSeenTime(J)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/location/cache/TemporalCache;->stats:Lcom/google/android/location/cache/Stats;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/location/cache/Stats;->addLookup(Z)V

    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public lookupWithoutUpdatingLastSeen(Ljava/lang/Object;)Lcom/google/android/location/cache/CacheResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lcom/google/android/location/cache/CacheResult",
            "<TP;>;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/location/cache/TemporalCache;->contents:Lcom/google/android/location/cache/TemporalCache$Contents;

    invoke-virtual {v1, p1}, Lcom/google/android/location/cache/TemporalCache$Contents;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/cache/CacheResult;

    iget-object v2, p0, Lcom/google/android/location/cache/TemporalCache;->stats:Lcom/google/android/location/cache/Stats;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/location/cache/Stats;->addLookup(Z)V

    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public save(Lcom/google/android/location/cache/TemporalCache;Ljava/io/DataOutput;)V
    .locals 4
    .param p2    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/cache/TemporalCache",
            "<TK;TP;>;",
            "Ljava/io/DataOutput;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p1, Lcom/google/android/location/cache/TemporalCache;->contents:Lcom/google/android/location/cache/TemporalCache$Contents;

    invoke-virtual {v2}, Lcom/google/android/location/cache/TemporalCache$Contents;->size()I

    move-result v2

    invoke-interface {p2, v2}, Ljava/io/DataOutput;->writeInt(I)V

    iget-object v2, p1, Lcom/google/android/location/cache/TemporalCache;->contents:Lcom/google/android/location/cache/TemporalCache$Contents;

    invoke-virtual {v2}, Lcom/google/android/location/cache/TemporalCache$Contents;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v2, p0, Lcom/google/android/location/cache/TemporalCache;->keySaver:Lcom/google/android/location/data/Persistent;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3, p2}, Lcom/google/android/location/data/Persistent;->save(Ljava/lang/Object;Ljava/io/DataOutput;)V

    iget-object v2, p0, Lcom/google/android/location/cache/TemporalCache;->valueSaver:Lcom/google/android/location/data/Persistent;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3, p2}, Lcom/google/android/location/data/Persistent;->save(Ljava/lang/Object;Ljava/io/DataOutput;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public bridge synthetic save(Ljava/lang/Object;Ljava/io/DataOutput;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/location/cache/TemporalCache;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/cache/TemporalCache;->save(Lcom/google/android/location/cache/TemporalCache;Ljava/io/DataOutput;)V

    return-void
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/cache/TemporalCache;->contents:Lcom/google/android/location/cache/TemporalCache$Contents;

    invoke-virtual {v0}, Lcom/google/android/location/cache/TemporalCache$Contents;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/cache/TemporalCache;->contents:Lcom/google/android/location/cache/TemporalCache$Contents;

    invoke-virtual {v0}, Lcom/google/android/location/cache/TemporalCache$Contents;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
