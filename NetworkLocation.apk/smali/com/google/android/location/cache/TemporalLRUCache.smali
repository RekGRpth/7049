.class Lcom/google/android/location/cache/TemporalLRUCache;
.super Ljava/util/LinkedHashMap;
.source "TemporalLRUCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/LinkedHashMap",
        "<TK;",
        "Lcom/google/android/location/data/TemporalObject",
        "<TV;>;>;"
    }
.end annotation


# instance fields
.field private final capacity:I


# direct methods
.method constructor <init>(I)V
    .locals 2
    .param p1    # I

    const/high16 v0, 0x3f400000

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput p1, p0, Lcom/google/android/location/cache/TemporalLRUCache;->capacity:I

    return-void
.end method


# virtual methods
.method public discardOldEntries(JJ)V
    .locals 5
    .param p1    # J
    .param p3    # J

    invoke-virtual {p0}, Lcom/google/android/location/cache/TemporalLRUCache;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/data/TemporalObject;

    invoke-virtual {v2}, Lcom/google/android/location/data/TemporalObject;->getCreationTimeInMillisSinceEpoch()J

    move-result-wide v3

    cmp-long v3, v3, p1

    if-gez v3, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/location/cache/TemporalLRUCache;->onRemoveEntry(Ljava/util/Map$Entry;)V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/google/android/location/data/TemporalObject;->getLastUsedTimeInMillisSinceEpoch()J

    move-result-wide v3

    cmp-long v3, v3, p3

    if-gez v3, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/location/cache/TemporalLRUCache;->onRemoveEntry(Ljava/util/Map$Entry;)V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public getCapacity()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/cache/TemporalLRUCache;->capacity:I

    return v0
.end method

.method protected onRemoveEntry(Ljava/util/Map$Entry;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Lcom/google/android/location/data/TemporalObject",
            "<TV;>;>;)V"
        }
    .end annotation

    return-void
.end method

.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;",
            "Lcom/google/android/location/data/TemporalObject",
            "<TV;>;>;)Z"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/location/cache/TemporalLRUCache;->size()I

    move-result v1

    iget v2, p0, Lcom/google/android/location/cache/TemporalLRUCache;->capacity:I

    if-le v1, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/location/cache/TemporalLRUCache;->onRemoveEntry(Ljava/util/Map$Entry;)V

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
