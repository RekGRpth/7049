.class final Lcom/google/android/location/cache/TemporalLRUCacheSavers$2;
.super Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;
.source "TemporalLRUCacheSavers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/location/cache/TemporalLRUCacheSavers;->createStringToStringSaver()Lcom/google/android/location/cache/TemporalLRUCacheSavers$TemporalLRUCacheSaver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(ILcom/google/gmm/common/io/protocol/ProtoBufType;Lcom/google/gmm/common/io/protocol/ProtoBufType;III)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/gmm/common/io/protocol/ProtoBufType;
    .param p3    # Lcom/google/gmm/common/io/protocol/ProtoBufType;
    .param p4    # I
    .param p5    # I
    .param p6    # I

    invoke-direct/range {p0 .. p6}, Lcom/google/android/location/cache/TemporalLRUCacheSavers$BaseTemporalLRUCacheSaver;-><init>(ILcom/google/gmm/common/io/protocol/ProtoBufType;Lcom/google/gmm/common/io/protocol/ProtoBufType;III)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic loadKeyFromProtoEntry(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {p0, p1}, Lcom/google/android/location/cache/TemporalLRUCacheSavers$2;->loadKeyFromProtoEntry(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected loadKeyFromProtoEntry(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadValueFromProtoEntry(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {p0, p1}, Lcom/google/android/location/cache/TemporalLRUCacheSavers$2;->loadValueFromProtoEntry(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected loadValueFromProtoEntry(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic writeKeyAndValueToProtoEntry(Lcom/google/gmm/common/io/protocol/ProtoBuf;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/Object;

    check-cast p2, Ljava/lang/String;

    check-cast p3, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/location/cache/TemporalLRUCacheSavers$2;->writeKeyAndValueToProtoEntry(Lcom/google/gmm/common/io/protocol/ProtoBuf;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected writeKeyAndValueToProtoEntry(Lcom/google/gmm/common/io/protocol/ProtoBuf;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-virtual {p1, v0, p2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    const/4 v0, 0x2

    invoke-virtual {p1, v0, p3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    return-void
.end method
