.class public Lcom/google/android/location/data/CellScreenTracker;
.super Ljava/lang/Object;
.source "CellScreenTracker.java"


# instance fields
.field private currentScreenState:Ljava/lang/Boolean;

.field private currentScreenStateTimestamp:J

.field private currentTower:Lcom/google/android/location/data/CellState;

.field private liveRilFromTowerHandoff:Z

.field private towerChangeTimestamp:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private differenceInSecs(JJ)I
    .locals 4
    .param p1    # J
    .param p3    # J

    sub-long v2, p1, p3

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long v2, v0, v2

    long-to-int v2, v2

    return v2
.end method


# virtual methods
.method declared-synchronized getStatus()I
    .locals 2

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/location/data/CellScreenTracker;->liveRilFromTowerHandoff:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/location/data/CellScreenTracker;->currentScreenState:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/data/CellScreenTracker;->currentTower:Lcom/google/android/location/data/CellState;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isOk()Z
    .locals 2

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/data/CellScreenTracker;->getStatus()I

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/android/location/data/CellScreenTracker;->currentScreenState:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/data/CellScreenTracker;->currentScreenState:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setCurrentCellTower(Lcom/google/android/location/data/CellState;J)V
    .locals 3
    .param p1    # Lcom/google/android/location/data/CellState;
    .param p2    # J

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/location/data/CellState;->hasBadValues()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/location/data/CellScreenTracker;->currentScreenState:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/data/CellScreenTracker;->currentScreenState:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/google/android/location/data/CellScreenTracker;->currentScreenStateTimestamp:J

    invoke-direct {p0, p2, p3, v1, v2}, Lcom/google/android/location/data/CellScreenTracker;->differenceInSecs(JJ)I

    move-result v1

    if-ge v0, v1, :cond_2

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/location/data/CellScreenTracker;->currentTower:Lcom/google/android/location/data/CellState;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/data/CellScreenTracker;->currentTower:Lcom/google/android/location/data/CellState;

    invoke-virtual {p1, v0}, Lcom/google/android/location/data/CellState;->sameCellTower(Lcom/google/android/location/data/CellState;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/data/CellScreenTracker;->liveRilFromTowerHandoff:Z

    :cond_2
    iput-wide p2, p0, Lcom/google/android/location/data/CellScreenTracker;->towerChangeTimestamp:J

    iput-object p1, p0, Lcom/google/android/location/data/CellScreenTracker;->currentTower:Lcom/google/android/location/data/CellState;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setCurrentCellTowerSignalStrength(IJ)V
    .locals 0
    .param p1    # I
    .param p2    # J

    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized setScreenState(ZJ)V
    .locals 1
    .param p1    # Z
    .param p2    # J

    monitor-enter p0

    :try_start_0
    iput-wide p2, p0, Lcom/google/android/location/data/CellScreenTracker;->currentScreenStateTimestamp:J

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/data/CellScreenTracker;->currentScreenState:Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
