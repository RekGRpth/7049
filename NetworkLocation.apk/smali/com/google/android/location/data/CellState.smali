.class public Lcom/google/android/location/data/CellState;
.super Ljava/lang/Object;
.source "CellState.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/data/CellState$NeighborCell;
    }
.end annotation


# instance fields
.field protected mCarrier:Ljava/lang/String;

.field protected mCid:I

.field protected mHomeMcc:I

.field protected mHomeMnc:I

.field protected mLac:I

.field protected mLatitude:I

.field protected mLongitude:I

.field protected mMcc:I

.field protected mMnc:I

.field protected mNeighbors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/data/CellState$NeighborCell;",
            ">;"
        }
    .end annotation
.end field

.field protected mPsc:I

.field protected mRadioType:I

.field protected mSignalStrength:I

.field protected final mTime:J


# direct methods
.method public constructor <init>(J)V
    .locals 2
    .param p1    # J

    const v1, 0x7fffffff

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/location/data/CellState;->mCid:I

    iput v0, p0, Lcom/google/android/location/data/CellState;->mLac:I

    iput v0, p0, Lcom/google/android/location/data/CellState;->mPsc:I

    iput v0, p0, Lcom/google/android/location/data/CellState;->mMcc:I

    iput v0, p0, Lcom/google/android/location/data/CellState;->mMnc:I

    iput v0, p0, Lcom/google/android/location/data/CellState;->mHomeMcc:I

    iput v0, p0, Lcom/google/android/location/data/CellState;->mHomeMnc:I

    iput v1, p0, Lcom/google/android/location/data/CellState;->mLatitude:I

    iput v1, p0, Lcom/google/android/location/data/CellState;->mLongitude:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/data/CellState;->mCarrier:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/data/CellState;->mNeighbors:Ljava/util/List;

    iput-wide p1, p0, Lcom/google/android/location/data/CellState;->mTime:J

    return-void
.end method

.method public static append(Ljava/lang/StringBuilder;Lcom/google/android/location/data/CellState;)V
    .locals 4
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # Lcom/google/android/location/data/CellState;

    if-nez p1, :cond_0

    const-string v3, "null"

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    return-void

    :cond_0
    const-string v3, "[cid: "

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p1, Lcom/google/android/location/data/CellState;->mCid:I

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " lac: "

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p1, Lcom/google/android/location/data/CellState;->mLac:I

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " mcc: "

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p1, Lcom/google/android/location/data/CellState;->mMcc:I

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " mnc: "

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p1, Lcom/google/android/location/data/CellState;->mMnc:I

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " radioType: "

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p1, Lcom/google/android/location/data/CellState;->mRadioType:I

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " signalStrength: "

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p1, Lcom/google/android/location/data/CellState;->mSignalStrength:I

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " neighbors["

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    iget-object v3, p1, Lcom/google/android/location/data/CellState;->mNeighbors:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/data/CellState$NeighborCell;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    const-string v3, ","

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_2
    const-string v3, "]"

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static dump(Ljava/io/PrintWriter;Lcom/google/android/location/data/CellState;)V
    .locals 4
    .param p0    # Ljava/io/PrintWriter;
    .param p1    # Lcom/google/android/location/data/CellState;

    if-nez p1, :cond_0

    const-string v3, "null"

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v3, "[cid: "

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v3, p1, Lcom/google/android/location/data/CellState;->mCid:I

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(I)V

    const-string v3, " lac: "

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v3, p1, Lcom/google/android/location/data/CellState;->mLac:I

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(I)V

    const-string v3, " mcc: "

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v3, p1, Lcom/google/android/location/data/CellState;->mMcc:I

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(I)V

    const-string v3, " mnc: "

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v3, p1, Lcom/google/android/location/data/CellState;->mMnc:I

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(I)V

    const-string v3, " radioType: "

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v3, p1, Lcom/google/android/location/data/CellState;->mRadioType:I

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(I)V

    const-string v3, " signalStrength: "

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v3, p1, Lcom/google/android/location/data/CellState;->mSignalStrength:I

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(I)V

    const-string v3, " neighbors["

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const/4 v0, 0x1

    iget-object v3, p1, Lcom/google/android/location/data/CellState;->mNeighbors:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/data/CellState$NeighborCell;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    const-string v3, ","

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    const-string v3, "]]"

    invoke-virtual {p0, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected clone()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/data/CellState;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/location/data/CellState;->mNeighbors:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/google/android/location/data/CellState;->mNeighbors:Ljava/util/List;

    return-object v0
.end method

.method public copy()Lcom/google/android/location/data/CellState;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/data/CellState;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/data/CellState;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public createCellularPlatformProfile()Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 4

    const/4 v3, -0x1

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCELLULAR_PLATFORM_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p0}, Lcom/google/android/location/data/CellState;->getCellPlatformProfileRadioType()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    iget v2, p0, Lcom/google/android/location/data/CellState;->mHomeMcc:I

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/location/data/CellState;->mHomeMnc:I

    if-eq v2, v3, :cond_0

    const/4 v2, 0x5

    iget v3, p0, Lcom/google/android/location/data/CellState;->mHomeMcc:I

    invoke-virtual {v0, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v2, 0x4

    iget v3, p0, Lcom/google/android/location/data/CellState;->mHomeMnc:I

    invoke-virtual {v0, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/location/data/CellState;->mCarrier:Ljava/lang/String;

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/location/data/CellState;->mCarrier:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_1
    return-object v0
.end method

.method public gcell(J)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 12
    .param p1    # J

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v7, Lcom/google/android/location/protocol/LocserverMessageTypes;->GCELL:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v7}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/4 v7, 0x1

    iget v8, p0, Lcom/google/android/location/data/CellState;->mLac:I

    invoke-virtual {v0, v7, v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v7, 0x2

    iget v8, p0, Lcom/google/android/location/data/CellState;->mCid:I

    invoke-virtual {v0, v7, v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    iget v5, p0, Lcom/google/android/location/data/CellState;->mMnc:I

    const/4 v7, -0x1

    if-eq v5, v7, :cond_0

    const/4 v7, 0x3

    invoke-virtual {v0, v7, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_0
    iget v4, p0, Lcom/google/android/location/data/CellState;->mMcc:I

    const/4 v7, -0x1

    if-eq v4, v7, :cond_1

    const/4 v7, 0x4

    invoke-virtual {v0, v7, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_1
    iget v6, p0, Lcom/google/android/location/data/CellState;->mSignalStrength:I

    const/16 v7, -0x270f

    if-eq v6, v7, :cond_2

    const/4 v7, 0x5

    invoke-virtual {v0, v7, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_2
    iget v7, p0, Lcom/google/android/location/data/CellState;->mPsc:I

    const/4 v8, -0x1

    if-eq v7, v8, :cond_3

    const/16 v7, 0x8

    iget v8, p0, Lcom/google/android/location/data/CellState;->mPsc:I

    invoke-virtual {v0, v7, v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_3
    const-wide/16 v7, 0x0

    cmp-long v7, p1, v7

    if-eqz v7, :cond_4

    const/4 v7, 0x6

    long-to-int v8, p1

    invoke-virtual {v0, v7, v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_4
    iget v1, p0, Lcom/google/android/location/data/CellState;->mLatitude:I

    iget v3, p0, Lcom/google/android/location/data/CellState;->mLongitude:I

    const v7, 0x7fffffff

    if-eq v1, v7, :cond_5

    const v7, 0x7fffffff

    if-eq v3, v7, :cond_5

    new-instance v2, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v7, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLAT_LNG:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v7}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/4 v7, 0x1

    int-to-double v8, v1

    const-wide v10, 0x4085b38e38e38e39L

    mul-double/2addr v8, v10

    double-to-int v8, v8

    invoke-virtual {v2, v7, v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v7, 0x2

    int-to-double v8, v3

    const-wide v10, 0x4085b38e38e38e39L

    mul-double/2addr v8, v10

    double-to-int v8, v8

    invoke-virtual {v2, v7, v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/16 v7, 0x9

    invoke-virtual {v0, v7, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    :cond_5
    const/16 v7, 0xa

    invoke-virtual {p0}, Lcom/google/android/location/data/CellState;->getGCellRadioType()I

    move-result v8

    invoke-virtual {v0, v7, v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    return-object v0
.end method

.method public getCellPlatformProfileRadioType()I
    .locals 3

    const/4 v0, -0x1

    iget v1, p0, Lcom/google/android/location/data/CellState;->mRadioType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v0, 0x3

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/location/data/CellState;->mRadioType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/google/android/location/data/CellState;->mRadioType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const/4 v0, 0x5

    goto :goto_0
.end method

.method public getCid()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/data/CellState;->mCid:I

    return v0
.end method

.method public getGCellRadioType()I
    .locals 3

    const/4 v0, -0x1

    iget v1, p0, Lcom/google/android/location/data/CellState;->mRadioType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v0, 0x3

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/location/data/CellState;->mRadioType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/google/android/location/data/CellState;->mRadioType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const/4 v0, 0x5

    goto :goto_0
.end method

.method public getLac()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/data/CellState;->mLac:I

    return v0
.end method

.method public getMcc()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/data/CellState;->mMcc:I

    return v0
.end method

.method public getMnc()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/data/CellState;->mMnc:I

    return v0
.end method

.method public getNeighbors()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/data/CellState$NeighborCell;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/data/CellState;->mNeighbors:Ljava/util/List;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/data/CellState;->mTime:J

    return-wide v0
.end method

.method public hasBadValues()Z
    .locals 1

    iget v0, p0, Lcom/google/android/location/data/CellState;->mCid:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/location/data/CellState;->mLac:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/location/data/CellState;->mMcc:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/location/data/CellState;->mMnc:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 2

    const/4 v1, -0x1

    iget v0, p0, Lcom/google/android/location/data/CellState;->mCid:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/location/data/CellState;->mLac:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/location/data/CellState;->mMcc:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/location/data/CellState;->mMnc:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sameCellTower(Lcom/google/android/location/data/CellState;)Z
    .locals 3
    .param p1    # Lcom/google/android/location/data/CellState;

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/location/data/CellState;->mCid:I

    iget v2, p1, Lcom/google/android/location/data/CellState;->mCid:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/location/data/CellState;->mLac:I

    iget v2, p1, Lcom/google/android/location/data/CellState;->mLac:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/location/data/CellState;->mPsc:I

    iget v2, p1, Lcom/google/android/location/data/CellState;->mPsc:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/location/data/CellState;->mMcc:I

    iget v2, p1, Lcom/google/android/location/data/CellState;->mMcc:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/location/data/CellState;->mMnc:I

    iget v2, p1, Lcom/google/android/location/data/CellState;->mMnc:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/location/data/CellState;->mRadioType:I

    iget v2, p1, Lcom/google/android/location/data/CellState;->mRadioType:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public sameCidLac(Lcom/google/android/location/data/CellState;)Z
    .locals 2
    .param p1    # Lcom/google/android/location/data/CellState;

    iget v0, p0, Lcom/google/android/location/data/CellState;->mCid:I

    iget v1, p1, Lcom/google/android/location/data/CellState;->mCid:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/location/data/CellState;->mLac:I

    iget v1, p1, Lcom/google/android/location/data/CellState;->mLac:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSignalStrength(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/location/data/CellState;->mSignalStrength:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, p0}, Lcom/google/android/location/data/CellState;->append(Ljava/lang/StringBuilder;Lcom/google/android/location/data/CellState;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
