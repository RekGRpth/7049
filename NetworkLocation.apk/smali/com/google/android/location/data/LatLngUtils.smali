.class public Lcom/google/android/location/data/LatLngUtils;
.super Ljava/lang/Object;
.source "LatLngUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static degE7ToRad(D)D
    .locals 4
    .param p0    # D

    const-wide v0, 0x416312d000000000L

    div-double v0, p0, v0

    const-wide v2, 0x400921fb54442d18L

    mul-double/2addr v0, v2

    const-wide v2, 0x4066800000000000L

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public static distanceInM(DDDD)D
    .locals 26
    .param p0    # D
    .param p2    # D
    .param p4    # D
    .param p6    # D

    invoke-static/range {p0 .. p1}, Lcom/google/android/location/data/LatLngUtils;->degE7ToRad(D)D

    move-result-wide v8

    invoke-static/range {p2 .. p3}, Lcom/google/android/location/data/LatLngUtils;->degE7ToRad(D)D

    move-result-wide v10

    invoke-static/range {p4 .. p5}, Lcom/google/android/location/data/LatLngUtils;->degE7ToRad(D)D

    move-result-wide v12

    invoke-static/range {p6 .. p7}, Lcom/google/android/location/data/LatLngUtils;->degE7ToRad(D)D

    move-result-wide v14

    const-wide/high16 v18, 0x3fe0000000000000L

    sub-double v20, v12, v8

    mul-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    const-wide/high16 v18, 0x3fe0000000000000L

    sub-double v20, v14, v10

    mul-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double v18, v4, v4

    mul-double v20, v6, v6

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v22

    mul-double v20, v20, v22

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v22

    mul-double v20, v20, v22

    add-double v16, v18, v20

    const-wide/high16 v18, 0x4000000000000000L

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v20

    const-wide/16 v22, 0x0

    const-wide/high16 v24, 0x3ff0000000000000L

    sub-double v24, v24, v16

    invoke-static/range {v22 .. v25}, Ljava/lang/Math;->max(DD)D

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v22

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v20

    mul-double v0, v18, v20

    const-wide v18, 0x415849c600000000L

    mul-double v2, v0, v18

    return-wide v2
.end method
