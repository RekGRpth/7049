.class public Lcom/google/android/location/data/LevelSelectorFactory;
.super Lcom/google/android/location/data/ProtoFactory;
.source "LevelSelectorFactory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/location/data/ProtoFactory",
        "<",
        "Lcom/google/android/location/data/LevelSelector;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSIGNAL_FINGERPRINT_MODEL_CLUSTER:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {p0, v0}, Lcom/google/android/location/data/ProtoFactory;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    return-void
.end method

.method private static createPredictionToLevel(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/util/Map;
    .locals 8
    .param p0    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gmm/common/io/protocol/ProtoBuf;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v7, 0x2

    const/4 v6, 0x1

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0, v7}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v6, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v4

    int-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v1, v7, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method


# virtual methods
.method public create(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/android/location/data/LevelSelector;
    .locals 9
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v7, 0x1

    const/4 v6, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v6

    :cond_1
    invoke-virtual {p1, v7}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v2

    const/4 v7, 0x3

    :try_start_0
    invoke-virtual {v2, v7}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/location/data/ModelFactory;->createBagOfTrees(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/android/location/learning/BagOfTrees;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/location/data/ModelFactory;->createMacToAttributeId(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v2}, Lcom/google/android/location/data/LevelSelectorFactory;->createPredictionToLevel(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/util/Map;

    move-result-object v4

    new-instance v5, Lcom/google/android/location/data/SignalStrengthBagPredictor;

    invoke-direct {v5, v3, v0}, Lcom/google/android/location/data/SignalStrengthBagPredictor;-><init>(Ljava/util/Map;Lcom/google/android/location/learning/BagOfTrees;)V

    new-instance v7, Lcom/google/android/location/data/LevelSelector;

    invoke-direct {v7, v5, v4}, Lcom/google/android/location/data/LevelSelector;-><init>(Lcom/google/android/location/data/SignalStrengthBagPredictor;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v6, v7

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v7, "LevelSelectorFactory"

    const-string v8, "Binary data of model corrupted."

    invoke-static {v7, v8}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic create(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    invoke-virtual {p0, p1}, Lcom/google/android/location/data/LevelSelectorFactory;->create(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/android/location/data/LevelSelector;

    move-result-object v0

    return-object v0
.end method
