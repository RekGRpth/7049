.class final Lcom/google/android/location/data/LocatorResult$1;
.super Ljava/lang/Object;
.source "LocatorResult.java"

# interfaces
.implements Lcom/google/android/location/data/Persistent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/data/LocatorResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/location/data/Persistent",
        "<",
        "Lcom/google/android/location/data/LocatorResult;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public load(Ljava/io/DataInput;)Lcom/google/android/location/data/LocatorResult;
    .locals 6
    .param p1    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/google/android/location/data/LocatorResult$ResultStatus;->values()[Lcom/google/android/location/data/LocatorResult$ResultStatus;

    move-result-object v3

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v4

    aget-object v2, v3, v4
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v3, Lcom/google/android/location/data/LocatorResult$ResultStatus;->OK:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    if-ne v2, v3, :cond_0

    sget-object v3, Lcom/google/android/location/data/Position;->SAVER:Lcom/google/android/location/data/Persistent;

    invoke-interface {v3, p1}, Lcom/google/android/location/data/Persistent;->load(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/data/Position;

    :cond_0
    new-instance v3, Lcom/google/android/location/data/LocatorResult;

    invoke-interface {p1}, Ljava/io/DataInput;->readLong()J

    move-result-wide v4

    invoke-direct {v3, v1, v2, v4, v5}, Lcom/google/android/location/data/LocatorResult;-><init>(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/LocatorResult$ResultStatus;J)V

    return-object v3

    :catch_0
    move-exception v0

    new-instance v3, Ljava/io/IOException;

    const-string v4, "invalid status"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public bridge synthetic load(Ljava/io/DataInput;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/location/data/LocatorResult$1;->load(Ljava/io/DataInput;)Lcom/google/android/location/data/LocatorResult;

    move-result-object v0

    return-object v0
.end method

.method public save(Lcom/google/android/location/data/LocatorResult;Ljava/io/DataOutput;)V
    .locals 2
    .param p1    # Lcom/google/android/location/data/LocatorResult;
    .param p2    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p1, Lcom/google/android/location/data/LocatorResult;->status:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    invoke-virtual {v0}, Lcom/google/android/location/data/LocatorResult$ResultStatus;->ordinal()I

    move-result v0

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeInt(I)V

    iget-object v0, p1, Lcom/google/android/location/data/LocatorResult;->status:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    sget-object v1, Lcom/google/android/location/data/LocatorResult$ResultStatus;->OK:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/location/data/Position;->SAVER:Lcom/google/android/location/data/Persistent;

    iget-object v1, p1, Lcom/google/android/location/data/LocatorResult;->position:Lcom/google/android/location/data/Position;

    invoke-interface {v0, v1, p2}, Lcom/google/android/location/data/Persistent;->save(Ljava/lang/Object;Ljava/io/DataOutput;)V

    :cond_0
    iget-wide v0, p1, Lcom/google/android/location/data/LocatorResult;->reportTime:J

    invoke-interface {p2, v0, v1}, Ljava/io/DataOutput;->writeLong(J)V

    return-void
.end method

.method public bridge synthetic save(Ljava/lang/Object;Ljava/io/DataOutput;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/location/data/LocatorResult;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/location/data/LocatorResult$1;->save(Lcom/google/android/location/data/LocatorResult;Ljava/io/DataOutput;)V

    return-void
.end method
