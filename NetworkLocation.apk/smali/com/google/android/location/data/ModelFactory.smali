.class Lcom/google/android/location/data/ModelFactory;
.super Ljava/lang/Object;
.source "ModelFactory.java"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static createBagOfTrees(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/android/location/learning/BagOfTrees;
    .locals 6
    .param p0    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x2

    invoke-virtual {p0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v2

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v1, Ljava/io/DataInputStream;

    new-instance v4, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v4, v3}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v1}, Lcom/google/android/location/learning/BagOfTrees;->read(Ljava/io/DataInputStream;)Lcom/google/android/location/learning/BagOfTrees;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    goto :goto_0
.end method

.method static createMacToAttributeId(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/util/Map;
    .locals 5
    .param p0    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gmm/common/io/protocol/ProtoBuf;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x1

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v4, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getLong(II)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {p0, v3, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method
