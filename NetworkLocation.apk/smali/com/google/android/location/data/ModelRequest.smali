.class public Lcom/google/android/location/data/ModelRequest;
.super Ljava/lang/Object;
.source "ModelRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/data/ModelRequest$Type;
    }
.end annotation


# instance fields
.field private final modelId:Ljava/lang/String;

.field private final type:Lcom/google/android/location/data/ModelRequest$Type;


# direct methods
.method private constructor <init>(Lcom/google/android/location/data/ModelRequest$Type;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/android/location/data/ModelRequest$Type;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/data/ModelRequest;->type:Lcom/google/android/location/data/ModelRequest$Type;

    iput-object p2, p0, Lcom/google/android/location/data/ModelRequest;->modelId:Ljava/lang/String;

    return-void
.end method

.method public static newLevelRequest(Ljava/lang/String;)Lcom/google/android/location/data/ModelRequest;
    .locals 2
    .param p0    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/location/data/ModelRequest;

    sget-object v1, Lcom/google/android/location/data/ModelRequest$Type;->LEVEL:Lcom/google/android/location/data/ModelRequest$Type;

    invoke-direct {v0, v1, p0}, Lcom/google/android/location/data/ModelRequest;-><init>(Lcom/google/android/location/data/ModelRequest$Type;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newLevelSelectorRequest(Ljava/lang/String;)Lcom/google/android/location/data/ModelRequest;
    .locals 2
    .param p0    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/location/data/ModelRequest;

    sget-object v1, Lcom/google/android/location/data/ModelRequest$Type;->LEVEL_SELECTOR:Lcom/google/android/location/data/ModelRequest$Type;

    invoke-direct {v0, v1, p0}, Lcom/google/android/location/data/ModelRequest;-><init>(Lcom/google/android/location/data/ModelRequest$Type;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/location/data/ModelRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/location/data/ModelRequest;

    iget-object v3, p0, Lcom/google/android/location/data/ModelRequest;->type:Lcom/google/android/location/data/ModelRequest$Type;

    iget-object v4, v0, Lcom/google/android/location/data/ModelRequest;->type:Lcom/google/android/location/data/ModelRequest$Type;

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/location/data/ModelRequest;->modelId:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/android/location/data/ModelRequest;->modelId:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/android/location/data/ModelRequest;->modelId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/location/data/ModelRequest;->modelId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getModelId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/data/ModelRequest;->modelId:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/google/android/location/data/ModelRequest$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/data/ModelRequest;->type:Lcom/google/android/location/data/ModelRequest$Type;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/location/data/ModelRequest;->type:Lcom/google/android/location/data/ModelRequest$Type;

    invoke-virtual {v1}, Lcom/google/android/location/data/ModelRequest$Type;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/location/data/ModelRequest;->modelId:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/data/ModelRequest;->modelId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/data/ModelRequest;->type:Lcom/google/android/location/data/ModelRequest$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "modelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/data/ModelRequest;->modelId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
