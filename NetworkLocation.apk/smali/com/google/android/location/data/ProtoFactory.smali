.class public abstract Lcom/google/android/location/data/ProtoFactory;
.super Ljava/lang/Object;
.source "ProtoFactory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final protoType:Lcom/google/gmm/common/io/protocol/ProtoBufType;


# direct methods
.method protected constructor <init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V
    .locals 0
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/data/ProtoFactory;->protoType:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    return-void
.end method


# virtual methods
.method public abstract create(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gmm/common/io/protocol/ProtoBuf;",
            ")TT;"
        }
    .end annotation
.end method

.method public newProto()Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 2

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v1, p0, Lcom/google/android/location/data/ProtoFactory;->protoType:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    return-object v0
.end method
