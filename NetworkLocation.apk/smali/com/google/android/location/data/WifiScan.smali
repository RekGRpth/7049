.class public Lcom/google/android/location/data/WifiScan;
.super Ljava/lang/Object;
.source "WifiScan.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/data/WifiScan$Device;
    }
.end annotation


# instance fields
.field public final deliveryTime:J

.field private final devices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/location/data/WifiScan$Device;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(JLjava/util/ArrayList;)V
    .locals 0
    .param p1    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/location/data/WifiScan$Device;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/location/data/WifiScan;->deliveryTime:J

    iput-object p3, p0, Lcom/google/android/location/data/WifiScan;->devices:Ljava/util/ArrayList;

    return-void
.end method

.method public static append(Ljava/lang/StringBuilder;Lcom/google/android/location/data/WifiScan;)V
    .locals 4
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # Lcom/google/android/location/data/WifiScan;

    if-nez p1, :cond_0

    const-string v2, "null"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    return-void

    :cond_0
    const-string v2, "WifiScan [deliveryTime="

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p1, Lcom/google/android/location/data/WifiScan;->deliveryTime:J

    invoke-virtual {p0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ", devices=["

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/google/android/location/data/WifiScan;->devices:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/data/WifiScan$Device;

    invoke-static {p0, v0}, Lcom/google/android/location/data/WifiScan$Device;->append(Ljava/lang/StringBuilder;Lcom/google/android/location/data/WifiScan$Device;)V

    const-string v2, ", "

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    const-string v2, "]]"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static dump(Ljava/io/PrintWriter;Lcom/google/android/location/data/WifiScan;)V
    .locals 4
    .param p0    # Ljava/io/PrintWriter;
    .param p1    # Lcom/google/android/location/data/WifiScan;

    if-nez p1, :cond_0

    const-string v2, "null"

    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v2, "WifiScan [deliveryTime="

    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-wide v2, p1, Lcom/google/android/location/data/WifiScan;->deliveryTime:J

    invoke-virtual {p0, v2, v3}, Ljava/io/PrintWriter;->print(J)V

    const-string v2, ", devices=["

    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/location/data/WifiScan;->devices:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/data/WifiScan$Device;

    invoke-static {p0, v0}, Lcom/google/android/location/data/WifiScan$Device;->dump(Ljava/io/PrintWriter;Lcom/google/android/location/data/WifiScan$Device;)V

    const-string v2, ", "

    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const-string v2, "]]"

    invoke-virtual {p0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final createWifiProfile(JZ)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 6
    .param p1    # J
    .param p3    # Z

    iget-object v3, p0, Lcom/google/android/location/data/WifiScan;->devices:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    const/4 v2, 0x0

    :cond_0
    return-object v2

    :cond_1
    new-instance v2, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/protocol/LocserverMessageTypes;->GWIFI_PROFILE:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/google/android/location/data/WifiScan;->deliveryTime:J

    add-long/2addr v4, p1

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setLong(IJ)V

    const/16 v3, 0x19

    iget-object v4, p0, Lcom/google/android/location/data/WifiScan;->devices:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    const/4 v4, 0x2

    iget-object v3, p0, Lcom/google/android/location/data/WifiScan;->devices:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/location/data/WifiScan$Device;

    invoke-virtual {v3, p3}, Lcom/google/android/location/data/WifiScan$Device;->createProto(Z)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final getDevice(I)Lcom/google/android/location/data/WifiScan$Device;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/location/data/WifiScan;->devices:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/data/WifiScan$Device;

    return-object v0
.end method

.method public final numDevices()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/data/WifiScan;->devices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WifiScan [deliveryTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/data/WifiScan;->deliveryTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", devices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/data/WifiScan;->devices:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
