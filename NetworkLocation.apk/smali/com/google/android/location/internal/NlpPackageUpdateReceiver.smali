.class public Lcom/google/android/location/internal/NlpPackageUpdateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NlpPackageUpdateReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;
    }
.end annotation


# static fields
.field private static final listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;->listeners:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static declared-synchronized addListener(Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;)V
    .locals 2
    .param p0    # Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;

    const-class v1, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;->listeners:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized notifyListeners()V
    .locals 4

    const-class v3, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;->listeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;

    invoke-interface {v1}, Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;->nlpPackageUpdated()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_0
    monitor-exit v3

    return-void
.end method

.method public static declared-synchronized removeListener(Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;)V
    .locals 2
    .param p0    # Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;

    const-class v1, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;->listeners:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;->notifyListeners()V

    return-void
.end method
