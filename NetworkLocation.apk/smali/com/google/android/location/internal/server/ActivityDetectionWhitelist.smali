.class public Lcom/google/android/location/internal/server/ActivityDetectionWhitelist;
.super Ljava/lang/Object;
.source "ActivityDetectionWhitelist.java"


# static fields
.field private static final PACKAGE_NAME_WHITE_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/regex/Pattern;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/location/internal/server/ActivityDetectionWhitelist;->PACKAGE_NAME_WHITE_LIST:Ljava/util/List;

    sget-object v0, Lcom/google/android/location/internal/server/ActivityDetectionWhitelist;->PACKAGE_NAME_WHITE_LIST:Ljava/util/List;

    const-string v1, "com.google.android.apps.sidekick"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/location/internal/server/ActivityDetectionWhitelist;->PACKAGE_NAME_WHITE_LIST:Ljava/util/List;

    const-string v1, "com.google.android.googlequicksearchbox"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/location/internal/server/ActivityDetectionWhitelist;->PACKAGE_NAME_WHITE_LIST:Ljava/util/List;

    const-string v1, "com.google.android.apps.maps.*"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/location/internal/server/ActivityDetectionWhitelist;->PACKAGE_NAME_WHITE_LIST:Ljava/util/List;

    const-string v1, "com.google.android.apps.lbs.demo.*"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isInWhitelist(Ljava/lang/String;)Z
    .locals 3
    .param p0    # Ljava/lang/String;

    sget-object v2, Lcom/google/android/location/internal/server/ActivityDetectionWhitelist;->PACKAGE_NAME_WHITE_LIST:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
