.class Lcom/google/android/location/internal/server/Listeners$LocationIntentReceiver;
.super Lcom/google/android/location/internal/server/Listeners$IntentReceiver;
.source "Listeners.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/internal/server/Listeners;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LocationIntentReceiver"
.end annotation


# instance fields
.field final needsDebugInfo:Z

.field final periodSecs:I


# direct methods
.method constructor <init>(Landroid/app/PendingIntent;ILandroid/os/PowerManager$WakeLock;Z)V
    .locals 0
    .param p1    # Landroid/app/PendingIntent;
    .param p2    # I
    .param p3    # Landroid/os/PowerManager$WakeLock;
    .param p4    # Z

    invoke-direct {p0, p1, p3}, Lcom/google/android/location/internal/server/Listeners$IntentReceiver;-><init>(Landroid/app/PendingIntent;Landroid/os/PowerManager$WakeLock;)V

    iput p2, p0, Lcom/google/android/location/internal/server/Listeners$LocationIntentReceiver;->periodSecs:I

    iput-boolean p4, p0, Lcom/google/android/location/internal/server/Listeners$LocationIntentReceiver;->needsDebugInfo:Z

    return-void
.end method
