.class Lcom/google/android/location/internal/server/Listeners;
.super Ljava/lang/Object;
.source "Listeners.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/internal/server/Listeners$LocationIntentReceiver;,
        Lcom/google/android/location/internal/server/Listeners$IntentReceiver;,
        Lcom/google/android/location/internal/server/Listeners$Receiver;
    }
.end annotation


# instance fields
.field private final activityIntentReceivers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/internal/server/Listeners$IntentReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final listeners:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/android/location/internal/server/Listeners$Receiver;",
            ">;"
        }
    .end annotation
.end field

.field private final locationIntentReceivers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/location/internal/server/Listeners$LocationIntentReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private minPeriodSecs:I

.field private final nlpReleaseVersion:I


# direct methods
.method constructor <init>(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/Listeners;->listeners:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/Listeners;->locationIntentReceivers:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/Listeners;->activityIntentReceivers:Ljava/util/Set;

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/location/internal/server/Listeners;->minPeriodSecs:I

    iput p1, p0, Lcom/google/android/location/internal/server/Listeners;->nlpReleaseVersion:I

    return-void
.end method

.method private listenersChanged()V
    .locals 7

    const v4, 0x7fffffff

    iput v4, p0, Lcom/google/android/location/internal/server/Listeners;->minPeriodSecs:I

    iget-object v4, p0, Lcom/google/android/location/internal/server/Listeners;->listeners:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/location/internal/server/Listeners$Receiver;

    const-string v4, "GmmNetworkLocationListeners"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Still have listener "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v3, Lcom/google/android/location/internal/server/Listeners$Receiver;->listener:Lcom/google/android/location/internal/ILocationListener;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/location/internal/server/Listeners;->minPeriodSecs:I

    iget v5, v3, Lcom/google/android/location/internal/server/Listeners$Receiver;->periodSecs:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/location/internal/server/Listeners;->minPeriodSecs:I

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/google/android/location/internal/server/Listeners;->locationIntentReceivers:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/internal/server/Listeners$LocationIntentReceiver;

    const-string v4, "GmmNetworkLocationListeners"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Still have pending intent "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/google/android/location/internal/server/Listeners$LocationIntentReceiver;->pendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/location/internal/server/Listeners;->minPeriodSecs:I

    iget v5, v2, Lcom/google/android/location/internal/server/Listeners$LocationIntentReceiver;->periodSecs:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/location/internal/server/Listeners;->minPeriodSecs:I

    goto :goto_1

    :cond_1
    return-void
.end method

.method private newCallbackIntent()Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.location.internal.EXTRA_RELEASE_VERSION"

    iget v2, p0, Lcom/google/android/location/internal/server/Listeners;->nlpReleaseVersion:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0
.end method

.method private notifyLocationIntentReceivers(Landroid/content/Context;Landroid/location/Location;Z)Z
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/location/Location;
    .param p3    # Z

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/location/internal/server/Listeners;->locationIntentReceivers:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/location/internal/server/Listeners;->newCallbackIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "location"

    invoke-virtual {v2, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/location/internal/server/Listeners$LocationIntentReceiver;

    iget-boolean v4, v3, Lcom/google/android/location/internal/server/Listeners$LocationIntentReceiver;->needsDebugInfo:Z

    if-ne p3, v4, :cond_0

    invoke-virtual {v3, p1, v2}, Lcom/google/android/location/internal/server/Listeners$LocationIntentReceiver;->deliverIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "GmmNetworkLocationListeners"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "dropping intent receiver"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v3, Lcom/google/android/location/internal/server/Listeners$LocationIntentReceiver;->pendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    return v0
.end method


# virtual methods
.method add(Lcom/google/android/location/internal/ILocationListener;I)V
    .locals 3
    .param p1    # Lcom/google/android/location/internal/ILocationListener;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/location/internal/server/Listeners;->listeners:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/location/internal/ILocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    new-instance v2, Lcom/google/android/location/internal/server/Listeners$Receiver;

    invoke-direct {v2, p1, p2}, Lcom/google/android/location/internal/server/Listeners$Receiver;-><init>(Lcom/google/android/location/internal/ILocationListener;I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/location/internal/server/Listeners;->listenersChanged()V

    return-void
.end method

.method addActivityPendingIntent(Landroid/content/Context;Landroid/app/PendingIntent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/app/PendingIntent;

    const-string v3, "power"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NLP ActivityPendingIntent client in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    new-instance v0, Lcom/google/android/location/internal/server/Listeners$IntentReceiver;

    invoke-direct {v0, p2, v2}, Lcom/google/android/location/internal/server/Listeners$IntentReceiver;-><init>(Landroid/app/PendingIntent;Landroid/os/PowerManager$WakeLock;)V

    iget-object v3, p0, Lcom/google/android/location/internal/server/Listeners;->activityIntentReceivers:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method addLocationPendingIntent(Landroid/content/Context;Landroid/app/PendingIntent;ILandroid/location/Location;Z)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/app/PendingIntent;
    .param p3    # I
    .param p4    # Landroid/location/Location;
    .param p5    # Z

    const-string v4, "power"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NLP PendingIntent client in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    new-instance v0, Lcom/google/android/location/internal/server/Listeners$LocationIntentReceiver;

    invoke-direct {v0, p2, p3, v3, p5}, Lcom/google/android/location/internal/server/Listeners$LocationIntentReceiver;-><init>(Landroid/app/PendingIntent;ILandroid/os/PowerManager$WakeLock;Z)V

    iget-object v4, p0, Lcom/google/android/location/internal/server/Listeners;->locationIntentReceivers:Ljava/util/Set;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    if-eqz p4, :cond_0

    invoke-direct {p0}, Lcom/google/android/location/internal/server/Listeners;->newCallbackIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v4, "location"

    invoke-virtual {v1, v4, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/location/internal/server/Listeners$LocationIntentReceiver;->deliverIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    :cond_0
    invoke-direct {p0}, Lcom/google/android/location/internal/server/Listeners;->listenersChanged()V

    return-void
.end method

.method getActivityListenerCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/internal/server/Listeners;->activityIntentReceivers:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method getMinPeriodSecs()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/internal/server/Listeners;->minPeriodSecs:I

    return v0
.end method

.method remove(Lcom/google/android/location/internal/ILocationListener;)V
    .locals 2
    .param p1    # Lcom/google/android/location/internal/ILocationListener;

    iget-object v0, p0, Lcom/google/android/location/internal/server/Listeners;->listeners:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/location/internal/ILocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/location/internal/server/Listeners;->listenersChanged()V

    return-void
.end method

.method reportEnabled(Landroid/content/Context;Z)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    const/4 v0, 0x0

    iget-object v6, p0, Lcom/google/android/location/internal/server/Listeners;->listeners:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/location/internal/server/Listeners$Receiver;

    if-eqz p2, :cond_0

    :try_start_0
    iget-object v6, v5, Lcom/google/android/location/internal/server/Listeners$Receiver;->listener:Lcom/google/android/location/internal/ILocationListener;

    invoke-interface {v6}, Lcom/google/android/location/internal/ILocationListener;->onProviderEnabled()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v6, "GmmNetworkLocationListeners"

    const-string v7, "dropping listener"

    invoke-static {v6, v7}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    :try_start_1
    iget-object v6, v5, Lcom/google/android/location/internal/server/Listeners$Receiver;->listener:Lcom/google/android/location/internal/ILocationListener;

    invoke-interface {v6}, Lcom/google/android/location/internal/ILocationListener;->onProviderDisabled()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    iget-object v6, p0, Lcom/google/android/location/internal/server/Listeners;->locationIntentReceivers:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    if-nez v4, :cond_3

    invoke-direct {p0}, Lcom/google/android/location/internal/server/Listeners;->newCallbackIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v6, "providerEnabled"

    invoke-virtual {v4, v6, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/location/internal/server/Listeners$IntentReceiver;

    invoke-virtual {v5, p1, v4}, Lcom/google/android/location/internal/server/Listeners$IntentReceiver;->deliverIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "GmmNetworkLocationListeners"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "dropping intent receiver"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lcom/google/android/location/internal/server/Listeners$IntentReceiver;->pendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/location/internal/server/Listeners;->listenersChanged()V

    :cond_5
    return-void
.end method

.method reportLocation(Landroid/content/Context;Landroid/location/Location;Landroid/location/Location;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/location/Location;
    .param p3    # Landroid/location/Location;

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/location/internal/server/Listeners;->listeners:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/location/internal/server/Listeners$Receiver;

    :try_start_0
    iget-object v4, v3, Lcom/google/android/location/internal/server/Listeners$Receiver;->listener:Lcom/google/android/location/internal/ILocationListener;

    invoke-interface {v4, p2}, Lcom/google/android/location/internal/ILocationListener;->onLocationChanged(Landroid/location/Location;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v4, "GmmNetworkLocationListeners"

    const-string v5, "dropping listener"

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, v4}, Lcom/google/android/location/internal/server/Listeners;->notifyLocationIntentReceivers(Landroid/content/Context;Landroid/location/Location;Z)Z

    move-result v4

    or-int/2addr v0, v4

    const/4 v4, 0x1

    invoke-direct {p0, p1, p3, v4}, Lcom/google/android/location/internal/server/Listeners;->notifyLocationIntentReceivers(Landroid/content/Context;Landroid/location/Location;Z)Z

    move-result v4

    or-int/2addr v0, v4

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/location/internal/server/Listeners;->listenersChanged()V

    :cond_1
    return-void
.end method
