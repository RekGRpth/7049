.class Lcom/google/android/location/internal/server/ServiceThread;
.super Landroid/os/Handler;
.source "ServiceThread.java"

# interfaces
.implements Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;
.implements Lcom/google/android/location/os/real/RealOs$LocationReceiver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/internal/server/ServiceThread$EnabledObserver;
    }
.end annotation


# instance fields
.field private final androidNlpVersionInfo:Lcom/google/android/location/internal/NlpVersionInfo;

.field private final context:Landroid/content/Context;

.field private created:Z

.field private enabled:Z

.field private enabledCursor:Landroid/database/Cursor;

.field private enabledObserver:Lcom/google/android/location/internal/server/ServiceThread$EnabledObserver;

.field private eventLog:Lcom/google/android/location/os/EventLog;

.field private lastKnownLocation:Lcom/google/android/location/os/real/RealLocation;

.field private lastPeriodStartTimeMillis:J

.field private final listeners:Lcom/google/android/location/internal/server/Listeners;

.field private final locationManager:Landroid/location/LocationManager;

.field private final lock:Ljava/lang/Object;

.field private migrated:Z

.field private modelState:Lcom/google/android/location/cache/ModelState;

.field private os:Lcom/google/android/location/os/real/RealOs;

.field private osActivityDetectionEnabled:Z

.field private osPeriodSecs:J

.field periodTracker:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private queryMap:Landroid/content/ContentQueryMap;

.field private final recentReportedLocations:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Looper;

    const-wide/16 v3, -0x1

    const/16 v2, 0xa

    const/4 v1, 0x0

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-boolean v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabled:Z

    iput-boolean v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->migrated:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/location/internal/server/ServiceThread$1;

    invoke-direct {v0, p0, v2}, Lcom/google/android/location/internal/server/ServiceThread$1;-><init>(Lcom/google/android/location/internal/server/ServiceThread;I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->recentReportedLocations:Ljava/util/LinkedHashMap;

    iput-boolean v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->created:Z

    iput-wide v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->osPeriodSecs:J

    iput-boolean v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->osActivityDetectionEnabled:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastKnownLocation:Lcom/google/android/location/os/real/RealLocation;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->periodTracker:Ljava/util/Map;

    iput-wide v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastPeriodStartTimeMillis:J

    iput-object p1, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/location/internal/server/ServiceThread;->killClientLibLogging()V

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->locationManager:Landroid/location/LocationManager;

    sget-object v0, Lcom/google/android/location/internal/NlpVersionInfo$NlpApk;->ANDROID:Lcom/google/android/location/internal/NlpVersionInfo$NlpApk;

    invoke-static {v0, p1}, Lcom/google/android/location/internal/NlpVersionInfo;->getNlpVersionInfo(Lcom/google/android/location/internal/NlpVersionInfo$NlpApk;Landroid/content/Context;)Lcom/google/android/location/internal/NlpVersionInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->androidNlpVersionInfo:Lcom/google/android/location/internal/NlpVersionInfo;

    new-instance v0, Lcom/google/android/location/internal/server/Listeners;

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->androidNlpVersionInfo:Lcom/google/android/location/internal/NlpVersionInfo;

    iget v1, v1, Lcom/google/android/location/internal/NlpVersionInfo;->releaseVersion:I

    invoke-direct {v0, v1}, Lcom/google/android/location/internal/server/Listeners;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->listeners:Lcom/google/android/location/internal/server/Listeners;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/location/internal/server/ServiceThread;)V
    .locals 0
    .param p0    # Lcom/google/android/location/internal/server/ServiceThread;

    invoke-direct {p0}, Lcom/google/android/location/internal/server/ServiceThread;->updateState()V

    return-void
.end method

.method private createExtras(Landroid/location/Location;Lcom/google/android/location/data/NetworkLocation;Lcom/google/android/location/data/TravelDetectionType;Z)Landroid/os/Bundle;
    .locals 5
    .param p1    # Landroid/location/Location;
    .param p2    # Lcom/google/android/location/data/NetworkLocation;
    .param p3    # Lcom/google/android/location/data/TravelDetectionType;
    .param p4    # Z

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p2, Lcom/google/android/location/data/NetworkLocation;->bestResult:Lcom/google/android/location/data/LocatorResult;

    iget-object v3, p2, Lcom/google/android/location/data/NetworkLocation;->glsResult:Lcom/google/android/location/data/GlsLocatorResult;

    if-ne v0, v3, :cond_2

    const-string v3, "networkLocationSource"

    const-string v4, "server"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    if-eqz p3, :cond_1

    sget-object v3, Lcom/google/android/location/data/TravelDetectionType;->UNKNOWN:Lcom/google/android/location/data/TravelDetectionType;

    if-eq p3, v3, :cond_1

    const-string v3, "travelState"

    invoke-virtual {p3}, Lcom/google/android/location/data/TravelDetectionType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-object v1

    :cond_2
    if-eqz p4, :cond_3

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0, p1, p2, v3, v4}, Lcom/google/android/location/internal/server/ServiceThread;->getLocationDebugInfo(Landroid/location/Location;Ljava/lang/Object;Ljava/lang/String;Z)[B

    move-result-object v2

    if-eqz v2, :cond_3

    const-string v3, "dbgProtoBuf"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    :cond_3
    const-string v3, "networkLocationSource"

    const-string v4, "cached"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p2, Lcom/google/android/location/data/NetworkLocation;->cellResult:Lcom/google/android/location/data/CellLocatorResult;

    if-ne v0, v3, :cond_4

    const-string v3, "networkLocationType"

    const-string v4, "cell"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v3, p2, Lcom/google/android/location/data/NetworkLocation;->wifiResult:Lcom/google/android/location/data/WifiLocatorResult;

    if-ne v0, v3, :cond_0

    const-string v3, "networkLocationType"

    const-string v4, "wifi"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p2, Lcom/google/android/location/data/NetworkLocation;->wifiResult:Lcom/google/android/location/data/WifiLocatorResult;

    iget-object v3, v3, Lcom/google/android/location/data/WifiLocatorResult;->position:Lcom/google/android/location/data/Position;

    if-eqz v3, :cond_5

    iget-object v3, p2, Lcom/google/android/location/data/NetworkLocation;->wifiResult:Lcom/google/android/location/data/WifiLocatorResult;

    iget-object v3, v3, Lcom/google/android/location/data/WifiLocatorResult;->position:Lcom/google/android/location/data/Position;

    iget-object v3, v3, Lcom/google/android/location/data/Position;->levelId:Ljava/lang/String;

    if-eqz v3, :cond_5

    const-string v3, "levelId"

    iget-object v4, p2, Lcom/google/android/location/data/NetworkLocation;->wifiResult:Lcom/google/android/location/data/WifiLocatorResult;

    iget-object v4, v4, Lcom/google/android/location/data/WifiLocatorResult;->position:Lcom/google/android/location/data/Position;

    iget-object v4, v4, Lcom/google/android/location/data/Position;->levelId:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v3, p2, Lcom/google/android/location/data/NetworkLocation;->wifiResult:Lcom/google/android/location/data/WifiLocatorResult;

    iget-object v3, v3, Lcom/google/android/location/data/WifiLocatorResult;->position:Lcom/google/android/location/data/Position;

    if-eqz v3, :cond_0

    iget-object v3, p2, Lcom/google/android/location/data/NetworkLocation;->wifiResult:Lcom/google/android/location/data/WifiLocatorResult;

    iget-object v3, v3, Lcom/google/android/location/data/WifiLocatorResult;->position:Lcom/google/android/location/data/Position;

    iget v3, v3, Lcom/google/android/location/data/Position;->levelNumberE3:I

    const/high16 v4, -0x80000000

    if-eq v3, v4, :cond_0

    const-string v3, "levelNumberE3"

    iget-object v4, p2, Lcom/google/android/location/data/NetworkLocation;->wifiResult:Lcom/google/android/location/data/WifiLocatorResult;

    iget-object v4, v4, Lcom/google/android/location/data/WifiLocatorResult;->position:Lcom/google/android/location/data/Position;

    iget v4, v4, Lcom/google/android/location/data/Position;->levelNumberE3:I

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private createLocation(Lcom/google/android/location/data/LocatorResult;)Landroid/location/Location;
    .locals 7
    .param p1    # Lcom/google/android/location/data/LocatorResult;

    const-wide v5, 0x416312d000000000L

    new-instance v1, Landroid/location/Location;

    const-string v3, "network"

    invoke-direct {v1, v3}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/location/data/LocatorResult;->position:Lcom/google/android/location/data/Position;

    iget v3, v2, Lcom/google/android/location/data/Position;->latE7:I

    int-to-double v3, v3

    div-double/2addr v3, v5

    invoke-virtual {v1, v3, v4}, Landroid/location/Location;->setLatitude(D)V

    iget v3, v2, Lcom/google/android/location/data/Position;->lngE7:I

    int-to-double v3, v3

    div-double/2addr v3, v5

    invoke-virtual {v1, v3, v4}, Landroid/location/Location;->setLongitude(D)V

    iget v3, v2, Lcom/google/android/location/data/Position;->accuracyMm:I

    int-to-float v3, v3

    const/high16 v4, 0x447a0000

    div-float v0, v3, v4

    const/high16 v3, 0x3f800000

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v3

    invoke-virtual {v1, v3}, Landroid/location/Location;->setAccuracy(F)V

    iget-wide v3, p1, Lcom/google/android/location/data/LocatorResult;->reportTime:J

    invoke-static {}, Lcom/google/android/location/os/real/RealOs;->bootTimeMillis()J

    move-result-wide v5

    add-long/2addr v3, v5

    invoke-virtual {v1, v3, v4}, Landroid/location/Location;->setTime(J)V

    return-object v1
.end method

.method private static final killClientLibLogging()V
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    const-class v1, Lcom/google/gmm/debug/SimpleLogger;

    const-string v2, "activationLevel"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-static {}, Lcom/google/gmm/debug/Log;->getLogger()Lcom/google/gmm/debug/Logger;

    move-result-object v2

    const/4 v3, 0x7

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    if-eqz v0, :cond_0

    const-string v1, "gmmNlpServiceThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception while modifying log-level: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private final locationProviderEnabled(Landroid/content/Context;)Z
    .locals 2
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->locationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private final locationServicesUserAgreed(Landroid/content/Context;)Z
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "network_location_opt_in"

    const/4 v3, -0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private startOrStopLocked()V
    .locals 3

    const-string v1, "gmmNlpServiceThread"

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->androidNlpVersionInfo:Lcom/google/android/location/internal/NlpVersionInfo;

    invoke-virtual {v2}, Lcom/google/android/location/internal/NlpVersionInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->androidNlpVersionInfo:Lcom/google/android/location/internal/NlpVersionInfo;

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/google/android/location/internal/server/ServiceThread;->locationProviderEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "gmmNlpServiceThread"

    const-string v2, "This NLP should run continuously."

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/location/internal/NlpVersionInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :goto_0
    return-void

    :cond_0
    const-string v1, "gmmNlpServiceThread"

    const-string v2, "This NLP should stop in the absence of clients."

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/location/internal/NlpVersionInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_0
.end method

.method private updateActivityDetectionEnabledLocked()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->listeners:Lcom/google/android/location/internal/server/Listeners;

    invoke-virtual {v1}, Lcom/google/android/location/internal/server/Listeners;->getActivityListenerCount()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "gmmNlpServiceThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateActivityEnabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " os: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->osActivityDetectionEnabled:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " new:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->osActivityDetectionEnabled:Z

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    invoke-virtual {v1, v0}, Lcom/google/android/location/os/real/RealOs;->setActivityDetectionEnabled(Z)V

    iput-boolean v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->osActivityDetectionEnabled:Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateLastKnownUsingAndroidNlpIfNeeded(J)V
    .locals 6
    .param p1    # J

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastKnownLocation:Lcom/google/android/location/os/real/RealLocation;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/location/os/real/RealOs;->sinceBootMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastKnownLocation:Lcom/google/android/location/os/real/RealLocation;

    invoke-virtual {v3}, Lcom/google/android/location/os/real/RealLocation;->getTimeSinceBoot()J

    move-result-wide v3

    sub-long/2addr v1, v3

    cmp-long v1, v1, p1

    if-lez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->locationManager:Landroid/location/LocationManager;

    const-string v2, "network"

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/location/os/real/RealOs;->sinceEpochMillis()J

    move-result-wide v1

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    cmp-long v1, v1, p1

    if-gtz v1, :cond_1

    const-string v1, "gmmNlpServiceThread"

    const-string v2, "Using lastKnownLocation of Android NLP"

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/location/os/real/RealLocation;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-static {}, Lcom/google/android/location/os/real/RealOs;->bootTimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const/4 v4, 0x0

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/location/os/real/RealLocation;-><init>(Landroid/location/Location;JI)V

    iput-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastKnownLocation:Lcom/google/android/location/os/real/RealLocation;

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->recentReportedLocations:Ljava/util/LinkedHashMap;

    new-instance v2, Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastKnownLocation:Lcom/google/android/location/os/real/RealLocation;

    invoke-virtual {v3}, Lcom/google/android/location/os/real/RealLocation;->getLocation()Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v3}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/lang/Long;-><init>(J)V

    iget-object v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastKnownLocation:Lcom/google/android/location/os/real/RealLocation;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method private updatePeriodLocked(Z)V
    .locals 11
    .param p1    # Z

    iget-object v7, p0, Lcom/google/android/location/internal/server/ServiceThread;->listeners:Lcom/google/android/location/internal/server/Listeners;

    invoke-virtual {v7}, Lcom/google/android/location/internal/server/Listeners;->getMinPeriodSecs()I

    move-result v6

    if-nez p1, :cond_0

    int-to-long v7, v6

    iget-wide v9, p0, Lcom/google/android/location/internal/server/ServiceThread;->osPeriodSecs:J

    cmp-long v7, v7, v9

    if-eqz v7, :cond_1

    :cond_0
    iget-object v7, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    invoke-virtual {v7, v6, p1}, Lcom/google/android/location/os/real/RealOs;->setPeriod(IZ)V

    :cond_1
    invoke-static {}, Lcom/google/android/location/os/real/RealOs;->sinceBootMillis()J

    move-result-wide v4

    iget-wide v7, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastPeriodStartTimeMillis:J

    const-wide/16 v9, -0x1

    cmp-long v7, v7, v9

    if-nez v7, :cond_3

    iput-wide v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastPeriodStartTimeMillis:J

    :cond_2
    :goto_0
    int-to-long v7, v6

    iput-wide v7, p0, Lcom/google/android/location/internal/server/ServiceThread;->osPeriodSecs:J

    return-void

    :cond_3
    iget-wide v7, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastPeriodStartTimeMillis:J

    sub-long v0, v4, v7

    iput-wide v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastPeriodStartTimeMillis:J

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v7, p0, Lcom/google/android/location/internal/server/ServiceThread;->periodTracker:Ljava/util/Map;

    invoke-interface {v7, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    if-nez v3, :cond_4

    iget-object v7, p0, Lcom/google/android/location/internal/server/ServiceThread;->periodTracker:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7

    const/16 v8, 0xa

    if-ge v7, v8, :cond_2

    iget-object v7, p0, Lcom/google/android/location/internal/server/ServiceThread;->periodTracker:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    add-long/2addr v0, v7

    iget-object v7, p0, Lcom/google/android/location/internal/server/ServiceThread;->periodTracker:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private updateState()V
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->created:Z

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    invoke-direct {p0, v4}, Lcom/google/android/location/internal/server/ServiceThread;->locationProviderEnabled(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_6

    move v1, v2

    :goto_0
    iget-object v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabledCursor:Landroid/database/Cursor;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    invoke-direct {p0, v4}, Lcom/google/android/location/internal/server/ServiceThread;->locationServicesUserAgreed(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_7

    move v0, v2

    :goto_1
    iget-object v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v3

    if-nez v0, :cond_0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabled:Z

    if-eq v2, v0, :cond_0

    const-string v2, "gmmNlpServiceThread"

    const-string v4, "Sending NLP deactivated msg"

    invoke-static {v2, v4}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabled:Z

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->listeners:Lcom/google/android/location/internal/server/Listeners;

    iget-object v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    invoke-virtual {v2, v4, v0}, Lcom/google/android/location/internal/server/Listeners;->reportEnabled(Landroid/content/Context;Z)V

    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_8

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    if-nez v2, :cond_8

    iget-object v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-boolean v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->created:Z

    if-eqz v2, :cond_1

    const-string v2, "gmmNlpServiceThread"

    const-string v4, "Creating RealOs"

    invoke-static {v2, v4}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/location/os/real/RealOs;

    iget-object v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/location/internal/server/ServiceThread;->eventLog:Lcom/google/android/location/os/EventLog;

    invoke-direct {v2, v4, v5, p0}, Lcom/google/android/location/os/real/RealOs;-><init>(Landroid/content/Context;Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/real/RealOs$LocationReceiver;)V

    iput-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    invoke-static {v2}, Lcom/google/android/location/cache/ModelState;->create(Lcom/google/android/location/os/Os;)Lcom/google/android/location/cache/ModelState;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->modelState:Lcom/google/android/location/cache/ModelState;

    new-instance v2, Lcom/google/android/location/NetworkProvider;

    iget-object v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    iget-object v5, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    invoke-virtual {v5}, Lcom/google/android/location/os/real/RealOs;->getNlpParamsState()Lcom/google/android/location/os/NlpParametersState;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/location/internal/server/ServiceThread;->modelState:Lcom/google/android/location/cache/ModelState;

    invoke-direct {v2, v4, v5, v6}, Lcom/google/android/location/NetworkProvider;-><init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/os/NlpParametersState;Lcom/google/android/location/cache/ModelState;)V

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/location/internal/server/ServiceThread;->updatePeriodLocked(Z)V

    invoke-direct {p0}, Lcom/google/android/location/internal/server/ServiceThread;->updateActivityDetectionEnabledLocked()V

    :cond_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    :goto_2
    iget-object v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v3

    if-eqz v0, :cond_3

    :try_start_2
    iget-boolean v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabled:Z

    if-eq v2, v0, :cond_3

    const-string v2, "gmmNlpServiceThread"

    const-string v4, "Sending NLP activated msg"

    invoke-static {v2, v4}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabled:Z

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->listeners:Lcom/google/android/location/internal/server/Listeners;

    iget-object v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    invoke-virtual {v2, v4, v0}, Lcom/google/android/location/internal/server/Listeners;->reportEnabled(Landroid/content/Context;Z)V

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.google.android.location.internal.server.ACTION_RESTARTED"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_3
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    iget-boolean v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabled:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/location/os/real/RealOs;->deleteState(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->modelState:Lcom/google/android/location/cache/ModelState;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->modelState:Lcom/google/android/location/cache/ModelState;

    invoke-virtual {v2}, Lcom/google/android/location/cache/ModelState;->deleteCacheFiles()V

    :cond_4
    if-nez v1, :cond_5

    const-string v2, "gmmNlpServiceThread"

    const-string v3, "Destroying our service"

    invoke-static {v2, v3}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->androidNlpVersionInfo:Lcom/google/android/location/internal/NlpVersionInfo;

    iget-object v3, v3, Lcom/google/android/location/internal/NlpVersionInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v2, v3}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    :cond_5
    return-void

    :cond_6
    move v1, v3

    goto/16 :goto_0

    :cond_7
    move v0, v3

    goto/16 :goto_1

    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    :catchall_1
    move-exception v2

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    :cond_8
    if-nez v0, :cond_2

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    if-eqz v2, :cond_2

    const-string v2, "gmmNlpServiceThread"

    const-string v3, "Destroying RealOs"

    invoke-static {v2, v3}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    invoke-virtual {v2, v0}, Lcom/google/android/location/os/real/RealOs;->quit(Z)V

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    invoke-virtual {v2}, Lcom/google/android/location/os/real/RealOs;->join()V

    iget-object v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v3

    const/4 v2, 0x0

    :try_start_5
    iput-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastKnownLocation:Lcom/google/android/location/os/real/RealLocation;

    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->osPeriodSecs:J

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->osActivityDetectionEnabled:Z

    monitor-exit v3

    goto :goto_2

    :catchall_2
    move-exception v2

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v2

    :catchall_3
    move-exception v2

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v2
.end method


# virtual methods
.method public addActivityPendingIntent(Landroid/app/PendingIntent;)V
    .locals 4
    .param p1    # Landroid/app/PendingIntent;

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    const-string v0, "gmmNlpServiceThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "adding activity pendingIntent "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->listeners:Lcom/google/android/location/internal/server/Listeners;

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    invoke-virtual {v0, v2, p1}, Lcom/google/android/location/internal/server/Listeners;->addActivityPendingIntent(Landroid/content/Context;Landroid/app/PendingIntent;)V

    invoke-direct {p0}, Lcom/google/android/location/internal/server/ServiceThread;->updateActivityDetectionEnabledLocked()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addLocationListener(Lcom/google/android/location/internal/ILocationListener;II)V
    .locals 9
    .param p1    # Lcom/google/android/location/internal/ILocationListener;
    .param p2    # I
    .param p3    # I

    mul-int/lit16 v3, p3, 0x3e8

    int-to-long v1, v3

    iget-object v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    const-string v3, "gmmNlpServiceThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "adding listener "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " with period "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, -0x1

    if-eq p3, v3, :cond_0

    invoke-direct {p0, v1, v2}, Lcom/google/android/location/internal/server/ServiceThread;->updateLastKnownUsingAndroidNlpIfNeeded(J)V

    iget-object v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastKnownLocation:Lcom/google/android/location/os/real/RealLocation;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/google/android/location/os/real/RealOs;->sinceBootMillis()J

    move-result-wide v5

    iget-object v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastKnownLocation:Lcom/google/android/location/os/real/RealLocation;

    invoke-virtual {v3}, Lcom/google/android/location/os/real/RealLocation;->getTimeSinceBoot()J

    move-result-wide v7

    sub-long/2addr v5, v7

    cmp-long v3, v5, v1

    if-gtz v3, :cond_0

    const-string v3, "gmmNlpServiceThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "reporting last known "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastKnownLocation:Lcom/google/android/location/os/real/RealLocation;

    invoke-virtual {v6}, Lcom/google/android/location/os/real/RealLocation;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastKnownLocation:Lcom/google/android/location/os/real/RealLocation;

    invoke-virtual {v3}, Lcom/google/android/location/os/real/RealLocation;->getLocation()Landroid/location/Location;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/google/android/location/internal/ILocationListener;->onLocationChanged(Landroid/location/Location;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    iget-object v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->listeners:Lcom/google/android/location/internal/server/Listeners;

    const/4 v5, 0x5

    invoke-static {p2, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-virtual {v3, p1, v5}, Lcom/google/android/location/internal/server/Listeners;->add(Lcom/google/android/location/internal/ILocationListener;I)V

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/google/android/location/internal/server/ServiceThread;->updatePeriodLocked(Z)V

    monitor-exit v4

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v3, "gmmNlpServiceThread"

    const-string v5, "not adding new listener"

    invoke-static {v3, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public addLocationListenerStats(Ljava/io/PrintWriter;)V
    .locals 11
    .param p1    # Ljava/io/PrintWriter;

    iget-object v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NLP-Period is currently "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v5, p0, Lcom/google/android/location/internal/server/ServiceThread;->osPeriodSecs:J

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->periodTracker:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/android/location/internal/server/ServiceThread;->periodTracker:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-wide v5, p0, Lcom/google/android/location/internal/server/ServiceThread;->osPeriodSecs:J

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v7, v3

    cmp-long v3, v5, v7

    if-nez v3, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {}, Lcom/google/android/location/os/real/RealOs;->sinceBootMillis()J

    move-result-wide v7

    iget-wide v9, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastPeriodStartTimeMillis:J

    sub-long/2addr v7, v9

    add-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NLP-Period interval "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const v6, 0x7fffffff

    if-ne v5, v6, :cond_1

    const-string v2, "<no-client>"

    :cond_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", duration was "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " seconds"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_2
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public addLocationPendingIntent(Landroid/app/PendingIntent;IZ)V
    .locals 7
    .param p1    # Landroid/app/PendingIntent;
    .param p2    # I
    .param p3    # Z

    iget-object v6, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    const-string v0, "gmmNlpServiceThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "adding pendingIntent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with period "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->listeners:Lcom/google/android/location/internal/server/Listeners;

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    const/4 v2, 0x5

    invoke-static {p2, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    const/4 v4, 0x0

    move-object v2, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/location/internal/server/Listeners;->addLocationPendingIntent(Landroid/content/Context;Landroid/app/PendingIntent;ILandroid/location/Location;Z)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/ServiceThread;->updatePeriodLocked(Z)V

    monitor-exit v6

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public createService()V
    .locals 8

    iget-object v6, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    invoke-static {p0}, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;->addListener(Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->created:Z

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabledCursor:Landroid/database/Cursor;

    if-nez v1, :cond_0

    const-string v1, "gmmNlpServiceThread"

    const-string v2, "start monitoring enabled"

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "(name=?)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "network_location_opt_in"

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabledCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabledCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    new-instance v1, Landroid/content/ContentQueryMap;

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabledCursor:Landroid/database/Cursor;

    const-string v3, "name"

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4, p0}, Landroid/content/ContentQueryMap;-><init>(Landroid/database/Cursor;Ljava/lang/String;ZLandroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->queryMap:Landroid/content/ContentQueryMap;

    new-instance v1, Lcom/google/android/location/internal/server/ServiceThread$EnabledObserver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/location/internal/server/ServiceThread$EnabledObserver;-><init>(Lcom/google/android/location/internal/server/ServiceThread;Lcom/google/android/location/internal/server/ServiceThread$1;)V

    iput-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabledObserver:Lcom/google/android/location/internal/server/ServiceThread$EnabledObserver;

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->queryMap:Landroid/content/ContentQueryMap;

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabledObserver:Lcom/google/android/location/internal/server/ServiceThread$EnabledObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentQueryMap;->addObserver(Ljava/util/Observer;)V

    :cond_0
    :goto_0
    const/4 v1, 0x1

    invoke-static {p0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    invoke-direct {p0}, Lcom/google/android/location/internal/server/ServiceThread;->startOrStopLocked()V

    monitor-exit v6

    return-void

    :cond_1
    const-string v1, "gmmNlpServiceThread"

    const-string v2, "Couldn\'t get a cursor to track opt in; disabling service."

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public destroyService()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;->removeListener(Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->created:Z

    iget-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabledCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    const-string v0, "gmmNlpServiceThread"

    const-string v2, "stop monitoring enabled"

    invoke-static {v0, v2}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->queryMap:Landroid/content/ContentQueryMap;

    iget-object v2, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabledObserver:Lcom/google/android/location/internal/server/ServiceThread$EnabledObserver;

    invoke-virtual {v0, v2}, Landroid/content/ContentQueryMap;->deleteObserver(Ljava/util/Observer;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->queryMap:Landroid/content/ContentQueryMap;

    invoke-virtual {v0}, Landroid/content/ContentQueryMap;->close()V

    iget-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabledCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabledObserver:Lcom/google/android/location/internal/server/ServiceThread$EnabledObserver;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->queryMap:Landroid/content/ContentQueryMap;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->enabledCursor:Landroid/database/Cursor;

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/location/os/real/RealOs;->quit(Z)V

    :cond_1
    const/4 v0, 0x1

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    invoke-direct {p0}, Lcom/google/android/location/internal/server/ServiceThread;->startOrStopLocked()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getLastKnownLocation()Lcom/google/android/location/os/LocationInterface;
    .locals 2

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastKnownLocation:Lcom/google/android/location/os/real/RealLocation;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method getLocationDebugInfo(Landroid/location/Location;Ljava/lang/Object;Ljava/lang/String;Z)[B
    .locals 17
    .param p1    # Landroid/location/Location;
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    new-instance v9, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v12, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST_ELEMENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v9, v12}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    new-instance v8, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v12, Lcom/google/android/location/protocol/LocserverMessageTypes;->NLP_ELEMENT_APP_DATA:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v8, v12}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    if-eqz p3, :cond_0

    const/4 v12, 0x5

    move-object/from16 v0, p3

    invoke-virtual {v8, v12, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_0
    new-instance v6, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v12, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOCATION:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v6, v12}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/4 v12, 0x3

    invoke-virtual {v9, v12, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v12

    if-eqz v12, :cond_1

    const/4 v12, 0x3

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getAccuracy()F

    move-result v13

    float-to-int v13, v13

    invoke-virtual {v6, v12, v13}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_1
    new-instance v5, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v12, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLAT_LNG:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v5, v12}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/4 v12, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v13

    const-wide v15, 0x416312d000000000L

    mul-double/2addr v13, v15

    double-to-int v13, v13

    invoke-virtual {v5, v12, v13}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v12, 0x2

    invoke-virtual/range {p1 .. p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v13

    const-wide v15, 0x416312d000000000L

    mul-double/2addr v13, v15

    double-to-int v13, v13

    invoke-virtual {v5, v12, v13}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v12, 0x1

    invoke-virtual {v6, v12, v5}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    if-nez p2, :cond_3

    const/4 v12, 0x6

    const/4 v13, 0x0

    invoke-virtual {v8, v12, v13}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_2
    :goto_0
    const/4 v12, 0x7

    :try_start_0
    invoke-virtual {v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v13

    invoke-virtual {v9, v12, v13}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->addBytes(I[B)V

    const/4 v12, 0x6

    const/4 v13, 0x2

    invoke-virtual {v9, v12, v13}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->addInt(II)V

    invoke-virtual {v9}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    :goto_1
    return-object v12

    :cond_3
    move-object/from16 v0, p2

    instance-of v12, v0, Lcom/google/android/location/data/NetworkLocation;

    if-eqz v12, :cond_7

    move-object/from16 v7, p2

    check-cast v7, Lcom/google/android/location/data/NetworkLocation;

    iget-object v12, v7, Lcom/google/android/location/data/NetworkLocation;->bestResult:Lcom/google/android/location/data/LocatorResult;

    iget-object v13, v7, Lcom/google/android/location/data/NetworkLocation;->cellResult:Lcom/google/android/location/data/CellLocatorResult;

    if-ne v12, v13, :cond_6

    const/4 v12, 0x6

    const/4 v13, 0x1

    invoke-virtual {v8, v12, v13}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_4
    :goto_2
    invoke-static {}, Lcom/google/android/location/os/real/RealOs;->bootTimeMillis()J

    move-result-wide v1

    iget-object v12, v7, Lcom/google/android/location/data/NetworkLocation;->cellResult:Lcom/google/android/location/data/CellLocatorResult;

    if-eqz v12, :cond_5

    iget-object v12, v7, Lcom/google/android/location/data/NetworkLocation;->cellResult:Lcom/google/android/location/data/CellLocatorResult;

    iget-object v3, v12, Lcom/google/android/location/data/CellLocatorResult;->cellStatus:Lcom/google/android/location/data/CellStatus;

    if-eqz v3, :cond_5

    move/from16 v0, p4

    invoke-virtual {v3, v9, v1, v2, v0}, Lcom/google/android/location/data/CellStatus;->addToRequestElement(Lcom/google/gmm/common/io/protocol/ProtoBuf;JZ)V

    :cond_5
    iget-object v12, v7, Lcom/google/android/location/data/NetworkLocation;->wifiResult:Lcom/google/android/location/data/WifiLocatorResult;

    if-eqz v12, :cond_2

    iget-object v12, v7, Lcom/google/android/location/data/NetworkLocation;->wifiResult:Lcom/google/android/location/data/WifiLocatorResult;

    iget-object v11, v12, Lcom/google/android/location/data/WifiLocatorResult;->wifiScan:Lcom/google/android/location/data/WifiScan;

    if-eqz v11, :cond_2

    const/4 v12, 0x0

    invoke-virtual {v11, v1, v2, v12}, Lcom/google/android/location/data/WifiScan;->createWifiProfile(JZ)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v10

    const/4 v12, 0x2

    invoke-virtual {v9, v12, v10}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_6
    iget-object v12, v7, Lcom/google/android/location/data/NetworkLocation;->bestResult:Lcom/google/android/location/data/LocatorResult;

    iget-object v13, v7, Lcom/google/android/location/data/NetworkLocation;->wifiResult:Lcom/google/android/location/data/WifiLocatorResult;

    if-ne v12, v13, :cond_4

    const/4 v12, 0x6

    const/4 v13, 0x2

    invoke-virtual {v8, v12, v13}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    goto :goto_2

    :cond_7
    move-object/from16 v0, p2

    instance-of v12, v0, Lcom/google/android/location/data/LocatorResult;

    if-eqz v12, :cond_8

    const/4 v12, 0x6

    const/4 v13, 0x3

    invoke-virtual {v8, v12, v13}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    goto :goto_0

    :cond_8
    move-object/from16 v0, p2

    instance-of v12, v0, Landroid/location/Location;

    if-eqz v12, :cond_2

    const/4 v12, 0x6

    const/4 v13, 0x4

    invoke-virtual {v8, v12, v13}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    goto :goto_0

    :catch_0
    move-exception v4

    const-string v12, "gmmNlpServiceThread"

    const-string v13, "getLocationDebugInfo"

    invoke-static {v12, v13, v4}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v12, 0x0

    goto :goto_1
.end method

.method public getReportedLocation(Landroid/location/Location;)Ljava/lang/Object;
    .locals 4
    .param p1    # Landroid/location/Location;

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->recentReportedLocations:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget-boolean v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->migrated:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->migrated:Z

    iget-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/os/real/RealOs;->migrateState(Landroid/content/Context;)V

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/location/internal/server/ServiceThread;->updateState()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/location/internal/server/ServiceThread;->startOrStopLocked()V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public locationReport(Lcom/google/android/location/data/NetworkLocation;Lcom/google/android/location/data/TravelDetectionType;)V
    .locals 10
    .param p1    # Lcom/google/android/location/data/NetworkLocation;
    .param p2    # Lcom/google/android/location/data/TravelDetectionType;

    const/4 v5, 0x0

    iget-object v4, p1, Lcom/google/android/location/data/NetworkLocation;->bestResult:Lcom/google/android/location/data/LocatorResult;

    invoke-direct {p0, v4}, Lcom/google/android/location/internal/server/ServiceThread;->createLocation(Lcom/google/android/location/data/LocatorResult;)Landroid/location/Location;

    move-result-object v1

    new-instance v2, Landroid/location/Location;

    invoke-direct {v2, v1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    const/4 v4, 0x1

    invoke-direct {p0, v1, p1, p2, v4}, Lcom/google/android/location/internal/server/ServiceThread;->createExtras(Landroid/location/Location;Lcom/google/android/location/data/NetworkLocation;Lcom/google/android/location/data/TravelDetectionType;Z)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v1, p1, p2, v5}, Lcom/google/android/location/internal/server/ServiceThread;->createExtras(Landroid/location/Location;Lcom/google/android/location/data/NetworkLocation;Lcom/google/android/location/data/TravelDetectionType;Z)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    invoke-virtual {v2, v0}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    iget-object v5, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->listeners:Lcom/google/android/location/internal/server/Listeners;

    iget-object v6, p0, Lcom/google/android/location/internal/server/ServiceThread;->context:Landroid/content/Context;

    invoke-virtual {v4, v6, v1, v2}, Lcom/google/android/location/internal/server/Listeners;->reportLocation(Landroid/content/Context;Landroid/location/Location;Landroid/location/Location;)V

    new-instance v4, Lcom/google/android/location/os/real/RealLocation;

    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    invoke-static {}, Lcom/google/android/location/os/real/RealOs;->bootTimeMillis()J

    move-result-wide v8

    sub-long/2addr v6, v8

    const/4 v8, 0x0

    invoke-direct {v4, v2, v6, v7, v8}, Lcom/google/android/location/os/real/RealLocation;-><init>(Landroid/location/Location;JI)V

    iput-object v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastKnownLocation:Lcom/google/android/location/os/real/RealLocation;

    iget-object v4, p0, Lcom/google/android/location/internal/server/ServiceThread;->recentReportedLocations:Ljava/util/LinkedHashMap;

    new-instance v6, Ljava/lang/Long;

    iget-object v7, p0, Lcom/google/android/location/internal/server/ServiceThread;->lastKnownLocation:Lcom/google/android/location/os/real/RealLocation;

    invoke-virtual {v7}, Lcom/google/android/location/os/real/RealLocation;->getLocation()Landroid/location/Location;

    move-result-object v7

    invoke-virtual {v7}, Landroid/location/Location;->getTime()J

    move-result-wide v7

    invoke-direct {v6, v7, v8}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v4, v6, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "gmmNlpServiceThread"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "reporting "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Landroid/location/Location;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/google/android/location/internal/server/ServiceThread;->updatePeriodLocked(Z)V

    monitor-exit v5

    return-void

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method public nlpPackageUpdated()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x2

    :try_start_0
    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeLocationListener(Lcom/google/android/location/internal/ILocationListener;)V
    .locals 3
    .param p1    # Lcom/google/android/location/internal/ILocationListener;

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    const-string v0, "gmmNlpServiceThread"

    const-string v2, "removing listener"

    invoke-static {v0, v2}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->listeners:Lcom/google/android/location/internal/server/Listeners;

    invoke-virtual {v0, p1}, Lcom/google/android/location/internal/server/Listeners;->remove(Lcom/google/android/location/internal/ILocationListener;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/location/internal/server/ServiceThread;->updatePeriodLocked(Z)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setEventLog(Lcom/google/android/location/os/EventLog;)V
    .locals 2
    .param p1    # Lcom/google/android/location/os/EventLog;

    iget-object v1, p0, Lcom/google/android/location/internal/server/ServiceThread;->lock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lcom/google/android/location/internal/server/ServiceThread;->eventLog:Lcom/google/android/location/os/EventLog;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method signalRmiRequest(Lcom/google/android/location/data/NetworkLocation;)V
    .locals 1
    .param p1    # Lcom/google/android/location/data/NetworkLocation;

    iget-object v0, p0, Lcom/google/android/location/internal/server/ServiceThread;->os:Lcom/google/android/location/os/real/RealOs;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/RealOs;->signalRmiRequest(Lcom/google/android/location/data/NetworkLocation;)V

    return-void
.end method
