.class final enum Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;
.super Ljava/lang/Enum;
.source "CompactRegressionTree.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "NodeType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

.field public static final enum INNER:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

.field public static final enum LEAF:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    const-string v1, "LEAF"

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;->LEAF:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    new-instance v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    const-string v1, "INNER"

    invoke-direct {v0, v1, v3}, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;->INNER:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    sget-object v1, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;->LEAF:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;->INNER:Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;->$VALUES:[Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;
    .locals 1

    const-class v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;
    .locals 1

    sget-object v0, Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;->$VALUES:[Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    invoke-virtual {v0}, [Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/location/learning/CompactRegressionTree$RegressionTreeNode$NodeType;

    return-object v0
.end method
