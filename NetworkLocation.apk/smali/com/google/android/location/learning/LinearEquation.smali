.class public Lcom/google/android/location/learning/LinearEquation;
.super Ljava/lang/Object;
.source "LinearEquation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/learning/LinearEquation$LinearTerm;
    }
.end annotation


# instance fields
.field private final terms:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/location/learning/LinearEquation$LinearTerm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/learning/LinearEquation;->terms:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/learning/LinearEquation;->terms:Ljava/util/ArrayList;

    return-void
.end method

.method private addTerm(Lcom/google/android/location/learning/LinearEquation$LinearTerm;)V
    .locals 1
    .param p1    # Lcom/google/android/location/learning/LinearEquation$LinearTerm;

    iget-object v0, p0, Lcom/google/android/location/learning/LinearEquation;->terms:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static read(Ljava/io/DataInputStream;)Lcom/google/android/location/learning/LinearEquation;
    .locals 4
    .param p0    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    new-instance v2, Lcom/google/android/location/learning/LinearEquation;

    invoke-direct {v2, v1}, Lcom/google/android/location/learning/LinearEquation;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-static {p0}, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->read(Ljava/io/DataInputStream;)Lcom/google/android/location/learning/LinearEquation$LinearTerm;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/location/learning/LinearEquation;->addTerm(Lcom/google/android/location/learning/LinearEquation$LinearTerm;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    if-ne p0, p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    instance-of v1, p1, Lcom/google/android/location/learning/LinearEquation;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/google/android/location/learning/LinearEquation;

    iget-object v1, p0, Lcom/google/android/location/learning/LinearEquation;->terms:Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/google/android/location/learning/LinearEquation;->terms:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public eval([F)F
    .locals 6
    .param p1    # [F

    const-wide/16 v1, 0x0

    iget-object v4, p0, Lcom/google/android/location/learning/LinearEquation;->terms:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/location/learning/LinearEquation$LinearTerm;

    iget v4, v3, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->index:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    iget v4, v3, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->coefficient:F

    float-to-double v4, v4

    add-double/2addr v1, v4

    goto :goto_0

    :cond_0
    iget v4, v3, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->coefficient:F

    iget v5, v3, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->index:I

    aget v5, p1, v5

    mul-float/2addr v4, v5

    float-to-double v4, v4

    add-double/2addr v1, v4

    goto :goto_0

    :cond_1
    double-to-float v4, v1

    return v4
.end method

.method public hashCode()I
    .locals 2

    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/location/learning/LinearEquation;->terms:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/location/learning/LinearEquation;->terms:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/learning/LinearEquation$LinearTerm;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/location/learning/LinearEquation$LinearTerm;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
