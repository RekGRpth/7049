.class public Lcom/google/android/location/localizer/DistanceDist;
.super Ljava/lang/Object;
.source "DistanceDist.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field final indexOffset:I

.field final metricValue:I

.field final numSamples:I

.field final prob:[F


# direct methods
.method constructor <init>(III[F)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # [F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/location/localizer/DistanceDist;->metricValue:I

    iput p3, p0, Lcom/google/android/location/localizer/DistanceDist;->indexOffset:I

    iput p2, p0, Lcom/google/android/location/localizer/DistanceDist;->numSamples:I

    iput-object p4, p0, Lcom/google/android/location/localizer/DistanceDist;->prob:[F

    return-void
.end method

.method static fromProto(Lcom/google/gmm/common/io/protocol/ProtoBuf;)Lcom/google/android/location/localizer/DistanceDist;
    .locals 9
    .param p0    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    invoke-virtual {p0, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-virtual {p0, v7}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p0, v7}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    :goto_0
    const/4 v6, 0x5

    invoke-virtual {p0, v6}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    invoke-virtual {p0, v8}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    new-array v4, v5, [F

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_1

    invoke-virtual {p0, v8, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v6

    aput v6, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    new-instance v6, Lcom/google/android/location/localizer/DistanceDist;

    invoke-direct {v6, v2, v3, v1, v4}, Lcom/google/android/location/localizer/DistanceDist;-><init>(III[F)V

    return-object v6
.end method
