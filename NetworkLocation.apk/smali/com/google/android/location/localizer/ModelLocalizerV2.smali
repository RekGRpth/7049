.class public Lcom/google/android/location/localizer/ModelLocalizerV2;
.super Ljava/lang/Object;
.source "ModelLocalizerV2.java"

# interfaces
.implements Lcom/google/android/location/localizer/WifiLocalizerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/localizer/ModelLocalizerV2$LevelClusterMatchResult;
    }
.end annotation


# instance fields
.field private final modelStateManager:Lcom/google/android/location/localizer/ModelStateManager;

.field private final normalizer:Lcom/google/android/location/localizer/WifiNormalizer;

.field private final os:Lcom/google/android/location/os/Os;

.field private previousScanTimeMillisSinceBoot:J

.field private previousStrengths:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/localizer/ModelStateManager;)V
    .locals 1
    .param p1    # Lcom/google/android/location/os/Os;
    .param p2    # Lcom/google/android/location/localizer/ModelStateManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/localizer/ModelLocalizerV2;->os:Lcom/google/android/location/os/Os;

    new-instance v0, Lcom/google/android/location/localizer/WifiNormalizer;

    invoke-direct {v0, p1}, Lcom/google/android/location/localizer/WifiNormalizer;-><init>(Lcom/google/android/location/os/Os;)V

    iput-object v0, p0, Lcom/google/android/location/localizer/ModelLocalizerV2;->normalizer:Lcom/google/android/location/localizer/WifiNormalizer;

    iput-object p2, p0, Lcom/google/android/location/localizer/ModelLocalizerV2;->modelStateManager:Lcom/google/android/location/localizer/ModelStateManager;

    return-void
.end method


# virtual methods
.method public getEstimatedPosition(Ljava/util/Map;Ljava/util/Map;J)Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;
    .locals 24
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;J)",
            "Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;"
        }
    .end annotation

    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/ModelLocalizerV2;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v4}, Lcom/google/android/location/os/Os;->millisSinceBoot()J

    move-result-wide v19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/ModelLocalizerV2;->os:Lcom/google/android/location/os/Os;

    invoke-interface {v4}, Lcom/google/android/location/os/Os;->millisSinceEpoch()J

    move-result-wide v21

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/location/localizer/ModelLocalizerV2;->previousScanTimeMillisSinceBoot:J

    sub-long v4, v19, v4

    const-wide/16 v6, 0x1f40

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/ModelLocalizerV2;->previousStrengths:Ljava/util/Map;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/ModelLocalizerV2;->previousStrengths:Ljava/util/Map;

    invoke-interface {v10, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/ModelLocalizerV2;->normalizer:Lcom/google/android/location/localizer/WifiNormalizer;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/google/android/location/localizer/WifiNormalizer;->normalize(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v10, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    const-string v4, "ModelLocalizerV2"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Orig AP count: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Augmented AP count: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v10}, Ljava/util/Map;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-wide/from16 v0, v19

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/location/localizer/ModelLocalizerV2;->previousScanTimeMillisSinceBoot:J

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/location/localizer/ModelLocalizerV2;->previousStrengths:Ljava/util/Map;

    move-object/from16 v0, p0

    move-wide/from16 v1, v21

    invoke-virtual {v0, v10, v1, v2}, Lcom/google/android/location/localizer/ModelLocalizerV2;->getLevelClusterMatch(Ljava/util/Map;J)Lcom/google/android/location/localizer/ModelLocalizerV2$LevelClusterMatchResult;

    move-result-object v14

    const-string v4, "ModelLocalizerV2"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cluster result is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v14}, Lcom/google/android/location/localizer/ModelLocalizerV2$LevelClusterMatchResult;->getConfidence()I

    move-result v4

    const/4 v5, 0x1

    if-lt v4, v5, :cond_4

    invoke-virtual {v14}, Lcom/google/android/location/localizer/ModelLocalizerV2$LevelClusterMatchResult;->getBestMatchCluster()Ljava/lang/String;

    move-result-object v11

    const-wide/16 v4, 0x4e20

    cmp-long v4, p3, v4

    if-gtz v4, :cond_1

    const/4 v13, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/ModelLocalizerV2;->modelStateManager:Lcom/google/android/location/localizer/ModelStateManager;

    move-wide/from16 v0, v21

    invoke-virtual {v4, v11, v0, v1, v13}, Lcom/google/android/location/localizer/ModelStateManager;->getLevelSelector(Ljava/lang/String;JZ)Lcom/google/android/location/data/LevelSelector;

    move-result-object v12

    if-nez v12, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/ModelLocalizerV2;->modelStateManager:Lcom/google/android/location/localizer/ModelStateManager;

    invoke-virtual {v4}, Lcom/google/android/location/localizer/ModelStateManager;->makeRequest()V

    const/16 v23, 0x0

    :goto_1
    return-object v23

    :cond_1
    const/4 v13, 0x0

    goto :goto_0

    :cond_2
    const-string v4, "ModelLocalizerV2"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Hit cached level selector: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v14}, Lcom/google/android/location/localizer/ModelLocalizerV2$LevelClusterMatchResult;->getBestMatchCluster()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Lcom/google/android/location/data/LevelSelector;->classifyFloor(Ljava/util/Map;)Lcom/google/android/location/data/LevelSelector$LevelResult;

    move-result-object v15

    const-string v4, "ModelLocalizerV2"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Level model result is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v15}, Lcom/google/android/location/data/LevelSelector$LevelResult;->getMostProbableLevel()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/ModelLocalizerV2;->modelStateManager:Lcom/google/android/location/localizer/ModelStateManager;

    move-wide/from16 v0, v21

    invoke-virtual {v4, v8, v0, v1, v13}, Lcom/google/android/location/localizer/ModelStateManager;->getLevelModel(Ljava/lang/String;JZ)Lcom/google/android/location/data/LevelModel;

    move-result-object v17

    if-nez v17, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/ModelLocalizerV2;->modelStateManager:Lcom/google/android/location/localizer/ModelStateManager;

    invoke-virtual {v4}, Lcom/google/android/location/localizer/ModelStateManager;->makeRequest()V

    const/16 v23, 0x0

    goto :goto_1

    :cond_3
    const-string v4, "ModelLocalizerV2"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Hit cached level model: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Lcom/google/android/location/data/LevelModel;->computeBestLocation(Ljava/util/Map;)Lcom/google/android/location/data/LevelModel$LevelModelResult;

    move-result-object v16

    const-string v4, "ModelLocalizerV2"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Level Model Result is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " time: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v21

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/location/data/Position;

    move-object/from16 v0, v16

    iget v4, v0, Lcom/google/android/location/data/LevelModel$LevelModelResult;->latE7:I

    move-object/from16 v0, v16

    iget v5, v0, Lcom/google/android/location/data/LevelModel$LevelModelResult;->lngE7:I

    move-object/from16 v0, v16

    iget v6, v0, Lcom/google/android/location/data/LevelModel$LevelModelResult;->accuracyMm:I

    invoke-virtual {v15}, Lcom/google/android/location/data/LevelSelector$LevelResult;->getProbability()F

    move-result v7

    const/high16 v9, 0x41200000

    mul-float/2addr v7, v9

    const/high16 v9, 0x3f800000

    sub-float/2addr v7, v9

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    add-int/lit8 v7, v7, 0x64

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/location/data/LevelModel;->getLevelNumberE3()I

    move-result v9

    invoke-direct/range {v3 .. v9}, Lcom/google/android/location/data/Position;-><init>(IIIILjava/lang/String;I)V

    new-instance v23, Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;

    invoke-virtual {v14}, Lcom/google/android/location/localizer/ModelLocalizerV2$LevelClusterMatchResult;->getConfidence()I

    move-result v4

    add-int/lit8 v4, v4, 0x64

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, v23

    invoke-direct {v0, v3, v4, v5}, Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;-><init>(Lcom/google/android/location/data/Position;ILjava/util/Set;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/ModelLocalizerV2;->modelStateManager:Lcom/google/android/location/localizer/ModelStateManager;

    invoke-virtual {v4}, Lcom/google/android/location/localizer/ModelStateManager;->makeRequest()V

    goto/16 :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/location/localizer/ModelLocalizerV2;->modelStateManager:Lcom/google/android/location/localizer/ModelStateManager;

    invoke-virtual {v4}, Lcom/google/android/location/localizer/ModelStateManager;->makeRequest()V

    const/16 v23, 0x0

    goto/16 :goto_1
.end method

.method getLevelClusterMatch(Ljava/util/Map;J)Lcom/google/android/location/localizer/ModelLocalizerV2$LevelClusterMatchResult;
    .locals 9
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;J)",
            "Lcom/google/android/location/localizer/ModelLocalizerV2$LevelClusterMatchResult;"
        }
    .end annotation

    new-instance v4, Lcom/google/android/location/localizer/ModelLocalizerV2$LevelClusterMatchResult;

    invoke-direct {v4}, Lcom/google/android/location/localizer/ModelLocalizerV2$LevelClusterMatchResult;-><init>()V

    const/4 v5, 0x0

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v6, p0, Lcom/google/android/location/localizer/ModelLocalizerV2;->modelStateManager:Lcom/google/android/location/localizer/ModelStateManager;

    invoke-virtual {v6, v2, v3, p2, p3}, Lcom/google/android/location/localizer/ModelStateManager;->getLevelClusterFeatureId(JJ)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v6, "ModelLocalizerV2"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Hit cached mac address: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, ""

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v4, v1}, Lcom/google/android/location/localizer/ModelLocalizerV2$LevelClusterMatchResult;->incrementMatchCount(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Lcom/google/android/location/localizer/ModelLocalizerV2$LevelClusterMatchResult;->computePrediction()V

    return-object v4
.end method
