.class Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;
.super Ljava/lang/Object;
.source "ModelRequestStack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/localizer/ModelRequestStack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UniqueStack"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final capacity:I

.field private final linkedList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<TT;>;"
        }
    .end annotation
.end field

.field private size:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->size:I

    iput p1, p0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->capacity:I

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->linkedList:Ljava/util/LinkedList;

    return-void
.end method


# virtual methods
.method public isEmpty()Z
    .locals 1

    iget v0, p0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->size:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pop()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->linkedList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->linkedList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    iget v1, p0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->size:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->size:I

    return-object v0
.end method

.method public push(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->linkedList:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->linkedList:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    if-nez v0, :cond_0

    iget v1, p0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->size:I

    iget v2, p0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->capacity:I

    if-ge v1, v2, :cond_1

    iget v1, p0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->size:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->size:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->linkedList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_0
.end method
