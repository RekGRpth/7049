.class public Lcom/google/android/location/localizer/ModelRequestStack;
.super Ljava/lang/Object;
.source "ModelRequestStack.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/localizer/ModelRequestStack$1;,
        Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;
    }
.end annotation


# instance fields
.field private levelRequests:Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack",
            "<",
            "Lcom/google/android/location/data/ModelRequest;",
            ">;"
        }
    .end annotation
.end field

.field private levelSelectorRequests:Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack",
            "<",
            "Lcom/google/android/location/data/ModelRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;

    invoke-direct {v0, v1}, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/localizer/ModelRequestStack;->levelSelectorRequests:Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;

    new-instance v0, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;

    invoke-direct {v0, v1}, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/localizer/ModelRequestStack;->levelRequests:Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;

    return-void
.end method


# virtual methods
.method public addRequest(Lcom/google/android/location/data/ModelRequest;)V
    .locals 3
    .param p1    # Lcom/google/android/location/data/ModelRequest;

    sget-object v0, Lcom/google/android/location/localizer/ModelRequestStack$1;->$SwitchMap$com$google$android$location$data$ModelRequest$Type:[I

    invoke-virtual {p1}, Lcom/google/android/location/data/ModelRequest;->getType()Lcom/google/android/location/data/ModelRequest$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/location/data/ModelRequest$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "ModelRequestQueue"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Programming error: unknown indoor request type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/location/data/ModelRequest;->getType()Lcom/google/android/location/data/ModelRequest$Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/location/localizer/ModelRequestStack;->levelSelectorRequests:Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;

    invoke-virtual {v0, p1}, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->push(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/location/localizer/ModelRequestStack;->levelRequests:Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;

    invoke-virtual {v0, p1}, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->push(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getNextRequest()Lcom/google/android/location/data/ModelRequest;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/localizer/ModelRequestStack;->levelRequests:Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;

    invoke-virtual {v0}, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/localizer/ModelRequestStack;->levelRequests:Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;

    invoke-virtual {v0}, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/data/ModelRequest;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/localizer/ModelRequestStack;->levelSelectorRequests:Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;

    invoke-virtual {v0}, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/localizer/ModelRequestStack;->levelSelectorRequests:Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;

    invoke-virtual {v0}, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/data/ModelRequest;

    goto :goto_0

    :cond_1
    const-string v0, "ModelRequestQueue"

    const-string v1, "Programming error: trying to get next request when there\'s none"

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRequest()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/localizer/ModelRequestStack;->levelRequests:Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;

    invoke-virtual {v0}, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/localizer/ModelRequestStack;->levelSelectorRequests:Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;

    invoke-virtual {v0}, Lcom/google/android/location/localizer/ModelRequestStack$UniqueStack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
