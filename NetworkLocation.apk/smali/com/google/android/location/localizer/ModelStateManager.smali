.class public Lcom/google/android/location/localizer/ModelStateManager;
.super Ljava/lang/Object;
.source "ModelStateManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/localizer/ModelStateManager$1;
    }
.end annotation


# instance fields
.field private isRequestPending:Z

.field private lastRequest:Lcom/google/android/location/data/ModelRequest;

.field private final modelState:Lcom/google/android/location/cache/ModelState;

.field private final os:Lcom/google/android/location/os/Os;

.field private final requestStack:Lcom/google/android/location/localizer/ModelRequestStack;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/Os;Lcom/google/android/location/cache/ModelState;)V
    .locals 1
    .param p1    # Lcom/google/android/location/os/Os;
    .param p2    # Lcom/google/android/location/cache/ModelState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/location/localizer/ModelRequestStack;

    invoke-direct {v0}, Lcom/google/android/location/localizer/ModelRequestStack;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->requestStack:Lcom/google/android/location/localizer/ModelRequestStack;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->isRequestPending:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->lastRequest:Lcom/google/android/location/data/ModelRequest;

    iput-object p1, p0, Lcom/google/android/location/localizer/ModelStateManager;->os:Lcom/google/android/location/os/Os;

    iput-object p2, p0, Lcom/google/android/location/localizer/ModelStateManager;->modelState:Lcom/google/android/location/cache/ModelState;

    return-void
.end method

.method private static createProtoBufRequest(Lcom/google/android/location/data/ModelRequest;)Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .locals 5
    .param p0    # Lcom/google/android/location/data/ModelRequest;

    new-instance v0, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/protocol/LocserverMessageTypes;->GSIGNAL_FINGERPRINT_MODEL_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/location/data/ModelRequest;->getType()Lcom/google/android/location/data/ModelRequest$Type;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/location/localizer/ModelStateManager;->getGSignalFingerprintModelRequest(Lcom/google/android/location/data/ModelRequest$Type;)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/android/location/data/ModelRequest;->getModelId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    new-instance v2, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST_ELEMENT:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/16 v3, 0xc

    invoke-virtual {v2, v3, v0}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    const/16 v3, 0xa

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->setInt(II)V

    new-instance v1, Lcom/google/gmm/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/android/location/protocol/LocserverMessageTypes;->GLOC_REQUEST:Lcom/google/gmm/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v3}, Lcom/google/gmm/common/io/protocol/ProtoBuf;-><init>(Lcom/google/gmm/common/io/protocol/ProtoBufType;)V

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/google/gmm/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/gmm/common/io/protocol/ProtoBuf;)V

    return-object v1
.end method

.method private static getGSignalFingerprintModelRequest(Lcom/google/android/location/data/ModelRequest$Type;)I
    .locals 3
    .param p0    # Lcom/google/android/location/data/ModelRequest$Type;

    sget-object v0, Lcom/google/android/location/localizer/ModelStateManager$1;->$SwitchMap$com$google$android$location$data$ModelRequest$Type:[I

    invoke-virtual {p0}, Lcom/google/android/location/data/ModelRequest$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Programming error: unsupported request type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x3

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public discardOldCacheEntries(J)V
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->modelState:Lcom/google/android/location/cache/ModelState;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/cache/ModelState;->discardOldCacheEntries(J)V

    return-void
.end method

.method public getLevelClusterFeatureId(JJ)Ljava/lang/String;
    .locals 1
    .param p1    # J
    .param p3    # J

    iget-object v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->modelState:Lcom/google/android/location/cache/ModelState;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/location/cache/ModelState;->getLevelClusterFeatureId(JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLevelModel(Ljava/lang/String;JZ)Lcom/google/android/location/data/LevelModel;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # Z

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/location/localizer/ModelStateManager;->modelState:Lcom/google/android/location/cache/ModelState;

    invoke-virtual {v2, p1}, Lcom/google/android/location/cache/ModelState;->hasLevelModelInMemoryOrDisk(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/location/localizer/ModelStateManager;->modelState:Lcom/google/android/location/cache/ModelState;

    invoke-virtual {v2, p1, p2, p3}, Lcom/google/android/location/cache/ModelState;->getLevelModel(Ljava/lang/String;J)Lcom/google/android/location/data/LevelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    if-eqz p4, :cond_2

    invoke-static {p1}, Lcom/google/android/location/data/ModelRequest;->newLevelRequest(Ljava/lang/String;)Lcom/google/android/location/data/ModelRequest;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/location/localizer/ModelStateManager;->queueRequest(Lcom/google/android/location/data/ModelRequest;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public getLevelSelector(Ljava/lang/String;JZ)Lcom/google/android/location/data/LevelSelector;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # Z

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/location/localizer/ModelStateManager;->modelState:Lcom/google/android/location/cache/ModelState;

    invoke-virtual {v2, p1}, Lcom/google/android/location/cache/ModelState;->hasLevelSelectorInMemoryOrDisk(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/location/localizer/ModelStateManager;->modelState:Lcom/google/android/location/cache/ModelState;

    invoke-virtual {v2, p1, p2, p3}, Lcom/google/android/location/cache/ModelState;->getLevelSelector(Ljava/lang/String;J)Lcom/google/android/location/data/LevelSelector;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    if-eqz p4, :cond_2

    invoke-static {p1}, Lcom/google/android/location/data/ModelRequest;->newLevelSelectorRequest(Ljava/lang/String;)Lcom/google/android/location/data/ModelRequest;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/location/localizer/ModelStateManager;->queueRequest(Lcom/google/android/location/data/ModelRequest;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public declared-synchronized glsModelQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;J)Z
    .locals 3
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p2    # J

    monitor-enter p0

    :try_start_0
    const-string v1, "ModelStateManager"

    const-string v2, "Received GLS model response..."

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/localizer/ModelStateManager;->modelState:Lcom/google/android/location/cache/ModelState;

    iget-object v2, p0, Lcom/google/android/location/localizer/ModelStateManager;->lastRequest:Lcom/google/android/location/data/ModelRequest;

    invoke-virtual {v1, p1, p2, p3, v2}, Lcom/google/android/location/cache/ModelState;->updateModelsFromGlsModelResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;JLcom/google/android/location/data/ModelRequest;)Z

    move-result v0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/location/localizer/ModelStateManager;->isRequestPending:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/location/localizer/ModelStateManager;->lastRequest:Lcom/google/android/location/data/ModelRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public glsQueryResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;J)Z
    .locals 2
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p2    # J

    const-string v0, "ModelStateManager"

    const-string v1, "Received GLS response (maybe update mac to cluster mappings)..."

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->modelState:Lcom/google/android/location/cache/ModelState;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/cache/ModelState;->updateMacsFromGlsResponse(Lcom/google/gmm/common/io/protocol/ProtoBuf;J)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized makeRequest()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->isRequestPending:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->requestStack:Lcom/google/android/location/localizer/ModelRequestStack;

    invoke-virtual {v0}, Lcom/google/android/location/localizer/ModelRequestStack;->hasRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ModelStateManager"

    const-string v1, "Sending GLS model request"

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->isRequestPending:Z

    iget-object v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->requestStack:Lcom/google/android/location/localizer/ModelRequestStack;

    invoke-virtual {v0}, Lcom/google/android/location/localizer/ModelRequestStack;->getNextRequest()Lcom/google/android/location/data/ModelRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->lastRequest:Lcom/google/android/location/data/ModelRequest;

    iget-object v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->os:Lcom/google/android/location/os/Os;

    iget-object v1, p0, Lcom/google/android/location/localizer/ModelStateManager;->lastRequest:Lcom/google/android/location/data/ModelRequest;

    invoke-static {v1}, Lcom/google/android/location/localizer/ModelStateManager;->createProtoBufRequest(Lcom/google/android/location/data/ModelRequest;)Lcom/google/gmm/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/location/os/Os;->glsModelQuery(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized queueRequest(Lcom/google/android/location/data/ModelRequest;)V
    .locals 3
    .param p1    # Lcom/google/android/location/data/ModelRequest;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->lastRequest:Lcom/google/android/location/data/ModelRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->lastRequest:Lcom/google/android/location/data/ModelRequest;

    invoke-virtual {v0, p1}, Lcom/google/android/location/data/ModelRequest;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v0, "ModelStateManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Queue "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/location/data/ModelRequest;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->requestStack:Lcom/google/android/location/localizer/ModelRequestStack;

    invoke-virtual {v0, p1}, Lcom/google/android/location/localizer/ModelRequestStack;->addRequest(Lcom/google/android/location/data/ModelRequest;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public saveState()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/localizer/ModelStateManager;->modelState:Lcom/google/android/location/cache/ModelState;

    invoke-virtual {v0}, Lcom/google/android/location/cache/ModelState;->save()V

    return-void
.end method
