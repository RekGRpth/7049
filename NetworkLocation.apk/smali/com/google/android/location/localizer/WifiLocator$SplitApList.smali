.class Lcom/google/android/location/localizer/WifiLocator$SplitApList;
.super Ljava/lang/Object;
.source "WifiLocator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/localizer/WifiLocator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SplitApList"
.end annotation


# instance fields
.field final knownAps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;"
        }
    .end annotation
.end field

.field final outliers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;"
        }
    .end annotation
.end field

.field final status:Lcom/google/android/location/data/LocatorResult$ResultStatus;

.field final synthetic this$0:Lcom/google/android/location/localizer/WifiLocator;


# direct methods
.method constructor <init>(Lcom/google/android/location/localizer/WifiLocator;Lcom/google/android/location/data/LocatorResult$ResultStatus;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .param p2    # Lcom/google/android/location/data/LocatorResult$ResultStatus;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/data/LocatorResult$ResultStatus;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/location/localizer/WifiLocator$SplitApList;->this$0:Lcom/google/android/location/localizer/WifiLocator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/location/localizer/WifiLocator$SplitApList;->status:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    iput-object p3, p0, Lcom/google/android/location/localizer/WifiLocator$SplitApList;->knownAps:Ljava/util/Map;

    iput-object p4, p0, Lcom/google/android/location/localizer/WifiLocator$SplitApList;->outliers:Ljava/util/Map;

    return-void
.end method
