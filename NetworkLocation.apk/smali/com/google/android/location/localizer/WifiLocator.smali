.class public Lcom/google/android/location/localizer/WifiLocator;
.super Ljava/lang/Object;
.source "WifiLocator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/localizer/WifiLocator$SplitApList;
    }
.end annotation


# instance fields
.field private final cache:Lcom/google/android/location/cache/TemporalCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/location/cache/TemporalCache",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;"
        }
    .end annotation
.end field

.field private final circleIntersectionLocalizer:Lcom/google/android/location/localizer/WifiLocalizerInterface;

.field private final clock:Lcom/google/android/location/os/Clock;

.field private final maxLreLocalizer:Lcom/google/android/location/localizer/WifiLocalizerInterface;

.field private final modelIndoorLocalizer:Lcom/google/android/location/localizer/ModelLocalizerV2;


# direct methods
.method public constructor <init>(Lcom/google/android/location/localizer/WifiLocationEstimator;Lcom/google/android/location/localizer/MaxLreLocalizer;Lcom/google/android/location/localizer/ModelLocalizerV2;Lcom/google/android/location/cache/TemporalCache;Lcom/google/android/location/os/Clock;)V
    .locals 0
    .param p1    # Lcom/google/android/location/localizer/WifiLocationEstimator;
    .param p2    # Lcom/google/android/location/localizer/MaxLreLocalizer;
    .param p3    # Lcom/google/android/location/localizer/ModelLocalizerV2;
    .param p5    # Lcom/google/android/location/os/Clock;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/localizer/WifiLocationEstimator;",
            "Lcom/google/android/location/localizer/MaxLreLocalizer;",
            "Lcom/google/android/location/localizer/ModelLocalizerV2;",
            "Lcom/google/android/location/cache/TemporalCache",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;",
            "Lcom/google/android/location/os/Clock;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/localizer/WifiLocator;->circleIntersectionLocalizer:Lcom/google/android/location/localizer/WifiLocalizerInterface;

    iput-object p2, p0, Lcom/google/android/location/localizer/WifiLocator;->maxLreLocalizer:Lcom/google/android/location/localizer/WifiLocalizerInterface;

    iput-object p3, p0, Lcom/google/android/location/localizer/WifiLocator;->modelIndoorLocalizer:Lcom/google/android/location/localizer/ModelLocalizerV2;

    iput-object p4, p0, Lcom/google/android/location/localizer/WifiLocator;->cache:Lcom/google/android/location/cache/TemporalCache;

    iput-object p5, p0, Lcom/google/android/location/localizer/WifiLocator;->clock:Lcom/google/android/location/os/Clock;

    return-void
.end method

.method private computeLocation(Ljava/util/Map;Ljava/util/Map;J)Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;
    .locals 4
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;J)",
            "Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/location/localizer/WifiLocator;->findHighestConfidencePositionType(Ljava/util/Map;)Lcom/google/android/location/data/WifiApPosition$PositionType;

    move-result-object v0

    sget-object v2, Lcom/google/android/location/data/WifiApPosition$PositionType;->UNKNOWN:Lcom/google/android/location/data/WifiApPosition$PositionType;

    if-ne v0, v2, :cond_0

    const-string v2, "WifiLocator"

    const-string v3, "No APs found with known confidence values. Not computing a location"

    invoke-static {v2, v3}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    sget-object v2, Lcom/google/android/location/data/WifiApPosition$PositionType;->LOW_CONFIDENCE:Lcom/google/android/location/data/WifiApPosition$PositionType;

    if-ne v0, v2, :cond_1

    const-string v2, "WifiLocator"

    const-string v3, "Computing location using circle intersection."

    invoke-static {v2, v3}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/localizer/WifiLocator;->circleIntersectionLocalizer:Lcom/google/android/location/localizer/WifiLocalizerInterface;

    invoke-interface {v2, p1, p2, p3, p4}, Lcom/google/android/location/localizer/WifiLocalizerInterface;->getEstimatedPosition(Ljava/util/Map;Ljava/util/Map;J)Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;

    move-result-object v1

    goto :goto_0

    :cond_1
    const-string v2, "WifiLocator"

    const-string v3, "Computing location using MaxLre."

    invoke-static {v2, v3}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/localizer/WifiLocator;->maxLreLocalizer:Lcom/google/android/location/localizer/WifiLocalizerInterface;

    invoke-interface {v2, p1, p2, p3, p4}, Lcom/google/android/location/localizer/WifiLocalizerInterface;->getEstimatedPosition(Ljava/util/Map;Ljava/util/Map;J)Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;

    move-result-object v1

    goto :goto_0
.end method

.method private findHighestConfidencePositionType(Ljava/util/Map;)Lcom/google/android/location/data/WifiApPosition$PositionType;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;)",
            "Lcom/google/android/location/data/WifiApPosition$PositionType;"
        }
    .end annotation

    const/4 v2, -0x1

    sget-object v3, Lcom/google/android/location/data/WifiApPosition$PositionType;->UNKNOWN:Lcom/google/android/location/data/WifiApPosition$PositionType;

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/location/data/WifiApPosition;

    iget-object v4, v5, Lcom/google/android/location/data/WifiApPosition;->positionType:Lcom/google/android/location/data/WifiApPosition$PositionType;

    invoke-virtual {v4}, Lcom/google/android/location/data/WifiApPosition$PositionType;->ordinal()I

    move-result v5

    if-le v5, v2, :cond_0

    invoke-static {}, Lcom/google/android/location/data/WifiApPosition$PositionType;->getHighestConfidenceType()Lcom/google/android/location/data/WifiApPosition$PositionType;

    move-result-object v5

    if-ne v4, v5, :cond_1

    :goto_1
    return-object v4

    :cond_1
    invoke-virtual {v4}, Lcom/google/android/location/data/WifiApPosition$PositionType;->ordinal()I

    move-result v2

    move-object v3, v4

    goto :goto_0

    :cond_2
    move-object v4, v3

    goto :goto_1
.end method

.method private getBestResult(Lcom/google/android/location/data/WifiLocatorResult;Lcom/google/android/location/data/WifiLocatorResult;)Lcom/google/android/location/data/WifiLocatorResult;
    .locals 2
    .param p1    # Lcom/google/android/location/data/WifiLocatorResult;
    .param p2    # Lcom/google/android/location/data/WifiLocatorResult;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/location/data/WifiLocatorResult;->position:Lcom/google/android/location/data/Position;

    if-nez v0, :cond_2

    :cond_0
    move-object p1, p2

    :cond_1
    :goto_0
    return-object p1

    :cond_2
    if-eqz p2, :cond_1

    iget-object v0, p2, Lcom/google/android/location/data/WifiLocatorResult;->position:Lcom/google/android/location/data/Position;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/location/data/WifiLocatorResult;->position:Lcom/google/android/location/data/Position;

    iget v0, v0, Lcom/google/android/location/data/Position;->accuracyMm:I

    iget-object v1, p2, Lcom/google/android/location/data/WifiLocatorResult;->position:Lcom/google/android/location/data/Position;

    iget v1, v1, Lcom/google/android/location/data/Position;->accuracyMm:I

    if-le v0, v1, :cond_1

    move-object p1, p2

    goto :goto_0
.end method

.method private getMedian(Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    div-int/lit8 v0, v1, 0x2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    :goto_0
    return v1

    :cond_0
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method static isApSetDiverse(Ljava/util/Set;)Z
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide v12, 0xfcffff000000L

    and-long v6, v10, v12

    if-eqz v1, :cond_1

    move-wide v4, v6

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    cmp-long v10, v6, v4

    if-eqz v10, :cond_2

    move v0, v8

    :goto_1
    if-eqz v0, :cond_0

    :goto_2
    return v8

    :cond_2
    move v0, v9

    goto :goto_1

    :cond_3
    move v8, v9

    goto :goto_2
.end method

.method private log(Ljava/lang/String;III)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " hasLocation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    add-int v2, p2, p3

    sub-int v0, p4, v2

    const-string v2, " noLocation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " cacheMiss="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "WifiLocator"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static positionsSpreadApart(Ljava/util/Collection;D)Z
    .locals 14
    .param p1    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/location/data/WifiApPosition;",
            ">;D)Z"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/location/data/WifiApPosition;

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/location/data/WifiApPosition;

    if-eq v12, v13, :cond_2

    iget v0, v12, Lcom/google/android/location/data/Position;->latE7:I

    int-to-double v0, v0

    iget v2, v12, Lcom/google/android/location/data/Position;->lngE7:I

    int-to-double v2, v2

    iget v4, v13, Lcom/google/android/location/data/Position;->latE7:I

    int-to-double v4, v4

    iget v6, v13, Lcom/google/android/location/data/Position;->lngE7:I

    int-to-double v6, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/location/data/LatLngUtils;->distanceInM(DDDD)D

    move-result-wide v8

    cmpg-double v0, v8, p1

    if-gez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public computeLocation(Ljava/util/List;Lcom/google/android/location/data/Position;J)Lcom/google/android/location/data/WifiLocatorResult;
    .locals 30
    .param p2    # Lcom/google/android/location/data/Position;
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/data/WifiScan;",
            ">;",
            "Lcom/google/android/location/data/Position;",
            "J)",
            "Lcom/google/android/location/data/WifiLocatorResult;"
        }
    .end annotation

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/location/localizer/WifiLocator;->extractSignalStrengths(Ljava/util/List;)Ljava/util/Map;

    move-result-object v29

    invoke-interface/range {v29 .. v29}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v5, v1}, Lcom/google/android/location/localizer/WifiLocator;->fetchWifiApFromCache(Ljava/util/Set;Lcom/google/android/location/data/Position;)Lcom/google/android/location/localizer/WifiLocator$SplitApList;

    move-result-object v21

    const/16 v24, 0x0

    const/high16 v25, -0x80000000

    move-object/from16 v0, v21

    iget-object v10, v0, Lcom/google/android/location/localizer/WifiLocator$SplitApList;->knownAps:Ljava/util/Map;

    move-object/from16 v0, v21

    iget-object v13, v0, Lcom/google/android/location/localizer/WifiLocator$SplitApList;->status:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/google/android/location/localizer/WifiLocator$SplitApList;->outliers:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Long;

    move-object/from16 v0, v29

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/location/data/WifiScan;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/localizer/WifiLocator;->modelIndoorLocalizer:Lcom/google/android/location/localizer/ModelLocalizerV2;

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/localizer/WifiLocator;->modelIndoorLocalizer:Lcom/google/android/location/localizer/ModelLocalizerV2;

    const/4 v6, 0x0

    move-object/from16 v0, v29

    move-wide/from16 v1, p3

    invoke-virtual {v5, v6, v0, v1, v2}, Lcom/google/android/location/localizer/ModelLocalizerV2;->getEstimatedPosition(Ljava/util/Map;Ljava/util/Map;J)Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;

    move-result-object v23

    if-eqz v23, :cond_2

    invoke-interface/range {v29 .. v29}, Ljava/util/Map;->size()I

    move-result v5

    const/4 v6, 0x2

    if-lt v5, v6, :cond_1

    const-string v5, "WifiLocator"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Indoor localizer returned: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/location/data/WifiLocatorResult;

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;->getPosition()Lcom/google/android/location/data/Position;

    move-result-object v5

    sget-object v6, Lcom/google/android/location/data/LocatorResult$ResultStatus;->OK:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/location/localizer/WifiLocator;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v7}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v7

    invoke-direct/range {v4 .. v10}, Lcom/google/android/location/data/WifiLocatorResult;-><init>(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/LocatorResult$ResultStatus;JLcom/google/android/location/data/WifiScan;Ljava/util/Map;)V

    iget-object v5, v4, Lcom/google/android/location/data/WifiLocatorResult;->position:Lcom/google/android/location/data/Position;

    if-eqz v5, :cond_1

    iget-object v5, v4, Lcom/google/android/location/data/WifiLocatorResult;->position:Lcom/google/android/location/data/Position;

    iget v5, v5, Lcom/google/android/location/data/Position;->accuracyMm:I

    const/16 v6, 0x7530

    if-ge v5, v6, :cond_1

    move-object v5, v4

    :goto_1
    return-object v5

    :cond_1
    invoke-virtual/range {v23 .. v23}, Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;->getPosition()Lcom/google/android/location/data/Position;

    move-result-object v5

    iget-object v0, v5, Lcom/google/android/location/data/Position;->levelId:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;->getPosition()Lcom/google/android/location/data/Position;

    move-result-object v5

    iget v0, v5, Lcom/google/android/location/data/Position;->levelNumberE3:I

    move/from16 v25, v0

    :cond_2
    const/4 v11, 0x0

    sget-object v5, Lcom/google/android/location/data/LocatorResult$ResultStatus;->OK:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    if-eq v13, v5, :cond_3

    new-instance v11, Lcom/google/android/location/data/WifiLocatorResult;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/localizer/WifiLocator;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v5}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v14

    move-object/from16 v16, v9

    move-object/from16 v17, v10

    invoke-direct/range {v11 .. v17}, Lcom/google/android/location/data/WifiLocatorResult;-><init>(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/LocatorResult$ResultStatus;JLcom/google/android/location/data/WifiScan;Ljava/util/Map;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v11}, Lcom/google/android/location/localizer/WifiLocator;->getBestResult(Lcom/google/android/location/data/WifiLocatorResult;Lcom/google/android/location/data/WifiLocatorResult;)Lcom/google/android/location/data/WifiLocatorResult;

    move-result-object v5

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-wide/from16 v2, p3

    invoke-direct {v0, v10, v1, v2, v3}, Lcom/google/android/location/localizer/WifiLocator;->computeLocation(Ljava/util/Map;Ljava/util/Map;J)Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;

    move-result-object v28

    if-eqz v28, :cond_4

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;->getPosition()Lcom/google/android/location/data/Position;

    move-result-object v5

    if-nez v5, :cond_5

    :cond_4
    const-string v5, "WifiLocator"

    const-string v6, "Locator did not find a location"

    invoke-static {v5, v6}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, Lcom/google/android/location/data/WifiLocatorResult;

    const/4 v15, 0x0

    sget-object v16, Lcom/google/android/location/data/LocatorResult$ResultStatus;->NO_LOCATION:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/localizer/WifiLocator;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v5}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v17

    move-object v14, v11

    move-object/from16 v19, v9

    move-object/from16 v20, v10

    invoke-direct/range {v14 .. v20}, Lcom/google/android/location/data/WifiLocatorResult;-><init>(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/LocatorResult$ResultStatus;JLcom/google/android/location/data/WifiScan;Ljava/util/Map;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v11}, Lcom/google/android/location/localizer/WifiLocator;->getBestResult(Lcom/google/android/location/data/WifiLocatorResult;Lcom/google/android/location/data/WifiLocatorResult;)Lcom/google/android/location/data/WifiLocatorResult;

    move-result-object v5

    goto :goto_1

    :cond_5
    invoke-virtual/range {v28 .. v28}, Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;->getPosition()Lcom/google/android/location/data/Position;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/location/localizer/LocalizerUtil;->hasSaneValues(Lcom/google/android/location/data/Position;)Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, "WifiLocator"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Locator found a location that did not have sane values: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, Lcom/google/android/location/data/WifiLocatorResult;

    const/4 v15, 0x0

    sget-object v16, Lcom/google/android/location/data/LocatorResult$ResultStatus;->NO_LOCATION:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/localizer/WifiLocator;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v5}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v17

    move-object v14, v11

    move-object/from16 v19, v9

    move-object/from16 v20, v10

    invoke-direct/range {v14 .. v20}, Lcom/google/android/location/data/WifiLocatorResult;-><init>(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/LocatorResult$ResultStatus;JLcom/google/android/location/data/WifiScan;Ljava/util/Map;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v11}, Lcom/google/android/location/localizer/WifiLocator;->getBestResult(Lcom/google/android/location/data/WifiLocatorResult;Lcom/google/android/location/data/WifiLocatorResult;)Lcom/google/android/location/data/WifiLocatorResult;

    move-result-object v5

    goto/16 :goto_1

    :cond_6
    const-string v5, "WifiLocator"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Finished computing WiFi location: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v27, Lcom/google/android/location/data/Position$PositionBuilder;

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;->getPosition()Lcom/google/android/location/data/Position;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-direct {v0, v5}, Lcom/google/android/location/data/Position$PositionBuilder;-><init>(Lcom/google/android/location/data/Position;)V

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/location/localizer/WifiLocalizerInterface$WifiLocationResult;->getConfidence()I

    move-result v5

    move-object/from16 v0, v27

    iput v5, v0, Lcom/google/android/location/data/Position$PositionBuilder;->confidence:I

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/google/android/location/data/Position$PositionBuilder;->levelId:Ljava/lang/String;

    move/from16 v0, v25

    move-object/from16 v1, v27

    iput v0, v1, Lcom/google/android/location/data/Position$PositionBuilder;->levelNumberE3:I

    new-instance v11, Lcom/google/android/location/data/WifiLocatorResult;

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/location/data/Position$PositionBuilder;->build()Lcom/google/android/location/data/Position;

    move-result-object v15

    sget-object v16, Lcom/google/android/location/data/LocatorResult$ResultStatus;->OK:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/location/localizer/WifiLocator;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v5}, Lcom/google/android/location/os/Clock;->millisSinceBoot()J

    move-result-wide v17

    move-object v14, v11

    move-object/from16 v19, v9

    move-object/from16 v20, v10

    invoke-direct/range {v14 .. v20}, Lcom/google/android/location/data/WifiLocatorResult;-><init>(Lcom/google/android/location/data/Position;Lcom/google/android/location/data/LocatorResult$ResultStatus;JLcom/google/android/location/data/WifiScan;Ljava/util/Map;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v11}, Lcom/google/android/location/localizer/WifiLocator;->getBestResult(Lcom/google/android/location/data/WifiLocatorResult;Lcom/google/android/location/data/WifiLocatorResult;)Lcom/google/android/location/data/WifiLocatorResult;

    move-result-object v5

    goto/16 :goto_1
.end method

.method extractSignalStrengths(Ljava/util/List;)Ljava/util/Map;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/data/WifiScan;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/location/data/WifiScan;

    if-eqz v9, :cond_0

    invoke-virtual {v9}, Lcom/google/android/location/data/WifiScan;->numDevices()I

    move-result v5

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v9, v2}, Lcom/google/android/location/data/WifiScan;->getDevice(I)Lcom/google/android/location/data/WifiScan$Device;

    move-result-object v1

    iget-object v10, v1, Lcom/google/android/location/data/WifiScan$Device;->mac:Ljava/lang/Long;

    invoke-interface {v6, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    if-nez v8, :cond_1

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iget-object v10, v1, Lcom/google/android/location/data/WifiScan$Device;->mac:Ljava/lang/Long;

    invoke-interface {v6, v10, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget v10, v1, Lcom/google/android/location/data/WifiScan$Device;->rssi:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    invoke-direct {p0, v8}, Lcom/google/android/location/localizer/WifiLocator;->getMedian(Ljava/util/List;)I

    move-result v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v7, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    return-object v7
.end method

.method fetchWifiApFromCache(Ljava/util/Set;Lcom/google/android/location/data/Position;)Lcom/google/android/location/localizer/WifiLocator$SplitApList;
    .locals 26
    .param p2    # Lcom/google/android/location/data/Position;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/google/android/location/data/Position;",
            ")",
            "Lcom/google/android/location/localizer/WifiLocator$SplitApList;"
        }
    .end annotation

    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/localizer/WifiLocator;->clock:Lcom/google/android/location/os/Clock;

    invoke-interface {v3}, Lcom/google/android/location/os/Clock;->millisSinceEpoch()J

    move-result-wide v22

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_0
    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/localizer/WifiLocator;->cache:Lcom/google/android/location/cache/TemporalCache;

    move-object/from16 v0, v19

    move-wide/from16 v1, v22

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/location/cache/TemporalCache;->lookup(Ljava/lang/Object;J)Lcom/google/android/location/cache/CacheResult;

    move-result-object v15

    if-eqz v15, :cond_4

    invoke-virtual {v15}, Lcom/google/android/location/cache/CacheResult;->getPosition()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/google/android/location/data/WifiApPosition;

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/location/data/WifiApPosition;->isValid()Z

    move-result v3

    if-eqz v3, :cond_3

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/google/android/location/data/WifiApPosition;->positionType:Lcom/google/android/location/data/WifiApPosition$PositionType;

    sget-object v4, Lcom/google/android/location/data/WifiApPosition$PositionType;->UNKNOWN:Lcom/google/android/location/data/WifiApPosition$PositionType;

    if-eq v3, v4, :cond_3

    const/4 v11, 0x1

    if-eqz p2, :cond_1

    move-object/from16 v0, p2

    iget v3, v0, Lcom/google/android/location/data/Position;->accuracyMm:I

    int-to-double v3, v3

    const-wide v6, 0x408f400000000000L

    div-double v20, v3, v6

    move-object/from16 v0, v24

    iget v3, v0, Lcom/google/android/location/data/WifiApPosition;->latE7:I

    int-to-double v3, v3

    move-object/from16 v0, v24

    iget v6, v0, Lcom/google/android/location/data/WifiApPosition;->lngE7:I

    int-to-double v5, v6

    move-object/from16 v0, p2

    iget v7, v0, Lcom/google/android/location/data/Position;->latE7:I

    int-to-double v7, v7

    move-object/from16 v0, p2

    iget v9, v0, Lcom/google/android/location/data/Position;->lngE7:I

    int-to-double v9, v9

    invoke-static/range {v3 .. v10}, Lcom/google/android/location/data/LatLngUtils;->distanceInM(DDDD)D

    move-result-wide v16

    cmpl-double v3, v16, v20

    if-lez v3, :cond_1

    const/4 v11, 0x0

    :cond_1
    if-eqz v11, :cond_2

    invoke-virtual {v15}, Lcom/google/android/location/cache/CacheResult;->getPosition()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-interface {v12, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    invoke-virtual {v15}, Lcom/google/android/location/cache/CacheResult;->getPosition()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-interface {v13, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/location/data/WifiApPosition;->isOutlier()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/location/data/WifiApPosition;->incrementOutlierCount()V

    goto :goto_0

    :cond_4
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_5
    if-nez p2, :cond_6

    invoke-interface {v12}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    const-wide v6, 0x40b3880000000000L

    invoke-static {v3, v6, v7}, Lcom/google/android/location/localizer/WifiLocator;->positionsSpreadApart(Ljava/util/Collection;D)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "WifiLocator"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AP set of size "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " has no overlap"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/location/localizer/WifiLocator$SplitApList;

    sget-object v4, Lcom/google/android/location/data/LocatorResult$ResultStatus;->NO_LOCATION:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v6, v13}, Lcom/google/android/location/localizer/WifiLocator$SplitApList;-><init>(Lcom/google/android/location/localizer/WifiLocator;Lcom/google/android/location/data/LocatorResult$ResultStatus;Ljava/util/Map;Ljava/util/Map;)V

    :goto_1
    return-object v3

    :cond_6
    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v3

    if-nez v3, :cond_9

    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_9

    invoke-interface {v13}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/location/localizer/WifiLocator;->isApSetDiverse(Ljava/util/Set;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "WifiLocator"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Diversity, rehabilitating ap list of size "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/location/utils/logging/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_7

    invoke-interface {v13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    const-wide v6, 0x40b3880000000000L

    invoke-static {v3, v6, v7}, Lcom/google/android/location/localizer/WifiLocator;->positionsSpreadApart(Ljava/util/Collection;D)Z

    move-result v3

    if-eqz v3, :cond_7

    new-instance v3, Lcom/google/android/location/localizer/WifiLocator$SplitApList;

    sget-object v4, Lcom/google/android/location/data/LocatorResult$ResultStatus;->NO_LOCATION:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v6, v13}, Lcom/google/android/location/localizer/WifiLocator$SplitApList;-><init>(Lcom/google/android/location/localizer/WifiLocator;Lcom/google/android/location/data/LocatorResult$ResultStatus;Ljava/util/Map;Ljava/util/Map;)V

    goto :goto_1

    :cond_7
    invoke-interface {v12, v13}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    invoke-interface {v13}, Ljava/util/Map;->clear()V

    :cond_8
    :goto_2
    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v25

    invoke-interface {v13}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/localizer/WifiLocator;->cache:Lcom/google/android/location/cache/TemporalCache;

    move-wide/from16 v0, v22

    invoke-virtual {v3, v5, v0, v1}, Lcom/google/android/location/cache/TemporalCache;->lookup(Ljava/lang/Object;J)Lcom/google/android/location/cache/CacheResult;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/location/localizer/WifiLocator;->cache:Lcom/google/android/location/cache/TemporalCache;

    const/4 v4, 0x0

    invoke-virtual {v15}, Lcom/google/android/location/cache/CacheResult;->getDatabaseVersion()I

    move-result v6

    invoke-static {}, Lcom/google/android/location/data/WifiApPosition;->createOutlierPosition()Lcom/google/android/location/data/WifiApPosition;

    move-result-object v7

    move-wide/from16 v8, v22

    invoke-virtual/range {v3 .. v9}, Lcom/google/android/location/cache/TemporalCache;->insertPosition(ZLjava/lang/Object;ILjava/lang/Object;J)V

    goto :goto_3

    :cond_9
    invoke-interface {v12}, Ljava/util/Map;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_8

    invoke-interface {v13}, Ljava/util/Map;->size()I

    move-result v3

    if-lez v3, :cond_8

    invoke-interface {v12, v13}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    invoke-interface {v13}, Ljava/util/Map;->clear()V

    goto :goto_2

    :cond_a
    if-lez v25, :cond_c

    const/4 v3, 0x5

    invoke-static {v3, v14}, Ljava/lang/Math;->min(II)I

    move-result v3

    move/from16 v0, v25

    if-lt v0, v3, :cond_b

    const-string v3, "Good cache hits. Computing WiFi location locally"

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v4

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v3, v1, v14, v4}, Lcom/google/android/location/localizer/WifiLocator;->log(Ljava/lang/String;III)V

    new-instance v3, Lcom/google/android/location/localizer/WifiLocator$SplitApList;

    sget-object v4, Lcom/google/android/location/data/LocatorResult$ResultStatus;->OK:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v12, v13}, Lcom/google/android/location/localizer/WifiLocator$SplitApList;-><init>(Lcom/google/android/location/localizer/WifiLocator;Lcom/google/android/location/data/LocatorResult$ResultStatus;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_1

    :cond_b
    const-string v3, "Not enough positive cache hits compared to misses. Need server request."

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v4

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v3, v1, v14, v4}, Lcom/google/android/location/localizer/WifiLocator;->log(Ljava/lang/String;III)V

    new-instance v3, Lcom/google/android/location/localizer/WifiLocator$SplitApList;

    sget-object v4, Lcom/google/android/location/data/LocatorResult$ResultStatus;->CACHE_MISS:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v6, v13}, Lcom/google/android/location/localizer/WifiLocator$SplitApList;-><init>(Lcom/google/android/location/localizer/WifiLocator;Lcom/google/android/location/data/LocatorResult$ResultStatus;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_1

    :cond_c
    if-lez v14, :cond_d

    const-string v3, "Too many cache  misses. Need server request."

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v4

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v3, v1, v14, v4}, Lcom/google/android/location/localizer/WifiLocator;->log(Ljava/lang/String;III)V

    new-instance v3, Lcom/google/android/location/localizer/WifiLocator$SplitApList;

    sget-object v4, Lcom/google/android/location/data/LocatorResult$ResultStatus;->CACHE_MISS:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v6, v13}, Lcom/google/android/location/localizer/WifiLocator$SplitApList;-><init>(Lcom/google/android/location/localizer/WifiLocator;Lcom/google/android/location/data/LocatorResult$ResultStatus;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_1

    :cond_d
    const-string v3, "Too many no-location APs. Will not compute a location nor go to the server."

    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    move-result v4

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v3, v1, v14, v4}, Lcom/google/android/location/localizer/WifiLocator;->log(Ljava/lang/String;III)V

    new-instance v3, Lcom/google/android/location/localizer/WifiLocator$SplitApList;

    sget-object v4, Lcom/google/android/location/data/LocatorResult$ResultStatus;->NO_LOCATION:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4, v6, v13}, Lcom/google/android/location/localizer/WifiLocator$SplitApList;-><init>(Lcom/google/android/location/localizer/WifiLocator;Lcom/google/android/location/data/LocatorResult$ResultStatus;Ljava/util/Map;Ljava/util/Map;)V

    goto/16 :goto_1
.end method
