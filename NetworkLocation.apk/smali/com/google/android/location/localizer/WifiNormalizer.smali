.class public Lcom/google/android/location/localizer/WifiNormalizer;
.super Ljava/lang/Object;
.source "WifiNormalizer.java"


# instance fields
.field private final offset:I


# direct methods
.method constructor <init>(Lcom/google/android/location/os/Os$BuildInfo;)V
    .locals 1
    .param p1    # Lcom/google/android/location/os/Os$BuildInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0, p1}, Lcom/google/android/location/localizer/WifiNormalizer;->getOffsetForPhone(Lcom/google/android/location/os/Os$BuildInfo;)I

    move-result v0

    iput v0, p0, Lcom/google/android/location/localizer/WifiNormalizer;->offset:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/location/os/Os;)V
    .locals 1
    .param p1    # Lcom/google/android/location/os/Os;

    invoke-interface {p1}, Lcom/google/android/location/os/Os;->getBuildInfo()Lcom/google/android/location/os/Os$BuildInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/location/localizer/WifiNormalizer;-><init>(Lcom/google/android/location/os/Os$BuildInfo;)V

    return-void
.end method

.method private getOffsetForPhone(Lcom/google/android/location/os/Os$BuildInfo;)I
    .locals 3
    .param p1    # Lcom/google/android/location/os/Os$BuildInfo;

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->manufacturer:Ljava/lang/String;

    const-string v2, "LGE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "P9"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "WifiNormalizer"

    const-string v2, "Recognized G2x"

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, -0xa

    :goto_0
    return v0

    :cond_0
    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->manufacturer:Ljava/lang/String;

    const-string v2, "HTC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "Nexus One"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "WifiNormalizer"

    const-string v2, "Recognized Nexus One"

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, -0xa

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/google/android/location/localizer/WifiNormalizer;->isMotorola(Lcom/google/android/location/os/Os$BuildInfo;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "Droid"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "WifiNormalizer"

    const-string v2, "Recognized Droid"

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, -0xa

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lcom/google/android/location/localizer/WifiNormalizer;->isSamsung(Lcom/google/android/location/os/Os$BuildInfo;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "Nexus S"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "WifiNormalizer"

    const-string v2, "Recognized Nexus S"

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x1

    goto :goto_0

    :cond_3
    invoke-static {p1}, Lcom/google/android/location/localizer/WifiNormalizer;->isSamsung(Lcom/google/android/location/os/Os$BuildInfo;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "Galaxy Nexus"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "WifiNormalizer"

    const-string v2, "Recognized Nexus Prime"

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x5

    goto :goto_0

    :cond_4
    invoke-static {p1}, Lcom/google/android/location/localizer/WifiNormalizer;->isSamsung(Lcom/google/android/location/os/Os$BuildInfo;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "SC-02"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "WifiNormalizer"

    const-string v2, "Recognized Galaxy S"

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x3

    goto/16 :goto_0

    :cond_5
    invoke-static {p1}, Lcom/google/android/location/localizer/WifiNormalizer;->isSony(Lcom/google/android/location/os/Os$BuildInfo;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "IS11S"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "SO-02C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_6
    const-string v1, "WifiNormalizer"

    const-string v2, "Recognized Xperia Acro"

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x1

    goto/16 :goto_0

    :cond_7
    invoke-static {p1}, Lcom/google/android/location/localizer/WifiNormalizer;->isMotorola(Lcom/google/android/location/os/Os$BuildInfo;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "Xoom"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "WifiNormalizer"

    const-string v2, "Recognized Motorola Xoom"

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x8

    goto/16 :goto_0

    :cond_8
    invoke-static {p1}, Lcom/google/android/location/localizer/WifiNormalizer;->isSamsung(Lcom/google/android/location/os/Os$BuildInfo;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "GT-P7510"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "WifiNormalizer"

    const-string v2, "Recognized Galaxy Tab 10.1"

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, -0xf

    goto/16 :goto_0

    :cond_9
    invoke-static {p1}, Lcom/google/android/location/localizer/WifiNormalizer;->isMotorola(Lcom/google/android/location/os/Os$BuildInfo;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "XT926"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "DROID RAZR HD"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "XT923"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "XT922"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "XT925"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "XT980"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "ME981"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    :cond_a
    const-string v1, "WifiNormalizer"

    const-string v2, "Recognized Motorola vanquish"

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, -0x10

    goto/16 :goto_0

    :cond_b
    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->manufacturer:Ljava/lang/String;

    const-string v2, "mock"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v1, p1, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    const-string v2, "mock"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const-string v1, "WifiNormalizer"

    const-string v2, "Recognized mock device"

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_c
    const-string v1, "WifiNormalizer"

    const-string v2, "No specific device detected. Offset set to -7."

    invoke-static {v1, v2}, Lcom/google/android/location/utils/logging/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x7

    goto/16 :goto_0
.end method

.method private static isMotorola(Lcom/google/android/location/os/Os$BuildInfo;)Z
    .locals 2
    .param p0    # Lcom/google/android/location/os/Os$BuildInfo;

    iget-object v0, p0, Lcom/google/android/location/os/Os$BuildInfo;->manufacturer:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "motorola"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private static isSamsung(Lcom/google/android/location/os/Os$BuildInfo;)Z
    .locals 2
    .param p0    # Lcom/google/android/location/os/Os$BuildInfo;

    iget-object v0, p0, Lcom/google/android/location/os/Os$BuildInfo;->manufacturer:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static isSony(Lcom/google/android/location/os/Os$BuildInfo;)Z
    .locals 2
    .param p0    # Lcom/google/android/location/os/Os$BuildInfo;

    iget-object v0, p0, Lcom/google/android/location/os/Os$BuildInfo;->manufacturer:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sony"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public normalize(I)I
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/location/localizer/WifiNormalizer;->offset:I

    add-int/2addr v0, p1

    return v0
.end method

.method public normalize(Ljava/util/Map;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/location/localizer/WifiNormalizer;->normalize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v2
.end method
