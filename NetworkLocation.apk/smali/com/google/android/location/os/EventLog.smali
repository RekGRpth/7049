.class public final Lcom/google/android/location/os/EventLog;
.super Ljava/lang/Object;
.source "EventLog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/EventLog$Timestamper;,
        Lcom/google/android/location/os/EventLog$Entry;
    }
.end annotation


# instance fields
.field private final date:Ljava/util/Date;

.field private final debugWriter:Ljava/io/PrintWriter;

.field private final endTime:[J

.field private final fieldPosition:Ljava/text/FieldPosition;

.field private lastCallbackTime:J

.field private final lastEvent:[Lcom/google/android/location/os/EventLog$Entry;

.field private lastEventOsClientId:I

.field private final log:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/location/os/EventLog$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final logger:Lcom/google/android/location/utils/logging/LoggerInterface;

.field private final notLogged:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/android/location/os/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final numEvents:[I

.field private final simpleDateFormat:Ljava/text/SimpleDateFormat;

.field private final stringBuffer:Ljava/lang/StringBuffer;

.field private final timestamper:Lcom/google/android/location/os/EventLog$Timestamper;


# direct methods
.method public constructor <init>(Lcom/google/android/location/os/EventLog$Timestamper;Lcom/google/android/location/utils/logging/LoggerInterface;Ljava/io/PrintWriter;)V
    .locals 4
    .param p1    # Lcom/google/android/location/os/EventLog$Timestamper;
    .param p2    # Lcom/google/android/location/utils/logging/LoggerInterface;
    .param p3    # Ljava/io/PrintWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/EventLog;->log:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/EventLog;->date:Ljava/util/Date;

    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/os/EventLog;->stringBuffer:Ljava/lang/StringBuffer;

    new-instance v0, Ljava/text/FieldPosition;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/text/FieldPosition;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/location/os/EventLog;->fieldPosition:Ljava/text/FieldPosition;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy.MM.dd HH:mm:ss "

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/location/os/EventLog;->simpleDateFormat:Ljava/text/SimpleDateFormat;

    sget-object v0, Lcom/google/android/location/os/Event;->CELL_SIGNAL_STRENGTH:Lcom/google/android/location/os/Event;

    sget-object v1, Lcom/google/android/location/os/Event;->MILLIS_SINCE_BOOT:Lcom/google/android/location/os/Event;

    sget-object v2, Lcom/google/android/location/os/Event;->MILLIS_SINCE_EPOCH:Lcom/google/android/location/os/Event;

    sget-object v3, Lcom/google/android/location/os/Event;->LOG:Lcom/google/android/location/os/Event;

    invoke-static {v0, v1, v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/EventLog;->notLogged:Ljava/util/EnumSet;

    invoke-static {}, Lcom/google/android/location/os/Event;->values()[Lcom/google/android/location/os/Event;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/location/os/EventLog$Entry;

    iput-object v0, p0, Lcom/google/android/location/os/EventLog;->lastEvent:[Lcom/google/android/location/os/EventLog$Entry;

    invoke-static {}, Lcom/google/android/location/os/Event;->values()[Lcom/google/android/location/os/Event;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/location/os/EventLog;->numEvents:[I

    invoke-static {}, Lcom/google/android/location/os/Event;->values()[Lcom/google/android/location/os/Event;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/android/location/os/EventLog;->endTime:[J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/location/os/EventLog;->lastCallbackTime:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/location/os/EventLog;->lastEventOsClientId:I

    iput-object p1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    iput-object p2, p0, Lcom/google/android/location/os/EventLog;->logger:Lcom/google/android/location/utils/logging/LoggerInterface;

    iput-object p3, p0, Lcom/google/android/location/os/EventLog;->debugWriter:Ljava/io/PrintWriter;

    iget-object v0, p0, Lcom/google/android/location/os/EventLog;->endTime:[J

    const-wide/16 v1, -0x1

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->fill([JJ)V

    return-void
.end method

.method private declared-synchronized addEntry(Lcom/google/android/location/os/EventLog$Entry;)V
    .locals 1
    .param p1    # Lcom/google/android/location/os/EventLog$Entry;

    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized addEntry(Lcom/google/android/location/os/EventLog$Entry;I)V
    .locals 9
    .param p1    # Lcom/google/android/location/os/EventLog$Entry;
    .param p2    # I

    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->log:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-lez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->log:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/os/EventLog$Entry;

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->endTime:[J

    iget-object v6, v2, Lcom/google/android/location/os/EventLog$Entry;->event:Lcom/google/android/location/os/Event;

    invoke-virtual {v6}, Lcom/google/android/location/os/Event;->ordinal()I

    move-result v6

    iget-wide v7, p1, Lcom/google/android/location/os/EventLog$Entry;->timestamp:J

    aput-wide v7, v5, v6

    :cond_0
    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->lastEvent:[Lcom/google/android/location/os/EventLog$Entry;

    iget-object v6, p1, Lcom/google/android/location/os/EventLog$Entry;->event:Lcom/google/android/location/os/Event;

    invoke-virtual {v6}, Lcom/google/android/location/os/Event;->ordinal()I

    move-result v6

    aput-object p1, v5, v6

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->numEvents:[I

    iget-object v6, p1, Lcom/google/android/location/os/EventLog$Entry;->event:Lcom/google/android/location/os/Event;

    invoke-virtual {v6}, Lcom/google/android/location/os/Event;->ordinal()I

    move-result v6

    aget v7, v5, v6

    add-int/lit8 v7, v7, 0x1

    aput v7, v5, v6

    iget-object v5, p1, Lcom/google/android/location/os/EventLog$Entry;->event:Lcom/google/android/location/os/Event;

    sget-object v6, Lcom/google/android/location/os/Event;->GPS_LOCATION:Lcom/google/android/location/os/Event;

    if-ne v5, v6, :cond_1

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->log:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    const/4 v6, 0x2

    if-lt v5, v6, :cond_1

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->log:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/location/os/EventLog$Entry;

    iget-object v5, v5, Lcom/google/android/location/os/EventLog$Entry;->event:Lcom/google/android/location/os/Event;

    sget-object v6, Lcom/google/android/location/os/Event;->GPS_LOCATION:Lcom/google/android/location/os/Event;

    if-ne v5, v6, :cond_1

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->log:Ljava/util/LinkedList;

    iget-object v6, p0, Lcom/google/android/location/os/EventLog;->log:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/location/os/EventLog$Entry;

    iget-object v5, v5, Lcom/google/android/location/os/EventLog$Entry;->event:Lcom/google/android/location/os/Event;

    sget-object v6, Lcom/google/android/location/os/Event;->GPS_LOCATION:Lcom/google/android/location/os/Event;

    if-ne v5, v6, :cond_1

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->log:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    :cond_1
    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->log:Ljava/util/LinkedList;

    invoke-virtual {v5, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    :goto_0
    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->log:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    const/16 v6, 0xc8

    if-le v5, v6, :cond_2

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->log:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    :cond_2
    :try_start_1
    iget-object v5, p1, Lcom/google/android/location/os/EventLog$Entry;->event:Lcom/google/android/location/os/Event;

    iget-object v5, v5, Lcom/google/android/location/os/Event;->kind:Lcom/google/android/location/os/Event$Kind;

    sget-object v6, Lcom/google/android/location/os/Event$Kind;->OS_GENERATED:Lcom/google/android/location/os/Event$Kind;

    if-ne v5, v6, :cond_3

    iget-wide v5, p1, Lcom/google/android/location/os/EventLog$Entry;->timestamp:J

    iput-wide v5, p0, Lcom/google/android/location/os/EventLog;->lastCallbackTime:J

    :cond_3
    iget-object v1, p1, Lcom/google/android/location/os/EventLog$Entry;->event:Lcom/google/android/location/os/Event;

    sget-object v5, Lcom/google/android/location/os/Event;->ALARM_CANCEL:Lcom/google/android/location/os/Event;

    if-eq v1, v5, :cond_4

    sget-object v5, Lcom/google/android/location/os/Event;->ALARM_RESET:Lcom/google/android/location/os/Event;

    if-eq v1, v5, :cond_4

    sget-object v5, Lcom/google/android/location/os/Event;->ALARM_RING:Lcom/google/android/location/os/Event;

    if-eq v1, v5, :cond_4

    sget-object v5, Lcom/google/android/location/os/Event;->WAKELOCK_ACQUIRE:Lcom/google/android/location/os/Event;

    if-eq v1, v5, :cond_4

    sget-object v5, Lcom/google/android/location/os/Event;->WAKELOCK_RELEASE:Lcom/google/android/location/os/Event;

    if-ne v1, v5, :cond_7

    :cond_4
    iput p2, p0, Lcom/google/android/location/os/EventLog;->lastEventOsClientId:I

    :goto_1
    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->logger:Lcom/google/android/location/utils/logging/LoggerInterface;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->notLogged:Ljava/util/EnumSet;

    iget-object v6, p1, Lcom/google/android/location/os/EventLog$Entry;->event:Lcom/google/android/location/os/Event;

    invoke-virtual {v5, v6}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->logger:Lcom/google/android/location/utils/logging/LoggerInterface;

    const-string v6, "gmmNlpEventLog"

    const/4 v7, 0x3

    invoke-interface {v5, v6, v7}, Lcom/google/android/location/utils/logging/LoggerInterface;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_5

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v4, Ljava/io/PrintWriter;

    invoke-direct {v4, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p1, v4}, Lcom/google/android/location/os/EventLog$Entry;->dump(Ljava/io/PrintWriter;)V

    invoke-virtual {v4}, Ljava/io/PrintWriter;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->logger:Lcom/google/android/location/utils/logging/LoggerInterface;

    const-string v6, "gmmNlpEventLog"

    invoke-interface {v5, v6, v3}, Lcom/google/android/location/utils/logging/LoggerInterface;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->debugWriter:Ljava/io/PrintWriter;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->date:Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/util/Date;->setTime(J)V

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->stringBuffer:Ljava/lang/StringBuffer;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->setLength(I)V

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->simpleDateFormat:Ljava/text/SimpleDateFormat;

    iget-object v6, p0, Lcom/google/android/location/os/EventLog;->date:Ljava/util/Date;

    iget-object v7, p0, Lcom/google/android/location/os/EventLog;->stringBuffer:Ljava/lang/StringBuffer;

    iget-object v8, p0, Lcom/google/android/location/os/EventLog;->fieldPosition:Ljava/text/FieldPosition;

    invoke-virtual {v5, v6, v7, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->debugWriter:Ljava/io/PrintWriter;

    iget-object v6, p0, Lcom/google/android/location/os/EventLog;->stringBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->debugWriter:Ljava/io/PrintWriter;

    invoke-virtual {p1, v5}, Lcom/google/android/location/os/EventLog$Entry;->dump(Ljava/io/PrintWriter;)V

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->debugWriter:Ljava/io/PrintWriter;

    invoke-virtual {v5}, Ljava/io/PrintWriter;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    monitor-exit p0

    return-void

    :cond_7
    const/4 v5, -0x1

    :try_start_2
    iput v5, p0, Lcom/google/android/location/os/EventLog;->lastEventOsClientId:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public addAirplaneModeChanged(Z)V
    .locals 6
    .param p1    # Z

    new-instance v0, Lcom/google/android/location/os/EventLog$2;

    sget-object v2, Lcom/google/android/location/os/Event;->AIRPLANE_MODE_CHANGED:Lcom/google/android/location/os/Event;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/EventLog$2;-><init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JZ)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;)V

    return-void
.end method

.method public addAlarmReset(IJ)V
    .locals 8
    .param p1    # I
    .param p2    # J

    new-instance v0, Lcom/google/android/location/os/EventLog$12;

    sget-object v2, Lcom/google/android/location/os/Event;->ALARM_RESET:Lcom/google/android/location/os/Event;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    move-wide v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/os/EventLog$12;-><init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JIJ)V

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;I)V

    return-void
.end method

.method public addAlarmRing(I)V
    .locals 6
    .param p1    # I

    new-instance v0, Lcom/google/android/location/os/EventLog$3;

    sget-object v2, Lcom/google/android/location/os/Event;->ALARM_RING:Lcom/google/android/location/os/Event;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/EventLog$3;-><init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JI)V

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;I)V

    return-void
.end method

.method public addBatteryStateChanged(IIZ)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    new-instance v0, Lcom/google/android/location/os/EventLog$4;

    sget-object v2, Lcom/google/android/location/os/Event;->BATTERY_STATE_CHANGED:Lcom/google/android/location/os/Event;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    move v6, p2

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/os/EventLog$4;-><init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JIIZ)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;)V

    return-void
.end method

.method public addCellScanResults(Lcom/google/android/location/data/CellState;)V
    .locals 6
    .param p1    # Lcom/google/android/location/data/CellState;

    new-instance v0, Lcom/google/android/location/os/EventLog$5;

    sget-object v2, Lcom/google/android/location/os/Event;->CELL_SCAN_RESULTS:Lcom/google/android/location/os/Event;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v3

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/EventLog$5;-><init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JLcom/google/android/location/data/CellState;)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;)V

    return-void
.end method

.method public addCellSignalStrength(I)V
    .locals 6
    .param p1    # I

    new-instance v0, Lcom/google/android/location/os/EventLog$6;

    sget-object v2, Lcom/google/android/location/os/Event;->CELL_SIGNAL_STRENGTH:Lcom/google/android/location/os/Event;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/EventLog$6;-><init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JI)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;)V

    return-void
.end method

.method public addEvent(Lcom/google/android/location/os/Event;)V
    .locals 3
    .param p1    # Lcom/google/android/location/os/Event;

    new-instance v0, Lcom/google/android/location/os/EventLog$Entry;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v1

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/location/os/EventLog$Entry;-><init>(Lcom/google/android/location/os/Event;J)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;)V

    return-void
.end method

.method public addLocationReport(Lcom/google/android/location/data/NetworkLocation;)V
    .locals 6
    .param p1    # Lcom/google/android/location/data/NetworkLocation;

    new-instance v0, Lcom/google/android/location/os/EventLog$14;

    sget-object v2, Lcom/google/android/location/os/Event;->LOCATION_REPORT:Lcom/google/android/location/os/Event;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v3

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/EventLog$14;-><init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JLcom/google/android/location/data/NetworkLocation;)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;)V

    return-void
.end method

.method public addLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/location/os/EventLog$18;

    sget-object v2, Lcom/google/android/location/os/Event;->LOG:Lcom/google/android/location/os/Event;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v3

    move-object v1, p0

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/os/EventLog$18;-><init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JLjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;)V

    return-void
.end method

.method public addNetworkChanged(ZZ)V
    .locals 7
    .param p1    # Z
    .param p2    # Z

    new-instance v0, Lcom/google/android/location/os/EventLog$7;

    sget-object v2, Lcom/google/android/location/os/Event;->NETWORK_CHANGED:Lcom/google/android/location/os/Event;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/os/EventLog$7;-><init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JZZ)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;)V

    return-void
.end method

.method public addScreenStateChanged(Z)V
    .locals 6
    .param p1    # Z

    new-instance v0, Lcom/google/android/location/os/EventLog$9;

    sget-object v2, Lcom/google/android/location/os/Event;->SCREEN_STATE_CHANGED:Lcom/google/android/location/os/Event;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/EventLog$9;-><init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JZ)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;)V

    return-void
.end method

.method public addSetPeriod(IZ)V
    .locals 7
    .param p1    # I
    .param p2    # Z

    new-instance v0, Lcom/google/android/location/os/EventLog$1;

    sget-object v2, Lcom/google/android/location/os/Event;->SET_PERIOD:Lcom/google/android/location/os/Event;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/location/os/EventLog$1;-><init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JIZ)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;)V

    return-void
.end method

.method public addWakeLockAcquire(I)V
    .locals 6
    .param p1    # I

    new-instance v0, Lcom/google/android/location/os/EventLog$16;

    sget-object v2, Lcom/google/android/location/os/Event;->WAKELOCK_ACQUIRE:Lcom/google/android/location/os/Event;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/EventLog$16;-><init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JI)V

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;I)V

    return-void
.end method

.method public addWakeLockRelease(I)V
    .locals 6
    .param p1    # I

    new-instance v0, Lcom/google/android/location/os/EventLog$17;

    sget-object v2, Lcom/google/android/location/os/Event;->WAKELOCK_RELEASE:Lcom/google/android/location/os/Event;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/EventLog$17;-><init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JI)V

    invoke-direct {p0, v0, p1}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;I)V

    return-void
.end method

.method public addWifiScanResults(Lcom/google/android/location/data/WifiScan;)V
    .locals 6
    .param p1    # Lcom/google/android/location/data/WifiScan;

    new-instance v0, Lcom/google/android/location/os/EventLog$10;

    sget-object v2, Lcom/google/android/location/os/Event;->WIFI_SCAN_RESULTS:Lcom/google/android/location/os/Event;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v3

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/EventLog$10;-><init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JLcom/google/android/location/data/WifiScan;)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;)V

    return-void
.end method

.method public addWifiStateChanged(Z)V
    .locals 6
    .param p1    # Z

    new-instance v0, Lcom/google/android/location/os/EventLog$11;

    sget-object v2, Lcom/google/android/location/os/Event;->WIFI_STATE_CHANGED:Lcom/google/android/location/os/Event;

    iget-object v1, p0, Lcom/google/android/location/os/EventLog;->timestamper:Lcom/google/android/location/os/EventLog$Timestamper;

    invoke-interface {v1}, Lcom/google/android/location/os/EventLog$Timestamper;->getNow()J

    move-result-wide v3

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/os/EventLog$11;-><init>(Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/Event;JZ)V

    invoke-direct {p0, v0}, Lcom/google/android/location/os/EventLog;->addEntry(Lcom/google/android/location/os/EventLog$Entry;)V

    return-void
.end method

.method public declared-synchronized dump(Ljava/io/PrintWriter;)V
    .locals 4
    .param p1    # Ljava/io/PrintWriter;

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/location/os/EventLog;->log:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/os/EventLog$Entry;

    iget-object v2, v0, Lcom/google/android/location/os/EventLog$Entry;->event:Lcom/google/android/location/os/Event;

    iget-object v2, v2, Lcom/google/android/location/os/Event;->kind:Lcom/google/android/location/os/Event$Kind;

    sget-object v3, Lcom/google/android/location/os/Event$Kind;->OS_GENERATED:Lcom/google/android/location/os/Event$Kind;

    if-ne v2, v3, :cond_0

    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(C)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/android/location/os/EventLog$Entry;->dump(Ljava/io/PrintWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized dump(Ljava/text/Format;JJLjava/io/PrintWriter;)V
    .locals 7
    .param p1    # Ljava/text/Format;
    .param p2    # J
    .param p4    # J
    .param p6    # Ljava/io/PrintWriter;

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/Date;

    const-wide/16 v5, 0x0

    invoke-direct {v0, v5, v6}, Ljava/util/Date;-><init>(J)V

    const-wide/32 v5, 0x927c0

    sub-long v3, p4, v5

    iget-object v5, p0, Lcom/google/android/location/os/EventLog;->log:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/location/os/EventLog$Entry;

    iget-wide v5, v1, Lcom/google/android/location/os/EventLog$Entry;->timestamp:J

    cmp-long v5, v5, v3

    if-ltz v5, :cond_0

    iget-object v5, v1, Lcom/google/android/location/os/EventLog$Entry;->event:Lcom/google/android/location/os/Event;

    iget-object v5, v5, Lcom/google/android/location/os/Event;->kind:Lcom/google/android/location/os/Event$Kind;

    sget-object v6, Lcom/google/android/location/os/Event$Kind;->OS_GENERATED:Lcom/google/android/location/os/Event$Kind;

    if-ne v5, v6, :cond_1

    const/16 v5, 0xa

    invoke-virtual {p6, v5}, Ljava/io/PrintWriter;->print(C)V

    :cond_1
    invoke-virtual {v0, p2, p3}, Ljava/util/Date;->setTime(J)V

    invoke-virtual {v1, p1, v0, p6}, Lcom/google/android/location/os/EventLog$Entry;->dump(Ljava/text/Format;Ljava/util/Date;Ljava/io/PrintWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    :cond_2
    monitor-exit p0

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x2710

    invoke-direct {v0, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p0, v1}, Lcom/google/android/location/os/EventLog;->dump(Ljava/io/PrintWriter;)V

    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
