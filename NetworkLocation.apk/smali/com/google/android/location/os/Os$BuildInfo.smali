.class public final Lcom/google/android/location/os/Os$BuildInfo;
.super Ljava/lang/Object;
.source "Os.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/Os;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BuildInfo"
.end annotation


# instance fields
.field public final manufacturer:Ljava/lang/String;

.field public final model:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/location/os/Os$BuildInfo;->manufacturer:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/location/os/Os$BuildInfo;->model:Ljava/lang/String;

    return-void
.end method
