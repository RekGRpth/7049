.class public interface abstract Lcom/google/android/location/os/Os;
.super Ljava/lang/Object;
.source "Os.java"

# interfaces
.implements Lcom/google/android/location/os/Clock;
.implements Lcom/google/android/location/os/FileSystem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/Os$BuildInfo;
    }
.end annotation


# virtual methods
.method public abstract alarmCancel(I)V
.end method

.method public abstract alarmReset(IJ)V
.end method

.method public abstract cellRequestScan()V
.end method

.method public abstract getBuildInfo()Lcom/google/android/location/os/Os$BuildInfo;
.end method

.method public abstract getEncryptionKey()Ljavax/crypto/SecretKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getEncryptionKeyOrNull()Ljavax/crypto/SecretKey;
.end method

.method public abstract getExecutor()Ljava/util/concurrent/ExecutorService;
.end method

.method public abstract getFreeMemoryKb()I
.end method

.method public abstract getLastKnownLocation()Lcom/google/android/location/os/LocationInterface;
.end method

.method public abstract getNlpMemoryKb()I
.end method

.method public abstract getSensorCacheDir()Ljava/io/File;
.end method

.method public abstract glsDeviceLocationQuery(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
.end method

.method public abstract glsModelQuery(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
.end method

.method public abstract glsQuery(Lcom/google/gmm/common/io/protocol/ProtoBuf;Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
.end method

.method public abstract hasAccelerometer()Z
.end method

.method public abstract locationReport(Lcom/google/android/location/data/NetworkLocation;Lcom/google/android/location/data/TravelDetectionType;)V
.end method

.method public abstract onTravelModeDetected(Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;)V
.end method

.method public abstract openMetricModelRawResource()Ljava/io/InputStream;
.end method

.method public abstract registerCallbacks(Lcom/google/android/location/os/Callbacks;)V
.end method

.method public abstract wakeLockAcquire(I)V
.end method

.method public abstract wakeLockRelease(I)V
.end method

.method public abstract wifiRequestScan()V
.end method
