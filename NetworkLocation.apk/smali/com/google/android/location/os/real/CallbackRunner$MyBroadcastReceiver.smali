.class final Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CallbackRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/CallbackRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/location/os/real/CallbackRunner;

.field private final wifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method private constructor <init>(Lcom/google/android/location/os/real/CallbackRunner;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iget-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/location/os/real/CallbackRunner;->access$000(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->wifiManager:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/os/real/CallbackRunner;Lcom/google/android/location/os/real/CallbackRunner$1;)V
    .locals 0
    .param p1    # Lcom/google/android/location/os/real/CallbackRunner;
    .param p2    # Lcom/google/android/location/os/real/CallbackRunner$1;

    invoke-direct {p0, p1}, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;-><init>(Lcom/google/android/location/os/real/CallbackRunner;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/16 v10, 0x15

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v12, -0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v9, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # invokes: Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(III)V
    invoke-static {v7, v10, v8, v12}, Lcom/google/android/location/os/real/CallbackRunner;->access$700(Lcom/google/android/location/os/real/CallbackRunner;III)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v9, "android.intent.action.SCREEN_ON"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v8, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # invokes: Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(III)V
    invoke-static {v8, v10, v7, v12}, Lcom/google/android/location/os/real/CallbackRunner;->access$700(Lcom/google/android/location/os/real/CallbackRunner;III)V

    goto :goto_0

    :cond_2
    sget-object v9, Lcom/google/android/location/os/real/CallbackRunner;->ALARM_WAKEUP_LOCATOR:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->alarmWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$400(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/os/PowerManager$WakeLock;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    const/4 v8, 0x6

    # invokes: Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(I)V
    invoke-static {v7, v8}, Lcom/google/android/location/os/real/CallbackRunner;->access$800(Lcom/google/android/location/os/real/CallbackRunner;I)V

    goto :goto_0

    :cond_3
    sget-object v9, Lcom/google/android/location/os/real/CallbackRunner;->ALARM_WAKEUP_CACHE_UPDATER:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->alarmWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$400(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/os/PowerManager$WakeLock;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    const/4 v8, 0x7

    # invokes: Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(I)V
    invoke-static {v7, v8}, Lcom/google/android/location/os/real/CallbackRunner;->access$800(Lcom/google/android/location/os/real/CallbackRunner;I)V

    goto :goto_0

    :cond_4
    sget-object v9, Lcom/google/android/location/os/real/CallbackRunner;->ALARM_WAKEUP_ACTIVITY_DETECTION:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->alarmWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$400(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/os/PowerManager$WakeLock;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    const/16 v8, 0x8

    # invokes: Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(I)V
    invoke-static {v7, v8}, Lcom/google/android/location/os/real/CallbackRunner;->access$800(Lcom/google/android/location/os/real/CallbackRunner;I)V

    goto :goto_0

    :cond_5
    const-string v9, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {v2, v3, v6}, Lcom/google/android/location/os/real/RealWifiScan;->create(JLjava/util/List;)Lcom/google/android/location/os/real/RealWifiScan;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    const/16 v8, 0x11

    # invokes: Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(ILjava/lang/Object;)V
    invoke-static {v7, v8, v5}, Lcom/google/android/location/os/real/CallbackRunner;->access$600(Lcom/google/android/location/os/real/CallbackRunner;ILjava/lang/Object;)V

    goto :goto_0

    :cond_6
    const-string v9, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    const-string v9, "wifi_state"

    const/4 v10, 0x4

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const/4 v1, 0x0

    const/4 v9, 0x3

    if-ne v4, v9, :cond_7

    const/4 v1, 0x1

    :goto_1
    iget-object v9, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    const/16 v10, 0x12

    if-eqz v1, :cond_8

    :goto_2
    # invokes: Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(III)V
    invoke-static {v9, v10, v7, v12}, Lcom/google/android/location/os/real/CallbackRunner;->access$700(Lcom/google/android/location/os/real/CallbackRunner;III)V

    goto/16 :goto_0

    :cond_7
    if-ne v4, v7, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :cond_8
    move v7, v8

    goto :goto_2

    :cond_9
    const-string v9, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    const/16 v8, 0x13

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    # invokes: Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(ILjava/lang/Object;)V
    invoke-static {v7, v8, v9}, Lcom/google/android/location/os/real/CallbackRunner;->access$600(Lcom/google/android/location/os/real/CallbackRunner;ILjava/lang/Object;)V

    goto/16 :goto_0

    :cond_a
    const-string v9, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    iget-object v9, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    const/16 v10, 0x17

    # invokes: Lcom/google/android/location/os/real/CallbackRunner;->isAirplaneModeEnabled(Landroid/content/Context;)Z
    invoke-static {p1}, Lcom/google/android/location/os/real/CallbackRunner;->access$900(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_b

    :goto_3
    # invokes: Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(III)V
    invoke-static {v9, v10, v7, v12}, Lcom/google/android/location/os/real/CallbackRunner;->access$700(Lcom/google/android/location/os/real/CallbackRunner;III)V

    goto/16 :goto_0

    :cond_b
    move v7, v8

    goto :goto_3

    :cond_c
    const-string v7, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    const/16 v8, 0x1a

    # invokes: Lcom/google/android/location/os/real/CallbackRunner;->sendMessage(I)V
    invoke-static {v7, v8}, Lcom/google/android/location/os/real/CallbackRunner;->access$800(Lcom/google/android/location/os/real/CallbackRunner;I)V

    goto/16 :goto_0

    :cond_d
    const-string v7, "NetworkLocationCallbackRunner"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unexpected action "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
