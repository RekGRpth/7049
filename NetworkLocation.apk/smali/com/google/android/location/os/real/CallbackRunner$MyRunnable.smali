.class final Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;
.super Ljava/lang/Object;
.source "CallbackRunner.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/CallbackRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyRunnable"
.end annotation


# instance fields
.field monitoringEvents:Z

.field final synthetic this$0:Lcom/google/android/location/os/real/CallbackRunner;


# direct methods
.method private constructor <init>(Lcom/google/android/location/os/real/CallbackRunner;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->monitoringEvents:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/location/os/real/CallbackRunner;Lcom/google/android/location/os/real/CallbackRunner$1;)V
    .locals 0
    .param p1    # Lcom/google/android/location/os/real/CallbackRunner;
    .param p2    # Lcom/google/android/location/os/real/CallbackRunner$1;

    invoke-direct {p0, p1}, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;-><init>(Lcom/google/android/location/os/real/CallbackRunner;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    const/4 v5, 0x1

    const/4 v7, -0x4

    invoke-static {v7}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-static {}, Landroid/os/Looper;->prepare()V

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->lock:Ljava/lang/Object;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$1000(Lcom/google/android/location/os/real/CallbackRunner;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    :try_start_0
    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    new-instance v9, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;

    iget-object v10, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    const/4 v11, 0x0

    invoke-direct {v9, v10, v11}, Lcom/google/android/location/os/real/CallbackRunner$MyHandler;-><init>(Lcom/google/android/location/os/real/CallbackRunner;Lcom/google/android/location/os/real/CallbackRunner$1;)V

    # setter for: Lcom/google/android/location/os/real/CallbackRunner;->handler:Landroid/os/Handler;
    invoke-static {v7, v9}, Lcom/google/android/location/os/real/CallbackRunner;->access$1102(Lcom/google/android/location/os/real/CallbackRunner;Landroid/os/Handler;)Landroid/os/Handler;

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v7, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v7, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v7, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v7, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v7, Lcom/google/android/location/os/real/CallbackRunner;->ALARM_WAKEUP_LOCATOR:Ljava/lang/String;

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v7, Lcom/google/android/location/os/real/CallbackRunner;->ALARM_WAKEUP_CACHE_UPDATER:Ljava/lang/String;

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v7, Lcom/google/android/location/os/real/CallbackRunner;->ALARM_WAKEUP_ACTIVITY_DETECTION:Ljava/lang/String;

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v7, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v7, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v7, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->lock:Ljava/lang/Object;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$1000(Lcom/google/android/location/os/real/CallbackRunner;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    :try_start_1
    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->quitCalled:Z
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$1300(Lcom/google/android/location/os/real/CallbackRunner;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    new-instance v9, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;

    iget-object v10, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    const/4 v11, 0x0

    invoke-direct {v9, v10, v11}, Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;-><init>(Lcom/google/android/location/os/real/CallbackRunner;Lcom/google/android/location/os/real/CallbackRunner$1;)V

    # setter for: Lcom/google/android/location/os/real/CallbackRunner;->broadcastReceiver:Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;
    invoke-static {v7, v9}, Lcom/google/android/location/os/real/CallbackRunner;->access$1402(Lcom/google/android/location/os/real/CallbackRunner;Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;)Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->context:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$000(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/content/Context;

    move-result-object v7

    iget-object v9, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->broadcastReceiver:Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;
    invoke-static {v9}, Lcom/google/android/location/os/real/CallbackRunner;->access$1400(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/real/CallbackRunner$MyBroadcastReceiver;

    move-result-object v9

    invoke-virtual {v7, v9, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->context:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$000(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/content/Context;

    move-result-object v7

    const-string v8, "phone"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->phoneStateListener:Lcom/google/android/location/os/real/CallbackRunner$MyPhoneStateListener;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$300(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/real/CallbackRunner$MyPhoneStateListener;

    move-result-object v7

    const/16 v8, 0x150

    invoke-virtual {v4, v7, v8}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    monitor-enter p0

    const/4 v7, 0x1

    :try_start_2
    iput-boolean v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->monitoringEvents:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v7

    sget-object v8, Lcom/google/android/location/os/Event;->INITIALIZE:Lcom/google/android/location/os/Event;

    invoke-virtual {v7, v8}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/location/os/Callbacks;->initialize()V

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->context:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$000(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/content/Context;

    move-result-object v7

    const-string v8, "wifi"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/WifiManager;

    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v7

    const/4 v8, 0x3

    if-ne v7, v8, :cond_1

    :goto_0
    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/google/android/location/os/EventLog;->addWifiStateChanged(Z)V

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v7

    invoke-interface {v7, v5}, Lcom/google/android/location/os/Callbacks;->wifiStateChanged(Z)V

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->context:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$000(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/content/Context;

    move-result-object v7

    const-string v8, "power"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v3

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/google/android/location/os/EventLog;->addScreenStateChanged(Z)V

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v7

    invoke-interface {v7, v3}, Lcom/google/android/location/os/Callbacks;->screenStateChanged(Z)V

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->context:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$000(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/content/Context;

    move-result-object v7

    # invokes: Lcom/google/android/location/os/real/CallbackRunner;->isAirplaneModeEnabled(Landroid/content/Context;)Z
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$900(Landroid/content/Context;)Z

    move-result v0

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->eventLog:Lcom/google/android/location/os/EventLog;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$100(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/EventLog;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/android/location/os/EventLog;->addAirplaneModeChanged(Z)V

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v7

    invoke-interface {v7, v0}, Lcom/google/android/location/os/Callbacks;->airplaneModeChanged(Z)V

    iget-object v8, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->context:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$000(Lcom/google/android/location/os/real/CallbackRunner;)Landroid/content/Context;

    move-result-object v7

    const-string v9, "connectivity"

    invoke-virtual {v7, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/ConnectivityManager;

    iget-object v9, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v9}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v9

    # invokes: Lcom/google/android/location/os/real/CallbackRunner;->notifyNetworkEvent(Landroid/net/ConnectivityManager;Lcom/google/android/location/os/Callbacks;)V
    invoke-static {v8, v7, v9}, Lcom/google/android/location/os/real/CallbackRunner;->access$500(Lcom/google/android/location/os/real/CallbackRunner;Landroid/net/ConnectivityManager;Lcom/google/android/location/os/Callbacks;)V

    iget-object v7, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->callbacks:Lcom/google/android/location/os/Callbacks;
    invoke-static {v7}, Lcom/google/android/location/os/real/CallbackRunner;->access$200(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/Callbacks;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/location/os/real/CallbackRunner$MyRunnable;->this$0:Lcom/google/android/location/os/real/CallbackRunner;

    # getter for: Lcom/google/android/location/os/real/CallbackRunner;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;
    invoke-static {v8}, Lcom/google/android/location/os/real/CallbackRunner;->access$1600(Lcom/google/android/location/os/real/CallbackRunner;)Lcom/google/android/location/os/NlpParametersState;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/google/android/location/os/Callbacks;->nlpParamtersStateChanged(Lcom/google/android/location/os/NlpParametersState;)V

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void

    :catchall_0
    move-exception v7

    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v7

    :catchall_1
    move-exception v7

    :try_start_4
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v7

    :catchall_2
    move-exception v7

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v7

    :cond_1
    const/4 v5, 0x0

    goto/16 :goto_0
.end method
