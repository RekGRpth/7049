.class final Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;
.super Ljava/lang/Object;
.source "MultipartZippedRequest.java"

# interfaces
.implements Lcom/google/masf/InputStreamProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/location/os/real/MultipartZippedRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "GZipInputStreamProvider"
.end annotation


# instance fields
.field private mRaw:[B

.field private mZipped:[B


# direct methods
.method public constructor <init>([B)V
    .locals 1
    .param p1    # [B

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;->mRaw:[B

    iput-object v0, p0, Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;->mZipped:[B

    iput-object p1, p0, Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;->mRaw:[B

    return-void
.end method

.method private gzip([B)[B
    .locals 3
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v0, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v0, p1}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    invoke-virtual {v0}, Ljava/util/zip/GZIPOutputStream;->close()V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;->mRaw:[B

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    return-object v2
.end method

.method private declared-synchronized mightZip()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;->mZipped:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;->mRaw:[B

    invoke-direct {p0, v0}, Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;->gzip([B)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;->mZipped:[B

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;->mRaw:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public dispose()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;->mRaw:[B

    iput-object v0, p0, Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;->mZipped:[B

    return-void
.end method

.method public declared-synchronized getInputStream()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;->mightZip()V

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;->mZipped:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getStreamLength()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;->mightZip()V

    iget-object v0, p0, Lcom/google/android/location/os/real/MultipartZippedRequest$GZipInputStreamProvider;->mZipped:[B

    array-length v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
