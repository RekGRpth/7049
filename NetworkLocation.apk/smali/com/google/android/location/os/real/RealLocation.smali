.class public Lcom/google/android/location/os/real/RealLocation;
.super Ljava/lang/Object;
.source "RealLocation.java"

# interfaces
.implements Lcom/google/android/location/os/LocationInterface;


# instance fields
.field private final location:Landroid/location/Location;

.field private final satellites:I

.field private final timeSinceBoot:J


# direct methods
.method public constructor <init>(Landroid/location/Location;JI)V
    .locals 2
    .param p1    # Landroid/location/Location;
    .param p2    # J
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null location in RealLocation constructor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/location/os/real/RealLocation;->location:Landroid/location/Location;

    iput-wide p2, p0, Lcom/google/android/location/os/real/RealLocation;->timeSinceBoot:J

    iput p4, p0, Lcom/google/android/location/os/real/RealLocation;->satellites:I

    return-void
.end method


# virtual methods
.method public getAccuracy()F
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/RealLocation;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    return v0
.end method

.method public getAltitude()D
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/RealLocation;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getBearing()F
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/RealLocation;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v0

    return v0
.end method

.method public getLat()D
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/RealLocation;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getLng()D
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/RealLocation;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    return-wide v0
.end method

.method public getLocation()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/RealLocation;->location:Landroid/location/Location;

    return-object v0
.end method

.method public getSatellites()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/os/real/RealLocation;->satellites:I

    return v0
.end method

.method public getTimeSinceBoot()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/os/real/RealLocation;->timeSinceBoot:J

    return-wide v0
.end method

.method public hasAltitude()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/RealLocation;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasAltitude()Z

    move-result v0

    return v0
.end method

.method public hasBearing()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/RealLocation;->location:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RealLocation [location="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/location/os/real/RealLocation;->location:Landroid/location/Location;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " satellites="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/location/os/real/RealLocation;->getSatellites()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/location/os/real/RealLocation;->hasBearing()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, " bearing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/location/os/real/RealLocation;->getBearing()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/os/real/RealLocation;->hasAltitude()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, " altitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/location/os/real/RealLocation;->getAltitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
