.class public Lcom/google/android/location/os/real/RealOs;
.super Ljava/lang/Object;
.source "RealOs.java"

# interfaces
.implements Lcom/google/android/location/os/Os;
.implements Lcom/google/android/location/utils/OnChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/os/real/RealOs$LocationReceiver;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/location/os/Os;",
        "Lcom/google/android/location/utils/OnChangeListener",
        "<",
        "Lcom/google/android/location/os/NlpParametersState;",
        ">;"
    }
.end annotation


# static fields
.field private static final key:[Ljava/lang/String;


# instance fields
.field private final alarmIntent:[Landroid/app/PendingIntent;

.field private final alarmManager:Landroid/app/AlarmManager;

.field private final callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

.field public final context:Landroid/content/Context;

.field private final eventLog:Lcom/google/android/location/os/EventLog;

.field private final executor:Ljava/util/concurrent/ExecutorService;

.field private final glsClient:Lcom/google/android/location/os/real/GlsClient;

.field private final locationManager:Landroid/location/LocationManager;

.field private final locationReceiver:Lcom/google/android/location/os/real/RealOs$LocationReceiver;

.field private final nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

.field private final sensorManager:Landroid/hardware/SensorManager;

.field private final wakeLock:[Landroid/os/PowerManager$WakeLock;

.field private wakeLockAcquired:[Z

.field private wifiLock:[Landroid/net/wifi/WifiManager$WifiLock;

.field private final wifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "NetworkLocationLocator"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "NetworkLocationCacheUpdater"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "NetworkLocationActivityDetection"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/location/os/real/RealOs;->key:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/location/os/EventLog;Lcom/google/android/location/os/real/RealOs$LocationReceiver;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/location/os/EventLog;
    .param p3    # Lcom/google/android/location/os/real/RealOs$LocationReceiver;

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x3

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v2, v6, [Landroid/app/PendingIntent;

    iput-object v2, p0, Lcom/google/android/location/os/real/RealOs;->alarmIntent:[Landroid/app/PendingIntent;

    new-array v2, v6, [Landroid/os/PowerManager$WakeLock;

    iput-object v2, p0, Lcom/google/android/location/os/real/RealOs;->wakeLock:[Landroid/os/PowerManager$WakeLock;

    new-array v2, v6, [Z

    iput-object v2, p0, Lcom/google/android/location/os/real/RealOs;->wakeLockAcquired:[Z

    new-array v2, v6, [Landroid/net/wifi/WifiManager$WifiLock;

    iput-object v2, p0, Lcom/google/android/location/os/real/RealOs;->wifiLock:[Landroid/net/wifi/WifiManager$WifiLock;

    iput-object p1, p0, Lcom/google/android/location/os/real/RealOs;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    iput-object p3, p0, Lcom/google/android/location/os/real/RealOs;->locationReceiver:Lcom/google/android/location/os/real/RealOs$LocationReceiver;

    new-instance v2, Lcom/google/android/location/os/NlpParametersState;

    invoke-direct {v2, p0, p0, p0}, Lcom/google/android/location/os/NlpParametersState;-><init>(Lcom/google/android/location/os/Clock;Lcom/google/android/location/os/FileSystem;Lcom/google/android/location/utils/OnChangeListener;)V

    iput-object v2, p0, Lcom/google/android/location/os/real/RealOs;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    iget-object v2, p0, Lcom/google/android/location/os/real/RealOs;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    invoke-virtual {v2}, Lcom/google/android/location/os/NlpParametersState;->load()V

    new-instance v2, Lcom/google/android/location/os/real/CallbackRunner;

    iget-object v3, p0, Lcom/google/android/location/os/real/RealOs;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    invoke-direct {v2, p1, v3, p2}, Lcom/google/android/location/os/real/CallbackRunner;-><init>(Landroid/content/Context;Lcom/google/android/location/os/NlpParametersState;Lcom/google/android/location/os/EventLog;)V

    iput-object v2, p0, Lcom/google/android/location/os/real/RealOs;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

    const-string v2, "alarm"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    iput-object v2, p0, Lcom/google/android/location/os/real/RealOs;->alarmManager:Landroid/app/AlarmManager;

    iget-object v2, p0, Lcom/google/android/location/os/real/RealOs;->alarmIntent:[Landroid/app/PendingIntent;

    new-instance v3, Landroid/content/Intent;

    sget-object v4, Lcom/google/android/location/os/real/CallbackRunner;->ALARM_WAKEUP_LOCATOR:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v5, v3, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v2, p0, Lcom/google/android/location/os/real/RealOs;->alarmIntent:[Landroid/app/PendingIntent;

    new-instance v3, Landroid/content/Intent;

    sget-object v4, Lcom/google/android/location/os/real/CallbackRunner;->ALARM_WAKEUP_CACHE_UPDATER:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v5, v3, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v2, v7

    iget-object v2, p0, Lcom/google/android/location/os/real/RealOs;->alarmIntent:[Landroid/app/PendingIntent;

    new-instance v3, Landroid/content/Intent;

    sget-object v4, Lcom/google/android/location/os/real/CallbackRunner;->ALARM_WAKEUP_ACTIVITY_DETECTION:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v5, v3, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v2, v8

    new-instance v2, Lcom/google/android/location/os/real/RealOs$1;

    invoke-direct {v2, p0}, Lcom/google/android/location/os/real/RealOs$1;-><init>(Lcom/google/android/location/os/real/RealOs;)V

    invoke-static {v2}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/location/os/real/RealOs;->executor:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/google/android/location/os/real/GlsClient;

    iget-object v3, p0, Lcom/google/android/location/os/real/RealOs;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    iget-object v4, p0, Lcom/google/android/location/os/real/RealOs;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

    invoke-direct {v2, p1, v3, v4}, Lcom/google/android/location/os/real/GlsClient;-><init>(Landroid/content/Context;Lcom/google/android/location/os/NlpParametersState;Lcom/google/android/location/os/real/CallbackRunner;)V

    iput-object v2, p0, Lcom/google/android/location/os/real/RealOs;->glsClient:Lcom/google/android/location/os/real/GlsClient;

    const-string v2, "power"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    const-string v2, "wifi"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    iput-object v2, p0, Lcom/google/android/location/os/real/RealOs;->wifiManager:Landroid/net/wifi/WifiManager;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v6, :cond_0

    iget-object v2, p0, Lcom/google/android/location/os/real/RealOs;->wakeLock:[Landroid/os/PowerManager$WakeLock;

    sget-object v3, Lcom/google/android/location/os/real/RealOs;->key:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v1, v7, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/android/location/os/real/RealOs;->wakeLock:[Landroid/os/PowerManager$WakeLock;

    aget-object v2, v2, v0

    invoke-virtual {v2, v5}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    iget-object v2, p0, Lcom/google/android/location/os/real/RealOs;->wifiLock:[Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v3, p0, Lcom/google/android/location/os/real/RealOs;->wifiManager:Landroid/net/wifi/WifiManager;

    sget-object v4, Lcom/google/android/location/os/real/RealOs;->key:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v8, v4}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v3

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/android/location/os/real/RealOs;->wifiLock:[Landroid/net/wifi/WifiManager$WifiLock;

    aget-object v2, v2, v0

    invoke-virtual {v2, v5}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v2, "sensor"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    iput-object v2, p0, Lcom/google/android/location/os/real/RealOs;->sensorManager:Landroid/hardware/SensorManager;

    const-string v2, "location"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/LocationManager;

    iput-object v2, p0, Lcom/google/android/location/os/real/RealOs;->locationManager:Landroid/location/LocationManager;

    return-void
.end method

.method public static bootTimeMillis()J
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public static computeInOutdoorHistoryFile(Landroid/content/Context;)Ljava/io/File;
    .locals 3
    .param p0    # Landroid/content/Context;

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_ioh"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static computeNlpStateFile(Landroid/content/Context;)Ljava/io/File;
    .locals 3
    .param p0    # Landroid/content/Context;

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_state"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static computeSeenDevicesCacheFile(Landroid/content/Context;)Ljava/io/File;
    .locals 3
    .param p0    # Landroid/content/Context;

    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/google/android/location/os/real/RealOs;->seenDevicesCacheDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_devices"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static deleteState(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    :try_start_0
    invoke-static {p0}, Lcom/google/android/location/os/real/RealOs;->computeNlpStateFile(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    invoke-static {p0}, Lcom/google/android/location/os/real/RealOs;->computeSeenDevicesCacheFile(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    invoke-static {p0}, Lcom/google/android/location/os/real/RealOs;->computeInOutdoorHistoryFile(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    invoke-static {p0}, Lcom/google/android/location/os/real/RealOs;->getSensorCacheDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/location/utils/Utils;->deleteDirResursive(Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    invoke-static {}, Lcom/google/android/location/os/real/GlsClient;->deletePlatformKey()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "NetworkLocationRealOs"

    const-string v2, "Unable to delete nlp state file"

    invoke-static {v1, v2, v0}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "NetworkLocationRealOs"

    const-string v2, "Unable to delete scache dir"

    invoke-static {v1, v2, v0}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private static getEncryptionKey(Landroid/content/Context;)Ljavax/crypto/SecretKey;
    .locals 13
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v12, 0x18

    const/16 v11, 0x10

    const/16 v10, 0x8

    const/4 v9, 0x0

    const-wide/16 v7, 0xff

    sget-boolean v3, Lcom/google/android/location/os/real/GlsClient;->USE_GSERVICES:Z

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "android_id"

    const-wide/16 v5, 0x0

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/gsf/Gservices;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    :goto_0
    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-nez v3, :cond_1

    new-instance v3, Ljava/io/IOException;

    const-string v4, "no android ID; can\'t access encrypted cache"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    const-wide/16 v0, 0x1

    goto :goto_0

    :cond_1
    const/16 v3, 0x20

    new-array v2, v3, [B

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v7

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v9

    const/4 v3, 0x1

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x2

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x3

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x4

    ushr-long v4, v0, v12

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x5

    ushr-long v4, v0, v11

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x6

    ushr-long v4, v0, v10

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x7

    ushr-long v4, v0, v9

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v7

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v10

    const/16 v3, 0x9

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xa

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xb

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xc

    ushr-long v4, v0, v12

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xd

    ushr-long v4, v0, v11

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xe

    ushr-long v4, v0, v10

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xf

    ushr-long v4, v0, v9

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v7

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v11

    const/16 v3, 0x11

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x12

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x13

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x14

    ushr-long v4, v0, v12

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x15

    ushr-long v4, v0, v11

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x16

    ushr-long v4, v0, v10

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x17

    ushr-long v4, v0, v9

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v7

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v12

    const/16 v3, 0x19

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1a

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1b

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1c

    ushr-long v4, v0, v12

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1d

    ushr-long v4, v0, v11

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1e

    ushr-long v4, v0, v10

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1f

    ushr-long v4, v0, v9

    and-long/2addr v4, v7

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v4, "AES"

    invoke-direct {v3, v2, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v3
.end method

.method public static getSensorCacheDir(Landroid/content/Context;)Ljava/io/File;
    .locals 3
    .param p0    # Landroid/content/Context;

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_s"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static migrateState(Landroid/content/Context;)V
    .locals 6
    .param p0    # Landroid/content/Context;

    const-string v4, "cache.cell"

    invoke-virtual {p0, v4}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    const-string v4, "cache.wifi"

    invoke-virtual {p0, v4}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    const-string v4, "gls.platform.key"

    invoke-virtual {p0, v4}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    const-string v4, "nlp_GlsPlatformKey"

    invoke-virtual {p0, v4}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    const-string v5, "nlp_state"

    invoke-direct {v2, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/location/os/real/RealOs;->computeNlpStateFile(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v2, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_0
    if-eqz v3, :cond_0

    invoke-static {}, Lcom/google/android/location/os/real/SdkSpecific;->getInstance()Lcom/google/android/location/os/real/SdkSpecific;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/android/location/os/real/SdkSpecific;->makeFilePrivate(Ljava/io/File;)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v4, "NetworkLocationRealOs"

    const-string v5, "Unable to renamed old nlp state file"

    invoke-static {v4, v5, v0}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    const-string v4, "nlp_state"

    invoke-virtual {p0, v4}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    goto :goto_1
.end method

.method private static seenDevicesCacheDir(Landroid/content/Context;)Ljava/io/File;
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static sinceBootMillis()J
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public static sinceEpochMillis()J
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public alarmCancel(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    sget-object v1, Lcom/google/android/location/os/Event;->ALARM_CANCEL:Lcom/google/android/location/os/Event;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->alarmManager:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/location/os/real/RealOs;->alarmIntent:[Landroid/app/PendingIntent;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public alarmReset(IJ)V
    .locals 3
    .param p1    # I
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/location/os/EventLog;->addAlarmReset(IJ)V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->alarmManager:Landroid/app/AlarmManager;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/os/real/RealOs;->alarmIntent:[Landroid/app/PendingIntent;

    aget-object v2, v2, p1

    invoke-virtual {v0, v1, p2, p3, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method public bootTime()J
    .locals 2

    invoke-static {}, Lcom/google/android/location/os/real/RealOs;->bootTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public cellRequestScan()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    sget-object v1, Lcom/google/android/location/os/Event;->CELL_REQUEST_SCAN:Lcom/google/android/location/os/Event;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

    invoke-virtual {v0}, Lcom/google/android/location/os/real/CallbackRunner;->cellRequestScan()V

    return-void
.end method

.method public getBuildInfo()Lcom/google/android/location/os/Os$BuildInfo;
    .locals 3

    new-instance v0, Lcom/google/android/location/os/Os$BuildInfo;

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/os/Os$BuildInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getEncryptionKey()Ljavax/crypto/SecretKey;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    sget-object v1, Lcom/google/android/location/os/Event;->GET_ENCRYPTION_KEY:Lcom/google/android/location/os/Event;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/os/real/RealOs;->getEncryptionKey(Landroid/content/Context;)Ljavax/crypto/SecretKey;

    move-result-object v0

    return-object v0
.end method

.method public getEncryptionKeyOrNull()Ljavax/crypto/SecretKey;
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/location/os/real/RealOs;->getEncryptionKey()Ljavax/crypto/SecretKey;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "NetworkLocationRealOs"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/location/utils/logging/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getExecutor()Ljava/util/concurrent/ExecutorService;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->executor:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public getFreeMemoryKb()I
    .locals 6

    new-instance v1, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v1}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    iget-object v2, p0, Lcom/google/android/location/os/real/RealOs;->context:Landroid/content/Context;

    const-string v3, "activity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    iget-wide v2, v1, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    const-wide/16 v4, 0x400

    div-long/2addr v2, v4

    long-to-int v2, v2

    return v2
.end method

.method public getLastKnownLocation()Lcom/google/android/location/os/LocationInterface;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->locationReceiver:Lcom/google/android/location/os/real/RealOs$LocationReceiver;

    invoke-interface {v0}, Lcom/google/android/location/os/real/RealOs$LocationReceiver;->getLastKnownLocation()Lcom/google/android/location/os/LocationInterface;

    move-result-object v0

    return-object v0
.end method

.method public getNlpMemoryKb()I
    .locals 2

    new-instance v0, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v0}, Landroid/os/Debug$MemoryInfo;-><init>()V

    invoke-static {v0}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    invoke-virtual {v0}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    move-result v1

    return v1
.end method

.method public getNlpParamsState()Lcom/google/android/location/os/NlpParametersState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->nlpParamsState:Lcom/google/android/location/os/NlpParametersState;

    return-object v0
.end method

.method public getSensorCacheDir()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/os/real/RealOs;->getSensorCacheDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public glsDeviceLocationQuery(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 2
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    sget-object v1, Lcom/google/android/location/os/Event;->GLS_DEVICE_LOCATION_QUERY:Lcom/google/android/location/os/Event;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->glsClient:Lcom/google/android/location/os/real/GlsClient;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/GlsClient;->deviceLocationQuery(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public glsModelQuery(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 2
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    sget-object v1, Lcom/google/android/location/os/Event;->GLS_MODEL_QUERY:Lcom/google/android/location/os/Event;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->glsClient:Lcom/google/android/location/os/real/GlsClient;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/GlsClient;->modelQuery(Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public glsQuery(Lcom/google/gmm/common/io/protocol/ProtoBuf;Lcom/google/gmm/common/io/protocol/ProtoBuf;)V
    .locals 2
    .param p1    # Lcom/google/gmm/common/io/protocol/ProtoBuf;
    .param p2    # Lcom/google/gmm/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    sget-object v1, Lcom/google/android/location/os/Event;->GLS_QUERY:Lcom/google/android/location/os/Event;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->glsClient:Lcom/google/android/location/os/real/GlsClient;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/real/GlsClient;->query(Lcom/google/gmm/common/io/protocol/ProtoBuf;Lcom/google/gmm/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public hasAccelerometer()Z
    .locals 3

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/os/real/RealOs;->sensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v2, v1}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public join()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/location/os/real/RealOs;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

    invoke-virtual {v1}, Lcom/google/android/location/os/real/CallbackRunner;->join()V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/os/real/RealOs;->executor:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v2, 0xa

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/location/os/real/RealOs;->alarmCancel(I)V

    const/4 v0, 0x0

    :goto_1
    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/location/os/real/RealOs;->wakeLockAcquired:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/location/os/real/RealOs;->wakeLockRelease(I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public locationReport(Lcom/google/android/location/data/NetworkLocation;Lcom/google/android/location/data/TravelDetectionType;)V
    .locals 2
    .param p1    # Lcom/google/android/location/data/NetworkLocation;
    .param p2    # Lcom/google/android/location/data/TravelDetectionType;

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/EventLog;->addLocationReport(Lcom/google/android/location/data/NetworkLocation;)V

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/location/data/NetworkLocation;->bestResult:Lcom/google/android/location/data/LocatorResult;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/location/data/NetworkLocation;->bestResult:Lcom/google/android/location/data/LocatorResult;

    iget-object v0, v0, Lcom/google/android/location/data/LocatorResult;->position:Lcom/google/android/location/data/Position;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/location/data/NetworkLocation;->bestResult:Lcom/google/android/location/data/LocatorResult;

    iget-object v0, v0, Lcom/google/android/location/data/LocatorResult;->status:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    sget-object v1, Lcom/google/android/location/data/LocatorResult$ResultStatus;->OK:Lcom/google/android/location/data/LocatorResult$ResultStatus;

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->locationReceiver:Lcom/google/android/location/os/real/RealOs$LocationReceiver;

    invoke-interface {v0, p1, p2}, Lcom/google/android/location/os/real/RealOs$LocationReceiver;->locationReport(Lcom/google/android/location/data/NetworkLocation;Lcom/google/android/location/data/TravelDetectionType;)V

    goto :goto_0
.end method

.method public makeFilePrivate(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    sget-object v1, Lcom/google/android/location/os/Event;->MAKE_FILE_PRIVATE:Lcom/google/android/location/os/Event;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    invoke-static {}, Lcom/google/android/location/os/real/SdkSpecific;->getInstance()Lcom/google/android/location/os/real/SdkSpecific;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/SdkSpecific;->makeFilePrivate(Ljava/io/File;)V

    return-void
.end method

.method public millisSinceBoot()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    sget-object v1, Lcom/google/android/location/os/Event;->MILLIS_SINCE_BOOT:Lcom/google/android/location/os/Event;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    invoke-static {}, Lcom/google/android/location/os/real/RealOs;->sinceBootMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public millisSinceEpoch()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    sget-object v1, Lcom/google/android/location/os/Event;->MILLIS_SINCE_EPOCH:Lcom/google/android/location/os/Event;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    invoke-static {}, Lcom/google/android/location/os/real/RealOs;->sinceEpochMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public nlpParamsStateDir()Ljava/io/File;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    sget-object v1, Lcom/google/android/location/os/Event;->NLP_PARAMS_STATE_DIR:Lcom/google/android/location/os/Event;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public onChange(Lcom/google/android/location/os/NlpParametersState;)V
    .locals 1
    .param p1    # Lcom/google/android/location/os/NlpParametersState;

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/CallbackRunner;->nlpParamtersStateChanged(Lcom/google/android/location/os/NlpParametersState;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onChange(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/location/os/NlpParametersState;

    invoke-virtual {p0, p1}, Lcom/google/android/location/os/real/RealOs;->onChange(Lcom/google/android/location/os/NlpParametersState;)V

    return-void
.end method

.method public onTravelModeDetected(Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;)V
    .locals 1
    .param p1    # Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/CallbackRunner;->onTravelModeDetected(Lcom/google/android/location/activity/TravelDetectionManager$TravelDetectionResult;)V

    return-void
.end method

.method public openMetricModelRawResource()Ljava/io/InputStream;
    .locals 3

    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v1, p0, Lcom/google/android/location/os/real/RealOs;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f020000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method

.method public persistentStateDir()Ljava/io/File;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    sget-object v1, Lcom/google/android/location/os/Event;->PERSISTENT_STATE_DIR:Lcom/google/android/location/os/Event;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public quit(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/CallbackRunner;->quit(Z)V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    return-void
.end method

.method public registerCallbacks(Lcom/google/android/location/os/Callbacks;)V
    .locals 1
    .param p1    # Lcom/google/android/location/os/Callbacks;

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/CallbackRunner;->start(Lcom/google/android/location/os/Callbacks;)V

    return-void
.end method

.method public seenDevicesCacheDir()Ljava/io/File;
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    sget-object v1, Lcom/google/android/location/os/Event;->SEEN_DEVICES_DIR:Lcom/google/android/location/os/Event;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/os/real/RealOs;->seenDevicesCacheDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public setActivityDetectionEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/CallbackRunner;->setActivityDetectionEnabled(Z)V

    return-void
.end method

.method public setPeriod(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/os/real/CallbackRunner;->setPeriod(IZ)V

    return-void
.end method

.method public signalRmiRequest(Lcom/google/android/location/data/NetworkLocation;)V
    .locals 1
    .param p1    # Lcom/google/android/location/data/NetworkLocation;

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->callbackRunner:Lcom/google/android/location/os/real/CallbackRunner;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/real/CallbackRunner;->signalRmiRequest(Lcom/google/android/location/data/NetworkLocation;)V

    return-void
.end method

.method public wakeLockAcquire(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/EventLog;->addWakeLockAcquire(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->wakeLockAcquired:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wakeLock "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already acquired"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->wakeLockAcquired:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->wakeLock:[Landroid/os/PowerManager$WakeLock;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->wifiLock:[Landroid/net/wifi/WifiManager$WifiLock;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    return-void
.end method

.method public wakeLockRelease(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    invoke-virtual {v0, p1}, Lcom/google/android/location/os/EventLog;->addWakeLockRelease(I)V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->wakeLockAcquired:[Z

    aget-boolean v0, v0, p1

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wakeLock "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already released"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->wifiLock:[Landroid/net/wifi/WifiManager$WifiLock;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->wakeLockAcquired:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, p1

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->wakeLock:[Landroid/os/PowerManager$WakeLock;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void
.end method

.method public wifiRequestScan()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->eventLog:Lcom/google/android/location/os/EventLog;

    sget-object v1, Lcom/google/android/location/os/Event;->WIFI_REQUEST_SCAN:Lcom/google/android/location/os/Event;

    invoke-virtual {v0, v1}, Lcom/google/android/location/os/EventLog;->addEvent(Lcom/google/android/location/os/Event;)V

    iget-object v0, p0, Lcom/google/android/location/os/real/RealOs;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    return-void
.end method
