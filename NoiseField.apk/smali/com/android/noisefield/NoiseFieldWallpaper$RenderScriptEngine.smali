.class Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;
.super Landroid/service/wallpaper/WallpaperService$Engine;
.source "NoiseFieldWallpaper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/noisefield/NoiseFieldWallpaper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RenderScriptEngine"
.end annotation


# instance fields
.field private mDensityDPI:I

.field private mRenderScript:Landroid/renderscript/RenderScriptGL;

.field private mWallpaperRS:Lcom/android/noisefield/NoiseFieldRS;

.field final synthetic this$0:Lcom/android/noisefield/NoiseFieldWallpaper;


# direct methods
.method private constructor <init>(Lcom/android/noisefield/NoiseFieldWallpaper;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->this$0:Lcom/android/noisefield/NoiseFieldWallpaper;

    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;-><init>(Landroid/service/wallpaper/WallpaperService;)V

    iput-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    iput-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/noisefield/NoiseFieldRS;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/noisefield/NoiseFieldWallpaper;Lcom/android/noisefield/NoiseFieldWallpaper$1;)V
    .locals 0
    .param p1    # Lcom/android/noisefield/NoiseFieldWallpaper;
    .param p2    # Lcom/android/noisefield/NoiseFieldWallpaper$1;

    invoke-direct {p0, p1}, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;-><init>(Lcom/android/noisefield/NoiseFieldWallpaper;)V

    return-void
.end method


# virtual methods
.method public destroyRenderer()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/noisefield/NoiseFieldRS;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/noisefield/NoiseFieldRS;

    invoke-virtual {v0}, Lcom/android/noisefield/NoiseFieldRS;->stop()V

    iput-object v1, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/noisefield/NoiseFieldRS;

    :cond_0
    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0, v1, v2, v2}, Landroid/renderscript/RenderScriptGL;->setSurface(Landroid/view/SurfaceHolder;II)V

    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->destroy()V

    iput-object v1, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onCreate(Landroid/view/SurfaceHolder;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/service/wallpaper/WallpaperService$Engine;->setTouchEventsEnabled(Z)V

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->setSizeFromLayout()V

    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iget-object v1, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->this$0:Lcom/android/noisefield/NoiseFieldWallpaper;

    invoke-virtual {v1}, Landroid/app/Service;->getApplication()Landroid/app/Application;

    move-result-object v1

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v1, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v1, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mDensityDPI:I

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->onDestroy()V

    invoke-virtual {p0}, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->destroyRenderer()V

    return-void
.end method

.method public onSurfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 6
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceChanged(Landroid/view/SurfaceHolder;III)V

    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0, p1, p3, p4}, Landroid/renderscript/RenderScriptGL;->setSurface(Landroid/view/SurfaceHolder;II)V

    :cond_0
    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/noisefield/NoiseFieldRS;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/noisefield/NoiseFieldRS;

    invoke-direct {v0}, Lcom/android/noisefield/NoiseFieldRS;-><init>()V

    iput-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/noisefield/NoiseFieldRS;

    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/noisefield/NoiseFieldRS;

    iget v1, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mDensityDPI:I

    iget-object v2, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    iget-object v3, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->this$0:Lcom/android/noisefield/NoiseFieldWallpaper;

    invoke-virtual {v3}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/noisefield/NoiseFieldRS;->init(ILandroid/renderscript/RenderScriptGL;Landroid/content/res/Resources;II)V

    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/noisefield/NoiseFieldRS;

    invoke-virtual {v0}, Lcom/android/noisefield/NoiseFieldRS;->start()V

    :cond_1
    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/noisefield/NoiseFieldRS;

    invoke-virtual {v0, p3, p4}, Lcom/android/noisefield/NoiseFieldRS;->resize(II)V

    return-void
.end method

.method public onSurfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceCreated(Landroid/view/SurfaceHolder;)V

    new-instance v0, Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    invoke-direct {v0}, Landroid/renderscript/RenderScriptGL$SurfaceConfig;-><init>()V

    new-instance v1, Landroid/renderscript/RenderScriptGL;

    iget-object v2, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->this$0:Lcom/android/noisefield/NoiseFieldWallpaper;

    invoke-direct {v1, v2, v0}, Landroid/renderscript/RenderScriptGL;-><init>(Landroid/content/Context;Landroid/renderscript/RenderScriptGL$SurfaceConfig;)V

    iput-object v1, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    iget-object v1, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    sget-object v2, Landroid/renderscript/RenderScript$Priority;->LOW:Landroid/renderscript/RenderScript$Priority;

    invoke-virtual {v1, v2}, Landroid/renderscript/RenderScript;->setPriority(Landroid/renderscript/RenderScript$Priority;)V

    return-void
.end method

.method public onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V

    invoke-virtual {p0}, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->destroyRenderer()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onTouchEvent(Landroid/view/MotionEvent;)V

    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/noisefield/NoiseFieldRS;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/noisefield/NoiseFieldRS;

    invoke-virtual {v0, p1}, Lcom/android/noisefield/NoiseFieldRS;->onTouchEvent(Landroid/view/MotionEvent;)V

    :cond_0
    return-void
.end method

.method public onVisibilityChanged(Z)V
    .locals 1
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onVisibilityChanged(Z)V

    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/noisefield/NoiseFieldRS;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/noisefield/NoiseFieldRS;

    invoke-virtual {v0}, Lcom/android/noisefield/NoiseFieldRS;->start()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/noisefield/NoiseFieldWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/noisefield/NoiseFieldRS;

    invoke-virtual {v0}, Lcom/android/noisefield/NoiseFieldRS;->stop()V

    goto :goto_0
.end method
