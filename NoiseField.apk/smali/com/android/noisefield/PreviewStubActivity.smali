.class public Lcom/android/noisefield/PreviewStubActivity;
.super Landroid/app/Activity;
.source "PreviewStubActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;
    }
.end annotation


# static fields
.field public static final CLASS_NAME:Ljava/lang/String; = "CLASS_NAME"

.field private static final LOG_TAG:Ljava/lang/String; = "PreviewStubActivity"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "PACKAGE_NAME"


# instance fields
.field private mView:Landroid/view/View;

.field private mWallpaperConnection:Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

.field private mWallpaperIntent:Landroid/content/Intent;

.field private mWallpaperManager:Landroid/app/WallpaperManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/noisefield/PreviewStubActivity;)Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;
    .locals 1
    .param p0    # Lcom/android/noisefield/PreviewStubActivity;

    iget-object v0, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperConnection:Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/noisefield/PreviewStubActivity;Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;)Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;
    .locals 0
    .param p0    # Lcom/android/noisefield/PreviewStubActivity;
    .param p1    # Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

    iput-object p1, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperConnection:Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/noisefield/PreviewStubActivity;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/noisefield/PreviewStubActivity;

    iget-object v0, p0, Lcom/android/noisefield/PreviewStubActivity;->mView:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onAttachedToWindow()V

    const-string v1, "PreviewStubActivity"

    const-string v2, "onAttachedToWindow() "

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iget-object v1, p0, Lcom/android/noisefield/PreviewStubActivity;->mView:Landroid/view/View;

    new-instance v2, Lcom/android/noisefield/PreviewStubActivity$1;

    invoke-direct {v2, p0}, Lcom/android/noisefield/PreviewStubActivity$1;-><init>(Lcom/android/noisefield/PreviewStubActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v3, "PreviewStubActivity"

    const-string v4, "onCreate() "

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v3, 0x7f030000

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    const/high16 v3, 0x7f080000

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/noisefield/PreviewStubActivity;->mView:Landroid/view/View;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "PACKAGE_NAME"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "CLASS_NAME"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    iput-object v3, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperIntent:Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperIntent:Landroid/content/Intent;

    const-string v4, "android.service.wallpaper.WallpaperService"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperIntent:Landroid/content/Intent;

    new-instance v4, Landroid/content/ComponentName;

    invoke-direct {v4, v2, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v3, "PreviewStubActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mWallpaperIntent = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperIntent:Landroid/content/Intent;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v3

    iput-object v3, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperManager:Landroid/app/WallpaperManager;

    new-instance v3, Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

    iget-object v4, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperIntent:Landroid/content/Intent;

    invoke-direct {v3, p0, v4}, Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;-><init>(Lcom/android/noisefield/PreviewStubActivity;Landroid/content/Intent;)V

    iput-object v3, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperConnection:Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperConnection:Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperConnection:Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

    invoke-virtual {v0}, Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;->disconnect()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperConnection:Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "PreviewStubActivity"

    const-string v1, "onPause() ......"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperConnection:Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperConnection:Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

    iget-object v0, v0, Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperConnection:Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

    iget-object v0, v0, Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/service/wallpaper/IWallpaperEngine;->setVisibility(Z)V

    const-string v0, "PreviewStubActivity"

    const-string v1, "onPause() setVisibility(false)"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "PreviewStubActivity"

    const-string v1, "onResume() "

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperConnection:Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperConnection:Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

    iget-object v0, v0, Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    if-eqz v0, :cond_0

    :try_start_0
    const-string v0, "PreviewStubActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume() inner mWallpaperConnection = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperConnection:Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperConnection:Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;

    iget-object v0, v0, Lcom/android/noisefield/PreviewStubActivity$WallpaperConnection;->mEngine:Landroid/service/wallpaper/IWallpaperEngine;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/service/wallpaper/IWallpaperEngine;->setVisibility(Z)V

    const-string v0, "PreviewStubActivity"

    const-string v1, "onResume() setVisibility(true)"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setLiveWallpaper(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const-string v1, "PreviewStubActivity"

    const-string v2, "setLiveWallpaper() "

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v1, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperManager:Landroid/app/WallpaperManager;

    invoke-virtual {v1}, Landroid/app/WallpaperManager;->getIWallpaperManager()Landroid/app/IWallpaperManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/app/IWallpaperManager;->setWallpaperComponent(Landroid/content/ComponentName;)V

    iget-object v1, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperManager:Landroid/app/WallpaperManager;

    const/high16 v2, 0x3f000000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/WallpaperManager;->setWallpaperOffsetSteps(FF)V

    iget-object v1, p0, Lcom/android/noisefield/PreviewStubActivity;->mWallpaperManager:Landroid/app/WallpaperManager;

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/high16 v3, 0x3f000000

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/WallpaperManager;->setWallpaperOffsets(Landroid/os/IBinder;FF)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "PreviewStubActivity"

    const-string v2, "Failure setting wallpaper"

    invoke-static {v1, v2, v0}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method
