.class public Lcom/mediatek/browser/op02/Op02BrowserSmallFeatureEx;
.super Lcom/mediatek/browser/ext/BrowserSmallFeatureEx;
.source "Op02BrowserSmallFeatureEx.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BrowserPluginEx"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/mediatek/browser/ext/BrowserSmallFeatureEx;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public addDefaultBookmarksForCustomer(Lcom/mediatek/browser/ext/IBrowserProvider2Ex;Landroid/database/sqlite/SQLiteDatabase;JI)I
    .locals 9
    .param p1    # Lcom/mediatek/browser/ext/IBrowserProvider2Ex;
    .param p2    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3    # J
    .param p5    # I

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: addDefaultBookmarksForCustomer --OP02 implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/browser/op02/Op02BrowserSmallFeatureEx;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v0, 0x7f040001

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v4

    array-length v8, v4

    const/4 v5, 0x2

    move-object v0, p1

    move-object v1, p2

    move-wide v2, p3

    move v6, p5

    invoke-interface/range {v0 .. v6}, Lcom/mediatek/browser/ext/IBrowserProvider2Ex;->addDefaultBookmarksHost(Landroid/database/sqlite/SQLiteDatabase;J[Ljava/lang/CharSequence;II)I

    move-result v0

    return v0
.end method

.method public getCustomerHomepage()Ljava/lang/String;
    .locals 2

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: getCustomerHomepage --OP02 implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/browser/op02/Op02BrowserSmallFeatureEx;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOperatorUA(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;

    const-string v4, "BrowserPluginEx"

    const-string v5, "Enter: getOperatorUA --OP02 implement"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/Date;

    sget-wide v4, Landroid/os/Build;->TIME:J

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    const/4 v3, 0x0

    const/4 v1, 0x0

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v5, "v3"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v1, "ztemt73/v3"

    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "MM.dd.yyyy"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Linux/3.0.13 Android/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Release/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Browser/AppleWebKit534.30 Profile/MIDP-2.0 Configuration/CLDC-1.1"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Mobile Safari/534.30"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Android 4.0.1;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    :cond_0
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v5, "75"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v5, "v1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v1, "eagle75/v1"

    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "MM.dd.yyyy"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v5, "athens15"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v5, "v1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v1, "athens15/v1"

    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "MM.dd.yyyy"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_2
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v5, "athens15"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v5, "v2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v1, "athens15/v2"

    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "MM.dd.yyyy"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    :cond_3
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v5, "lenovo75"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v1, "lenovo75/v1"

    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "MM.dd.yyyy"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    :cond_4
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "MM.dd.yyyy"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0
.end method

.method public setTextEncodingChoices(Landroid/preference/ListPreference;)V
    .locals 2
    .param p1    # Landroid/preference/ListPreference;

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: setTextEncodingChoices --OP02 implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/browser/op02/Op02BrowserSmallFeatureEx;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f040003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/mediatek/browser/op02/Op02BrowserSmallFeatureEx;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f040004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    return-void
.end method

.method public updatePreferenceItem(Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/String;

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: updatePreferenceItem --OP02 implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public updatePreferenceItemAndSetListener(Landroid/preference/Preference;Ljava/lang/String;Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .locals 2
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/preference/Preference$OnPreferenceChangeListener;

    const-string v0, "BrowserPluginEx"

    const-string v1, "Enter: updatePreferenceItemAndSetListener --OP02 implement"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
