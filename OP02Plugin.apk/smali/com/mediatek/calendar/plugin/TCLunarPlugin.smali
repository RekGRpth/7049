.class public Lcom/mediatek/calendar/plugin/TCLunarPlugin;
.super Lcom/mediatek/calendar/ext/DefaultLunarExtension;
.source "TCLunarPlugin.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TCLunarPlugin"

.field private static sLunarFestChunjie:Ljava/lang/String;

.field private static sLunarFestDuanwu:Ljava/lang/String;

.field private static sLunarFestZhongqiu:Ljava/lang/String;

.field private static sSolarTermNames:[Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/mediatek/calendar/ext/DefaultLunarExtension;-><init>()V

    const-string v0, "TCLunarPlugin"

    const-string v1, "in constructor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->loadResources()V

    const-string v0, "TCLunarPlugin"

    const-string v1, "load resources done"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private canShowTCLunar()Z
    .locals 2

    sget-object v0, Ljava/util/Locale;->TRADITIONAL_CHINESE:Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private loadResources()V
    .locals 2

    iget-object v1, p0, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f050000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->sLunarFestChunjie:Ljava/lang/String;

    const/high16 v1, 0x7f040000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->sSolarTermNames:[Ljava/lang/String;

    const v1, 0x7f050001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->sLunarFestDuanwu:Ljava/lang/String;

    const v1, 0x7f050002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->sLunarFestZhongqiu:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public canShowLunarCalendar()Z
    .locals 1

    invoke-direct {p0}, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->canShowTCLunar()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/mediatek/calendar/ext/DefaultLunarExtension;->canShowLunarCalendar()Z

    move-result v0

    goto :goto_0
.end method

.method public getGregFestival(II)Ljava/lang/String;
    .locals 1
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->canShowTCLunar()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/mediatek/calendar/ext/DefaultLunarExtension;->getGregFestival(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getLunarFestival(II)Ljava/lang/String;
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x5

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->canShowTCLunar()Z

    move-result v0

    if-eqz v0, :cond_2

    if-ne p1, v1, :cond_0

    if-ne p2, v1, :cond_0

    sget-object v0, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->sLunarFestChunjie:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    if-ne p1, v2, :cond_1

    if-ne p2, v2, :cond_1

    sget-object v0, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->sLunarFestDuanwu:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    if-ne p1, v0, :cond_2

    const/16 v0, 0xf

    if-ne p2, v0, :cond_2

    sget-object v0, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->sLunarFestZhongqiu:Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2}, Lcom/mediatek/calendar/ext/DefaultLunarExtension;->getLunarFestival(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSolarTermNameByIndex(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    invoke-direct {p0}, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->canShowTCLunar()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    sget-object v0, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->sSolarTermNames:[Ljava/lang/String;

    array-length v0, v0

    if-le p1, v0, :cond_1

    :cond_0
    const-string v0, "TCLunarPlugin"

    const-string v1, "SolarTerm should between [1, 24]"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/mediatek/calendar/plugin/TCLunarPlugin;->sSolarTermNames:[Ljava/lang/String;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/mediatek/calendar/ext/DefaultLunarExtension;->getSolarTermNameByIndex(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
