.class public Lcom/mediatek/contacts/plugin/OP02SimPickExtension;
.super Lcom/android/contacts/ext/SimPickExtension;
.source "OP02SimPickExtension.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "OP02ContactEx"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/contacts/ext/SimPickExtension;-><init>()V

    return-void
.end method


# virtual methods
.method public setSimSignal(Landroid/widget/TextView;II)V
    .locals 2
    .param p1    # Landroid/widget/TextView;
    .param p2    # I
    .param p3    # I

    if-ne p2, p3, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    const-string v0, "OP02ContactEx"

    const-string v1, "[setSimSignal] mSimSignal is visible"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "OP02ContactEx"

    const-string v1, "[setSimSignal] mSimSignal is gone"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
