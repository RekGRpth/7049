.class Lcom/mediatek/filemanager/plugin/SystemIconExtension$1;
.super Ljava/lang/Object;
.source "SystemIconExtension.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/filemanager/plugin/SystemIconExtension;->createSystemFolder()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/filemanager/plugin/SystemIconExtension;


# direct methods
.method constructor <init>(Lcom/mediatek/filemanager/plugin/SystemIconExtension;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension$1;->this$0:Lcom/mediatek/filemanager/plugin/SystemIconExtension;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v3, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension$1;->this$0:Lcom/mediatek/filemanager/plugin/SystemIconExtension;

    # getter for: Lcom/mediatek/filemanager/plugin/SystemIconExtension;->mSystemFolderIconMap:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/mediatek/filemanager/plugin/SystemIconExtension;->access$000(Lcom/mediatek/filemanager/plugin/SystemIconExtension;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;

    new-instance v0, Ljava/io/File;

    invoke-static {v2}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;->access$100(Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    goto :goto_0

    :cond_1
    return-void
.end method
