.class Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;
.super Ljava/lang/Object;
.source "SystemIconExtension.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/filemanager/plugin/SystemIconExtension;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IconInfo"
.end annotation


# instance fields
.field private mIcon:Landroid/graphics/Bitmap;

.field private mResourceId:I


# direct methods
.method public constructor <init>(ILandroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;->mIcon:Landroid/graphics/Bitmap;

    iput p1, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;->mResourceId:I

    return-void
.end method


# virtual methods
.method public getIcon()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;->mIcon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getResourceId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;->mResourceId:I

    return v0
.end method

.method public setIcon(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;->mIcon:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setResourceId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;->mResourceId:I

    return-void
.end method
