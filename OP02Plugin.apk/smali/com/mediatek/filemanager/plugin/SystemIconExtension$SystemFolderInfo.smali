.class Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;
.super Ljava/lang/Object;
.source "SystemIconExtension.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/filemanager/plugin/SystemIconExtension;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SystemFolderInfo"
.end annotation


# instance fields
.field private mPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;->mPath:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;

    iget-object v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;->mPath:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    const/4 v0, 0x1

    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;

    if-eqz v1, :cond_2

    check-cast p1, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;

    invoke-virtual {p1}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;->mPath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
