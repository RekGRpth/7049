.class public Lcom/mediatek/filemanager/plugin/SystemIconExtension;
.super Lcom/mediatek/filemanager/ext/DefaultIconExtension;
.source "SystemIconExtension.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;,
        Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;
    }
.end annotation


# static fields
.field private static final DOCUMENT:Ljava/lang/String; = "Document"

.field private static final DOWNLOAD:Ljava/lang/String; = "Download"

.field private static final MUSIC:Ljava/lang/String; = "Music"

.field private static final PHOTO:Ljava/lang/String; = "Photo"

.field private static final RECEIVED:Ljava/lang/String; = "Received File"

.field private static final VIDEO:Ljava/lang/String; = "Video"


# instance fields
.field private mPluginResource:Landroid/content/res/Resources;

.field private mSystemFolderIconMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;",
            "Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/mediatek/filemanager/ext/DefaultIconExtension;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension;->mSystemFolderIconMap:Ljava/util/HashMap;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension;->mPluginResource:Landroid/content/res/Resources;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/filemanager/plugin/SystemIconExtension;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mediatek/filemanager/plugin/SystemIconExtension;

    iget-object v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension;->mSystemFolderIconMap:Ljava/util/HashMap;

    return-object v0
.end method

.method private createSystemFolder()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mediatek/filemanager/plugin/SystemIconExtension$1;

    invoke-direct {v1, p0}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$1;-><init>(Lcom/mediatek/filemanager/plugin/SystemIconExtension;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method


# virtual methods
.method public createSystemFolder(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension;->mSystemFolderIconMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension;->mSystemFolderIconMap:Ljava/util/HashMap;

    new-instance v1, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Document"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;

    const/high16 v3, 0x7f020000

    invoke-direct {v2, v3, v4}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;-><init>(ILandroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension;->mSystemFolderIconMap:Ljava/util/HashMap;

    new-instance v1, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Download"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;

    const v3, 0x7f020001

    invoke-direct {v2, v3, v4}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;-><init>(ILandroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension;->mSystemFolderIconMap:Ljava/util/HashMap;

    new-instance v1, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Music"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;

    const v3, 0x7f020002

    invoke-direct {v2, v3, v4}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;-><init>(ILandroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension;->mSystemFolderIconMap:Ljava/util/HashMap;

    new-instance v1, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Photo"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;

    const v3, 0x7f020003

    invoke-direct {v2, v3, v4}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;-><init>(ILandroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension;->mSystemFolderIconMap:Ljava/util/HashMap;

    new-instance v1, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Received File"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;

    const v3, 0x7f020004

    invoke-direct {v2, v3, v4}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;-><init>(ILandroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension;->mSystemFolderIconMap:Ljava/util/HashMap;

    new-instance v1, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Video"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;

    const v3, 0x7f020005

    invoke-direct {v2, v3, v4}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;-><init>(ILandroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/mediatek/filemanager/plugin/SystemIconExtension;->createSystemFolder()V

    return-void
.end method

.method public getSystemFolderIcon(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v0, 0x0

    new-instance v2, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;

    invoke-direct {v2, p1}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension;->mSystemFolderIconMap:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;->getIcon()Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension;->mPluginResource:Landroid/content/res/Resources;

    invoke-virtual {v1}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;->getResourceId()I

    move-result v4

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;->setIcon(Landroid/graphics/Bitmap;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$IconInfo;->getIcon()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public isSystemFolder(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/filemanager/plugin/SystemIconExtension;->mSystemFolderIconMap:Ljava/util/HashMap;

    new-instance v1, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;

    invoke-direct {v1, p1}, Lcom/mediatek/filemanager/plugin/SystemIconExtension$SystemFolderInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
