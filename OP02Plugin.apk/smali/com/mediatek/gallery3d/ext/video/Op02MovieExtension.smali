.class public Lcom/mediatek/gallery3d/ext/video/Op02MovieExtension;
.super Lcom/mediatek/gallery3d/ext/MovieExtension;
.source "Op02MovieExtension.java"

# interfaces
.implements Lcom/mediatek/gallery3d/ext/IMovieStrategy;


# static fields
.field private static final LOG:Z = true

.field private static final TAG:Ljava/lang/String; = "Op02MovieExtension"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/gallery3d/ext/MovieExtension;-><init>()V

    return-void
.end method


# virtual methods
.method public getFeatureList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public getMovieStrategy()Lcom/mediatek/gallery3d/ext/IMovieStrategy;
    .locals 0

    return-object p0
.end method

.method public shouldEnableCheckLongSleep()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public shouldEnableNMP(Lcom/mediatek/gallery3d/ext/IMovieItem;)Z
    .locals 3
    .param p1    # Lcom/mediatek/gallery3d/ext/IMovieItem;

    const/4 v0, 0x0

    invoke-interface {p1}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {p1}, Lcom/mediatek/gallery3d/ext/IMovieItem;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/gallery3d/ext/MovieUtils;->isLocalFile(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public shouldEnableServerTimeout()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
