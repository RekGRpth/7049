.class public Lcom/mediatek/mms/op02/Op02MmsConfigExt;
.super Lcom/mediatek/mms/ext/MmsConfigImpl;
.source "Op02MmsConfigExt.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/Op02MmsConfigExt"

.field private static mOp02SmsToMmsThreshold:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xa

    sput v0, Lcom/mediatek/mms/op02/Op02MmsConfigExt;->mOp02SmsToMmsThreshold:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mediatek/mms/ext/MmsConfigImpl;-><init>()V

    return-void
.end method


# virtual methods
.method public getSmsToMmsTextThreshold()I
    .locals 1

    sget v0, Lcom/mediatek/mms/op02/Op02MmsConfigExt;->mOp02SmsToMmsThreshold:I

    return v0
.end method

.method public isEnableMultiSmsSaveLocation()Z
    .locals 2

    const-string v0, "Mms/Op02MmsConfigExt"

    const-string v1, "Enable MultiSmsSaveLocation "

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public isEnableSIMSmsForSetting()Z
    .locals 2

    const-string v0, "Mms/Op02MmsConfigExt"

    const-string v1, "Disable SIM Message at setting "

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public setSmsToMmsTextThreshold(I)V
    .locals 0
    .param p1    # I

    return-void
.end method
