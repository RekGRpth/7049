.class public Lcom/mediatek/mms/op02/Op02MmsConversationExt;
.super Lcom/mediatek/mms/ext/MmsConversationImpl;
.source "Op02MmsConversationExt.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Mms/Op02MmsConversationExt"


# instance fields
.field private mMenuSimSms:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/mediatek/mms/ext/MmsConversationImpl;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/mms/op02/Op02MmsConversationExt;->mMenuSimSms:I

    return-void
.end method


# virtual methods
.method public addOptionMenu(Landroid/view/Menu;I)V
    .locals 8
    .param p1    # Landroid/view/Menu;
    .param p2    # I

    const v5, 0x7f020006

    const v7, 0x7f050009

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/mediatek/mms/op02/Op02MmsConversationExt;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    iput v3, p0, Lcom/mediatek/mms/op02/Op02MmsConversationExt;->mMenuSimSms:I

    iget v3, p0, Lcom/mediatek/mms/op02/Op02MmsConversationExt;->mMenuSimSms:I

    invoke-virtual {p0, v7}, Lcom/mediatek/mms/op02/Op02MmsConversationExt;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v6, v3, v6, v4}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const-string v3, "Mms/Op02MmsConversationExt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Add Menu: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v7}, Lcom/mediatek/mms/op02/Op02MmsConversationExt;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v3, p0, Lcom/mediatek/mms/op02/Op02MmsConversationExt;->mMenuSimSms:I

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {p0}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const-string v3, "Mms/Op02MmsConversationExt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Menu: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v7}, Lcom/mediatek/mms/op02/Op02MmsConversationExt;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is disabled"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    iget v1, p0, Lcom/mediatek/mms/op02/Op02MmsConversationExt;->mMenuSimSms:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/mms/op02/Op02MmsConversationExt;->getHost()Lcom/mediatek/mms/ext/IMmsConversationHost;

    move-result-object v0

    invoke-interface {v0}, Lcom/mediatek/mms/ext/IMmsConversationHost;->showSimSms()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
