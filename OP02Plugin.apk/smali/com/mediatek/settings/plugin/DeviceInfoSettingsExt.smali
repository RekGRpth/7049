.class public Lcom/mediatek/settings/plugin/DeviceInfoSettingsExt;
.super Lcom/android/settings/ext/DefaultDeviceInfoSettingsExt;
.source "DeviceInfoSettingsExt.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DeviceInfoSettingsExt"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/ext/DefaultDeviceInfoSettingsExt;-><init>()V

    return-void
.end method


# virtual methods
.method public initSummary(Landroid/preference/Preference;)V
    .locals 2
    .param p1    # Landroid/preference/Preference;

    if-eqz p1, :cond_0

    const-string v0, "DeviceInfoSettingsExt"

    const-string v1, "empty summary"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
