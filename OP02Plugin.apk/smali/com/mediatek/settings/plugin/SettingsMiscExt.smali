.class public Lcom/mediatek/settings/plugin/SettingsMiscExt;
.super Landroid/content/ContextWrapper;
.source "SettingsMiscExt.java"

# interfaces
.implements Lcom/android/settings/ext/ISettingsMiscExt;


# static fields
.field private static final CU_3GNET_NAME:Ljava/lang/String; = "3gnet"

.field private static final CU_3GWAP_NAME:Ljava/lang/String; = "3gwap"

.field private static final CU_MMS_TYPE:Ljava/lang/String; = "mms"

.field private static final SIM_CARD_1:I = 0x0

.field private static final SIM_CARD_2:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SettingsMiscExt"


# instance fields
.field private mApnToUseId:J

.field mContext:Landroid/content/Context;

.field private mResult:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/settings/plugin/SettingsMiscExt;->mApnToUseId:J

    invoke-virtual {p0}, Lcom/mediatek/settings/plugin/SettingsMiscExt;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/settings/plugin/SettingsMiscExt;->mContext:Landroid/content/Context;

    return-void
.end method

.method private IsDefaultApn(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    const-string v1, "3gnet"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p2, :cond_0

    const-string v1, "mms"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    const-string v1, "3gwap"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private UpdateOperApn(Landroid/content/Context;Landroid/content/ContentValues;ZLjava/lang/String;Ljava/lang/String;Landroid/net/Uri;ILjava/lang/String;)Z
    .locals 17
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Z
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Landroid/net/Uri;
    .param p7    # I
    .param p8    # Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2}, Lcom/mediatek/settings/plugin/SettingsMiscExt;->IsDefaultApn(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "SettingsMiscExt"

    const-string v8, "not oper APN"

    invoke-static {v5, v8}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    :goto_0
    return v5

    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v4, p6

    if-eqz p3, :cond_1

    rsub-int/lit8 v13, p7, 0x1

    packed-switch v13, :pswitch_data_0

    :cond_1
    :goto_1
    const/4 v5, 0x2

    new-array v7, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p8, v7, v5

    const/4 v5, 0x1

    aput-object p4, v7, v5

    const-string v6, "numeric=? AND apn=? AND sourcetype=0"

    const-string v5, "SettingsMiscExt"

    invoke-static {v5, v6}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v15, "_id"

    aput-object v15, v5, v8

    const/4 v8, 0x1

    const-string v15, "type"

    aput-object v15, v5, v8

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    if-nez v10, :cond_2

    const-string v5, "SettingsMiscExt"

    const-string v8, "query in updated cursor == null"

    invoke-static {v5, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    if-eqz v10, :cond_a

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_9

    const/4 v5, 0x1

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    if-eqz p5, :cond_3

    const-string v5, "mms"

    move-object/from16 v0, p5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    :cond_3
    if-eqz v14, :cond_5

    const-string v5, "mms"

    invoke-virtual {v14, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_4
    if-eqz p5, :cond_8

    const-string v5, "mms"

    move-object/from16 v0, p5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    if-eqz v14, :cond_8

    const-string v5, "mms"

    invoke-virtual {v14, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_5
    const/4 v5, 0x0

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    int-to-long v15, v5

    move-wide v0, v15

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v12

    :try_start_0
    sget-object v5, Lcom/mediatek/settings/plugin/OmacpApn;->sProjection:[Ljava/lang/String;

    const/16 v8, 0x10

    aget-object v5, v5, v8

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v3, v12, v0, v5, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-gez v5, :cond_6

    sget-object v5, Lcom/mediatek/settings/plugin/OmacpApn;->sProjection:[Ljava/lang/String;

    const/16 v8, 0x10

    aget-object v5, v5, v8

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v5, 0x0

    goto/16 :goto_0

    :pswitch_0
    sget-object v4, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    goto/16 :goto_1

    :pswitch_1
    sget-object v4, Landroid/provider/Telephony$Carriers$GeminiCarriers;->CONTENT_URI:Landroid/net/Uri;

    goto/16 :goto_1

    :cond_6
    if-nez p3, :cond_7

    const/4 v5, 0x0

    :try_start_1
    invoke-interface {v10, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v15

    move-object/from16 v0, p0

    iput-wide v15, v0, Lcom/mediatek/settings/plugin/SettingsMiscExt;->mApnToUseId:J

    :cond_7
    const/4 v9, 0x1

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/mediatek/settings/plugin/SettingsMiscExt;->mResult:Z
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_8
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_2

    :catch_0
    move-exception v11

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_9
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    const-string v5, "SettingsMiscExt"

    const-string v8, "cursor end"

    invoke-static {v5, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "SettingsMiscExt"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "return bSuc = "

    invoke-virtual {v8, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v9

    goto/16 :goto_0

    :cond_a
    const/4 v5, 0x0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private forTheOtherCard(Landroid/content/Context;ILandroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/content/ContentValues;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const-string v10, "SettingsMiscExt"

    const-string v11, "for the other card"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    rsub-int/lit8 v8, p2, 0x1

    const/4 v9, 0x0

    packed-switch v8, :pswitch_data_0

    :goto_0
    if-nez v9, :cond_0

    const/4 v10, 0x0

    :goto_1
    return v10

    :pswitch_0
    sget-object v9, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    :pswitch_1
    sget-object v9, Landroid/provider/Telephony$Carriers$GeminiCarriers;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    :cond_0
    new-instance v7, Lcom/mediatek/settings/plugin/OmacpApn;

    move-object/from16 v0, p4

    invoke-direct {v7, p1, v8, v9, v0}, Lcom/mediatek/settings/plugin/OmacpApn;-><init>(Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/mediatek/settings/plugin/OmacpApn;->getExistOmacpId()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const-string v10, "SettingsMiscExt"

    const-string v11, "The other card: this apn already exists!"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    const-wide/16 v5, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v7, p1, v0}, Lcom/mediatek/settings/plugin/OmacpApn;->insert(Landroid/content/Context;Landroid/content/ContentValues;)J

    move-result-wide v5

    const-string v10, "SettingsMiscExt"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "The other id = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v10, -0x1

    cmp-long v10, v5, v10

    if-nez v10, :cond_3

    const/4 v10, 0x0

    goto :goto_1

    :cond_3
    const/4 v10, 0x1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private insertAPN(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Landroid/net/Uri;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;

    const/4 v6, 0x0

    const-string v12, "mms"

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    new-instance v8, Lcom/mediatek/settings/plugin/OmacpApn;

    move/from16 v0, p4

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    invoke-direct {v8, p1, v0, v1, v2}, Lcom/mediatek/settings/plugin/OmacpApn;-><init>(Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/mediatek/settings/plugin/OmacpApn;->getExistOmacpId()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v11, :cond_0

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/HashMap;

    move-object/from16 v0, p7

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    const/4 v6, 0x1

    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/mediatek/settings/plugin/SettingsMiscExt;->mResult:Z

    if-nez v7, :cond_0

    move-object/from16 v0, p7

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    iput-wide v12, p0, Lcom/mediatek/settings/plugin/SettingsMiscExt;->mApnToUseId:J

    :cond_0
    const/4 v12, 0x1

    if-eq v6, v12, :cond_1

    move-object/from16 v0, p2

    invoke-virtual {v8, p1, v0}, Lcom/mediatek/settings/plugin/OmacpApn;->insert(Landroid/content/Context;Landroid/content/ContentValues;)J

    move-result-wide v4

    const-wide/16 v12, -0x1

    cmp-long v12, v4, v12

    if-eqz v12, :cond_1

    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/mediatek/settings/plugin/SettingsMiscExt;->mResult:Z

    if-nez v7, :cond_1

    iput-wide v4, p0, Lcom/mediatek/settings/plugin/SettingsMiscExt;->mApnToUseId:J

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getApnPref(Landroid/preference/PreferenceGroup;I[I)Landroid/preference/Preference;
    .locals 7
    .param p1    # Landroid/preference/PreferenceGroup;
    .param p2    # I
    .param p3    # [I

    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_1

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v3

    const-string v4, "3gnet"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    aget v4, p3, v1

    if-nez v4, :cond_0

    move-object v0, v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const-string v4, "SettingsMiscExt"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Get apn: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method public getApnUserId()J
    .locals 2

    iget-wide v0, p0, Lcom/mediatek/settings/plugin/SettingsMiscExt;->mApnToUseId:J

    return-wide v0
.end method

.method public getResult()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/settings/plugin/SettingsMiscExt;->mResult:Z

    return v0
.end method

.method public getTetherWifiSSID(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/settings/plugin/SettingsMiscExt;->mContext:Landroid/content/Context;

    const v1, 0x104040b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isAllowEditPresetApn(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const-string v0, "46001"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWifiToggleCouldDisabled(Landroid/content/Context;)Z
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x1

    return v0
.end method

.method public removeTetherApnSettings(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)V
    .locals 0
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Landroid/preference/Preference;

    invoke-virtual {p1, p2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    return-void
.end method

.method public setTimeoutPrefTitle(Landroid/preference/Preference;)V
    .locals 3
    .param p1    # Landroid/preference/Preference;

    const v2, 0x7f05000a

    move-object v0, p1

    check-cast v0, Landroid/preference/DialogPreference;

    iget-object v1, p0, Lcom/mediatek/settings/plugin/SettingsMiscExt;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/settings/plugin/SettingsMiscExt;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/DialogPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public updateApn(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Landroid/net/Uri;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v4, p8

    move-object v5, p3

    move-object v6, p5

    move v7, p4

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/mediatek/settings/plugin/SettingsMiscExt;->UpdateOperApn(Landroid/content/Context;Landroid/content/ContentValues;ZLjava/lang/String;Ljava/lang/String;Landroid/net/Uri;ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v4, p8

    move-object v5, p3

    move-object v6, p5

    move v7, p4

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/mediatek/settings/plugin/SettingsMiscExt;->UpdateOperApn(Landroid/content/Context;Landroid/content/ContentValues;ZLjava/lang/String;Ljava/lang/String;Landroid/net/Uri;ILjava/lang/String;)Z

    :goto_0
    return-void

    :cond_0
    invoke-direct/range {p0 .. p7}, Lcom/mediatek/settings/plugin/SettingsMiscExt;->insertAPN(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move v2, p4

    move-object v3, p2

    move-object v4, p6

    move-object/from16 v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/settings/plugin/SettingsMiscExt;->forTheOtherCard(Landroid/content/Context;ILandroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method
