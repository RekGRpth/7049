.class public Lcom/mediatek/settings/plugin/SimManagementExt;
.super Ljava/lang/Object;
.source "SimManagementExt.java"

# interfaces
.implements Lcom/android/settings/ext/ISimManagementExt;


# static fields
.field private static final KEY_3G_SERVICE_SETTING:Ljava/lang/String; = "3g_service_settings"

.field private static final TAG:Ljava/lang/String; = "SimManagementExt"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public updateSimManagementPref(Landroid/preference/PreferenceGroup;)V
    .locals 3
    .param p1    # Landroid/preference/PreferenceGroup;

    const-string v1, "SimManagementExt"

    const-string v2, "SimManagementExt---updateSimManagementPref()"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "3g_service_settings"

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    :cond_0
    if-eqz v0, :cond_1

    const-string v1, "SimManagementExt"

    const-string v2, "FeatureOption.MTK_GEMINI_3G_SWITCH=false"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    return-void
.end method
