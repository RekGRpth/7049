.class public Lcom/mediatek/systemui/plugin/Op02StatusBarPlugin;
.super Lcom/mediatek/systemui/ext/DefaultStatusBarPlugin;
.source "Op02StatusBarPlugin.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/mediatek/systemui/ext/DefaultStatusBarPlugin;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public get3gDisabledWarningString()Ljava/lang/String;
    .locals 1

    const v0, 0x7f05000c

    invoke-virtual {p0, v0}, Lcom/mediatek/systemui/plugin/Op02StatusBarPlugin;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDataActivityIconList()[I
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getDataNetworkTypeIconGemini(Lcom/mediatek/systemui/ext/NetworkType;I)I
    .locals 2
    .param p1    # Lcom/mediatek/systemui/ext/NetworkType;
    .param p2    # I

    invoke-virtual {p1}, Lcom/mediatek/systemui/ext/NetworkType;->getTypeId()I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    sget-object v1, Lcom/mediatek/systemui/plugin/TelephonyIcons;->NETWORK_TYPE:[[I

    aget-object v1, v1, v0

    aget v1, v1, p2

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getDataTypeIconListGemini(ZLcom/mediatek/systemui/ext/DataType;)[I
    .locals 3
    .param p1    # Z
    .param p2    # Lcom/mediatek/systemui/ext/DataType;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    sget-object v1, Lcom/mediatek/systemui/plugin/TelephonyIcons;->DATA_ROAM:[[I

    invoke-virtual {p2}, Lcom/mediatek/systemui/ext/DataType;->getTypeId()I

    move-result v2

    aget-object v0, v1, v2

    :cond_0
    return-object v0
.end method

.method public getPluginResources()Landroid/content/res/Resources;
    .locals 1

    invoke-virtual {p0}, Lcom/mediatek/systemui/plugin/Op02StatusBarPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public getSignalIndicatorIconGemini(I)I
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    const v0, 0x7f020027

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    const v0, 0x7f020028

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getSignalStrengthDescription(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSignalStrengthIcon(ZII)I
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # I

    const/4 v0, -0x1

    return v0
.end method

.method public getSignalStrengthIconGemini(II)I
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, -0x1

    return v0
.end method

.method public getSignalStrengthIconGemini(III)I
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, -0x1

    return v0
.end method

.method public getSignalStrengthNullIconGemini(I)I
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    const v0, 0x7f020029

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    const v0, 0x7f02002a

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getSignalStrengthSearchingIconGemini(I)I
    .locals 1
    .param p1    # I

    const/4 v0, -0x1

    return v0
.end method

.method public isHspaDataDistinguishable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public supportDataTypeAlwaysDisplayWhileOn()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public supportDisableWifiAtAirplaneMode()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
