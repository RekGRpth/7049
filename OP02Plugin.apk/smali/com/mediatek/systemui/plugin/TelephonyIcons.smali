.class final Lcom/mediatek/systemui/plugin/TelephonyIcons;
.super Ljava/lang/Object;
.source "TelephonyIcons.java"


# static fields
.field static final DATA_1X_ROAM:[I

.field static final DATA_3G_ROAM:[I

.field static final DATA_4G_ROAM:[I

.field static final DATA_E_ROAM:[I

.field static final DATA_G_ROAM:[I

.field static final DATA_H_ROAM:[I

.field static final DATA_ROAM:[[I

.field static final NETWORK_TYPE:[[I

.field static final NETWORK_TYPE_3G:[I

.field static final NETWORK_TYPE_G:[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x4

    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/systemui/plugin/TelephonyIcons;->DATA_1X_ROAM:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/mediatek/systemui/plugin/TelephonyIcons;->DATA_3G_ROAM:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/mediatek/systemui/plugin/TelephonyIcons;->DATA_4G_ROAM:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/mediatek/systemui/plugin/TelephonyIcons;->DATA_E_ROAM:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/mediatek/systemui/plugin/TelephonyIcons;->DATA_G_ROAM:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/mediatek/systemui/plugin/TelephonyIcons;->DATA_H_ROAM:[I

    const/4 v0, 0x6

    new-array v0, v0, [[I

    sget-object v1, Lcom/mediatek/systemui/plugin/TelephonyIcons;->DATA_1X_ROAM:[I

    aput-object v1, v0, v4

    sget-object v1, Lcom/mediatek/systemui/plugin/TelephonyIcons;->DATA_3G_ROAM:[I

    aput-object v1, v0, v5

    sget-object v1, Lcom/mediatek/systemui/plugin/TelephonyIcons;->DATA_4G_ROAM:[I

    aput-object v1, v0, v6

    const/4 v1, 0x3

    sget-object v2, Lcom/mediatek/systemui/plugin/TelephonyIcons;->DATA_E_ROAM:[I

    aput-object v2, v0, v1

    sget-object v1, Lcom/mediatek/systemui/plugin/TelephonyIcons;->DATA_G_ROAM:[I

    aput-object v1, v0, v3

    const/4 v1, 0x5

    sget-object v2, Lcom/mediatek/systemui/plugin/TelephonyIcons;->DATA_H_ROAM:[I

    aput-object v2, v0, v1

    sput-object v0, Lcom/mediatek/systemui/plugin/TelephonyIcons;->DATA_ROAM:[[I

    new-array v0, v3, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/mediatek/systemui/plugin/TelephonyIcons;->NETWORK_TYPE_G:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/mediatek/systemui/plugin/TelephonyIcons;->NETWORK_TYPE_3G:[I

    new-array v0, v6, [[I

    sget-object v1, Lcom/mediatek/systemui/plugin/TelephonyIcons;->NETWORK_TYPE_G:[I

    aput-object v1, v0, v4

    sget-object v1, Lcom/mediatek/systemui/plugin/TelephonyIcons;->NETWORK_TYPE_3G:[I

    aput-object v1, v0, v5

    sput-object v0, Lcom/mediatek/systemui/plugin/TelephonyIcons;->NETWORK_TYPE:[[I

    return-void

    :array_0
    .array-data 4
        0x7f020007
        0x7f020009
        0x7f020008
        0x7f02000a
    .end array-data

    :array_1
    .array-data 4
        0x7f02000b
        0x7f02000d
        0x7f02000c
        0x7f02000e
    .end array-data

    :array_2
    .array-data 4
        0x7f02000f
        0x7f020011
        0x7f020010
        0x7f020012
    .end array-data

    :array_3
    .array-data 4
        0x7f020013
        0x7f020015
        0x7f020014
        0x7f020016
    .end array-data

    :array_4
    .array-data 4
        0x7f020017
        0x7f020019
        0x7f020018
        0x7f02001a
    .end array-data

    :array_5
    .array-data 4
        0x7f02001b
        0x7f02001d
        0x7f02001c
        0x7f02001e
    .end array-data

    :array_6
    .array-data 4
        0x7f020023
        0x7f020025
        0x7f020024
        0x7f020026
    .end array-data

    :array_7
    .array-data 4
        0x7f02001f
        0x7f020021
        0x7f020020
        0x7f020022
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
