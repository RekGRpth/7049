.class public final Lcom/mediatek/omacp/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/omacp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final agps_app_name:I = 0x7f050020

.field public static final apn_apn_label:I = 0x7f05003a

.field public static final apn_app_name:I = 0x7f05001a

.field public static final apn_auth_type_label:I = 0x7f05003d

.field public static final apn_mms_port_label:I = 0x7f05003c

.field public static final apn_mms_proxy_label:I = 0x7f05003b

.field public static final apn_type_label:I = 0x7f05003e

.field public static final application_label:I = 0x7f050014

.field public static final auth_data_label:I = 0x7f050052

.field public static final auth_level_label:I = 0x7f050050

.field public static final auth_type_label:I = 0x7f050051

.field public static final bookmark_folder_label:I = 0x7f050041

.field public static final bookmark_label:I = 0x7f05002c

.field public static final bookmark_name_label:I = 0x7f050044

.field public static final browser_app_name:I = 0x7f050019

.field public static final configuration_message:I = 0x7f050000

.field public static final confirm_delete_all_messages:I = 0x7f05000f

.field public static final confirm_delete_message:I = 0x7f050010

.field public static final content_type_label:I = 0x7f05005b

.field public static final custom_install_text:I = 0x7f050012

.field public static final delete:I = 0x7f050004

.field public static final delete_all:I = 0x7f050006

.field public static final detail_activity_label:I = 0x7f05002b

.field public static final detail_invalid_setting_error_msg:I = 0x7f050061

.field public static final dm_app_name:I = 0x7f05001f

.field public static final dm_init_label:I = 0x7f050053

.field public static final ds_app_name:I = 0x7f050022

.field public static final ds_cliuri_label:I = 0x7f05005c

.field public static final ds_database_name_label:I = 0x7f050059

.field public static final ds_database_url_label:I = 0x7f05005a

.field public static final ds_sync_type_label:I = 0x7f05005d

.field public static final email_account_label:I = 0x7f050033

.field public static final email_app_name:I = 0x7f05001b

.field public static final email_from_label:I = 0x7f050056

.field public static final email_imap4_app_name:I = 0x7f05001e

.field public static final email_inbound_label:I = 0x7f050032

.field public static final email_need_sign_label:I = 0x7f050034

.field public static final email_need_sign_no:I = 0x7f050036

.field public static final email_need_sign_yes:I = 0x7f050035

.field public static final email_outbound_label:I = 0x7f050031

.field public static final email_pop3_app_name:I = 0x7f05001d

.field public static final email_rt_address_label:I = 0x7f050057

.field public static final email_secure_label:I = 0x7f050055

.field public static final email_setting_name_label:I = 0x7f050054

.field public static final email_smtp_app_name:I = 0x7f05001c

.field public static final error:I = 0x7f050062

.field public static final from_label:I = 0x7f05000d

.field public static final full_install_text:I = 0x7f050011

.field public static final homepage_label:I = 0x7f05002d

.field public static final imps_app_name:I = 0x7f050023

.field public static final imps_cid_prefix_label:I = 0x7f05005f

.field public static final imps_services_label:I = 0x7f05005e

.field public static final info_unavaliable:I = 0x7f050040

.field public static final info_unsupport:I = 0x7f05003f

.field public static final install_text:I = 0x7f050013

.field public static final install_timeout_error_msg:I = 0x7f050027

.field public static final installation_report:I = 0x7f050024

.field public static final installed_indicator:I = 0x7f05000b

.field public static final installing_progress_message:I = 0x7f050026

.field public static final message_details_title:I = 0x7f05000a

.field public static final message_options:I = 0x7f050007

.field public static final message_type_label:I = 0x7f05000c

.field public static final mms_app_name:I = 0x7f050018

.field public static final mms_cm_label:I = 0x7f050048

.field public static final mms_ma_label:I = 0x7f05004c

.field public static final mms_ms_label:I = 0x7f05004a

.field public static final mms_pc_addr_label:I = 0x7f05004b

.field public static final mms_rm_label:I = 0x7f050049

.field public static final mmsc_label:I = 0x7f05002e

.field public static final mmsc_name_label:I = 0x7f050047

.field public static final name_label:I = 0x7f050060

.field public static final nap_label:I = 0x7f050043

.field public static final no:I = 0x7f050009

.field public static final notification_multiple:I = 0x7f050001

.field public static final notification_multiple_title:I = 0x7f050002

.field public static final password_label:I = 0x7f050046

.field public static final port_number_label:I = 0x7f050030

.field public static final proxy_label:I = 0x7f050042

.field public static final re_install_notify_message:I = 0x7f050028

.field public static final re_install_notify_title:I = 0x7f050029

.field public static final received_label:I = 0x7f05000e

.field public static final refreshing:I = 0x7f050008

.field public static final result_failed:I = 0x7f050016

.field public static final result_success:I = 0x7f050015

.field public static final rtsp_app_name:I = 0x7f050021

.field public static final rtsp_max_bandwidth_label:I = 0x7f050058

.field public static final rtsp_max_udp_port_label:I = 0x7f050038

.field public static final rtsp_min_udp_port_label:I = 0x7f050039

.field public static final rtsp_netinfo_label:I = 0x7f050037

.field public static final server_addr_type_label:I = 0x7f05004f

.field public static final server_address_label:I = 0x7f05002f

.field public static final server_id_label:I = 0x7f05004d

.field public static final server_name_label:I = 0x7f05004e

.field public static final unknown:I = 0x7f050017

.field public static final unlock_pin_dialog_title:I = 0x7f05002a

.field public static final user_name_label:I = 0x7f050045

.field public static final view:I = 0x7f050003

.field public static final view_message_detail:I = 0x7f050005

.field public static final yes:I = 0x7f050025


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
