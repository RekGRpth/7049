.class Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$17;
.super Landroid/os/Handler;
.source "OmacpMessageSettingsDetail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;


# direct methods
.method constructor <init>(Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$17;->this$0:Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    const/4 v4, -0x1

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    const-string v2, "Omacp/OmacpMessageSettingsDetail"

    const-string v3, "OmacpMessageSettingsDetail no proper event type."

    invoke-static {v2, v3}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    const-string v2, "Omacp/OmacpMessageSettingsDetail"

    const-string v3, "OmacpMessageSettingsDetail application install time out......"

    invoke-static {v2, v3}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$17;->this$0:Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;

    invoke-static {v2}, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;->access$1400(Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$17;->this$0:Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;

    invoke-static {v2}, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;->access$1400(Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$ResultType;

    iget v2, v2, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$ResultType;->mResult:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$17;->this$0:Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;

    invoke-static {v2}, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;->access$1400(Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$ResultType;

    iput v4, v2, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$ResultType;->mResult:I

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$17;->this$0:Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;

    invoke-static {v2}, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;->access$1900(Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;)V

    goto :goto_0

    :pswitch_1
    const-string v2, "Omacp/OmacpMessageSettingsDetail"

    const-string v3, "OmacpMessageSettingsDetail apn install time out......"

    invoke-static {v2, v3}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$17;->this$0:Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;

    invoke-static {v2}, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;->access$1100(Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;)Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$ResultType;

    move-result-object v2

    iget v2, v2, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$ResultType;->mResult:I

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$17;->this$0:Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;

    invoke-static {v2}, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;->access$1100(Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;)Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$ResultType;

    move-result-object v2

    iput v4, v2, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$ResultType;->mResult:I

    :cond_2
    iget-object v2, p0, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$17;->this$0:Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;

    invoke-static {v2}, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;->access$1700(Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;)V

    goto :goto_0

    :pswitch_2
    const-string v2, "Omacp/OmacpMessageSettingsDetail"

    const-string v3, "OmacpMessageSettingsDetail apn switch time out......"

    invoke-static {v2, v3}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail$17;->this$0:Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;

    invoke-static {v2}, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;->access$1700(Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7d0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
