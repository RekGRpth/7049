.class public Lcom/mediatek/omacp/message/OmacpMessageUtils;
.super Ljava/lang/Object;
.source "OmacpMessageUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/omacp/message/OmacpMessageUtils$1;,
        Lcom/mediatek/omacp/message/OmacpMessageUtils$ApnProxyParam;
    }
.end annotation


# static fields
.field static final APN_APPID:Ljava/lang/String; = "apn"

.field static final BROWSER_APPID:Ljava/lang/String; = "w2"

.field private static final DEBUG:Z = true

.field static final DM_APPID:Ljava/lang/String; = "w7"

.field static final DS_APID:Ljava/lang/String; = "w5"

.field static final IMAP4_APPID:Ljava/lang/String; = "143"

.field static final IMPS_APPID:Ljava/lang/String; = "wA"

.field static final MMS_2_APPID:Ljava/lang/String; = "ap0005"

.field static final MMS_APPID:Ljava/lang/String; = "w4"

.field static final POP3_APPID:Ljava/lang/String; = "110"

.field static final RTSP_APPID:Ljava/lang/String; = "554"

.field static final SMTP_APPID:Ljava/lang/String; = "25"

.field static final SUPL_APPID:Ljava/lang/String; = "ap0004"

.field private static final TAG:Ljava/lang/String; = "Omacp/OmacpMessageUtils"

.field static sEmailAccountName:Ljava/lang/String;

.field static sEmailInboundType:Ljava/lang/String;

.field static sInboundEmailSetting:Ljava/lang/String;

.field static sOutboundEmailSetting:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailAccountName:Ljava/lang/String;

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sInboundEmailSetting:Ljava/lang/String;

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailInboundType:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/text/SpannableStringBuilder;
    .param p2    # Ljava/lang/String;

    const/16 v3, 0x11

    if-nez p1, :cond_0

    const-string v1, "Omacp/OmacpMessageUtils"

    const-string v2, "OmacpMessageUtils addApplicationLabel info is null."

    invoke-static {v1, v2}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const v1, 0x7f050014

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-static {p0, p2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getAppName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v2, -0xff01

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method private static appendApplicationParams(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Landroid/text/SpannableStringBuilder;Ljava/lang/StringBuilder;)V
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .param p2    # Landroid/text/SpannableStringBuilder;
    .param p3    # Ljava/lang/StringBuilder;

    iget-object v6, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v7, "w2"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {p0, p1}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getBrowserSettingInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendSpanableDetailInfo(Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;)V

    :goto_0
    return-void

    :cond_0
    iget-object v6, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v7, "w4"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v7, "ap0005"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_1
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-nez v6, :cond_2

    invoke-static {p0, p1, p3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getMmsSettingsInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V

    goto :goto_0

    :cond_2
    iget-object v6, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v7, "w7"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-static {p0, p1}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getDMSettingInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendSpanableDetailInfo(Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;)V

    goto :goto_0

    :cond_3
    iget-object v6, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v7, "25"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v7, "110"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v7, "143"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    :cond_4
    invoke-static {p0, p1}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getEmailSetting(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)V

    goto :goto_0

    :cond_5
    iget-object v6, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v7, "554"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-static {p0, p1}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getRtspSettingInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-static {v4, p2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendSpanableDetailInfo(Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;)V

    goto :goto_0

    :cond_6
    iget-object v6, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v7, "ap0004"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-static {p0, p1}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getSuplSettingInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    invoke-static {v5, p2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendSpanableDetailInfo(Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;)V

    goto :goto_0

    :cond_7
    iget-object v6, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v7, "w5"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-static {p0, p1}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getDsSettingInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendSpanableDetailInfo(Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;)V

    goto/16 :goto_0

    :cond_8
    iget-object v6, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v7, "wA"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-static {p0, p1}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getImpsSettingInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-static {v3, p2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendSpanableDetailInfo(Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;)V

    goto/16 :goto_0

    :cond_9
    const-string v6, "Omacp/OmacpMessageUtils"

    const-string v7, "OmacpMessageUtils getSettingsDetailInfo appid unknown."

    invoke-static {v6, v7}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static appendDmAddrSettingsInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .param p2    # Ljava/lang/StringBuilder;

    const v1, 0x7f05002f

    const/4 v3, 0x0

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddr:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmServerAddress:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddr:Ljava/lang/String;

    invoke-static {p0, v1, v2, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    :goto_0
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddrtype:Ljava/lang/String;

    if-eqz v0, :cond_1

    const v0, 0x7f05004f

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmAddrType:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddrtype:Ljava/lang/String;

    invoke-static {p0, v1, v2, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mPortnbr:Ljava/lang/String;

    if-eqz v0, :cond_2

    const v0, 0x7f050030

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmPortNumber:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mPortnbr:Ljava/lang/String;

    invoke-static {p0, v1, v2, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    return-void

    :cond_3
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmServerAddress:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v1, v2, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method private static appendDmAuthSettingsInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .param p2    # Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iget-object v1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthlevel:Ljava/lang/String;

    if-eqz v1, :cond_0

    const v1, 0x7f050050

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmAuthLevel:Z

    iget-object v3, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthlevel:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthtype:Ljava/lang/String;

    if-eqz v1, :cond_1

    const v1, 0x7f050051

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmAuthType:Z

    iget-object v3, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthtype:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthname:Ljava/lang/String;

    if-eqz v1, :cond_2

    const v1, 0x7f050045

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmAuthName:Z

    iget-object v3, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthname:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthsecret:Ljava/lang/String;

    if-eqz v1, :cond_3

    const v1, 0x7f050046

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmAuthSecret:Z

    iget-object v3, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthsecret:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthdata:Ljava/lang/String;

    if-eqz v1, :cond_4

    const v1, 0x7f050052

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmAuthData:Z

    iget-object v3, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthdata:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    return-void
.end method

.method private static appendDmIsSupport(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .param p2    # Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mInit:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "\n"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v0, 0x7f050053

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmInit:Z

    if-nez v0, :cond_1

    const v0, 0x7f05003f

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mInit:Ljava/lang/String;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f050025

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    const v0, 0x7f050009

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static appendDmSettingsInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .param p2    # Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const v0, 0x7f05004d

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmProviderId:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    if-eqz v0, :cond_1

    const v0, 0x7f05004e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDmServerName:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-static {p0, p1, p2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendDmAddrSettingsInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0, p1, p2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendDmAuthSettingsInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V

    :cond_2
    return-void
.end method

.method private static appendEmailInfo(Landroid/content/Context;Landroid/text/SpannableStringBuilder;)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/text/SpannableStringBuilder;

    const v5, 0x7f05003f

    const/4 v4, 0x1

    const/16 v3, 0x11

    sget-object v1, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    if-nez v1, :cond_0

    sget-object v1, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sInboundEmailSetting:Ljava/lang/String;

    if-eqz v1, :cond_3

    :cond_0
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    const-string v1, "\n"

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_1
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const v1, 0x7f050014

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const v1, 0x7f05001b

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v2, -0xff01

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v1, Landroid/text/style/StyleSpan;

    invoke-direct {v1, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmail:Z

    if-nez v1, :cond_4

    const-string v1, "\n"

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    :goto_0
    invoke-static {}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->resetEmailStaticSetting()V

    :cond_3
    return-void

    :cond_4
    sget-object v1, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailAccountName:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, "\n"

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const v1, 0x7f050033

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailProviderId:Z

    if-nez v1, :cond_7

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_5
    :goto_1
    sget-object v1, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, "\n"

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const v1, 0x7f05001c

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/StyleSpan;

    invoke-direct {v1, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    sget-object v1, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_6
    sget-object v1, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sInboundEmailSetting:Ljava/lang/String;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailInboundType:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, "\n"

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    sget-object v1, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailInboundType:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/StyleSpan;

    invoke-direct {v1, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    sget-object v1, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sInboundEmailSetting:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    :cond_7
    sget-object v1, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailAccountName:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1
.end method

.method private static appendEmailOutBoundInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;Z)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .param p2    # Ljava/lang/StringBuilder;
    .param p3    # Z

    if-eqz p3, :cond_1

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mFrom:Ljava/lang/String;

    if-eqz v0, :cond_0

    const v0, 0x7f050056

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailFrom:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mFrom:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mRtAddr:Ljava/lang/String;

    if-eqz v0, :cond_1

    const v0, 0x7f050057

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailRtAddr:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mRtAddr:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    return-void
.end method

.method private static appendEmailServerInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;Z)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .param p2    # Ljava/lang/StringBuilder;
    .param p3    # Z

    const v1, 0x7f05002f

    const/4 v6, 0x0

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddr:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailOutboundAddr:Z

    sget-boolean v3, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailInboundAddr:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v5, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddr:Ljava/lang/String;

    move-object v0, p0

    move v4, p3

    invoke-static/range {v0 .. v5}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getEmailElement(Landroid/content/Context;Ljava/lang/String;ZZZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    :goto_0
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mPortnbr:Ljava/lang/String;

    if-eqz v0, :cond_1

    const v0, 0x7f050030

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailOutboundPortNumber:Z

    sget-boolean v3, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailInboundPortNumber:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v5, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mPortnbr:Ljava/lang/String;

    move-object v0, p0

    move v4, p3

    invoke-static/range {v0 .. v5}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getEmailElement(Landroid/content/Context;Ljava/lang/String;ZZZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mService:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mService:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    const v0, 0x7f050055

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailOutboundSecure:Z

    sget-boolean v3, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailInboundSecure:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mService:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object v0, p0

    move v4, p3

    invoke-static/range {v0 .. v5}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getEmailElement(Landroid/content/Context;Ljava/lang/String;ZZZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    return-void

    :cond_3
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailOutboundAddr:Z

    sget-boolean v3, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailInboundAddr:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object v0, p0

    move v4, p3

    invoke-static/range {v0 .. v5}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getEmailElement(Landroid/content/Context;Ljava/lang/String;ZZZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method private static appendEmailUserInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;Z)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .param p2    # Ljava/lang/StringBuilder;
    .param p3    # Z

    const/4 v6, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthname:Ljava/lang/String;

    if-eqz v0, :cond_0

    const v0, 0x7f050045

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailOutboundUserName:Z

    sget-boolean v3, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailInboundUserName:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iget-object v5, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthname:Ljava/lang/String;

    move-object v0, p0

    move v4, p3

    invoke-static/range {v0 .. v5}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getEmailElement(Landroid/content/Context;Ljava/lang/String;ZZZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthsecret:Ljava/lang/String;

    if-eqz v0, :cond_0

    const v0, 0x7f050046

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailOutboundPassword:Z

    sget-boolean v3, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailInboundPassword:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iget-object v5, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthsecret:Ljava/lang/String;

    move-object v0, p0

    move v4, p3

    invoke-static/range {v0 .. v5}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getEmailElement(Landroid/content/Context;Ljava/lang/String;ZZZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static appendImpsServerAuthInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .param p2    # Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iget-object v1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthlevel:Ljava/lang/String;

    if-eqz v1, :cond_0

    const v1, 0x7f050050

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsAuthLevel:Z

    iget-object v3, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthlevel:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthname:Ljava/lang/String;

    if-eqz v1, :cond_1

    const v1, 0x7f050045

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsAuthName:Z

    iget-object v3, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthname:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthsecret:Ljava/lang/String;

    if-eqz v1, :cond_2

    const v1, 0x7f050046

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsAuthSecret:Z

    iget-object v3, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthsecret:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    return-void
.end method

.method private static appendImpsServerBaseInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .param p2    # Ljava/lang/StringBuilder;

    const v4, 0x7f05002f

    const/4 v3, 0x0

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const v0, 0x7f05004d

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsProviderId:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    if-eqz v0, :cond_1

    const v0, 0x7f05004e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsServerName:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAaccept:Ljava/lang/String;

    if-eqz v0, :cond_2

    const v0, 0x7f05005b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsContentType:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAaccept:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddr:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsServerAddress:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddr:Ljava/lang/String;

    invoke-static {p0, v1, v2, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    :goto_0
    return-void

    :cond_4
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsServerAddress:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v1, v2, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static appendImpsServerInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .param p2    # Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-static {p0, p1, p2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendImpsServerBaseInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddrtype:Ljava/lang/String;

    if-eqz v0, :cond_0

    const v0, 0x7f05004f

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsAddressType:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddrtype:Ljava/lang/String;

    invoke-static {p0, v1, v2, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendImpsServerAuthInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mServices:Ljava/lang/String;

    if-eqz v0, :cond_1

    const v0, 0x7f05005e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsServices:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mServices:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mCidprefix:Ljava/lang/String;

    if-eqz v0, :cond_2

    const v0, 0x7f05005f

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImpsClientIdPrefix:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mCidprefix:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    return-void
.end method

.method private static appendMmsInfo(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/StringBuilder;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/text/SpannableStringBuilder;
    .param p2    # Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    const-string v0, "w4"

    invoke-static {p0, p1, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    sget-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMms:Z

    if-nez v0, :cond_2

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const v0, 0x7f05003f

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method private static appendMmscProxyParams(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/StringBuilder;Lcom/mediatek/omacp/message/OmacpMessageUtils$ApnProxyParam;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p2    # Ljava/lang/StringBuilder;
    .param p3    # Lcom/mediatek/omacp/message/OmacpMessageUtils$ApnProxyParam;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass;",
            ">;",
            "Ljava/lang/StringBuilder;",
            "Lcom/mediatek/omacp/message/OmacpMessageUtils$ApnProxyParam;",
            ")V"
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v4, v4, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v5, "w4"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v4, v4, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v5, "ap0005"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v4, v4, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v4, v4, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v4, v4, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v4, v4, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v4, v4, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v4, v4, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddr:Ljava/lang/String;

    if-eqz v4, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v4, v4, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v4, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddr:Ljava/lang/String;

    goto :goto_1

    :cond_3
    if-eqz v0, :cond_5

    const-string v4, "\n"

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v4, 0x7f05002e

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p3, Lcom/mediatek/omacp/message/OmacpMessageUtils$ApnProxyParam;->mProxy:Ljava/lang/String;

    iget-object v2, p3, Lcom/mediatek/omacp/message/OmacpMessageUtils$ApnProxyParam;->mPort:Ljava/lang/String;

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_4

    const-string v4, "\n"

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v4, 0x7f05003b

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_5

    const-string v4, "\n"

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v4, 0x7f05003c

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    return-void
.end method

.method private static appendSpanableDetailInfo(Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;)V
    .locals 1
    .param p0    # Landroid/text/SpannableStringBuilder;
    .param p1    # Landroid/text/SpannableStringBuilder;

    if-eqz p0, :cond_1

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_0
    invoke-virtual {p1, p0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_1
    return-void
.end method

.method private static checkPxphysical(Lcom/mediatek/omacp/parser/ProxyClass;Lcom/mediatek/omacp/parser/NapdefClass;)Z
    .locals 5
    .param p0    # Lcom/mediatek/omacp/parser/ProxyClass;
    .param p1    # Lcom/mediatek/omacp/parser/NapdefClass;

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v2, v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mToNapid:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_0

    move v3, v4

    :goto_0
    return v3

    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, p1, Lcom/mediatek/omacp/parser/NapdefClass;->mNapid:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_0
.end method

.method private static getAPNBaseParams(Landroid/content/Context;Lcom/mediatek/omacp/parser/NapdefClass;Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/NapdefClass;
    .param p2    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/mediatek/omacp/parser/NapdefClass;",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x0

    iget-object v0, p1, Lcom/mediatek/omacp/parser/NapdefClass;->mName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "\n"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v0, 0x7f050060

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/mediatek/omacp/parser/NapdefClass;->mName:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p1, Lcom/mediatek/omacp/parser/NapdefClass;->mNapaddress:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "\n"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v0, 0x7f05003a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/mediatek/omacp/parser/NapdefClass;->mNapaddress:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v0, p1, Lcom/mediatek/omacp/parser/NapdefClass;->mNapauthinfo:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/mediatek/omacp/parser/NapdefClass;->mNapauthinfo:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;->mAuthname:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string v0, "\n"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v0, 0x7f050045

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/mediatek/omacp/parser/NapdefClass;->mNapauthinfo:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;->mAuthname:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v0, p1, Lcom/mediatek/omacp/parser/NapdefClass;->mNapauthinfo:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/mediatek/omacp/parser/NapdefClass;->mNapauthinfo:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;->mAuthsecret:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v0, "\n"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v0, 0x7f050046

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/mediatek/omacp/parser/NapdefClass;->mNapauthinfo:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;->mAuthsecret:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    return-void
.end method

.method private static getAPNProxyParams(Landroid/content/Context;Ljava/util/ArrayList;Lcom/mediatek/omacp/parser/NapdefClass;Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/omacp/parser/NapdefClass;
    .param p3    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ProxyClass;",
            ">;",
            "Lcom/mediatek/omacp/parser/NapdefClass;",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/mediatek/omacp/message/OmacpMessageUtils$ApnProxyParam;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/mediatek/omacp/message/OmacpMessageUtils$ApnProxyParam;-><init>(Lcom/mediatek/omacp/message/OmacpMessageUtils$1;)V

    invoke-static {p0, p1, p2, p3, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getProxyClassParams(Landroid/content/Context;Ljava/util/ArrayList;Lcom/mediatek/omacp/parser/NapdefClass;Ljava/lang/StringBuilder;Lcom/mediatek/omacp/message/OmacpMessageUtils$ApnProxyParam;)V

    invoke-static {p0, p4, p3, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendMmscProxyParams(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/StringBuilder;Lcom/mediatek/omacp/message/OmacpMessageUtils$ApnProxyParam;)V

    return-void
.end method

.method public static getAPNType(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_8

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_8

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v2, v2, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v3, "w2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v0, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",default"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "default"

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v2, v2, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v3, "w4"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v2, v2, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v3, "ap0005"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_3
    if-eqz v0, :cond_4

    const-string v2, "mms"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    if-eqz v0, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",mms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    const-string v0, "mms"

    goto :goto_1

    :cond_6
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v2, v2, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v3, "ap0004"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",supl"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_7
    const-string v0, "supl"

    goto :goto_1

    :cond_8
    return-object v0
.end method

.method private static getAPNTypeAndAuthParams(Landroid/content/Context;Lcom/mediatek/omacp/parser/NapdefClass;Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/NapdefClass;
    .param p2    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/mediatek/omacp/parser/NapdefClass;",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass;",
            ">;)V"
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v1, p1, Lcom/mediatek/omacp/parser/NapdefClass;->mNapauthinfo:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/mediatek/omacp/parser/NapdefClass;->mNapauthinfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;->mAuthtype:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "\n"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v1, 0x7f05003d

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/mediatek/omacp/parser/NapdefClass;->mNapauthinfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;->mAuthtype:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getAPNType(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "\n"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v1, 0x7f05003e

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    return-void
.end method

.method private static getApnSettingInfo(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)Landroid/text/SpannableStringBuilder;
    .locals 7
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/NapdefClass;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ProxyClass;",
            ">;)",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    const-string v5, "Omacp/OmacpMessageUtils"

    const-string v6, "OmacpMessageUtils getApnSettingInfo napList is null or size is 0."

    invoke-static {v5, v6}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_1
    return-object v0

    :cond_2
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/omacp/parser/NapdefClass;

    invoke-static {p0, v4, v2, p2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getAPNBaseParams(Landroid/content/Context;Lcom/mediatek/omacp/parser/NapdefClass;Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V

    invoke-static {p0, p3, v4, v2, p2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getAPNProxyParams(Landroid/content/Context;Ljava/util/ArrayList;Lcom/mediatek/omacp/parser/NapdefClass;Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V

    invoke-static {p0, v4, v2, p2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getAPNTypeAndAuthParams(Landroid/content/Context;Lcom/mediatek/omacp/parser/NapdefClass;Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V

    const-string v5, ""

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    if-lez v1, :cond_3

    const-string v5, "\n"

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_3
    const-string v5, "apn"

    invoke-static {p0, v0, v5}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static getAppName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const v3, 0x7f050018

    const v2, 0x7f05001b

    const/4 v0, 0x0

    const-string v1, "w4"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "ap0005"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v1, "w2"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f050019

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v1, "apn"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x7f05001a

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v1, "143"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    const-string v1, "110"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const-string v1, "25"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    const-string v1, "w7"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const v1, 0x7f05001f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_7
    const-string v1, "ap0004"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    const v1, 0x7f050020

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_8
    const-string v1, "554"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    const v1, 0x7f050021

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    const-string v1, "w5"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    const v1, 0x7f050022

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_a
    const-string v1, "wA"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    const v1, 0x7f050023

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_b
    const-string v1, "Omacp/OmacpMessageUtils"

    const-string v2, "OmacpMessageUtils getAppName unknown app."

    invoke-static {v1, v2}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static getBrowserSettingInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)Landroid/text/SpannableStringBuilder;
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;

    const v9, 0x7f05003f

    if-nez p1, :cond_0

    const-string v6, "Omacp/OmacpMessageUtils"

    const-string v7, "OmacpMessageUtils getBrowserSettingInfo application is null."

    invoke-static {v6, v7}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-boolean v6, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowser:Z

    if-nez v6, :cond_1

    iget-object v6, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    invoke-static {p0, v0, v6}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    const-string v6, "\n"

    invoke-virtual {v0, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    :cond_1
    iget-object v6, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    if-eqz v6, :cond_2

    const v6, 0x7f050041

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-boolean v7, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowserBookMarkFolder:Z

    iget-object v8, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    invoke-static {p0, v6, v7, v8}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v5, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mResource:Ljava/util/ArrayList;

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_7

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iget-object v6, v4, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mUri:Ljava/lang/String;

    if-eqz v6, :cond_6

    iget-object v6, v4, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mName:Ljava/lang/String;

    if-eqz v6, :cond_3

    const v6, 0x7f050044

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-boolean v7, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowserBookMarkName:Z

    iget-object v8, v4, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mName:Ljava/lang/String;

    invoke-static {p0, v6, v7, v8}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const v6, 0x7f05002c

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-boolean v7, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowserBookMark:Z

    iget-object v8, v4, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mUri:Ljava/lang/String;

    invoke-static {p0, v6, v7, v8}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v4, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthname:Ljava/lang/String;

    if-eqz v6, :cond_4

    const v6, 0x7f050045

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-boolean v7, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowserUserName:Z

    iget-object v8, v4, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthname:Ljava/lang/String;

    invoke-static {p0, v6, v7, v8}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    iget-object v6, v4, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthsecret:Ljava/lang/String;

    if-eqz v6, :cond_5

    const v6, 0x7f050046

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-boolean v7, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowserPassWord:Z

    iget-object v8, v4, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthsecret:Ljava/lang/String;

    invoke-static {p0, v6, v7, v8}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    iget-object v6, v4, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mStartpage:Ljava/lang/String;

    if-eqz v6, :cond_6

    iget-object v6, v4, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mStartpage:Ljava/lang/String;

    const-string v7, "1"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    if-nez v2, :cond_6

    iget-object v2, v4, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mUri:Ljava/lang/String;

    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_7
    if-eqz v2, :cond_8

    const-string v6, "\n"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v6, 0x7f05002d

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v6, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sBrowserHomePage:Z

    if-nez v6, :cond_9

    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    :goto_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_a

    iget-object v6, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    invoke-static {p0, v0, v6}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_a
    iget-object v6, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    invoke-static {p0, v0, v6}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    const v6, 0x7f050040

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0
.end method

.method private static getDMSettingInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)Landroid/text/SpannableStringBuilder;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;

    if-nez p1, :cond_1

    const-string v2, "Omacp/OmacpMessageUtils"

    const-string v3, "OmacpMessageUtils getDMSettingInfo application is null."

    invoke-static {v2, v3}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDm:Z

    if-nez v2, :cond_2

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    invoke-static {p0, v0, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const v2, 0x7f05003f

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    :cond_2
    invoke-static {p0, p1, v1}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendDmSettingsInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V

    invoke-static {p0, p1, v1}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendDmIsSupport(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    invoke-static {p0, v0, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method private static getDsAuthInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .param p2    # Ljava/lang/StringBuilder;

    iget-object v1, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    iget-object v1, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iget-object v1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthlevel:Ljava/lang/String;

    if-eqz v1, :cond_0

    const v1, 0x7f050050

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsAuthLevel:Z

    iget-object v3, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthlevel:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthtype:Ljava/lang/String;

    if-eqz v1, :cond_1

    const v1, 0x7f050051

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsAuthType:Z

    iget-object v3, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthtype:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthname:Ljava/lang/String;

    if-eqz v1, :cond_2

    const v1, 0x7f050045

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsAuthName:Z

    iget-object v3, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthname:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthsecret:Ljava/lang/String;

    if-eqz v1, :cond_3

    const v1, 0x7f050046

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsAuthSecret:Z

    iget-object v3, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthsecret:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthdata:Ljava/lang/String;

    if-eqz v1, :cond_4

    const v1, 0x7f050052

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsAuthData:Z

    iget-object v3, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthdata:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    return-void
.end method

.method private static getDsResourceInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .param p2    # Ljava/lang/StringBuilder;

    iget-object v3, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mResource:Ljava/util/ArrayList;

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_8

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mUri:Ljava/lang/String;

    if-eqz v4, :cond_7

    iget-object v4, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mName:Ljava/lang/String;

    if-eqz v4, :cond_0

    const v4, 0x7f050059

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-boolean v5, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsDatabaseName:Z

    iget-object v6, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mName:Ljava/lang/String;

    invoke-static {p0, v4, v5, v6}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const v4, 0x7f05005a

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-boolean v5, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsDatabaseUrl:Z

    iget-object v6, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mUri:Ljava/lang/String;

    invoke-static {p0, v4, v5, v6}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAaccept:Ljava/lang/String;

    if-eqz v4, :cond_1

    const v4, 0x7f05005b

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-boolean v5, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsDatabaseContentType:Z

    iget-object v6, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAaccept:Ljava/lang/String;

    invoke-static {p0, v4, v5, v6}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v4, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthtype:Ljava/lang/String;

    if-eqz v4, :cond_2

    const v4, 0x7f050051

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-boolean v5, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsDatabaseAuthType:Z

    iget-object v6, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthtype:Ljava/lang/String;

    invoke-static {p0, v4, v5, v6}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v4, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthname:Ljava/lang/String;

    if-eqz v4, :cond_3

    const v4, 0x7f050045

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-boolean v5, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsDatabaseAuthName:Z

    iget-object v6, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthname:Ljava/lang/String;

    invoke-static {p0, v4, v5, v6}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v4, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthsecret:Ljava/lang/String;

    if-eqz v4, :cond_4

    const v4, 0x7f050046

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-boolean v5, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsDatabaseAuthSecret:Z

    iget-object v6, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthsecret:Ljava/lang/String;

    invoke-static {p0, v4, v5, v6}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    iget-object v4, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mCliuri:Ljava/lang/String;

    if-eqz v4, :cond_5

    const v4, 0x7f05005c

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-boolean v5, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsClientDatabaseUrl:Z

    iget-object v6, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mCliuri:Ljava/lang/String;

    invoke-static {p0, v4, v5, v6}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    iget-object v4, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mSynctype:Ljava/lang/String;

    if-eqz v4, :cond_6

    const v4, 0x7f05005d

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-boolean v5, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsSyncType:Z

    iget-object v6, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mSynctype:Ljava/lang/String;

    invoke-static {p0, v4, v5, v6}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_8
    return-void
.end method

.method private static getDsServerInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .param p2    # Ljava/lang/StringBuilder;

    const v4, 0x7f05002f

    const/4 v3, 0x0

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const v0, 0x7f05004e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsServerName:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    if-eqz v0, :cond_1

    const v0, 0x7f05004d

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sSuplProviderId:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddr:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsServerAddress:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddr:Ljava/lang/String;

    invoke-static {p0, v1, v2, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    :goto_0
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddrtype:Ljava/lang/String;

    if-eqz v0, :cond_3

    const v0, 0x7f05004f

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsAddressType:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddrtype:Ljava/lang/String;

    invoke-static {p0, v1, v2, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mPortnbr:Ljava/lang/String;

    if-eqz v0, :cond_4

    const v0, 0x7f050030

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsPortNumber:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mPortnbr:Ljava/lang/String;

    invoke-static {p0, v1, v2, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    return-void

    :cond_5
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDsServerAddress:Z

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v1, v2, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method private static getDsSettingInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)Landroid/text/SpannableStringBuilder;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;

    if-nez p1, :cond_1

    const-string v2, "Omacp/OmacpMessageUtils"

    const-string v3, "OmacpMessageUtils getDsSettingInfo application is null."

    invoke-static {v2, v3}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sDs:Z

    if-nez v2, :cond_2

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    invoke-static {p0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const v2, 0x7f05003f

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    :cond_2
    invoke-static {p0, p1, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getDsServerInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V

    invoke-static {p0, p1, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getDsAuthInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V

    invoke-static {p0, p1, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getDsResourceInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    invoke-static {p0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method private static getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Ljava/lang/String;

    const-string v0, "\n"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez p2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f05003f

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getEmailElement(Landroid/content/Context;Ljava/lang/String;ZZZLjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z
    .param p5    # Ljava/lang/String;

    const v2, 0x7f05003f

    const-string v0, "\n"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz p4, :cond_1

    if-eqz p2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    if-eqz p3, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getEmailSetting(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v1, "25"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailAccountName:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailAccountName:Ljava/lang/String;

    :cond_0
    sget-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {p0, p1}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getEmailSettingInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f050034

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    sget-boolean v0, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailOutboundAuthType:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f05003f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthtype:Ljava/lang/String;

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f050035

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f050036

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    sget-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailAccountName:Ljava/lang/String;

    if-nez v0, :cond_6

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailAccountName:Ljava/lang/String;

    :cond_6
    sget-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailInboundType:Ljava/lang/String;

    if-nez v0, :cond_8

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v1, "110"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x7f05001d

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailInboundType:Ljava/lang/String;

    :cond_7
    :goto_1
    sget-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sInboundEmailSetting:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-static {p0, p1}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getEmailSettingInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sInboundEmailSetting:Ljava/lang/String;

    goto/16 :goto_0

    :cond_8
    sget-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailInboundType:Ljava/lang/String;

    if-nez v0, :cond_7

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v1, "143"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const v0, 0x7f05001e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailInboundType:Ljava/lang/String;

    goto :goto_1
.end method

.method private static getEmailSettingInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)Ljava/lang/String;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;

    if-nez p1, :cond_0

    const-string v2, "Omacp/OmacpMessageUtils"

    const-string v3, "OmacpMessageUtils getEmailInboundSettingInfo application is null."

    invoke-static {v2, v3}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    const/4 v1, 0x0

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v3, "25"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    if-eqz v2, :cond_2

    const v2, 0x7f050054

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-boolean v3, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sEmailSettingName:Z

    iget-object v4, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    invoke-static {p0, v2, v3, v4}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-static {p0, p1, v0, v1}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendEmailUserInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;Z)V

    invoke-static {p0, p1, v0, v1}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendEmailServerInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;Z)V

    invoke-static {p0, p1, v0, v1}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendEmailOutBoundInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;Z)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private static getImpsSettingInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)Landroid/text/SpannableStringBuilder;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;

    if-nez p1, :cond_1

    const-string v2, "Omacp/OmacpMessageUtils"

    const-string v3, "OmacpMessageUtils getDsSettingInfo application is null."

    invoke-static {v2, v3}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sImps:Z

    if-nez v2, :cond_2

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    invoke-static {p0, v0, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const v2, 0x7f05003f

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    :cond_2
    invoke-static {p0, p1, v1}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendImpsServerInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    invoke-static {p0, v0, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method private static getMmsSettingsInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/lang/StringBuilder;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .param p2    # Ljava/lang/StringBuilder;

    if-nez p1, :cond_1

    const-string v0, "Omacp/OmacpMessageUtils"

    const-string v1, "OmacpMessageUtils addMmsSettingsInfo application is null."

    invoke-static {v0, v1}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v1, "w4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mCm:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, "Omacp/OmacpMessageUtils"

    const-string v1, "OmacpMessageUtils invalid w4 mms setting."

    invoke-static {v0, v1}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v1, "ap0005"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mCm:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mRm:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMs:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mPcAddr:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMa:Ljava/lang/String;

    if-nez v0, :cond_3

    const-string v0, "Omacp/OmacpMessageUtils"

    const-string v1, "OmacpMessageUtils invalid ap0005 mms setting."

    invoke-static {v0, v1}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mCm:Ljava/lang/String;

    if-eqz v0, :cond_4

    const v0, 0x7f050048

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMmsCm:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mCm:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mRm:Ljava/lang/String;

    if-eqz v0, :cond_5

    const v0, 0x7f050049

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMmsRm:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mRm:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMs:Ljava/lang/String;

    if-eqz v0, :cond_6

    const v0, 0x7f05004a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMmsMs:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMs:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mPcAddr:Ljava/lang/String;

    if-eqz v0, :cond_7

    const v0, 0x7f05004b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMmsPcAddr:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mPcAddr:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMa:Ljava/lang/String;

    if-eqz v0, :cond_0

    const v0, 0x7f05004c

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sMmsMa:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMa:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method private static getOneValidApplicationNameSet(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v0, v1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, v1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v4, "25"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v4, "110"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v4, "143"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    const v3, 0x7f05001b

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_1
    :goto_0
    if-eqz v2, :cond_2

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void

    :cond_3
    iget-object v3, v1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v4, "w4"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, v1, Lcom/mediatek/omacp/parser/ApplicationClass;->mCm:Ljava/lang/String;

    if-eqz v3, :cond_1

    invoke-static {p0, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getAppName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_4
    iget-object v3, v1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v4, "ap0005"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, v1, Lcom/mediatek/omacp/parser/ApplicationClass;->mCm:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v1, Lcom/mediatek/omacp/parser/ApplicationClass;->mRm:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMs:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v1, Lcom/mediatek/omacp/parser/ApplicationClass;->mPcAddr:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMa:Ljava/lang/String;

    if-eqz v3, :cond_1

    :cond_5
    invoke-static {p0, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getAppName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_6
    invoke-static {p0, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getAppName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private static getProxyClassParams(Landroid/content/Context;Ljava/util/ArrayList;Lcom/mediatek/omacp/parser/NapdefClass;Ljava/lang/StringBuilder;Lcom/mediatek/omacp/message/OmacpMessageUtils$ApnProxyParam;)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p2    # Lcom/mediatek/omacp/parser/NapdefClass;
    .param p3    # Ljava/lang/StringBuilder;
    .param p4    # Lcom/mediatek/omacp/message/OmacpMessageUtils$ApnProxyParam;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ProxyClass;",
            ">;",
            "Lcom/mediatek/omacp/parser/NapdefClass;",
            "Ljava/lang/StringBuilder;",
            "Lcom/mediatek/omacp/message/OmacpMessageUtils$ApnProxyParam;",
            ")V"
        }
    .end annotation

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ProxyClass;

    invoke-static {v2, p2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->checkPxphysical(Lcom/mediatek/omacp/parser/ProxyClass;Lcom/mediatek/omacp/parser/NapdefClass;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPxaddr:Ljava/lang/String;

    if-eqz v3, :cond_3

    const-string v3, "\n"

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v3, 0x7f050042

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v2, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPxaddr:Ljava/lang/String;

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v2, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPxaddr:Ljava/lang/String;

    iput-object v3, p4, Lcom/mediatek/omacp/message/OmacpMessageUtils$ApnProxyParam;->mProxy:Ljava/lang/String;

    :cond_3
    iget-object v3, v2, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mPortnbr:Ljava/lang/String;

    if-eqz v3, :cond_0

    const-string v3, "\n"

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v3, 0x7f050030

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v2, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mPortnbr:Ljava/lang/String;

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v2, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mPortnbr:Ljava/lang/String;

    iput-object v3, p4, Lcom/mediatek/omacp/message/OmacpMessageUtils$ApnProxyParam;->mPort:Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1
.end method

.method private static getRtspSettingInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)Landroid/text/SpannableStringBuilder;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;

    if-nez p1, :cond_1

    const-string v3, "Omacp/OmacpMessageUtils"

    const-string v4, "OmacpMessageUtils getRtspSettingInfo application is null."

    invoke-static {v3, v4}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-boolean v3, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtsp:Z

    if-nez v3, :cond_2

    iget-object v3, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    invoke-static {p0, v1, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const v3, 0x7f05003f

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    :cond_2
    iget-object v3, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    if-eqz v3, :cond_3

    const v3, 0x7f05004d

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-boolean v4, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtspProviderId:Z

    iget-object v5, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    invoke-static {p0, v3, v4, v5}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v3, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    if-eqz v3, :cond_4

    const v3, 0x7f050060

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-boolean v4, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtspName:Z

    iget-object v5, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    invoke-static {p0, v3, v4, v5}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    iget-object v3, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMaxBandwidth:Ljava/lang/String;

    if-eqz v3, :cond_5

    const v3, 0x7f050058

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-boolean v4, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtspMaxBandwidth:Z

    iget-object v5, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMaxBandwidth:Ljava/lang/String;

    invoke-static {p0, v3, v4, v5}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    iget-object v3, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mNetinfo:Ljava/util/ArrayList;

    if-eqz v3, :cond_6

    const/4 v0, 0x0

    :goto_1
    iget-object v3, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mNetinfo:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_6

    const v3, 0x7f050037

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-boolean v5, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtspNetInfo:Z

    iget-object v3, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mNetinfo:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {p0, v4, v5, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    iget-object v3, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMaxUdpPort:Ljava/lang/String;

    if-eqz v3, :cond_7

    const v3, 0x7f050038

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-boolean v4, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtspMaxUdpPort:Z

    iget-object v5, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMaxUdpPort:Ljava/lang/String;

    invoke-static {p0, v3, v4, v5}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    iget-object v3, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMinUdpPort:Ljava/lang/String;

    if-eqz v3, :cond_8

    const v3, 0x7f050039

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-boolean v4, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sRtspMinUdpPort:Z

    iget-object v5, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMinUdpPort:Ljava/lang/String;

    invoke-static {p0, v3, v4, v5}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    invoke-static {p0, v1, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0
.end method

.method public static getSettingsDetailInfo(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)Landroid/text/SpannableStringBuilder;
    .locals 9
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/NapdefClass;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ProxyClass;",
            ">;)",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass;

    const-string v6, "Omacp/OmacpMessageUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "OmacpMessageUtils getSettingsDetailInfo application is : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/omacp/utils/MTKlog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v1, v5, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendApplicationParams(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;Landroid/text/SpannableStringBuilder;Ljava/lang/StringBuilder;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-static {p0, v5, v3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendMmsInfo(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/StringBuilder;)V

    invoke-static {p0, v5}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->appendEmailInfo(Landroid/content/Context;Landroid/text/SpannableStringBuilder;)V

    :cond_1
    if-eqz p2, :cond_3

    invoke-static {p0, p2, p1, p3}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getApnSettingInfo(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_2

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    invoke-virtual {v5, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_3
    const-string v6, "Omacp/OmacpMessageUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "OmacpMessageUtils getSettingsDetailInfo info is : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/omacp/utils/MTKlog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-object v5
.end method

.method public static getSummary(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const v9, 0x7f050014

    const/4 v8, 0x0

    const-string v5, "Omacp/OmacpMessageUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "OmacpMessageUtils savedSummary is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/omacp/utils/MTKlog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    :cond_0
    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    const-string v5, ","

    invoke-virtual {v1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v0, 0x0

    if-ltz v3, :cond_0

    invoke-virtual {v1, v8, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v6, v3, 0x1

    if-le v5, v6, :cond_1

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-static {p0, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getAppName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "Omacp/OmacpMessageUtils"

    const-string v6, "OmacpMessageUtils summary is null."

    invoke-static {v5, v6}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    :goto_2
    return-object v4

    :cond_3
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-virtual {v4, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_2
.end method

.method private static getSuplSettingInfo(Landroid/content/Context;Lcom/mediatek/omacp/parser/ApplicationClass;)Landroid/text/SpannableStringBuilder;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;

    const v6, 0x7f05002f

    const/4 v5, 0x0

    if-nez p1, :cond_1

    const-string v2, "Omacp/OmacpMessageUtils"

    const-string v3, "OmacpMessageUtils getSuplSettingInfo application is null."

    invoke-static {v2, v3}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-boolean v2, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sSupl:Z

    if-nez v2, :cond_2

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    invoke-static {p0, v0, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const v2, 0x7f05003f

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    if-eqz v2, :cond_3

    const v2, 0x7f05004d

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-boolean v3, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sSuplProviderId:Z

    iget-object v4, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    invoke-static {p0, v2, v3, v4}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    if-eqz v2, :cond_4

    const v2, 0x7f05004e

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-boolean v3, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sSuplServerName:Z

    iget-object v4, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    invoke-static {p0, v2, v3, v4}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v2, v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddr:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-boolean v4, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sSuplServerAddr:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v2, v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddr:Ljava/lang/String;

    invoke-static {p0, v3, v4, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    :goto_1
    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v2, v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddrtype:Ljava/lang/String;

    if-eqz v2, :cond_6

    const v2, 0x7f05004f

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-boolean v4, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sSuplAddrType:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v2, v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddrtype:Ljava/lang/String;

    invoke-static {p0, v3, v4, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    invoke-static {p0, v0, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->addApplicationLabel(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    :cond_7
    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-boolean v4, Lcom/mediatek/omacp/message/OmacpApplicationCapability;->sSuplServerAddr:Z

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {p0, v3, v4, v2}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getElement(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static getValidApplicationNameSet(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/NapdefClass;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-static {p0, p1, v1, v0}, Lcom/mediatek/omacp/message/OmacpMessageUtils;->getOneValidApplicationNameSet(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    const v3, 0x7f05001a

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object v1
.end method

.method private static resetEmailStaticSetting()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailAccountName:Ljava/lang/String;

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sInboundEmailSetting:Ljava/lang/String;

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sOutboundEmailSetting:Ljava/lang/String;

    sput-object v0, Lcom/mediatek/omacp/message/OmacpMessageUtils;->sEmailInboundType:Ljava/lang/String;

    return-void
.end method
