.class public Lcom/mediatek/omacp/message/OmacpReceiverService;
.super Landroid/app/Service;
.source "OmacpReceiverService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/omacp/message/OmacpReceiverService$1;,
        Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;,
        Lcom/mediatek/omacp/message/OmacpReceiverService$ServiceHandler;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final TAG:Ljava/lang/String; = "Omacp/OmacpReceiverService"


# instance fields
.field private mServiceHandler:Lcom/mediatek/omacp/message/OmacpReceiverService$ServiceHandler;

.field private mServiceLooper:Landroid/os/Looper;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/omacp/message/OmacpReceiverService;Landroid/content/Intent;I)V
    .locals 0
    .param p0    # Lcom/mediatek/omacp/message/OmacpReceiverService;
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/omacp/message/OmacpReceiverService;->handleOmacpReceived(Landroid/content/Intent;I)V

    return-void
.end method

.method private getApplicationSummary(Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/util/ArrayList;)V
    .locals 4
    .param p1    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mediatek/omacp/parser/ApplicationClass;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v3, "25"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v3, "110"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v3, "143"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const-string v1, "25"

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void

    :cond_2
    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v3, "w4"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mCm:Ljava/lang/String;

    if-eqz v2, :cond_3

    const-string v1, "w4"

    goto :goto_0

    :cond_3
    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    const-string v3, "ap0005"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mCm:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mRm:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMs:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mPcAddr:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/mediatek/omacp/parser/ApplicationClass;->mMa:Ljava/lang/String;

    if-eqz v2, :cond_5

    :cond_4
    const-string v1, "w4"

    goto :goto_0

    :cond_5
    move-object v1, v0

    goto :goto_0
.end method

.method private getParser(Landroid/content/Intent;Lcom/mediatek/omacp/parser/OmacpParser;[B)V
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/mediatek/omacp/parser/OmacpParser;
    .param p3    # [B

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "text/vnd.wap.connectivity-xml"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/mediatek/omacp/parser/OmacpParser;->getTextParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/mediatek/omacp/parser/OmacpParser;->setParser(Ljava/lang/Object;)V

    :cond_0
    :goto_0
    if-eqz p3, :cond_1

    invoke-virtual {p2, p3}, Lcom/mediatek/omacp/parser/OmacpParser;->parse([B)V

    :cond_1
    return-void

    :cond_2
    const-string v1, "application/vnd.wap.connectivity-wbxml"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mediatek/omacp/parser/OmacpParser;->getWbxmlParser()Lorg/kxml2/wap/WbxmlParser;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/mediatek/omacp/parser/OmacpParser;->setParser(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private getSavedSummary(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/NapdefClass;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass;

    invoke-direct {p0, v0, v3}, Lcom/mediatek/omacp/message/OmacpReceiverService;->getApplicationSummary(Lcom/mediatek/omacp/parser/ApplicationClass;Ljava/util/ArrayList;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "apn"

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method private handleOmacpReceived(Landroid/content/Intent;I)V
    .locals 11
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    const/4 v10, 0x1

    const-string v7, "Omacp/OmacpReceiverService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "OmacpReceiverService handleOmacpReceived: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/omacp/utils/MTKlog;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "data"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;

    const/4 v7, 0x0

    invoke-direct {v3, p0, v7}, Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;-><init>(Lcom/mediatek/omacp/message/OmacpReceiverService;Lcom/mediatek/omacp/message/OmacpReceiverService$1;)V

    invoke-direct {p0, p1, v1, v3}, Lcom/mediatek/omacp/message/OmacpReceiverService;->handlePinVerify(Landroid/content/Intent;[BLcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    invoke-direct {p0, p1, v1, v6, v3}, Lcom/mediatek/omacp/message/OmacpReceiverService;->parseOmacpMessage(Landroid/content/Intent;[BLandroid/content/ContentValues;Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/mediatek/omacp/provider/OmacpProviderDatabase;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-static {p0, v10}, Lcom/mediatek/omacp/message/OmacpMessageNotification;->blockingUpdateNewMessageIndicator(Landroid/content/Context;Z)V

    const-string v7, "omacp"

    invoke-virtual {p0, v7, v10}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v7, "configuration_msg_exist"

    invoke-interface {v0, v7, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private handlePinVerify(Landroid/content/Intent;[BLcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;)Z
    .locals 11
    .param p1    # Landroid/content/Intent;
    .param p2    # [B
    .param p3    # Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;

    const/4 v8, 0x0

    const-string v7, "contentTypeParameters"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const/4 v5, 0x0

    const/4 v4, 0x0

    if-nez v0, :cond_0

    const-string v7, "Omacp/OmacpReceiverService"

    const-string v9, "OmacpReceiverService contentTypeParamaters is null."

    invoke-static {v7, v9}, Lcom/mediatek/omacp/utils/MTKlog;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    if-eqz v5, :cond_2

    const-string v7, ""

    invoke-virtual {v5, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    if-eqz v4, :cond_2

    const-string v7, ""

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "simId"

    const/4 v9, -0x1

    invoke-virtual {p1, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    invoke-static {v6}, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;->getSimImsi(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;->imsiToKey(Ljava/lang/String;)[B

    move-result-object v3

    if-nez v3, :cond_1

    move v7, v8

    :goto_1
    return v7

    :cond_0
    const-string v7, "MAC"

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {p3, v7}, Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;->access$202(Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;Ljava/lang/String;)Ljava/lang/String;

    const-string v7, "SEC"

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {p3, v7}, Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;->access$302(Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p3}, Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;->access$300(Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p3}, Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;->access$200(Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "Omacp/OmacpReceiverService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "OmacpReceiverService handleOmacpReceived: MAC is : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "SEC is : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/mediatek/omacp/utils/MTKlog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {v3, v8, p2, v4}, Lcom/mediatek/omacp/message/OmacpMessageSettingsDetail;->verifyPin([BI[BLjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v7, "Omacp/OmacpReceiverService"

    const-string v9, "OmacpReceiverService Network PIN IMSI verify failed. Will ignore this message."

    invoke-static {v7, v9}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v7, v8

    goto :goto_1

    :cond_2
    const/4 v7, 0x1

    goto :goto_1
.end method

.method private parseOmacpMessage(Landroid/content/Intent;[BLandroid/content/ContentValues;Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;)V
    .locals 20
    .param p1    # Landroid/content/Intent;
    .param p2    # [B
    .param p3    # Landroid/content/ContentValues;
    .param p4    # Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;

    const/4 v3, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v5, 0x0

    new-instance v9, Lcom/mediatek/omacp/parser/OmacpParser;

    invoke-direct {v9}, Lcom/mediatek/omacp/parser/OmacpParser;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v9, v2}, Lcom/mediatek/omacp/message/OmacpReceiverService;->getParser(Landroid/content/Intent;Lcom/mediatek/omacp/parser/OmacpParser;[B)V

    invoke-virtual {v9}, Lcom/mediatek/omacp/parser/OmacpParser;->getApSectionList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v9}, Lcom/mediatek/omacp/parser/OmacpParser;->getNapList()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v9}, Lcom/mediatek/omacp/parser/OmacpParser;->getPxList()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v9}, Lcom/mediatek/omacp/parser/OmacpParser;->getContextName()Ljava/lang/String;

    move-result-object v5

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v16

    if-eqz v16, :cond_3

    :cond_0
    if-eqz v8, :cond_1

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v16

    if-eqz v16, :cond_3

    :cond_1
    if-eqz v10, :cond_2

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v16

    if-eqz v16, :cond_3

    :cond_2
    const-string v16, "Omacp/OmacpReceiverService"

    const-string v17, "OmacpReceiverService handleOmacpReceived parser error."

    invoke-static/range {v16 .. v17}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    const-wide/16 v18, 0x3e8

    div-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v6, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v8}, Lcom/mediatek/omacp/message/OmacpReceiverService;->getSavedSummary(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v16, "address"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v16, "service_center"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v16, "simId"

    const/16 v17, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v14

    const-string v16, "Omacp/OmacpReceiverService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "OmacpReceiverService handleOmacpReceived from : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "service center is : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "simId is : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/mediatek/omacp/utils/MTKlog;->i(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v12, :cond_4

    if-nez v13, :cond_5

    :cond_4
    const-string v16, "Omacp/OmacpReceiverService"

    const-string v17, "OmacpReceiverService handleOmacpReceived: sender or service center is null!"

    invoke-static/range {v16 .. v17}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v16, "sim_id"

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v16, "sender"

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v16, "service_center"

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v16, "seen"

    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v16, "read"

    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v16, "date"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v16, "installed"

    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static/range {p4 .. p4}, Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;->access$300(Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;)Ljava/lang/String;

    move-result-object v11

    invoke-static/range {p4 .. p4}, Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;->access$200(Lcom/mediatek/omacp/message/OmacpReceiverService$NetWorkPinData;)Ljava/lang/String;

    move-result-object v7

    if-eqz v11, :cond_6

    if-eqz v7, :cond_6

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_6

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_7

    :cond_6
    const-string v16, "pin_unlock"

    const/16 v17, 0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_1
    const-string v16, "sec"

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v16, "mac"

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v16, "title"

    const/high16 v17, 0x7f050000

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v16, "summary"

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v16, "body"

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v16, "context"

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v16, "mime_type"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v16, "pin_unlock"

    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    const-string v1, "Omacp/OmacpReceiverService"

    const-string v2, "OmacpReceiverService onCreate"

    invoke-static {v1, v2}, Lcom/mediatek/omacp/utils/MTKlog;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Omacp"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/omacp/message/OmacpReceiverService;->mServiceLooper:Landroid/os/Looper;

    new-instance v1, Lcom/mediatek/omacp/message/OmacpReceiverService$ServiceHandler;

    iget-object v2, p0, Lcom/mediatek/omacp/message/OmacpReceiverService;->mServiceLooper:Landroid/os/Looper;

    invoke-direct {v1, p0, v2}, Lcom/mediatek/omacp/message/OmacpReceiverService$ServiceHandler;-><init>(Lcom/mediatek/omacp/message/OmacpReceiverService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mediatek/omacp/message/OmacpReceiverService;->mServiceHandler:Lcom/mediatek/omacp/message/OmacpReceiverService$ServiceHandler;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "Omacp/OmacpReceiverService"

    const-string v1, "OmacpReceiverService onDestroy"

    invoke-static {v0, v1}, Lcom/mediatek/omacp/utils/MTKlog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/omacp/message/OmacpReceiverService;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const-string v1, "Omacp/OmacpReceiverService"

    const-string v2, "OmacpReceiverService onStartCommand"

    invoke-static {v1, v2}, Lcom/mediatek/omacp/utils/MTKlog;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/omacp/message/OmacpReceiverService;->mServiceHandler:Lcom/mediatek/omacp/message/OmacpReceiverService$ServiceHandler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput p3, v0, Landroid/os/Message;->arg1:I

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/mediatek/omacp/message/OmacpReceiverService;->mServiceHandler:Lcom/mediatek/omacp/message/OmacpReceiverService$ServiceHandler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v1, 0x2

    return v1
.end method
