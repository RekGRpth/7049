.class public Lcom/mediatek/omacp/parser/OmacpParserUtils;
.super Ljava/lang/Object;
.source "OmacpParserUtils.java"


# static fields
.field private static final DEBUG:Z = true

.field private static final TAG:Ljava/lang/String; = "Omacp/OmacpParserUtils"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static handleApParameters(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/ApplicationClass;)V
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/mediatek/omacp/parser/ApplicationClass;

    if-nez p3, :cond_1

    const-string v3, "Omacp/OmacpParserUtils"

    const-string v4, "OmacpParserUtils handleApParameters application is null."

    invoke-static {v3, v4}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    iget-object v1, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppauth:Ljava/util/ArrayList;

    iget-object v2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mResource:Ljava/util/ArrayList;

    const-string v3, "APPLICATION"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {p0, p1, p2, p3}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleApplicationParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/ApplicationClass;)V

    goto :goto_0

    :cond_2
    const-string v3, "APPADDR"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {p1, p2, v0, p3}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleAppAddrParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/mediatek/omacp/parser/ApplicationClass;)V

    goto :goto_0

    :cond_3
    const-string v3, "PORT"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {p1, p2, v0}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleApPortParam(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_4
    const-string v3, "APPAUTH"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {p1, p2, v1}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleAppAuthParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_5
    const-string v3, "RESOURCE"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p1, p2, v2}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleResourceParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private static handleApPortParam(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    const-string v2, "Omacp/OmacpParserUtils"

    const-string v3, "OmacpParserUtils handleApParameters APPADDR size is 0."

    invoke-static {v2, v3}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v2, v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    const-string v2, "Omacp/OmacpParserUtils"

    const-string v3, "OmacpParserUtils handleApParameters PORT size is 0."

    invoke-static {v2, v3}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v2, "PORTNBR"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v2, v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v2, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mPortnbr:Ljava/lang/String;

    if-nez v2, :cond_3

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v2, v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iput-object p1, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mPortnbr:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v2, "SERVICE"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v2, v2, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mPort:Ljava/util/ArrayList;

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v2, v2, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mService:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static handleAppAddrParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/mediatek/omacp/parser/ApplicationClass;)V
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p3    # Lcom/mediatek/omacp/parser/ApplicationClass;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;",
            ">;",
            "Lcom/mediatek/omacp/parser/ApplicationClass;",
            ")V"
        }
    .end annotation

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    const-string v1, "Omacp/OmacpParserUtils"

    const-string v2, "OmacpParserUtils handleApParameters APPADDR size is 0."

    invoke-static {v1, v2}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "ADDR"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddr:Ljava/lang/String;

    if-nez v1, :cond_2

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddr:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v1, "ADDRTYPE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppaddr:Ljava/util/ArrayList;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddrtype:Ljava/lang/String;

    if-nez v1, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAddr;->mAddrtype:Ljava/lang/String;

    goto :goto_0
.end method

.method private static handleAppAuthParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    const-string v1, "Omacp/OmacpParserUtils"

    const-string v2, "OmacpParserUtils handleApParameters APPAUTH size is 0."

    invoke-static {v1, v2}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "AAUTHLEVEL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthlevel:Ljava/lang/String;

    if-nez v1, :cond_2

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthlevel:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v1, "AAUTHTYPE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthtype:Ljava/lang/String;

    if-nez v1, :cond_3

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthtype:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v1, "AAUTHNAME"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthname:Ljava/lang/String;

    if-nez v1, :cond_4

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthname:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v1, "AAUTHSECRET"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthsecret:Ljava/lang/String;

    if-nez v1, :cond_5

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthsecret:Ljava/lang/String;

    goto :goto_0

    :cond_5
    const-string v1, "AAUTHDATA"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthdata:Ljava/lang/String;

    if-nez v1, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$AppAuth;->mAauthdata:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private static handleApplicationParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/ApplicationClass;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/mediatek/omacp/parser/ApplicationClass;

    const-string v0, "APPID"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    if-nez v0, :cond_0

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const-string v0, "PROVIDER-ID"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    if-nez v0, :cond_1

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mProviderId:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v0, "NAME"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    if-nez v0, :cond_2

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mName:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, "AACCEPT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mAaccept:Ljava/lang/String;

    if-nez v0, :cond_3

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mAaccept:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "APROTOCOL"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mAprotocol:Ljava/lang/String;

    if-nez v0, :cond_4

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mAprotocol:Ljava/lang/String;

    goto :goto_0

    :cond_4
    invoke-static {p0, p1, p2, p3}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleOtherApplicationParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/ApplicationClass;)V

    goto :goto_0
.end method

.method private static handleLowUseApplicationParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/ApplicationClass;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/mediatek/omacp/parser/ApplicationClass;

    const-string v0, "MAX-BANDWIDTH"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mMaxBandwidth:Ljava/lang/String;

    if-nez v0, :cond_1

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mMaxBandwidth:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "NETINFO"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mNetinfo:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const-string v0, "MIN-UDP-PORT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mMinUdpPort:Ljava/lang/String;

    if-nez v0, :cond_3

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mMinUdpPort:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "MAX-UDP-PORT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mMaxUdpPort:Ljava/lang/String;

    if-nez v0, :cond_4

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mMaxUdpPort:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v0, "SERVICES"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mServices:Ljava/lang/String;

    if-nez v0, :cond_5

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mServices:Ljava/lang/String;

    goto :goto_0

    :cond_5
    const-string v0, "CIDPREFIX"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mCidprefix:Ljava/lang/String;

    if-nez v0, :cond_0

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mCidprefix:Ljava/lang/String;

    goto :goto_0
.end method

.method private static handleLowUseNapDefParams(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/NapdefClass;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/omacp/parser/NapdefClass;

    const-string v0, "TRANSFER-DELAY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mTransferdelay:Ljava/lang/String;

    if-nez v0, :cond_1

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mTransferdelay:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "GUARANTEED-BITRATE-UPLINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mGuaranteedbitrateuplink:Ljava/lang/String;

    if-nez v0, :cond_2

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mGuaranteedbitrateuplink:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, "GUARANTEED-BITRATE-DNLINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mGuaranteedbitratednlink:Ljava/lang/String;

    if-nez v0, :cond_3

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mGuaranteedbitratednlink:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "MAX-NUM-RETRY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mMaxnumretry:Ljava/lang/String;

    if-nez v0, :cond_4

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mMaxnumretry:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v0, "FIRST-RETRY-TIMEOUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mFirstretrytimeout:Ljava/lang/String;

    if-nez v0, :cond_5

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mFirstretrytimeout:Ljava/lang/String;

    goto :goto_0

    :cond_5
    const-string v0, "REREG-THRESHOLD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mReregthreshold:Ljava/lang/String;

    if-nez v0, :cond_6

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mReregthreshold:Ljava/lang/String;

    goto :goto_0

    :cond_6
    const-string v0, "T-BIT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mTbit:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "1"

    iput-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mTbit:Ljava/lang/String;

    goto :goto_0
.end method

.method private static handleLowUsePxLogicalParams(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/ProxyClass;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/omacp/parser/ProxyClass;

    const-string v0, "MASTER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mMaster:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "1"

    iput-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mMaster:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "STARTPAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mStartpage:Ljava/lang/String;

    if-nez v0, :cond_2

    iput-object p1, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mStartpage:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, "BASAUTH-ID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mBasauthId:Ljava/lang/String;

    if-nez v0, :cond_3

    iput-object p1, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mBasauthId:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "BASAUTH-PW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mBasauthPw:Ljava/lang/String;

    if-nez v0, :cond_4

    iput-object p1, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mBasauthPw:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v0, "WSP-VERSION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mWspVersion:Ljava/lang/String;

    if-nez v0, :cond_5

    iput-object p1, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mWspVersion:Ljava/lang/String;

    goto :goto_0

    :cond_5
    const-string v0, "PUSHENABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mPushenabled:Ljava/lang/String;

    if-nez v0, :cond_6

    iput-object p1, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mPushenabled:Ljava/lang/String;

    goto :goto_0

    :cond_6
    const-string v0, "PULLENBALED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mPullenbaled:Ljava/lang/String;

    if-nez v0, :cond_0

    iput-object p1, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mPullenbaled:Ljava/lang/String;

    goto :goto_0
.end method

.method private static handleLowUsePxPhysicalParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;",
            ">;I)V"
        }
    .end annotation

    const-string v0, "PXADDR-FQDN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPxaddrFqdn:Ljava/lang/String;

    if-nez v0, :cond_1

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iput-object p1, v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPxaddrFqdn:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "WSP-VERSION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mWspVersion:Ljava/lang/String;

    if-nez v0, :cond_2

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iput-object p1, v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mWspVersion:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, "PUSHENABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPushenabled:Ljava/lang/String;

    if-nez v0, :cond_3

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iput-object p1, v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPushenabled:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "PULLENABLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPullenabled:Ljava/lang/String;

    if-nez v0, :cond_4

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iput-object p1, v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPullenabled:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v0, "TO-NAPID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mToNapid:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static handleNapAuthInfoParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    const-string v1, "Omacp/OmacpParserUtils"

    const-string v2, "OmacpParserUtils handleNapParameters NAPAUTHINFO size is 0."

    invoke-static {v1, v2}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "AUTHTYPE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;->mAuthtype:Ljava/lang/String;

    if-nez v1, :cond_2

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;->mAuthtype:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v1, "AUTHNAME"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;->mAuthname:Ljava/lang/String;

    if-nez v1, :cond_3

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;->mAuthname:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v1, "AUTHSECRET"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;->mAuthsecret:Ljava/lang/String;

    if-nez v1, :cond_4

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;->mAuthsecret:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v1, "AUTH_ENTITY"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;->mAuthentity:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    const-string v1, "SPI"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;->mSpi:Ljava/lang/String;

    if-nez v1, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/NapdefClass$NapAuthInfo;->mSpi:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private static handleNapDefAddrParams(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/NapdefClass;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/omacp/parser/NapdefClass;

    const-string v0, "NAP-ADDRESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mNapaddress:Ljava/lang/String;

    if-nez v0, :cond_0

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mNapaddress:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const-string v0, "NAP-ADDRTYPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mNapaddrtype:Ljava/lang/String;

    if-nez v0, :cond_1

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mNapaddrtype:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v0, "DNS-ADDR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mDnsaddr:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const-string v0, "CALLTYPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mCalltype:Ljava/lang/String;

    if-nez v0, :cond_3

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mCalltype:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "LOCAL_ADDR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mLocaladdr:Ljava/lang/String;

    if-nez v0, :cond_4

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mLocaladdr:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v0, "LOCAL_ADDRTYPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mLocaladdrtype:Ljava/lang/String;

    if-nez v0, :cond_5

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mLocaladdrtype:Ljava/lang/String;

    goto :goto_0

    :cond_5
    invoke-static {p0, p1, p2}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleNapDefLinkParams(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/NapdefClass;)V

    goto :goto_0
.end method

.method private static handleNapDefLinkParams(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/NapdefClass;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/omacp/parser/NapdefClass;

    const-string v0, "LINKSPEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mLinkspeed:Ljava/lang/String;

    if-nez v0, :cond_0

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mLinkspeed:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const-string v0, "DNLINKSPEED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mDnlinkspeed:Ljava/lang/String;

    if-nez v0, :cond_1

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mDnlinkspeed:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v0, "LINGER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mLinger:Ljava/lang/String;

    if-nez v0, :cond_2

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mLinger:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, "DELIVERY-ERR-SDU"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mDeliveryerrsdu:Ljava/lang/String;

    if-nez v0, :cond_3

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mDeliveryerrsdu:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "DELIVERY-ORDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mDeliveryorder:Ljava/lang/String;

    if-nez v0, :cond_4

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mDeliveryorder:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v0, "TRAFFIC-CLASS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mTrafficclass:Ljava/lang/String;

    if-nez v0, :cond_5

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mTrafficclass:Ljava/lang/String;

    goto :goto_0

    :cond_5
    invoke-static {p0, p1, p2}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleNapDefMaxParams(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/NapdefClass;)V

    goto :goto_0
.end method

.method private static handleNapDefMaxParams(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/NapdefClass;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/omacp/parser/NapdefClass;

    const-string v0, "MAX-SDU-SIZE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mMaxsdusize:Ljava/lang/String;

    if-nez v0, :cond_0

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mMaxsdusize:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const-string v0, "MAX-BITRATE-UPLINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mMaxbitrateuplink:Ljava/lang/String;

    if-nez v0, :cond_1

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mMaxbitrateuplink:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v0, "MAX-BITRATE-DNLINK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mMaxbitratednlink:Ljava/lang/String;

    if-nez v0, :cond_2

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mMaxbitratednlink:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, "RESIDUAL-BER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mResidualber:Ljava/lang/String;

    if-nez v0, :cond_3

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mResidualber:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "SDU-ERROR-RATIO"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mSduerrorratio:Ljava/lang/String;

    if-nez v0, :cond_4

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mSduerrorratio:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v0, "TRAFFIC-HANDL-PROI"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mTraffichandlproi:Ljava/lang/String;

    if-nez v0, :cond_5

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mTraffichandlproi:Ljava/lang/String;

    goto :goto_0

    :cond_5
    invoke-static {p0, p1, p2}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleLowUseNapDefParams(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/NapdefClass;)V

    goto :goto_0
.end method

.method private static handleNapDefParams(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/NapdefClass;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/omacp/parser/NapdefClass;

    const-string v0, "NAPID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mNapid:Ljava/lang/String;

    if-nez v0, :cond_0

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mNapid:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const-string v0, "BEARER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mBearer:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string v0, "NAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mName:Ljava/lang/String;

    if-nez v0, :cond_2

    iput-object p1, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mName:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, "INTERNET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mInternet:Ljava/lang/String;

    if-nez v0, :cond_3

    const-string v0, "1"

    iput-object v0, p2, Lcom/mediatek/omacp/parser/NapdefClass;->mInternet:Ljava/lang/String;

    goto :goto_0

    :cond_3
    invoke-static {p0, p1, p2}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleNapDefAddrParams(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/NapdefClass;)V

    goto :goto_0
.end method

.method public static handleNapParameters(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/NapdefClass;)V
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/mediatek/omacp/parser/NapdefClass;

    if-nez p3, :cond_1

    const-string v2, "Omacp/OmacpParserUtils"

    const-string v3, "OmacpParserUtils handleNapParameters nap is null."

    invoke-static {v2, v3}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p3, Lcom/mediatek/omacp/parser/NapdefClass;->mNapauthinfo:Ljava/util/ArrayList;

    iget-object v1, p3, Lcom/mediatek/omacp/parser/NapdefClass;->mValidity:Ljava/util/ArrayList;

    const-string v2, "NAPDEF"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p1, p2, p3}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleNapDefParams(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/NapdefClass;)V

    goto :goto_0

    :cond_2
    const-string v2, "NAPAUTHINFO"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p1, p2, v0}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleNapAuthInfoParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_3
    const-string v2, "VALIDITY"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1, p2, v1}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleValidityParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private static handleOtherAddrApplicationParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/ApplicationClass;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/mediatek/omacp/parser/ApplicationClass;

    const-string v0, "PC-ADDR"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mPcAddr:Ljava/lang/String;

    if-nez v0, :cond_0

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mPcAddr:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const-string v0, "Ma"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mMa:Ljava/lang/String;

    if-nez v0, :cond_1

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mMa:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v0, "INIT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mInit:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, "1"

    iput-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mInit:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, "FROM"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mFrom:Ljava/lang/String;

    if-nez v0, :cond_3

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mFrom:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "RT-ADDR"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mRtAddr:Ljava/lang/String;

    if-nez v0, :cond_4

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mRtAddr:Ljava/lang/String;

    goto :goto_0

    :cond_4
    invoke-static {p0, p1, p2, p3}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleLowUseApplicationParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/ApplicationClass;)V

    goto :goto_0
.end method

.method private static handleOtherApplicationParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/ApplicationClass;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/mediatek/omacp/parser/ApplicationClass;

    const-string v0, "TO-PROXY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mToProxy:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "TO-NAPID"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mToNapid:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string v0, "ADDR"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "APPLICATION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mAddr:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const-string v0, "CM"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mCm:Ljava/lang/String;

    if-nez v0, :cond_3

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mCm:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "RM"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mRm:Ljava/lang/String;

    if-nez v0, :cond_4

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mRm:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v0, "MS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mMs:Ljava/lang/String;

    if-nez v0, :cond_5

    iput-object p2, p3, Lcom/mediatek/omacp/parser/ApplicationClass;->mMs:Ljava/lang/String;

    goto :goto_0

    :cond_5
    invoke-static {p0, p1, p2, p3}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleOtherAddrApplicationParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/ApplicationClass;)V

    goto :goto_0
.end method

.method private static handleOtherResourceParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass$Resource;",
            ">;I)V"
        }
    .end annotation

    const-string v0, "AAUTHSECRET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthsecret:Ljava/lang/String;

    if-nez v0, :cond_1

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iput-object p1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthsecret:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "AAUTHDATA"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthdata:Ljava/lang/String;

    if-nez v0, :cond_2

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iput-object p1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthdata:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, "STARTPAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mStartpage:Ljava/lang/String;

    if-nez v0, :cond_3

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    const-string v1, "1"

    iput-object v1, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mStartpage:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "CLIURI"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mCliuri:Ljava/lang/String;

    if-nez v0, :cond_4

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iput-object p0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mCliuri:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v0, "SYNCTYPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iget-object v0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mSynctype:Ljava/lang/String;

    if-nez v0, :cond_0

    add-int/lit8 v0, p3, -0x1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iput-object p0, v0, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mSynctype:Ljava/lang/String;

    goto :goto_0
.end method

.method private static handlePortParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/mediatek/omacp/parser/ProxyClass;)V
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p4    # Lcom/mediatek/omacp/parser/ProxyClass;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass$Port;",
            ">;",
            "Lcom/mediatek/omacp/parser/ProxyClass;",
            ")V"
        }
    .end annotation

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    const-string v3, "PXLOGICAL"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-nez v2, :cond_1

    const-string v3, "Omacp/OmacpParserUtils"

    const-string v4, "OmacpParserUtils handlePxParameters PORT size is 0."

    invoke-static {v3, v4}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "PORTNBR"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    add-int/lit8 v3, v2, -0x1

    invoke-virtual {p3, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mPortnbr:Ljava/lang/String;

    if-nez v3, :cond_2

    add-int/lit8 v3, v2, -0x1

    invoke-virtual {p3, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iput-object p1, v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mPortnbr:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v3, "SERVICE"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v3, v2, -0x1

    invoke-virtual {p3, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mService:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    const-string v3, "PXPHYSICAL"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p4, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_4

    const-string v3, "Omacp/OmacpParserUtils"

    const-string v4, "OmacpParserUtils handlePxParameters PXPHYSICAL size is 0."

    invoke-static {v3, v4}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v3, p4, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPort:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_5

    const-string v3, "Omacp/OmacpParserUtils"

    const-string v4, "OmacpParserUtils handlePxParameters PXPHYSICAL PORT size is 0."

    invoke-static {v3, v4}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v3, "PORTNBR"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p4, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPort:Ljava/util/ArrayList;

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mPortnbr:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, p4, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPort:Ljava/util/ArrayList;

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iput-object p1, v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mPortnbr:Ljava/lang/String;

    goto/16 :goto_0

    :cond_6
    const-string v3, "SERVICE"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p4, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPort:Ljava/util/ArrayList;

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ApplicationClass$Port;->mService:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private static handlePxAuthInfoParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ProxyClass$PxAuthInfo;",
            ">;)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const-string v1, "Omacp/OmacpParserUtils"

    const-string v2, "OmacpParserUtils handlePxParameters PXAUTHINFO size is 0."

    invoke-static {v1, v2}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const-string v1, "PXAUTH-TYPE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ProxyClass$PxAuthInfo;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ProxyClass$PxAuthInfo;->mPxauthType:Ljava/lang/String;

    if-nez v1, :cond_3

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ProxyClass$PxAuthInfo;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ProxyClass$PxAuthInfo;->mPxauthType:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v1, "PXAUTH-ID"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ProxyClass$PxAuthInfo;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ProxyClass$PxAuthInfo;->mPxauthId:Ljava/lang/String;

    if-nez v1, :cond_4

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ProxyClass$PxAuthInfo;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ProxyClass$PxAuthInfo;->mPxauthId:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v1, "PXAUTH-PW"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ProxyClass$PxAuthInfo;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ProxyClass$PxAuthInfo;->mPxauthPw:Ljava/lang/String;

    if-nez v1, :cond_1

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ProxyClass$PxAuthInfo;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ProxyClass$PxAuthInfo;->mPxauthPw:Ljava/lang/String;

    goto :goto_0
.end method

.method private static handlePxLogicalParams(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/ProxyClass;)V
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/omacp/parser/ProxyClass;

    const-string v0, "PROXY-ID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mProxyId:Ljava/lang/String;

    if-nez v0, :cond_0

    iput-object p1, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mProxyId:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const-string v0, "PROXY-PW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mProxyPw:Ljava/lang/String;

    if-nez v0, :cond_1

    iput-object p1, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mProxyPw:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v0, "PPGAUTH-TYPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mPpgauthType:Ljava/lang/String;

    if-nez v0, :cond_2

    iput-object p1, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mPpgauthType:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, "PROXY-PROVIDER-ID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mProxyProviderId:Ljava/lang/String;

    if-nez v0, :cond_3

    iput-object p1, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mProxyProviderId:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "NAME"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mName:Ljava/lang/String;

    if-nez v0, :cond_4

    iput-object p1, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mName:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v0, "DOMAIN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mDomain:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    const-string v0, "TRUST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mTrust:Ljava/lang/String;

    if-nez v0, :cond_6

    const-string v0, "1"

    iput-object v0, p2, Lcom/mediatek/omacp/parser/ProxyClass;->mTrust:Ljava/lang/String;

    goto :goto_0

    :cond_6
    invoke-static {p0, p1, p2}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleLowUsePxLogicalParams(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/ProxyClass;)V

    goto :goto_0
.end method

.method public static handlePxParameters(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/ProxyClass;)V
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/mediatek/omacp/parser/ProxyClass;

    if-nez p4, :cond_1

    const-string v3, "Omacp/OmacpParserUtils"

    const-string v4, "OmacpParserUtils handlePxParameters px is null."

    invoke-static {v3, v4}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p4, Lcom/mediatek/omacp/parser/ProxyClass;->mPxauthinfo:Ljava/util/ArrayList;

    iget-object v1, p4, Lcom/mediatek/omacp/parser/ProxyClass;->mPort:Ljava/util/ArrayList;

    iget-object v0, p4, Lcom/mediatek/omacp/parser/ProxyClass;->mPxphysical:Ljava/util/ArrayList;

    const-string v3, "PXLOGICAL"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {p2, p3, p4}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handlePxLogicalParams(Ljava/lang/String;Ljava/lang/String;Lcom/mediatek/omacp/parser/ProxyClass;)V

    goto :goto_0

    :cond_2
    const-string v3, "PXAUTHINFO"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {p2, p3, v2}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handlePxAuthInfoParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_3
    const-string v3, "PORT"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {p2, p3, p0, v1, p4}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handlePortParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/mediatek/omacp/parser/ProxyClass;)V

    goto :goto_0

    :cond_4
    const-string v3, "PXPHYSICAL"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p2, p3, v0}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handlePxPhysicalParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private static handlePxPhysicalParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "Omacp/OmacpParserUtils"

    const-string v2, "OmacpParserUtils handlePxParameters PXPHYSICAL size is 0."

    invoke-static {v1, v2}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "PHYSICAL-PROXY-ID"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPhysicalProxyId:Ljava/lang/String;

    if-nez v1, :cond_1

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPhysicalProxyId:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v1, "DOMAIN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mDomain:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const-string v1, "PXADDR"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPxaddr:Ljava/lang/String;

    if-nez v1, :cond_3

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPxaddr:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v1, "PXADDRTYPE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPxaddrtype:Ljava/lang/String;

    if-nez v1, :cond_4

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ProxyClass$PxPhysical;->mPxaddrtype:Ljava/lang/String;

    goto :goto_0

    :cond_4
    invoke-static {p0, p1, p2, v0}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleLowUsePxPhysicalParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V

    goto :goto_0
.end method

.method private static handleResourceParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass$Resource;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "Omacp/OmacpParserUtils"

    const-string v2, "OmacpParserUtils handleApParameters RESOURCE size is 0."

    invoke-static {v1, v2}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "URI"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mUri:Ljava/lang/String;

    if-nez v1, :cond_1

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mUri:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v1, "NAME"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mName:Ljava/lang/String;

    if-nez v1, :cond_2

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mName:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v1, "AACCEPT"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAaccept:Ljava/lang/String;

    if-nez v1, :cond_3

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAaccept:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v1, "AAUTHTYPE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthtype:Ljava/lang/String;

    if-nez v1, :cond_4

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthtype:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v1, "AAUTHNAME"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthname:Ljava/lang/String;

    if-nez v1, :cond_5

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/ApplicationClass$Resource;->mAauthname:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    invoke-static {p0, p1, p2, v0}, Lcom/mediatek/omacp/parser/OmacpParserUtils;->handleOtherResourceParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V

    goto/16 :goto_0
.end method

.method private static handleValidityParams(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/NapdefClass$Validity;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    const-string v1, "Omacp/OmacpParserUtils"

    const-string v2, "OmacpParserUtils handleNapParameters VALIDITY size is 0."

    invoke-static {v1, v2}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "COUNTRY"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;->mCountry:Ljava/lang/String;

    if-nez v1, :cond_2

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;->mCountry:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v1, "NETWORK"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;->mNetwork:Ljava/lang/String;

    if-nez v1, :cond_3

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;->mNetwork:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v1, "SID"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;->mSid:Ljava/lang/String;

    if-nez v1, :cond_4

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;->mSid:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v1, "SOC"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;->mSoc:Ljava/lang/String;

    if-nez v1, :cond_5

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;->mSoc:Ljava/lang/String;

    goto :goto_0

    :cond_5
    const-string v1, "VALIDUNTIL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;

    iget-object v1, v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;->mValiduntil:Ljava/lang/String;

    if-nez v1, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;

    iput-object p1, v1, Lcom/mediatek/omacp/parser/NapdefClass$Validity;->mValiduntil:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public static removeInvalidApSettings(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mediatek/omacp/parser/ApplicationClass;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_1

    const-string v3, "Omacp/OmacpParserUtils"

    const-string v4, "OmacpParserUtils removeDuplicateApSettings apList is null."

    invoke-static {v3, v4}, Lcom/mediatek/omacp/utils/MTKlog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x0

    :cond_0
    return-object p0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v0, v3, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    if-nez v0, :cond_3

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v1, 0x1

    :goto_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/omacp/parser/ApplicationClass;

    iget-object v3, v3, Lcom/mediatek/omacp/parser/ApplicationClass;->mAppid:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "Omacp/OmacpParserUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OmacpParserUtils removeDuplicateApSettings duplicate application settings, will remove "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "element"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/omacp/utils/MTKlog;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v2, v2, -0x1

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
