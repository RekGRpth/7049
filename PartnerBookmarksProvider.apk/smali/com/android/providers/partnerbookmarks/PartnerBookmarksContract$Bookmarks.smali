.class public final Lcom/android/providers/partnerbookmarks/PartnerBookmarksContract$Bookmarks;
.super Ljava/lang/Object;
.source "PartnerBookmarksContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/partnerbookmarks/PartnerBookmarksContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Bookmarks"
.end annotation


# static fields
.field public static final BOOKMARK_PARENT_ROOT_ID:I = 0x0

.field public static final BOOKMARK_TYPE_BOOKMARK:I = 0x1

.field public static final BOOKMARK_TYPE_FOLDER:I = 0x2

.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/partnerbookmark"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/partnerbookmark"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CONTENT_URI_PARTNER_BOOKMARKS_FOLDER:Landroid/net/Uri;

.field public static final FAVICON:Ljava/lang/String; = "favicon"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final PARENT:Ljava/lang/String; = "parent"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TOUCHICON:Ljava/lang/String; = "touchicon"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final URL:Ljava/lang/String; = "url"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-object v0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "bookmarks"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksContract$Bookmarks;->CONTENT_URI:Landroid/net/Uri;

    sget-object v0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksContract$Bookmarks;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "folder"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksContract$Bookmarks;->CONTENT_URI_PARTNER_BOOKMARKS_FOLDER:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final buildFolderUri(J)Landroid/net/Uri;
    .locals 1
    .param p0    # J

    sget-object v0, Lcom/android/providers/partnerbookmarks/PartnerBookmarksContract$Bookmarks;->CONTENT_URI_PARTNER_BOOKMARKS_FOLDER:Landroid/net/Uri;

    invoke-static {v0, p0, p1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
