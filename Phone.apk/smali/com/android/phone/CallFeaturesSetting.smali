.class public Lcom/android/phone/CallFeaturesSetting;
.super Landroid/preference/PreferenceActivity;
.source "CallFeaturesSetting.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field public static final ACTION_ADD_VOICEMAIL:Ljava/lang/String; = "com.android.phone.CallFeaturesSetting.ADD_VOICEMAIL"

.field public static final ACTION_CONFIGURE_VOICEMAIL:Ljava/lang/String; = "com.android.phone.CallFeaturesSetting.CONFIGURE_VOICEMAIL"

.field private static final BUTTON_CALL_ADDITIONAL_KEY:Ljava/lang/String; = "button_more_expand_key"

.field private static final BUTTON_CALL_BAR_KEY:Ljava/lang/String; = "button_cb_expand_key"

.field private static final BUTTON_CALL_FWD_KEY:Ljava/lang/String; = "button_cf_expand_key"

.field private static final BUTTON_CALL_VOICEMAIL_KEY:Ljava/lang/String; = "button_voicemail_key"

.field private static final BUTTON_IP_PREFIX_KEY:Ljava/lang/String; = "button_ip_prefix_key"

.field private static final DBG:Z = true

.field static final DTMF_TONE_TYPE_LONG:I = 0x1

.field static final DTMF_TONE_TYPE_NORMAL:I = 0x0

.field public static final FWD_NUMBER_EXTRA:Ljava/lang/String; = "com.android.phone.ForwardingNumber"

.field public static final FWD_NUMBER_TIME_EXTRA:Ljava/lang/String; = "com.android.phone.ForwardingNumberTime"

.field public static final HAC_KEY:Ljava/lang/String; = "HACSetting"

.field public static final HAC_VAL_OFF:Ljava/lang/String; = "OFF"

.field public static final HAC_VAL_ON:Ljava/lang/String; = "ON"

.field private static final LOG_TAG:Ljava/lang/String; = "Settings/CallFeaturesSetting"

.field public static final SIGNOUT_EXTRA:Ljava/lang/String; = "com.android.phone.Signout"

.field public static final VM_NUMBER_EXTRA:Ljava/lang/String; = "com.android.phone.VoicemailNumber"


# instance fields
.field private isOnlyOneSim:Z

.field private mButtonCallAdditional:Landroid/preference/Preference;

.field private mButtonCallBar:Landroid/preference/Preference;

.field private mButtonCallFwd:Landroid/preference/Preference;

.field private mButtonIpPrefix:Landroid/preference/Preference;

.field private mButtonVoiceMail:Landroid/preference/Preference;

.field private mSimId:I

.field private preCfr:Lcom/mediatek/settings/PreCheckForRunning;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    iput v0, p0, Lcom/android/phone/CallFeaturesSetting;->mSimId:I

    iput-boolean v0, p0, Lcom/android/phone/CallFeaturesSetting;->isOnlyOneSim:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/phone/CallFeaturesSetting;->preCfr:Lcom/mediatek/settings/PreCheckForRunning;

    return-void
.end method

.method public static getVibrateWhenRinging(Landroid/content/Context;)Z
    .locals 4
    .param p0    # Landroid/content/Context;

    const/4 v1, 0x0

    const-string v2, "vibrator"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "vibrate_when_ringing"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f060020

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    const v2, 0x7f060011

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string v2, "button_more_expand_key"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonCallAdditional:Landroid/preference/Preference;

    const-string v2, "button_cf_expand_key"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonCallFwd:Landroid/preference/Preference;

    const-string v2, "button_cb_expand_key"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonCallBar:Landroid/preference/Preference;

    const-string v2, "button_voicemail_key"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonVoiceMail:Landroid/preference/Preference;

    const-string v2, "button_ip_prefix_key"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    iput-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonIpPrefix:Landroid/preference/Preference;

    const-string v2, "IP_DIAL"

    invoke-static {v2}, Lcom/android/phone/PhoneUtils;->isSupportFeature(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonIpPrefix:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonIpPrefix:Landroid/preference/Preference;

    :cond_0
    new-instance v2, Lcom/mediatek/settings/PreCheckForRunning;

    invoke-direct {v2, p0}, Lcom/mediatek/settings/PreCheckForRunning;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->preCfr:Lcom/mediatek/settings/PreCheckForRunning;

    invoke-static {}, Lcom/mediatek/settings/CallSettings;->isMultipleSim()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p0}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v3, :cond_1

    iput-boolean v3, p0, Lcom/android/phone/CallFeaturesSetting;->isOnlyOneSim:Z

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/provider/Telephony$SIMInfo;

    iget v2, v2, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    iput v2, p0, Lcom/android/phone/CallFeaturesSetting;->mSimId:I

    :cond_1
    iget-object v5, p0, Lcom/android/phone/CallFeaturesSetting;->preCfr:Lcom/mediatek/settings/PreCheckForRunning;

    iget-boolean v2, p0, Lcom/android/phone/CallFeaturesSetting;->isOnlyOneSim:Z

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    iput-boolean v2, v5, Lcom/mediatek/settings/PreCheckForRunning;->byPass:Z

    :goto_1
    return-void

    :cond_2
    move v2, v4

    goto :goto_0

    :cond_3
    iput-boolean v3, p0, Lcom/android/phone/CallFeaturesSetting;->isOnlyOneSim:Z

    iput v4, p0, Lcom/android/phone/CallFeaturesSetting;->mSimId:I

    iget-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->preCfr:Lcom/mediatek/settings/PreCheckForRunning;

    iget-boolean v5, p0, Lcom/android/phone/CallFeaturesSetting;->isOnlyOneSim:Z

    if-nez v5, :cond_4

    :goto_2
    iput-boolean v3, v2, Lcom/mediatek/settings/PreCheckForRunning;->byPass:Z

    goto :goto_1

    :cond_4
    move v3, v4

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    iget-object v0, p0, Lcom/android/phone/CallFeaturesSetting;->preCfr:Lcom/mediatek/settings/PreCheckForRunning;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/phone/CallFeaturesSetting;->preCfr:Lcom/mediatek/settings/PreCheckForRunning;

    invoke-virtual {v0}, Lcom/mediatek/settings/PreCheckForRunning;->deRegister()V

    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v0, 0x0

    return v0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 6
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Landroid/preference/Preference;

    const/4 v2, 0x0

    const/16 v5, 0x12e

    const/4 v1, 0x1

    invoke-static {}, Lcom/mediatek/settings/CallSettings;->isMultipleSim()Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonVoiceMail:Landroid/preference/Preference;

    if-ne p2, v3, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/settings/VoiceMailSetting;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->preCfr:Lcom/mediatek/settings/PreCheckForRunning;

    iget v3, p0, Lcom/android/phone/CallFeaturesSetting;->mSimId:I

    invoke-virtual {v2, v0, v3, v5}, Lcom/mediatek/settings/PreCheckForRunning;->checkToRun(Landroid/content/Intent;II)V

    :goto_0
    return v1

    :cond_0
    iget-object v3, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonCallFwd:Landroid/preference/Preference;

    if-ne p2, v3, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/android/phone/GsmUmtsCallForwardOptions;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->preCfr:Lcom/mediatek/settings/PreCheckForRunning;

    iget v3, p0, Lcom/android/phone/CallFeaturesSetting;->mSimId:I

    invoke-virtual {v2, v0, v3, v5}, Lcom/mediatek/settings/PreCheckForRunning;->checkToRun(Landroid/content/Intent;II)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonCallBar:Landroid/preference/Preference;

    if-ne p2, v3, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/settings/CallBarring;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->preCfr:Lcom/mediatek/settings/PreCheckForRunning;

    iget v3, p0, Lcom/android/phone/CallFeaturesSetting;->mSimId:I

    invoke-virtual {v2, v0, v3, v5}, Lcom/mediatek/settings/PreCheckForRunning;->checkToRun(Landroid/content/Intent;II)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonCallAdditional:Landroid/preference/Preference;

    if-ne p2, v3, :cond_3

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/android/phone/GsmUmtsAdditionalCallOptions;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->preCfr:Lcom/mediatek/settings/PreCheckForRunning;

    iget v3, p0, Lcom/android/phone/CallFeaturesSetting;->mSimId:I

    invoke-virtual {v2, v0, v3, v5}, Lcom/mediatek/settings/PreCheckForRunning;->checkToRun(Landroid/content/Intent;II)V

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonVoiceMail:Landroid/preference/Preference;

    if-ne p2, v3, :cond_5

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/settings/MultipleSimActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lcom/mediatek/settings/MultipleSimActivity;->initTitleName:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    sget-object v2, Lcom/mediatek/settings/MultipleSimActivity;->intentKey:Ljava/lang/String;

    const-string v3, "PreferenceScreen"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v2, Lcom/mediatek/settings/MultipleSimActivity;->targetClassKey:Ljava/lang/String;

    const-string v3, "com.mediatek.settings.VoiceMailSetting"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->preCfr:Lcom/mediatek/settings/PreCheckForRunning;

    iget v3, p0, Lcom/android/phone/CallFeaturesSetting;->mSimId:I

    invoke-virtual {v2, v0, v3, v5}, Lcom/mediatek/settings/PreCheckForRunning;->checkToRun(Landroid/content/Intent;II)V

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonCallFwd:Landroid/preference/Preference;

    if-ne p2, v3, :cond_6

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/settings/MultipleSimActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lcom/mediatek/settings/MultipleSimActivity;->initTitleName:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    sget-object v2, Lcom/mediatek/settings/MultipleSimActivity;->intentKey:Ljava/lang/String;

    const-string v3, "PreferenceScreen"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v2, Lcom/mediatek/settings/MultipleSimActivity;->targetClassKey:Ljava/lang/String;

    const-string v3, "com.android.phone.GsmUmtsCallForwardOptions"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->preCfr:Lcom/mediatek/settings/PreCheckForRunning;

    iget v3, p0, Lcom/android/phone/CallFeaturesSetting;->mSimId:I

    invoke-virtual {v2, v0, v3, v5}, Lcom/mediatek/settings/PreCheckForRunning;->checkToRun(Landroid/content/Intent;II)V

    goto/16 :goto_0

    :cond_6
    iget-object v3, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonCallBar:Landroid/preference/Preference;

    if-ne p2, v3, :cond_7

    const-string v2, "Settings/CallFeaturesSetting"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onPreferenceTreeClick , call barring key , simId= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/phone/CallFeaturesSetting;->mSimId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/settings/MultipleSimActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lcom/mediatek/settings/MultipleSimActivity;->initTitleName:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    sget-object v2, Lcom/mediatek/settings/MultipleSimActivity;->intentKey:Ljava/lang/String;

    const-string v3, "PreferenceScreen"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v2, Lcom/mediatek/settings/MultipleSimActivity;->targetClassKey:Ljava/lang/String;

    const-string v3, "com.mediatek.settings.CallBarring"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->preCfr:Lcom/mediatek/settings/PreCheckForRunning;

    iget v3, p0, Lcom/android/phone/CallFeaturesSetting;->mSimId:I

    invoke-virtual {v2, v0, v3, v5}, Lcom/mediatek/settings/PreCheckForRunning;->checkToRun(Landroid/content/Intent;II)V

    goto/16 :goto_0

    :cond_7
    iget-object v3, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonCallAdditional:Landroid/preference/Preference;

    if-ne p2, v3, :cond_8

    const-string v2, "Settings/CallFeaturesSetting"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onPreferenceTreeClick , call cost key , simId= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/phone/CallFeaturesSetting;->mSimId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/settings/MultipleSimActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lcom/mediatek/settings/MultipleSimActivity;->initTitleName:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    sget-object v2, Lcom/mediatek/settings/MultipleSimActivity;->intentKey:Ljava/lang/String;

    const-string v3, "PreferenceScreen"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v2, Lcom/mediatek/settings/MultipleSimActivity;->targetClassKey:Ljava/lang/String;

    const-string v3, "com.android.phone.GsmUmtsAdditionalCallOptions"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/phone/CallFeaturesSetting;->preCfr:Lcom/mediatek/settings/PreCheckForRunning;

    iget v3, p0, Lcom/android/phone/CallFeaturesSetting;->mSimId:I

    invoke-virtual {v2, v0, v3, v5}, Lcom/mediatek/settings/PreCheckForRunning;->checkToRun(Landroid/content/Intent;II)V

    goto/16 :goto_0

    :cond_8
    iget-object v3, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonIpPrefix:Landroid/preference/Preference;

    if-ne v3, p2, :cond_9

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/mediatek/settings/MultipleSimActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lcom/mediatek/settings/MultipleSimActivity;->initTitleName:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    sget-object v2, Lcom/mediatek/settings/MultipleSimActivity;->intentKey:Ljava/lang/String;

    const-string v3, "PreferenceScreen"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v2, Lcom/mediatek/settings/MultipleSimActivity;->targetClassKey:Ljava/lang/String;

    const-string v3, "com.mediatek.settings.IpPrefixPreference"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_9
    move v1, v2

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-boolean v1, p0, Lcom/android/phone/CallFeaturesSetting;->isOnlyOneSim:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/android/phone/CallFeaturesSetting;->mSimId:I

    invoke-static {v1}, Lcom/mediatek/settings/CallSettings;->isRadioOn(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonCallAdditional:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonCallFwd:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonCallBar:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonVoiceMail:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/android/phone/CallFeaturesSetting;->mButtonIpPrefix:Landroid/preference/Preference;

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_0
    return-void
.end method
