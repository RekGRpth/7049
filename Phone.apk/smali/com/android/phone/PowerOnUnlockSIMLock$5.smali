.class Lcom/android/phone/PowerOnUnlockSIMLock$5;
.super Landroid/os/Handler;
.source "PowerOnUnlockSIMLock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/PowerOnUnlockSIMLock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/PowerOnUnlockSIMLock;


# direct methods
.method constructor <init>(Lcom/android/phone/PowerOnUnlockSIMLock;)V
    .locals 0

    iput-object p1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 16
    .param p1    # Landroid/os/Message;

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v10, Landroid/os/AsyncResult;

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    const-string v1, "PowerOnUnlockSIMLock "

    const-string v2, "[mHandler][UNLOCK_ICC_SML_COMPLETE]+"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, v10, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_2

    const-string v1, "PowerOnUnlockSIMLock "

    const-string v2, "[mHandler][UNLOCK_ICC_SML_COMPLETE][GEMINI Card]+"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v15

    check-cast v15, Lcom/android/internal/telephony/gemini/GeminiPhone;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v1, v1, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMEUnlockNo:I

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-static {v1}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$100(Lcom/android/phone/PowerOnUnlockSIMLock;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x6e

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v8

    const/4 v1, 0x0

    invoke-virtual {v15, v1}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v1

    invoke-static {}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$200()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/android/internal/telephony/IccCard;->QueryIccNetworkLock(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-static {v1}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$300(Lcom/android/phone/PowerOnUnlockSIMLock;)Landroid/widget/TextView;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const v3, 0x7f0d0079

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v1, v1, Lcom/android/phone/PowerOnUnlockSIMLock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v1, v1, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMEUnlockNo:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-static {v1}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$100(Lcom/android/phone/PowerOnUnlockSIMLock;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x6e

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v8

    const/4 v1, 0x1

    invoke-virtual {v15, v1}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v1

    invoke-static {}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$200()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/android/internal/telephony/IccCard;->QueryIccNetworkLock(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_1

    :cond_2
    const-string v1, "PowerOnUnlockSIMLock "

    const-string v2, "[mHandler][GEMINI]+"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "PowerOnUnlockSIMLock "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[mHandler][iSIMMEUnlockNo] : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v3, v3, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMEUnlockNo:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "PowerOnUnlockSIMLock "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[mHandler][lockCategory] : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$200()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v2, v2, Lcom/android/phone/PowerOnUnlockSIMLock;->SIM1MELockStatus:[I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v3, v3, Lcom/android/phone/PowerOnUnlockSIMLock;->SIM2MELockStatus:[I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v4, v4, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMEUnlockNo:I

    invoke-static {v2, v3, v4}, Lcom/android/phone/PowerOnSetupUnlockSIMLock;->resetISIMMELockStatus([I[II)I

    move-result v2

    iput v2, v1, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMELockStatus:I

    const-string v1, "PowerOnUnlockSIMLock "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[mHandler][UNLOCK_ICC_SML_COMPLETE][iSIMMELockStatus][new] : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v3, v3, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMELockStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v1, v1, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMELockStatus:I

    if-eqz v1, :cond_3

    new-instance v14, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const-class v2, Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-direct {v14, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    const-string v1, "Phone.GEMINI_SIM_ID_KEY"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v2, v2, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMEUnlockNo:I

    invoke-virtual {v12, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "SIMMELOCKSTATUS"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v2, v2, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMELockStatus:I

    invoke-virtual {v12, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v14, v12}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v1, v14}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v1, v1, Lcom/android/phone/PowerOnUnlockSIMLock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v1, v1, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMEUnlockNo:I

    if-nez v1, :cond_5

    sget-object v1, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$400(Lcom/android/phone/PowerOnUnlockSIMLock;II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v1, v1, Lcom/android/phone/PowerOnUnlockSIMLock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    sget-object v1, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    new-instance v13, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const-class v2, Lcom/android/phone/PowerOnSetupUnlockSIMLock;

    invoke-direct {v13, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    const-string v1, "Phone.GEMINI_SIM_ID_KEY"

    const/4 v2, 0x1

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v13, v11}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v1, v13}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    const-string v1, "PowerOnUnlockSIMLock "

    const-string v2, "[mHandler][UNLOCK_ICC_SML_COMPLETE][SIM1][Finished]"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    sget-object v1, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput v3, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const/4 v2, 0x1

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$400(Lcom/android/phone/PowerOnUnlockSIMLock;II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v1, v1, Lcom/android/phone/PowerOnUnlockSIMLock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    sget-object v1, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    new-instance v13, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const-class v2, Lcom/android/phone/PowerOnSetupUnlockSIMLock;

    invoke-direct {v13, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    const-string v1, "Phone.GEMINI_SIM_ID_KEY"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v13, v11}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v1, v13}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    const-string v1, "PowerOnUnlockSIMLock "

    const-string v2, "[mHandler][UNLOCK_ICC_SML_COMPLETE][SIM2][Finished]"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_1
    const-string v1, "PowerOnUnlockSIMLock "

    const-string v2, "[mHandler][UNLOCK_ICC_SML_QUERYLEFTTIMES]+"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, v10, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_7

    :goto_2
    const-string v1, "PowerOnUnlockSIMLock "

    const-string v2, "[mHandler][UNLOCK_ICC_SML_QUERYLEFTTIMES]-"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    iget-object v1, v10, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v1, [I

    move-object v9, v1

    check-cast v9, [I

    const/4 v1, 0x2

    aget v1, v9, v1

    if-lez v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const/4 v2, 0x2

    aget v2, v9, v2

    iput v2, v1, Lcom/android/phone/PowerOnUnlockSIMLock;->mPwdLeftChances:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-static {v1}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$500(Lcom/android/phone/PowerOnUnlockSIMLock;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const v4, 0x7f0d0073

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v3, v3, Lcom/android/phone/PowerOnUnlockSIMLock;->mPwdLeftChances:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v1, "PowerOnUnlockSIMLock "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[mHandler][UNLOCK_ICC_SML_QUERYLEFTTIMES][query Left times]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v3, v3, Lcom/android/phone/PowerOnUnlockSIMLock;->mPwdLeftChances:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_8
    const-string v1, "PowerOnUnlockSIMLock "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[mHandler][UNLOCK_ICC_SML_QUERYLEFTTIMES][no chances to unlock current SIM Slot]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v3, v3, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMEUnlockNo:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const/16 v2, 0x1f7

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/android/phone/PowerOnUnlockSIMLock;->sendVerifyResult(IZ)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v1, v1, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMEUnlockNo:I

    if-nez v1, :cond_a

    sget-object v1, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v1, v2

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$602(I)I

    const-string v1, "PowerOnUnlockSIMLock "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[changed][arySIMLockStatus]: ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$400(Lcom/android/phone/PowerOnUnlockSIMLock;II)V

    sget-object v1, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_9

    new-instance v13, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const-class v2, Lcom/android/phone/PowerOnSetupUnlockSIMLock;

    invoke-direct {v13, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    const-string v1, "Phone.GEMINI_SIM_ID_KEY"

    const/4 v2, 0x1

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v13, v11}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v1, v13}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_9
    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v1, v1, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMEUnlockNo:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    sget-object v1, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput v3, v1, v2

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$702(I)I

    const-string v1, "PowerOnUnlockSIMLock "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[changed][arySIMLockStatus]: ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const/4 v2, 0x1

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$400(Lcom/android/phone/PowerOnUnlockSIMLock;II)V

    sget-object v1, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_9

    new-instance v13, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const-class v2, Lcom/android/phone/PowerOnSetupUnlockSIMLock;

    invoke-direct {v13, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    const-string v1, "Phone.GEMINI_SIM_ID_KEY"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v13, v11}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/phone/PowerOnUnlockSIMLock$5;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v1, v13}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_3

    :sswitch_data_0
    .sparse-switch
        0x6e -> :sswitch_1
        0x78 -> :sswitch_0
    .end sparse-switch
.end method
