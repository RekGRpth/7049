.class Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;
.super Ljava/lang/Object;
.source "PowerOnUnlockSIMLock.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/PowerOnUnlockSIMLock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TouchInput"
.end annotation


# instance fields
.field digit:I

.field private mCancelButton:Landroid/widget/TextView;

.field private mEight:Landroid/widget/TextView;

.field private mFive:Landroid/widget/TextView;

.field private mFour:Landroid/widget/TextView;

.field private mNine:Landroid/widget/TextView;

.field private mOk:Landroid/widget/TextView;

.field private mOne:Landroid/widget/TextView;

.field private mSeven:Landroid/widget/TextView;

.field private mSix:Landroid/widget/TextView;

.field private mThree:Landroid/widget/TextView;

.field private mTwo:Landroid/widget/TextView;

.field private mZero:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/android/phone/PowerOnUnlockSIMLock;


# direct methods
.method constructor <init>(Lcom/android/phone/PowerOnUnlockSIMLock;)V
    .locals 2

    iput-object p1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->digit:I

    const v0, 0x7f080048

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mZero:Landroid/widget/TextView;

    const v0, 0x7f08003e

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mOne:Landroid/widget/TextView;

    const v0, 0x7f08003f

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mTwo:Landroid/widget/TextView;

    const v0, 0x7f080040

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mThree:Landroid/widget/TextView;

    const v0, 0x7f080041

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mFour:Landroid/widget/TextView;

    const v0, 0x7f080042

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mFive:Landroid/widget/TextView;

    const v0, 0x7f080043

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mSix:Landroid/widget/TextView;

    const v0, 0x7f080044

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mSeven:Landroid/widget/TextView;

    const v0, 0x7f080045

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mEight:Landroid/widget/TextView;

    const v0, 0x7f080046

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mNine:Landroid/widget/TextView;

    const v0, 0x7f0800fd

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mCancelButton:Landroid/widget/TextView;

    const v0, 0x7f080068

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mOk:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mZero:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mOne:Landroid/widget/TextView;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mTwo:Landroid/widget/TextView;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mThree:Landroid/widget/TextView;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mFour:Landroid/widget/TextView;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mFive:Landroid/widget/TextView;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mSix:Landroid/widget/TextView;

    const-string v1, "6"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mSeven:Landroid/widget/TextView;

    const-string v1, "7"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mEight:Landroid/widget/TextView;

    const-string v1, "8"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mNine:Landroid/widget/TextView;

    const-string v1, "9"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mZero:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mOne:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mTwo:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mThree:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mFour:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mFive:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mSix:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mSeven:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mEight:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mNine:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mOk:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mCancelButton:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mZero:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mOne:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mTwo:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mThree:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mFour:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mFive:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mSix:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mSeven:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mEight:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mNine:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mOk:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mCancelButton:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private checkDigit(Landroid/view/View;)I
    .locals 2
    .param p1    # Landroid/view/View;

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mZero:Landroid/widget/TextView;

    if-ne p1, v1, :cond_1

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->digit:I

    const/4 v0, 0x7

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mOne:Landroid/widget/TextView;

    if-ne p1, v1, :cond_2

    const/4 v1, 0x1

    iput v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->digit:I

    const/16 v0, 0x8

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mTwo:Landroid/widget/TextView;

    if-ne p1, v1, :cond_3

    const/4 v1, 0x2

    iput v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->digit:I

    const/16 v0, 0x9

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mThree:Landroid/widget/TextView;

    if-ne p1, v1, :cond_4

    const/4 v1, 0x3

    iput v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->digit:I

    const/16 v0, 0xa

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mFour:Landroid/widget/TextView;

    if-ne p1, v1, :cond_5

    const/4 v1, 0x4

    iput v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->digit:I

    const/16 v0, 0xb

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mFive:Landroid/widget/TextView;

    if-ne p1, v1, :cond_6

    const/4 v1, 0x5

    iput v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->digit:I

    const/16 v0, 0xc

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mSix:Landroid/widget/TextView;

    if-ne p1, v1, :cond_7

    const/4 v1, 0x6

    iput v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->digit:I

    const/16 v0, 0xd

    goto :goto_0

    :cond_7
    iget-object v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mSeven:Landroid/widget/TextView;

    if-ne p1, v1, :cond_8

    const/4 v1, 0x7

    iput v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->digit:I

    const/16 v0, 0xe

    goto :goto_0

    :cond_8
    iget-object v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mEight:Landroid/widget/TextView;

    if-ne p1, v1, :cond_9

    const/16 v1, 0x8

    iput v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->digit:I

    const/16 v0, 0xf

    goto :goto_0

    :cond_9
    iget-object v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mNine:Landroid/widget/TextView;

    if-ne p1, v1, :cond_0

    const/16 v1, 0x9

    iput v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->digit:I

    const/16 v0, 0x10

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14
    .param p1    # Landroid/view/View;

    const/16 v13, 0x78

    const/16 v12, 0x8

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mCancelButton:Landroid/widget/TextView;

    if-ne p1, v6, :cond_5

    const-string v6, "PowerOnUnlockSIMLock "

    const-string v7, "[onClick][mCandelButton]+"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const/16 v7, 0x1f7

    invoke-virtual {v6, v7, v9}, Lcom/android/phone/PowerOnUnlockSIMLock;->sendVerifyResult(IZ)V

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMEUnlockNo:I

    if-nez v6, :cond_2

    sget-object v6, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    aput v9, v6, v9

    const-string v6, "PowerOnUnlockSIMLock "

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[changed][arySIMLockStatus]: ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    aget v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    aget v8, v8, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    aget v6, v6, v10

    if-ne v6, v11, :cond_0

    new-instance v2, Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const-class v7, Lcom/android/phone/PowerOnSetupUnlockSIMLock;

    invoke-direct {v2, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v6, "Phone.GEMINI_SIM_ID_KEY"

    invoke-virtual {v0, v6, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v6, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-static {v6, v9, v11}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$400(Lcom/android/phone/PowerOnUnlockSIMLock;II)V

    new-instance v5, Landroid/content/Intent;

    const-string v6, "action_melock_dismiss"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "simslot"

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v6, v5}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v6}, Landroid/app/Activity;->finish()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMEUnlockNo:I

    if-ne v6, v10, :cond_4

    sget-object v6, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    aput v9, v6, v10

    const-string v6, "PowerOnUnlockSIMLock "

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[changed][arySIMLockStatus]: ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    aget v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    aget v8, v8, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, Lcom/android/phone/PhoneApp;->arySIMLockStatus:[I

    aget v6, v6, v9

    if-ne v6, v11, :cond_3

    new-instance v2, Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const-class v7, Lcom/android/phone/PowerOnSetupUnlockSIMLock;

    invoke-direct {v2, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v6, "Phone.GEMINI_SIM_ID_KEY"

    invoke-virtual {v0, v6, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v6, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_3
    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-static {v6, v10, v11}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$400(Lcom/android/phone/PowerOnUnlockSIMLock;II)V

    new-instance v5, Landroid/content/Intent;

    const-string v6, "action_melock_dismiss"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "simslot"

    invoke-virtual {v5, v6, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v6, v5}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v6}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_4
    const-string v6, "PowerOnUnlockSIMLock "

    const-string v7, "[onClick][mCandelButton]-"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->mOk:Landroid/widget/TextView;

    if-ne p1, v6, :cond_6

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->mPwdDisplay:Landroid/widget/TextView;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v7, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v7, v7, Lcom/android/phone/PowerOnUnlockSIMLock;->mPwdDisplay:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->strPwd:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v7, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v7, v7, Lcom/android/phone/PowerOnUnlockSIMLock;->strPwd:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    iput v7, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->PwdLength:I

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->strPwd:Ljava/lang/String;

    invoke-static {v6}, Lcom/android/phone/PowerOnUnlockSIMLock;->bStringValid(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_9

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-static {v6}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$300(Lcom/android/phone/PowerOnUnlockSIMLock;)Landroid/widget/TextView;

    move-result-object v6

    iget-object v7, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const v8, 0x7f0d007a

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const/4 v7, 0x0

    iput-object v7, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->strPwd:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->mPwdDisplay:Landroid/widget/TextView;

    const-string v7, ""

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_6
    const-string v6, "PowerOnUnlockSIMLock "

    const-string v7, "[onClick][digit]"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->checkDigit(Landroid/view/View;)I

    move-result v3

    if-ltz v3, :cond_1

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->mPwdDisplay:Landroid/widget/TextView;

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v7, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v7, v7, Lcom/android/phone/PowerOnUnlockSIMLock;->mPwdDisplay:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->strPwd:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v7, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v7, v7, Lcom/android/phone/PowerOnUnlockSIMLock;->strPwd:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    iput v7, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->PwdLength:I

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->PwdLength:I

    if-ge v6, v12, :cond_7

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v8, v7, Lcom/android/phone/PowerOnUnlockSIMLock;->strPwd:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->digit:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v7, Lcom/android/phone/PowerOnUnlockSIMLock;->strPwd:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-virtual {v6, v3}, Lcom/android/phone/PowerOnUnlockSIMLock;->sendDownUpKeyEvents(I)V

    :cond_7
    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->mPwdDisplay:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v7, v7, Lcom/android/phone/PowerOnUnlockSIMLock;->strPwd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_8
    const/4 v6, -0x1

    iput v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->digit:I

    goto/16 :goto_0

    :cond_9
    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->PwdLength:I

    const/4 v7, 0x4

    if-lt v6, v7, :cond_c

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->PwdLength:I

    if-gt v6, v12, :cond_c

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    new-instance v7, Landroid/app/ProgressDialog;

    iget-object v8, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-direct {v7, v8}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v7, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->progressDialog:Landroid/app/ProgressDialog;

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->progressDialog:Landroid/app/ProgressDialog;

    iget-object v7, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const v8, 0x7f0d0087

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v6, v10}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v6, v9}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v6}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/16 v7, 0x7d9

    invoke-virtual {v6, v7}, Landroid/view/Window;->setType(I)V

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v6}, Landroid/app/Dialog;->show()V

    const-string v6, "PowerOnUnlockSIMLock "

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[onClick][mOK][Gemini Card][SetSIMLock]+"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v8, v8, Lcom/android/phone/PowerOnUnlockSIMLock;->mPwdDisplay:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v4

    check-cast v4, Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMEUnlockNo:I

    if-nez v6, :cond_b

    const-string v6, "PowerOnUnlockSIMLock "

    const-string v7, "[onClick][mOK][Gemini Card][SetSIMLock][SIM 1]+"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-static {v6}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$100(Lcom/android/phone/PowerOnUnlockSIMLock;)Landroid/os/Handler;

    move-result-object v6

    invoke-static {v6, v13}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v4, v9}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v6

    iget-object v7, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v7, v7, Lcom/android/phone/PowerOnUnlockSIMLock;->mPwdDisplay:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Lcom/android/internal/telephony/IccCard;->supplyNetworkDepersonalization(Ljava/lang/String;Landroid/os/Message;)V

    const-string v6, "PowerOnUnlockSIMLock "

    const-string v7, "[onClick][mOK][Gemini Card][SetSIMLock][SIM 1]-"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    :goto_2
    const-string v6, "PowerOnUnlockSIMLock "

    const-string v7, "[onClick][mOK][Gemini Card][SetSIMLock]-"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_b
    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget v6, v6, Lcom/android/phone/PowerOnUnlockSIMLock;->iSIMMEUnlockNo:I

    if-ne v6, v10, :cond_a

    const-string v6, "PowerOnUnlockSIMLock "

    const-string v7, "[onClick][mOK][Gemini Card][SetSIMLock][SIM 2]+"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-static {v6}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$100(Lcom/android/phone/PowerOnUnlockSIMLock;)Landroid/os/Handler;

    move-result-object v6

    invoke-static {v6, v13}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v4, v10}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getIccCardGemini(I)Lcom/android/internal/telephony/IccCard;

    move-result-object v6

    iget-object v7, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    iget-object v7, v7, Lcom/android/phone/PowerOnUnlockSIMLock;->mPwdDisplay:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Lcom/android/internal/telephony/IccCard;->supplyNetworkDepersonalization(Ljava/lang/String;Landroid/os/Message;)V

    const-string v6, "PowerOnUnlockSIMLock "

    const-string v7, "[onClick][mOK][Gemini Card][SetSIMLock][SIM 2]-"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_c
    iget-object v6, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-static {v6}, Lcom/android/phone/PowerOnUnlockSIMLock;->access$300(Lcom/android/phone/PowerOnUnlockSIMLock;)Landroid/widget/TextView;

    move-result-object v6

    iget-object v7, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    const v8, 0x7f0d007a

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method protected showAlertDialog(I)V
    .locals 4
    .param p1    # I

    const/16 v1, 0x78

    if-ne p1, v1, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->this$0:Lcom/android/phone/PowerOnUnlockSIMLock;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;->digit:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "OK"

    new-instance v3, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput$1;

    invoke-direct {v3, p0}, Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput$1;-><init>(Lcom/android/phone/PowerOnUnlockSIMLock$TouchInput;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x78
        :pswitch_0
    .end packed-switch
.end method
