.class public final Lcom/android/phone/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final btn_grid_padding:I = 0x7f0c0044

.field public static final button_cluster_side_padding:I = 0x7f0c000f

.field public static final call_banner_call_state_label_text_size:I = 0x7f0c0023

.field public static final call_banner_height:I = 0x7f0c0004

.field public static final call_banner_name_number_right_padding:I = 0x7f0c000a

.field public static final call_banner_side_padding:I = 0x7f0c0006

.field public static final call_banner_sim_indicator_padding:I = 0x7f0c0025

.field public static final call_banner_small_text_size:I = 0x7f0c0024

.field public static final call_banner_text_size:I = 0x7f0c0022

.field public static final call_banner_top_bottom_padding:I = 0x7f0c0007

.field public static final call_card_geo_description_max_width:I = 0x7f0c003f

.field public static final call_card_label_number_max_label:I = 0x7f0c0002

.field public static final call_card_main_margin:I = 0x7f0c0045

.field public static final call_card_name_number_max_name:I = 0x7f0c0000

.field public static final call_card_name_number_max_number:I = 0x7f0c0001

.field public static final call_card_operator_name_max:I = 0x7f0c0003

.field public static final call_card_second_margin:I = 0x7f0c0046

.field public static final call_card_title_margin:I = 0x7f0c0047

.field public static final call_state_label_height:I = 0x7f0c0021

.field public static final contact_photo_inset_height:I = 0x7f0c0011

.field public static final contact_photo_inset_left_margin:I = 0x7f0c0012

.field public static final contact_photo_inset_width:I = 0x7f0c0010

.field public static final dialpad_digits_text_size:I = 0x7f0c001b

.field public static final dialpad_horizontal_margin:I = 0x7f0c0018

.field public static final dialpad_vertical_margin:I = 0x7f0c0019

.field public static final dialpad_vertical_margin_dtmf:I = 0x7f0c001a

.field public static final dtmf_bottom_buttons_margin_bottom:I = 0x7f0c0069

.field public static final dtmf_bottom_buttons_margin_internal:I = 0x7f0c006b

.field public static final dtmf_bottom_buttons_margin_top:I = 0x7f0c006a

.field public static final dtmf_dial_button_height:I = 0x7f0c0071

.field public static final dtmf_dial_button_width:I = 0x7f0c0070

.field public static final dtmf_dialpad_margin_top:I = 0x7f0c0072

.field public static final dtmf_digits_height:I = 0x7f0c0074

.field public static final dtmf_digits_margin:I = 0x7f0c0073

.field public static final dtmf_digits_text_size:I = 0x7f0c0075

.field public static final dtmf_end_button_height:I = 0x7f0c006d

.field public static final dtmf_end_button_width:I = 0x7f0c006c

.field public static final dtmf_hide_button_height:I = 0x7f0c006f

.field public static final dtmf_hide_button_width:I = 0x7f0c006e

.field public static final dtmfdigits_height:I = 0x7f0c0048

.field public static final dtmfdigits_margin_bottom:I = 0x7f0c004a

.field public static final dtmfdigits_margin_top:I = 0x7f0c0049

.field public static final emergency_dialer_digits_height:I = 0x7f0c001c

.field public static final emergency_dialer_digits_margin_bottom:I = 0x7f0c0085

.field public static final emergency_dialer_digits_text_size:I = 0x7f0c0086

.field public static final extra_row_button_height:I = 0x7f0c000e

.field public static final extra_row_button_width:I = 0x7f0c000d

.field public static final in_call_button_height:I = 0x7f0c000b

.field public static final in_call_button_width:I = 0x7f0c0087

.field public static final in_call_end_button_height:I = 0x7f0c000c

.field public static final in_call_touch_ui_upper_margin:I = 0x7f0c0005

.field public static final incall_btn_end_call_height:I = 0x7f0c0054

.field public static final incall_btn_end_call_margin_top:I = 0x7f0c0055

.field public static final incall_btn_end_call_text_size:I = 0x7f0c0056

.field public static final incall_btn_end_call_width:I = 0x7f0c0053

.field public static final incall_btn_group_height:I = 0x7f0c004c

.field public static final incall_btn_group_margin_bottom:I = 0x7f0c004d

.field public static final incall_btn_group_margin_internal:I = 0x7f0c004e

.field public static final incall_btn_group_margin_left:I = 0x7f0c004f

.field public static final incall_btn_group_margin_right:I = 0x7f0c0050

.field public static final incall_btn_group_padding_top:I = 0x7f0c0052

.field public static final incall_btn_group_text_size:I = 0x7f0c0051

.field public static final incall_btn_group_width:I = 0x7f0c004b

.field public static final incall_card_button_margin_left:I = 0x7f0c0067

.field public static final incall_card_button_margin_top:I = 0x7f0c0068

.field public static final incall_card_label_text_size:I = 0x7f0c0064

.field public static final incall_card_margin_top:I = 0x7f0c0062

.field public static final incall_card_name_margin_bottom:I = 0x7f0c0066

.field public static final incall_card_name_margin_top:I = 0x7f0c0065

.field public static final incall_card_name_text_size:I = 0x7f0c0063

.field public static final incall_record_icon_size:I = 0x7f0c003c

.field public static final incall_status0_height:I = 0x7f0c0058

.field public static final incall_status_height:I = 0x7f0c0057

.field public static final incall_status_left_internal_margin:I = 0x7f0c005c

.field public static final incall_status_left_margin:I = 0x7f0c005d

.field public static final incall_status_right_internal_margin:I = 0x7f0c005e

.field public static final incall_status_right_margin:I = 0x7f0c005f

.field public static final incall_status_text_size0:I = 0x7f0c0060

.field public static final incall_status_text_size4:I = 0x7f0c0061

.field public static final incall_status_view0_margin_top:I = 0x7f0c0059

.field public static final incall_status_view1_margin_top:I = 0x7f0c005a

.field public static final incall_status_view4_margin_top:I = 0x7f0c005b

.field public static final incoming_call_2_name_margintop:I = 0x7f0c0041

.field public static final incoming_call_2_name_top_bottom_padding:I = 0x7f0c0008

.field public static final incoming_call_2_photo_height:I = 0x7f0c0042

.field public static final incoming_call_2_width:I = 0x7f0c0040

.field public static final incoming_call_button_height:I = 0x7f0c007f

.field public static final incoming_call_button_margin:I = 0x7f0c0082

.field public static final incoming_call_button_margin_bottom:I = 0x7f0c0080

.field public static final incoming_call_button_margin_internal:I = 0x7f0c0081

.field public static final incoming_call_button_width:I = 0x7f0c007e

.field public static final incoming_call_slidingbar_height:I = 0x7f0c0084

.field public static final incoming_call_slidingbar_width:I = 0x7f0c0083

.field public static final incoming_call_widget_asset_margin:I = 0x7f0c0020

.field public static final incoming_call_widget_circle_size:I = 0x7f0c001f

.field public static final manage_conference_button_end_height:I = 0x7f0c007b

.field public static final manage_conference_button_end_width:I = 0x7f0c007c

.field public static final manage_conference_button_height:I = 0x7f0c0078

.field public static final manage_conference_button_margin:I = 0x7f0c007a

.field public static final manage_conference_button_margin_bottom:I = 0x7f0c0079

.field public static final manage_conference_button_private_width:I = 0x7f0c007d

.field public static final manage_conference_button_width:I = 0x7f0c0077

.field public static final manage_conference_padding_top:I = 0x7f0c0076

.field public static final menu_height:I = 0x7f0c0043

.field public static final notification_icon_size:I = 0x7f0c001e

.field public static final otaactivate_layout_marginTop:I = 0x7f0c0015

.field public static final otabase_layout_marginTop:I = 0x7f0c0013

.field public static final otabase_minHeight:I = 0x7f0c0014

.field public static final otalistenprogress_layout_marginTop:I = 0x7f0c0016

.field public static final otasuccessfail_layout_marginTop:I = 0x7f0c0017

.field public static final provider_info_top_bottom_padding:I = 0x7f0c0009

.field public static final provider_overlay_top_margin:I = 0x7f0c001d

.field public static final reject_noti_text_size:I = 0x7f0c003e

.field public static final vt_incall_phone_number_max_width:I = 0x7f0c0027

.field public static final vt_incall_phone_number_max_width_when_recording:I = 0x7f0c0026

.field public static final vt_incall_screen_button_drawable_padding:I = 0x7f0c002f

.field public static final vt_incall_screen_button_height:I = 0x7f0c0032

.field public static final vt_incall_screen_button_interval:I = 0x7f0c0036

.field public static final vt_incall_screen_button_margin_out:I = 0x7f0c003d

.field public static final vt_incall_screen_button_margin_right:I = 0x7f0c0033

.field public static final vt_incall_screen_button_margin_top:I = 0x7f0c0034

.field public static final vt_incall_screen_button_padding_left:I = 0x7f0c0035

.field public static final vt_incall_screen_button_width:I = 0x7f0c0030

.field public static final vt_incall_screen_end_button_width:I = 0x7f0c0031

.field public static final vt_incall_screen_high_video_height:I = 0x7f0c0029

.field public static final vt_incall_screen_high_video_image_button_margin_edge:I = 0x7f0c002c

.field public static final vt_incall_screen_high_video_width:I = 0x7f0c0028

.field public static final vt_incall_screen_image_button_height:I = 0x7f0c002a

.field public static final vt_incall_screen_image_button_width:I = 0x7f0c002b

.field public static final vt_incall_screen_low_video_height:I = 0x7f0c0037

.field public static final vt_incall_screen_low_video_image_button_margin_edge:I = 0x7f0c003b

.field public static final vt_incall_screen_low_video_margin_left:I = 0x7f0c003a

.field public static final vt_incall_screen_low_video_margin_top:I = 0x7f0c0039

.field public static final vt_incall_screen_low_video_width:I = 0x7f0c0038

.field public static final vt_incall_screen_record_icon_margin_right:I = 0x7f0c002e

.field public static final vt_incall_screen_transparency_area_height:I = 0x7f0c002d


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
