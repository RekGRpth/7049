.class Lcom/android/phone/UnlockPIN2Lock$4;
.super Landroid/os/Handler;
.source "UnlockPIN2Lock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/UnlockPIN2Lock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/UnlockPIN2Lock;


# direct methods
.method constructor <init>(Lcom/android/phone/UnlockPIN2Lock;)V
    .locals 0

    iput-object p1, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1    # Landroid/os/Message;

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget-object v5, v5, Lcom/android/phone/UnlockPIN2Lock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->dismiss()V

    return-void

    :pswitch_1
    const-string v5, "UnlockPIN2Lock "

    const-string v6, "[mHandler][MSG2] +"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v5, :cond_1

    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v3

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget-object v6, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-static {v6, v8}, Lcom/android/phone/UnlockPIN2Lock;->access$100(Lcom/android/phone/UnlockPIN2Lock;I)I

    move-result v6

    iput v6, v5, Lcom/android/phone/UnlockPIN2Lock;->retryCount:I

    const-string v5, "UnlockPIN2Lock "

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[mHandler][MSG2][Single Card][PIN_REQUIRED][New Retry Count] : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget v7, v7, Lcom/android/phone/UnlockPIN2Lock;->retryCount:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget v5, v5, Lcom/android/phone/UnlockPIN2Lock;->retryCount:I

    if-lez v5, :cond_0

    new-instance v2, Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    const-class v6, Lcom/android/phone/UnlockPIN2Lock;

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v5, "PUKLEFTRETRIES"

    iget-object v6, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget v6, v6, Lcom/android/phone/UnlockPIN2Lock;->retryCount:I

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v5, "Phone.GEMINI_SIM_ID_KEY"

    sget v6, Lcom/android/phone/UnlockPIN2Lock;->iSIMMEUnlockNo:I

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v5, "WRONGCODE"

    const-string v6, "Wrong code."

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-virtual {v5, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_0
    new-instance v2, Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    const-class v6, Lcom/android/phone/UnlockPUK2Lock;

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-static {v5, v8}, Lcom/android/phone/UnlockPIN2Lock;->access$200(Lcom/android/phone/UnlockPIN2Lock;I)I

    move-result v4

    const-string v5, "PUKPHASE"

    const-string v6, "1"

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "PUKLEFTRETRIES"

    invoke-virtual {v1, v5, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v5, "Phone.GEMINI_SIM_ID_KEY"

    sget v6, Lcom/android/phone/UnlockPIN2Lock;->iSIMMEUnlockNo:I

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-virtual {v5, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_1
    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :pswitch_2
    const-string v5, "UnlockPIN2Lock "

    const-string v6, "[mHandler][MSG3] +"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v5, :cond_3

    const-string v5, "UnlockPIN2Lock "

    const-string v6, "[mHandler][MSG3][Gemini Card][SIM1][UnlockPinFail]+"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "UnlockPIN2Lock "

    const-string v6, "[mHandler][MSG3][Gemini Card][SIM1][Still PIN_REQUIRED]"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget-object v6, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-static {v6, v8}, Lcom/android/phone/UnlockPIN2Lock;->access$100(Lcom/android/phone/UnlockPIN2Lock;I)I

    move-result v6

    iput v6, v5, Lcom/android/phone/UnlockPIN2Lock;->retryCount:I

    const-string v5, "UnlockPIN2Lock "

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[mHandler][MSG3][Gemini Card][SIM1][New Retry Count] : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget v7, v7, Lcom/android/phone/UnlockPIN2Lock;->retryCount:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget v5, v5, Lcom/android/phone/UnlockPIN2Lock;->retryCount:I

    if-lez v5, :cond_2

    new-instance v2, Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    const-class v6, Lcom/android/phone/UnlockPIN2Lock;

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v5, "PUKLEFTRETRIES"

    iget-object v6, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget v6, v6, Lcom/android/phone/UnlockPIN2Lock;->retryCount:I

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v5, "Phone.GEMINI_SIM_ID_KEY"

    sget v6, Lcom/android/phone/UnlockPIN2Lock;->iSIMMEUnlockNo:I

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v5, "WRONGCODE"

    const-string v6, "Wrong code."

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-virtual {v5, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_2
    new-instance v2, Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    const-class v6, Lcom/android/phone/UnlockPUK2Lock;

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-static {v5, v8}, Lcom/android/phone/UnlockPIN2Lock;->access$200(Lcom/android/phone/UnlockPIN2Lock;I)I

    move-result v4

    const-string v5, "PUKPHASE"

    const-string v6, "1"

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "PUKLEFTRETRIES"

    invoke-virtual {v1, v5, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v5, "Phone.GEMINI_SIM_ID_KEY"

    sget v6, Lcom/android/phone/UnlockPIN2Lock;->iSIMMEUnlockNo:I

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-virtual {v5, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_3
    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget-object v5, v5, Lcom/android/phone/UnlockPIN2Lock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->dismiss()V

    goto/16 :goto_0

    :pswitch_3
    const-string v5, "UnlockPIN2Lock "

    const-string v6, "[mHandler][MSG4] +"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v5, :cond_5

    const-string v5, "UnlockPIN2Lock "

    const-string v6, "[mHandler][MSG3][Gemini Card][SIM2][PIN_REQUIRED][UnlockPinFail]+"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "UnlockPIN2Lock "

    const-string v6, "[mHandler][MSG3][Gemini Card][SIM2][PIN_REQUIRED][Still PIN_REQUIRED]"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget-object v6, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-static {v6, v9}, Lcom/android/phone/UnlockPIN2Lock;->access$100(Lcom/android/phone/UnlockPIN2Lock;I)I

    move-result v6

    iput v6, v5, Lcom/android/phone/UnlockPIN2Lock;->retryCount:I

    const-string v5, "UnlockPIN2Lock "

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[mHandler][MSG3][Gemini Card][SIM2][New Retry Count] : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget v7, v7, Lcom/android/phone/UnlockPIN2Lock;->retryCount:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget v5, v5, Lcom/android/phone/UnlockPIN2Lock;->retryCount:I

    if-lez v5, :cond_4

    new-instance v2, Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    const-class v6, Lcom/android/phone/UnlockPIN2Lock;

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v5, "PUKLEFTRETRIES"

    iget-object v6, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget v6, v6, Lcom/android/phone/UnlockPIN2Lock;->retryCount:I

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v5, "Phone.GEMINI_SIM_ID_KEY"

    sget v6, Lcom/android/phone/UnlockPIN2Lock;->iSIMMEUnlockNo:I

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v5, "WRONGCODE"

    const-string v6, "Wrong code."

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-virtual {v5, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget-object v5, v5, Lcom/android/phone/UnlockPIN2Lock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->dismiss()V

    goto/16 :goto_0

    :cond_4
    new-instance v2, Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    const-class v6, Lcom/android/phone/UnlockPUK2Lock;

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-static {v5, v9}, Lcom/android/phone/UnlockPIN2Lock;->access$200(Lcom/android/phone/UnlockPIN2Lock;I)I

    move-result v4

    const-string v5, "PUKPHASE"

    const-string v6, "1"

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "PUKLEFTRETRIES"

    invoke-virtual {v1, v5, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v5, "Phone.GEMINI_SIM_ID_KEY"

    sget v6, Lcom/android/phone/UnlockPIN2Lock;->iSIMMEUnlockNo:I

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-virtual {v5, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget-object v5, v5, Lcom/android/phone/UnlockPIN2Lock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->dismiss()V

    goto/16 :goto_0

    :cond_5
    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    iget-object v5, p0, Lcom/android/phone/UnlockPIN2Lock$4;->this$0:Lcom/android/phone/UnlockPIN2Lock;

    iget-object v5, v5, Lcom/android/phone/UnlockPIN2Lock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->dismiss()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x66
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
