.class Lcom/android/phone/UnlockPUKLock$TouchInput;
.super Ljava/lang/Object;
.source "UnlockPUKLock.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/UnlockPUKLock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TouchInput"
.end annotation


# instance fields
.field digit:I

.field private mCancelButton:Landroid/widget/TextView;

.field private mEight:Landroid/widget/TextView;

.field private mFive:Landroid/widget/TextView;

.field private mFour:Landroid/widget/TextView;

.field private mNine:Landroid/widget/TextView;

.field private mOk:Landroid/widget/TextView;

.field private mOne:Landroid/widget/TextView;

.field private mSeven:Landroid/widget/TextView;

.field private mSix:Landroid/widget/TextView;

.field private mThree:Landroid/widget/TextView;

.field private mTwo:Landroid/widget/TextView;

.field private mZero:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/android/phone/UnlockPUKLock;


# direct methods
.method constructor <init>(Lcom/android/phone/UnlockPUKLock;)V
    .locals 2

    iput-object p1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->digit:I

    const v0, 0x7f080048

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mZero:Landroid/widget/TextView;

    const v0, 0x7f08003e

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mOne:Landroid/widget/TextView;

    const v0, 0x7f08003f

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mTwo:Landroid/widget/TextView;

    const v0, 0x7f080040

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mThree:Landroid/widget/TextView;

    const v0, 0x7f080041

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mFour:Landroid/widget/TextView;

    const v0, 0x7f080042

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mFive:Landroid/widget/TextView;

    const v0, 0x7f080043

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mSix:Landroid/widget/TextView;

    const v0, 0x7f080044

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mSeven:Landroid/widget/TextView;

    const v0, 0x7f080045

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mEight:Landroid/widget/TextView;

    const v0, 0x7f080046

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mNine:Landroid/widget/TextView;

    const v0, 0x7f0800fd

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mCancelButton:Landroid/widget/TextView;

    const v0, 0x7f080068

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mOk:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mCancelButton:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x7

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mCancelButton:Landroid/widget/TextView;

    const/high16 v1, 0x41600000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    :cond_0
    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mZero:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mOne:Landroid/widget/TextView;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mTwo:Landroid/widget/TextView;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mThree:Landroid/widget/TextView;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mFour:Landroid/widget/TextView;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mFive:Landroid/widget/TextView;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mSix:Landroid/widget/TextView;

    const-string v1, "6"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mSeven:Landroid/widget/TextView;

    const-string v1, "7"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mEight:Landroid/widget/TextView;

    const-string v1, "8"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mNine:Landroid/widget/TextView;

    const-string v1, "9"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mZero:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mOne:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mTwo:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mThree:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mFour:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mFive:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mSix:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mSeven:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mEight:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mNine:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mOk:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mCancelButton:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mZero:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mOne:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mTwo:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mThree:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mFour:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mFive:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mSix:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mSeven:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mEight:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mNine:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mOk:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mCancelButton:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private checkDigit(Landroid/view/View;)I
    .locals 2
    .param p1    # Landroid/view/View;

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mZero:Landroid/widget/TextView;

    if-ne p1, v1, :cond_1

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->digit:I

    const/4 v0, 0x7

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mOne:Landroid/widget/TextView;

    if-ne p1, v1, :cond_2

    const/4 v1, 0x1

    iput v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->digit:I

    const/16 v0, 0x8

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mTwo:Landroid/widget/TextView;

    if-ne p1, v1, :cond_3

    const/4 v1, 0x2

    iput v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->digit:I

    const/16 v0, 0x9

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mThree:Landroid/widget/TextView;

    if-ne p1, v1, :cond_4

    const/4 v1, 0x3

    iput v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->digit:I

    const/16 v0, 0xa

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mFour:Landroid/widget/TextView;

    if-ne p1, v1, :cond_5

    const/4 v1, 0x4

    iput v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->digit:I

    const/16 v0, 0xb

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mFive:Landroid/widget/TextView;

    if-ne p1, v1, :cond_6

    const/4 v1, 0x5

    iput v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->digit:I

    const/16 v0, 0xc

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mSix:Landroid/widget/TextView;

    if-ne p1, v1, :cond_7

    const/4 v1, 0x6

    iput v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->digit:I

    const/16 v0, 0xd

    goto :goto_0

    :cond_7
    iget-object v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mSeven:Landroid/widget/TextView;

    if-ne p1, v1, :cond_8

    const/4 v1, 0x7

    iput v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->digit:I

    const/16 v0, 0xe

    goto :goto_0

    :cond_8
    iget-object v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mEight:Landroid/widget/TextView;

    if-ne p1, v1, :cond_9

    const/16 v1, 0x8

    iput v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->digit:I

    const/16 v0, 0xf

    goto :goto_0

    :cond_9
    iget-object v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mNine:Landroid/widget/TextView;

    if-ne p1, v1, :cond_0

    const/16 v1, 0x9

    iput v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->digit:I

    const/16 v0, 0x10

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const/4 v9, 0x4

    const/4 v6, 0x0

    const/4 v8, 0x0

    const v5, 0x7f0d007a

    const/16 v7, 0x8

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mCancelButton:Landroid/widget/TextView;

    if-ne p1, v3, :cond_1

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    const/16 v4, 0x1f6

    invoke-virtual {v3, v4, v6}, Lcom/android/phone/UnlockPUKLock;->sendVerifyResult(IZ)V

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->mOk:Landroid/widget/TextView;

    if-ne p1, v3, :cond_3

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->strPUKUnlockPhase:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->strPUKUnlockPhase:Ljava/lang/String;

    const-string v4, "1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_2
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->mPwdDisplay:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, v4, Lcom/android/phone/UnlockPUKLock;->mPwdDisplay:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, v4, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iput v4, v3, Lcom/android/phone/UnlockPUKLock;->PwdLength:I

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/phone/UnlockPUKLock;->bStringValid(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-static {v3}, Lcom/android/phone/UnlockPUKLock;->access$000(Lcom/android/phone/UnlockPUKLock;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    const v5, 0x7f0d007b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iput-object v8, v3, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->mPwdDisplay:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_2
    invoke-direct {p0, p1}, Lcom/android/phone/UnlockPUKLock$TouchInput;->checkDigit(Landroid/view/View;)I

    move-result v2

    if-ltz v2, :cond_0

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->mPwdDisplay:Landroid/widget/TextView;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, v4, Lcom/android/phone/UnlockPUKLock;->mPwdDisplay:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, v4, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iput v4, v3, Lcom/android/phone/UnlockPUKLock;->PwdLength:I

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget v3, v3, Lcom/android/phone/UnlockPUKLock;->PwdLength:I

    if-ge v3, v7, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v5, v4, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->digit:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    :cond_4
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->mPwdDisplay:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, v4, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    const/4 v3, -0x1

    iput v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->digit:I

    goto/16 :goto_0

    :cond_6
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget v3, v3, Lcom/android/phone/UnlockPUKLock;->PwdLength:I

    if-ne v3, v7, :cond_7

    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    const-class v4, Lcom/android/phone/UnlockPUKLock;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "PUKPHASE"

    const-string v4, "2"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "PUKLEFTRETRIES"

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget v4, v4, Lcom/android/phone/UnlockPUKLock;->retryCount:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "Phone.GEMINI_SIM_ID_KEY"

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget v4, v4, Lcom/android/phone/UnlockPUKLock;->iSIMMEUnlockNo:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "INPUTPUK"

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, v4, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-virtual {v3, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_7
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-static {v3}, Lcom/android/phone/UnlockPUKLock;->access$000(Lcom/android/phone/UnlockPUKLock;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    const v5, 0x7f0d007b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_8
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->strPUKUnlockPhase:Ljava/lang/String;

    const-string v4, "2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->mPwdDisplay:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, v4, Lcom/android/phone/UnlockPUKLock;->mPwdDisplay:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, v4, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iput v4, v3, Lcom/android/phone/UnlockPUKLock;->PwdLength:I

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/phone/UnlockPUKLock;->bStringValid(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-static {v3}, Lcom/android/phone/UnlockPUKLock;->access$000(Lcom/android/phone/UnlockPUKLock;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iput-object v8, v3, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->mPwdDisplay:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_9
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget v3, v3, Lcom/android/phone/UnlockPUKLock;->PwdLength:I

    if-lt v3, v9, :cond_a

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget v3, v3, Lcom/android/phone/UnlockPUKLock;->PwdLength:I

    if-gt v3, v7, :cond_a

    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    const-class v4, Lcom/android/phone/UnlockPUKLock;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "PUKPHASE"

    const-string v4, "3"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "PUKLEFTRETRIES"

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget v4, v4, Lcom/android/phone/UnlockPUKLock;->retryCount:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "Phone.GEMINI_SIM_ID_KEY"

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget v4, v4, Lcom/android/phone/UnlockPUKLock;->iSIMMEUnlockNo:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "INPUTPUK"

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, v4, Lcom/android/phone/UnlockPUKLock;->strPUK:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "INPUTORIPIN"

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, v4, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-virtual {v3, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_a
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-static {v3}, Lcom/android/phone/UnlockPUKLock;->access$000(Lcom/android/phone/UnlockPUKLock;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_b
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->strPUKUnlockPhase:Ljava/lang/String;

    const-string v4, "3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->mPwdDisplay:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, v4, Lcom/android/phone/UnlockPUKLock;->mPwdDisplay:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, v4, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iput v4, v3, Lcom/android/phone/UnlockPUKLock;->PwdLength:I

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/phone/UnlockPUKLock;->bStringValid(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-static {v3}, Lcom/android/phone/UnlockPUKLock;->access$000(Lcom/android/phone/UnlockPUKLock;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iput-object v8, v3, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->mPwdDisplay:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_c
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget v3, v3, Lcom/android/phone/UnlockPUKLock;->PwdLength:I

    if-lt v3, v9, :cond_f

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget v3, v3, Lcom/android/phone/UnlockPUKLock;->PwdLength:I

    if-gt v3, v7, :cond_f

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->strOriPIN:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, v4, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    const-class v4, Lcom/android/phone/UnlockPUKLock;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "PUKPHASE"

    const-string v4, "2"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "PUKLEFTRETRIES"

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget v4, v4, Lcom/android/phone/UnlockPUKLock;->retryCount:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "Phone.GEMINI_SIM_ID_KEY"

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget v4, v4, Lcom/android/phone/UnlockPUKLock;->iSIMMEUnlockNo:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "WRONGCODE"

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    const v5, 0x7f0d0086

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "INPUTPUK"

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, v4, Lcom/android/phone/UnlockPUKLock;->strPUK:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-virtual {v3, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_d
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    new-instance v4, Landroid/app/ProgressDialog;

    iget-object v5, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-direct {v4, v5}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v4, v3, Lcom/android/phone/UnlockPUKLock;->progressDialog:Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->progressDialog:Landroid/app/ProgressDialog;

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    const v5, 0x7f0d0087

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v6}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x7d9

    invoke-virtual {v3, v4}, Landroid/view/Window;->setType(I)V

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v3, v3, Lcom/android/phone/UnlockPUKLock;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    const-string v3, "UnlockPUKLock "

    const-string v4, "[onClick][OK Button][Start Unlock Process]"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget v3, v3, Lcom/android/phone/UnlockPUKLock;->iSIMMEUnlockNo:I

    if-nez v3, :cond_e

    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget v4, v4, Lcom/android/phone/UnlockPUKLock;->iSIMMEUnlockNo:I

    iget-object v5, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v5, v5, Lcom/android/phone/UnlockPUKLock;->strPUK:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v6, v6, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/phone/UnlockPUKLock;->unlockPUK(ILjava/lang/String;Ljava/lang/String;)V

    :goto_5
    const-string v3, "UnlockPUKLock "

    const-string v4, "[onClick][OK Button][Finish Unlock Process]"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_e
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget v4, v4, Lcom/android/phone/UnlockPUKLock;->iSIMMEUnlockNo:I

    iget-object v5, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v5, v5, Lcom/android/phone/UnlockPUKLock;->strPUK:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    iget-object v6, v6, Lcom/android/phone/UnlockPUKLock;->strPwd:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/phone/UnlockPUKLock;->unlockPUK(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_f
    iget-object v3, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-static {v3}, Lcom/android/phone/UnlockPUKLock;->access$000(Lcom/android/phone/UnlockPUKLock;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method protected showAlertDialog(I)V
    .locals 4
    .param p1    # I

    const/16 v1, 0x78

    if-ne p1, v1, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->this$0:Lcom/android/phone/UnlockPUKLock;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/android/phone/UnlockPUKLock$TouchInput;->digit:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "OK"

    new-instance v3, Lcom/android/phone/UnlockPUKLock$TouchInput$1;

    invoke-direct {v3, p0}, Lcom/android/phone/UnlockPUKLock$TouchInput$1;-><init>(Lcom/android/phone/UnlockPUKLock$TouchInput;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x78
        :pswitch_0
    .end packed-switch
.end method
