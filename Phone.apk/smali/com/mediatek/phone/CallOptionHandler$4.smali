.class Lcom/mediatek/phone/CallOptionHandler$4;
.super Ljava/lang/Object;
.source "CallOptionHandler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/phone/CallOptionHandler;->onMakeCall(Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/phone/CallOptionHandler;

.field final synthetic val$args:Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;


# direct methods
.method constructor <init>(Lcom/mediatek/phone/CallOptionHandler;Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/phone/CallOptionHandler$4;->this$0:Lcom/mediatek/phone/CallOptionHandler;

    iput-object p2, p0, Lcom/mediatek/phone/CallOptionHandler$4;->val$args:Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.android.phone"

    const-string v2, "com.mediatek.settings.VoiceMailSetting"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v1, "simId"

    iget-object v2, p0, Lcom/mediatek/phone/CallOptionHandler$4;->val$args:Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;

    iget-wide v2, v2, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->id:J

    long-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/mediatek/phone/CallOptionHandler$4;->this$0:Lcom/mediatek/phone/CallOptionHandler;

    iget-object v1, v1, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
