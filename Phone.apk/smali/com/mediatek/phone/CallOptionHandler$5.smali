.class Lcom/mediatek/phone/CallOptionHandler$5;
.super Ljava/lang/Object;
.source "CallOptionHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/phone/CallOptionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/phone/CallOptionHandler;


# direct methods
.method constructor <init>(Lcom/mediatek/phone/CallOptionHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/phone/CallOptionHandler$5;->this$0:Lcom/mediatek/phone/CallOptionHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v3, p0, Lcom/mediatek/phone/CallOptionHandler$5;->this$0:Lcom/mediatek/phone/CallOptionHandler;

    iget-object v3, v3, Lcom/mediatek/phone/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    invoke-virtual {v3}, Lcom/mediatek/CellConnService/CellConnMgr;->getResult()I

    move-result v1

    iget-object v3, p0, Lcom/mediatek/phone/CallOptionHandler$5;->this$0:Lcom/mediatek/phone/CallOptionHandler;

    iget-object v3, v3, Lcom/mediatek/phone/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    invoke-virtual {v3}, Lcom/mediatek/CellConnService/CellConnMgr;->getPreferSlot()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/phone/CallOptionHandler$5;->this$0:Lcom/mediatek/phone/CallOptionHandler;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "run, result = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " slot = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/phone/CallOptionHandler$5;->this$0:Lcom/mediatek/phone/CallOptionHandler;

    invoke-static {v3, v1, v2}, Lcom/mediatek/phone/CallOptionHandler;->access$000(Lcom/mediatek/phone/CallOptionHandler;II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/mediatek/phone/CallOptionHandler$5;->this$0:Lcom/mediatek/phone/CallOptionHandler;

    invoke-virtual {v3}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete()V

    :cond_0
    return-void
.end method
