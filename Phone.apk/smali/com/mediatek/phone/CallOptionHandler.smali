.class public Lcom/mediatek/phone/CallOptionHandler;
.super Ljava/lang/Object;
.source "CallOptionHandler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Lcom/mediatek/phone/CallOptionHelper$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/phone/CallOptionHandler$OnHandleCallOption;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CallOptionHandler"


# instance fields
.field protected mApp:Lcom/android/phone/PhoneApp;

.field protected mAssociateSimMissingArgs:Lcom/mediatek/phone/CallOptionHelper$AssociateSimMissingArgs;

.field protected mAssociateSimMissingClicked:Z

.field protected mCallOptionHelper:Lcom/mediatek/phone/CallOptionHelper;

.field protected mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

.field protected mClicked:Z

.field protected mContext:Landroid/content/Context;

.field protected mDialogs:[Landroid/app/Dialog;

.field protected mIntent:Landroid/content/Intent;

.field protected mNumber:Ljava/lang/String;

.field protected mOnHandleCallOption:Lcom/mediatek/phone/CallOptionHandler$OnHandleCallOption;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field protected mReason:I

.field private mRunnable:Ljava/lang/Runnable;

.field private mServiceStateExt:Lcom/mediatek/common/telephony/IServiceStateExt;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [Landroid/app/Dialog;

    iput-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    iput v2, p0, Lcom/mediatek/phone/CallOptionHandler;->mReason:I

    new-instance v0, Lcom/mediatek/phone/CallOptionHandler$5;

    invoke-direct {v0, p0}, Lcom/mediatek/phone/CallOptionHandler$5;-><init>(Lcom/mediatek/phone/CallOptionHandler;)V

    iput-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mRunnable:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/mediatek/phone/CallOptionHelper;->getInstance(Landroid/content/Context;)Lcom/mediatek/phone/CallOptionHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mCallOptionHelper:Lcom/mediatek/phone/CallOptionHelper;

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mCallOptionHelper:Lcom/mediatek/phone/CallOptionHelper;

    invoke-virtual {v0, p0}, Lcom/mediatek/phone/CallOptionHelper;->setCallback(Lcom/mediatek/phone/CallOptionHelper$Callback;)V

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mApp:Lcom/android/phone/PhoneApp;

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mApp:Lcom/android/phone/PhoneApp;

    iget-object v0, v0, Lcom/android/phone/PhoneApp;->cellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    iput-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    const-class v0, Lcom/mediatek/common/telephony/IServiceStateExt;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/mediatek/common/MediatekClassFactory;->createInstance(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/common/telephony/IServiceStateExt;

    iput-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mServiceStateExt:Lcom/mediatek/common/telephony/IServiceStateExt;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/phone/CallOptionHandler;II)Z
    .locals 1
    .param p0    # Lcom/mediatek/phone/CallOptionHandler;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/phone/CallOptionHandler;->afterCheckSIMStatus(II)Z

    move-result v0

    return v0
.end method

.method private afterCheckSIMStatus(II)Z
    .locals 12
    .param p1    # I
    .param p2    # I

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "afterCheckSIMStatus, result = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " slot = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    const/4 v8, 0x4

    if-eq p1, v8, :cond_0

    const/4 v8, 0x1

    :goto_0
    return v8

    :cond_0
    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mediatek/phone/SIMInfoWrapper;->getInsertedSimCount()I

    move-result v8

    if-nez v8, :cond_1

    const/4 v3, 0x1

    :goto_1
    iget-object v8, p0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v9, "com.android.phone.extra.video"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v8, p0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v9, "com.android.phone.extra.ip"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_3

    if-nez v3, :cond_3

    invoke-direct {p0, p2}, Lcom/mediatek/phone/CallOptionHandler;->queryIPPrefix(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    new-instance v1, Landroid/content/Intent;

    const-string v8, "com.android.phone.MAIN"

    invoke-direct {v1, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v8, "com.android.phone"

    const-class v9, Lcom/mediatek/settings/CallSettings;

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v8

    invoke-virtual {v8, p2}, Lcom/mediatek/phone/SIMInfoWrapper;->getSimInfoBySlot(I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v5

    const-string v8, "simId"

    iget-wide v9, v5, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    invoke-virtual {v1, v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v8

    const v9, 0x7f0d00b1

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    iget-object v8, p0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    const/4 v8, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    iget-object v8, p0, Lcom/mediatek/phone/CallOptionHandler;->mNumber:Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v9, "android.phone.extra.ACTUAL_NUMBER_TO_DIAL"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/mediatek/phone/CallOptionHandler;->mNumber:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    const-string v8, "voicemail:"

    iget-object v9, p0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    invoke-virtual {v9}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    const-string v9, "phone"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/telephony/TelephonyManager;

    invoke-virtual {v6, p2}, Landroid/telephony/TelephonyManager;->getVoiceMailNumberGemini(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    new-instance v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;

    iget-object v8, p0, Lcom/mediatek/phone/CallOptionHandler;->mCallOptionHelper:Lcom/mediatek/phone/CallOptionHelper;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, v8}, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;-><init>(Lcom/mediatek/phone/CallOptionHelper;)V

    const/4 v8, 0x7

    iput v8, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->reason:I

    const/4 v8, 0x2

    iput v8, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->type:I

    int-to-long v8, p2

    iput-wide v8, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->id:J

    invoke-virtual {p0, v0}, Lcom/mediatek/phone/CallOptionHandler;->onMakeCall(Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;)V

    const/4 v8, 0x0

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v6, p2}, Landroid/telephony/TelephonyManager;->getVoiceMailNumberGemini(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v9, "android.phone.extra.ACTUAL_NUMBER_TO_DIAL"

    invoke-virtual {v8, v9, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_5
    iget-object v8, p0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v9, "com.android.phone.extra.slot"

    const/4 v10, -0x1

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "afterCheckSIMStatus, oldSolt = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    const/4 v8, -0x1

    if-eq v4, v8, :cond_6

    if-eq p2, v4, :cond_6

    iget-object v8, p0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v9, "com.android.phone.extra.slot"

    invoke-virtual {v8, v9, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_6
    const/4 v8, 0x1

    iget-object v9, p0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    invoke-virtual {p0, v8, v9}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete(ZLandroid/content/Intent;)V

    const/4 v8, 0x0

    goto/16 :goto_0
.end method

.method private dismissProgressIndication()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method private isRoamingNeeded(I)Z
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isRoamingNeeded slot = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isRoamingNeeded = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "gsm.roaming.indicator.needed.2"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    const-string v0, "gsm.roaming.indicator.needed.2"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isRoamingNeeded = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "gsm.roaming.indicator.needed"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    const-string v0, "gsm.roaming.indicator.needed"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private needToCheckSIMStatus(I)Z
    .locals 5
    .param p1    # I

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v3

    iget-object v0, v3, Lcom/android/phone/PhoneApp;->phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

    if-ltz p1, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/phone/PhoneInterfaceManager;->isSimInsert(I)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    const-string v2, "the sim not insert, bail out!"

    invoke-virtual {p0, v2}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return v1

    :cond_2
    invoke-virtual {v0, p1}, Lcom/android/phone/PhoneInterfaceManager;->isRadioOnGemini(I)Z

    move-result v3

    if-nez v3, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/telephony/TelephonyManager;->getSimStateGemini(I)I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_4

    invoke-direct {p0, p1}, Lcom/mediatek/phone/CallOptionHandler;->roamingRequest(I)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method private newSipCallOptionHandlerIntent(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3
    .param p1    # Landroid/content/Intent;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.phone.SIP_SELECT_PHONE"

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v1, p0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    const-class v2, Lcom/android/phone/SipCallOptionHandler;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "android.phone.extra.NEW_CALL_INTENT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-object v0
.end method

.method private queryIPPrefix(I)Ljava/lang/String;
    .locals 6
    .param p1    # I

    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/mediatek/phone/SIMInfoWrapper;->getSimInfoBySlot(I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ipprefix"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, v3, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "queryIPPrefix, ipPrefix = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    return-object v1
.end method

.method private roamingRequest(I)Z
    .locals 6
    .param p1    # I

    const/4 v2, 0x0

    const/4 v5, -0x1

    const/4 v1, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "roamingRequest slot = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/telephony/TelephonyManager;->isNetworkRoamingGemini(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "roamingRequest slot = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is roaming"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "roaming_reminder_mode_setting"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0, p1}, Lcom/mediatek/phone/CallOptionHandler;->isRoamingNeeded(I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v2, "roamingRequest reminder once and need to indicate"

    invoke-virtual {p0, v2}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "roamingRequest slot = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is not roaming"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    move v1, v2

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "roaming_reminder_mode_setting"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v1, v3, :cond_2

    const-string v2, "roamingRequest reminder always"

    invoke-virtual {p0, v2}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v1, "roamingRequest result = false"

    invoke-virtual {p0, v1}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    move v1, v2

    goto :goto_0
.end method

.method private showProgressIndication()V
    .locals 3

    const-string v0, "showProgressIndication(searching network message )"

    invoke-virtual {p0, v0}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/phone/CallOptionHandler;->dismissProgressIndication()V

    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0205

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method


# virtual methods
.method protected handleCallOptionComplete()V
    .locals 3

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mOnHandleCallOption:Lcom/mediatek/phone/CallOptionHandler$OnHandleCallOption;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mOnHandleCallOption:Lcom/mediatek/phone/CallOptionHandler$OnHandleCallOption;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/mediatek/phone/CallOptionHandler$OnHandleCallOption;->onHandleCallOption(ZLandroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method protected handleCallOptionComplete(ZLandroid/content/Intent;)V
    .locals 1
    .param p1    # Z
    .param p2    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mOnHandleCallOption:Lcom/mediatek/phone/CallOptionHandler$OnHandleCallOption;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mOnHandleCallOption:Lcom/mediatek/phone/CallOptionHandler$OnHandleCallOption;

    invoke-interface {v0, p1, p2}, Lcom/mediatek/phone/CallOptionHandler$OnHandleCallOption;->onHandleCallOption(ZLandroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "CallOptionHandler"

    invoke-static {v0, p1}, Lcom/mediatek/phone/PhoneLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 13
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x0

    const/4 v8, -0x2

    const/4 v9, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onClick, dialog = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " which = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/phone/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    aget-object v6, v6, v12

    if-ne p1, v6, :cond_6

    move-object v0, p1

    check-cast v0, Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, p2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onClick, slot = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    if-ne v5, v8, :cond_3

    iget-object v6, p0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v7, "com.android.phone.extra.ip"

    invoke-virtual {v6, v7, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v6

    const v7, 0x7f0d00b2

    invoke-static {v6, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete()V

    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iput-boolean v9, p0, Lcom/mediatek/phone/CallOptionHandler;->mClicked:Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/mediatek/phone/CallOptionHandler;->startSipCallOptionHandler()V

    invoke-virtual {p0}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v7, "com.android.phone.extra.slot"

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-direct {p0, v5}, Lcom/mediatek/phone/CallOptionHandler;->needToCheckSIMStatus(I)Z

    move-result v6

    if-eqz v6, :cond_5

    if-ltz v5, :cond_4

    iget-object v6, p0, Lcom/mediatek/phone/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    const/16 v7, 0x132

    iget-object v8, p0, Lcom/mediatek/phone/CallOptionHandler;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v6, v5, v7, v8}, Lcom/mediatek/CellConnService/CellConnMgr;->handleCellConn(IILjava/lang/Runnable;)I

    move-result v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "result = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/phone/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    if-ne v4, v9, :cond_0

    invoke-direct {p0}, Lcom/mediatek/phone/CallOptionHandler;->showProgressIndication()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_0

    :cond_5
    invoke-direct {p0, v11, v5}, Lcom/mediatek/phone/CallOptionHandler;->afterCheckSIMStatus(II)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_0

    :cond_6
    iget-object v6, p0, Lcom/mediatek/phone/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    const/4 v7, 0x6

    aget-object v6, v6, v7

    if-ne p1, v6, :cond_1

    move-object v0, p1

    check-cast v0, Landroid/app/AlertDialog;

    iget-object v6, p0, Lcom/mediatek/phone/CallOptionHandler;->mAssociateSimMissingArgs:Lcom/mediatek/phone/CallOptionHelper$AssociateSimMissingArgs;

    if-eqz v6, :cond_8

    const/4 v6, -0x1

    if-ne p2, v6, :cond_c

    iget-object v6, p0, Lcom/mediatek/phone/CallOptionHandler;->mAssociateSimMissingArgs:Lcom/mediatek/phone/CallOptionHelper$AssociateSimMissingArgs;

    iget-object v6, v6, Lcom/mediatek/phone/CallOptionHelper$AssociateSimMissingArgs;->viaSimInfo:Landroid/provider/Telephony$SIMInfo;

    if-eqz v6, :cond_b

    iget-object v6, p0, Lcom/mediatek/phone/CallOptionHandler;->mAssociateSimMissingArgs:Lcom/mediatek/phone/CallOptionHelper$AssociateSimMissingArgs;

    iget-object v6, v6, Lcom/mediatek/phone/CallOptionHelper$AssociateSimMissingArgs;->viaSimInfo:Landroid/provider/Telephony$SIMInfo;

    iget v5, v6, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    iget-object v6, p0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    const-string v7, "com.android.phone.extra.slot"

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-direct {p0, v5}, Lcom/mediatek/phone/CallOptionHandler;->needToCheckSIMStatus(I)Z

    move-result v6

    if-eqz v6, :cond_a

    if-ltz v5, :cond_9

    iget-object v6, p0, Lcom/mediatek/phone/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    const/16 v7, 0x132

    iget-object v8, p0, Lcom/mediatek/phone/CallOptionHandler;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v6, v5, v7, v8}, Lcom/mediatek/CellConnService/CellConnMgr;->handleCellConn(IILjava/lang/Runnable;)I

    move-result v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "result = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/phone/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    if-ne v4, v9, :cond_7

    invoke-direct {p0}, Lcom/mediatek/phone/CallOptionHandler;->showProgressIndication()V

    :cond_7
    :goto_2
    iput-boolean v9, p0, Lcom/mediatek/phone/CallOptionHandler;->mAssociateSimMissingClicked:Z

    const/4 v6, 0x0

    iput-object v6, p0, Lcom/mediatek/phone/CallOptionHandler;->mAssociateSimMissingArgs:Lcom/mediatek/phone/CallOptionHelper$AssociateSimMissingArgs;

    :cond_8
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto/16 :goto_1

    :cond_9
    invoke-virtual {p0}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_2

    :cond_a
    invoke-direct {p0, v11, v5}, Lcom/mediatek/phone/CallOptionHandler;->afterCheckSIMStatus(II)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_2

    :cond_b
    invoke-virtual {p0}, Lcom/mediatek/phone/CallOptionHandler;->startSipCallOptionHandler()V

    invoke-virtual {p0}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_2

    :cond_c
    if-ne p2, v8, :cond_7

    new-instance v2, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;

    iget-object v6, p0, Lcom/mediatek/phone/CallOptionHandler;->mCallOptionHelper:Lcom/mediatek/phone/CallOptionHelper;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v2, v6}, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;-><init>(Lcom/mediatek/phone/CallOptionHelper;)V

    iget-object v6, p0, Lcom/mediatek/phone/CallOptionHandler;->mAssociateSimMissingArgs:Lcom/mediatek/phone/CallOptionHelper$AssociateSimMissingArgs;

    iget-wide v6, v6, Lcom/mediatek/phone/CallOptionHelper$AssociateSimMissingArgs;->suggested:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v2, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    iput v12, v2, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->reason:I

    invoke-virtual {p0, v2}, Lcom/mediatek/phone/CallOptionHandler;->onMakeCall(Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;)V

    goto :goto_2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDismiss, mClicked = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/mediatek/phone/CallOptionHandler;->mClicked:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mClicked:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    if-ne p1, v0, :cond_2

    iget-boolean v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mAssociateSimMissingClicked:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_0
.end method

.method public onMakeCall(Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;)V
    .locals 26
    .param p1    # Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "onMakeCall, reason = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p1

    iget v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->reason:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " args = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->reason:I

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/mediatek/phone/CallOptionHandler;->mReason:I

    move-object/from16 v0, p1

    iget v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->reason:I

    move/from16 v22, v0

    packed-switch v22, :pswitch_data_0

    const-string v22, "onMakeCall: no match case found!"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const/16 v18, -0x1

    move-object/from16 v0, p1

    iget v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->type:I

    move/from16 v22, v0

    if-nez v22, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    move-object/from16 v22, v0

    const-string v23, "com.android.phone.extra.ip"

    const/16 v24, 0x0

    invoke-virtual/range {v22 .. v24}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v22

    if-eqz v22, :cond_1

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v22

    const v23, 0x7f0d00b2

    const/16 v24, 0x0

    invoke-static/range {v22 .. v24}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/widget/Toast;->show()V

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete()V

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/mediatek/phone/CallOptionHandler;->newSipCallOptionHandlerIntent(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v17

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "startSipCallOptionHandler(): calling startActivity: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_2
    move-object/from16 v0, p1

    iget v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->type:I

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->id:J

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v18, v0

    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    move-object/from16 v22, v0

    const-string v23, "com.android.phone.extra.slot"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/mediatek/phone/CallOptionHandler;->needToCheckSIMStatus(I)Z

    move-result v22

    if-eqz v22, :cond_6

    if-ltz v18, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    move-object/from16 v22, v0

    const/16 v23, 0x132

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mRunnable:Ljava/lang/Runnable;

    move-object/from16 v24, v0

    move-object/from16 v0, v22

    move/from16 v1, v18

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/mediatek/CellConnService/CellConnMgr;->handleCellConn(IILjava/lang/Runnable;)I

    move-result v16

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "result = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    move-object/from16 v22, v0

    const/16 v22, 0x1

    move/from16 v0, v16

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/mediatek/phone/CallOptionHandler;->showProgressIndication()V

    goto/16 :goto_0

    :cond_4
    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v22

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->id:J

    move-wide/from16 v23, v0

    move-wide/from16 v0, v23

    long-to-int v0, v0

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Lcom/mediatek/phone/SIMInfoWrapper;->getSimSlotById(I)I

    move-result v18

    const/16 v22, -0x1

    move/from16 v0, v18

    move/from16 v1, v22

    if-ne v0, v1, :cond_3

    const/16 v18, 0x0

    goto/16 :goto_2

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete()V

    goto/16 :goto_0

    :cond_6
    const/16 v22, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/mediatek/phone/CallOptionHandler;->afterCheckSIMStatus(II)Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete()V

    goto/16 :goto_0

    :pswitch_2
    new-instance v12, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v10, 0x1

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v22

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/phone/PhoneApp;->phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v22, v0

    if-eqz v22, :cond_7

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v22

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/phone/PhoneApp;->phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Lcom/android/phone/PhoneInterfaceManager;->isSimInsert(I)Z

    move-result v22

    if-nez v22, :cond_7

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v22

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/phone/PhoneApp;->phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    invoke-virtual/range {v22 .. v23}, Lcom/android/phone/PhoneInterfaceManager;->isSimInsert(I)Z

    move-result v22

    if-nez v22, :cond_7

    const/4 v10, 0x0

    :cond_7
    if-nez v10, :cond_9

    const v22, 0x7f0d0091

    move/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mServiceStateExt:Lcom/mediatek/common/telephony/IServiceStateExt;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Lcom/mediatek/common/telephony/IServiceStateExt;->isImeiLocked()Z

    move-result v22

    if-eqz v22, :cond_8

    const v22, 0x7f0d0196

    :goto_3
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v23

    const v24, 0x104000a

    const/16 v22, 0x0

    check-cast v22, Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :goto_4
    invoke-virtual {v12}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->reason:I

    move/from16 v23, v0

    aput-object v13, v22, v23

    invoke-virtual {v13}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :cond_8
    const v22, 0x7f0d018e

    goto :goto_3

    :cond_9
    const v22, 0x7f0d0091

    move/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v22

    const v23, 0x7f0d0016

    invoke-virtual/range {v22 .. v23}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v23

    const v24, 0x1040009

    const/16 v22, 0x0

    check-cast v22, Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v22

    const v23, 0x1040013

    new-instance v24, Lcom/mediatek/phone/CallOptionHandler$1;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/mediatek/phone/CallOptionHandler$1;-><init>(Lcom/mediatek/phone/CallOptionHandler;)V

    invoke-virtual/range {v22 .. v24}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_4

    :pswitch_3
    new-instance v12, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v22, 0x7f0d0091

    move/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v22

    const v23, 0x7f0d00ad

    invoke-virtual/range {v22 .. v23}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v23

    const v24, 0x1040009

    const/16 v22, 0x0

    check-cast v22, Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v22

    const v23, 0x1040013

    new-instance v24, Lcom/mediatek/phone/CallOptionHandler$3;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/mediatek/phone/CallOptionHandler$3;-><init>(Lcom/mediatek/phone/CallOptionHandler;)V

    invoke-virtual/range {v22 .. v24}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v12}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->reason:I

    move/from16 v23, v0

    aput-object v13, v22, v23

    invoke-virtual {v13}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :pswitch_4
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    move-object/from16 v22, v0

    if-nez v22, :cond_a

    const-wide/16 v19, -0x5

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f0d0141

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-wide/from16 v2, v19

    move-object/from16 v4, p0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/mediatek/phone/SimPickerDialog;->create(Landroid/content/Context;Ljava/lang/String;JLandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->reason:I

    move/from16 v23, v0

    aput-object v13, v22, v23

    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/phone/CallOptionHandler;->mClicked:Z

    invoke-virtual {v13}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    :cond_a
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/Long;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v19

    goto :goto_5

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    check-cast v8, Lcom/mediatek/phone/CallOptionHelper$AssociateSimMissingArgs;

    new-instance v12, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p1

    iget-wide v5, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->id:J

    invoke-static {}, Lcom/mediatek/phone/SIMInfoWrapper;->getDefault()Lcom/mediatek/phone/SIMInfoWrapper;

    move-result-object v22

    long-to-int v0, v5

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Lcom/mediatek/phone/SIMInfoWrapper;->getSimInfoById(I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v7

    const-string v9, ""

    if-eqz v7, :cond_b

    iget-object v9, v7, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    :cond_b
    iget-object v0, v8, Lcom/mediatek/phone/CallOptionHelper$AssociateSimMissingArgs;->viaSimInfo:Landroid/provider/Telephony$SIMInfo;

    move-object/from16 v22, v0

    if-eqz v22, :cond_d

    iget-object v0, v8, Lcom/mediatek/phone/CallOptionHelper$AssociateSimMissingArgs;->viaSimInfo:Landroid/provider/Telephony$SIMInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    move-object/from16 v21, v0

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0d0017

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v9, v24, v25

    const/16 v25, 0x1

    aput-object v21, v24, v25

    invoke-virtual/range {v22 .. v24}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->number:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v22

    const v23, 0x1040013

    move-object/from16 v0, v22

    move/from16 v1, v23

    move-object/from16 v2, p0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget v0, v8, Lcom/mediatek/phone/CallOptionHelper$AssociateSimMissingArgs;->type:I

    move/from16 v22, v0

    if-nez v22, :cond_e

    const/high16 v22, 0x1040000

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v12, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_c
    :goto_7
    invoke-virtual {v12}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->reason:I

    move/from16 v23, v0

    aput-object v13, v22, v23

    invoke-virtual {v13}, Landroid/app/Dialog;->show()V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/mediatek/phone/CallOptionHandler;->mAssociateSimMissingArgs:Lcom/mediatek/phone/CallOptionHelper$AssociateSimMissingArgs;

    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/mediatek/phone/CallOptionHandler;->mAssociateSimMissingClicked:Z

    goto/16 :goto_0

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0d02e0

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_6

    :cond_e
    iget v0, v8, Lcom/mediatek/phone/CallOptionHelper$AssociateSimMissingArgs;->type:I

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_c

    const v22, 0x7f0d0018

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-virtual {v12, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_7

    :pswitch_6
    new-instance v12, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v22, 0x7f0d01aa

    move/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v22

    const v23, 0x7f0d01ab

    invoke-virtual/range {v22 .. v23}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v23

    const v24, 0x1040009

    const/16 v22, 0x0

    check-cast v22, Landroid/content/DialogInterface$OnClickListener;

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v22

    const v23, 0x7f0d01ac

    new-instance v24, Lcom/mediatek/phone/CallOptionHandler$4;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/mediatek/phone/CallOptionHandler$4;-><init>(Lcom/mediatek/phone/CallOptionHandler;Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;)V

    invoke-virtual/range {v22 .. v24}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v12}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/mediatek/phone/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->reason:I

    move/from16 v23, v0

    aput-object v13, v22, v23

    invoke-virtual {v13}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public onStop()V
    .locals 4

    iget-object v0, p0, Lcom/mediatek/phone/CallOptionHandler;->mDialogs:[Landroid/app/Dialog;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setOnHandleCallOption(Lcom/mediatek/phone/CallOptionHandler$OnHandleCallOption;)V
    .locals 0
    .param p1    # Lcom/mediatek/phone/CallOptionHandler$OnHandleCallOption;

    iput-object p1, p0, Lcom/mediatek/phone/CallOptionHandler;->mOnHandleCallOption:Lcom/mediatek/phone/CallOptionHandler$OnHandleCallOption;

    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Intent;

    const/4 v6, 0x1

    const/4 v5, -0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startActivity, intent = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    :try_start_0
    invoke-static {p1}, Lcom/android/phone/PhoneUtils;->getInitialNumber(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/phone/CallOptionHandler;->mNumber:Ljava/lang/String;
    :try_end_0
    .catch Lcom/android/phone/PhoneUtils$VoiceMailNumberMissingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v3, "com.android.phone.extra.slot"

    invoke-virtual {p1, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v5, :cond_2

    invoke-direct {p0, v2}, Lcom/mediatek/phone/CallOptionHandler;->needToCheckSIMStatus(I)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/phone/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    const/16 v4, 0x132

    iget-object v5, p0, Lcom/mediatek/phone/CallOptionHandler;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v2, v4, v5}, Lcom/mediatek/CellConnService/CellConnMgr;->handleCellConn(IILjava/lang/Runnable;)I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/phone/CallOptionHandler;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    if-ne v1, v6, :cond_0

    invoke-direct {p0}, Lcom/mediatek/phone/CallOptionHandler;->showProgressIndication()V

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/mediatek/phone/CallOptionHandler;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v6, p1}, Lcom/mediatek/phone/CallOptionHandler;->handleCallOptionComplete(ZLandroid/content/Intent;)V

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/mediatek/phone/CallOptionHandler;->mCallOptionHelper:Lcom/mediatek/phone/CallOptionHelper;

    invoke-virtual {v3, p1}, Lcom/mediatek/phone/CallOptionHelper;->makeCall(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method protected startSipCallOptionHandler()V
    .locals 5

    iget-object v3, p0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    iget-object v4, p0, Lcom/mediatek/phone/CallOptionHandler;->mApp:Lcom/android/phone/PhoneApp;

    invoke-static {v3, v4}, Landroid/telephony/PhoneNumberUtils;->getNumberFromIntent(Landroid/content/Intent;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sip:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/phone/CallOptionHandler;->mNumber:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/mediatek/phone/CallOptionHandler;->mIntent:Landroid/content/Intent;

    invoke-direct {p0, v3}, Lcom/mediatek/phone/CallOptionHandler;->newSipCallOptionHandlerIntent(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    iget-object v3, p0, Lcom/mediatek/phone/CallOptionHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
