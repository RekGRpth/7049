.class public Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;
.super Ljava/lang/Object;
.source "CallOptionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/phone/CallOptionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CallbackArgs"
.end annotation


# instance fields
.field public args:Ljava/lang/Object;

.field public id:J

.field public number:Ljava/lang/String;

.field public reason:I

.field final synthetic this$0:Lcom/mediatek/phone/CallOptionHelper;

.field public type:I


# direct methods
.method public constructor <init>(Lcom/mediatek/phone/CallOptionHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->this$0:Lcom/mediatek/phone/CallOptionHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/mediatek/phone/CallOptionHelper;IIJLjava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2    # I
    .param p3    # I
    .param p4    # J
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->this$0:Lcom/mediatek/phone/CallOptionHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->reason:I

    iput p3, p0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->type:I

    iput-object p6, p0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->number:Ljava/lang/String;

    iput-object p7, p0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->args:Ljava/lang/Object;

    iput-wide p4, p0, Lcom/mediatek/phone/CallOptionHelper$CallbackArgs;->id:J

    return-void
.end method
