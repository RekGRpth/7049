.class public final Lcom/mediatek/phone/HyphonManager;
.super Ljava/lang/Object;
.source "HyphonManager.java"

# interfaces
.implements Landroid/location/CountryListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/phone/HyphonManager$HyphonReceiver;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "HyphonManager/Phone"

.field private static sMe:Lcom/mediatek/phone/HyphonManager;


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mCurrentCountryIso:Ljava/lang/String;

.field protected mHyphonMaps:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mHyphonReceiver:Landroid/content/BroadcastReceiver;

.field private final mPhoneStateListener:Landroid/telephony/PhoneStateListener;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/mediatek/phone/HyphonManager;->mHyphonMaps:Ljava/util/HashMap;

    new-instance v2, Lcom/mediatek/phone/HyphonManager$HyphonReceiver;

    invoke-direct {v2, p0}, Lcom/mediatek/phone/HyphonManager$HyphonReceiver;-><init>(Lcom/mediatek/phone/HyphonManager;)V

    iput-object v2, p0, Lcom/mediatek/phone/HyphonManager;->mHyphonReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Lcom/mediatek/phone/HyphonManager$1;

    invoke-direct {v2, p0}, Lcom/mediatek/phone/HyphonManager$1;-><init>(Lcom/mediatek/phone/HyphonManager;)V

    iput-object v2, p0, Lcom/mediatek/phone/HyphonManager;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const-string v2, "HyphonManager()"

    invoke-virtual {p0, v2}, Lcom/mediatek/phone/HyphonManager;->log(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/mediatek/phone/HyphonManager;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/mediatek/phone/HyphonManager;->detectCountry()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/phone/HyphonManager;->mCurrentCountryIso:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/phone/HyphonManager;->mContext:Landroid/content/Context;

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/mediatek/phone/HyphonManager;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v4, v3}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    iget-object v2, p0, Lcom/mediatek/phone/HyphonManager;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v1, v2, v4, v4}, Landroid/telephony/TelephonyManager;->listenGemini(Landroid/telephony/PhoneStateListener;II)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/phone/HyphonManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/mediatek/phone/HyphonManager;->mHyphonReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public static destroy()V
    .locals 1

    sget-object v0, Lcom/mediatek/phone/HyphonManager;->sMe:Lcom/mediatek/phone/HyphonManager;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/phone/HyphonManager;->sMe:Lcom/mediatek/phone/HyphonManager;

    invoke-virtual {v0}, Lcom/mediatek/phone/HyphonManager;->onDestroy()V

    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/mediatek/phone/HyphonManager;
    .locals 2

    sget-object v0, Lcom/mediatek/phone/HyphonManager;->sMe:Lcom/mediatek/phone/HyphonManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/phone/HyphonManager;

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mediatek/phone/HyphonManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mediatek/phone/HyphonManager;->sMe:Lcom/mediatek/phone/HyphonManager;

    :cond_0
    sget-object v0, Lcom/mediatek/phone/HyphonManager;->sMe:Lcom/mediatek/phone/HyphonManager;

    return-object v0
.end method


# virtual methods
.method detectCountry()Ljava/lang/String;
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mediatek/phone/HyphonManager;->mContext:Landroid/content/Context;

    const-string v4, "country_detector"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/CountryDetector;

    invoke-virtual {v1, p0, v2}, Landroid/location/CountryDetector;->addCountryListener(Landroid/location/CountryListener;Landroid/os/Looper;)V

    invoke-virtual {v1}, Landroid/location/CountryDetector;->detectCountry()Landroid/location/Country;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "detect country, iso = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " source = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/location/Country;->getSource()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/mediatek/phone/HyphonManager;->log(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    move-result-object v2

    :cond_0
    return-object v2
.end method

.method public formatNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/phone/HyphonManager;->mCurrentCountryIso:Ljava/lang/String;

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/mediatek/phone/HyphonManager;->detectCountry()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/phone/HyphonManager;->mCurrentCountryIso:Ljava/lang/String;

    :cond_2
    iget-object v1, p0, Lcom/mediatek/phone/HyphonManager;->mHyphonMaps:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/phone/HyphonManager;->mCurrentCountryIso:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/mediatek/phone/HyphonManager;->mHyphonMaps:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    move-object v0, p1

    goto :goto_0
.end method

.method log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "HyphonManager/Phone"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onCountryDetected(Landroid/location/Country;)V
    .locals 2
    .param p1    # Landroid/location/Country;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onCountryDetected, country = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/phone/HyphonManager;->log(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/location/Country;->getCountryIso()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/phone/HyphonManager;->setCountryIso(Ljava/lang/String;)V

    return-void
.end method

.method public onDestroy()V
    .locals 3

    iget-object v1, p0, Lcom/mediatek/phone/HyphonManager;->mCurrentCountryIso:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/phone/HyphonManager;->mContext:Landroid/content/Context;

    const-string v2, "country_detector"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/CountryDetector;

    invoke-virtual {v0, p0}, Landroid/location/CountryDetector;->removeCountryListener(Landroid/location/CountryListener;)V

    :cond_0
    return-void
.end method

.method public setCountryIso(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setCountryIso, mCurrentCountryIso = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/phone/HyphonManager;->mCurrentCountryIso:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " countryIso = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/phone/HyphonManager;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/HyphonManager;->mCurrentCountryIso:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/HyphonManager;->mCurrentCountryIso:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/mediatek/phone/HyphonManager;->mCurrentCountryIso:Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/phone/HyphonManager;->mHyphonMaps:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    return-void
.end method
