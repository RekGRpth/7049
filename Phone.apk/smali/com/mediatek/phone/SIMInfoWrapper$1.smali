.class Lcom/mediatek/phone/SIMInfoWrapper$1;
.super Landroid/content/BroadcastReceiver;
.source "SIMInfoWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/phone/SIMInfoWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/phone/SIMInfoWrapper;


# direct methods
.method constructor <init>(Lcom/mediatek/phone/SIMInfoWrapper;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/phone/SIMInfoWrapper$1;->this$0:Lcom/mediatek/phone/SIMInfoWrapper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SIM_SETTING_INFO_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.SIM_NAME_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/mediatek/phone/SIMInfoWrapper$1;->this$0:Lcom/mediatek/phone/SIMInfoWrapper;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[updateSimInfo][action] : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/phone/SIMInfoWrapper;->access$000(Lcom/mediatek/phone/SIMInfoWrapper;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/phone/SIMInfoWrapper$1;->this$0:Lcom/mediatek/phone/SIMInfoWrapper;

    iget-object v2, p0, Lcom/mediatek/phone/SIMInfoWrapper$1;->this$0:Lcom/mediatek/phone/SIMInfoWrapper;

    invoke-static {v2}, Lcom/mediatek/phone/SIMInfoWrapper;->access$100(Lcom/mediatek/phone/SIMInfoWrapper;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/phone/SIMInfoWrapper;->access$200(Lcom/mediatek/phone/SIMInfoWrapper;Landroid/content/Context;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "android.intent.action.SIM_INFO_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/phone/SIMInfoWrapper$1;->this$0:Lcom/mediatek/phone/SIMInfoWrapper;

    const-string v2, "[updateSimInfo][ACTION_RADIO_OFF]"

    invoke-static {v1, v2}, Lcom/mediatek/phone/SIMInfoWrapper;->access$000(Lcom/mediatek/phone/SIMInfoWrapper;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/phone/SIMInfoWrapper$1;->this$0:Lcom/mediatek/phone/SIMInfoWrapper;

    iget-object v2, p0, Lcom/mediatek/phone/SIMInfoWrapper$1;->this$0:Lcom/mediatek/phone/SIMInfoWrapper;

    invoke-static {v2}, Lcom/mediatek/phone/SIMInfoWrapper;->access$100(Lcom/mediatek/phone/SIMInfoWrapper;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/phone/SIMInfoWrapper;->access$200(Lcom/mediatek/phone/SIMInfoWrapper;Landroid/content/Context;)V

    iget-object v1, p0, Lcom/mediatek/phone/SIMInfoWrapper$1;->this$0:Lcom/mediatek/phone/SIMInfoWrapper;

    invoke-static {v1}, Lcom/mediatek/phone/SIMInfoWrapper;->access$300(Lcom/mediatek/phone/SIMInfoWrapper;)Landroid/widget/CursorAdapter;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/phone/SIMInfoWrapper$1;->this$0:Lcom/mediatek/phone/SIMInfoWrapper;

    invoke-static {v1}, Lcom/mediatek/phone/SIMInfoWrapper;->access$300(Lcom/mediatek/phone/SIMInfoWrapper;)Landroid/widget/CursorAdapter;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/phone/SIMInfoWrapper$1;->this$0:Lcom/mediatek/phone/SIMInfoWrapper;

    invoke-static {v1}, Lcom/mediatek/phone/SIMInfoWrapper;->access$300(Lcom/mediatek/phone/SIMInfoWrapper;)Landroid/widget/CursorAdapter;

    move-result-object v1

    instance-of v1, v1, Landroid/widget/CursorAdapter;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/phone/SIMInfoWrapper$1;->this$0:Lcom/mediatek/phone/SIMInfoWrapper;

    invoke-static {v1}, Lcom/mediatek/phone/SIMInfoWrapper;->access$300(Lcom/mediatek/phone/SIMInfoWrapper;)Landroid/widget/CursorAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_3
    iget-object v1, p0, Lcom/mediatek/phone/SIMInfoWrapper$1;->this$0:Lcom/mediatek/phone/SIMInfoWrapper;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[]adapter"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/phone/SIMInfoWrapper$1;->this$0:Lcom/mediatek/phone/SIMInfoWrapper;

    invoke-static {v3}, Lcom/mediatek/phone/SIMInfoWrapper;->access$300(Lcom/mediatek/phone/SIMInfoWrapper;)Landroid/widget/CursorAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/phone/SIMInfoWrapper;->access$000(Lcom/mediatek/phone/SIMInfoWrapper;Ljava/lang/String;)V

    goto :goto_0
.end method
