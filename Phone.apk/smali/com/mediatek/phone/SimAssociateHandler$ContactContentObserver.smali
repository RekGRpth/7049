.class public Lcom/mediatek/phone/SimAssociateHandler$ContactContentObserver;
.super Landroid/database/ContentObserver;
.source "SimAssociateHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/phone/SimAssociateHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ContactContentObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/phone/SimAssociateHandler;


# direct methods
.method public constructor <init>(Lcom/mediatek/phone/SimAssociateHandler;)V
    .locals 1

    iput-object p1, p0, Lcom/mediatek/phone/SimAssociateHandler$ContactContentObserver;->this$0:Lcom/mediatek/phone/SimAssociateHandler;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1    # Z

    iget-object v0, p0, Lcom/mediatek/phone/SimAssociateHandler$ContactContentObserver;->this$0:Lcom/mediatek/phone/SimAssociateHandler;

    const-string v1, "ContactContentObserver: "

    invoke-virtual {v0, v1}, Lcom/mediatek/phone/SimAssociateHandler;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/SimAssociateHandler$ContactContentObserver;->this$0:Lcom/mediatek/phone/SimAssociateHandler;

    invoke-static {v0}, Lcom/mediatek/phone/SimAssociateHandler;->access$000(Lcom/mediatek/phone/SimAssociateHandler;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/SimAssociateHandler$ContactContentObserver;->this$0:Lcom/mediatek/phone/SimAssociateHandler;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mediatek/phone/SimAssociateHandler;->access$002(Lcom/mediatek/phone/SimAssociateHandler;Z)Z

    iget-object v0, p0, Lcom/mediatek/phone/SimAssociateHandler$ContactContentObserver;->this$0:Lcom/mediatek/phone/SimAssociateHandler;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ContactContentObserver: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/phone/SimAssociateHandler$ContactContentObserver;->this$0:Lcom/mediatek/phone/SimAssociateHandler;

    invoke-static {v2}, Lcom/mediatek/phone/SimAssociateHandler;->access$000(Lcom/mediatek/phone/SimAssociateHandler;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mediatek/phone/SimAssociateHandler;->log(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
