.class public final Lcom/mediatek/phone/SimPickerDialog;
.super Ljava/lang/Object;
.source "SimPickerDialog.java"


# static fields
.field public static final DEFAULT_SIM_NOT_SET:I = -0x5


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Landroid/content/Context;Ljava/lang/String;JLandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # Landroid/content/DialogInterface$OnClickListener;

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/mediatek/phone/SimPickerDialog;->createItemHolder(Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/mediatek/phone/SimPickerDialog;->create(Landroid/content/Context;Ljava/lang/String;JLjava/util/List;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method protected static create(Landroid/content/Context;Ljava/lang/String;JLjava/util/List;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p5    # Landroid/content/DialogInterface$OnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/phone/SimPickerAdapter$ItemHolder;",
            ">;",
            "Landroid/content/DialogInterface$OnClickListener;",
            ")",
            "Landroid/app/AlertDialog;"
        }
    .end annotation

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/mediatek/phone/SimPickerAdapter;

    invoke-direct {v1, p0, p4, p2, p3}, Lcom/mediatek/phone/SimPickerAdapter;-><init>(Landroid/content/Context;Ljava/util/List;J)V

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2, p5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2
.end method

.method public static create(Landroid/content/Context;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/DialogInterface$OnClickListener;

    const-wide/16 v2, -0x5

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/mediatek/phone/SimPickerDialog;->createItemHolder(Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/mediatek/phone/SimPickerDialog;->create(Landroid/content/Context;Ljava/lang/String;JLjava/util/List;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method protected static createItemHolder(Landroid/content/Context;Ljava/lang/String;ZLjava/util/ArrayList;)Ljava/util/List;
    .locals 13
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/accounts/Account;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/phone/SimPickerAdapter$ItemHolder;",
            ">;"
        }
    .end annotation

    invoke-static {p0}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v7

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v9, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    new-instance v9, Lcom/mediatek/phone/SimPickerAdapter$ItemHolder;

    const/4 v10, 0x2

    invoke-direct {v9, p1, v10}, Lcom/mediatek/phone/SimPickerAdapter$ItemHolder;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 v3, 0x0

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/provider/Telephony$SIMInfo;

    new-instance v9, Lcom/mediatek/phone/SimPickerAdapter$ItemHolder;

    const/4 v10, 0x0

    invoke-direct {v9, v6, v10}, Lcom/mediatek/phone/SimPickerAdapter$ItemHolder;-><init>(Ljava/lang/Object;I)V

    if-nez v3, :cond_1

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v10

    add-int/lit8 v5, v10, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/mediatek/phone/SimPickerAdapter$ItemHolder;

    iget-object v8, v10, Lcom/mediatek/phone/SimPickerAdapter$ItemHolder;->mData:Ljava/lang/Object;

    check-cast v8, Landroid/provider/Telephony$SIMInfo;

    iget v10, v6, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    iget v11, v8, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-ge v10, v11, :cond_2

    invoke-virtual {v4, v5, v9}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "enable_internet_call_value"

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz p2, :cond_4

    invoke-static {p0}, Landroid/net/sip/SipManager;->isVoipSupported(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/4 v10, 0x1

    if-ne v1, v10, :cond_4

    new-instance v9, Lcom/mediatek/phone/SimPickerAdapter$ItemHolder;

    const-string v10, "Internet"

    const/4 v11, 0x1

    invoke-direct {v9, v10, v11}, Lcom/mediatek/phone/SimPickerAdapter$ItemHolder;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    if-eqz p3, :cond_5

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    new-instance v9, Lcom/mediatek/phone/SimPickerAdapter$ItemHolder;

    const/4 v10, 0x3

    invoke-direct {v9, v0, v10}, Lcom/mediatek/phone/SimPickerAdapter$ItemHolder;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    return-object v4
.end method

.method protected static createItemHolder(Landroid/content/Context;Z)Ljava/util/List;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/phone/SimPickerAdapter$ItemHolder;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {p0, v0, p1, v0}, Lcom/mediatek/phone/SimPickerDialog;->createItemHolder(Landroid/content/Context;Ljava/lang/String;ZLjava/util/ArrayList;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
