.class public final Lcom/mediatek/phone/ext/ExtensionManager;
.super Ljava/lang/Object;
.source "ExtensionManager.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ExtensionManager"

.field private static sInstance:Lcom/mediatek/phone/ext/ExtensionManager;


# instance fields
.field private mIPhonePluginList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/mediatek/phone/ext/IPhonePlugin;",
            ">;"
        }
    .end annotation
.end field

.field private mPhonePluginExtContainer:Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;

    invoke-direct {v0}, Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/phone/ext/ExtensionManager;->mPhonePluginExtContainer:Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;

    invoke-direct {p0}, Lcom/mediatek/phone/ext/ExtensionManager;->initContainerByPlugin()V

    return-void
.end method

.method public static getInstance()Lcom/mediatek/phone/ext/ExtensionManager;
    .locals 1

    sget-object v0, Lcom/mediatek/phone/ext/ExtensionManager;->sInstance:Lcom/mediatek/phone/ext/ExtensionManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mediatek/phone/ext/ExtensionManager;

    invoke-direct {v0}, Lcom/mediatek/phone/ext/ExtensionManager;-><init>()V

    sput-object v0, Lcom/mediatek/phone/ext/ExtensionManager;->sInstance:Lcom/mediatek/phone/ext/ExtensionManager;

    :cond_0
    sget-object v0, Lcom/mediatek/phone/ext/ExtensionManager;->sInstance:Lcom/mediatek/phone/ext/ExtensionManager;

    return-object v0
.end method

.method private initContainerByPlugin()V
    .locals 8

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v5

    const-class v6, Lcom/mediatek/phone/ext/IPhonePlugin;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    new-array v7, v7, [Landroid/content/pm/Signature;

    invoke-static {v5, v6, v7}, Lcom/mediatek/pluginmanager/PluginManager;->create(Landroid/content/Context;Ljava/lang/String;[Landroid/content/pm/Signature;)Lcom/mediatek/pluginmanager/PluginManager;

    move-result-object v4

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    invoke-virtual {v4}, Lcom/mediatek/pluginmanager/PluginManager;->getPluginCount()I

    move-result v5

    if-ge v1, v5, :cond_2

    invoke-virtual {v4, v1}, Lcom/mediatek/pluginmanager/PluginManager;->getPlugin(I)Lcom/mediatek/pluginmanager/Plugin;

    move-result-object v3

    if-eqz v3, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "create plugin object, number = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/mediatek/phone/ext/ExtensionManager;->log(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/mediatek/pluginmanager/Plugin;->createObject()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/phone/ext/IPhonePlugin;

    iget-object v5, p0, Lcom/mediatek/phone/ext/ExtensionManager;->mPhonePluginExtContainer:Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;

    invoke-virtual {v5, v2}, Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;->addExtensions(Lcom/mediatek/phone/ext/IPhonePlugin;)V

    iget-object v5, p0, Lcom/mediatek/phone/ext/ExtensionManager;->mIPhonePluginList:Ljava/util/LinkedList;

    if-nez v5, :cond_0

    const-string v5, "create mIPhonePluglist"

    invoke-static {v5}, Lcom/mediatek/phone/ext/ExtensionManager;->log(Ljava/lang/String;)V

    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    iput-object v5, p0, Lcom/mediatek/phone/ext/ExtensionManager;->mIPhonePluginList:Ljava/util/LinkedList;

    :cond_0
    iget-object v5, p0, Lcom/mediatek/phone/ext/ExtensionManager;->mIPhonePluginList:Ljava/util/LinkedList;

    invoke-virtual {v5, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/mediatek/pluginmanager/Plugin$ObjectCreationException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v5, "create plugin object failed"

    invoke-static {v5}, Lcom/mediatek/phone/ext/ExtensionManager;->log(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_2
    return-void
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "ExtensionManager"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public getCallCardExtension()Lcom/mediatek/phone/ext/CallCardExtension;
    .locals 1

    const-string v0, "getCallCardExtension()"

    invoke-static {v0}, Lcom/mediatek/phone/ext/ExtensionManager;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/ext/ExtensionManager;->mPhonePluginExtContainer:Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;->getCallCardExtension()Lcom/mediatek/phone/ext/CallCardExtension;

    move-result-object v0

    return-object v0
.end method

.method public getInCallScreenExtension()Lcom/mediatek/phone/ext/InCallScreenExtension;
    .locals 1

    const-string v0, "getInCallScreenExtension()"

    invoke-static {v0}, Lcom/mediatek/phone/ext/ExtensionManager;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/ext/ExtensionManager;->mPhonePluginExtContainer:Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;->getInCallScreenExtension()Lcom/mediatek/phone/ext/InCallScreenExtension;

    move-result-object v0

    return-object v0
.end method

.method public getInCallTouchUiExtension()Lcom/mediatek/phone/ext/InCallTouchUiExtension;
    .locals 1

    const-string v0, "getInCallTouchUiExtension()"

    invoke-static {v0}, Lcom/mediatek/phone/ext/ExtensionManager;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/ext/ExtensionManager;->mPhonePluginExtContainer:Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;->getInCallTouchUiExtension()Lcom/mediatek/phone/ext/InCallTouchUiExtension;

    move-result-object v0

    return-object v0
.end method

.method public getOthersSettingsExtension()Lcom/mediatek/phone/ext/OthersSettingsExtension;
    .locals 1

    const-string v0, "getOthersSettingsExtension()"

    invoke-static {v0}, Lcom/mediatek/phone/ext/ExtensionManager;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/ext/ExtensionManager;->mPhonePluginExtContainer:Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;->getOthersSettingsExtension()Lcom/mediatek/phone/ext/OthersSettingsExtension;

    move-result-object v0

    return-object v0
.end method

.method public getPhoneAppBroadcastReceiverExtension()Lcom/mediatek/phone/ext/PhoneAppBroadcastReceiverExtension;
    .locals 1

    const-string v0, "PhoneAppBroadcastReceiverExtension()"

    invoke-static {v0}, Lcom/mediatek/phone/ext/ExtensionManager;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/ext/ExtensionManager;->mPhonePluginExtContainer:Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;->getPhoneAppBroadcastReceiverExtension()Lcom/mediatek/phone/ext/PhoneAppBroadcastReceiverExtension;

    move-result-object v0

    return-object v0
.end method

.method public getSettingsExtension()Lcom/mediatek/phone/ext/SettingsExtension;
    .locals 1

    const-string v0, "getSettingsExtension()"

    invoke-static {v0}, Lcom/mediatek/phone/ext/ExtensionManager;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/ext/ExtensionManager;->mPhonePluginExtContainer:Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;->getSettingsExtension()Lcom/mediatek/phone/ext/SettingsExtension;

    move-result-object v0

    return-object v0
.end method

.method public getVTCallBannerControllerExtension()Lcom/mediatek/phone/ext/VTCallBannerControllerExtension;
    .locals 1

    const-string v0, "getVTCallBannerControllerExtension()"

    invoke-static {v0}, Lcom/mediatek/phone/ext/ExtensionManager;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/ext/ExtensionManager;->mPhonePluginExtContainer:Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;->getVTCallBannerControllerExtension()Lcom/mediatek/phone/ext/VTCallBannerControllerExtension;

    move-result-object v0

    return-object v0
.end method

.method public getVTInCallScreenExtension()Lcom/mediatek/phone/ext/VTInCallScreenExtension;
    .locals 1

    const-string v0, "getVTInCallScreenExtension()"

    invoke-static {v0}, Lcom/mediatek/phone/ext/ExtensionManager;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/ext/ExtensionManager;->mPhonePluginExtContainer:Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;->getVTInCallScreenExtension()Lcom/mediatek/phone/ext/VTInCallScreenExtension;

    move-result-object v0

    return-object v0
.end method

.method public getVTInCallScreenFlagsExtension()Lcom/mediatek/phone/ext/VTInCallScreenFlagsExtension;
    .locals 1

    const-string v0, "getVTInCallScreenFlagsExtension()"

    invoke-static {v0}, Lcom/mediatek/phone/ext/ExtensionManager;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/ext/ExtensionManager;->mPhonePluginExtContainer:Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/PhonePluginExtensionContainer;->getVTInCallScreenFlagsExtension()Lcom/mediatek/phone/ext/VTInCallScreenFlagsExtension;

    move-result-object v0

    return-object v0
.end method
