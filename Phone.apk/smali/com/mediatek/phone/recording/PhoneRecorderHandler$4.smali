.class Lcom/mediatek/phone/recording/PhoneRecorderHandler$4;
.super Lcom/mediatek/phone/recording/IPhoneRecordStateListener$Stub;
.source "PhoneRecorderHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/phone/recording/PhoneRecorderHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/phone/recording/PhoneRecorderHandler;


# direct methods
.method constructor <init>(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler$4;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    invoke-direct {p0}, Lcom/mediatek/phone/recording/IPhoneRecordStateListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(I)V
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler$4;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    invoke-static {v1}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->access$100(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)Lcom/mediatek/phone/recording/IPhoneRecorder;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void

    :pswitch_0
    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onStateChange(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler$4;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStateChange, state is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->access$200(Lcom/mediatek/phone/recording/PhoneRecorderHandler;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler$4;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    invoke-static {v0, p1}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->access$602(Lcom/mediatek/phone/recording/PhoneRecorderHandler;I)I

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler$4;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    invoke-static {v0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->access$700(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler$4;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    invoke-static {v0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->access$700(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler$4;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    invoke-static {v1}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->access$800(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;->requestUpdateRecordState(II)V

    :cond_0
    return-void
.end method
