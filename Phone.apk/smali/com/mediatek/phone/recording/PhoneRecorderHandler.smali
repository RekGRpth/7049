.class public Lcom/mediatek/phone/recording/PhoneRecorderHandler;
.super Ljava/lang/Object;
.source "PhoneRecorderHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final LOG_TAG:Ljava/lang/String; = "PhoneRecorderHandler"

.field private static final VDBG:Z = true

.field private static sInstance:Lcom/mediatek/phone/recording/PhoneRecorderHandler;


# instance fields
.field private mConnection:Landroid/content/ServiceConnection;

.field private mCustomValue:I

.field private mHandler:Landroid/os/Handler;

.field private mListener:Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;

.field private mPhoneRecordStateListener:Lcom/mediatek/phone/recording/IPhoneRecordStateListener;

.field private mPhoneRecorder:Lcom/mediatek/phone/recording/IPhoneRecorder;

.field private mPhoneRecorderState:I

.field private mRecordDiskCheck:Ljava/lang/Runnable;

.field private mRecordType:I

.field private mRecorderServiceIntent:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    invoke-direct {v0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;-><init>()V

    sput-object v0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->sInstance:Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v1

    const-class v2, Lcom/mediatek/phone/recording/PhoneRecorderServices;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecorderServiceIntent:Landroid/content/Intent;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorderState:I

    new-instance v0, Lcom/mediatek/phone/recording/PhoneRecorderHandler$1;

    invoke-direct {v0, p0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler$1;-><init>(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)V

    iput-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/phone/recording/PhoneRecorderHandler$2;

    invoke-direct {v0, p0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler$2;-><init>(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)V

    iput-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecordDiskCheck:Ljava/lang/Runnable;

    new-instance v0, Lcom/mediatek/phone/recording/PhoneRecorderHandler$3;

    invoke-direct {v0, p0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler$3;-><init>(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)V

    iput-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mConnection:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/mediatek/phone/recording/PhoneRecorderHandler$4;

    invoke-direct {v0, p0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler$4;-><init>(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)V

    iput-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecordStateListener:Lcom/mediatek/phone/recording/IPhoneRecordStateListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)V
    .locals 0
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    invoke-direct {p0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->checkRecordDisk()V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)Lcom/mediatek/phone/recording/IPhoneRecorder;
    .locals 1
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorder:Lcom/mediatek/phone/recording/IPhoneRecorder;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mediatek/phone/recording/PhoneRecorderHandler;Lcom/mediatek/phone/recording/IPhoneRecorder;)Lcom/mediatek/phone/recording/IPhoneRecorder;
    .locals 0
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderHandler;
    .param p1    # Lcom/mediatek/phone/recording/IPhoneRecorder;

    iput-object p1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorder:Lcom/mediatek/phone/recording/IPhoneRecorder;

    return-object p1
.end method

.method static synthetic access$200(Lcom/mediatek/phone/recording/PhoneRecorderHandler;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderHandler;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)Lcom/mediatek/phone/recording/IPhoneRecordStateListener;
    .locals 1
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecordStateListener:Lcom/mediatek/phone/recording/IPhoneRecordStateListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecordDiskCheck:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$602(Lcom/mediatek/phone/recording/PhoneRecorderHandler;I)I
    .locals 0
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderHandler;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorderState:I

    return p1
.end method

.method static synthetic access$700(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;
    .locals 1
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mListener:Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/phone/recording/PhoneRecorderHandler;)I
    .locals 1
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    iget v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mCustomValue:I

    return v0
.end method

.method private checkRecordDisk()V
    .locals 4

    const-wide/32 v0, 0x200000

    invoke-static {v0, v1}, Lcom/android/phone/PhoneUtils;->diskSpaceAvailable(J)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "AN: "

    const-string v1, "Checking result, disk is full, stop recording..."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/phone/recording/PhoneRecorder;->isRecording()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->isVTRecording()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-static {}, Lcom/mediatek/phone/recording/PhoneRecorder;->isRecording()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->stopVoiceRecord()V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mListener:Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mListener:Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;

    invoke-interface {v0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;->onStorageFull()V

    :cond_2
    :goto_1
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->isVTRecording()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->stopVideoRecord()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecordDiskCheck:Ljava/lang/Runnable;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method public static getInstance()Lcom/mediatek/phone/recording/PhoneRecorderHandler;
    .locals 1

    sget-object v0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->sInstance:Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    return-object v0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "PhoneRecorderHandler"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public clearListener(Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;)V
    .locals 1
    .param p1    # Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mListener:Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mListener:Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;

    :cond_0
    return-void
.end method

.method public getCustomValue()I
    .locals 1

    iget v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mCustomValue:I

    return v0
.end method

.method public getPhoneRecorderState()I
    .locals 1

    iget v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorderState:I

    return v0
.end method

.method public getRecordType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecordType:I

    return v0
.end method

.method public isVTRecording()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mCustomValue:I

    if-ne v0, v1, :cond_0

    iget v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorderState:I

    if-ne v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCustomValue(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mCustomValue:I

    return-void
.end method

.method public setListener(Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;)V
    .locals 0
    .param p1    # Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;

    iput-object p1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mListener:Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;

    return-void
.end method

.method public setPhoneRecorderState(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorderState:I

    return-void
.end method

.method public setRecordType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecordType:I

    return-void
.end method

.method public startVideoRecord(IJI)V
    .locals 4
    .param p1    # I
    .param p2    # J
    .param p4    # I

    iput p1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecordType:I

    iput p4, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mCustomValue:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "- start call VTManager.startRecording() : type = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sd max size = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/mediatek/vt/VTManager;->startRecording(IJ)I

    const-string v0, "- end call VTManager.startRecording()"

    invoke-direct {p0, v0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->log(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorderState:I

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mListener:Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mListener:Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;

    iget v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorderState:I

    iget v2, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mCustomValue:I

    invoke-interface {v0, v1, v2}, Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;->requestUpdateRecordState(II)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecordDiskCheck:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public startVoiceRecord(I)V
    .locals 5
    .param p1    # I

    iput p1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mCustomValue:I

    const/4 v1, 0x2

    iput v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecordType:I

    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecorderServiceIntent:Landroid/content/Intent;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorder:Lcom/mediatek/phone/recording/IPhoneRecorder;

    if-nez v1, :cond_1

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecorderServiceIntent:Landroid/content/Intent;

    iget-object v3, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mConnection:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecorderServiceIntent:Landroid/content/Intent;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorder:Lcom/mediatek/phone/recording/IPhoneRecorder;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorder:Lcom/mediatek/phone/recording/IPhoneRecorder;

    invoke-interface {v1}, Lcom/mediatek/phone/recording/IPhoneRecorder;->startRecord()V

    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecordDiskCheck:Ljava/lang/Runnable;

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "PhoneRecorderHandler"

    const-string v2, "start Record failed"

    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v1, v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public stopVideoRecord()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "- start call VTManager.stopRecording() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecordType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecordType:I

    invoke-virtual {v0, v1}, Lcom/mediatek/vt/VTManager;->stopRecording(I)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "- end call VTManager.stopRecording() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mRecordType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorderState:I

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mListener:Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mListener:Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;

    iget v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorderState:I

    iget v2, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mCustomValue:I

    invoke-interface {v0, v1, v2}, Lcom/mediatek/phone/recording/PhoneRecorderHandler$Listener;->requestUpdateRecordState(II)V

    :cond_0
    return-void
.end method

.method public stopVoiceRecord()V
    .locals 4

    :try_start_0
    const-string v1, "stopRecord"

    invoke-direct {p0, v1}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorder:Lcom/mediatek/phone/recording/IPhoneRecorder;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorder:Lcom/mediatek/phone/recording/IPhoneRecorder;

    invoke-interface {v1}, Lcom/mediatek/phone/recording/IPhoneRecorder;->stopRecord()V

    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorder:Lcom/mediatek/phone/recording/IPhoneRecorder;

    invoke-interface {v1}, Lcom/mediatek/phone/recording/IPhoneRecorder;->remove()V

    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mConnection:Landroid/content/ServiceConnection;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->mPhoneRecorder:Lcom/mediatek/phone/recording/IPhoneRecorder;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "PhoneRecorderHandler"

    const-string v2, "stopRecord: couldn\'t call to record service"

    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v1, v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
