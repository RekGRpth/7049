.class Lcom/mediatek/phone/recording/PhoneRecorderServices$3;
.super Lcom/mediatek/phone/recording/IPhoneRecorder$Stub;
.source "PhoneRecorderServices.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/phone/recording/PhoneRecorderServices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/phone/recording/PhoneRecorderServices;


# direct methods
.method constructor <init>(Lcom/mediatek/phone/recording/PhoneRecorderServices;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices$3;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderServices;

    invoke-direct {p0}, Lcom/mediatek/phone/recording/IPhoneRecorder$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public listen(Lcom/mediatek/phone/recording/IPhoneRecordStateListener;)V
    .locals 2
    .param p1    # Lcom/mediatek/phone/recording/IPhoneRecordStateListener;

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices$3;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderServices;

    const-string v1, "listen"

    invoke-virtual {v0, v1}, Lcom/mediatek/phone/recording/PhoneRecorderServices;->log(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices$3;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderServices;

    iput-object p1, v0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mStateListener:Lcom/mediatek/phone/recording/IPhoneRecordStateListener;

    :cond_0
    return-void
.end method

.method public remove()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices$3;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderServices;

    const-string v1, "remove"

    invoke-virtual {v0, v1}, Lcom/mediatek/phone/recording/PhoneRecorderServices;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices$3;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderServices;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mStateListener:Lcom/mediatek/phone/recording/IPhoneRecordStateListener;

    return-void
.end method

.method public startRecord()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices$3;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderServices;

    const-string v1, "startRecord"

    invoke-virtual {v0, v1}, Lcom/mediatek/phone/recording/PhoneRecorderServices;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices$3;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderServices;

    invoke-static {v0}, Lcom/mediatek/phone/recording/PhoneRecorderServices;->access$200(Lcom/mediatek/phone/recording/PhoneRecorderServices;)Lcom/mediatek/phone/recording/PhoneRecorder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices$3;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderServices;

    invoke-static {v0}, Lcom/mediatek/phone/recording/PhoneRecorderServices;->access$200(Lcom/mediatek/phone/recording/PhoneRecorderServices;)Lcom/mediatek/phone/recording/PhoneRecorder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/phone/recording/PhoneRecorder;->startRecord()V

    :cond_0
    return-void
.end method

.method public stopRecord()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices$3;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderServices;

    const-string v1, "stopRecord"

    invoke-virtual {v0, v1}, Lcom/mediatek/phone/recording/PhoneRecorderServices;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices$3;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderServices;

    invoke-static {v0}, Lcom/mediatek/phone/recording/PhoneRecorderServices;->access$200(Lcom/mediatek/phone/recording/PhoneRecorderServices;)Lcom/mediatek/phone/recording/PhoneRecorder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices$3;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderServices;

    invoke-static {v0}, Lcom/mediatek/phone/recording/PhoneRecorderServices;->access$200(Lcom/mediatek/phone/recording/PhoneRecorderServices;)Lcom/mediatek/phone/recording/PhoneRecorder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices$3;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderServices;

    invoke-static {v1}, Lcom/mediatek/phone/recording/PhoneRecorderServices;->access$000(Lcom/mediatek/phone/recording/PhoneRecorderServices;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/mediatek/phone/recording/PhoneRecorder;->stopRecord(Z)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices$3;->this$0:Lcom/mediatek/phone/recording/PhoneRecorderServices;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mediatek/phone/recording/PhoneRecorderServices;->access$202(Lcom/mediatek/phone/recording/PhoneRecorderServices;Lcom/mediatek/phone/recording/PhoneRecorder;)Lcom/mediatek/phone/recording/PhoneRecorder;

    return-void
.end method
