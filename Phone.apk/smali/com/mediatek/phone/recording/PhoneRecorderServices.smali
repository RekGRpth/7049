.class public Lcom/mediatek/phone/recording/PhoneRecorderServices;
.super Landroid/app/Service;
.source "PhoneRecorderServices.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "RecorderServices"

.field private static final PHONE_VOICE_RECORD_STATE_CHANGE_MESSAGE:Ljava/lang/String; = "com.android.phone.VoiceRecorder.STATE"


# instance fields
.field private final mBinder:Lcom/mediatek/phone/recording/IPhoneRecorder$Stub;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mMount:Z

.field private mPhoneRecorder:Lcom/mediatek/phone/recording/PhoneRecorder;

.field private mPhoneRecorderStateListener:Lcom/mediatek/phone/recording/Recorder$OnStateChangedListener;

.field private mPhoneRecorderStatus:I

.field mStateListener:Lcom/mediatek/phone/recording/IPhoneRecordStateListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mMount:Z

    new-instance v0, Lcom/mediatek/phone/recording/PhoneRecorderServices$1;

    invoke-direct {v0, p0}, Lcom/mediatek/phone/recording/PhoneRecorderServices$1;-><init>(Lcom/mediatek/phone/recording/PhoneRecorderServices;)V

    iput-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mPhoneRecorderStatus:I

    new-instance v0, Lcom/mediatek/phone/recording/PhoneRecorderServices$2;

    invoke-direct {v0, p0}, Lcom/mediatek/phone/recording/PhoneRecorderServices$2;-><init>(Lcom/mediatek/phone/recording/PhoneRecorderServices;)V

    iput-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mPhoneRecorderStateListener:Lcom/mediatek/phone/recording/Recorder$OnStateChangedListener;

    new-instance v0, Lcom/mediatek/phone/recording/PhoneRecorderServices$3;

    invoke-direct {v0, p0}, Lcom/mediatek/phone/recording/PhoneRecorderServices$3;-><init>(Lcom/mediatek/phone/recording/PhoneRecorderServices;)V

    iput-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mBinder:Lcom/mediatek/phone/recording/IPhoneRecorder$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/phone/recording/PhoneRecorderServices;)Z
    .locals 1
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderServices;

    iget-boolean v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mMount:Z

    return v0
.end method

.method static synthetic access$002(Lcom/mediatek/phone/recording/PhoneRecorderServices;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderServices;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mMount:Z

    return p1
.end method

.method static synthetic access$100(Lcom/mediatek/phone/recording/PhoneRecorderServices;)I
    .locals 1
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderServices;

    iget v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mPhoneRecorderStatus:I

    return v0
.end method

.method static synthetic access$102(Lcom/mediatek/phone/recording/PhoneRecorderServices;I)I
    .locals 0
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderServices;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mPhoneRecorderStatus:I

    return p1
.end method

.method static synthetic access$200(Lcom/mediatek/phone/recording/PhoneRecorderServices;)Lcom/mediatek/phone/recording/PhoneRecorder;
    .locals 1
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderServices;

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mPhoneRecorder:Lcom/mediatek/phone/recording/PhoneRecorder;

    return-object v0
.end method

.method static synthetic access$202(Lcom/mediatek/phone/recording/PhoneRecorderServices;Lcom/mediatek/phone/recording/PhoneRecorder;)Lcom/mediatek/phone/recording/PhoneRecorder;
    .locals 0
    .param p0    # Lcom/mediatek/phone/recording/PhoneRecorderServices;
    .param p1    # Lcom/mediatek/phone/recording/PhoneRecorder;

    iput-object p1, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mPhoneRecorder:Lcom/mediatek/phone/recording/PhoneRecorder;

    return-object p1
.end method


# virtual methods
.method public log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "RecorderServices"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "RecorderServices"

    const-string v1, "onBind"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mBinder:Lcom/mediatek/phone/recording/IPhoneRecorder$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v1, "onCreate"

    invoke-virtual {p0, v1}, Lcom/mediatek/phone/recording/PhoneRecorderServices;->log(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/mediatek/phone/recording/PhoneRecorder;->getInstance(Landroid/content/Context;)Lcom/mediatek/phone/recording/PhoneRecorder;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mPhoneRecorder:Lcom/mediatek/phone/recording/PhoneRecorder;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mPhoneRecorder:Lcom/mediatek/phone/recording/PhoneRecorder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mPhoneRecorder:Lcom/mediatek/phone/recording/PhoneRecorder;

    iget-object v2, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mPhoneRecorderStateListener:Lcom/mediatek/phone/recording/Recorder$OnStateChangedListener;

    invoke-virtual {v1, v2}, Lcom/mediatek/phone/recording/Recorder;->setOnStateChangedListener(Lcom/mediatek/phone/recording/Recorder$OnStateChangedListener;)V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "onDestroy"

    invoke-virtual {p0, v0}, Lcom/mediatek/phone/recording/PhoneRecorderServices;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mPhoneRecorder:Lcom/mediatek/phone/recording/PhoneRecorder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mPhoneRecorder:Lcom/mediatek/phone/recording/PhoneRecorder;

    iget-boolean v1, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mMount:Z

    invoke-virtual {v0, v1}, Lcom/mediatek/phone/recording/PhoneRecorder;->stopRecord(Z)V

    iput-object v2, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mPhoneRecorder:Lcom/mediatek/phone/recording/PhoneRecorder;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    :cond_1
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    const-string v0, "onStart"

    invoke-virtual {p0, v0}, Lcom/mediatek/phone/recording/PhoneRecorderServices;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mPhoneRecorder:Lcom/mediatek/phone/recording/PhoneRecorder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/recording/PhoneRecorderServices;->mPhoneRecorder:Lcom/mediatek/phone/recording/PhoneRecorder;

    invoke-virtual {v0}, Lcom/mediatek/phone/recording/PhoneRecorder;->startRecord()V

    :cond_0
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "RecorderServices"

    const-string v1, "onUnbind"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
