.class Lcom/mediatek/phone/vt/VTInCallScreen$17;
.super Ljava/lang/Object;
.source "VTInCallScreen.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/phone/vt/VTInCallScreen;->showStartVTRecorderDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/phone/vt/VTInCallScreen;


# direct methods
.method constructor <init>(Lcom/mediatek/phone/vt/VTInCallScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/phone/vt/VTInCallScreen$17;->this$0:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v2, p0, Lcom/mediatek/phone/vt/VTInCallScreen$17;->this$0:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-static {v2}, Lcom/mediatek/phone/vt/VTInCallScreen;->access$2200(Lcom/mediatek/phone/vt/VTInCallScreen;)Landroid/app/AlertDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/phone/vt/VTInCallScreen$17;->this$0:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-static {v2}, Lcom/mediatek/phone/vt/VTInCallScreen;->access$2200(Lcom/mediatek/phone/vt/VTInCallScreen;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->dismiss()V

    iget-object v2, p0, Lcom/mediatek/phone/vt/VTInCallScreen$17;->this$0:Lcom/mediatek/phone/vt/VTInCallScreen;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/mediatek/phone/vt/VTInCallScreen;->access$2202(Lcom/mediatek/phone/vt/VTInCallScreen;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    :cond_0
    iget-object v2, p0, Lcom/mediatek/phone/vt/VTInCallScreen$17;->this$0:Lcom/mediatek/phone/vt/VTInCallScreen;

    iget-object v2, v2, Lcom/mediatek/phone/vt/VTInCallScreen;->mVTRecorderEntries:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mediatek/phone/vt/VTInCallScreen$17;->this$0:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0066

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/phone/vt/VTInCallScreen$17;->this$0:Lcom/mediatek/phone/vt/VTInCallScreen;

    const-string v3, "The choice of start VT recording : voice and peer video"

    invoke-static {v2, v3}, Lcom/mediatek/phone/vt/VTInCallScreen;->access$000(Lcom/mediatek/phone/vt/VTInCallScreen;Ljava/lang/String;)V

    const/4 v1, 0x1

    :goto_0
    iget-object v2, p0, Lcom/mediatek/phone/vt/VTInCallScreen$17;->this$0:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-static {v2, v1}, Lcom/mediatek/phone/vt/VTInCallScreen;->access$2300(Lcom/mediatek/phone/vt/VTInCallScreen;I)V

    :goto_1
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mediatek/phone/vt/VTInCallScreen$17;->this$0:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0067

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/phone/vt/VTInCallScreen$17;->this$0:Lcom/mediatek/phone/vt/VTInCallScreen;

    const-string v3, "The choice of start VT recording : only voice"

    invoke-static {v2, v3}, Lcom/mediatek/phone/vt/VTInCallScreen;->access$000(Lcom/mediatek/phone/vt/VTInCallScreen;Ljava/lang/String;)V

    const/4 v1, 0x2

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/mediatek/phone/vt/VTInCallScreen$17;->this$0:Lcom/mediatek/phone/vt/VTInCallScreen;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0068

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/phone/vt/VTInCallScreen$17;->this$0:Lcom/mediatek/phone/vt/VTInCallScreen;

    const-string v3, "The choice of start VT recording : only peer video"

    invoke-static {v2, v3}, Lcom/mediatek/phone/vt/VTInCallScreen;->access$000(Lcom/mediatek/phone/vt/VTInCallScreen;Ljava/lang/String;)V

    const/4 v1, 0x3

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/mediatek/phone/vt/VTInCallScreen$17;->this$0:Lcom/mediatek/phone/vt/VTInCallScreen;

    const-string v3, "The choice of start VT recording : wrong string"

    invoke-static {v2, v3}, Lcom/mediatek/phone/vt/VTInCallScreen;->access$000(Lcom/mediatek/phone/vt/VTInCallScreen;Ljava/lang/String;)V

    goto :goto_1
.end method
