.class Lcom/mediatek/settings/CallBarringBasePreference$1;
.super Ljava/lang/Object;
.source "CallBarringBasePreference.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/settings/CallBarringBasePreference;->doPreferenceClick(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/settings/CallBarringBasePreference;

.field final synthetic val$textEntryView:Landroid/view/View;

.field final synthetic val$title:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/mediatek/settings/CallBarringBasePreference;Landroid/view/View;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/settings/CallBarringBasePreference$1;->this$0:Lcom/mediatek/settings/CallBarringBasePreference;

    iput-object p2, p0, Lcom/mediatek/settings/CallBarringBasePreference$1;->val$textEntryView:Landroid/view/View;

    iput-object p3, p0, Lcom/mediatek/settings/CallBarringBasePreference$1;->val$title:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/mediatek/settings/CallBarringBasePreference$1;->val$textEntryView:Landroid/view/View;

    const v4, 0x7f080021

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/settings/CallBarringBasePreference$1;->this$0:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-static {v3, v1}, Lcom/mediatek/settings/CallBarringBasePreference;->access$100(Lcom/mediatek/settings/CallBarringBasePreference;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/mediatek/settings/CallBarringBasePreference$1;->this$0:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-static {v3}, Lcom/mediatek/settings/CallBarringBasePreference;->access$200(Lcom/mediatek/settings/CallBarringBasePreference;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/mediatek/settings/CallBarringBasePreference$1;->val$title:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/mediatek/settings/CallBarringBasePreference$1;->this$0:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-static {v3}, Lcom/mediatek/settings/CallBarringBasePreference;->access$200(Lcom/mediatek/settings/CallBarringBasePreference;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0d00ed

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v3, 0x7f0d019a

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/mediatek/settings/CallBarringBasePreference$1;->this$0:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-static {v3}, Lcom/mediatek/settings/CallBarringBasePreference;->access$300(Lcom/mediatek/settings/CallBarringBasePreference;)Lcom/android/phone/TimeConsumingPreferenceListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/settings/CallBarringBasePreference$1;->this$0:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-static {v3}, Lcom/mediatek/settings/CallBarringBasePreference;->access$300(Lcom/mediatek/settings/CallBarringBasePreference;)Lcom/android/phone/TimeConsumingPreferenceListener;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/settings/CallBarringBasePreference$1;->this$0:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-interface {v3, v4, v5}, Lcom/android/phone/TimeConsumingPreferenceListener;->onStarted(Landroid/preference/Preference;Z)V

    iget-object v3, p0, Lcom/mediatek/settings/CallBarringBasePreference$1;->this$0:Lcom/mediatek/settings/CallBarringBasePreference;

    iget-object v4, p0, Lcom/mediatek/settings/CallBarringBasePreference$1;->this$0:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-static {v4}, Lcom/mediatek/settings/CallBarringBasePreference;->access$400(Lcom/mediatek/settings/CallBarringBasePreference;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/settings/CallBarringBasePreference$1;->this$0:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-static {v5}, Lcom/mediatek/settings/CallBarringBasePreference;->access$500(Lcom/mediatek/settings/CallBarringBasePreference;)Z

    move-result v5

    invoke-static {v3, v4, v5, v1}, Lcom/mediatek/settings/CallBarringBasePreference;->access$600(Lcom/mediatek/settings/CallBarringBasePreference;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0
.end method
