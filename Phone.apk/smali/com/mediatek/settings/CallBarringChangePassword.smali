.class public Lcom/mediatek/settings/CallBarringChangePassword;
.super Lcom/android/phone/EditPinPreference;
.source "CallBarringChangePassword.java"

# interfaces
.implements Lcom/android/phone/EditPinPreference$OnPinEnteredListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/settings/CallBarringChangePassword$1;,
        Lcom/mediatek/settings/CallBarringChangePassword$MyHandler;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field public static final DEFAULT_SIM:I = 0x2

.field private static final LOG_TAG:Ljava/lang/String; = "Settings/CallChangePassword"

.field private static final PASSWORD_CHANGE_NEW:I = 0x1

.field private static final PASSWORD_CHANGE_OLD:I = 0x0

.field private static final PASSWORD_CHANGE_REENTER:I = 0x2

.field private static final PASSWORD_LENGTH:I = 0x4


# instance fields
.field private mCallBarringInterface:Lcom/mediatek/settings/CallBarringInterface;

.field private mContext:Landroid/content/Context;

.field private mHandler:Lcom/mediatek/settings/CallBarringChangePassword$MyHandler;

.field private mNewPassword:Ljava/lang/String;

.field private mOldPassword:Ljava/lang/String;

.field private mPasswordChangeState:I

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mSimId:I

.field private mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/mediatek/settings/CallBarringChangePassword;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/mediatek/settings/CallBarringChangePassword;->init(Landroid/content/Context;)V

    invoke-static {}, Lcom/android/phone/PhoneApp;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mPhone:Lcom/android/internal/telephony/Phone;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/phone/EditPinPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/mediatek/settings/CallBarringChangePassword$MyHandler;

    invoke-direct {v0, p0, v1}, Lcom/mediatek/settings/CallBarringChangePassword$MyHandler;-><init>(Lcom/mediatek/settings/CallBarringChangePassword;Lcom/mediatek/settings/CallBarringChangePassword$1;)V

    iput-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mHandler:Lcom/mediatek/settings/CallBarringChangePassword$MyHandler;

    iput-object v1, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    iput-object v1, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mContext:Landroid/content/Context;

    iput-object v1, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mCallBarringInterface:Lcom/mediatek/settings/CallBarringInterface;

    const/4 v0, 0x2

    iput v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mSimId:I

    invoke-direct {p0, p1}, Lcom/mediatek/settings/CallBarringChangePassword;->init(Landroid/content/Context;)V

    invoke-static {}, Lcom/android/phone/PhoneApp;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mPhone:Lcom/android/internal/telephony/Phone;

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/settings/CallBarringChangePassword;)Lcom/android/phone/TimeConsumingPreferenceListener;
    .locals 1
    .param p0    # Lcom/mediatek/settings/CallBarringChangePassword;

    iget-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/settings/CallBarringChangePassword;)Lcom/mediatek/settings/CallBarringInterface;
    .locals 1
    .param p0    # Lcom/mediatek/settings/CallBarringChangePassword;

    iget-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mCallBarringInterface:Lcom/mediatek/settings/CallBarringInterface;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/settings/CallBarringChangePassword;I)V
    .locals 0
    .param p0    # Lcom/mediatek/settings/CallBarringChangePassword;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/settings/CallBarringChangePassword;->displayMessage(I)V

    return-void
.end method

.method private final displayMessage(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private final displayPasswordChangeDialog(IZ)V
    .locals 3
    .param p1    # I
    .param p2    # Z

    const/4 v0, 0x0

    iget v1, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mPasswordChangeState:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    if-eqz p1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/preference/DialogPreference;->setDialogMessage(Ljava/lang/CharSequence;)V

    :goto_1
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/android/phone/EditPinPreference;->showPinDialog()V

    :cond_0
    return-void

    :pswitch_0
    const v0, 0x7f0d00e6

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0d00e7

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0d00e9

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Landroid/preference/DialogPreference;->setDialogMessage(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private doChangePassword(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v3, 0x0

    const-string v0, "Settings/CallChangePassword"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doChangePassword() is called with oldPassword is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "newPassword is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mHandler:Lcom/mediatek/settings/CallBarringChangePassword$MyHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v3, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-static {}, Lcom/mediatek/settings/CallSettings;->isMultipleSim()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mPhone:Lcom/android/internal/telephony/Phone;

    check-cast v0, Lcom/android/internal/telephony/gemini/GeminiPhone;

    const-string v1, "AB"

    iget v5, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mSimId:I

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/telephony/gemini/GeminiPhone;->changeBarringPasswordGemini(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mPhone:Lcom/android/internal/telephony/Phone;

    const-string v1, "AB"

    invoke-interface {v0, v1, p1, p2, v4}, Lcom/android/internal/telephony/Phone;->changeBarringPassword(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    goto :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mContext:Landroid/content/Context;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    invoke-virtual {p0, p0}, Lcom/android/phone/EditPinPreference;->setOnPinEnteredListener(Lcom/android/phone/EditPinPreference$OnPinEnteredListener;)V

    invoke-direct {p0}, Lcom/mediatek/settings/CallBarringChangePassword;->resetPasswordChangeState()V

    return-void
.end method

.method private final resetPasswordChangeState()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mPasswordChangeState:I

    invoke-direct {p0, v0, v0}, Lcom/mediatek/settings/CallBarringChangePassword;->displayPasswordChangeDialog(IZ)V

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mOldPassword:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mNewPassword:Ljava/lang/String;

    return-void
.end method

.method private validatePassword(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onPinEntered(Lcom/android/phone/EditPinPreference;Z)V
    .locals 4
    .param p1    # Lcom/android/phone/EditPinPreference;
    .param p2    # Z

    const v1, 0x7f0d00e8

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-nez p2, :cond_0

    invoke-direct {p0}, Lcom/mediatek/settings/CallBarringChangePassword;->resetPasswordChangeState()V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mPasswordChangeState:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mOldPassword:Ljava/lang/String;

    const-string v0, ""

    invoke-virtual {p0, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mOldPassword:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/mediatek/settings/CallBarringChangePassword;->validatePassword(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput v2, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mPasswordChangeState:I

    invoke-direct {p0, v3, v2}, Lcom/mediatek/settings/CallBarringChangePassword;->displayPasswordChangeDialog(IZ)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1, v2}, Lcom/mediatek/settings/CallBarringChangePassword;->displayPasswordChangeDialog(IZ)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mNewPassword:Ljava/lang/String;

    const-string v0, ""

    invoke-virtual {p0, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mNewPassword:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/mediatek/settings/CallBarringChangePassword;->validatePassword(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    iput v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mPasswordChangeState:I

    invoke-direct {p0, v3, v2}, Lcom/mediatek/settings/CallBarringChangePassword;->displayPasswordChangeDialog(IZ)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v1, v2}, Lcom/mediatek/settings/CallBarringChangePassword;->displayPasswordChangeDialog(IZ)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mNewPassword:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iput v2, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mPasswordChangeState:I

    const-string v0, ""

    invoke-virtual {p0, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    const v0, 0x7f0d00ea

    invoke-direct {p0, v0, v2}, Lcom/mediatek/settings/CallBarringChangePassword;->displayPasswordChangeDialog(IZ)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    invoke-interface {v0, p0, v3}, Lcom/android/phone/TimeConsumingPreferenceListener;->onStarted(Landroid/preference/Preference;Z)V

    :cond_4
    iget-object v0, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mOldPassword:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mNewPassword:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/mediatek/settings/CallBarringChangePassword;->doChangePassword(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/settings/CallBarringChangePassword;->resetPasswordChangeState()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setTimeConsumingListener(Lcom/android/phone/TimeConsumingPreferenceListener;I)V
    .locals 0
    .param p1    # Lcom/android/phone/TimeConsumingPreferenceListener;
    .param p2    # I

    iput-object p1, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mTcpListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    check-cast p1, Lcom/mediatek/settings/CallBarringInterface;

    iput-object p1, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mCallBarringInterface:Lcom/mediatek/settings/CallBarringInterface;

    iput p2, p0, Lcom/mediatek/settings/CallBarringChangePassword;->mSimId:I

    return-void
.end method
