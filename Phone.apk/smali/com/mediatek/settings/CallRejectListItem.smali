.class public Lcom/mediatek/settings/CallRejectListItem;
.super Ljava/lang/Object;
.source "CallRejectListItem.java"


# instance fields
.field private id:Ljava/lang/String;

.field private isChecked:Z

.field private name:Ljava/lang/String;

.field private phoneNum:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/settings/CallRejectListItem;->name:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/settings/CallRejectListItem;->phoneNum:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mediatek/settings/CallRejectListItem;->id:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/settings/CallRejectListItem;->isChecked:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/settings/CallRejectListItem;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/settings/CallRejectListItem;->phoneNum:Ljava/lang/String;

    iput-object p3, p0, Lcom/mediatek/settings/CallRejectListItem;->id:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/mediatek/settings/CallRejectListItem;->isChecked:Z

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/settings/CallRejectListItem;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getIsChecked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/settings/CallRejectListItem;->isChecked:Z

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/settings/CallRejectListItem;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNum()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/settings/CallRejectListItem;->phoneNum:Ljava/lang/String;

    return-object v0
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/settings/CallRejectListItem;->id:Ljava/lang/String;

    return-void
.end method

.method public setIsChecked(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/settings/CallRejectListItem;->isChecked:Z

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/settings/CallRejectListItem;->name:Ljava/lang/String;

    return-void
.end method

.method public setPhoneNum(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/settings/CallRejectListItem;->phoneNum:Ljava/lang/String;

    return-void
.end method
