.class public Lcom/mediatek/settings/CellBroadcastActivity;
.super Lcom/android/phone/TimeConsumingPreferenceActivity;
.source "CellBroadcastActivity.java"


# static fields
.field private static final BUTTON_CB_CHECKBOX_KEY:Ljava/lang/String; = "enable_cellBroadcast"

.field private static final BUTTON_CB_SETTINGS_KEY:Ljava/lang/String; = "cbsettings"


# instance fields
.field cbCheckBox:Lcom/mediatek/settings/CellBroadcastCheckBox;

.field cbSetting:Landroid/preference/Preference;

.field mSimId:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/phone/TimeConsumingPreferenceActivity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mSimId:I

    iput-object v1, p0, Lcom/mediatek/settings/CellBroadcastActivity;->cbCheckBox:Lcom/mediatek/settings/CellBroadcastCheckBox;

    iput-object v1, p0, Lcom/mediatek/settings/CellBroadcastActivity;->cbSetting:Landroid/preference/Preference;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f06000a

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "simId"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mSimId:I

    const-string v0, "enable_cellBroadcast"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/mediatek/settings/CellBroadcastCheckBox;

    iput-object v0, p0, Lcom/mediatek/settings/CellBroadcastActivity;->cbCheckBox:Lcom/mediatek/settings/CellBroadcastCheckBox;

    const-string v0, "cbsettings"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/settings/CellBroadcastActivity;->cbSetting:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastActivity;->cbCheckBox:Lcom/mediatek/settings/CellBroadcastCheckBox;

    iget-object v0, p0, Lcom/mediatek/settings/CellBroadcastActivity;->cbCheckBox:Lcom/mediatek/settings/CellBroadcastCheckBox;

    invoke-virtual {v0}, Landroid/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0d0114

    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/mediatek/settings/MultipleSimActivity;->SUB_TITLE_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/mediatek/settings/MultipleSimActivity;->SUB_TITLE_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/settings/CellBroadcastActivity;->cbCheckBox:Lcom/mediatek/settings/CellBroadcastCheckBox;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/settings/CellBroadcastActivity;->cbCheckBox:Lcom/mediatek/settings/CellBroadcastCheckBox;

    iget v1, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mSimId:I

    invoke-virtual {v0, p0, v2, v1}, Lcom/mediatek/settings/CellBroadcastCheckBox;->init(Lcom/android/phone/TimeConsumingPreferenceListener;ZI)V

    :cond_1
    return-void

    :cond_2
    const v0, 0x7f0d0115

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Landroid/preference/Preference;

    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastActivity;->cbSetting:Landroid/preference/Preference;

    if-ne p2, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "simId"

    iget v2, p0, Lcom/mediatek/settings/CellBroadcastActivity;->mSimId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
