.class public Lcom/mediatek/settings/CellBroadcastCheckBox;
.super Landroid/preference/CheckBoxPreference;
.source "CellBroadcastCheckBox.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/settings/CellBroadcastCheckBox$1;,
        Lcom/mediatek/settings/CellBroadcastCheckBox$MyHandler;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final LOG_TAG:Ljava/lang/String; = "Settings/CellBroadcastCheckBox"

.field private static final MESSAGE_GET_STATE:I = 0x64

.field private static final MESSAGE_SET_STATE:I = 0x65


# instance fields
.field private lastCheckStatus:Z

.field private mHandler:Lcom/mediatek/settings/CellBroadcastCheckBox$MyHandler;

.field private mListener:Lcom/android/phone/TimeConsumingPreferenceListener;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field mSimId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/mediatek/settings/CellBroadcastCheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/mediatek/settings/CellBroadcastCheckBox$MyHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/mediatek/settings/CellBroadcastCheckBox$MyHandler;-><init>(Lcom/mediatek/settings/CellBroadcastCheckBox;Lcom/mediatek/settings/CellBroadcastCheckBox$1;)V

    iput-object v0, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mHandler:Lcom/mediatek/settings/CellBroadcastCheckBox$MyHandler;

    invoke-static {}, Lcom/android/phone/PhoneApp;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mPhone:Lcom/android/internal/telephony/Phone;

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/settings/CellBroadcastCheckBox;)Lcom/android/phone/TimeConsumingPreferenceListener;
    .locals 1
    .param p0    # Lcom/mediatek/settings/CellBroadcastCheckBox;

    iget-object v0, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/settings/CellBroadcastCheckBox;)Z
    .locals 1
    .param p0    # Lcom/mediatek/settings/CellBroadcastCheckBox;

    iget-boolean v0, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->lastCheckStatus:Z

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/settings/CellBroadcastCheckBox;Z)V
    .locals 0
    .param p0    # Lcom/mediatek/settings/CellBroadcastCheckBox;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mediatek/settings/CellBroadcastCheckBox;->getCBState(Z)V

    return-void
.end method

.method private getCBState(Z)V
    .locals 6
    .param p1    # Z

    const/4 v5, 0x0

    const/16 v4, 0x65

    const/4 v3, 0x0

    const/16 v2, 0x64

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mHandler:Lcom/mediatek/settings/CellBroadcastCheckBox$MyHandler;

    invoke-virtual {v1, v2, v3, v2, v5}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    :goto_0
    invoke-static {}, Lcom/mediatek/settings/CallSettings;->isMultipleSim()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mPhone:Lcom/android/internal/telephony/Phone;

    check-cast v1, Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget v2, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mSimId:I

    invoke-virtual {v1, v0, v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->queryCellBroadcastSmsActivationGemini(Landroid/os/Message;I)V

    :goto_1
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/os/Message;->arg2:I

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    invoke-interface {v1, p0, p1}, Lcom/android/phone/TimeConsumingPreferenceListener;->onStarted(Landroid/preference/Preference;Z)V

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mHandler:Lcom/mediatek/settings/CellBroadcastCheckBox$MyHandler;

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v1, v0}, Lcom/android/internal/telephony/Phone;->queryCellBroadcastSmsActivation(Landroid/os/Message;)V

    goto :goto_1
.end method

.method private getPrefContext()Landroid/content/Context;
    .locals 1

    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method private setCBState(I)V
    .locals 5
    .param p1    # I

    const/16 v4, 0x65

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mHandler:Lcom/mediatek/settings/CellBroadcastCheckBox$MyHandler;

    const/4 v2, 0x0

    invoke-virtual {v1, v4, v3, v4, v2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-static {}, Lcom/mediatek/settings/CallSettings;->isMultipleSim()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mPhone:Lcom/android/internal/telephony/Phone;

    check-cast v1, Lcom/android/internal/telephony/gemini/GeminiPhone;

    iget v2, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mSimId:I

    invoke-virtual {v1, p1, v0, v2}, Lcom/android/internal/telephony/gemini/GeminiPhone;->activateCellBroadcastSmsGemini(ILandroid/os/Message;I)V

    :goto_0
    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    invoke-interface {v1, p0, v3}, Lcom/android/phone/TimeConsumingPreferenceListener;->onStarted(Landroid/preference/Preference;Z)V

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v1, p1, v0}, Lcom/android/internal/telephony/Phone;->activateCellBroadcastSms(ILandroid/os/Message;)V

    goto :goto_0
.end method


# virtual methods
.method public final displayMessage(I)V
    .locals 3
    .param p1    # I

    invoke-direct {p0}, Lcom/mediatek/settings/CellBroadcastCheckBox;->getPrefContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0}, Lcom/mediatek/settings/CellBroadcastCheckBox;->getPrefContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public final displayMessage(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/mediatek/settings/CellBroadcastCheckBox;->getPrefContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method init(Lcom/android/phone/TimeConsumingPreferenceListener;ZI)V
    .locals 6
    .param p1    # Lcom/android/phone/TimeConsumingPreferenceListener;
    .param p2    # Z
    .param p3    # I

    const/4 v5, 0x0

    const-string v2, "Settings/CellBroadcastCheckBox"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "init, simId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mListener:Lcom/android/phone/TimeConsumingPreferenceListener;

    iput p3, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mSimId:I

    if-nez p2, :cond_0

    check-cast p1, Lcom/mediatek/settings/CellBroadcastActivity;

    const-string v2, "phone"

    invoke-virtual {p1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    invoke-static {}, Lcom/mediatek/settings/CallSettings;->isMultipleSim()Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->mSimId:I

    invoke-virtual {v1, v2}, Landroid/telephony/TelephonyManager;->hasIccCardGemini(I)Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/mediatek/settings/CellBroadcastCheckBox;->getCBState(Z)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->hasIccCard()Z

    move-result v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v5}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    invoke-virtual {p0, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_1
.end method

.method protected onClick()V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-super {p0}, Landroid/preference/TwoStatePreference;->onClick()V

    invoke-virtual {p0}, Landroid/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/mediatek/settings/CellBroadcastCheckBox;->lastCheckStatus:Z

    if-eqz v0, :cond_1

    move v1, v3

    :goto_1
    invoke-direct {p0, v1}, Lcom/mediatek/settings/CellBroadcastCheckBox;->setCBState(I)V

    if-nez v0, :cond_2

    :goto_2
    invoke-virtual {p0, v2}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    return-void

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method

.method public updateSummary(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    const v0, 0x7f0d0115

    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setSummary(I)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f0d0114

    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0
.end method
