.class public Lcom/mediatek/settings/CellBroadcastLanguage;
.super Ljava/lang/Object;
.source "CellBroadcastLanguage.java"


# instance fields
.field private mLanguageId:I

.field private mLanguageName:Ljava/lang/String;

.field private mLanguageState:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Z)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mediatek/settings/CellBroadcastLanguage;->mLanguageId:I

    iput-object p2, p0, Lcom/mediatek/settings/CellBroadcastLanguage;->mLanguageName:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/mediatek/settings/CellBroadcastLanguage;->mLanguageState:Z

    return-void
.end method


# virtual methods
.method public getLanguageId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/settings/CellBroadcastLanguage;->mLanguageId:I

    return v0
.end method

.method public getLanguageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/settings/CellBroadcastLanguage;->mLanguageName:Ljava/lang/String;

    return-object v0
.end method

.method public getLanguageState()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/settings/CellBroadcastLanguage;->mLanguageState:Z

    return v0
.end method

.method public setLanguageId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/settings/CellBroadcastLanguage;->mLanguageId:I

    return-void
.end method

.method public setLanguageName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/settings/CellBroadcastLanguage;->mLanguageName:Ljava/lang/String;

    return-void
.end method

.method public setLanguageState(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/settings/CellBroadcastLanguage;->mLanguageState:Z

    return-void
.end method
