.class Lcom/mediatek/settings/CellBroadcastSettings$6;
.super Ljava/lang/Object;
.source "CellBroadcastSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/settings/CellBroadcastSettings;->showLanguageSelectDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/settings/CellBroadcastSettings;

.field final synthetic val$temp:[Z

.field final synthetic val$temp2:[Z


# direct methods
.method constructor <init>(Lcom/mediatek/settings/CellBroadcastSettings;[Z[Z)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    iput-object p2, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->val$temp:[Z

    iput-object p3, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->val$temp2:[Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v5, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->val$temp:[Z

    array-length v4, v5

    iget-object v5, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->val$temp:[Z

    const/4 v6, 0x0

    aget-boolean v5, v5, v6

    if-eqz v5, :cond_0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    iget-object v5, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->val$temp:[Z

    const/4 v6, 0x1

    aput-boolean v6, v5, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_2

    iget-object v5, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v5}, Lcom/mediatek/settings/CellBroadcastSettings;->access$2300(Lcom/mediatek/settings/CellBroadcastSettings;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/settings/CellBroadcastLanguage;

    iget-object v6, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->val$temp:[Z

    aget-boolean v6, v6, v2

    invoke-virtual {v5, v6}, Lcom/mediatek/settings/CellBroadcastLanguage;->setLanguageState(Z)V

    iget-object v5, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->val$temp:[Z

    aget-boolean v5, v5, v2

    if-eqz v5, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_4

    iget-object v5, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v5}, Lcom/mediatek/settings/CellBroadcastSettings;->access$2400(Lcom/mediatek/settings/CellBroadcastSettings;)[Lcom/android/internal/telephony/gsm/SmsBroadcastConfigInfo;

    move-result-object v3

    :try_start_0
    iget-object v5, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v5, v3}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1000(Lcom/mediatek/settings/CellBroadcastSettings;[Lcom/android/internal/telephony/gsm/SmsBroadcastConfigInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    return-void

    :catch_0
    move-exception v0

    iget-object v5, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v5}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1100(Lcom/mediatek/settings/CellBroadcastSettings;)V

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v4, :cond_3

    iget-object v5, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v5}, Lcom/mediatek/settings/CellBroadcastSettings;->access$2300(Lcom/mediatek/settings/CellBroadcastSettings;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/settings/CellBroadcastLanguage;

    iget-object v6, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->val$temp2:[Z

    aget-boolean v6, v6, v2

    invoke-virtual {v5, v6}, Lcom/mediatek/settings/CellBroadcastLanguage;->setLanguageState(Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    iget-object v5, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    const v6, 0x7f0d00fd

    invoke-static {v5, v6}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1200(Lcom/mediatek/settings/CellBroadcastSettings;I)V

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v4, :cond_3

    iget-object v5, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v5}, Lcom/mediatek/settings/CellBroadcastSettings;->access$2300(Lcom/mediatek/settings/CellBroadcastSettings;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/settings/CellBroadcastLanguage;

    iget-object v6, p0, Lcom/mediatek/settings/CellBroadcastSettings$6;->val$temp2:[Z

    aget-boolean v6, v6, v2

    invoke-virtual {v5, v6}, Lcom/mediatek/settings/CellBroadcastLanguage;->setLanguageState(Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method
