.class Lcom/mediatek/settings/CellBroadcastSettings$7;
.super Ljava/lang/Object;
.source "CellBroadcastSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnMultiChoiceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/settings/CellBroadcastSettings;->showLanguageSelectDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/settings/CellBroadcastSettings;

.field final synthetic val$temp:[Z


# direct methods
.method constructor <init>(Lcom/mediatek/settings/CellBroadcastSettings;[Z)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/settings/CellBroadcastSettings$7;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    iput-object p2, p0, Lcom/mediatek/settings/CellBroadcastSettings$7;->val$temp:[Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;IZ)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I
    .param p3    # Z

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/mediatek/settings/CellBroadcastSettings$7;->val$temp:[Z

    aput-boolean p3, v3, p2

    const/4 v2, 0x0

    instance-of v3, p1, Landroid/app/AlertDialog;

    if-eqz v3, :cond_0

    move-object v2, p1

    check-cast v2, Landroid/app/AlertDialog;

    :cond_0
    if-nez p2, :cond_1

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, Lcom/mediatek/settings/CellBroadcastSettings$7;->val$temp:[Z

    array-length v3, v3

    if-ge v0, v3, :cond_2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0, p3}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    iget-object v3, p0, Lcom/mediatek/settings/CellBroadcastSettings$7;->val$temp:[Z

    aput-boolean p3, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    if-nez p3, :cond_2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v4, p3}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    iget-object v3, p0, Lcom/mediatek/settings/CellBroadcastSettings$7;->val$temp:[Z

    aput-boolean v4, v3, v4

    :cond_2
    return-void
.end method
