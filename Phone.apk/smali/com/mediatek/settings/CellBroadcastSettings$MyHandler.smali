.class Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;
.super Landroid/os/Handler;
.source "CellBroadcastSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/settings/CellBroadcastSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/settings/CellBroadcastSettings;


# direct methods
.method private constructor <init>(Lcom/mediatek/settings/CellBroadcastSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/settings/CellBroadcastSettings;Lcom/mediatek/settings/CellBroadcastSettings$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/settings/CellBroadcastSettings;
    .param p2    # Lcom/mediatek/settings/CellBroadcastSettings$1;

    invoke-direct {p0, p1}, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;-><init>(Lcom/mediatek/settings/CellBroadcastSettings;)V

    return-void
.end method

.method private handleGetCellBroadcastConfigResponse(Landroid/os/Message;)V
    .locals 8
    .param p1    # Landroid/os/Message;

    const/16 v7, 0x190

    const/16 v6, 0x64

    const/4 v5, 0x0

    iget v2, p1, Landroid/os/Message;->arg2:I

    if-ne v2, v6, :cond_1

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    iget-object v3, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v3}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1600(Lcom/mediatek/settings/CellBroadcastSettings;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onFinished(Landroid/preference/Preference;Z)V

    :goto_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    if-nez v0, :cond_2

    const-string v2, "Settings/CellBroadcastSettings"

    const-string v3, "handleGetCellBroadcastConfigResponse,ar is null"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    iget-object v3, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v3}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1600(Lcom/mediatek/settings/CellBroadcastSettings;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v2, v3, v7}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onError(Landroid/preference/Preference;I)V

    iget v2, p1, Landroid/os/Message;->arg2:I

    if-ne v2, v6, :cond_0

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v2}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1600(Lcom/mediatek/settings/CellBroadcastSettings;)Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/preference/PreferenceGroup;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v2}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1700(Lcom/mediatek/settings/CellBroadcastSettings;)Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/preference/PreferenceGroup;->setEnabled(Z)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    iget-object v3, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v3}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1600(Lcom/mediatek/settings/CellBroadcastSettings;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onFinished(Landroid/preference/Preference;Z)V

    goto :goto_0

    :cond_2
    iget-object v2, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v2, :cond_3

    const-string v2, "Settings/CellBroadcastSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleGetCellBroadcastConfigResponse: ar.exception="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    iget-object v3, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v3}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1600(Lcom/mediatek/settings/CellBroadcastSettings;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    const/16 v4, 0x12c

    invoke-virtual {v2, v3, v4}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onError(Landroid/preference/Preference;I)V

    iget v2, p1, Landroid/os/Message;->arg2:I

    if-ne v2, v6, :cond_0

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v2}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1600(Lcom/mediatek/settings/CellBroadcastSettings;)Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/preference/PreferenceGroup;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v2}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1700(Lcom/mediatek/settings/CellBroadcastSettings;)Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/preference/PreferenceGroup;->setEnabled(Z)V

    goto :goto_1

    :cond_3
    if-eqz v0, :cond_4

    iget-object v2, v0, Landroid/os/AsyncResult;->userObj:Ljava/lang/Object;

    instance-of v2, v2, Ljava/lang/Throwable;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    iget-object v3, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v3}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1600(Lcom/mediatek/settings/CellBroadcastSettings;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v2, v3, v7}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onError(Landroid/preference/Preference;I)V

    goto :goto_1

    :cond_4
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    if-eqz v2, :cond_6

    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v2}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1800(Lcom/mediatek/settings/CellBroadcastSettings;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v2}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1900(Lcom/mediatek/settings/CellBroadcastSettings;)V

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v2, v1}, Lcom/mediatek/settings/CellBroadcastSettings;->access$2000(Lcom/mediatek/settings/CellBroadcastSettings;Ljava/util/ArrayList;)V

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v2}, Lcom/mediatek/settings/CellBroadcastSettings;->access$900(Lcom/mediatek/settings/CellBroadcastSettings;)V

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v2}, Lcom/mediatek/settings/CellBroadcastSettings;->access$2100(Lcom/mediatek/settings/CellBroadcastSettings;)V

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v2}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1100(Lcom/mediatek/settings/CellBroadcastSettings;)V

    goto/16 :goto_1

    :cond_6
    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    iget-object v3, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v3}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1600(Lcom/mediatek/settings/CellBroadcastSettings;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v2, v3, v7}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onError(Landroid/preference/Preference;I)V

    const-string v2, "Settings/CellBroadcastSettings"

    const-string v3, "handleGetCellBroadcastConfigResponse: ar.result is null"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p1, Landroid/os/Message;->arg2:I

    if-ne v2, v6, :cond_0

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v2}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1600(Lcom/mediatek/settings/CellBroadcastSettings;)Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/preference/PreferenceGroup;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v2}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1700(Lcom/mediatek/settings/CellBroadcastSettings;)Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/preference/PreferenceGroup;->setEnabled(Z)V

    goto/16 :goto_1
.end method

.method private handleSetCellBroadcastConfigResponse(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    iget v1, p1, Landroid/os/Message;->arg2:I

    const/16 v2, 0x65

    if-ne v1, v2, :cond_2

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    if-nez v0, :cond_0

    const-string v1, "Settings/CellBroadcastSettings"

    const-string v2, "handleSetCellBroadcastConfigResponse,ar is null"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v2}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1600(Lcom/mediatek/settings/CellBroadcastSettings;)Landroid/preference/PreferenceScreen;

    move-result-object v2

    const/16 v3, 0x190

    invoke-virtual {v1, v2, v3}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onError(Landroid/preference/Preference;I)V

    :cond_0
    iget-object v1, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v1, :cond_1

    const-string v1, "Settings/CellBroadcastSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleSetCellBroadcastConfigResponse: ar.exception="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    iget-object v2, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    invoke-static {v2}, Lcom/mediatek/settings/CellBroadcastSettings;->access$1600(Lcom/mediatek/settings/CellBroadcastSettings;)Landroid/preference/PreferenceScreen;

    move-result-object v2

    const/16 v3, 0x12c

    invoke-virtual {v1, v2, v3}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onError(Landroid/preference/Preference;I)V

    :cond_1
    iget-object v1, p0, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->this$0:Lcom/mediatek/settings/CellBroadcastSettings;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/mediatek/settings/CellBroadcastSettings;->access$2200(Lcom/mediatek/settings/CellBroadcastSettings;Z)V

    :cond_2
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->handleGetCellBroadcastConfigResponse(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/mediatek/settings/CellBroadcastSettings$MyHandler;->handleSetCellBroadcastConfigResponse(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
