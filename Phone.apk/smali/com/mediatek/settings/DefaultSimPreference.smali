.class public Lcom/mediatek/settings/DefaultSimPreference;
.super Landroid/preference/DialogPreference;
.source "DefaultSimPreference.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/settings/DefaultSimPreference$SelectionListAdapter;
    }
.end annotation


# static fields
.field private static final DISPLAY_FIRST_FOUR:I = 0x1

.field private static final DISPLAY_LAST_FOUR:I = 0x2

.field private static final DISPLAY_NONE:I = 0x0

.field private static final PIN1_REQUEST_CODE:I = 0x12e

.field static final TAG:Ljava/lang/String; = "DefaultSimPreference"


# instance fields
.field private mAdapter:Lcom/mediatek/settings/DefaultSimPreference$SelectionListAdapter;

.field private mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

.field private mContext:Landroid/content/Context;

.field private mFlater:Landroid/view/LayoutInflater;

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mInitValue:I

.field private mListView:Landroid/widget/ListView;

.field private mSelected:I

.field private mSimItemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/settings/SimItem;",
            ">;"
        }
    .end annotation
.end field

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/mediatek/settings/DefaultSimPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSelected:I

    iput v0, p0, Lcom/mediatek/settings/DefaultSimPreference;->mInitValue:I

    iput-object p1, p0, Lcom/mediatek/settings/DefaultSimPreference;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/settings/DefaultSimPreference;->mFlater:Landroid/view/LayoutInflater;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/settings/DefaultSimPreference;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/mediatek/settings/DefaultSimPreference;

    iget-object v0, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSimItemList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mediatek/settings/DefaultSimPreference;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0    # Lcom/mediatek/settings/DefaultSimPreference;

    iget-object v0, p0, Lcom/mediatek/settings/DefaultSimPreference;->mFlater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/settings/DefaultSimPreference;)I
    .locals 1
    .param p0    # Lcom/mediatek/settings/DefaultSimPreference;

    iget v0, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSelected:I

    return v0
.end method

.method static synthetic access$300(Lcom/mediatek/settings/DefaultSimPreference;I)I
    .locals 1
    .param p0    # Lcom/mediatek/settings/DefaultSimPreference;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/settings/DefaultSimPreference;->getStatusResource(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/mediatek/settings/DefaultSimPreference;I)I
    .locals 1
    .param p0    # Lcom/mediatek/settings/DefaultSimPreference;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/settings/DefaultSimPreference;->getSimColorResource(I)I

    move-result v0

    return v0
.end method

.method private getSimColorResource(I)I
    .locals 1
    .param p1    # I

    if-ltz p1, :cond_0

    const/4 v0, 0x4

    if-gt p1, v0, :cond_0

    sget-object v0, Landroid/provider/Telephony;->SIMBackgroundRes:[I

    aget v0, v0, p1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private getStatusResource(I)I
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x20200f7

    goto :goto_0

    :pswitch_2
    const v0, 0x20200e4

    goto :goto_0

    :pswitch_3
    const v0, 0x20200e1

    goto :goto_0

    :pswitch_4
    const v0, 0x20200fe

    goto :goto_0

    :pswitch_5
    const v0, 0x20200fc

    goto :goto_0

    :pswitch_6
    const v0, 0x20200dc

    goto :goto_0

    :pswitch_7
    const v0, 0x20200fd

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private updateData()V
    .locals 8

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSimItemList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mediatek/settings/SimItem;

    iget-boolean v5, v3, Lcom/mediatek/settings/SimItem;->mIsSim:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/settings/DefaultSimPreference;->mContext:Landroid/content/Context;

    iget-object v6, v3, Lcom/mediatek/settings/SimItem;->mSiminfo:Landroid/provider/Telephony$SIMInfo;

    iget-wide v6, v6, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    invoke-static {v5, v6, v7}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoById(Landroid/content/Context;J)Landroid/provider/Telephony$SIMInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v4, Lcom/mediatek/settings/SimItem;

    invoke-direct {v4, v2}, Lcom/mediatek/settings/SimItem;-><init>(Landroid/provider/Telephony$SIMInfo;)V

    iget-object v5, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSimItemList:Ljava/util/List;

    invoke-interface {v5, v1, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public SetCellConnMgr(Lcom/mediatek/CellConnService/CellConnMgr;)V
    .locals 0
    .param p1    # Lcom/mediatek/CellConnService/CellConnMgr;

    iput-object p1, p0, Lcom/mediatek/settings/DefaultSimPreference;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    return-void
.end method

.method getValue()J
    .locals 2

    iget-object v0, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSimItemList:Ljava/util/List;

    iget v1, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSelected:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/settings/SimItem;

    iget-object v0, v0, Lcom/mediatek/settings/SimItem;->mSiminfo:Landroid/provider/Telephony$SIMInfo;

    iget-wide v0, v0, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    return-wide v0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    const v1, 0x1020010

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    :cond_0
    const-string v1, "DefaultSimPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "summary is +"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 5
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    const-string v2, "DefaultSimPreference"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDialogClosed---mSelected = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSelected:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "DefaultSimPreference"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDialogClosed---mInitValue = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/settings/DefaultSimPreference;->mInitValue:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iget v2, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSelected:I

    if-ltz v2, :cond_0

    iget v2, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSelected:I

    iget v3, p0, Lcom/mediatek/settings/DefaultSimPreference;->mInitValue:I

    if-eq v2, v3, :cond_0

    const-string v2, "DefaultSimPreference"

    const-string v3, "callChangeListener"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSimItemList:Ljava/util/List;

    iget v3, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSelected:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/settings/SimItem;

    iget-object v2, v2, Lcom/mediatek/settings/SimItem;->mSiminfo:Landroid/provider/Telephony$SIMInfo;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSimItemList:Ljava/util/List;

    iget v3, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSelected:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/settings/SimItem;

    iget-object v2, v2, Lcom/mediatek/settings/SimItem;->mSiminfo:Landroid/provider/Telephony$SIMInfo;

    iget-wide v0, v2, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/preference/Preference;->callChangeListener(Ljava/lang/Object;)Z

    iget v2, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSelected:I

    iput v2, p0, Lcom/mediatek/settings/DefaultSimPreference;->mInitValue:I

    :cond_0
    return-void

    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const-string v1, "DefaultSimPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "positon is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSimItemList:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/settings/SimItem;

    iget-boolean v1, v0, Lcom/mediatek/settings/SimItem;->mIsSim:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/mediatek/settings/DefaultSimPreference;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    if-eqz v1, :cond_2

    iget v1, v0, Lcom/mediatek/settings/SimItem;->mState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/mediatek/settings/DefaultSimPreference;->mCellConnMgr:Lcom/mediatek/CellConnService/CellConnMgr;

    iget-object v2, v0, Lcom/mediatek/settings/SimItem;->mSiminfo:Landroid/provider/Telephony$SIMInfo;

    iget v2, v2, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    const/16 v3, 0x12e

    invoke-virtual {v1, v2, v3}, Lcom/mediatek/CellConnService/CellConnMgr;->handleCellConn(II)I

    goto :goto_0

    :cond_2
    iput p3, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSelected:I

    invoke-virtual {p0}, Landroid/preference/DialogPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2}, Landroid/preference/DialogPreference;->onClick(Landroid/content/DialogInterface;I)V

    invoke-virtual {p0}, Landroid/preference/DialogPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 6
    .param p1    # Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x0

    const-string v0, "DefaultSimPreference"

    const-string v1, "onPrepareDialogBuilder"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSimItemList:Ljava/util/List;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mediatek/settings/DefaultSimPreference$SelectionListAdapter;

    invoke-direct {v0, p0}, Lcom/mediatek/settings/DefaultSimPreference$SelectionListAdapter;-><init>(Lcom/mediatek/settings/DefaultSimPreference;)V

    iput-object v0, p0, Lcom/mediatek/settings/DefaultSimPreference;->mAdapter:Lcom/mediatek/settings/DefaultSimPreference$SelectionListAdapter;

    new-instance v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/settings/DefaultSimPreference;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/settings/DefaultSimPreference;->mListView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/mediatek/settings/DefaultSimPreference;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/mediatek/settings/DefaultSimPreference;->mAdapter:Lcom/mediatek/settings/DefaultSimPreference$SelectionListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/mediatek/settings/DefaultSimPreference;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/mediatek/settings/DefaultSimPreference;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v1, p0, Lcom/mediatek/settings/DefaultSimPreference;->mListView:Landroid/widget/ListView;

    move-object v0, p1

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;IIII)Landroid/app/AlertDialog$Builder;

    const/high16 v0, 0x1040000

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    return-void
.end method

.method public setInitData(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/settings/SimItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "DefaultSimPreference"

    const-string v1, "setInitData()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSimItemList:Ljava/util/List;

    iget-object v0, p0, Lcom/mediatek/settings/DefaultSimPreference;->mAdapter:Lcom/mediatek/settings/DefaultSimPreference$SelectionListAdapter;

    if-eqz v0, :cond_0

    const-string v0, "DefaultSimPreference"

    const-string v1, "setInitData()+mAdapter!=null"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/settings/DefaultSimPreference;->mAdapter:Lcom/mediatek/settings/DefaultSimPreference$SelectionListAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public setInitValue(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/settings/DefaultSimPreference;->mInitValue:I

    iput p1, p0, Lcom/mediatek/settings/DefaultSimPreference;->mSelected:I

    return-void
.end method
