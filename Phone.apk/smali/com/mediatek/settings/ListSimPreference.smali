.class public Lcom/mediatek/settings/ListSimPreference;
.super Landroid/preference/ListPreference;
.source "ListSimPreference.java"


# static fields
.field private static final DBG:Z = true

.field private static final LOG_TAG:Ljava/lang/String; = "Settings/SimPreference"


# instance fields
.field private mSimColor:I

.field private mSimIconNumber:Ljava/lang/String;

.field private mSimName:Ljava/lang/String;

.field private mSimNumber:Ljava/lang/String;

.field private mSimSlot:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/mediatek/settings/ListSimPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private getSimStatusImge(I)I
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x20200f7

    goto :goto_0

    :pswitch_2
    const v0, 0x20200e4

    goto :goto_0

    :pswitch_3
    const v0, 0x20200e1

    goto :goto_0

    :pswitch_4
    const v0, 0x20200fe

    goto :goto_0

    :pswitch_5
    const v0, 0x20200fc

    goto :goto_0

    :pswitch_6
    const v0, 0x20200dc

    goto :goto_0

    :pswitch_7
    const v0, 0x20200fd

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public onBindView(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const/16 v9, 0x8

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    new-instance v5, Lcom/mediatek/telephony/TelephonyManagerEx;

    invoke-virtual {p0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v5, v7}, Lcom/mediatek/telephony/TelephonyManagerEx;-><init>(Landroid/content/Context;)V

    const v7, 0x7f0800b5

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget v7, p0, Lcom/mediatek/settings/ListSimPreference;->mSimSlot:I

    invoke-virtual {v5, v7}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimIndicatorStateGemini(I)I

    move-result v7

    invoke-direct {p0, v7}, Lcom/mediatek/settings/ListSimPreference;->getSimStatusImge(I)I

    move-result v2

    const/4 v7, -0x1

    if-ne v2, v7, :cond_5

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    :goto_0
    const v7, 0x7f0800b4

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    if-eqz v6, :cond_1

    iget v7, p0, Lcom/mediatek/settings/ListSimPreference;->mSimColor:I

    if-ltz v7, :cond_6

    iget v7, p0, Lcom/mediatek/settings/ListSimPreference;->mSimColor:I

    const/4 v8, 0x3

    if-gt v7, v8, :cond_6

    sget-object v7, Landroid/provider/Telephony;->SIMBackgroundRes:[I

    iget v8, p0, Lcom/mediatek/settings/ListSimPreference;->mSimColor:I

    aget v7, v7, v8

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_1
    :goto_1
    const v7, 0x7f0800b9

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    if-eqz v4, :cond_2

    iget-object v7, p0, Lcom/mediatek/settings/ListSimPreference;->mSimNumber:Ljava/lang/String;

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/mediatek/settings/ListSimPreference;->mSimNumber:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_7

    iget-object v7, p0, Lcom/mediatek/settings/ListSimPreference;->mSimNumber:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_2
    const v7, 0x7f0800b8

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_3

    iget-object v7, p0, Lcom/mediatek/settings/ListSimPreference;->mSimName:Ljava/lang/String;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    const v7, 0x7f0800b6

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    if-eqz v3, :cond_4

    iget-object v7, p0, Lcom/mediatek/settings/ListSimPreference;->mSimIconNumber:Ljava/lang/String;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    return-void

    :cond_5
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_6
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_7
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public setSimColor(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/settings/ListSimPreference;->mSimColor:I

    return-void
.end method

.method public setSimIconNumber(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/settings/ListSimPreference;->mSimIconNumber:Ljava/lang/String;

    return-void
.end method

.method public setSimName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/settings/ListSimPreference;->mSimName:Ljava/lang/String;

    return-void
.end method

.method public setSimNumber(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/settings/ListSimPreference;->mSimNumber:Ljava/lang/String;

    return-void
.end method

.method public setSimSlot(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/settings/ListSimPreference;->mSimSlot:I

    return-void
.end method
