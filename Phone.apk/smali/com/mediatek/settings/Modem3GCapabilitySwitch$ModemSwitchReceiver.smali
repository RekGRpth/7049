.class Lcom/mediatek/settings/Modem3GCapabilitySwitch$ModemSwitchReceiver;
.super Landroid/content/BroadcastReceiver;
.source "Modem3GCapabilitySwitch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/settings/Modem3GCapabilitySwitch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ModemSwitchReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/settings/Modem3GCapabilitySwitch;


# direct methods
.method constructor <init>(Lcom/mediatek/settings/Modem3GCapabilitySwitch;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/settings/Modem3GCapabilitySwitch$ModemSwitchReceiver;->this$0:Lcom/mediatek/settings/Modem3GCapabilitySwitch;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v6, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiPhone;->EVENT_3G_SWITCH_LOCK_CHANGED:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "Settings/Modem3GCapabilitySwitch"

    const-string v4, "receives EVENT_3G_SWITCH_LOCK_CHANGED..."

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiPhone;->EXTRA_3G_SWITCH_LOCKED:Ljava/lang/String;

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iget-object v3, p0, Lcom/mediatek/settings/Modem3GCapabilitySwitch$ModemSwitchReceiver;->this$0:Lcom/mediatek/settings/Modem3GCapabilitySwitch;

    invoke-static {v3}, Lcom/mediatek/settings/Modem3GCapabilitySwitch;->access$600(Lcom/mediatek/settings/Modem3GCapabilitySwitch;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiPhone;->EVENT_PRE_3G_SWITCH:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "Settings/Modem3GCapabilitySwitch"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Starting the switch......@"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/settings/Modem3GCapabilitySwitch$ModemSwitchReceiver;->this$0:Lcom/mediatek/settings/Modem3GCapabilitySwitch;

    invoke-virtual {v3}, Lcom/mediatek/settings/Modem3GCapabilitySwitch;->showSwitchProgress()V

    iget-object v3, p0, Lcom/mediatek/settings/Modem3GCapabilitySwitch$ModemSwitchReceiver;->this$0:Lcom/mediatek/settings/Modem3GCapabilitySwitch;

    const-string v4, "Receive starting switch broadcast"

    invoke-static {v3, v4}, Lcom/mediatek/settings/Modem3GCapabilitySwitch;->access$700(Lcom/mediatek/settings/Modem3GCapabilitySwitch;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/settings/Modem3GCapabilitySwitch$ModemSwitchReceiver;->this$0:Lcom/mediatek/settings/Modem3GCapabilitySwitch;

    invoke-static {v3, v6}, Lcom/mediatek/settings/Modem3GCapabilitySwitch;->access$800(Lcom/mediatek/settings/Modem3GCapabilitySwitch;Z)V

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/android/internal/telephony/gemini/GeminiPhone;->EVENT_3G_SWITCH_DONE:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "Settings/Modem3GCapabilitySwitch"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Done the switch......@"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/settings/Modem3GCapabilitySwitch$ModemSwitchReceiver;->this$0:Lcom/mediatek/settings/Modem3GCapabilitySwitch;

    const-string v4, "Receive switch done broadcast"

    invoke-static {v3, v4}, Lcom/mediatek/settings/Modem3GCapabilitySwitch;->access$700(Lcom/mediatek/settings/Modem3GCapabilitySwitch;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/settings/Modem3GCapabilitySwitch$ModemSwitchReceiver;->this$0:Lcom/mediatek/settings/Modem3GCapabilitySwitch;

    invoke-virtual {v3, p2}, Lcom/mediatek/settings/Modem3GCapabilitySwitch;->clearAfterSwitch(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    const-string v3, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "state"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    const-string v3, "Settings/Modem3GCapabilitySwitch"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "airplaneMode new  state is ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/settings/Modem3GCapabilitySwitch$ModemSwitchReceiver;->this$0:Lcom/mediatek/settings/Modem3GCapabilitySwitch;

    invoke-static {v3}, Lcom/mediatek/settings/Modem3GCapabilitySwitch;->access$900(Lcom/mediatek/settings/Modem3GCapabilitySwitch;)V

    iget-object v3, p0, Lcom/mediatek/settings/Modem3GCapabilitySwitch$ModemSwitchReceiver;->this$0:Lcom/mediatek/settings/Modem3GCapabilitySwitch;

    invoke-static {v3}, Lcom/mediatek/settings/Modem3GCapabilitySwitch;->access$1000(Lcom/mediatek/settings/Modem3GCapabilitySwitch;)V

    goto/16 :goto_0
.end method
