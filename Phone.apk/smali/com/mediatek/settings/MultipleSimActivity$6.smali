.class Lcom/mediatek/settings/MultipleSimActivity$6;
.super Ljava/lang/Object;
.source "MultipleSimActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/settings/MultipleSimActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/settings/MultipleSimActivity;


# direct methods
.method constructor <init>(Lcom/mediatek/settings/MultipleSimActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/settings/MultipleSimActivity$6;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/settings/MultipleSimActivity$6;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v1}, Lcom/mediatek/settings/MultipleSimActivity;->access$1200(Lcom/mediatek/settings/MultipleSimActivity;)Landroid/widget/ImageView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/mediatek/settings/MultipleSimActivity$6;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v1}, Lcom/mediatek/settings/MultipleSimActivity;->access$1300(Lcom/mediatek/settings/MultipleSimActivity;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v1, p0, Lcom/mediatek/settings/MultipleSimActivity$6;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    const/16 v2, 0x12c

    invoke-virtual {v1, v2}, Landroid/app/Activity;->removeDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/mediatek/settings/MultipleSimActivity$6;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " - Bitmap.isRecycled() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$6;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v3}, Lcom/mediatek/settings/MultipleSimActivity;->access$1300(Lcom/mediatek/settings/MultipleSimActivity;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/settings/MultipleSimActivity;->access$100(Lcom/mediatek/settings/MultipleSimActivity;Ljava/lang/String;)V

    goto :goto_0
.end method
