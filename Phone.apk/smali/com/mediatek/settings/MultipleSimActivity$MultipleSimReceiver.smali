.class Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MultipleSimActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/settings/MultipleSimActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MultipleSimReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/settings/MultipleSimActivity;


# direct methods
.method private constructor <init>(Lcom/mediatek/settings/MultipleSimActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mediatek/settings/MultipleSimActivity;Lcom/mediatek/settings/MultipleSimActivity$1;)V
    .locals 0
    .param p1    # Lcom/mediatek/settings/MultipleSimActivity;
    .param p2    # Lcom/mediatek/settings/MultipleSimActivity$1;

    invoke-direct {p0, p1}, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;-><init>(Lcom/mediatek/settings/MultipleSimActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v6, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.android.phone.NETWORK_MODE_CHANGE_RESPONSE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Landroid/app/Activity;->removeDialog(I)V

    const-string v3, "com.android.phone.NETWORK_MODE_CHANGE_RESPONSE"

    const/4 v4, 0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    const-string v4, "BroadcastReceiver: network mode change failed! restore the old value."

    invoke-static {v3, v4}, Lcom/mediatek/settings/MultipleSimActivity;->access$100(Lcom/mediatek/settings/MultipleSimActivity;Ljava/lang/String;)V

    const-string v3, "com.android.phone.OLD_NETWORK_MODE"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v3}, Lcom/mediatek/settings/MultipleSimActivity;->access$200(Lcom/mediatek/settings/MultipleSimActivity;)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "preferred_network_mode"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "BroadcastReceiver  = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/settings/MultipleSimActivity;->access$100(Lcom/mediatek/settings/MultipleSimActivity;Ljava/lang/String;)V

    const-string v3, "NETWORK_MODE"

    iget-object v4, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v4}, Lcom/mediatek/settings/MultipleSimActivity;->access$300(Lcom/mediatek/settings/MultipleSimActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    const-string v4, "setValue  to oldMode "

    invoke-static {v3, v4}, Lcom/mediatek/settings/MultipleSimActivity;->access$100(Lcom/mediatek/settings/MultipleSimActivity;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-virtual {v3}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    const-string v4, "BroadcastReceiver: network mode change success! set to the new value."

    invoke-static {v3, v4}, Lcom/mediatek/settings/MultipleSimActivity;->access$100(Lcom/mediatek/settings/MultipleSimActivity;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v3}, Lcom/mediatek/settings/MultipleSimActivity;->access$200(Lcom/mediatek/settings/MultipleSimActivity;)Lcom/android/internal/telephony/Phone;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "preferred_network_mode"

    const-string v5, "NEW_NETWORK_MODE"

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "BroadcastReceiver  = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "NEW_NETWORK_MODE"

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/settings/MultipleSimActivity;->access$100(Lcom/mediatek/settings/MultipleSimActivity;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v3, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "android.intent.action.DUAL_SIM_MODE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v3}, Lcom/mediatek/settings/MultipleSimActivity;->access$400(Lcom/mediatek/settings/MultipleSimActivity;)V

    goto :goto_0

    :cond_4
    const-string v3, "android.intent.action.ACTION_EF_CSP_CONTENT_NOTIFY"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "NETWORK_SEARCH"

    iget-object v4, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v4}, Lcom/mediatek/settings/MultipleSimActivity;->access$300(Lcom/mediatek/settings/MultipleSimActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v3}, Lcom/mediatek/settings/MultipleSimActivity;->access$700(Lcom/mediatek/settings/MultipleSimActivity;)Lcom/mediatek/phone/ext/SettingsExtension;

    move-result-object v3

    iget-object v4, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v4}, Lcom/mediatek/settings/MultipleSimActivity;->access$200(Lcom/mediatek/settings/MultipleSimActivity;)Lcom/android/internal/telephony/Phone;

    move-result-object v4

    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v5}, Lcom/mediatek/settings/MultipleSimActivity;->access$500(Lcom/mediatek/settings/MultipleSimActivity;)Ljava/util/List;

    move-result-object v5

    iget-object v6, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v6}, Lcom/mediatek/settings/MultipleSimActivity;->access$600(Lcom/mediatek/settings/MultipleSimActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/mediatek/phone/ext/SettingsExtension;->removeNMOpForMultiSim(Lcom/android/internal/telephony/Phone;Ljava/util/List;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v3}, Lcom/mediatek/settings/MultipleSimActivity;->access$800(Lcom/mediatek/settings/MultipleSimActivity;)V

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v3}, Lcom/mediatek/settings/MultipleSimActivity;->access$900(Lcom/mediatek/settings/MultipleSimActivity;)V

    goto/16 :goto_0

    :cond_5
    const-string v3, "android.intent.action.SIM_INDICATOR_STATE_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v3}, Lcom/mediatek/settings/MultipleSimActivity;->access$1000(Lcom/mediatek/settings/MultipleSimActivity;)V

    goto/16 :goto_0

    :cond_6
    const-string v3, "android.intent.action.SIM_INFO_UPDATE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mediatek/settings/MultipleSimActivity;->access$1100()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ACTION_SIM_INFO_UPDATE received"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v3}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_7

    invoke-static {}, Lcom/mediatek/settings/MultipleSimActivity;->access$1100()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Activity finished"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_7
    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;->this$0:Lcom/mediatek/settings/MultipleSimActivity;

    invoke-static {v3}, Lcom/mediatek/settings/MultipleSimActivity;->access$400(Lcom/mediatek/settings/MultipleSimActivity;)V

    goto/16 :goto_0
.end method
