.class public Lcom/mediatek/settings/MultipleSimActivity;
.super Landroid/preference/PreferenceActivity;
.source "MultipleSimActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;
    }
.end annotation


# static fields
.field private static final ALERT_DIALOG:I = 0xc8

.field private static final ALERT_DIALOG_DEFAULT:I = 0x12c

.field private static final DBG:Z = true

.field public static final LIST_TITLE:Ljava/lang/String; = "LIST_TITLE_NAME"

.field public static final NETWORK_MODE_NAME:Ljava/lang/String; = "NETWORK_MODE"

.field private static final PROGRESS_DIALOG:I = 0x64

.field private static final SELECT_DEFAULT_PICTURE:Ljava/lang/String; = "0"

.field private static final SELECT_DEFAULT_PICTURE2:Ljava/lang/String; = "0"

.field private static final SELECT_MY_PICTURE:Ljava/lang/String; = "2"

.field private static final SELECT_MY_PICTURE2:Ljava/lang/String; = "1"

.field public static SUB_TITLE_NAME:Ljava/lang/String; = null

.field private static TAG:Ljava/lang/String; = null

.field public static final VT_FEATURE_NAME:Ljava/lang/String; = "VT"

.field public static initArray:Ljava/lang/String;

.field public static initArrayValue:Ljava/lang/String;

.field public static initBaseKey:Ljava/lang/String;

.field public static initFeatureName:Ljava/lang/String;

.field public static initSimId:Ljava/lang/String;

.field public static initSimNumber:Ljava/lang/String;

.field public static initTitleName:Ljava/lang/String;

.field public static intentKey:Ljava/lang/String;

.field private static m3GSupportSlot:I

.field public static targetClassKey:Ljava/lang/String;


# instance fields
.field private mBaseKey:Ljava/lang/String;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mExtension:Lcom/mediatek/phone/ext/SettingsExtension;

.field private mFeatureName:Ljava/lang/String;

.field private mImage:Landroid/widget/ImageView;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mItemType:Ljava/lang/String;

.field private mListTitle:Ljava/lang/String;

.field private mPhone:Lcom/android/internal/telephony/Phone;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPreCheckForRunning:Lcom/mediatek/settings/PreCheckForRunning;

.field private final mReceiver:Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;

.field private mSimNumbers:I

.field private mTargetClass:Ljava/lang/String;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mTelephonyManagerEx:Lcom/mediatek/telephony/TelephonyManagerEx;

.field private mTitleName:Ljava/lang/String;

.field private mVTWhichToSave:I

.field phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

.field private pref2CardSlot:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private simIds:[J

.field private simList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/provider/Telephony$SIMInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "ITEM_TYPE"

    sput-object v0, Lcom/mediatek/settings/MultipleSimActivity;->intentKey:Ljava/lang/String;

    const-string v0, "sub_title_name"

    sput-object v0, Lcom/mediatek/settings/MultipleSimActivity;->SUB_TITLE_NAME:Ljava/lang/String;

    const-string v0, "TARGET_CLASS"

    sput-object v0, Lcom/mediatek/settings/MultipleSimActivity;->targetClassKey:Ljava/lang/String;

    const-string v0, "INIT_ARRAY"

    sput-object v0, Lcom/mediatek/settings/MultipleSimActivity;->initArray:Ljava/lang/String;

    const-string v0, "INIT_ARRAY_VALUE"

    sput-object v0, Lcom/mediatek/settings/MultipleSimActivity;->initArrayValue:Ljava/lang/String;

    const-string v0, "INIT_SIM_NUMBER"

    sput-object v0, Lcom/mediatek/settings/MultipleSimActivity;->initSimNumber:Ljava/lang/String;

    const-string v0, "INIT_FEATURE_NAME"

    sput-object v0, Lcom/mediatek/settings/MultipleSimActivity;->initFeatureName:Ljava/lang/String;

    const-string v0, "INIT_TITLE_NAME"

    sput-object v0, Lcom/mediatek/settings/MultipleSimActivity;->initTitleName:Ljava/lang/String;

    const-string v0, "INIT_SIM_ID"

    sput-object v0, Lcom/mediatek/settings/MultipleSimActivity;->initSimId:Ljava/lang/String;

    const-string v0, "INIT_BASE_KEY"

    sput-object v0, Lcom/mediatek/settings/MultipleSimActivity;->initBaseKey:Ljava/lang/String;

    const-string v0, "MultipleSimActivity"

    sput-object v0, Lcom/mediatek/settings/MultipleSimActivity;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput v0, Lcom/mediatek/settings/MultipleSimActivity;->m3GSupportSlot:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    const-string v0, "PreferenceScreen"

    iput-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mItemType:Ljava/lang/String;

    iput v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mVTWhichToSave:I

    iput v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mSimNumbers:I

    iput-object v1, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTargetClass:Ljava/lang/String;

    iput-object v1, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    iput-object v1, p0, Lcom/mediatek/settings/MultipleSimActivity;->simIds:[J

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->pref2CardSlot:Ljava/util/HashMap;

    new-instance v0, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;

    invoke-direct {v0, p0, v1}, Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;-><init>(Lcom/mediatek/settings/MultipleSimActivity;Lcom/mediatek/settings/MultipleSimActivity$1;)V

    iput-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mReceiver:Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;

    iput-object v1, p0, Lcom/mediatek/settings/MultipleSimActivity;->phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

    new-instance v0, Lcom/mediatek/settings/MultipleSimActivity$1;

    invoke-direct {v0, p0}, Lcom/mediatek/settings/MultipleSimActivity$1;-><init>(Lcom/mediatek/settings/MultipleSimActivity;)V

    iput-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/settings/MultipleSimActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/settings/MultipleSimActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/settings/MultipleSimActivity;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/settings/MultipleSimActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/settings/MultipleSimActivity;

    invoke-direct {p0}, Lcom/mediatek/settings/MultipleSimActivity;->updatePreferenceList()V

    return-void
.end method

.method static synthetic access$1100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/mediatek/settings/MultipleSimActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/mediatek/settings/MultipleSimActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/mediatek/settings/MultipleSimActivity;

    iget-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/mediatek/settings/MultipleSimActivity;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/mediatek/settings/MultipleSimActivity;

    iget-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/settings/MultipleSimActivity;)Lcom/android/internal/telephony/Phone;
    .locals 1
    .param p0    # Lcom/mediatek/settings/MultipleSimActivity;

    iget-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/settings/MultipleSimActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/settings/MultipleSimActivity;

    iget-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mFeatureName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/mediatek/settings/MultipleSimActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/settings/MultipleSimActivity;

    invoke-direct {p0}, Lcom/mediatek/settings/MultipleSimActivity;->updatePreferenceEnableState()V

    return-void
.end method

.method static synthetic access$500(Lcom/mediatek/settings/MultipleSimActivity;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/mediatek/settings/MultipleSimActivity;

    iget-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600(Lcom/mediatek/settings/MultipleSimActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/settings/MultipleSimActivity;

    iget-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTargetClass:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mediatek/settings/MultipleSimActivity;)Lcom/mediatek/phone/ext/SettingsExtension;
    .locals 1
    .param p0    # Lcom/mediatek/settings/MultipleSimActivity;

    iget-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mExtension:Lcom/mediatek/phone/ext/SettingsExtension;

    return-object v0
.end method

.method static synthetic access$800(Lcom/mediatek/settings/MultipleSimActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/settings/MultipleSimActivity;

    invoke-direct {p0}, Lcom/mediatek/settings/MultipleSimActivity;->sortSimList()V

    return-void
.end method

.method static synthetic access$900(Lcom/mediatek/settings/MultipleSimActivity;)V
    .locals 0
    .param p0    # Lcom/mediatek/settings/MultipleSimActivity;

    invoke-direct {p0}, Lcom/mediatek/settings/MultipleSimActivity;->createSubItems()V

    return-void
.end method

.method private checkToStart(ILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    invoke-direct {p0}, Lcom/mediatek/settings/MultipleSimActivity;->isNeededToCheckLock()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPreCheckForRunning:Lcom/mediatek/settings/PreCheckForRunning;

    const/16 v1, 0x12e

    invoke-virtual {v0, p2, p1, v1}, Lcom/mediatek/settings/PreCheckForRunning;->checkToRun(Landroid/content/Intent;II)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private createSubItems()V
    .locals 9

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v5}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v7

    if-ge v1, v7, :cond_1

    invoke-virtual {v5, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v7

    invoke-virtual {v7}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->mItemType:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v5, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v7

    invoke-virtual {v7}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {v5}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    :goto_2
    iget v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->mSimNumbers:I

    add-int/lit8 v7, v7, -0x1

    if-le v1, v7, :cond_3

    invoke-virtual {v5, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_3
    iget-object v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->mItemType:Ljava/lang/String;

    const-string v8, "PreferenceScreen"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-direct {p0}, Lcom/mediatek/settings/MultipleSimActivity;->initPreferenceScreen()V

    :cond_4
    :goto_3
    iget-object v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTitleName:Ljava/lang/String;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTitleName:Ljava/lang/String;

    invoke-virtual {p0, v7}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_5
    return-void

    :cond_6
    iget-object v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->mItemType:Ljava/lang/String;

    const-string v8, "CheckBoxPreference"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-direct {p0}, Lcom/mediatek/settings/MultipleSimActivity;->initCheckBoxPreference()V

    goto :goto_3

    :cond_7
    iget-object v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->mItemType:Ljava/lang/String;

    const-string v8, "ListPreference"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-direct {p0}, Lcom/mediatek/settings/MultipleSimActivity;->initListPreference()V

    goto :goto_3
.end method

.method private customizeForEVDO(Landroid/content/Intent;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    if-nez p2, :cond_0

    invoke-virtual {p1, p3, p4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, p2, p1}, Lcom/mediatek/settings/MultipleSimActivity;->checkToStart(ILandroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "com.android.phone.GsmUmtsCallForwardOptions"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "com.mediatek.settings.CdmaCallForwardOptions"

    invoke-virtual {p1, p3, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, p2, p1}, Lcom/mediatek/settings/MultipleSimActivity;->checkToStart(ILandroid/content/Intent;)V

    goto :goto_0

    :cond_1
    const-string v0, "com.android.phone.GsmUmtsAdditionalCallOptions"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "com.mediatek.settings.CdmaCallWaitingOptions"

    invoke-virtual {p1, p3, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, p2, p1}, Lcom/mediatek/settings/MultipleSimActivity;->checkToStart(ILandroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const-string v0, "com.mediatek.settings.FdnSetting2"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "com.mediatek.settings.CallBarring"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_4
    invoke-virtual {p1, p3, p4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, p2, p1}, Lcom/mediatek/settings/MultipleSimActivity;->checkToStart(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method private getProperOperatorNumber(Landroid/provider/Telephony$SIMInfo;)Ljava/lang/String;
    .locals 5
    .param p1    # Landroid/provider/Telephony$SIMInfo;

    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x4

    if-nez p1, :cond_0

    move-object v2, v1

    :goto_0
    return-object v2

    :cond_0
    iget-object v1, p1, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    iget v3, p1, Landroid/provider/Telephony$SIMInfo;->mDispalyNumberFormat:I

    packed-switch v3, :pswitch_data_0

    const-string v1, ""

    :cond_1
    :goto_1
    move-object v2, v1

    goto :goto_0

    :pswitch_0
    const-string v1, ""

    goto :goto_1

    :pswitch_1
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v4, :cond_1

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_2
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v4, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v3, v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private initCheckBoxPreference()V
    .locals 9

    const/4 v8, 0x0

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    const/4 v0, 0x0

    :goto_0
    iget v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->mSimNumbers:I

    if-ge v0, v5, :cond_3

    const/4 v1, 0x0

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/mediatek/settings/CheckSimPreference;

    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/provider/Telephony$SIMInfo;

    iget-object v5, v5, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/provider/Telephony$SIMInfo;

    iget v5, v5, Landroid/provider/Telephony$SIMInfo;->mColor:I

    invoke-virtual {v2, v5}, Lcom/mediatek/settings/CheckSimPreference;->setSimColor(I)V

    invoke-virtual {v2, v0}, Lcom/mediatek/settings/CheckSimPreference;->setSimSlot(I)V

    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/provider/Telephony$SIMInfo;

    iget-object v5, v5, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcom/mediatek/settings/CheckSimPreference;->setSimName(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/provider/Telephony$SIMInfo;

    iget-object v5, v5, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcom/mediatek/settings/CheckSimPreference;->setSimNumber(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/provider/Telephony$SIMInfo;

    invoke-direct {p0, v5}, Lcom/mediatek/settings/MultipleSimActivity;->getProperOperatorNumber(Landroid/provider/Telephony$SIMInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/mediatek/settings/CheckSimPreference;->setSimIconNumber(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/mediatek/settings/MultipleSimActivity;->pref2CardSlot:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v7

    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/provider/Telephony$SIMInfo;

    iget v5, v5, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    const-string v6, "@"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v8, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    :cond_0
    :goto_1
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v2, v5}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    const-string v5, "button_vt_auto_dropback_key"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4, v1, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v2, v5}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :cond_1
    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_2
    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/provider/Telephony$SIMInfo;

    iget v5, v5, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method private initListPreference()V
    .locals 12

    const/4 v11, 0x0

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    sget-object v9, Lcom/mediatek/settings/MultipleSimActivity;->initArray:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/content/Intent;->getCharSequenceArrayExtra(Ljava/lang/String;)[Ljava/lang/CharSequence;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    move-object v0, v8

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    sget-object v9, Lcom/mediatek/settings/MultipleSimActivity;->initArrayValue:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/content/Intent;->getCharSequenceArrayExtra(Ljava/lang/String;)[Ljava/lang/CharSequence;

    move-result-object v1

    const-string v8, "NETWORK_MODE"

    iget-object v9, p0, Lcom/mediatek/settings/MultipleSimActivity;->mFeatureName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v9, "com.android.phone.NETWORK_MODE_CHANGE_RESPONSE"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iget v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->mSimNumbers:I

    if-ge v2, v8, :cond_5

    const/4 v3, 0x0

    invoke-virtual {v5, v2}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Lcom/mediatek/settings/ListSimPreference;

    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/provider/Telephony$SIMInfo;

    iget-object v8, v8, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v8}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/provider/Telephony$SIMInfo;

    iget v8, v8, Landroid/provider/Telephony$SIMInfo;->mColor:I

    invoke-virtual {v4, v8}, Lcom/mediatek/settings/ListSimPreference;->setSimColor(I)V

    invoke-virtual {v4, v2}, Lcom/mediatek/settings/ListSimPreference;->setSimSlot(I)V

    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/provider/Telephony$SIMInfo;

    iget-object v8, v8, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v8}, Lcom/mediatek/settings/ListSimPreference;->setSimName(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/provider/Telephony$SIMInfo;

    iget-object v8, v8, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    invoke-virtual {v4, v8}, Lcom/mediatek/settings/ListSimPreference;->setSimNumber(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/provider/Telephony$SIMInfo;

    invoke-direct {p0, v8}, Lcom/mediatek/settings/MultipleSimActivity;->getProperOperatorNumber(Landroid/provider/Telephony$SIMInfo;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/mediatek/settings/ListSimPreference;->setSimIconNumber(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    const-string v9, "@"

    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    iget-object v9, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v8, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    invoke-virtual {v4, v8}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->mListTitle:Ljava/lang/String;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->mListTitle:Ljava/lang/String;

    invoke-virtual {v4, v8}, Landroid/preference/DialogPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {v4, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v1}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/mediatek/settings/MultipleSimActivity;->pref2CardSlot:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v10

    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/provider/Telephony$SIMInfo;

    iget v8, v8, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v9, v10, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v3, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v8, "NETWORK_MODE"

    iget-object v9, p0, Lcom/mediatek/settings/MultipleSimActivity;->mFeatureName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v8}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "preferred_network_mode"

    invoke-static {v8, v9, v11}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_4
    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    if-eqz v8, :cond_1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v8, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/provider/Telephony$SIMInfo;

    iget v8, v8, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    return-void
.end method

.method private initPreferenceScreen()V
    .locals 6

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    iget v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->mSimNumbers:I

    if-ge v0, v3, :cond_0

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/mediatek/settings/SimPreference;

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/Telephony$SIMInfo;

    iget-object v3, v3, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/Telephony$SIMInfo;

    iget v3, v3, Landroid/provider/Telephony$SIMInfo;->mColor:I

    invoke-virtual {v1, v3}, Lcom/mediatek/settings/SimPreference;->setSimColor(I)V

    invoke-virtual {v1, v0}, Lcom/mediatek/settings/SimPreference;->setSimSlot(I)V

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/Telephony$SIMInfo;

    iget-object v3, v3, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/mediatek/settings/SimPreference;->setSimName(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/Telephony$SIMInfo;

    iget-object v3, v3, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/mediatek/settings/SimPreference;->setSimNumber(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/Telephony$SIMInfo;

    invoke-direct {p0, v3}, Lcom/mediatek/settings/MultipleSimActivity;->getProperOperatorNumber(Landroid/provider/Telephony$SIMInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/mediatek/settings/SimPreference;->setSimIconNumber(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/settings/MultipleSimActivity;->pref2CardSlot:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v5

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/provider/Telephony$SIMInfo;

    iget v3, v3, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private isNeededToCheckLock()Z
    .locals 2

    const-string v0, "com.mediatek.settings.IpPrefixPreference"

    iget-object v1, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTargetClass:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/mediatek/settings/MultipleSimActivity;->TAG:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private showDialogPic(Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mImage:Landroid/widget/ImageView;

    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, p2}, Landroid/app/Activity;->showDialog(I)V

    return-void
.end method

.method private skipUsIfNeeded()V
    .locals 7

    const/4 v6, 0x0

    const-string v4, "VT"

    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->mFeatureName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "NETWORK_MODE"

    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->mFeatureName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v4, p0, Lcom/mediatek/settings/MultipleSimActivity;->mSimNumbers:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTargetClass:Ljava/lang/String;

    if-eqz v4, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v4, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTargetClass:Ljava/lang/String;

    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    iget-object v4, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTargetClass:Ljava/lang/String;

    invoke-virtual {v4, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v4, "com.mediatek.settings"

    const-string v5, "com.android.phone"

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "android.intent.action.MAIN"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/provider/Telephony$SIMInfo;

    iget v3, v4, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    const-string v4, "simId"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v4, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTargetClass:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, v3, v0}, Lcom/mediatek/settings/MultipleSimActivity;->checkToStart(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method private sortSimList()V
    .locals 5

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sortSimList()+simList.size()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/settings/MultipleSimActivity;->log(Ljava/lang/String;)V

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/provider/Telephony$SIMInfo;

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/provider/Telephony$SIMInfo;

    iget v3, v1, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    iget v4, v2, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    if-le v3, v4, :cond_0

    const-string v3, "swap the position of simList"

    invoke-direct {p0, v3}, Lcom/mediatek/settings/MultipleSimActivity;->log(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private updatePreferenceEnableState()V
    .locals 10

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    instance-of v6, v6, Lcom/android/internal/telephony/gemini/GeminiPhone;

    if-eqz v6, :cond_0

    iget-object v1, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    check-cast v1, Lcom/android/internal/telephony/gemini/GeminiPhone;

    :cond_0
    iget-object v6, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v6

    if-nez v6, :cond_2

    move v3, v7

    :goto_0
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v5}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v6

    if-ge v2, v6, :cond_5

    invoke-virtual {v5, v2}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v4

    if-eqz v1, :cond_1

    const-string v6, "NETWORK_MODE"

    iget-object v9, p0, Lcom/mediatek/settings/MultipleSimActivity;->mFeatureName:Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/mediatek/settings/MultipleSimActivity;->pref2CardSlot:Ljava/util/HashMap;

    invoke-virtual {v6, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->isRadioOnGemini(I)Z

    move-result v6

    if-eqz v6, :cond_3

    if-eqz v3, :cond_3

    move v6, v7

    :goto_2
    invoke-virtual {v4, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    :goto_3
    invoke-virtual {v4}, Landroid/preference/Preference;->isEnabled()Z

    move-result v6

    if-nez v6, :cond_1

    instance-of v6, v4, Landroid/preference/ListPreference;

    if-eqz v6, :cond_1

    check-cast v4, Landroid/preference/ListPreference;

    invoke-virtual {v4}, Landroid/preference/DialogPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v3, v8

    goto :goto_0

    :cond_3
    move v6, v8

    goto :goto_2

    :cond_4
    iget-object v6, p0, Lcom/mediatek/settings/MultipleSimActivity;->pref2CardSlot:Ljava/util/HashMap;

    invoke-virtual {v6, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->isRadioOnGemini(I)Z

    move-result v6

    invoke-virtual {v4, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_3

    :cond_5
    return-void
.end method

.method private updatePreferenceList()V
    .locals 2

    const-string v1, "---------[update mutiple list views]---------"

    invoke-direct {p0, v1}, Lcom/mediatek/settings/MultipleSimActivity;->log(Ljava/lang/String;)V

    const v1, 0x102000a

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidateViews()V

    return-void
.end method


# virtual methods
.method public checkAllowedRun(Landroid/content/Intent;Landroid/preference/Preference;)V
    .locals 4
    .param p1    # Landroid/content/Intent;
    .param p2    # Landroid/preference/Preference;

    const-string v2, "simId"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    instance-of v2, v2, Lcom/android/internal/telephony/gemini/GeminiPhone;

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    check-cast v0, Lcom/android/internal/telephony/gemini/GeminiPhone;

    :cond_0
    return-void
.end method

.method public getNetworkMode(I)I
    .locals 5
    .param p1    # I

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "preferred_network_mode"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eq p1, v1, :cond_0

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v2}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "preferred_network_mode"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return v0

    :pswitch_0
    const/4 v0, 0x7

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x6

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_7
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/16 v4, 0xc8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult: requestCode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", resultCode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/settings/MultipleSimActivity;->log(Ljava/lang/String;)V

    const/4 v2, -0x1

    if-eq p2, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    :try_start_0
    const-string v2, "data"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mVTWhichToSave:I

    if-nez v2, :cond_2

    invoke-static {}, Lcom/mediatek/settings/VTAdvancedSetting;->getPicPathUserselect()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/mediatek/phone/vt/VTCallUtils;->saveMyBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    :try_start_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_2
    iget v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mVTWhichToSave:I

    if-nez v2, :cond_3

    invoke-static {}, Lcom/mediatek/settings/VTAdvancedSetting;->getPicPathUserselect()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v4}, Lcom/mediatek/settings/MultipleSimActivity;->showDialogPic(Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    :try_start_2
    invoke-static {}, Lcom/mediatek/settings/VTAdvancedSetting;->getPicPathUserselect2()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/mediatek/phone/vt/VTCallUtils;->saveMyBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v1

    :try_start_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " - Bitmap.isRecycled() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/settings/MultipleSimActivity;->log(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    :cond_3
    invoke-static {}, Lcom/mediatek/settings/VTAdvancedSetting;->getPicPathUserselect2()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v4}, Lcom/mediatek/settings/MultipleSimActivity;->showDialogPic(Ljava/lang/String;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xbcd
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/mediatek/phone/ext/ExtensionManager;->getInstance()Lcom/mediatek/phone/ext/ExtensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/phone/ext/ExtensionManager;->getSettingsExtension()Lcom/mediatek/phone/ext/SettingsExtension;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mExtension:Lcom/mediatek/phone/ext/SettingsExtension;

    invoke-static {}, Lcom/android/phone/PhoneApp;->getPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    sget-object v3, Lcom/mediatek/settings/MultipleSimActivity;->initSimId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->simIds:[J

    const v2, 0x7f060014

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    sget-object v3, Lcom/mediatek/settings/MultipleSimActivity;->intentKey:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iput-object v1, p0, Lcom/mediatek/settings/MultipleSimActivity;->mItemType:Ljava/lang/String;

    :cond_0
    new-instance v2, Lcom/mediatek/settings/PreCheckForRunning;

    invoke-direct {v2, p0}, Lcom/mediatek/settings/PreCheckForRunning;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPreCheckForRunning:Lcom/mediatek/settings/PreCheckForRunning;

    new-instance v2, Lcom/mediatek/telephony/TelephonyManagerEx;

    invoke-direct {v2, p0}, Lcom/mediatek/telephony/TelephonyManagerEx;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTelephonyManagerEx:Lcom/mediatek/telephony/TelephonyManagerEx;

    const-string v2, "phone"

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    iput-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v4, 0x20

    invoke-virtual {v2, v3, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    sget-object v3, Lcom/mediatek/settings/MultipleSimActivity;->targetClassKey:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTargetClass:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    sget-object v3, Lcom/mediatek/settings/MultipleSimActivity;->initFeatureName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mFeatureName:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    sget-object v3, Lcom/mediatek/settings/MultipleSimActivity;->initTitleName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTitleName:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    sget-object v3, Lcom/mediatek/settings/MultipleSimActivity;->initBaseKey:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBaseKey:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "LIST_TITLE_NAME"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mListTitle:Ljava/lang/String;

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->simIds:[J

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->simIds:[J

    array-length v2, v2

    iput v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mSimNumbers:I

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mSimNumbers:I

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->simIds:[J

    aget-wide v3, v3, v0

    invoke-static {p0, v3, v4}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoById(Landroid/content/Context;J)Landroid/provider/Telephony$SIMInfo;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-static {p0}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iput v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mSimNumbers:I

    :cond_2
    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mExtension:Lcom/mediatek/phone/ext/SettingsExtension;

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v4, p0, Lcom/mediatek/settings/MultipleSimActivity;->simList:Ljava/util/List;

    iget-object v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTargetClass:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Lcom/mediatek/phone/ext/SettingsExtension;->removeNMOpForMultiSim(Lcom/android/internal/telephony/Phone;Ljava/util/List;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/settings/MultipleSimActivity;->sortSimList()V

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mIntentFilter:Landroid/content/IntentFilter;

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.SIM_INDICATOR_STATE_CHANGED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.ACTION_EF_CSP_CONTENT_NOTIFY"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.DUAL_SIM_MODE"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.SIM_INFO_UPDATE"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v2

    iget-object v2, v2, Lcom/android/phone/PhoneApp;->phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

    iput-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

    const-string v2, "3G_SWITCH"

    invoke-static {v2}, Lcom/android/phone/PhoneUtils;->isSupportFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

    invoke-virtual {v2}, Lcom/android/phone/PhoneInterfaceManager;->get3GCapabilitySIM()I

    move-result v2

    sput v2, Lcom/mediatek/settings/MultipleSimActivity;->m3GSupportSlot:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "3G support slot ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/mediatek/settings/MultipleSimActivity;->m3GSupportSlot:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/settings/MultipleSimActivity;->log(Ljava/lang/String;)V

    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/mediatek/settings/MultipleSimActivity;->skipUsIfNeeded()V

    invoke-direct {p0}, Lcom/mediatek/settings/MultipleSimActivity;->createSubItems()V

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mReceiver:Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v2, v3}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :cond_4
    const-string v2, "Fail to get phone app instance"

    invoke-direct {p0, v2}, Lcom/mediatek/settings/MultipleSimActivity;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .param p1    # I

    const v5, 0x104000a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "--------------------[onCreateDialog]["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]-----------------"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/settings/MultipleSimActivity;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/settings/MultipleSimActivity;->mImage:Landroid/widget/ImageView;

    if-nez v2, :cond_1

    :cond_0
    move-object v1, v0

    :goto_0
    return-object v1

    :cond_1
    sparse-switch p1, :sswitch_data_0

    :goto_1
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    move-object v1, v0

    goto :goto_0

    :sswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    move-object v2, v0

    check-cast v2, Landroid/app/ProgressDialog;

    const v3, 0x7f0d01e7

    invoke-virtual {p0, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    move-object v2, v0

    check-cast v2, Landroid/app/ProgressDialog;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    move-object v2, v0

    check-cast v2, Landroid/app/ProgressDialog;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    goto :goto_1

    :sswitch_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0d0036

    new-instance v4, Lcom/mediatek/settings/MultipleSimActivity$3;

    invoke-direct {v4, p0}, Lcom/mediatek/settings/MultipleSimActivity$3;-><init>(Lcom/mediatek/settings/MultipleSimActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/mediatek/settings/MultipleSimActivity$2;

    invoke-direct {v3, p0}, Lcom/mediatek/settings/MultipleSimActivity$2;-><init>(Lcom/mediatek/settings/MultipleSimActivity;)V

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/app/AlertDialog;

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->mImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d002d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/mediatek/settings/MultipleSimActivity$4;

    invoke-direct {v2, p0}, Lcom/mediatek/settings/MultipleSimActivity$4;-><init>(Lcom/mediatek/settings/MultipleSimActivity;)V

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_1

    :sswitch_2
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/mediatek/settings/MultipleSimActivity$5;

    invoke-direct {v3, p0}, Lcom/mediatek/settings/MultipleSimActivity$5;-><init>(Lcom/mediatek/settings/MultipleSimActivity;)V

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/app/AlertDialog;

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->mImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d002b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/mediatek/settings/MultipleSimActivity$6;

    invoke-direct {v2, p0}, Lcom/mediatek/settings/MultipleSimActivity$6;-><init>(Lcom/mediatek/settings/MultipleSimActivity;)V

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPreCheckForRunning:Lcom/mediatek/settings/PreCheckForRunning;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPreCheckForRunning:Lcom/mediatek/settings/PreCheckForRunning;

    invoke-virtual {v0}, Lcom/mediatek/settings/PreCheckForRunning;->deRegister()V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mReceiver:Lcom/mediatek/settings/MultipleSimActivity$MultipleSimReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 9
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/16 v8, 0x12c

    const/16 v7, 0xc8

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v3, "VT"

    iget-object v4, p0, Lcom/mediatek/settings/MultipleSimActivity;->mFeatureName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/mediatek/phone/vt/VTCallUtils;->checkVTFile()V

    const-string v3, "button_vt_replace_expand_key"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iput v5, p0, Lcom/mediatek/settings/MultipleSimActivity;->mVTWhichToSave:I

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/mediatek/settings/VTAdvancedSetting;->getPicPathDefault()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v8}, Lcom/mediatek/settings/MultipleSimActivity;->showDialogPic(Ljava/lang/String;I)V

    :cond_0
    :goto_0
    return v6

    :cond_1
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mediatek/settings/VTAdvancedSetting;->getPicPathUserselect()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v7}, Lcom/mediatek/settings/MultipleSimActivity;->showDialogPic(Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    const-string v3, "button_vt_replace_peer_expand_key"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iput v6, p0, Lcom/mediatek/settings/MultipleSimActivity;->mVTWhichToSave:I

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {}, Lcom/mediatek/settings/VTAdvancedSetting;->getPicPathDefault2()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v8}, Lcom/mediatek/settings/MultipleSimActivity;->showDialogPic(Ljava/lang/String;I)V

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mediatek/settings/VTAdvancedSetting;->getPicPathUserselect2()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v7}, Lcom/mediatek/settings/MultipleSimActivity;->showDialogPic(Ljava/lang/String;I)V

    goto :goto_0

    :cond_4
    const-string v3, "NETWORK_MODE"

    iget-object v4, p0, Lcom/mediatek/settings/MultipleSimActivity;->mFeatureName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "preferred_network_mode"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Current network mode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/settings/MultipleSimActivity;->log(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/mediatek/settings/MultipleSimActivity;->getNetworkMode(I)I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "new network mode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/mediatek/settings/MultipleSimActivity;->log(Ljava/lang/String;)V

    if-eq v2, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.android.phone.NETWORK_MODE_CHANGE"

    const/4 v4, 0x0

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v3, "com.android.phone.OLD_NETWORK_MODE"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "com.android.phone.NETWORK_MODE_CHANGE"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "simId"

    iget-object v3, p0, Lcom/mediatek/settings/MultipleSimActivity;->pref2CardSlot:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/Serializable;

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/16 v3, 0x64

    invoke-virtual {p0, v3}, Landroid/app/Activity;->showDialog(I)V

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 10
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Landroid/preference/Preference;

    const/4 v9, 0x0

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    const/4 v0, 0x0

    iget-object v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    instance-of v7, v7, Lcom/android/internal/telephony/gemini/GeminiPhone;

    if-eqz v7, :cond_0

    iget-object v0, p0, Lcom/mediatek/settings/MultipleSimActivity;->mPhone:Lcom/android/internal/telephony/Phone;

    check-cast v0, Lcom/android/internal/telephony/gemini/GeminiPhone;

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v5}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v7

    if-ge v1, v7, :cond_3

    invoke-virtual {v5, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v7

    if-ne p2, v7, :cond_2

    iget-object v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTargetClass:Ljava/lang/String;

    if-eqz v7, :cond_2

    if-eqz v0, :cond_2

    iget-object v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->pref2CardSlot:Ljava/util/HashMap;

    invoke-virtual {v7, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->isRadioOnGemini(I)Z

    move-result v7

    if-eqz v7, :cond_2

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTargetClass:Ljava/lang/String;

    const/16 v8, 0x2e

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    iget-object v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTargetClass:Ljava/lang/String;

    invoke-virtual {v7, v9, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const-string v7, "com.mediatek.settings"

    const-string v8, "com.android.phone"

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTargetClass:Ljava/lang/String;

    invoke-virtual {v2, v3, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v7, "android.intent.action.MAIN"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v7, "simId"

    invoke-virtual {v2, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    sget-object v7, Lcom/mediatek/settings/MultipleSimActivity;->SUB_TITLE_NAME:Ljava/lang/String;

    invoke-static {p0, v6}, Landroid/provider/Telephony$SIMInfo;->getSIMInfoBySlot(Landroid/content/Context;I)Landroid/provider/Telephony$SIMInfo;

    move-result-object v8

    iget-object v8, v8, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->mFeatureName:Ljava/lang/String;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->mFeatureName:Ljava/lang/String;

    const-string v8, "VT"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "ISVT"

    const/4 v8, 0x1

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_1
    iget-object v7, p0, Lcom/mediatek/settings/MultipleSimActivity;->mTargetClass:Ljava/lang/String;

    invoke-virtual {v2, v3, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, v6, v2}, Lcom/mediatek/settings/MultipleSimActivity;->checkToStart(ILandroid/content/Intent;)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    return v9
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/mediatek/settings/MultipleSimActivity;->updatePreferenceList()V

    invoke-direct {p0}, Lcom/mediatek/settings/MultipleSimActivity;->updatePreferenceEnableState()V

    return-void
.end method
