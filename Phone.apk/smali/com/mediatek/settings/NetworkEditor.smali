.class public Lcom/mediatek/settings/NetworkEditor;
.super Landroid/preference/PreferenceActivity;
.source "NetworkEditor.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final BUTTON_NETWORK_ID_KEY:Ljava/lang/String; = "network_id_key"

.field private static final BUTTON_NEWWORK_MODE_KEY:Ljava/lang/String; = "network_mode_key"

.field private static final BUTTON_PRIORITY_KEY:Ljava/lang/String; = "priority_key"

.field private static final DUAL_MODE:I = 0x2

.field private static final GSM:I = 0x0

.field private static final MENU_DELETE:I = 0x1

.field private static final MENU_DISCARD:I = 0x3

.field private static final MENU_SAVE:I = 0x2

.field public static final MODEM_MASK_TDSCDMA:I = 0x8

.field public static final PLMN_ADD:Ljava/lang/String; = "plmn_add"

.field public static final PLMN_CODE:Ljava/lang/String; = "plmn_code"

.field public static final PLMN_NAME:Ljava/lang/String; = "plmn_name"

.field public static final PLMN_PRIORITY:Ljava/lang/String; = "plmn_priority"

.field public static final PLMN_SERVICE:Ljava/lang/String; = "plmn_service"

.field public static final PLMN_SIZE:Ljava/lang/String; = "plmn_size"

.field public static PROPERTY_KEY:Ljava/lang/String; = null

.field public static final RESULT_DELETE:I = 0xc8

.field public static final RESULT_MODIFY:I = 0x64

.field private static final RIL_2G:I = 0x1

.field private static final RIL_2G_3G:I = 0x5

.field private static final RIL_3G:I = 0x4

.field private static final TAG:Ljava/lang/String; = "NetworkEditor"

.field private static final WCDMA_TDSCDMA:I = 0x1


# instance fields
.field private mAirplaneModeEnabled:Z

.field private mDualSimMode:I

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mNetworkId:Landroid/preference/EditTextPreference;

.field private mNetworkMode:Landroid/preference/ListPreference;

.field private mNotSet:Ljava/lang/String;

.field private mPLMNName:Ljava/lang/String;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPriority:Landroid/preference/EditTextPreference;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "gsm.baseband.capability"

    sput-object v0, Lcom/mediatek/settings/NetworkEditor;->PROPERTY_KEY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    iput-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkId:Landroid/preference/EditTextPreference;

    iput-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mPriority:Landroid/preference/EditTextPreference;

    iput-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkMode:Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mNotSet:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/settings/NetworkEditor;->mAirplaneModeEnabled:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/mediatek/settings/NetworkEditor;->mDualSimMode:I

    new-instance v0, Lcom/mediatek/settings/NetworkEditor$1;

    invoke-direct {v0, p0}, Lcom/mediatek/settings/NetworkEditor$1;-><init>(Lcom/mediatek/settings/NetworkEditor;)V

    iput-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/mediatek/settings/NetworkEditor$2;

    invoke-direct {v0, p0}, Lcom/mediatek/settings/NetworkEditor$2;-><init>(Lcom/mediatek/settings/NetworkEditor;)V

    iput-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/settings/NetworkEditor;)V
    .locals 0
    .param p0    # Lcom/mediatek/settings/NetworkEditor;

    invoke-direct {p0}, Lcom/mediatek/settings/NetworkEditor;->setScreenEnabled()V

    return-void
.end method

.method static synthetic access$102(Lcom/mediatek/settings/NetworkEditor;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/settings/NetworkEditor;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/settings/NetworkEditor;->mAirplaneModeEnabled:Z

    return p1
.end method

.method static synthetic access$202(Lcom/mediatek/settings/NetworkEditor;I)I
    .locals 0
    .param p0    # Lcom/mediatek/settings/NetworkEditor;
    .param p1    # I

    iput p1, p0, Lcom/mediatek/settings/NetworkEditor;->mDualSimMode:I

    return p1
.end method

.method private checkNotSet(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mNotSet:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string p1, ""

    :cond_1
    return-object p1
.end method

.method private checkNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object p1, p0, Lcom/mediatek/settings/NetworkEditor;->mNotSet:Ljava/lang/String;

    :cond_1
    return-object p1
.end method

.method public static covertApNW2Ril(I)I
    .locals 2
    .param p0    # I

    const/4 v0, 0x0

    const/4 v1, 0x2

    if-ne p0, v1, :cond_0

    const/4 v0, 0x5

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x1

    if-ne p0, v1, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static covertRilNW2Ap(I)I
    .locals 2
    .param p0    # I

    const/4 v0, 0x0

    const/4 v1, 0x5

    if-lt p0, v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    and-int/lit8 v1, p0, 0x4

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createNetworkInfo(Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Intent;

    const v7, 0x7f07001e

    const/4 v6, 0x0

    const-string v4, "plmn_name"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/mediatek/settings/NetworkEditor;->mPLMNName:Ljava/lang/String;

    const-string v4, "plmn_code"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkId:Landroid/preference/EditTextPreference;

    invoke-direct {p0, v1}, Lcom/mediatek/settings/NetworkEditor;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkId:Landroid/preference/EditTextPreference;

    invoke-virtual {v4, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    const-string v4, "plmn_priority"

    invoke-virtual {p1, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iget-object v4, p0, Lcom/mediatek/settings/NetworkEditor;->mPriority:Landroid/preference/EditTextPreference;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/mediatek/settings/NetworkEditor;->mPriority:Landroid/preference/EditTextPreference;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    const-string v4, "plmn_service"

    invoke-virtual {p1, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/mediatek/settings/NetworkEditor;->covertRilNW2Ap(I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v4, 0x2

    if-le v0, v4, :cond_1

    :cond_0
    const/4 v0, 0x0

    :cond_1
    const-string v3, ""

    invoke-static {}, Lcom/mediatek/settings/NetworkEditor;->getBaseBand()I

    move-result v4

    and-int/lit8 v4, v4, 0x8

    if-nez v4, :cond_2

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07001d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    aget-object v3, v4, v0

    :goto_0
    iget-object v4, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkMode:Landroid/preference/ListPreference;

    invoke-virtual {v4, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkMode:Landroid/preference/ListPreference;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    return-void

    :cond_2
    iget-object v4, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkMode:Landroid/preference/ListPreference;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    aget-object v3, v4, v0

    goto :goto_0
.end method

.method private genNetworkInfo(Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Intent;

    const/4 v5, 0x0

    const-string v3, "plmn_name"

    iget-object v4, p0, Lcom/mediatek/settings/NetworkEditor;->mPLMNName:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/mediatek/settings/NetworkEditor;->checkNotSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "plmn_code"

    iget-object v4, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkId:Landroid/preference/EditTextPreference;

    invoke-virtual {v4}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "plmn_size"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/settings/NetworkEditor;->mPriority:Landroid/preference/EditTextPreference;

    invoke-virtual {v3}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "plmn_add"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    if-le v1, v2, :cond_0

    move v1, v2

    :cond_0
    :goto_1
    const-string v3, "plmn_priority"

    invoke-virtual {p1, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :try_start_1
    const-string v3, "plmn_service"

    iget-object v4, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkMode:Landroid/preference/ListPreference;

    invoke-virtual {v4}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Lcom/mediatek/settings/NetworkEditor;->covertApNW2Ril(I)I

    move-result v4

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    return-void

    :cond_1
    if-lt v1, v2, :cond_0

    add-int/lit8 v1, v2, -0x1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v3, "plmn_service"

    invoke-static {v5}, Lcom/mediatek/settings/NetworkEditor;->covertApNW2Ril(I)I

    move-result v4

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2

    :catch_1
    move-exception v3

    goto :goto_0
.end method

.method static getBaseBand()I
    .locals 6

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-static {}, Lcom/android/phone/PhoneApp;->getInstance()Lcom/android/phone/PhoneApp;

    move-result-object v4

    iget-object v2, v4, Lcom/android/phone/PhoneApp;->phoneMgr:Lcom/android/phone/PhoneInterfaceManager;

    const/4 v4, 0x1

    invoke-virtual {v2}, Lcom/android/phone/PhoneInterfaceManager;->get3GCapabilitySIM()I

    move-result v5

    if-ne v4, v5, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/mediatek/settings/NetworkEditor;->PROPERTY_KEY:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "2"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/mediatek/settings/NetworkEditor;->PROPERTY_KEY:Ljava/lang/String;

    :cond_0
    sget-object v4, Lcom/mediatek/settings/NetworkEditor;->PROPERTY_KEY:Ljava/lang/String;

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v4, ""

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    :goto_0
    return v3

    :cond_2
    const/16 v4, 0x10

    :try_start_0
    invoke-static {v0, v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v4, "NetworkEditor"

    const-string v5, "parse value of basband error"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setRemovedNetwork()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/settings/PLMNListPreference;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0xc8

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-direct {p0, v0}, Lcom/mediatek/settings/NetworkEditor;->genNetworkInfo(Landroid/content/Intent;)V

    return-void
.end method

.method private setScreenEnabled()V
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/mediatek/settings/NetworkEditor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v4, p0, Lcom/mediatek/settings/NetworkEditor;->mAirplaneModeEnabled:Z

    if-nez v4, :cond_1

    iget v4, p0, Lcom/mediatek/settings/NetworkEditor;->mDualSimMode:I

    if-eqz v4, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceGroup;->setEnabled(Z)V

    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1
.end method

.method private validateAndSetResult()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/mediatek/settings/PLMNListPreference;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x64

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-direct {p0, v0}, Lcom/mediatek/settings/NetworkEditor;->genNetworkInfo(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f060017

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0317

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mNotSet:Ljava/lang/String;

    const-string v0, "network_id_key"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/EditTextPreference;

    iput-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkId:Landroid/preference/EditTextPreference;

    const-string v0, "priority_key"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/EditTextPreference;

    iput-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mPriority:Landroid/preference/EditTextPreference;

    const-string v0, "network_mode_key"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkMode:Landroid/preference/ListPreference;

    iget-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkId:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mPriority:Landroid/preference/EditTextPreference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkMode:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/mediatek/settings/NetworkEditor;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-static {}, Lcom/mediatek/settings/CallSettings;->isMultipleSim()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.DUAL_SIM_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/mediatek/settings/NetworkEditor;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "plmn_add"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x10403c2

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    :cond_0
    const/4 v0, 0x2

    const v1, 0x7f0d0285

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    const/4 v0, 0x3

    const/high16 v1, 0x1040000

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    return v3
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/settings/NetworkEditor;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/Menu;

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuOpened(ILandroid/view/Menu;)Z

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/mediatek/settings/NetworkEditor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v5

    if-nez v5, :cond_3

    move v1, v3

    :goto_0
    if-eqz v1, :cond_4

    iget-boolean v5, p0, Lcom/mediatek/settings/NetworkEditor;->mAirplaneModeEnabled:Z

    if-nez v5, :cond_4

    iget v5, p0, Lcom/mediatek/settings/NetworkEditor;->mDualSimMode:I

    if-eqz v5, :cond_4

    move v2, v3

    :goto_1
    iget-object v5, p0, Lcom/mediatek/settings/NetworkEditor;->mNotSet:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkId:Landroid/preference/EditTextPreference;

    invoke-virtual {v6}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/mediatek/settings/NetworkEditor;->mNotSet:Ljava/lang/String;

    iget-object v6, p0, Lcom/mediatek/settings/NetworkEditor;->mPriority:Landroid/preference/EditTextPreference;

    invoke-virtual {v6}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_0
    move v0, v3

    :goto_2
    if-eqz p2, :cond_2

    invoke-interface {p2, v4, v2}, Landroid/view/Menu;->setGroupEnabled(IZ)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "plmn_add"

    invoke-virtual {v5, v6, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {p2, v4}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    move v4, v3

    :cond_1
    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_2
    :goto_3
    return v3

    :cond_3
    move v1, v4

    goto :goto_0

    :cond_4
    move v2, v4

    goto :goto_1

    :cond_5
    move v0, v4

    goto :goto_2

    :cond_6
    invoke-interface {p2, v3}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-eqz v2, :cond_7

    if-nez v0, :cond_7

    move v4, v3

    :cond_7
    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_3
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    invoke-direct {p0}, Lcom/mediatek/settings/NetworkEditor;->setRemovedNetwork()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/mediatek/settings/NetworkEditor;->validateAndSetResult()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkId:Landroid/preference/EditTextPreference;

    if-ne p1, v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkId:Landroid/preference/EditTextPreference;

    invoke-direct {p0, v2}, Lcom/mediatek/settings/NetworkEditor;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    const/4 v3, 0x1

    return v3

    :cond_1
    iget-object v3, p0, Lcom/mediatek/settings/NetworkEditor;->mPriority:Landroid/preference/EditTextPreference;

    if-ne p1, v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/settings/NetworkEditor;->mPriority:Landroid/preference/EditTextPreference;

    invoke-direct {p0, v2}, Lcom/mediatek/settings/NetworkEditor;->checkNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkMode:Landroid/preference/ListPreference;

    if-ne p1, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkMode:Landroid/preference/ListPreference;

    invoke-virtual {v3, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    const-string v1, ""

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {}, Lcom/mediatek/settings/NetworkEditor;->getBaseBand()I

    move-result v3

    and-int/lit8 v3, v3, 0x8

    if-nez v3, :cond_3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07001d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    aget-object v1, v3, v0

    :goto_1
    iget-object v3, p0, Lcom/mediatek/settings/NetworkEditor;->mNetworkMode:Landroid/preference/ListPreference;

    invoke-virtual {v3, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07001e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    aget-object v1, v3, v0

    goto :goto_1
.end method

.method protected onResume()V
    .locals 4

    const/4 v0, 0x1

    const/4 v3, -0x1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/settings/NetworkEditor;->createNetworkInfo(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_on"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v0, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/mediatek/settings/NetworkEditor;->mAirplaneModeEnabled:Z

    invoke-static {}, Lcom/mediatek/settings/CallSettings;->isMultipleSim()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "dual_sim_mode_setting"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/mediatek/settings/NetworkEditor;->mDualSimMode:I

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/settings/NetworkEditor;->setScreenEnabled()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
