.class Lcom/mediatek/settings/PLMNListPreference$SIMCapability;
.super Ljava/lang/Object;
.source "PLMNListPreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/settings/PLMNListPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SIMCapability"
.end annotation


# instance fields
.field firstFormat:I

.field firstIndex:I

.field lastFormat:I

.field lastIndex:I

.field final synthetic this$0:Lcom/mediatek/settings/PLMNListPreference;


# direct methods
.method public constructor <init>(Lcom/mediatek/settings/PLMNListPreference;IIII)V
    .locals 0
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/mediatek/settings/PLMNListPreference$SIMCapability;->this$0:Lcom/mediatek/settings/PLMNListPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/mediatek/settings/PLMNListPreference$SIMCapability;->firstIndex:I

    iput p3, p0, Lcom/mediatek/settings/PLMNListPreference$SIMCapability;->lastIndex:I

    iput p4, p0, Lcom/mediatek/settings/PLMNListPreference$SIMCapability;->firstFormat:I

    iput p5, p0, Lcom/mediatek/settings/PLMNListPreference$SIMCapability;->lastFormat:I

    return-void
.end method


# virtual methods
.method public setCapability([I)V
    .locals 2
    .param p1    # [I

    array-length v0, p1

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Lcom/mediatek/settings/PLMNListPreference$SIMCapability;->firstIndex:I

    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Lcom/mediatek/settings/PLMNListPreference$SIMCapability;->lastIndex:I

    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Lcom/mediatek/settings/PLMNListPreference$SIMCapability;->firstFormat:I

    const/4 v0, 0x3

    aget v0, p1, v0

    iput v0, p0, Lcom/mediatek/settings/PLMNListPreference$SIMCapability;->lastFormat:I

    goto :goto_0
.end method
