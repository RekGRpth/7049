.class Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ServiceSelectList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/settings/ServiceSelectList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SelectionListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field mSimItemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/settings/ServiceSelectList$SimItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/mediatek/settings/ServiceSelectList;


# direct methods
.method public constructor <init>(Lcom/mediatek/settings/ServiceSelectList;Landroid/content/Context;)V
    .locals 10
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->this$0:Lcom/mediatek/settings/ServiceSelectList;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->mSimItemList:Ljava/util/List;

    invoke-static {p2}, Landroid/provider/Telephony$SIMInfo;->getInsertedSIMList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/provider/Telephony$SIMInfo;

    iget-object v0, p0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->mSimItemList:Ljava/util/List;

    new-instance v1, Lcom/mediatek/settings/ServiceSelectList$SimItem;

    invoke-direct {v1, p1, v7}, Lcom/mediatek/settings/ServiceSelectList$SimItem;-><init>(Lcom/mediatek/settings/ServiceSelectList;Landroid/provider/Telephony$SIMInfo;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v9, p0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->mSimItemList:Ljava/util/List;

    new-instance v0, Lcom/mediatek/settings/ServiceSelectList$SimItem;

    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/mediatek/settings/ServiceSelectList$SimItem;-><init>(Lcom/mediatek/settings/ServiceSelectList;Ljava/lang/String;IJ)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Lcom/mediatek/settings/ServiceSelectList;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/settings/ServiceSelectList$SimItem;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->this$0:Lcom/mediatek/settings/ServiceSelectList;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->mSimItemList:Ljava/util/List;

    return-void
.end method

.method private setImageSim(Landroid/widget/RelativeLayout;Lcom/mediatek/settings/ServiceSelectList$SimItem;)V
    .locals 5
    .param p1    # Landroid/widget/RelativeLayout;
    .param p2    # Lcom/mediatek/settings/ServiceSelectList$SimItem;

    const/16 v4, 0x8

    const/4 v3, 0x0

    iget-boolean v1, p2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mIsSim:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget v1, p2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mColor:I

    invoke-static {v1}, Lcom/android/phone/Utils;->getSimColorResource(I)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mColor:I

    if-ne v1, v4, :cond_2

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x20200d3

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setImageStatus(Landroid/widget/ImageView;Lcom/mediatek/settings/ServiceSelectList$SimItem;)V
    .locals 4
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Lcom/mediatek/settings/ServiceSelectList$SimItem;

    iget-boolean v2, p2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mIsSim:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->this$0:Lcom/mediatek/settings/ServiceSelectList;

    invoke-static {v2}, Lcom/mediatek/settings/ServiceSelectList;->access$200(Lcom/mediatek/settings/ServiceSelectList;)Lcom/mediatek/telephony/TelephonyManagerEx;

    move-result-object v2

    iget v3, p2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mSlot:I

    invoke-virtual {v2, v3}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimIndicatorStateGemini(I)I

    move-result v1

    invoke-static {v1}, Lcom/android/phone/Utils;->getStatusResource(I)I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    const/16 v2, 0x8

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private setNameAndNum(Landroid/widget/TextView;Landroid/widget/TextView;Lcom/mediatek/settings/ServiceSelectList$SimItem;)V
    .locals 4
    .param p1    # Landroid/widget/TextView;
    .param p2    # Landroid/widget/TextView;
    .param p3    # Lcom/mediatek/settings/ServiceSelectList$SimItem;

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget-object v0, p3, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mName:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p3, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-boolean v0, p3, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mIsSim:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p3, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mNumber:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p3, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mNumber:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p3, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mNumber:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private setTextNumFormat(Landroid/widget/TextView;Lcom/mediatek/settings/ServiceSelectList$SimItem;)V
    .locals 4
    .param p1    # Landroid/widget/TextView;
    .param p2    # Lcom/mediatek/settings/ServiceSelectList$SimItem;

    const/4 v3, 0x4

    const/4 v2, 0x0

    iget-boolean v0, p2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mIsSim:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mNumber:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget v0, p2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mDispalyNumberFormat:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mNumber:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v3, :cond_1

    iget-object v0, p2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mNumber:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mNumber:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v3, :cond_2

    iget-object v0, p2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mNumber:Ljava/lang/String;

    iget-object v1, p2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mNumber:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v0, p2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setViewHolderId(Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;Landroid/view/View;)V
    .locals 1
    .param p1    # Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;
    .param p2    # Landroid/view/View;

    const v0, 0x7f0800c5

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;->textName:Landroid/widget/TextView;

    const v0, 0x7f0800c6

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;->textNum:Landroid/widget/TextView;

    const v0, 0x7f0800c2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;->imageStatus:Landroid/widget/ImageView;

    const v0, 0x7f0800c3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;->textNumFormat:Landroid/widget/TextView;

    const v0, 0x7f0800c7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p1, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;->ckRadioOn:Landroid/widget/RadioButton;

    const v0, 0x7f0800c1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p1, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;->imageSim:Landroid/widget/RelativeLayout;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->mSimItemList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getHas3GService()I
    .locals 4

    const/4 v1, -0x1

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->mSimItemList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->mSimItemList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mediatek/settings/ServiceSelectList$SimItem;

    iget-boolean v3, v2, Lcom/mediatek/settings/ServiceSelectList$SimItem;->has3GCapability:Z

    if-eqz v3, :cond_1

    move v1, v0

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->mSimItemList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_0

    iget-object v2, p0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->this$0:Lcom/mediatek/settings/ServiceSelectList;

    invoke-static {v2}, Lcom/mediatek/settings/ServiceSelectList;->access$000(Lcom/mediatek/settings/ServiceSelectList;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f04002c

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;

    invoke-direct {v0, p0}, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;-><init>(Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;)V

    invoke-direct {p0, v0, p2}, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->setViewHolderId(Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {p0, p1}, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mediatek/settings/ServiceSelectList$SimItem;

    iget-object v2, v0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;->textName:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;->textNum:Landroid/widget/TextView;

    invoke-direct {p0, v2, v3, v1}, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->setNameAndNum(Landroid/widget/TextView;Landroid/widget/TextView;Lcom/mediatek/settings/ServiceSelectList$SimItem;)V

    iget-object v2, v0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;->imageSim:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v2, v1}, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->setImageSim(Landroid/widget/RelativeLayout;Lcom/mediatek/settings/ServiceSelectList$SimItem;)V

    iget-object v2, v0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;->imageStatus:Landroid/widget/ImageView;

    invoke-direct {p0, v2, v1}, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->setImageStatus(Landroid/widget/ImageView;Lcom/mediatek/settings/ServiceSelectList$SimItem;)V

    iget-object v2, v0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;->textNumFormat:Landroid/widget/TextView;

    invoke-direct {p0, v2, v1}, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->setTextNumFormat(Landroid/widget/TextView;Lcom/mediatek/settings/ServiceSelectList$SimItem;)V

    iget-object v3, v0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;->ckRadioOn:Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter;->this$0:Lcom/mediatek/settings/ServiceSelectList;

    invoke-static {v2}, Lcom/mediatek/settings/ServiceSelectList;->access$100(Lcom/mediatek/settings/ServiceSelectList;)I

    move-result v2

    if-ne v2, p1, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v3, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/settings/ServiceSelectList$SelectionListAdapter$ViewHolder;

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method
