.class public Lcom/mediatek/vt/VTSettings;
.super Ljava/lang/Object;
.source "VTSettings.java"


# static fields
.field static final CAMERA_ZOOM_SCALE_LEVELS:I = 0x10

.field private static final DEBUG:Z = true

.field static final KEY_CAMERA:Ljava/lang/String; = "camera"

.field static final KEY_IS_MUTE:Ljava/lang/String; = "microphone_is_mute"

.field static final KEY_LOCAL_IMAGE_PATH:Ljava/lang/String; = "KEY_LOCAL_IMAGE_PATH"

.field static final KEY_LOCAL_IMAGE_TYPE:Ljava/lang/String; = "KEY_LOCAL_IMAGE_TYPE"

.field static final KEY_LOCAL_VIDEO_TYPE:Ljava/lang/String; = "KEY_LOCAL_VIDEO_TYPE"

.field static final KEY_SPEAKER_IS_ON:Ljava/lang/String; = "SPEAKER_IS_ON"

.field static final KEY_VIDEO_QUALITY:Ljava/lang/String; = "video_quality"

.field public static final OFF:I = 0x0

.field public static final ON:I = 0x1

.field private static final TAG:Ljava/lang/String; = "VTSettings"

.field static final VTSETTING_FILE:Ljava/lang/String; = "vt_settings"


# instance fields
.field localSurface:Landroid/view/SurfaceHolder;

.field mCameraParamters:Lcom/mediatek/vt/CameraParamters;

.field mCameraZoomIncVal:I

.field mContext:Landroid/content/Context;

.field mImagePath:Ljava/lang/String;

.field mIsSwitch:Z

.field mVideoQuality:I

.field mVideoType:I

.field peerSurface:Landroid/view/SurfaceHolder;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public canDecBrightness()Z
    .locals 4

    const-string v2, "VTSettings"

    const-string v3, "canDecBrightness"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v2}, Lcom/mediatek/vt/CameraParamters;->getExposureCompensation()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v2}, Lcom/mediatek/vt/CameraParamters;->getMinExposureCompensation()I

    move-result v0

    if-le v1, v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public canDecContrast()Z
    .locals 4

    const/4 v1, 0x0

    const-string v2, "VTSettings"

    const-string v3, "canDecContrast"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v2}, Lcom/mediatek/vt/CameraParamters;->getSupportedContrastMode()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const-string v2, "low"

    invoke-virtual {p0}, Lcom/mediatek/vt/VTSettings;->getContrastMode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public canDecZoom()Z
    .locals 3

    const/4 v0, 0x0

    const-string v1, "VTSettings"

    const-string v2, "canDecZoom"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v1}, Lcom/mediatek/vt/CameraParamters;->isZoomSupported()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/vt/VTSettings;->getZoom()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public canIncBrightness()Z
    .locals 4

    const-string v2, "VTSettings"

    const-string v3, "getBrightnessMode"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v2}, Lcom/mediatek/vt/CameraParamters;->getExposureCompensation()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v2}, Lcom/mediatek/vt/CameraParamters;->getMaxExposureCompensation()I

    move-result v0

    if-ge v1, v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public canIncContrast()Z
    .locals 4

    const/4 v1, 0x0

    const-string v2, "VTSettings"

    const-string v3, "canIncContrast"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v2}, Lcom/mediatek/vt/CameraParamters;->getSupportedContrastMode()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const-string v2, "high"

    invoke-virtual {p0}, Lcom/mediatek/vt/VTSettings;->getContrastMode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public canIncZoom()Z
    .locals 3

    const/4 v0, 0x0

    const-string v1, "VTSettings"

    const-string v2, "canIncZoom"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v1}, Lcom/mediatek/vt/CameraParamters;->isZoomSupported()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/vt/VTSettings;->getZoom()I

    move-result v1

    iget-object v2, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v2}, Lcom/mediatek/vt/CameraParamters;->getMaxZoom()I

    move-result v2

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public decBrightness()Z
    .locals 5

    const-string v3, "VTSettings"

    const-string v4, "decBrightness"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v3}, Lcom/mediatek/vt/CameraParamters;->getExposureCompensation()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v3}, Lcom/mediatek/vt/CameraParamters;->getMinExposureCompensation()I

    move-result v0

    iget-object v3, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v3}, Lcom/mediatek/vt/CameraParamters;->getExposureCompensationStep()F

    move-result v1

    int-to-float v3, v2

    sub-float/2addr v3, v1

    float-to-int v2, v3

    if-ge v2, v0, :cond_0

    move v2, v0

    :cond_0
    iget-object v3, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v3, v2}, Lcom/mediatek/vt/CameraParamters;->setExposureCompensation(I)V

    const/4 v3, 0x1

    return v3
.end method

.method public decContrast()Z
    .locals 3

    const-string v1, "VTSettings"

    const-string v2, "decContrast"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/vt/VTSettings;->getContrastMode()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    const-string v2, "low"

    invoke-virtual {v1, v2}, Lcom/mediatek/vt/CameraParamters;->setContrastMode(Ljava/lang/String;)V

    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    const-string v1, "high"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    const-string v2, "middle"

    invoke-virtual {v1, v2}, Lcom/mediatek/vt/CameraParamters;->setContrastMode(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v1, "middle"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    const-string v2, "low"

    invoke-virtual {v1, v2}, Lcom/mediatek/vt/CameraParamters;->setContrastMode(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public decZoom()Z
    .locals 3

    const-string v1, "VTSettings"

    const-string v2, "decZoom"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/vt/VTSettings;->getZoom()I

    move-result v1

    iget v2, p0, Lcom/mediatek/vt/VTSettings;->mCameraZoomIncVal:I

    sub-int v0, v1, v2

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v1, v0}, Lcom/mediatek/vt/CameraParamters;->setZoom(I)V

    const/4 v1, 0x1

    return v1
.end method

.method deinit()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    return-void
.end method

.method public getBrightnessMode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0}, Lcom/mediatek/vt/CameraParamters;->getExposure()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method getCameraSettings()V
    .locals 1

    invoke-static {}, Lcom/mediatek/vt/VTelProvider;->getParameters()Lcom/mediatek/vt/CameraParamters;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0}, Lcom/mediatek/vt/CameraParamters;->isZoomSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraZoomIncVal:I

    :cond_0
    return-void
.end method

.method public getColorEffect()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0}, Lcom/mediatek/vt/CameraParamters;->getColorEffect()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getContrastMode()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v1}, Lcom/mediatek/vt/CameraParamters;->getContrastMode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "VTSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getContrastMode ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method getDefaultSettings()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    return-void
.end method

.method public getImagePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mImagePath:Ljava/lang/String;

    return-object v0
.end method

.method public getIsSwitch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/vt/VTSettings;->mIsSwitch:Z

    return v0
.end method

.method public getLocalSurface()Landroid/view/SurfaceHolder;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->localSurface:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method public getNightMode()Z
    .locals 2

    invoke-virtual {p0}, Lcom/mediatek/vt/VTSettings;->getSceneMode()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/mediatek/vt/VTSettings;->getSceneMode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "night"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getPeerSurface()Landroid/view/SurfaceHolder;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->peerSurface:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method public getSceneMode()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v1}, Lcom/mediatek/vt/CameraParamters;->getSceneMode()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, "VTSettings"

    const-string v2, "mCameraParamters.getSceneMode() is null"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v0, "VTSettings"

    iget-object v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v1}, Lcom/mediatek/vt/CameraParamters;->getSceneMode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0}, Lcom/mediatek/vt/CameraParamters;->getSceneMode()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSupportedBrightnessMode()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0}, Lcom/mediatek/vt/CameraParamters;->getSupportedExposure()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getSupportedColorEffects()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0}, Lcom/mediatek/vt/CameraParamters;->getSupportedColorEffects()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getSupportedContrastMode()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0}, Lcom/mediatek/vt/CameraParamters;->getSupportedContrastMode()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getSupportedSceneModes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0}, Lcom/mediatek/vt/CameraParamters;->getSupportedSceneModes()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getVideoQuality()I
    .locals 1

    iget v0, p0, Lcom/mediatek/vt/VTSettings;->mVideoQuality:I

    return v0
.end method

.method public getVideoType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/vt/VTSettings;->mVideoType:I

    return v0
.end method

.method public getZoom()I
    .locals 2

    const-string v0, "VTSettings"

    const-string v1, "getZoom"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0}, Lcom/mediatek/vt/CameraParamters;->getZoom()I

    move-result v0

    goto :goto_0
.end method

.method public getZoomRatios()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v0, "VTSettings"

    const-string v1, "getZoomRatios"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0}, Lcom/mediatek/vt/CameraParamters;->getZoomRatios()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public incBrightness()Z
    .locals 5

    const-string v3, "VTSettings"

    const-string v4, "incBrightness"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v3}, Lcom/mediatek/vt/CameraParamters;->getExposureCompensation()I

    move-result v2

    iget-object v3, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v3}, Lcom/mediatek/vt/CameraParamters;->getMaxExposureCompensation()I

    move-result v0

    iget-object v3, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v3}, Lcom/mediatek/vt/CameraParamters;->getExposureCompensationStep()F

    move-result v1

    int-to-float v3, v2

    add-float/2addr v3, v1

    float-to-int v2, v3

    if-le v2, v0, :cond_0

    move v2, v0

    :cond_0
    iget-object v3, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v3, v2}, Lcom/mediatek/vt/CameraParamters;->setExposureCompensation(I)V

    const/4 v3, 0x1

    return v3
.end method

.method public incContrast()Z
    .locals 3

    const-string v1, "VTSettings"

    const-string v2, "incContrast"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/vt/VTSettings;->getContrastMode()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    const-string v2, "high"

    invoke-virtual {v1, v2}, Lcom/mediatek/vt/CameraParamters;->setContrastMode(Ljava/lang/String;)V

    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    const-string v1, "low"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    const-string v2, "middle"

    invoke-virtual {v1, v2}, Lcom/mediatek/vt/CameraParamters;->setContrastMode(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v1, "middle"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    const-string v2, "high"

    invoke-virtual {v1, v2}, Lcom/mediatek/vt/CameraParamters;->setContrastMode(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public incZoom()Z
    .locals 4

    const-string v2, "VTSettings"

    const-string v3, "incZoom"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/vt/VTSettings;->getZoom()I

    move-result v2

    iget v3, p0, Lcom/mediatek/vt/VTSettings;->mCameraZoomIncVal:I

    add-int v1, v2, v3

    iget-object v2, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v2}, Lcom/mediatek/vt/CameraParamters;->getMaxZoom()I

    move-result v0

    if-le v1, v0, :cond_0

    move v1, v0

    :cond_0
    iget-object v2, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v2, v1}, Lcom/mediatek/vt/CameraParamters;->setZoom(I)V

    const/4 v2, 0x1

    return v2
.end method

.method init(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-eqz v0, :cond_0

    const-string v0, "VTSettings"

    const-string v1, "init error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/mediatek/vt/VTSettings;->mContext:Landroid/content/Context;

    iput v1, p0, Lcom/mediatek/vt/VTSettings;->mVideoType:I

    iput-object v2, p0, Lcom/mediatek/vt/VTSettings;->mImagePath:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/mediatek/vt/VTSettings;->mIsSwitch:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/mediatek/vt/VTSettings;->mVideoQuality:I

    iput v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraZoomIncVal:I

    iput-object v2, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    goto :goto_0
.end method

.method public isSupportNightMode()Z
    .locals 5

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/mediatek/vt/VTSettings;->getSupportedSceneModes()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v4, "night"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    goto :goto_0
.end method

.method public isZoomSupported()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0}, Lcom/mediatek/vt/CameraParamters;->isZoomSupported()Z

    move-result v0

    goto :goto_0
.end method

.method public setBrightnessMode(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0, p1}, Lcom/mediatek/vt/CameraParamters;->setExposure(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setColorEffect(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0, p1}, Lcom/mediatek/vt/CameraParamters;->setColorEffect(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setContrastMode(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "VTSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setContrastMode ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0, p1}, Lcom/mediatek/vt/CameraParamters;->setContrastMode(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setImagePath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/vt/VTSettings;->mImagePath:Ljava/lang/String;

    return-void
.end method

.method public setIsSwitch(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/vt/VTSettings;->mIsSwitch:Z

    return-void
.end method

.method public setLocalSurface(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    iput-object p1, p0, Lcom/mediatek/vt/VTSettings;->localSurface:Landroid/view/SurfaceHolder;

    return-void
.end method

.method public setNightMode(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    const-string v0, "night"

    :goto_0
    invoke-virtual {p0, p1}, Lcom/mediatek/vt/VTSettings;->setNightModeFrameRate(Z)V

    invoke-virtual {p0, v0}, Lcom/mediatek/vt/VTSettings;->setSceneMode(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "auto"

    goto :goto_0
.end method

.method public setNightModeFrameRate(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-eqz p1, :cond_0

    const/16 v0, 0xf

    :goto_0
    invoke-virtual {v1, v0}, Lcom/mediatek/vt/CameraParamters;->setPreviewFrameRate(I)V

    return-void

    :cond_0
    const/16 v0, 0x1e

    goto :goto_0
.end method

.method public setPeerSurface(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    iput-object p1, p0, Lcom/mediatek/vt/VTSettings;->peerSurface:Landroid/view/SurfaceHolder;

    return-void
.end method

.method public setSceneMode(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "VTSettings"

    const-string v1, "setSceneMode"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "VTSettings"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0, p1}, Lcom/mediatek/vt/CameraParamters;->setSceneMode(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setVideoQuality(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vt/VTSettings;->mVideoQuality:I

    return-void
.end method

.method public setVideoType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/vt/VTSettings;->mVideoType:I

    return-void
.end method

.method public setZoom(I)V
    .locals 2
    .param p1    # I

    const-string v0, "VTSettings"

    const-string v1, "setZoom"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-gez p1, :cond_1

    const/4 p1, 0x0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/vt/VTSettings;->mCameraParamters:Lcom/mediatek/vt/CameraParamters;

    invoke-virtual {v0, p1}, Lcom/mediatek/vt/CameraParamters;->setZoom(I)V

    goto :goto_0
.end method
