.class public final Lcom/android/vending/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AccountSelectorView:[I

.field public static final AudienceView:[I

.field public static final CircleButton:[I

.field public static final CircleListItemView:[I

.field public static final ColumnGridView_Layout:[I

.field public static final ContactListItemView:[I

.field public static final ContentFrame:[I

.field public static final DecoratedTextView:[I

.field public static final DocImageView:[I

.field public static final ExactLayout_Layout:[I

.field public static final FifeImageView:[I

.field public static final HeroGraphicView:[I

.field public static final MarginBox:[I

.field public static final MaxWidthView:[I

.field public static final MultiWaveView:[I

.field public static final ParticipantsGalleryFragment:[I

.field public static final PlayActionButton:[I

.field public static final PlayCardClusterViewHeader:[I

.field public static final PlayCardThumbnail:[I

.field public static final PlayCardView:[I

.field public static final PlaySeparatorLayout:[I

.field public static final PlayTextView:[I

.field public static final ProportionalWidthFrame:[I

.field public static final ScaledLayout:[I

.field public static final TabButton:[I

.field public static final Theme:[I

.field public static final Thermometer:[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [I

    const v1, 0x7f01000c

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->AccountSelectorView:[I

    new-array v0, v3, [I

    const v1, 0x1010153

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->AudienceView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/vending/R$styleable;->CircleButton:[I

    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/vending/R$styleable;->CircleListItemView:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/vending/R$styleable;->ColumnGridView_Layout:[I

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/vending/R$styleable;->ContactListItemView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/vending/R$styleable;->ContentFrame:[I

    new-array v0, v3, [I

    const v1, 0x7f010009

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->DecoratedTextView:[I

    new-array v0, v3, [I

    const v1, 0x7f010006

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->DocImageView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/android/vending/R$styleable;->ExactLayout_Layout:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/android/vending/R$styleable;->FifeImageView:[I

    new-array v0, v3, [I

    const/high16 v1, 0x7f010000

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->HeroGraphicView:[I

    new-array v0, v3, [I

    const v1, 0x7f01000b

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->MarginBox:[I

    new-array v0, v3, [I

    const v1, 0x7f01000a

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->MaxWidthView:[I

    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/android/vending/R$styleable;->MultiWaveView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/android/vending/R$styleable;->ParticipantsGalleryFragment:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/android/vending/R$styleable;->PlayActionButton:[I

    new-array v0, v3, [I

    const v1, 0x7f01001b

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->PlayCardClusterViewHeader:[I

    new-array v0, v3, [I

    const v1, 0x7f010018

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->PlayCardThumbnail:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/android/vending/R$styleable;->PlayCardView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/android/vending/R$styleable;->PlaySeparatorLayout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/android/vending/R$styleable;->PlayTextView:[I

    new-array v0, v3, [I

    const v1, 0x7f01000d

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->ProportionalWidthFrame:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/android/vending/R$styleable;->ScaledLayout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/android/vending/R$styleable;->TabButton:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_f

    sput-object v0, Lcom/android/vending/R$styleable;->Theme:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_10

    sput-object v0, Lcom/android/vending/R$styleable;->Thermometer:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f010067
        0x7f010068
    .end array-data

    :array_1
    .array-data 4
        0x7f010046
        0x7f010047
        0x7f010048
        0x7f010049
        0x7f01004a
        0x7f01004b
        0x7f01004c
        0x7f01004d
        0x7f01004e
        0x7f01004f
        0x7f010050
        0x7f010051
        0x7f010052
        0x7f010053
        0x7f010054
    .end array-data

    :array_2
    .array-data 4
        0x7f010064
        0x7f010065
        0x7f010066
    .end array-data

    :array_3
    .array-data 4
        0x7f010034
        0x7f010035
        0x7f010036
        0x7f010037
        0x7f010038
        0x7f010039
        0x7f01003a
        0x7f01003b
        0x7f01003c
        0x7f01003d
        0x7f01003e
        0x7f01003f
        0x7f010040
        0x7f010041
        0x7f010042
        0x7f010043
        0x7f010044
        0x7f010045
    .end array-data

    :array_4
    .array-data 4
        0x7f010007
        0x7f010008
    .end array-data

    :array_5
    .array-data 4
        0x7f010069
        0x7f01006a
        0x7f01006b
        0x7f01006c
    .end array-data

    :array_6
    .array-data 4
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
    .end array-data

    :array_7
    .array-data 4
        0x7f010055
        0x7f010056
        0x7f010057
        0x7f010058
        0x7f010059
        0x7f01005a
        0x7f01005b
        0x7f01005c
        0x7f01005d
        0x7f01005e
        0x7f01005f
        0x7f010060
        0x7f010061
        0x7f010062
        0x7f010063
    .end array-data

    :array_8
    .array-data 4
        0x7f010026
        0x7f010027
    .end array-data

    :array_9
    .array-data 4
        0x7f010019
        0x7f01001a
    .end array-data

    :array_a
    .array-data 4
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
    .end array-data

    :array_b
    .array-data 4
        0x7f010010
        0x7f010011
        0x7f010012
        0x7f010013
    .end array-data

    :array_c
    .array-data 4
        0x7f01000e
        0x7f01000f
    .end array-data

    :array_d
    .array-data 4
        0x7f010028
        0x7f010029
        0x7f01002a
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
        0x7f01002f
        0x7f010030
    .end array-data

    :array_e
    .array-data 4
        0x1010002
        0x101014f
    .end array-data

    :array_f
    .array-data 4
        0x7f01001e
        0x7f01001f
        0x7f010020
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
        0x7f010025
    .end array-data

    :array_10
    .array-data 4
        0x7f010031
        0x7f010032
        0x7f010033
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
