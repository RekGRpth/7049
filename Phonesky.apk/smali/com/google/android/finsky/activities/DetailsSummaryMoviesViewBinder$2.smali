.class Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$2;
.super Ljava/lang/Object;
.source "DetailsSummaryMoviesViewBinder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->setupListOfferButton(Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/finsky/utils/DocUtils$OfferFilter;Lcom/google/android/finsky/layout/play/PlayActionButton;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;

.field final synthetic val$logEventType:I

.field final synthetic val$offerFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;

    iput p2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$2;->val$logEventType:I

    iput-object p3, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$2;->val$offerFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    iget v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$2;->val$logEventType:I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;

    iget-object v3, v3, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;

    iget-object v1, v1, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;

    iget-object v2, v2, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$2;->val$offerFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;

    iget-object v5, v5, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mExternalReferrer:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;

    iget-object v6, v6, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mContinueUrl:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->buy(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
