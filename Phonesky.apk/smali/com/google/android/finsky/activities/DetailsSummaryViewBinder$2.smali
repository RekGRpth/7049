.class Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;
.super Ljava/lang/Object;
.source "DetailsSummaryViewBinder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->configureCancelButton(Lcom/google/android/finsky/layout/play/PlayActionButton;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

.field final synthetic val$doc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$owner:Landroid/accounts/Account;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iput-object p2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iput-object p3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->val$owner:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    # getter for: Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mAnalytics:Lcom/google/android/finsky/analytics/Analytics;
    invoke-static {v3}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->access$000(Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;)Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "preorder.cancelStarted?doc="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v6, v6, v4}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iget-object v3, v3, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v4, 0xeb

    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iget-object v5, v5, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v3, v4, v6, v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iget-object v3, v3, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070178

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0700fa

    const v4, 0x7f0700fb

    invoke-static {v2, v3, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(Ljava/lang/String;II)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v3, "DetailsSummaryViewBinder.doc"

    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v3, "DetailsSummaryViewBinder.ownerAccountName"

    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->val$owner:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iget-object v3, v3, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

    const/4 v4, 0x7

    invoke-virtual {v0, v3, v4, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iget-object v3, v3, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

    invoke-virtual {v3}, Lcom/google/android/finsky/fragments/PageFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "DetailsSummaryViewBinder.confirm_cancel_dialog"

    invoke-virtual {v0, v3, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
