.class public final Lcom/google/android/finsky/activities/ListTab;
.super Ljava/lang/Object;
.source "ListTab.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/google/android/finsky/activities/ViewPagerTab;
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;


# instance fields
.field private mBinder:Lcom/google/android/finsky/fragments/BucketedListBinder;

.field private final mCurrentBrowseUrl:Ljava/lang/String;

.field private mIsCurrentlySelected:Z

.field private mLastKnownBucket:Lcom/google/android/finsky/api/model/Bucket;

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private final mList:Lcom/google/android/finsky/api/model/DfeList;

.field private mListBoundAlready:Z

.field private mListTabContainer:Landroid/view/ViewGroup;

.field private mListTabWrapper:Landroid/view/ViewGroup;

.field protected final mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

.field private final mQuickLinks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mReferrerBrowseUrl:Ljava/lang/String;

.field private mRestoreBundle:Landroid/os/Bundle;

.field private final mTitle:Ljava/lang/String;

.field protected final mToc:Lcom/google/android/finsky/api/model/DfeToc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/api/DfeApi;Landroid/view/LayoutInflater;Lcom/google/android/finsky/activities/TabbedAdapter$TabType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/DfeToc;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p4    # Lcom/google/android/finsky/api/DfeApi;
    .param p5    # Landroid/view/LayoutInflater;
    .param p6    # Lcom/google/android/finsky/activities/TabbedAdapter$TabType;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p10    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/ListTab;->mListBoundAlready:Z

    iput-object p5, p0, Lcom/google/android/finsky/activities/ListTab;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget-object v0, p6, Lcom/google/android/finsky/activities/TabbedAdapter$TabType;->tabListData:Lcom/google/android/finsky/api/model/DfeList;

    iput-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    iget-object v0, p6, Lcom/google/android/finsky/activities/TabbedAdapter$TabType;->mQuickLinks:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mQuickLinks:Ljava/util/List;

    iget-object v0, p6, Lcom/google/android/finsky/activities/TabbedAdapter$TabType;->mTitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mTitle:Ljava/lang/String;

    iget-object v0, p6, Lcom/google/android/finsky/activities/TabbedAdapter$TabType;->mElementNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    iput-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    iput-object p7, p0, Lcom/google/android/finsky/activities/ListTab;->mReferrerBrowseUrl:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/finsky/activities/ListTab;->mCurrentBrowseUrl:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-wide/high16 v3, 0x3ff0000000000000L

    invoke-static {v2, v3, v4}, Lcom/google/android/finsky/utils/PlayUtils;->getFeaturedGridColumnCount(Landroid/content/res/Resources;D)I

    move-result v2

    mul-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/DfeList;->setWindowDistance(I)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->startLoadItems()V

    iput-object p9, p0, Lcom/google/android/finsky/activities/ListTab;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    iput-object p2, p0, Lcom/google/android/finsky/activities/ListTab;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/finsky/activities/ListTab;->createBinder(Landroid/content/Context;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/api/DfeApi;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/ListTab;)Lcom/google/android/finsky/api/model/DfeList;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/ListTab;

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/ListTab;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/ListTab;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ListTab;->syncViewToState()V

    return-void
.end method

.method private bindList(Landroid/view/View;Landroid/widget/ListView;)V
    .locals 9
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/widget/ListView;

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/ListTab;->mListBoundAlready:Z

    if-nez v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mBinder:Lcom/google/android/finsky/fragments/BucketedListBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/fragments/BucketedListBinder;->setData(Lcom/google/android/finsky/api/model/BucketedList;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mBinder:Lcom/google/android/finsky/fragments/BucketedListBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/google/android/finsky/activities/ListTab;->mLastKnownBucket:Lcom/google/android/finsky/api/model/Bucket;

    iget-object v4, p0, Lcom/google/android/finsky/activities/ListTab;->mQuickLinks:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/finsky/activities/ListTab;->mTitle:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mCurrentBrowseUrl:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/finsky/activities/ListTab;->mRestoreBundle:Landroid/os/Bundle;

    iget-object v8, p0, Lcom/google/android/finsky/activities/ListTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/finsky/fragments/BucketedListBinder;->bind(Landroid/view/ViewGroup;ILcom/google/android/finsky/api/model/Bucket;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mRestoreBundle:Landroid/os/Bundle;

    iput-boolean v2, p0, Lcom/google/android/finsky/activities/ListTab;->mListBoundAlready:Z

    :cond_0
    return-void
.end method

.method private createBinder(Landroid/content/Context;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/api/DfeApi;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p3    # Lcom/google/android/finsky/api/DfeApi;

    new-instance v0, Lcom/google/android/finsky/fragments/BucketedListBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    invoke-direct {v0, v1, p3}, Lcom/google/android/finsky/fragments/BucketedListBinder;-><init>(Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/DfeApi;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mBinder:Lcom/google/android/finsky/fragments/BucketedListBinder;

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mBinder:Lcom/google/android/finsky/fragments/BucketedListBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0, p1, v1, p2}, Lcom/google/android/finsky/fragments/BucketedListBinder;->init(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/utils/BitmapLoader;)V

    return-void
.end method

.method private logViewImpressionForList()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeList;->isReady()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeList;->getBucketCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeList;->getBucketList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Bucket;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Bucket;->getAnalyticsCookie()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/ListTab;->mReferrerBrowseUrl:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/activities/ListTab;->mCurrentBrowseUrl:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/google/android/finsky/analytics/Analytics;->logListViewOnPage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/ListTab;->mCurrentBrowseUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/DfeList;->getInitialUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private syncViewToState()V
    .locals 10

    const/4 v2, 0x1

    const/16 v9, 0x8

    const/4 v7, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    const v8, 0x7f08016f

    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    const v8, 0x7f0801ab

    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/DfeList;->inErrorState()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-boolean v6, p0, Lcom/google/android/finsky/activities/ListTab;->mListBoundAlready:Z

    if-nez v6, :cond_0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    const v6, 0x7f08004f

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/DfeList;->getVolleyError()Lcom/android/volley/VolleyError;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v6, 0x7f080122

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    new-instance v7, Lcom/google/android/finsky/activities/ListTab$1;

    invoke-direct {v7, p0}, Lcom/google/android/finsky/activities/ListTab$1;-><init>(Lcom/google/android/finsky/activities/ListTab;)V

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabContainer:Landroid/view/ViewGroup;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabContainer:Landroid/view/ViewGroup;

    invoke-virtual {v6, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/DfeList;->isReady()Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/DfeList;->getBucketCount()I

    move-result v6

    if-lez v6, :cond_3

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mLastKnownBucket:Lcom/google/android/finsky/api/model/Bucket;

    if-nez v6, :cond_3

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/DfeList;->getBucketList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/api/model/Bucket;

    iput-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mLastKnownBucket:Lcom/google/android/finsky/api/model/Bucket;

    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabContainer:Landroid/view/ViewGroup;

    if-nez v6, :cond_4

    const/4 v5, -0x1

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/DfeList;->getBucketCount()I

    move-result v6

    if-ne v6, v2, :cond_5

    :goto_1
    if-eqz v2, :cond_6

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/DfeList;->getBucketList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/api/model/Bucket;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Bucket;->hasRecommendationsTemplate()Z

    move-result v6

    if-eqz v6, :cond_6

    const v5, 0x7f040025

    :goto_2
    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget-object v8, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    invoke-virtual {v6, v5, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    iput-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabContainer:Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    iget-object v8, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabContainer:Landroid/view/ViewGroup;

    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_4
    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    const v8, 0x7f080072

    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v7}, Landroid/widget/ListView;->setVisibility(I)V

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v4, v3}, Lcom/google/android/finsky/activities/ListTab;->bindList(Landroid/view/View;Landroid/widget/ListView;)V

    goto/16 :goto_0

    :cond_5
    move v2, v7

    goto :goto_1

    :cond_6
    if-eqz v2, :cond_7

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/DfeList;->getBucketList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/api/model/Bucket;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Bucket;->hasEditorialSeriesContainer()Z

    move-result v6

    if-eqz v6, :cond_7

    const v5, 0x7f040090

    goto :goto_2

    :cond_7
    const v5, 0x7f040022

    goto :goto_2

    :cond_8
    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabContainer:Landroid/view/ViewGroup;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabContainer:Landroid/view/ViewGroup;

    invoke-virtual {v6, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public getView(I)Landroid/view/View;
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040092

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ListTab;->syncViewToState()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public onDataChanged()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/ListTab;->mIsCurrentlySelected:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ListTab;->logViewImpressionForList()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ListTab;->syncViewToState()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mBinder:Lcom/google/android/finsky/fragments/BucketedListBinder;

    invoke-virtual {v0}, Lcom/google/android/finsky/fragments/BucketedListBinder;->onDestroyView()V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->flushUnusedPages()V

    iput-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabContainer:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/ListTab;->mListBoundAlready:Z

    return-void
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 0
    .param p1    # Lcom/android/volley/VolleyError;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/ListTab;->syncViewToState()V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    iput-object p1, p0, Lcom/google/android/finsky/activities/ListTab;->mRestoreBundle:Landroid/os/Bundle;

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Bundle;
    .locals 4

    iget-object v2, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabContainer:Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabContainer:Landroid/view/ViewGroup;

    const v3, 0x7f080072

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/google/android/finsky/activities/ListTab;->mBinder:Lcom/google/android/finsky/fragments/BucketedListBinder;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/finsky/fragments/BucketedListBinder;->onSaveInstanceState(Landroid/widget/ListView;Landroid/os/Bundle;)V

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setTabSelected(Z)V
    .locals 2
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/ListTab;->mIsCurrentlySelected:Z

    if-eq p1, v0, :cond_1

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->startNewImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;->setNodeSelected(Z)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;->getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mListTabWrapper:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->requestImpressions(Landroid/view/ViewGroup;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ListTab;->logViewImpressionForList()V

    :goto_0
    iput-boolean p1, p0, Lcom/google/android/finsky/activities/ListTab;->mIsCurrentlySelected:Z

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/activities/ListTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;->setNodeSelected(Z)V

    goto :goto_0
.end method
