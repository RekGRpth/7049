.class Lcom/google/android/finsky/activities/ReviewDialog$8;
.super Ljava/lang/Object;
.source "ReviewDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/ReviewDialog;->confirmPlus(Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/ReviewDialog;

.field final synthetic val$analytics:Lcom/google/android/finsky/analytics/Analytics;

.field final synthetic val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field final synthetic val$profile:Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/ReviewDialog;Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;Lcom/google/android/finsky/analytics/Analytics;Lcom/google/android/finsky/analytics/FinskyEventLog;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/activities/ReviewDialog$8;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    iput-object p2, p0, Lcom/google/android/finsky/activities/ReviewDialog$8;->val$profile:Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;

    iput-object p3, p0, Lcom/google/android/finsky/activities/ReviewDialog$8;->val$analytics:Lcom/google/android/finsky/analytics/Analytics;

    iput-object p4, p0, Lcom/google/android/finsky/activities/ReviewDialog$8;->val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog$8;->val$profile:Lcom/google/android/finsky/remoting/protos/PlusData$PlusProfile;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog$8;->val$analytics:Lcom/google/android/finsky/analytics/Analytics;

    const-string v1, "reviews.plusShowSignup"

    invoke-interface {v0, v2, v2, v1}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog$8;->val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-string v1, "reviews.plusShowSignup"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logTag(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog$8;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # invokes: Lcom/google/android/finsky/activities/ReviewDialog;->launchGooglePlusSignup()V
    invoke-static {v0}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1700(Lcom/google/android/finsky/activities/ReviewDialog;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog$8;->val$analytics:Lcom/google/android/finsky/analytics/Analytics;

    const-string v1, "reviews.plusAccepted"

    invoke-interface {v0, v2, v2, v1}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog$8;->val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-string v1, "reviews.plusAccepted"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logTag(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog$8;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # getter for: Lcom/google/android/finsky/activities/ReviewDialog;->mAcceptedPlus:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    invoke-static {v0}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1300(Lcom/google/android/finsky/activities/ReviewDialog;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog$8;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # setter for: Lcom/google/android/finsky/activities/ReviewDialog;->mRequiresPlusCheck:Z
    invoke-static {v0, v3}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1502(Lcom/google/android/finsky/activities/ReviewDialog;Z)Z

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog$8;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # invokes: Lcom/google/android/finsky/activities/ReviewDialog;->syncOkButtonState()V
    invoke-static {v0}, Lcom/google/android/finsky/activities/ReviewDialog;->access$000(Lcom/google/android/finsky/activities/ReviewDialog;)V

    goto :goto_0
.end method
