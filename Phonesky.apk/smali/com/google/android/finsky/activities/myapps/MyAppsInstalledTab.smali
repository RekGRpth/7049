.class public Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;
.super Lcom/google/android/finsky/activities/myapps/MyAppsTab;
.source "MyAppsInstalledTab.java"

# interfaces
.implements Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/activities/myapps/MyAppsTab",
        "<",
        "Lcom/google/android/finsky/api/model/DfeBulkDetails;",
        ">;",
        "Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

.field private mInstalledView:Landroid/view/ViewGroup;

.field private mListInitialized:Z

.field private mMyAppsList:Landroid/widget/ListView;

.field private mOwnedDocIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/activities/AuthenticatedActivity;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/activities/myapps/OpenDetailsHandler;Landroid/os/Bundle;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 10
    .param p1    # Lcom/google/android/finsky/activities/AuthenticatedActivity;
    .param p2    # Lcom/google/android/finsky/api/DfeApi;
    .param p3    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p4    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5    # Lcom/google/android/finsky/activities/myapps/OpenDetailsHandler;
    .param p6    # Landroid/os/Bundle;
    .param p7    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p8    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-direct/range {p0 .. p6}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;-><init>(Lcom/google/android/finsky/activities/AuthenticatedActivity;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/activities/myapps/OpenDetailsHandler;Landroid/os/Bundle;)V

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mOwnedDocIds:Ljava/util/List;

    new-instance v9, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    const/16 v1, 0x195

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p8

    invoke-direct {v9, v1, v2, v3, v0}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    new-instance v1, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getInstallPolicies()Lcom/google/android/finsky/installer/InstallPolicies;

    move-result-object v4

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v5

    move-object v2, p1

    move-object/from16 v6, p7

    move-object v7, p0

    move-object v8, p0

    invoke-direct/range {v1 .. v9}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/installer/InstallPolicies;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/utils/BitmapLoader;Landroid/view/View$OnClickListener;Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mOwnedDocIds:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mOwnedDocIds:Ljava/util/List;

    return-object p1
.end method


# virtual methods
.method public bucketsChanged()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstalledView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstalledView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->restoreTabSelection()V

    :cond_0
    return-void
.end method

.method protected getAdapter()Lcom/google/android/finsky/activities/myapps/MyAppsListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    return-object v0
.end method

.method protected getDocumentForView(Landroid/view/View;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->getViewDoc(Landroid/view/View;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    return-object v0
.end method

.method protected getFullView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstalledView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method protected getListView()Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    return-object v0
.end method

.method public getView(I)Landroid/view/View;
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstalledView:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04009b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstalledView:Landroid/view/ViewGroup;

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstalledView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public onDataChanged()V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->onDataChanged()V

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v1, Lcom/google/android/finsky/api/model/DfeBulkDetails;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeBulkDetails;->getDocuments()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->addDocs(Ljava/util/Collection;)V

    iget-boolean v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mListInitialized:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstalledView:Landroid/view/ViewGroup;

    const v2, 0x7f08016f

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mInstalledView:Landroid/view/ViewGroup;

    const v2, 0x7f08018e

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-boolean v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mUseTwoColumnLayout:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mMyAppsList:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setChoiceMode(I)V

    :cond_1
    iput-boolean v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mListInitialized:Z

    const/4 v1, 0x0

    const v2, 0x7f07027a

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->configureEmptyUI(ZI)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->restoreTabSelection()V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->syncViewToState()V

    goto :goto_0
.end method

.method public onInstallPackageEvent(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;
    .param p3    # I

    sget-object v0, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOADING:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->INSTALLING:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    if-ne p2, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->notifyDataSetChanged()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->requestData()V

    goto :goto_0
.end method

.method public onLibraryContentsChanged(Lcom/google/android/finsky/library/AccountLibrary;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->requestData()V

    return-void
.end method

.method protected requestData()V
    .locals 2

    new-instance v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected retryFromError()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->clearState()V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->requestData()V

    return-void
.end method
