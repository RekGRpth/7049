.class public Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;
.super Lcom/google/android/finsky/fragments/PageFragment;
.source "MyAppsTabbedFragment.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/activities/myapps/OpenDetailsHandler;
.implements Lcom/google/android/finsky/utils/AppSupport$RefundListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$6;
    }
.end annotation


# static fields
.field private static sLastSelectedTabType:I


# instance fields
.field private mAccountSelector:Lcom/google/android/finsky/layout/AccountSelectorView;

.field private mArchiveProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

.field private mBreadcrumb:Ljava/lang/String;

.field private mCustomActionBar:Lcom/google/android/finsky/layout/CustomActionBar;

.field private mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

.field mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

.field mInstaller:Lcom/google/android/finsky/receivers/Installer;

.field private mListContainer:Landroid/view/ViewGroup;

.field mMobileDataState:Z

.field mNetworkStateChangedFilter:Landroid/content/IntentFilter;

.field mNetworkStateIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mTabContainer:Lcom/google/android/finsky/layout/play/PlayTabContainer;

.field private final mTabListener:Lcom/google/android/finsky/layout/CustomActionBar$TabListener;

.field private mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

.field private mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

.field private mUseActionBarTabs:Z

.field private mUseTwoColumnLayout:Z

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->sLastSelectedTabType:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/finsky/fragments/PageFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    new-instance v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$1;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabListener:Lcom/google/android/finsky/layout/CustomActionBar$TabListener;

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;I)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->switchSelectedTab(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->dismissArchiveProgressDialog()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->refundDocument(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)Lcom/google/android/finsky/activities/myapps/DocumentView;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)Lcom/google/android/finsky/api/DfeApi;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    return-object v0
.end method

.method private archiveDocs(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v4, 0x0

    const-string v1, ","

    invoke-static {v1, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "myApps.removeFromHistory?docIds="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v4, v4, v2}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    const-string v2, "myApps.removeFromHistory"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "docIds"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logTag(Ljava/lang/String;[Ljava/lang/Object;)V

    const v1, 0x7f0702e9

    invoke-static {v1}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->newInstance(I)Lcom/google/android/finsky/billing/ProgressDialogFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mArchiveProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mArchiveProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "archive_progress_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    sget-object v2, Lcom/google/android/finsky/library/AccountLibrary;->LIBRARY_ID_APPS:Ljava/lang/String;

    new-instance v3, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$4;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$4;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)V

    new-instance v4, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$5;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$5;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)V

    invoke-interface {v1, p1, v2, v3, v4}, Lcom/google/android/finsky/api/DfeApi;->archiveFromLibrary(Ljava/util/Collection;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    return-void
.end method

.method private cleanupActionModeIfNecessary()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->finishActiveMode()V

    return-void
.end method

.method private clearState()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDataView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabContainer:Lcom/google/android/finsky/layout/play/PlayTabContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabContainer:Lcom/google/android/finsky/layout/play/PlayTabContainer;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    iput-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mCustomActionBar:Lcom/google/android/finsky/layout/CustomActionBar;

    invoke-interface {v0}, Lcom/google/android/finsky/layout/CustomActionBar;->clearTabs()V

    return-void
.end method

.method private configureViewPager(ZZ)V
    .locals 5
    .param p1    # Z
    .param p2    # Z

    const/16 v4, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDataView:Landroid/view/ViewGroup;

    const v2, 0x7f080189

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    iput-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b001a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDataView:Landroid/view/ViewGroup;

    const v2, 0x7f0801c3

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayTabContainer;

    iput-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabContainer:Lcom/google/android/finsky/layout/play/PlayTabContainer;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabContainer:Lcom/google/android/finsky/layout/play/PlayTabContainer;

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->setVisibility(I)V

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabContainer:Lcom/google/android/finsky/layout/play/PlayTabContainer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mContext:Landroid/content/Context;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabContainer:Lcom/google/android/finsky/layout/play/PlayTabContainer;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->setSelectedIndicatorColor(I)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabContainer:Lcom/google/android/finsky/layout/play/PlayTabContainer;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabContainer:Lcom/google/android/finsky/layout/play/PlayTabContainer;

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->setVisibility(I)V

    goto :goto_1
.end method

.method private dismissArchiveProgressDialog()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mArchiveProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mArchiveProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mArchiveProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    :cond_0
    return-void
.end method

.method public static newInstance(Lcom/google/android/finsky/api/model/DfeToc;)Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;
    .locals 1
    .param p0    # Lcom/google/android/finsky/api/model/DfeToc;

    new-instance v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->setDfeToc(Lcom/google/android/finsky/api/model/DfeToc;)V

    return-object v0
.end method

.method private recordState()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->isDataReady()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->onSaveInstanceState(Landroid/os/Bundle;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->getTabType(I)I

    move-result v2

    sput v2, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->sLastSelectedTabType:I

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v3, "MyAppsTabbedAdapter.CurrentTabType"

    sget v4, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->sLastSelectedTabType:I

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mListContainer:Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    const/4 v1, -0x1

    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mListContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mListContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_4

    move v1, v0

    :cond_3
    if-ltz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v2, v1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->getTabType(I)I

    move-result v2

    sput v2, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->sLastSelectedTabType:I

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v3, "MyAppsTabbedAdapter.CurrentTabType"

    sget v4, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->sLastSelectedTabType:I

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private refundDocument(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "refund_confirm"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-eqz v3, :cond_0

    :goto_0
    return-void

    :cond_0
    const v3, 0x7f0701e3

    const v4, 0x7f0700fa

    const v5, 0x7f0700fb

    invoke-static {v3, v4, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(III)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v3, "package_name"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "account_name"

    invoke-virtual {v1, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x4

    invoke-virtual {v0, p0, v3, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    const-string v3, "refund_confirm"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setDocumentView()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDataView:Landroid/view/ViewGroup;

    const v2, 0x7f080190

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/activities/myapps/DocumentView;

    iput-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/activities/myapps/DocumentView;->init(Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    :cond_0
    return-void
.end method

.method private showAccountSelectorIfNecessary(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mAccountSelector:Lcom/google/android/finsky/layout/AccountSelectorView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->getTabType(I)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mAccountSelector:Lcom/google/android/finsky/layout/AccountSelectorView;

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/finsky/layout/AccountSelectorView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v1, 0x8

    goto :goto_0
.end method

.method private switchSelectedTab(I)V
    .locals 8
    .param p1    # I

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mListContainer:Landroid/view/ViewGroup;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/activities/myapps/DocumentView;->setVisibility(I)V

    :cond_0
    iget-object v6, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mCustomActionBar:Lcom/google/android/finsky/layout/CustomActionBar;

    invoke-interface {v6, p1}, Lcom/google/android/finsky/layout/CustomActionBar;->setSelectedTab(I)V

    const/4 v1, 0x0

    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mListContainer:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    if-ge v1, v6, :cond_4

    if-ne p1, v1, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_3

    move v4, v5

    :goto_2
    iget-object v6, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mListContainer:Landroid/view/ViewGroup;

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/activities/ViewPagerTab;

    if-nez v3, :cond_1

    iget-object v6, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    iget-object v7, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v6, v7, v1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/activities/ViewPagerTab;

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_1
    invoke-interface {v3, v2}, Lcom/google/android/finsky/activities/ViewPagerTab;->setTabSelected(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v2, v5

    goto :goto_1

    :cond_3
    const/16 v4, 0x8

    goto :goto_2

    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->showAccountSelectorIfNecessary(I)V

    return-void
.end method


# virtual methods
.method public adjustMenu(Ljava/util/List;Landroid/view/Menu;)V
    .locals 5
    .param p2    # Landroid/view/Menu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;",
            "Landroid/view/Menu;",
            ")V"
        }
    .end annotation

    const/4 v1, 0x1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    invoke-static {v2}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->canRemoveFromLibrary(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v1, 0x0

    :cond_1
    const v4, 0x7f08025e

    invoke-interface {p2, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    return-void
.end method

.method public clearDocDetails()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/myapps/DocumentView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public confirmArchiveDocs(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)V"
        }
    .end annotation

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ne v6, v8, :cond_0

    const v7, 0x7f0702ea

    new-array v8, v8, [Ljava/lang/Object;

    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v9

    invoke-virtual {p0, v7, v8}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :goto_0
    const v6, 0x7f0701da

    const v7, 0x7f070058

    invoke-static {v4, v6, v7}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(Ljava/lang/String;II)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    const v6, 0x7f0702eb

    invoke-virtual {p0, v6}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_1
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v6, "docid_list"

    invoke-virtual {v5, v6, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const/4 v6, 0x6

    invoke-virtual {v0, p0, v6, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v7, "archive_confirm"

    invoke-virtual {v0, v6, v7}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public getDisplayedDocId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mListContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myapps/DocumentView;->getDocId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected getLayoutRes()I
    .locals 1

    const v0, 0x7f04009e

    return v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method handleMenuItem(Ljava/util/List;Landroid/view/MenuItem;)Z
    .locals 1
    .param p2    # Landroid/view/MenuItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;",
            "Landroid/view/MenuItem;",
            ")Z"
        }
    .end annotation

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->confirmArchiveDocs(Ljava/util/List;)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f08025e
        :pswitch_0
    .end packed-switch
.end method

.method protected isDataReady()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onActivityCreated(Landroid/os/Bundle;)V

    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getCorpusMyCollectionDescription(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mContext:Landroid/content/Context;

    const v4, 0x7f0701ae

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mBreadcrumb:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v3}, Lcom/google/android/finsky/activities/MainActivity;->getCustomActionBar()Lcom/google/android/finsky/layout/CustomActionBar;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mCustomActionBar:Lcom/google/android/finsky/layout/CustomActionBar;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mUseActionBarTabs:Z

    const v3, 0x7f090003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mUseTwoColumnLayout:Z

    if-eqz p1, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {v3}, Landroid/os/Bundle;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getInstallPolicies()Lcom/google/android/finsky/installer/InstallPolicies;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    invoke-virtual {v3}, Lcom/google/android/finsky/installer/InstallPolicies;->isMobileNetwork()Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mMobileDataState:Z

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mNetworkStateChangedFilter:Landroid/content/IntentFilter;

    new-instance v3, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$2;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$2;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)V

    iput-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mNetworkStateIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/finsky/utils/Notifier;->hideUpdatesAvailableMessage()V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->isDataReady()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->switchToLoading()V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->requestData()V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->rebindActionBar()V

    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->rebindViews()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->setRetainInstance(Z)V

    return-void
.end method

.method public onDestroyView()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->cleanupActionModeIfNecessary()V

    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->recordState()V

    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->clearState()V

    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onDestroyView()V

    return-void
.end method

.method protected onInitViewBinders()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->setDocumentView()V

    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/activities/myapps/DocumentView;->onNegativeClick(ILandroid/os/Bundle;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->refresh()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPageScrollStateChanged(I)V
    .locals 1
    .param p1    # I

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mUseActionBarTabs:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabContainer:Lcom/google/android/finsky/layout/play/PlayTabContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->onPageScrollStateChanged(I)V

    :cond_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1
    .param p1    # I
    .param p2    # F
    .param p3    # I

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mUseActionBarTabs:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabContainer:Lcom/google/android/finsky/layout/play/PlayTabContainer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->onPageScrolled(IFI)V

    :cond_0
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1    # I

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mUseActionBarTabs:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->switchSelectedTab(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->onPageSelected(I)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabContainer:Lcom/google/android/finsky/layout/play/PlayTabContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->onPageSelected(I)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onPause()V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mNetworkStateIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 9
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string v5, "Unexpected requestCode %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    invoke-virtual {v5, p1, p2}, Lcom/google/android/finsky/activities/myapps/DocumentView;->onPositiveClick(ILandroid/os/Bundle;)V

    goto :goto_0

    :pswitch_2
    const-string v5, "package_name"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-interface {v5, v2}, Lcom/google/android/finsky/receivers/Installer;->uninstallAssetSilently(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    const-string v5, "package_name"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "account_name"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3, p0}, Lcom/google/android/finsky/utils/AppSupport;->silentRefund(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/utils/AppSupport$RefundListener;)V

    goto :goto_0

    :pswitch_4
    const-string v5, "docid_list"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v5}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->getCurrentTab()Lcom/google/android/finsky/activities/myapps/MyAppsTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->getDisplayedDocId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->clearDocumentView()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->cleanupActionModeIfNecessary()V

    invoke-direct {p0, v1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->archiveDocs(Ljava/util/List;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onRefundComplete(Lcom/google/android/finsky/utils/AppSupport$RefundResult;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/android/finsky/utils/AppSupport$RefundResult;
    .param p2    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$6;->$SwitchMap$com$google$android$finsky$utils$AppSupport$RefundResult:[I

    invoke-virtual {p1}, Lcom/google/android/finsky/utils/AppSupport$RefundResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "enum %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-interface {v0, p2}, Lcom/google/android/finsky/receivers/Installer;->uninstallAssetSilently(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/AppSupport;->showRefundFailureDialog(Landroid/support/v4/app/FragmentManager;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->refresh()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public onRefundStart()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mNetworkStateIntentReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mNetworkStateChangedFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->recordState()V

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onStart()V

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "archive_progress_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/ProgressDialogFragment;

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mArchiveProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->dismissArchiveProgressDialog()V

    return-void
.end method

.method public openDocDetails(Lcom/google/android/finsky/api/model/Document;)V
    .locals 6
    .param p1    # Lcom/google/android/finsky/api/model/Document;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mListContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    const-string v3, "myApps"

    move-object v1, p1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myapps/DocumentView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/myapps/DocumentView;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDocView:Lcom/google/android/finsky/activities/myapps/DocumentView;

    new-instance v1, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$3;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;Lcom/google/android/finsky/api/model/Document;)V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, p0, p1, v1, v2}, Lcom/google/android/finsky/activities/myapps/DocumentView;->bind(Landroid/support/v4/app/Fragment;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/activities/myapps/DocumentView$DocumentActionHandler;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public rebindActionBar()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mBreadcrumb:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateBreadcrumb(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateCurrentBackendId(I)V

    return-void
.end method

.method public rebindViews()V
    .locals 19

    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->setDocumentView()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->switchToData()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->rebindActionBar()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mContext:Landroid/content/Context;

    const/4 v3, 0x3

    invoke-static {v1, v3}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v10

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getView()Landroid/view/View;

    move-result-object v12

    const v1, 0x7f0800b8

    invoke-virtual {v12, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    if-eqz v17, :cond_2

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mUseActionBarTabs:Z

    if-nez v1, :cond_4

    const/4 v15, 0x1

    :goto_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/library/Libraries;->hasSubscriptions()Z

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/activities/AuthenticatedActivity;

    new-instance v1, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mBitmapLoader:Lcom/google/android/finsky/utils/BitmapLoader;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    move-object/from16 v9, p0

    invoke-direct/range {v1 .. v9}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;-><init>(Lcom/google/android/finsky/activities/AuthenticatedActivity;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/BitmapLoader;ZLandroid/os/Bundle;Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v7}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->configureViewPager(ZZ)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v3, "MyAppsTabbedAdapter.CurrentTabType"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v3, "MyAppsTabbedAdapter.CurrentTabType"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    :goto_2
    const/4 v1, 0x1

    sput v1, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->sLastSelectedTabType:I

    const/4 v14, 0x0

    const/4 v13, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->getCount()I

    move-result v1

    if-ge v13, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v1, v13}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->getTabType(I)I

    move-result v1

    if-ne v1, v11, :cond_6

    move v14, v13

    :cond_3
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mUseActionBarTabs:Z

    if-eqz v1, :cond_a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDataView:Landroid/view/ViewGroup;

    const v3, 0x7f08018f

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mListContainer:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mCustomActionBar:Lcom/google/android/finsky/layout/CustomActionBar;

    invoke-interface {v1}, Lcom/google/android/finsky/layout/CustomActionBar;->clearTabs()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mListContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    const/4 v13, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->getCount()I

    move-result v1

    if-ge v13, v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mCustomActionBar:Lcom/google/android/finsky/layout/CustomActionBar;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v3, v13}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->getPageTitle(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabListener:Lcom/google/android/finsky/layout/CustomActionBar$TabListener;

    invoke-interface {v1, v3, v4}, Lcom/google/android/finsky/layout/CustomActionBar;->addTab(Ljava/lang/String;Lcom/google/android/finsky/layout/CustomActionBar$TabListener;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mListContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3, v13}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/android/finsky/activities/ViewPagerTab;

    if-ne v13, v14, :cond_7

    const/16 v18, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mListContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v13}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    move/from16 v0, v18

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mListContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v13}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    if-ne v13, v14, :cond_8

    const/4 v1, 0x1

    :goto_6
    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Lcom/google/android/finsky/activities/ViewPagerTab;->setTabSelected(Z)V

    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    :cond_4
    const/4 v15, 0x0

    goto/16 :goto_1

    :cond_5
    sget v11, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->sLastSelectedTabType:I

    goto/16 :goto_2

    :cond_6
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_3

    :cond_7
    const/16 v18, 0x8

    goto :goto_5

    :cond_8
    const/4 v1, 0x0

    goto :goto_6

    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mCustomActionBar:Lcom/google/android/finsky/layout/CustomActionBar;

    invoke-interface {v1, v14}, Lcom/google/android/finsky/layout/CustomActionBar;->setSelectedTab(I)V

    :goto_7
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mUseTwoColumnLayout:Z

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDataView:Landroid/view/ViewGroup;

    const v3, 0x7f080185

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/AccountSelectorView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mAccountSelector:Lcom/google/android/finsky/layout/AccountSelectorView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mAccountSelector:Lcom/google/android/finsky/layout/AccountSelectorView;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/activities/AuthenticatedActivity;

    invoke-virtual {v3, v1}, Lcom/google/android/finsky/layout/AccountSelectorView;->configure(Lcom/google/android/finsky/activities/AuthenticatedActivity;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->showAccountSelectorIfNecessary(I)V

    goto/16 :goto_0

    :cond_a
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mListContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v1, v3}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->onPageScrolled(IFI)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v3, 0x0

    invoke-virtual {v1, v14, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    goto :goto_7
.end method

.method public refresh()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->isDataReady()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->rebindViews()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->refresh()V

    goto :goto_0
.end method

.method protected requestData()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->clearState()V

    return-void
.end method
