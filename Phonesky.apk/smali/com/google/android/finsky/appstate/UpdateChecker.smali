.class public Lcom/google/android/finsky/appstate/UpdateChecker;
.super Ljava/lang/Object;
.source "UpdateChecker.java"


# instance fields
.field private final mAppStates:Lcom/google/android/finsky/appstate/AppStates;

.field private final mContext:Landroid/content/Context;

.field private final mDfeApiProvider:Lcom/google/android/finsky/api/DfeApiProvider;

.field private final mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

.field private final mInstaller:Lcom/google/android/finsky/receivers/Installer;

.field private final mLibraries:Lcom/google/android/finsky/library/Libraries;

.field private final mNotifier:Lcom/google/android/finsky/utils/Notifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/api/DfeApiProvider;Lcom/google/android/finsky/installer/InstallPolicies;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/utils/Notifier;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/finsky/library/Libraries;
    .param p3    # Lcom/google/android/finsky/appstate/AppStates;
    .param p4    # Lcom/google/android/finsky/api/DfeApiProvider;
    .param p5    # Lcom/google/android/finsky/installer/InstallPolicies;
    .param p6    # Lcom/google/android/finsky/receivers/Installer;
    .param p7    # Lcom/google/android/finsky/utils/Notifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    iput-object p3, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    iput-object p4, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mDfeApiProvider:Lcom/google/android/finsky/api/DfeApiProvider;

    iput-object p5, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    iput-object p6, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    iput-object p7, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/appstate/UpdateChecker;)Lcom/google/android/finsky/library/Libraries;
    .locals 1
    .param p0    # Lcom/google/android/finsky/appstate/UpdateChecker;

    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/appstate/UpdateChecker;)Lcom/google/android/finsky/appstate/AppStates;
    .locals 1
    .param p0    # Lcom/google/android/finsky/appstate/UpdateChecker;

    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/appstate/UpdateChecker;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/finsky/appstate/UpdateChecker;

    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/appstate/UpdateChecker;Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/appstate/UpdateChecker;
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/appstate/UpdateChecker;->handleUpdates(Landroid/content/Context;Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/appstate/UpdateChecker;)Lcom/google/android/finsky/api/DfeApiProvider;
    .locals 1
    .param p0    # Lcom/google/android/finsky/appstate/UpdateChecker;

    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mDfeApiProvider:Lcom/google/android/finsky/api/DfeApiProvider;

    return-object v0
.end method

.method private handleUpdates(Landroid/content/Context;Ljava/util/List;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)V"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    invoke-virtual {v3, p2}, Lcom/google/android/finsky/installer/InstallPolicies;->getApplicationsWithUpdates(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sget-object v3, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v3}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v3}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    invoke-virtual {v3}, Lcom/google/android/finsky/installer/InstallPolicies;->isWifiNetwork()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v4}, Lcom/google/android/finsky/installer/InstallPolicies;->getApplicationsEligibleForAutoUpdate(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    const-string v4, "auto_update"

    invoke-interface {v3, v2, v4}, Lcom/google/android/finsky/receivers/Installer;->updateInstalledApps(Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    invoke-virtual {v3, v1}, Lcom/google/android/finsky/installer/InstallPolicies;->getApplicationsEligibleForNotification(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/finsky/appstate/UpdateChecker;->showManualUpdateNotifications(Ljava/util/List;Ljava/util/List;)V

    invoke-direct {p0, v0}, Lcom/google/android/finsky/appstate/UpdateChecker;->markAppsAsNotified(Ljava/util/List;)V

    return-void
.end method

.method private markAppsAsNotified(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v6}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;->getVersionCode()I

    move-result v5

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getLastNotifiedVersion()I

    move-result v6

    if-le v5, v6, :cond_0

    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v6}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v6

    invoke-interface {v6, v4, v5}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setLastNotifiedVersion(Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public static migrateAllAppsToUseGlobalUpdateSetting(Lcom/google/android/finsky/appstate/AppStates;ZLcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;)V
    .locals 8
    .param p0    # Lcom/google/android/finsky/appstate/AppStates;
    .param p1    # Z
    .param p2    # Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->getAll()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->isGmsCore(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAutoUpdate()Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    move-result-object v3

    if-nez p1, :cond_1

    sget-object v5, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->DEFAULT:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    if-ne v3, v5, :cond_0

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setAutoUpdate(Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;)V

    const-string v5, "Migrate %s autoupdate from default to %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    const/4 v7, 0x1

    aput-object p2, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public static migrateAutoUpdateSettings(Lcom/google/android/finsky/appstate/AppStates;)V
    .locals 8
    .param p0    # Lcom/google/android/finsky/appstate/AppStates;

    const/4 v3, 0x0

    const/4 v6, 0x1

    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/AppStates;->isLoaded()Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "Require loaded app states to migrate auto-update state."

    new-array v7, v3, [Ljava/lang/Object;

    invoke-static {v5, v7}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    sget-object v5, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_UPDATE_BY_DEFAULT:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->exists()Z

    move-result v2

    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->exists()Z

    move-result v1

    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    :goto_0
    return-void

    :cond_1
    if-nez v2, :cond_2

    if-nez v1, :cond_2

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getVersionCodeOfLastRun()I

    move-result v5

    const/4 v7, -0x1

    if-ne v5, v7, :cond_2

    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    sget-object v5, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_UPDATE_BY_DEFAULT:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->exists()Z

    move-result v5

    if-nez v5, :cond_4

    const/4 v4, 0x1

    sget-object v5, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    sget-object v5, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    :cond_3
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    :cond_4
    if-nez v0, :cond_5

    move v3, v6

    :cond_5
    sget-object v5, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->USE_GLOBAL:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    invoke-static {p0, v3, v5}, Lcom/google/android/finsky/appstate/UpdateChecker;->migrateAllAppsToUseGlobalUpdateSetting(Lcom/google/android/finsky/appstate/AppStates;ZLcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;)V

    sget-object v5, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_UPDATE_BY_DEFAULT:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    sget-object v5, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->hadPreJsAutoUpdateSettings:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static setAllAppsToUseGlobalDefault(Lcom/google/android/finsky/appstate/AppStates;)V
    .locals 2
    .param p0    # Lcom/google/android/finsky/appstate/AppStates;

    const/4 v0, 0x1

    sget-object v1, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->USE_GLOBAL:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    invoke-static {p0, v0, v1}, Lcom/google/android/finsky/appstate/UpdateChecker;->migrateAllAppsToUseGlobalUpdateSetting(Lcom/google/android/finsky/appstate/AppStates;ZLcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;)V

    return-void
.end method

.method private showManualUpdateNotifications(Ljava/util/List;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)V"
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    sget-object v2, Lcom/google/android/finsky/utils/VendingPreferences;->NOTIFY_UPDATES:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "Notifying user that [%d/%d] applications have updates."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    if-ne v0, v6, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    invoke-interface {v3, v2}, Lcom/google/android/finsky/utils/Notifier;->showSingleUpdateAvailableMessage(Lcom/google/android/finsky/api/model/Document;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-interface {v2, p2, v1}, Lcom/google/android/finsky/utils/Notifier;->showUpdatesAvailableMessage(Ljava/util/List;I)V

    goto :goto_0
.end method


# virtual methods
.method public checkForUpdates(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 8
    .param p1    # Ljava/lang/Runnable;
    .param p2    # Ljava/lang/Runnable;

    const/4 v7, 0x0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v6

    if-nez v6, :cond_1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v0}, Lcom/google/android/finsky/appstate/AppStates;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "Require loaded app states to perform update check."

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-static {v0}, Lcom/google/android/finsky/appstate/UpdateChecker;->migrateAutoUpdateSettings(Lcom/google/android/finsky/appstate/AppStates;)V

    move-object v3, v6

    new-instance v2, Lcom/google/android/finsky/appstate/GmsCoreHelper;

    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-direct {v2, v0, v3}, Lcom/google/android/finsky/appstate/GmsCoreHelper;-><init>(Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)V

    new-instance v0, Lcom/google/android/finsky/appstate/UpdateChecker$1;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/appstate/UpdateChecker$1;-><init>(Lcom/google/android/finsky/appstate/UpdateChecker;Lcom/google/android/finsky/appstate/GmsCoreHelper;Landroid/accounts/Account;Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/UpdateChecker$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
