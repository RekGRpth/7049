.class public Lcom/google/android/finsky/billing/AsyncAuthenticator;
.super Ljava/lang/Object;
.source "AsyncAuthenticator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/AsyncAuthenticator$Listener;
    }
.end annotation


# instance fields
.field private final mAuthenticator:Lcom/android/volley/toolbox/Authenticator;


# direct methods
.method public constructor <init>(Lcom/android/volley/toolbox/Authenticator;)V
    .locals 0
    .param p1    # Lcom/android/volley/toolbox/Authenticator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/finsky/billing/AsyncAuthenticator;->mAuthenticator:Lcom/android/volley/toolbox/Authenticator;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/AsyncAuthenticator;)Lcom/android/volley/toolbox/Authenticator;
    .locals 1
    .param p0    # Lcom/google/android/finsky/billing/AsyncAuthenticator;

    iget-object v0, p0, Lcom/google/android/finsky/billing/AsyncAuthenticator;->mAuthenticator:Lcom/android/volley/toolbox/Authenticator;

    return-object v0
.end method


# virtual methods
.method public getToken(Lcom/google/android/finsky/billing/AsyncAuthenticator$Listener;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/billing/AsyncAuthenticator$Listener;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/finsky/billing/AsyncAuthenticator$1;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/finsky/billing/AsyncAuthenticator$1;-><init>(Lcom/google/android/finsky/billing/AsyncAuthenticator;Ljava/lang/String;Lcom/google/android/finsky/billing/AsyncAuthenticator$Listener;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/AsyncAuthenticator$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
