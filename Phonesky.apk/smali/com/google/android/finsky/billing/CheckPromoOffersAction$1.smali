.class Lcom/google/android/finsky/billing/CheckPromoOffersAction$1;
.super Ljava/lang/Object;
.source "CheckPromoOffersAction.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/CheckPromoOffersAction;->onAuthTokenReceived(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/CheckPromoOffersAction;

.field final synthetic val$authToken:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/CheckPromoOffersAction;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/billing/CheckPromoOffersAction$1;->this$0:Lcom/google/android/finsky/billing/CheckPromoOffersAction;

    iput-object p2, p0, Lcom/google/android/finsky/billing/CheckPromoOffersAction$1;->val$authToken:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;)V
    .locals 5
    .param p1    # Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;->getCheckoutTokenRequired()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "Checkout token invalid."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/finsky/billing/CheckPromoOffersAction$1;->this$0:Lcom/google/android/finsky/billing/CheckPromoOffersAction;

    # getter for: Lcom/google/android/finsky/billing/CheckPromoOffersAction;->sNumAuthRetries:I
    invoke-static {v1}, Lcom/google/android/finsky/billing/CheckPromoOffersAction;->access$000(Lcom/google/android/finsky/billing/CheckPromoOffersAction;)I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/billing/CheckPromoOffersAction$1;->this$0:Lcom/google/android/finsky/billing/CheckPromoOffersAction;

    iget-object v2, p0, Lcom/google/android/finsky/billing/CheckPromoOffersAction$1;->val$authToken:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/billing/CheckPromoOffersAction;->checkPromoOffers(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/CheckPromoOffersAction;->access$100(Lcom/google/android/finsky/billing/CheckPromoOffersAction;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/finsky/billing/CheckPromoOffersAction$1;->this$0:Lcom/google/android/finsky/billing/CheckPromoOffersAction;

    # --operator for: Lcom/google/android/finsky/billing/CheckPromoOffersAction;->sNumAuthRetries:I
    invoke-static {v1}, Lcom/google/android/finsky/billing/CheckPromoOffersAction;->access$006(Lcom/google/android/finsky/billing/CheckPromoOffersAction;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "auth retries exceeded, skipping promo offer check"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    :goto_1
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->checkPromoOffers:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    iget-object v2, p0, Lcom/google/android/finsky/billing/CheckPromoOffersAction$1;->this$0:Lcom/google/android/finsky/billing/CheckPromoOffersAction;

    # getter for: Lcom/google/android/finsky/billing/CheckPromoOffersAction;->mAccount:Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/finsky/billing/CheckPromoOffersAction;->access$200(Lcom/google/android/finsky/billing/CheckPromoOffersAction;)Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/billing/CheckPromoOffersAction$1;->this$0:Lcom/google/android/finsky/billing/CheckPromoOffersAction;

    # getter for: Lcom/google/android/finsky/billing/CheckPromoOffersAction;->mActivity:Lcom/google/android/finsky/activities/MainActivity;
    invoke-static {v1}, Lcom/google/android/finsky/billing/CheckPromoOffersAction;->access$300(Lcom/google/android/finsky/billing/CheckPromoOffersAction;)Lcom/google/android/finsky/activities/MainActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/activities/MainActivity;->startActivity(Landroid/content/Intent;)V

    :cond_2
    # setter for: Lcom/google/android/finsky/billing/CheckPromoOffersAction;->sRunning:Z
    invoke-static {v4}, Lcom/google/android/finsky/billing/CheckPromoOffersAction;->access$402(Z)Z

    iget-object v1, p0, Lcom/google/android/finsky/billing/CheckPromoOffersAction$1;->this$0:Lcom/google/android/finsky/billing/CheckPromoOffersAction;

    # getter for: Lcom/google/android/finsky/billing/CheckPromoOffersAction;->mCallback:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/finsky/billing/CheckPromoOffersAction;->access$500(Lcom/google/android/finsky/billing/CheckPromoOffersAction;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;->getAvailableOfferCount()I

    move-result v1

    if-lez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/billing/CheckPromoOffersAction$1;->this$0:Lcom/google/android/finsky/billing/CheckPromoOffersAction;

    # getter for: Lcom/google/android/finsky/billing/CheckPromoOffersAction;->mAccount:Landroid/accounts/Account;
    invoke-static {v1}, Lcom/google/android/finsky/billing/CheckPromoOffersAction;->access$200(Lcom/google/android/finsky/billing/CheckPromoOffersAction;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;->getAvailableOffer(I)Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$AvailablePromoOffer;

    move-result-object v2

    const-string v3, "checkPromoOffersAction"

    invoke-static {v1, v2, v3}, Lcom/google/android/finsky/activities/AvailablePromoOfferActivity;->getIntent(Ljava/lang/String;Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$AvailablePromoOffer;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;->hasRedeemedOffer()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/CheckPromoOffersAction$1;->onResponse(Lcom/google/android/finsky/remoting/protos/CheckPromoOffer$CheckPromoOfferResponse;)V

    return-void
.end method
