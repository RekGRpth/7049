.class Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;
.super Ljava/lang/Object;
.source "BillingProfileFragment.java"

# interfaces
.implements Lcom/google/android/finsky/fragments/SidecarFragment$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BillingProfileListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$1;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;
    .param p2    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$1;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;-><init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V

    return-void
.end method


# virtual methods
.method public onStateChange(Lcom/google/android/finsky/fragments/SidecarFragment;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/fragments/SidecarFragment;

    invoke-virtual {p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->getStateInstance()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mLastBillingProfileStateInstance:I
    invoke-static {v1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$1400(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)I

    move-result v1

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    invoke-virtual {p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->getStateInstance()I

    move-result v1

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mLastBillingProfileStateInstance:I
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$1402(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;I)I

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProgressIndicator:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$1500(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfileView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$1600(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->getState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->requestBillingProfile()V
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$1300(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;
    invoke-static {v1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$500(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getBillingProfile()Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;

    move-result-object v1

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->handleSuccess(Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$1700(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->handleError()V
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$1800(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfileView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$1600(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
