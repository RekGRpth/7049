.class public Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;
.super Landroid/support/v4/app/Fragment;
.source "BillingProfileFragment.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$Listener;,
        Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$CarrierBillingProvisioningListener;,
        Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;
    }
.end annotation


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mActionsHeader:Landroid/widget/TextView;

.field private mActionsHeaderSeparator:Landroid/view/View;

.field private mActionsView:Landroid/view/ViewGroup;

.field private final mBillingProfileListener:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;

.field private mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

.field private final mCarrierBillingProvisioningListener:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$CarrierBillingProvisioningListener;

.field private mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

.field private mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mExistingInstrumentsView:Landroid/view/ViewGroup;

.field private mInstrumentFactory:Lcom/google/android/finsky/billing/InstrumentFactory;

.field private mLastBillingProfileStateInstance:I

.field private mLastCarrierBillingStateInstance:I

.field private mProfile:Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;

.field private mProfileDirty:Z

.field private mProfileView:Landroid/view/View;

.field private mProgressFragment:Lcom/google/android/finsky/billing/ProgressDialogFragment;

.field private mProgressIndicator:Landroid/view/View;

.field private mPurchaseContextToken:Ljava/lang/String;

.field private mStoredValueInstrumentId:Ljava/lang/String;

.field private final mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;

    invoke-direct {v0, p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;-><init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$1;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileListener:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;

    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$CarrierBillingProvisioningListener;

    invoke-direct {v0, p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$CarrierBillingProvisioningListener;-><init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$1;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mCarrierBillingProvisioningListener:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$CarrierBillingProvisioningListener;

    iput v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mLastBillingProfileStateInstance:I

    iput v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mLastCarrierBillingStateInstance:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfileDirty:Z

    new-instance v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-direct {v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;-><init>()V

    const/16 v1, 0x320

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;
    .param p1    # Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->addDcb3(Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->ensureProvisionedAndAddDcb2()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->notifyListenerOnInstrumentSelected(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->requestBillingProfile()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)I
    .locals 1
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    iget v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mLastBillingProfileStateInstance:I

    return v0
.end method

.method static synthetic access$1402(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;I)I
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;
    .param p1    # I

    iput p1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mLastBillingProfileStateInstance:I

    return p1
.end method

.method static synthetic access$1500(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProgressIndicator:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfileView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;
    .param p1    # Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->handleSuccess(Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->handleError()V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)I
    .locals 1
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    iget v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mLastCarrierBillingStateInstance:I

    return v0
.end method

.method static synthetic access$1902(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;I)I
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;
    .param p1    # I

    iput p1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mLastCarrierBillingStateInstance:I

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Lcom/google/android/finsky/billing/InstrumentFactory;
    .locals 1
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mInstrumentFactory:Lcom/google/android/finsky/billing/InstrumentFactory;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Lcom/google/android/finsky/billing/ProgressDialogFragment;
    .locals 1
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProgressFragment:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/finsky/billing/ProgressDialogFragment;)Lcom/google/android/finsky/billing/ProgressDialogFragment;
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;
    .param p1    # Lcom/google/android/finsky/billing/ProgressDialogFragment;

    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProgressFragment:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    return-object p1
.end method

.method static synthetic access$202(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/finsky/billing/InstrumentFactory;)Lcom/google/android/finsky/billing/InstrumentFactory;
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;
    .param p1    # Lcom/google/android/finsky/billing/InstrumentFactory;

    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mInstrumentFactory:Lcom/google/android/finsky/billing/InstrumentFactory;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;
    .locals 1
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->addDcb2()V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->showError(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->renderProfile()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;
    .locals 1
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileListener:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;
    .locals 1
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Lcom/google/android/finsky/analytics/FinskyEventLog;
    .locals 1
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->addCreditCard()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->redeemCheckoutCode()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;
    .param p1    # Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->topup(Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;)V

    return-void
.end method

.method private addCreditCard()V
    .locals 8

    const/4 v5, 0x0

    const/4 v2, 0x0

    new-instance v1, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->setInstrumentFamily(I)Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v3, Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;->INTERNAL:Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;

    move v4, v2

    move-object v6, v5

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/activities/FinskyCreateInstrumentActivity;->createIntent(Ljava/lang/String;Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;ILcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;ZLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    const/4 v0, 0x1

    invoke-virtual {p0, v7, v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private addDcb(Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;)V
    .locals 7
    .param p1    # Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->isDcb3SetupOption(Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v6, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$5;

    invoke-direct {v6, p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$5;-><init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getDisplayTitle()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0200b3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->addEntry(Landroid/view/ViewGroup;Ljava/lang/String;IZLjava/lang/String;Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    new-instance v6, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$6;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$6;-><init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V

    goto :goto_0
.end method

.method private addDcb2()V
    .locals 9

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v8, 0x2

    new-instance v0, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;

    invoke-direct {v0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;-><init>()V

    invoke-virtual {v0, v8}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->setInstrumentFamily(I)Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->setVersion(I)Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v3, Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;->INTERNAL:Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;

    move v4, v2

    move-object v6, v5

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/activities/FinskyCreateInstrumentActivity;->createIntent(Ljava/lang/String;Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;ILcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;ZLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7, v8}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private addDcb3(Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;)V
    .locals 10
    .param p1    # Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;

    const/4 v5, 0x0

    const/4 v9, 0x2

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;

    invoke-direct {v0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;-><init>()V

    invoke-virtual {v0, v9}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->setInstrumentFamily(I)Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;

    move-result-object v0

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->setVersion(I)Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->setCarrierBillingStatus(Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;)Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v3, Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;->INTERNAL:Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;

    move v4, v2

    move-object v6, v5

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/activities/FinskyCreateInstrumentActivity;->createIntent(Ljava/lang/String;Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;ILcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;ZLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v0, "dcb_instrument"

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v2

    invoke-virtual {v7, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "extra_paramters"

    invoke-virtual {v8, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p0, v8, v9}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private addEntry(Landroid/view/ViewGroup;Ljava/lang/String;IZLjava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 6
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Z
    .param p5    # Ljava/lang/String;
    .param p6    # Landroid/view/View$OnClickListener;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    if-nez p5, :cond_2

    const v4, 0x7f04001d

    invoke-virtual {v1, v4, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    if-eqz p4, :cond_0

    const v4, 0x7f08006b

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-virtual {v2, p6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    const v4, 0x7f080069

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p3, :cond_1

    invoke-virtual {v3, p3, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :cond_1
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void

    :cond_2
    const v4, 0x7f04001c

    invoke-virtual {v1, v4, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v4, 0x7f08006a

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private ensureProvisionedAndAddDcb2()V
    .locals 3

    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getCarrierBillingStorage()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->isProvisioned(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->addDcb2()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "BillingProfileFragment.carrierBillingSidecar"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    if-eqz v0, :cond_1

    const-string v0, "Not expected to have a carrier billing fragment."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mCarrierBillingProvisioningListener:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$CarrierBillingProvisioningListener;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    const-string v2, "BillingProfileFragment.carrierBillingSidecar"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method private getListener()Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$Listener;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$Listener;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$Listener;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "No listener registered."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handleError()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getSubstate()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, "Don\'t know how to handle error substate %d, cancel."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getSubstate()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->notifyListenerOnCancel()V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getErrorMessageHtml()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->showError(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getVolleyError()Lcom/android/volley/VolleyError;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->showError(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleSuccess(Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;)V
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;

    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfile:Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfileDirty:Z

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->renderProfile()V

    return-void
.end method

.method private isDcb3SetupOption(Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;)Z
    .locals 2
    .param p1    # Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasCarrierBillingInstrumentStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getCarrierBillingInstrumentStatus()Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;->getApiVersion()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;
    .locals 3
    .param p0    # Landroid/accounts/Account;
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "BillingProfileFragment.account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "BillingProfileFragment.purchaseContextToken"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private notifyListenerOnCancel()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getListener()Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$Listener;->onCancel()V

    :cond_0
    return-void
.end method

.method private notifyListenerOnInstrumentSelected(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getListener()Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$Listener;->onInstrumentSelected(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private redeemCheckoutCode()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, v2, v2, v3, v3}, Lcom/google/android/finsky/activities/RedeemGiftCardActivity;->createIntent(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private renderInstruments(Ljava/util/List;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfile:Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;->getSelectedExternalInstrumentId()Ljava/lang/String;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;

    invoke-virtual {v10}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->getDisabledInfoCount()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {v10, v0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->getDisabledInfo(I)Lcom/google/android/finsky/remoting/protos/CommonDevice$DisabledInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$DisabledInfo;->getDisabledMessageHtml()Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-virtual {v10}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->getExternalInstrumentId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    new-instance v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    invoke-direct {v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;-><init>()V

    invoke-virtual {v10}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->getInstrumentFamily()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->setInstrumentFamily(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->setIsDefault(Z)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    move-result-object v12

    new-instance v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    invoke-direct {v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;-><init>()V

    invoke-virtual {v0, v12}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->setInstrumentInfo(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    move-result-object v7

    invoke-virtual {v10}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->getInstrumentFamily()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    invoke-virtual {v10}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->hasStoredValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v10}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->getStoredValue()Lcom/google/android/finsky/remoting/protos/CommonDevice$StoredValueInstrument;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/finsky/remoting/protos/CommonDevice$StoredValueInstrument;->getType()I

    move-result v0

    const/16 v1, 0x21

    if-ne v0, v1, :cond_0

    invoke-virtual {v10}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->getExternalInstrumentId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mStoredValueInstrumentId:Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mExistingInstrumentsView:Landroid/view/ViewGroup;

    invoke-virtual {v10}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->getDisplayTitle()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-instance v6, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$7;

    invoke-direct {v6, p0, v7, v11}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$7;-><init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;Ljava/lang/String;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->addEntry(Landroid/view/ViewGroup;Ljava/lang/String;IZLjava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mExistingInstrumentsView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mExistingInstrumentsView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mExistingInstrumentsView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->hideSeparator()V

    :cond_3
    return-void
.end method

.method private renderOptions(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;",
            ">;)V"
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;

    invoke-virtual {v9}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, "Skipping unknown option: type=%d, displayTitle=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v9}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-virtual {v9}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getDisplayTitle()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    invoke-virtual {v9}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getDisplayTitle()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0200b4

    new-instance v6, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$2;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$2;-><init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->addEntry(Landroid/view/ViewGroup;Ljava/lang/String;IZLjava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v9}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->addDcb(Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    invoke-virtual {v9}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getDisplayTitle()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0200b5

    new-instance v6, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$3;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$3;-><init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->addEntry(Landroid/view/ViewGroup;Ljava/lang/String;IZLjava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    invoke-virtual {v9}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getDisplayTitle()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0200b6

    new-instance v6, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$4;

    invoke-direct {v6, p0, v9}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$4;-><init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->addEntry(Landroid/view/ViewGroup;Ljava/lang/String;IZLjava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->hideSeparator()V

    :cond_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private renderProfile()V
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfileDirty:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfileDirty:Z

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mExistingInstrumentsView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfile:Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProgressIndicator:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfile:Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;->getInstrumentList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->renderInstruments(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfile:Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;->getInstrumentCount()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsHeader:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsHeaderSeparator:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfile:Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;->getBillingProfileOptionList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->renderOptions(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfileView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v1, 0x0

    const/16 v3, 0x321

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsHeader:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsHeaderSeparator:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private requestBillingProfile()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mInstrumentFactory:Lcom/google/android/finsky/billing/InstrumentFactory;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/billing/InstrumentFactory;->getAllPrepareOrBillingProfileParams(Z)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->start(Ljava/util/Map;)V

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->showLoading()V

    return-void
.end method

.method private showError(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->renderProfile()V

    const v1, 0x7f0701da

    const/4 v2, -0x1

    invoke-static {p1, v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(Ljava/lang/String;II)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "BillingProfileFragment.errorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showLoading()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v1, 0x0

    const/16 v3, 0xd5

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfileView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProgressIndicator:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private topup(Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;)V
    .locals 4
    .param p1    # Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;

    new-instance v2, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;

    invoke-direct {v2}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;-><init>()V

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->setInstrumentFamily(I)Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->setTopupInfoDeprecated(Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;)Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->createIntent(Ljava/lang/String;Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const-string v0, "Not using tree impressions."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v4, 0x0

    const/4 v0, -0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    packed-switch p1, :pswitch_data_0

    :goto_0
    packed-switch p1, :pswitch_data_1

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    :goto_1
    return-void

    :pswitch_0
    const/16 v1, 0x140

    goto :goto_0

    :pswitch_1
    const/16 v1, 0x141

    goto :goto_0

    :pswitch_2
    const/16 v1, 0x142

    goto :goto_0

    :pswitch_3
    const/16 v1, 0x143

    goto :goto_0

    :pswitch_4
    if-ne p2, v0, :cond_0

    const-string v0, "instrument_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/play/analytics/PlayStore$AppData;)V

    const-string v0, "instrument_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->notifyListenerOnInstrumentSelected(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_5
    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/play/analytics/PlayStore$AppData;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mStoredValueInstrumentId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mStoredValueInstrumentId:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->notifyListenerOnInstrumentSelected(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    new-instance v2, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$8;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$8;-><init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, -0x1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BillingProfileFragment.account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BillingProfileFragment.purchaseContextToken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mPurchaseContextToken:Ljava/lang/String;

    if-eqz p1, :cond_1

    const-string v0, "BillingProfileFragment.profile"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BillingProfileFragment.profile"

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfile:Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfileDirty:Z

    :cond_0
    const-string v0, "BillingProfileFragment.lastBillingProfileStateInstance"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mLastBillingProfileStateInstance:I

    const-string v0, "BillingProfileFragment.lastCarrierBillingStateInstance"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mLastCarrierBillingStateInstance:I

    const-string v0, "BillingProfileFragment.storedValueInstrumentId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mStoredValueInstrumentId:Ljava/lang/String;

    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    :cond_2
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v1, 0x7f04001a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f080063

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mExistingInstrumentsView:Landroid/view/ViewGroup;

    const v1, 0x7f080066

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    const v1, 0x7f080067

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProgressIndicator:Landroid/view/View;

    const v1, 0x7f080062

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfileView:Landroid/view/View;

    const v1, 0x7f080064

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsHeader:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsHeader:Landroid/widget/TextView;

    const v2, 0x7f07008e

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f080065

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mActionsHeaderSeparator:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->renderProfile()V

    return-object v0
.end method

.method public onPause()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$1;-><init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->initializeStorageAndParams(Landroid/content/Context;ZLjava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mCarrierBillingProvisioningListener:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$CarrierBillingProvisioningListener;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfile:Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;

    if-eqz v0, :cond_0

    const-string v0, "BillingProfileFragment.profile"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProfile:Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfile;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    const-string v0, "BillingProfileFragment.lastBillingProfileStateInstance"

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mLastBillingProfileStateInstance:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "BillingProfileFragment.lastCarrierBillingStateInstance"

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mLastCarrierBillingStateInstance:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "BillingProfileFragment.storedValueInstrumentId"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mStoredValueInstrumentId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onStart()V
    .locals 3

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "BillingProfileFragment.billingProfileSidecar"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mPurchaseContextToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->newInstance(Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    const-string v2, "BillingProfileFragment.billingProfileSidecar"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "BillingProfileFragment.carrierBillingSidecar"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mDcb2ProvisioningSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/Dcb2ProvisioningSidecar;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "BillingProfileFragment.carrierBillingProgressFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/ProgressDialogFragment;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mProgressFragment:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    return-void
.end method
