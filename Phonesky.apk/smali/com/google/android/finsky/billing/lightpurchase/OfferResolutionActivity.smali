.class public Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "OfferResolutionActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;
.implements Lcom/google/android/finsky/layout/play/RootUiElementNode;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$1;,
        Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;
    }
.end annotation


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

.field private mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field private mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mOfferFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

.field private final mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    new-instance v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-direct {v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;-><init>()V

    const/16 v1, 0x30c

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method private applyOfferFilter(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;",
            ">;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mOfferFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    if-nez v2, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mOfferFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/remoting/protos/Common$Offer;

    invoke-virtual {v3}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getOfferType()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->matches(I)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0
.end method

.method public static createIntent(Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/DocUtils$OfferFilter;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "OfferResolutionActivity.dfeToc"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "OfferResolutionActivity.account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "OfferResolutionActivity.docid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OfferResolutionActivity.doc"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    if-eqz p4, :cond_0

    const-string v1, "OfferResolutionActivity.offerFilter"

    invoke-virtual {p4}, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method public static extractAvailableOffer(Landroid/content/Intent;)Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;
    .locals 6
    .param p0    # Landroid/content/Intent;

    const-string v0, "OfferResolutionActivity.docid"

    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/remoting/protos/Common$Docid;

    const-string v0, "OfferResolutionActivity.docidStr"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "OfferResolutionActivity.offer"

    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/remoting/protos/Common$Offer;

    const-string v0, "OfferResolutionActivity.serverLogsCookie"

    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getByteStringMicroFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;-><init>(Lcom/google/android/finsky/remoting/protos/Common$Docid;Ljava/lang/String;Lcom/google/android/finsky/remoting/protos/Common$Offer;Lcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$1;)V

    return-object v0
.end method

.method private getAvailableOffers()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v12

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v10

    const/4 v0, 0x6

    if-eq v10, v0, :cond_0

    const/16 v0, 0x13

    if-eq v10, v0, :cond_0

    const/16 v0, 0x14

    if-ne v10, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/remoting/protos/Common$Offer;

    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getFullDocid()Lcom/google/android/finsky/remoting/protos/Common$Docid;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;-><init>(Lcom/google/android/finsky/remoting/protos/Common$Docid;Ljava/lang/String;Lcom/google/android/finsky/remoting/protos/Common$Offer;Lcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$1;)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->hasSubscriptions()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/DocUtils;->getSubscriptions(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getSubscriptionsList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/finsky/api/model/Document;

    const/4 v0, 0x1

    invoke-virtual {v13, v0}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    move-result-object v7

    if-nez v7, :cond_2

    const-string v0, "Skipping subscription doc, no PURCHASE offer: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v13}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;

    invoke-virtual {v13}, Lcom/google/android/finsky/api/model/Document;->getFullDocid()Lcom/google/android/finsky/remoting/protos/Common$Docid;

    move-result-object v5

    invoke-virtual {v13}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v13}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct/range {v4 .. v9}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;-><init>(Lcom/google/android/finsky/remoting/protos/Common$Docid;Ljava/lang/String;Lcom/google/android/finsky/remoting/protos/Common$Offer;Lcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$1;)V

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    return-object v12
.end method

.method private hideLoading()V
    .locals 2

    const v0, 0x7f080159

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080067

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private showError(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const v1, 0x7f0701da

    const/4 v2, -0x1

    invoke-static {p1, v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(Ljava/lang/String;II)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "OfferResolutionActivity.errorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showLoading()V
    .locals 4

    const v0, 0x7f080159

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080067

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v1, 0x0

    const/16 v3, 0xd5

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    return-void
.end method

.method private updateFromDoc()V
    .locals 17

    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->hideLoading()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v13}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v11

    if-eqz v11, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {v13, v11}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v14, 0x0

    const/16 v16, 0x30d

    invoke-virtual/range {v13 .. v17}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    const v13, 0x7f080168

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->getAvailableOffers()Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->applyOfferFilter(Ljava/util/List;)V

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_2

    const v13, 0x7f070067

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->showError(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v13}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v9, :cond_1

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;

    const v13, 0x7f04008a

    const/4 v14, 0x0

    invoke-virtual {v8, v13, v5, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    const v13, 0x7f080069

    invoke-virtual {v6, v13}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    const v13, 0x7f080133

    invoke-virtual {v6, v13}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    const v13, 0x7f08006a

    invoke-virtual {v6, v13}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v13, v1, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/remoting/protos/Common$Offer;

    invoke-virtual {v13}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getFormattedName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v13, v1, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/remoting/protos/Common$Offer;

    invoke-virtual {v13}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getFormattedAmount()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getSecondaryColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v13

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v13, v1, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/remoting/protos/Common$Offer;

    invoke-virtual {v13}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedDescription()Z

    move-result v13

    if-eqz v13, :cond_4

    iget-object v13, v1, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/remoting/protos/Common$Offer;

    invoke-virtual {v13}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getFormattedDescription()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v13, v9, -0x1

    if-ge v7, v13, :cond_3

    const v13, 0x7f04008c

    const/4 v14, 0x0

    invoke-virtual {v8, v13, v5, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v5, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_4
    const/16 v13, 0x8

    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const-string v0, "Not using tree impressions."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public finish()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v1, 0x0

    const/16 v3, 0x25b

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "OfferResolutionActivity.docid"

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->docid:Lcom/google/android/finsky/remoting/protos/Common$Docid;

    invoke-static {v3}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "OfferResolutionActivity.docidStr"

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->docidStr:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "OfferResolutionActivity.offer"

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/remoting/protos/Common$Offer;

    invoke-static {v3}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "OfferResolutionActivity.serverLogsCookie"

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->serverLogsCookie:Lcom/google/protobuf/micro/ByteStringMicro;

    invoke-static {v1, v2, v3}, Lcom/google/android/finsky/utils/ParcelableProto;->putByteStringMicroIntoIntent(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/micro/ByteStringMicro;)V

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v3, 0x30e

    iget-object v4, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->serverLogsCookie:Lcom/google/protobuf/micro/ByteStringMicro;

    invoke-virtual {v2, v3, v4, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f040089

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "OfferResolutionActivity.dfeToc"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/DfeToc;

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    const-string v2, "OfferResolutionActivity.account"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/accounts/Account;

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mAccount:Landroid/accounts/Account;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-string v2, "OfferResolutionActivity.docid"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "OfferResolutionActivity.doc"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    const-string v2, "OfferResolutionActivity.offerFilter"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "OfferResolutionActivity.offerFilter"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->valueOf(Ljava/lang/String;)Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mOfferFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    :cond_0
    if-nez p1, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v3, 0x0

    invoke-virtual {v2, v3, v4, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-nez v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->showLoading()V

    new-instance v2, Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/finsky/api/DfeUtils;->createDetailsUrlFromId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/finsky/api/model/DfeDetails;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v2, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v2, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->updateFromDoc()V

    goto :goto_0
.end method

.method public onDataChanged()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeDetails;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->updateFromDoc()V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f070067

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->showError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 1
    .param p1    # Lcom/android/volley/VolleyError;

    invoke-static {p0, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->showError(Ljava/lang/String;)V

    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->finish()V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    return-void
.end method

.method public startNewImpression()V
    .locals 2

    const-string v0, "Not using impression id\'s."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
