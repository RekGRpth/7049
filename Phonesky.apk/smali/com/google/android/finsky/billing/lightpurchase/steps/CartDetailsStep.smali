.class public Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;
.super Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;
.source "CartDetailsStep.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# instance fields
.field private mBackend:I

.field private mCart:Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;

.field private mContinueButtonConfirmsPurchase:Z

.field private mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mExpanded:Z

.field private mHeader:Landroid/view/View;

.field private mIsExpandable:Z

.field private mPaymentSettingsView:Landroid/widget/TextView;

.field private mPriceView:Landroid/widget/TextView;

.field private mPurchaseDetailsView:Landroid/view/View;

.field private final mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;-><init>()V

    new-instance v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-direct {v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;-><init>()V

    const/16 v1, 0x2c6

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method public static newInstance(ILcom/google/android/finsky/remoting/protos/Purchase$ClientCart;)Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;
    .locals 4
    .param p0    # I
    .param p1    # Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;-><init>()V

    const-string v2, "CartDetailsStep.backend"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "CartDetailsStep.cart"

    invoke-static {p1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->setArguments(Landroid/os/Bundle;)V

    iput-object p1, v1, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;

    return-object v1
.end method

.method private toggleExpansion()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v1, 0x0

    const/16 v3, 0x2cb

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->updateExpansion()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateExpansion()V
    .locals 4

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v1

    iget-boolean v3, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->expand(Z)V

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPurchaseDetailsView:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mIsExpandable:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mBackend:I

    invoke-static {v1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getMenuExpanderMaximized(I)I

    move-result v0

    :goto_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPriceView:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v2, v0, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :cond_0
    return-void

    :cond_1
    const/16 v1, 0x8

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mBackend:I

    invoke-static {v1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getMenuExpanderMinimized(I)I

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;->getButtonText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mHeader:Landroid/view/View;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x2c9

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->toggleExpansion()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPaymentSettingsView:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x2ca

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->launchBillingProfile()V

    goto :goto_0
.end method

.method public onContinueButtonClicked(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;)V
    .locals 3
    .param p1    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mContinueButtonConfirmsPurchase:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x2c7

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->confirmPurchase()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x2c8

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->launchBillingProfile()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CartDetailsStep.backend"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mBackend:I

    const-string v1, "CartDetailsStep.cart"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;

    if-eqz p1, :cond_0

    const-string v1, "CartDetailsStep.expanded"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 20
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mBackend:I

    move/from16 v19, v0

    invoke-static/range {v18 .. v19}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getSecondaryColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v5

    const v18, 0x7f040086

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move-object/from16 v2, p2

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v15

    const v18, 0x7f08015f

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    const v18, 0x7f08015e

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPriceView:Landroid/widget/TextView;

    const v18, 0x7f080160

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    const v18, 0x7f080162

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    const v18, 0x7f080068

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v18, 0x7f080163

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPurchaseDetailsView:Landroid/view/View;

    const v18, 0x7f080166

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const v18, 0x7f080164

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPaymentSettingsView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPaymentSettingsView:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPaymentSettingsView:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    const v18, 0x7f0800ad

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mHeader:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;->getTitle()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPriceView:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;->getFormattedPrice()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPriceView:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;->hasInstrument()Z

    move-result v18

    if-eqz v18, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;->getInstrument()Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->getDisplayTitle()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v10}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->getDisabledInfoCount()I

    move-result v18

    if-lez v18, :cond_1

    const v18, 0x7f080161

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$Instrument;->getDisabledInfo(I)Lcom/google/android/finsky/remoting/protos/CommonDevice$DisabledInfo;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/remoting/protos/CommonDevice$DisabledInfo;->getDisabledMessageHtml()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;->hasFooterHtml()Z

    move-result v18

    if-eqz v18, :cond_4

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;->getFooterHtml()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v8}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mIsExpandable:Z

    move/from16 v18, v0

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mHeader:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->updateExpansion()V

    return-object v15

    :cond_1
    const v18, 0x7f080165

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;->getDetailHtmlList()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const v18, 0x7f040087

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move-object/from16 v2, v16

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v7}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_2

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mIsExpandable:Z

    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mContinueButtonConfirmsPurchase:Z

    goto/16 :goto_0

    :cond_3
    const/16 v18, 0x8

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/remoting/protos/Purchase$ClientCart;->getAddInstrumentPromptHtml()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_4
    const/16 v18, 0x8

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "CartDetailsStep.expanded"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
