.class public abstract Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;
.super Landroid/support/v4/app/Fragment;
.source "PurchaseStepFragment.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const-string v0, "Not using tree impressions."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public abstract getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v0

    return-object v0
.end method

.method protected getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    return-object v0
.end method

.method public abstract onContinueButtonClicked(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;)V
.end method
