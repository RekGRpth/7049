.class public Lcom/google/android/finsky/exploreactivity/DrawingUtils;
.super Ljava/lang/Object;
.source "DrawingUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/exploreactivity/DrawingUtils$2;,
        Lcom/google/android/finsky/exploreactivity/DrawingUtils$PoolingImageRequest;,
        Lcom/google/android/finsky/exploreactivity/DrawingUtils$TransformingImageInfo;
    }
.end annotation


# static fields
.field private static final BASE_PLANE:Lcom/jme3/math/Plane;

.field public static final UNIT_QUAD:Lcom/jme3/scene/Mesh;

.field public static final UNIT_X:Lcom/jme3/math/Vector2f;

.field private static final sDecodeLock:Ljava/lang/Object;


# instance fields
.field private final click3d:Lcom/jme3/math/Vector3f;

.field private final dir:Lcom/jme3/math/Vector3f;

.field private final intersectionPoint:Lcom/jme3/math/Vector3f;

.field private final inverseMatrix:Lcom/jme3/math/Matrix4f;

.field private mBitmapPool:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mBitmapPoolBytes:I

.field private final mEdgeLineGlowWidth:F

.field private final mEdgeLineWidth:F

.field private final mGlowBackgroundColor:I

.field private final mGlowForegroundColor:I

.field private final mGlowingLineMaterial:Lcom/jme3/material/Material;

.field private final mInvisibleTexture:Lcom/jme3/material/Material;

.field private final mLineForegroundColor:I

.field private final mMaxBitmapPoolBytes:I

.field public final mNodeBitmapSize:F

.field private final mNodeGlowMaterial:Lcom/jme3/material/Material;

.field private final mNodeGlowOutlineWidth:F

.field public final mNodeGlowWidth:F

.field private final mUnshadedMaterial:Lcom/jme3/material/Material;

.field private final quat:Lcom/jme3/math/Quaternion;

.field private final ray:Lcom/jme3/math/Ray;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000

    new-instance v0, Lcom/jme3/math/Plane;

    sget-object v1, Lcom/jme3/math/Vector3f;->UNIT_Z:Lcom/jme3/math/Vector3f;

    invoke-direct {v0, v1, v3}, Lcom/jme3/math/Plane;-><init>(Lcom/jme3/math/Vector3f;F)V

    sput-object v0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->BASE_PLANE:Lcom/jme3/math/Plane;

    new-instance v0, Lcom/jme3/math/Vector2f;

    invoke-direct {v0, v2, v3}, Lcom/jme3/math/Vector2f;-><init>(FF)V

    sput-object v0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->UNIT_X:Lcom/jme3/math/Vector2f;

    new-instance v0, Lcom/jme3/scene/shape/Quad;

    invoke-direct {v0, v2, v2}, Lcom/jme3/scene/shape/Quad;-><init>(FF)V

    sput-object v0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->UNIT_QUAD:Lcom/jme3/scene/Mesh;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->sDecodeLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/jme3/asset/AssetManager;Landroid/content/res/Resources;I)V
    .locals 5
    .param p1    # Lcom/jme3/asset/AssetManager;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPool:Ljava/util/List;

    iput v4, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPoolBytes:I

    new-instance v1, Lcom/jme3/math/Vector3f;

    invoke-direct {v1}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->click3d:Lcom/jme3/math/Vector3f;

    new-instance v1, Lcom/jme3/math/Vector3f;

    invoke-direct {v1}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->dir:Lcom/jme3/math/Vector3f;

    new-instance v1, Lcom/jme3/math/Ray;

    invoke-direct {v1}, Lcom/jme3/math/Ray;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->ray:Lcom/jme3/math/Ray;

    new-instance v1, Lcom/jme3/math/Vector3f;

    invoke-direct {v1}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->intersectionPoint:Lcom/jme3/math/Vector3f;

    new-instance v1, Lcom/jme3/math/Quaternion;

    invoke-direct {v1}, Lcom/jme3/math/Quaternion;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->quat:Lcom/jme3/math/Quaternion;

    new-instance v1, Lcom/jme3/math/Matrix4f;

    invoke-direct {v1}, Lcom/jme3/math/Matrix4f;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->inverseMatrix:Lcom/jme3/math/Matrix4f;

    const/4 v1, 0x1

    const/high16 v2, 0x3f800000

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    const/high16 v1, 0x3fc00000

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    const/high16 v0, 0x3f800000

    :cond_0
    const v1, 0x7f0c0018

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mNodeBitmapSize:F

    const v1, 0x7f0c001a

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mNodeGlowWidth:F

    const v1, 0x7f0a002d

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mGlowBackgroundColor:I

    const v1, 0x7f0a002e

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mGlowForegroundColor:I

    const v1, 0x7f0a002f

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mLineForegroundColor:I

    const v1, 0x7f0c001d

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mNodeGlowOutlineWidth:F

    const v1, 0x7f0c001b

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mEdgeLineGlowWidth:F

    const v1, 0x7f0c001c

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mEdgeLineWidth:F

    new-instance v1, Lcom/jme3/material/Material;

    const-string v2, "Common/MatDefs/Misc/Unshaded.j3md"

    invoke-direct {v1, p1, v2}, Lcom/jme3/material/Material;-><init>(Lcom/jme3/asset/AssetManager;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mUnshadedMaterial:Lcom/jme3/material/Material;

    invoke-virtual {p0}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->createLineBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->convertBitmapToMaterial(Landroid/graphics/Bitmap;Z)Lcom/jme3/material/Material;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mGlowingLineMaterial:Lcom/jme3/material/Material;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mGlowingLineMaterial:Lcom/jme3/material/Material;

    invoke-virtual {v1}, Lcom/jme3/material/Material;->getAdditionalRenderState()Lcom/jme3/material/RenderState;

    move-result-object v1

    sget-object v2, Lcom/jme3/material/RenderState$BlendMode;->AlphaAdditive:Lcom/jme3/material/RenderState$BlendMode;

    invoke-virtual {v1, v2}, Lcom/jme3/material/RenderState;->setBlendMode(Lcom/jme3/material/RenderState$BlendMode;)V

    invoke-direct {p0}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getUnshadedMaterial()Lcom/jme3/material/Material;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mInvisibleTexture:Lcom/jme3/material/Material;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mInvisibleTexture:Lcom/jme3/material/Material;

    const-string v2, "Color"

    sget-object v3, Lcom/jme3/math/ColorRGBA;->BlackNoAlpha:Lcom/jme3/math/ColorRGBA;

    invoke-virtual {v1, v2, v3}, Lcom/jme3/material/Material;->setColor(Ljava/lang/String;Lcom/jme3/math/ColorRGBA;)V

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mInvisibleTexture:Lcom/jme3/material/Material;

    invoke-virtual {v1}, Lcom/jme3/material/Material;->getAdditionalRenderState()Lcom/jme3/material/RenderState;

    move-result-object v1

    sget-object v2, Lcom/jme3/material/RenderState$BlendMode;->Alpha:Lcom/jme3/material/RenderState$BlendMode;

    invoke-virtual {v1, v2}, Lcom/jme3/material/RenderState;->setBlendMode(Lcom/jme3/material/RenderState$BlendMode;)V

    invoke-direct {p0}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->createGlowBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->convertBitmapToMaterial(Landroid/graphics/Bitmap;Z)Lcom/jme3/material/Material;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mNodeGlowMaterial:Lcom/jme3/material/Material;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mNodeGlowMaterial:Lcom/jme3/material/Material;

    invoke-virtual {v1}, Lcom/jme3/material/Material;->getAdditionalRenderState()Lcom/jme3/material/RenderState;

    move-result-object v1

    sget-object v2, Lcom/jme3/material/RenderState$BlendMode;->AlphaAdditive:Lcom/jme3/material/RenderState$BlendMode;

    invoke-virtual {v1, v2}, Lcom/jme3/material/RenderState;->setBlendMode(Lcom/jme3/material/RenderState$BlendMode;)V

    iput p3, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mMaxBitmapPoolBytes:I

    return-void
.end method

.method static synthetic access$100()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->sDecodeLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/exploreactivity/DrawingUtils;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/DrawingUtils;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private convertBlueToAlpha(Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-ge v2, v4, :cond_1

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-ge v3, v4, :cond_0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    and-int/lit16 v1, v0, 0xff

    shl-int/lit8 v4, v1, 0x18

    const v5, 0xffffff

    or-int v0, v4, v5

    invoke-virtual {p1, v2, v3, v0}, Landroid/graphics/Bitmap;->setPixel(III)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private createGlowBitmap()Landroid/graphics/Bitmap;
    .locals 9

    const/high16 v7, 0x40000000

    iget v6, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mNodeBitmapSize:F

    div-float v3, v6, v7

    iget v6, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mNodeGlowWidth:F

    add-float v4, v3, v6

    mul-float v6, v4, v7

    float-to-int v1, v6

    invoke-direct {p0, v1, v1}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    iget v6, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mGlowBackgroundColor:I

    invoke-virtual {v0, v6}, Landroid/graphics/Bitmap;->eraseColor(I)V

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget v6, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mNodeGlowOutlineWidth:F

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v6, Landroid/graphics/BlurMaskFilter;

    sub-float v7, v4, v3

    sget-object v8, Landroid/graphics/BlurMaskFilter$Blur;->OUTER:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v6, v7, v8}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    iget v6, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mGlowForegroundColor:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {v2, v4, v4, v3, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    invoke-virtual {v2, v4, v4, v3, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    invoke-direct {p0, v0}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->convertBlueToAlpha(Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method private getAngle(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)F
    .locals 4
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->dir:Lcom/jme3/math/Vector3f;

    iget v2, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v3, p1, Lcom/jme3/math/Vector3f;->x:F

    sub-float/2addr v2, v3

    iput v2, v1, Lcom/jme3/math/Vector3f;->x:F

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->dir:Lcom/jme3/math/Vector3f;

    iget v2, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v3, p1, Lcom/jme3/math/Vector3f;->y:F

    sub-float/2addr v2, v3

    iput v2, v1, Lcom/jme3/math/Vector3f;->y:F

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->dir:Lcom/jme3/math/Vector3f;

    iget v2, p2, Lcom/jme3/math/Vector3f;->z:F

    iget v3, p1, Lcom/jme3/math/Vector3f;->z:F

    sub-float/2addr v2, v3

    iput v2, v1, Lcom/jme3/math/Vector3f;->z:F

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->dir:Lcom/jme3/math/Vector3f;

    invoke-virtual {v1}, Lcom/jme3/math/Vector3f;->normalizeLocal()Lcom/jme3/math/Vector3f;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->dir:Lcom/jme3/math/Vector3f;

    sget-object v2, Lcom/jme3/math/Vector3f;->UNIT_X:Lcom/jme3/math/Vector3f;

    invoke-virtual {v1, v2}, Lcom/jme3/math/Vector3f;->angleBetween(Lcom/jme3/math/Vector3f;)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->dir:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->y:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    neg-float v0, v0

    :cond_0
    return v0
.end method

.method private getBitmap(II)Landroid/graphics/Bitmap;
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPool:Ljava/util/List;

    monitor-enter v5

    const/4 v3, 0x0

    :goto_0
    :try_start_0
    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPool:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPool:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->removeBitmapFromPool(I)Landroid/graphics/Bitmap;

    const-string v4, "recycled"

    invoke-direct {p0, v1, v4}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->poolLog(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    add-int/lit8 v3, v3, -0x1

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-ne v4, p1, :cond_2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-ne v4, p2, :cond_2

    invoke-direct {p0, v3}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->removeBitmapFromPool(I)Landroid/graphics/Bitmap;

    const-string v4, "hit"

    invoke-direct {p0, v1, v4}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->poolLog(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    :cond_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_3

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    move-object v2, v1

    :goto_2
    return-object v2

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    :cond_3
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    const-string v4, "miss"

    invoke-direct {p0, v1, v4}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->poolLog(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    move-object v2, v1

    goto :goto_2
.end method

.method public static getPolyCenter([Lcom/jme3/math/Vector2f;Lcom/jme3/math/Vector2f;)V
    .locals 9
    .param p0    # [Lcom/jme3/math/Vector2f;
    .param p1    # Lcom/jme3/math/Vector2f;

    const/high16 v8, 0x40000000

    const/high16 v4, 0x7f800000

    const/high16 v2, -0x800000

    const/high16 v5, 0x7f800000

    const/high16 v3, -0x800000

    const/4 v1, 0x0

    :goto_0
    array-length v7, p0

    if-ge v1, v7, :cond_0

    aget-object v7, p0, v1

    iget v7, v7, Lcom/jme3/math/Vector2f;->x:F

    invoke-static {v4, v7}, Ljava/lang/Math;->min(FF)F

    move-result v4

    aget-object v7, p0, v1

    iget v7, v7, Lcom/jme3/math/Vector2f;->x:F

    invoke-static {v2, v7}, Ljava/lang/Math;->max(FF)F

    move-result v2

    aget-object v7, p0, v1

    iget v7, v7, Lcom/jme3/math/Vector2f;->y:F

    invoke-static {v5, v7}, Ljava/lang/Math;->min(FF)F

    move-result v5

    aget-object v7, p0, v1

    iget v7, v7, Lcom/jme3/math/Vector2f;->y:F

    invoke-static {v3, v7}, Ljava/lang/Math;->max(FF)F

    move-result v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    sub-float v6, v2, v4

    sub-float v0, v3, v5

    div-float v7, v6, v8

    add-float/2addr v7, v4

    iput v7, p1, Lcom/jme3/math/Vector2f;->x:F

    div-float v7, v0, v8

    add-float/2addr v7, v5

    iput v7, p1, Lcom/jme3/math/Vector2f;->y:F

    return-void
.end method

.method public static getPolySize([Lcom/jme3/math/Vector2f;Lcom/jme3/math/Vector2f;)V
    .locals 6
    .param p0    # [Lcom/jme3/math/Vector2f;
    .param p1    # Lcom/jme3/math/Vector2f;

    const v3, 0x7f7fffff

    const/4 v1, 0x1

    const v4, 0x7f7fffff

    const/4 v2, 0x1

    const/4 v0, 0x0

    :goto_0
    array-length v5, p0

    if-ge v0, v5, :cond_0

    aget-object v5, p0, v0

    iget v5, v5, Lcom/jme3/math/Vector2f;->x:F

    invoke-static {v3, v5}, Ljava/lang/Math;->min(FF)F

    move-result v3

    aget-object v5, p0, v0

    iget v5, v5, Lcom/jme3/math/Vector2f;->x:F

    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    aget-object v5, p0, v0

    iget v5, v5, Lcom/jme3/math/Vector2f;->y:F

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    aget-object v5, p0, v0

    iget v5, v5, Lcom/jme3/math/Vector2f;->y:F

    invoke-static {v2, v5}, Ljava/lang/Math;->max(FF)F

    move-result v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sub-float v5, v1, v3

    iput v5, p1, Lcom/jme3/math/Vector2f;->x:F

    sub-float v5, v2, v4

    iput v5, p1, Lcom/jme3/math/Vector2f;->y:F

    return-void
.end method

.method private getUnshadedMaterial()Lcom/jme3/material/Material;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mUnshadedMaterial:Lcom/jme3/material/Material;

    invoke-virtual {v0}, Lcom/jme3/material/Material;->clone()Lcom/jme3/material/Material;

    move-result-object v0

    return-object v0
.end method

.method private getWorldCoordinates(Lcom/jme3/renderer/Camera;Lcom/jme3/math/Vector2f;FLcom/jme3/math/Vector3f;)V
    .locals 7
    .param p1    # Lcom/jme3/renderer/Camera;
    .param p2    # Lcom/jme3/math/Vector2f;
    .param p3    # F
    .param p4    # Lcom/jme3/math/Vector3f;

    const/high16 v6, 0x40000000

    const/high16 v5, 0x3f800000

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->inverseMatrix:Lcom/jme3/math/Matrix4f;

    invoke-virtual {p1}, Lcom/jme3/renderer/Camera;->getViewProjectionMatrix()Lcom/jme3/math/Matrix4f;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jme3/math/Matrix4f;->set(Lcom/jme3/math/Matrix4f;)Lcom/jme3/math/Matrix4f;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->inverseMatrix:Lcom/jme3/math/Matrix4f;

    invoke-virtual {v1}, Lcom/jme3/math/Matrix4f;->invertLocal()Lcom/jme3/math/Matrix4f;

    iget v1, p2, Lcom/jme3/math/Vector2f;->x:F

    invoke-virtual {p1}, Lcom/jme3/renderer/Camera;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p1}, Lcom/jme3/renderer/Camera;->getViewPortLeft()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-virtual {p1}, Lcom/jme3/renderer/Camera;->getViewPortRight()F

    move-result v2

    invoke-virtual {p1}, Lcom/jme3/renderer/Camera;->getViewPortLeft()F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v1, v2

    mul-float/2addr v1, v6

    sub-float/2addr v1, v5

    iget v2, p2, Lcom/jme3/math/Vector2f;->y:F

    invoke-virtual {p1}, Lcom/jme3/renderer/Camera;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {p1}, Lcom/jme3/renderer/Camera;->getViewPortBottom()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-virtual {p1}, Lcom/jme3/renderer/Camera;->getViewPortTop()F

    move-result v3

    invoke-virtual {p1}, Lcom/jme3/renderer/Camera;->getViewPortBottom()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v2, v3

    mul-float/2addr v2, v6

    sub-float/2addr v2, v5

    mul-float v3, p3, v6

    sub-float/2addr v3, v5

    invoke-virtual {p4, v1, v2, v3}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->inverseMatrix:Lcom/jme3/math/Matrix4f;

    invoke-virtual {v1, p4, p4}, Lcom/jme3/math/Matrix4f;->multProj(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)F

    move-result v0

    div-float v1, v5, v0

    invoke-virtual {p4, v1}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    return-void
.end method

.method private poolLog(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 11
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Ljava/lang/String;

    sget-boolean v6, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-nez v6, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPool:Ljava/util/List;

    monitor-enter v7

    :try_start_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPool:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const-string v6, "%dx%d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    :cond_1
    const/4 v6, 0x1

    :try_start_1
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ":"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    const-string v6, "Pool %10s: %3dx%3d, now %2s/%8s: %s"

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p2, v8, v9

    const/4 v9, 0x1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    iget-object v10, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPool:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x4

    iget v10, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPoolBytes:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x5

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v8}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method private removeBitmapFromPool(I)Landroid/graphics/Bitmap;
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPool:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPoolBytes:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v3

    mul-int/2addr v2, v3

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPoolBytes:I

    return-object v0
.end method


# virtual methods
.method convertBitmapToMaterial(Landroid/graphics/Bitmap;Z)Lcom/jme3/material/Material;
    .locals 7
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Z

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v5, Lcom/google/android/finsky/exploreactivity/DrawingUtils$2;->$SwitchMap$android$graphics$Bitmap$Config:[I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Bitmap$Config;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    :goto_0
    return-object v3

    :pswitch_0
    sget-object v0, Lcom/jme3/texture/Image$Format;->Alpha8:Lcom/jme3/texture/Image$Format;

    :goto_1
    new-instance v2, Lcom/jme3/texture/Image;

    invoke-direct {v2, v0, v4, v1, v3}, Lcom/jme3/texture/Image;-><init>(Lcom/jme3/texture/Image$Format;IILjava/nio/ByteBuffer;)V

    new-instance v5, Lcom/google/android/finsky/exploreactivity/DrawingUtils$TransformingImageInfo;

    invoke-direct {v5, p0, p1, p2, v3}, Lcom/google/android/finsky/exploreactivity/DrawingUtils$TransformingImageInfo;-><init>(Lcom/google/android/finsky/exploreactivity/DrawingUtils;Landroid/graphics/Bitmap;ZLcom/google/android/finsky/exploreactivity/DrawingUtils$1;)V

    invoke-virtual {v2, v5}, Lcom/jme3/texture/Image;->setEfficentData(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getUnshadedMaterial()Lcom/jme3/material/Material;

    move-result-object v3

    const-string v5, "ColorMap"

    new-instance v6, Lcom/jme3/texture/Texture2D;

    invoke-direct {v6, v2}, Lcom/jme3/texture/Texture2D;-><init>(Lcom/jme3/texture/Image;)V

    invoke-virtual {v3, v5, v6}, Lcom/jme3/material/Material;->setTexture(Ljava/lang/String;Lcom/jme3/texture/Texture;)V

    invoke-virtual {v3}, Lcom/jme3/material/Material;->getAdditionalRenderState()Lcom/jme3/material/RenderState;

    move-result-object v5

    sget-object v6, Lcom/jme3/material/RenderState$BlendMode;->Alpha:Lcom/jme3/material/RenderState$BlendMode;

    invoke-virtual {v5, v6}, Lcom/jme3/material/RenderState;->setBlendMode(Lcom/jme3/material/RenderState$BlendMode;)V

    invoke-virtual {v3}, Lcom/jme3/material/Material;->getAdditionalRenderState()Lcom/jme3/material/RenderState;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/jme3/material/RenderState;->setAlphaTest(Z)V

    invoke-virtual {v3}, Lcom/jme3/material/Material;->getAdditionalRenderState()Lcom/jme3/material/RenderState;

    move-result-object v5

    const v6, 0x3dcccccd

    invoke-virtual {v5, v6}, Lcom/jme3/material/RenderState;->setAlphaFallOff(F)V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/jme3/texture/Image$Format;->ARGB4444:Lcom/jme3/texture/Image$Format;

    goto :goto_1

    :pswitch_2
    sget-object v0, Lcom/jme3/texture/Image$Format;->RGBA8:Lcom/jme3/texture/Image$Format;

    goto :goto_1

    :pswitch_3
    sget-object v0, Lcom/jme3/texture/Image$Format;->RGB565:Lcom/jme3/texture/Image$Format;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method createLineBitmap()Landroid/graphics/Bitmap;
    .locals 9

    const/4 v1, 0x0

    const/high16 v8, 0x3f000000

    iget v2, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mEdgeLineGlowWidth:F

    const/high16 v3, 0x40000000

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mEdgeLineWidth:F

    add-float/2addr v2, v3

    float-to-int v7, v2

    invoke-direct {p0, v7, v7}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v6

    iget v2, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mGlowBackgroundColor:I

    invoke-virtual {v6, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v2, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget v2, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mEdgeLineWidth:F

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v2, Landroid/graphics/BlurMaskFilter;

    iget v3, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mEdgeLineGlowWidth:F

    sget-object v4, Landroid/graphics/BlurMaskFilter$Blur;->OUTER:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v2, v3, v4}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    iget v2, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mGlowForegroundColor:I

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget v2, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mEdgeLineGlowWidth:F

    sub-float/2addr v2, v8

    int-to-float v3, v7

    iget v4, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mEdgeLineGlowWidth:F

    add-float/2addr v4, v8

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    iget v2, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mLineForegroundColor:I

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget v2, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mEdgeLineGlowWidth:F

    sub-float/2addr v2, v8

    int-to-float v3, v7

    iget v4, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mEdgeLineGlowWidth:F

    add-float/2addr v4, v8

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    invoke-direct {p0, v6}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->convertBlueToAlpha(Landroid/graphics/Bitmap;)V

    return-object v6
.end method

.method public createNodeBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 14
    .param p1    # Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-static {v13, v8}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    const/high16 v0, 0x3f800000

    const/high16 v1, -0x40800000

    invoke-virtual {v6, v0, v1}, Landroid/graphics/Matrix;->preScale(FF)Z

    iget v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mNodeBitmapSize:F

    int-to-float v1, v4

    div-float v12, v0, v1

    invoke-virtual {v6, v12, v12}, Landroid/graphics/Matrix;->postScale(FF)Z

    sub-int v0, v13, v4

    div-int/lit8 v2, v0, 0x2

    sub-int v0, v8, v4

    div-int/lit8 v3, v0, 0x2

    move-object v0, p0

    move-object v1, p1

    move v5, v4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->transformBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;)Landroid/graphics/Bitmap;

    move-result-object p1

    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    int-to-float v0, v4

    const/high16 v1, 0x40000000

    invoke-static {v1}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x40000000

    div-float v10, v0, v1

    int-to-float v0, v4

    const/high16 v1, 0x40000000

    div-float v11, v0, v1

    sub-float v0, v10, v11

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    int-to-float v0, v4

    const/high16 v1, 0x40000000

    div-float/2addr v0, v1

    int-to-float v1, v4

    const/high16 v2, 0x40000000

    div-float/2addr v1, v2

    add-float v2, v10, v11

    const/high16 v3, 0x40000000

    div-float/2addr v2, v3

    invoke-virtual {v7, v0, v1, v2, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-object p1
.end method

.method public createViewBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1}, Landroid/graphics/Canvas;-><init>()V

    invoke-virtual {v1, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    return-object v0
.end method

.method public getBasePlaneCoords(Lcom/jme3/renderer/Camera;Lcom/jme3/math/Vector2f;Lcom/jme3/math/Vector2f;)V
    .locals 3
    .param p1    # Lcom/jme3/renderer/Camera;
    .param p2    # Lcom/jme3/math/Vector2f;
    .param p3    # Lcom/jme3/math/Vector2f;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->click3d:Lcom/jme3/math/Vector3f;

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getWorldCoordinates(Lcom/jme3/renderer/Camera;Lcom/jme3/math/Vector2f;FLcom/jme3/math/Vector3f;)V

    const/high16 v0, 0x3f800000

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->dir:Lcom/jme3/math/Vector3f;

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getWorldCoordinates(Lcom/jme3/renderer/Camera;Lcom/jme3/math/Vector2f;FLcom/jme3/math/Vector3f;)V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->dir:Lcom/jme3/math/Vector3f;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->dir:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->x:F

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->click3d:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->x:F

    sub-float/2addr v1, v2

    iput v1, v0, Lcom/jme3/math/Vector3f;->x:F

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->dir:Lcom/jme3/math/Vector3f;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->dir:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->y:F

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->click3d:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->y:F

    sub-float/2addr v1, v2

    iput v1, v0, Lcom/jme3/math/Vector3f;->y:F

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->dir:Lcom/jme3/math/Vector3f;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->dir:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->z:F

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->click3d:Lcom/jme3/math/Vector3f;

    iget v2, v2, Lcom/jme3/math/Vector3f;->z:F

    sub-float/2addr v1, v2

    iput v1, v0, Lcom/jme3/math/Vector3f;->z:F

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->ray:Lcom/jme3/math/Ray;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->click3d:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, v1}, Lcom/jme3/math/Ray;->setOrigin(Lcom/jme3/math/Vector3f;)V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->ray:Lcom/jme3/math/Ray;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->dir:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, v1}, Lcom/jme3/math/Ray;->setDirection(Lcom/jme3/math/Vector3f;)V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->ray:Lcom/jme3/math/Ray;

    sget-object v1, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->BASE_PLANE:Lcom/jme3/math/Plane;

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->intersectionPoint:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, v1, v2}, Lcom/jme3/math/Ray;->intersectsWherePlane(Lcom/jme3/math/Plane;Lcom/jme3/math/Vector3f;)Z

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->intersectionPoint:Lcom/jme3/math/Vector3f;

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    iput v0, p3, Lcom/jme3/math/Vector2f;->x:F

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->intersectionPoint:Lcom/jme3/math/Vector3f;

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    iput v0, p3, Lcom/jme3/math/Vector2f;->y:F

    return-void
.end method

.method public getGlowingLineMaterial()Lcom/jme3/material/Material;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mGlowingLineMaterial:Lcom/jme3/material/Material;

    invoke-virtual {v0}, Lcom/jme3/material/Material;->clone()Lcom/jme3/material/Material;

    move-result-object v0

    return-object v0
.end method

.method public getInvisibleMaterial()Lcom/jme3/material/Material;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mInvisibleTexture:Lcom/jme3/material/Material;

    return-object v0
.end method

.method public getNodeGlowMaterial()Lcom/jme3/material/Material;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mNodeGlowMaterial:Lcom/jme3/material/Material;

    invoke-virtual {v0}, Lcom/jme3/material/Material;->clone()Lcom/jme3/material/Material;

    move-result-object v0

    return-object v0
.end method

.method public getThumbnailRequest(Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/android/volley/Request$Priority;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;
    .locals 6
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocWrapper;
    .param p2    # Lcom/android/volley/Request$Priority;
    .param p3    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/exploreactivity/DocWrapper;",
            "Lcom/android/volley/Request$Priority;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/android/volley/Request",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mNodeBitmapSize:F

    float-to-int v1, v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ThumbnailUtils;->getCroppedIconUrlFromDocument(Lcom/google/android/finsky/api/model/Document;I)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/google/android/finsky/exploreactivity/DrawingUtils$PoolingImageRequest;

    new-instance v4, Lcom/google/android/finsky/exploreactivity/DrawingUtils$1;

    invoke-direct {v4, p0, p1}, Lcom/google/android/finsky/exploreactivity/DrawingUtils$1;-><init>(Lcom/google/android/finsky/exploreactivity/DrawingUtils;Lcom/google/android/finsky/exploreactivity/DocWrapper;)V

    move-object v1, p0

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/exploreactivity/DrawingUtils$PoolingImageRequest;-><init>(Lcom/google/android/finsky/exploreactivity/DrawingUtils;Ljava/lang/String;Lcom/android/volley/Request$Priority;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    return-object v0
.end method

.method public hasBitmapTexture(Lcom/jme3/scene/Geometry;)Z
    .locals 2
    .param p1    # Lcom/jme3/scene/Geometry;

    invoke-virtual {p1}, Lcom/jme3/scene/Geometry;->getMaterial()Lcom/jme3/material/Material;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/jme3/scene/Geometry;->getMaterial()Lcom/jme3/material/Material;

    move-result-object v0

    const-string v1, "ColorMap"

    invoke-virtual {v0, v1}, Lcom/jme3/material/Material;->getParam(Ljava/lang/String;)Lcom/jme3/material/MatParam;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public recycleBitmap(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1    # Landroid/graphics/Bitmap;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPool:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPool:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPoolBytes:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v3

    mul-int/2addr v2, v3

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPoolBytes:I

    const-string v0, "return"

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->poolLog(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    :goto_0
    iget v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPoolBytes:I

    iget v2, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mMaxBitmapPoolBytes:I

    if-le v0, v2, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->removeBitmapFromPool(I)Landroid/graphics/Bitmap;

    move-result-object p1

    const-string v0, "overrun"

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->poolLog(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    return-void
.end method

.method public recycleMaterial(Lcom/jme3/scene/Geometry;)V
    .locals 7
    .param p1    # Lcom/jme3/scene/Geometry;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/jme3/scene/Geometry;->getMaterial()Lcom/jme3/material/Material;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mInvisibleTexture:Lcom/jme3/material/Material;

    invoke-virtual {p1, v6}, Lcom/jme3/scene/Geometry;->setMaterial(Lcom/jme3/material/Material;)V

    if-eqz v2, :cond_0

    const-string v6, "ColorMap"

    invoke-virtual {v2, v6}, Lcom/jme3/material/Material;->getParam(Ljava/lang/String;)Lcom/jme3/material/MatParam;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/jme3/material/MatParam;->getValue()Ljava/lang/Object;

    move-result-object v4

    instance-of v6, v4, Lcom/jme3/texture/Texture;

    if-eqz v6, :cond_0

    check-cast v4, Lcom/jme3/texture/Texture;

    invoke-virtual {v4}, Lcom/jme3/texture/Texture;->getImage()Lcom/jme3/texture/Image;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/jme3/texture/Image;->getEfficentData()Ljava/lang/Object;

    move-result-object v5

    instance-of v6, v5, Lcom/jme3/asset/AndroidImageInfo;

    if-eqz v6, :cond_0

    check-cast v5, Lcom/jme3/asset/AndroidImageInfo;

    invoke-virtual {v5}, Lcom/jme3/asset/AndroidImageInfo;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->recycleBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setupLine(Lcom/jme3/scene/Spatial;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V
    .locals 4
    .param p1    # Lcom/jme3/scene/Spatial;
    .param p2    # Lcom/jme3/math/Vector3f;
    .param p3    # Lcom/jme3/math/Vector3f;

    const/high16 v3, 0x3f800000

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->quat:Lcom/jme3/math/Quaternion;

    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getAngle(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)F

    move-result v1

    invoke-virtual {v0, v2, v2, v1}, Lcom/jme3/math/Quaternion;->fromAngles(FFF)Lcom/jme3/math/Quaternion;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->quat:Lcom/jme3/math/Quaternion;

    invoke-virtual {p1, v0}, Lcom/jme3/scene/Spatial;->setLocalRotation(Lcom/jme3/math/Quaternion;)V

    iget v0, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p2, Lcom/jme3/math/Vector3f;->z:F

    invoke-virtual {p1, v0, v1, v2}, Lcom/jme3/scene/Spatial;->setLocalTranslation(FFF)V

    invoke-virtual {p2, p3}, Lcom/jme3/math/Vector3f;->distance(Lcom/jme3/math/Vector3f;)F

    move-result v0

    invoke-virtual {p1, v0, v3, v3}, Lcom/jme3/scene/Spatial;->setLocalScale(FFF)V

    return-void
.end method

.method transformBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;)Landroid/graphics/Bitmap;
    .locals 13
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Landroid/graphics/Matrix;

    move/from16 v6, p4

    move/from16 v5, p5

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2}, Landroid/graphics/Canvas;-><init>()V

    new-instance v8, Landroid/graphics/Rect;

    add-int v9, p2, p4

    add-int v10, p3, p5

    move/from16 v0, p3

    invoke-direct {v8, p2, v0, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v4, Landroid/graphics/RectF;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move/from16 v0, p4

    int-to-float v11, v0

    move/from16 v0, p5

    int-to-float v12, v0

    invoke-direct {v4, v9, v10, v11, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v0, p6

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-direct {p0, v6, v5}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v1

    iget v9, v3, Landroid/graphics/RectF;->left:F

    neg-float v9, v9

    iget v10, v3, Landroid/graphics/RectF;->top:F

    neg-float v10, v10

    invoke-virtual {v2, v9, v10}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getDensity()I

    move-result v9

    invoke-virtual {v1, v9}, Landroid/graphics/Bitmap;->setDensity(I)V

    invoke-virtual {v2, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, p1, v8, v4, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    return-object v1
.end method

.method public unloadCache()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPool:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPool:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->mBitmapPoolBytes:I

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
