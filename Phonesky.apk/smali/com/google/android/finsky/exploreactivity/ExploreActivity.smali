.class public Lcom/google/android/finsky/exploreactivity/ExploreActivity;
.super Lcom/jme3/app/AndroidHarness;
.source "ExploreActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/exploreactivity/ExploreActivity$2;
    }
.end annotation


# instance fields
.field private mMusicPreviewManager:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;

.field private mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

.field private mParentLayout:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/jme3/app/AndroidHarness;-><init>()V

    sget-object v0, Lcom/jme3/system/android/AndroidConfigChooser$ConfigType;->BEST:Lcom/jme3/system/android/AndroidConfigChooser$ConfigType;

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->eglConfigType:Lcom/jme3/system/android/AndroidConfigChooser$ConfigType;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->eglConfigVerboseLogging:Z

    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->screenOrientation:I

    iput-boolean v1, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->mouseEventsInvertX:Z

    iput-boolean v1, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->mouseEventsInvertY:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/exploreactivity/ExploreActivity;)Lcom/jme3/app/Application;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->app:Lcom/jme3/app/Application;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/exploreactivity/ExploreActivity;)Lcom/jme3/app/Application;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->app:Lcom/jme3/app/Application;

    return-object v0
.end method

.method public static createIntent(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x20020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "ExploreActivity.doc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "ExploreActivity.referrer"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private parseAndLogIntent(Landroid/content/Intent;)Lcom/google/android/finsky/api/model/Document;
    .locals 7
    .param p1    # Landroid/content/Intent;

    if-nez p1, :cond_1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ExploreActivity.doc"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ExploreActivity.referrer"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getCookie()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "exploreStarted?doc="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v0, v3, v4}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    const-string v3, "exploreStarted"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "cidi"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "c"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logTag(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public handleError(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Throwable;

    :try_start_0
    const-string v0, "Exception in ExploreActivity, exiting. Exception:\n%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    instance-of v0, p2, Ljava/lang/OutOfMemoryError;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/NodeController;->dumpControllerState()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->app:Lcom/jme3/app/Application;

    invoke-virtual {v0}, Lcom/jme3/app/Application;->stop()V

    invoke-virtual {p0}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->finish()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->app:Lcom/jme3/app/Application;

    invoke-virtual {v1}, Lcom/jme3/app/Application;->stop()V

    invoke-virtual {p0}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->finish()V

    throw v0
.end method

.method public layoutDisplay()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->mParentLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->view:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->mParentLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->parseAndLogIntent(Landroid/content/Intent;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    new-instance v3, Landroid/widget/RelativeLayout;

    invoke-direct {v3, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->mParentLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    new-instance v3, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;

    invoke-virtual {p0}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->mMusicPreviewManager:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;

    new-instance v3, Lcom/google/android/finsky/exploreactivity/NodeController;

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->mMusicPreviewManager:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v5

    invoke-direct {v3, p0, v0, v4, v5}, Lcom/google/android/finsky/exploreactivity/NodeController;-><init>(Lcom/google/android/finsky/exploreactivity/ExploreActivity;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;Lcom/android/volley/RequestQueue;)V

    iput-object v3, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    new-instance v3, Lcom/google/android/finsky/exploreactivity/ExploreApplication;

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->mParentLayout:Landroid/widget/RelativeLayout;

    invoke-direct {v3, p0, v4, v2, v5}, Lcom/google/android/finsky/exploreactivity/ExploreApplication;-><init>(Lcom/google/android/finsky/exploreactivity/ExploreActivity;Lcom/google/android/finsky/exploreactivity/NodeController;Lcom/google/android/finsky/api/model/Document;Landroid/widget/RelativeLayout;)V

    iput-object v3, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->app:Lcom/jme3/app/Application;

    invoke-super {p0, p1}, Lcom/jme3/app/AndroidHarness;->onCreate(Landroid/os/Bundle;)V

    iget-object v3, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->app:Lcom/jme3/app/Application;

    check-cast v3, Lcom/google/android/finsky/exploreactivity/ExploreApplication;

    invoke-virtual {v3}, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->createViews()V

    const-string v3, "com.jme3"

    invoke-static {v3}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v3

    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    return-void
.end method

.method public onDestroy()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "Destroying explorer"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-super {p0}, Lcom/jme3/app/AndroidHarness;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->mMusicPreviewManager:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->destroy()V

    invoke-static {v2}, Lcom/jme3/system/JmeSystem;->setSoftTextDialogInput(Lcom/jme3/input/SoftTextDialogInput;)V

    invoke-static {v2}, Lcom/jme3/system/android/JmeAndroidSystem;->setActivity(Landroid/app/Activity;)V

    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->parseAndLogIntent(Landroid/content/Intent;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->setIntent(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->app:Lcom/jme3/app/Application;

    check-cast v1, Lcom/google/android/finsky/exploreactivity/ExploreApplication;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->setSeedDocument(Lcom/google/android/finsky/api/model/Document;)V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    const-string v0, "Pausing explorer"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->app:Lcom/jme3/app/Application;

    check-cast v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->onPause()V

    invoke-super {p0}, Lcom/jme3/app/AndroidHarness;->onPause()V

    return-void
.end method

.method public onStop()V
    .locals 2

    const-string v0, "Stopping explorer"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->app:Lcom/jme3/app/Application;

    check-cast v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->onStop()V

    invoke-super {p0}, Lcom/jme3/app/AndroidHarness;->onStop()V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->mMusicPreviewManager:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->clear()V

    return-void
.end method

.method public onTouch(Ljava/lang/String;Lcom/jme3/input/event/TouchEvent;F)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/jme3/input/event/TouchEvent;
    .param p3    # F

    const-string v0, "TouchEscape"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/finsky/exploreactivity/ExploreActivity$2;->$SwitchMap$com$jme3$input$event$TouchEvent$Type:[I

    invoke-virtual {p2}, Lcom/jme3/input/event/TouchEvent;->getType()Lcom/jme3/input/event/TouchEvent$Type;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jme3/input/event/TouchEvent$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/jme3/app/AndroidHarness;->onTouch(Ljava/lang/String;Lcom/jme3/input/event/TouchEvent;F)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    new-instance v0, Lcom/google/android/finsky/exploreactivity/ExploreActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/exploreactivity/ExploreActivity$1;-><init>(Lcom/google/android/finsky/exploreactivity/ExploreActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public runOnGlThread(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->view:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, p1}, Landroid/opengl/GLSurfaceView;->queueEvent(Ljava/lang/Runnable;)V

    return-void
.end method
