.class public Lcom/google/android/finsky/fragments/GaiaAuthFragment;
.super Landroid/support/v4/app/Fragment;
.source "GaiaAuthFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;
    }
.end annotation


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mAuthRequest:Lcom/android/volley/Request;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation
.end field

.field private mAuthenticationType:I

.field private mChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$AuthenticationChallenge;

.field private mInput:Landroid/widget/EditText;

.field private mListener:Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

.field private mRetryCount:I

.field private mShowWarning:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthRequest:Lcom/android/volley/Request;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/fragments/GaiaAuthFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->verifyInput()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/fragments/GaiaAuthFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->success()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/fragments/GaiaAuthFragment;I)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/fragments/GaiaAuthFragment;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->failure(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/fragments/GaiaAuthFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->failure()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/fragments/GaiaAuthFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAccountName:Ljava/lang/String;

    return-object v0
.end method

.method private failure()V
    .locals 1

    const v0, 0x7f07005d

    invoke-direct {p0, v0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->failure(I)V

    return-void
.end method

.method private failure(I)V
    .locals 6
    .param p1    # I

    const/4 v3, 0x0

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mRetryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mRetryCount:I

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "authChallenge.failure?type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthenticationType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&retries="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mRetryCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v3, v1}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const-string v1, "authChallenge.failure"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "type"

    aput-object v4, v2, v3

    iget v3, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthenticationType:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string v4, "retries"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mRetryCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logTag(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;ZLcom/google/android/finsky/remoting/protos/ChallengeProtos$AuthenticationChallenge;)Lcom/google/android/finsky/fragments/GaiaAuthFragment;
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Z
    .param p2    # Lcom/google/android/finsky/remoting/protos/ChallengeProtos$AuthenticationChallenge;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "GaiaAuthFragment_showWarning"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "GaiaAuthFragment_authenticationChallenge"

    invoke-static {p2}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v1, Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private success()V
    .locals 6

    const/4 v5, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mListener:Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mListener:Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

    iget v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthenticationType:I

    iget v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mRetryCount:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;->onSuccess(II)V

    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "authChallenge.success?type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthenticationType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&retries="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mRetryCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v3, v1}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const-string v1, "authChallenge.success"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "type"

    aput-object v4, v2, v3

    iget v3, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthenticationType:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string v4, "retries"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mRetryCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logTag(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private verifyGaia(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/finsky/billing/challenge/ClientLoginApi;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/finsky/billing/challenge/ClientLoginApi;-><init>(Lcom/android/volley/RequestQueue;)V

    iget-object v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAccountName:Ljava/lang/String;

    new-instance v2, Lcom/google/android/finsky/fragments/GaiaAuthFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment$2;-><init>(Lcom/google/android/finsky/fragments/GaiaAuthFragment;)V

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/finsky/billing/challenge/ClientLoginApi;->validateUser(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/billing/challenge/ClientLoginApi$ClientLoginListener;)Lcom/android/volley/Request;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthRequest:Lcom/android/volley/Request;

    return-void
.end method

.method private verifyInput()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    invoke-direct {p0, v0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->verifyGaia(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->success()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->failure()V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->verifyInput()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mListener:Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mListener:Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

    invoke-interface {v0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;->onCancel()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f08003b
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "GaiaAuthFragment_authenticationChallenge"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/ChallengeProtos$AuthenticationChallenge;

    iput-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$AuthenticationChallenge;

    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAccountName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "GaiaAuthFragment_showWarning"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mShowWarning:Z

    if-eqz p1, :cond_0

    const-string v0, "GaiaAuthFragment_retryCount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mRetryCount:I

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 13
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v8, 0x7f040078

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$AuthenticationChallenge;

    invoke-virtual {v8}, Lcom/google/android/finsky/remoting/protos/ChallengeProtos$AuthenticationChallenge;->getAuthenticationType()I

    move-result v8

    iput v8, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthenticationType:I

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "authChallenge.trigger?type="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthenticationType:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v8, v9, v10, v11}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v8

    const-string v9, "authChallenge.trigger"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "type"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget v12, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthenticationType:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logTag(Ljava/lang/String;[Ljava/lang/Object;)V

    const v8, 0x7f08004d

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v8, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$AuthenticationChallenge;

    invoke-virtual {v8}, Lcom/google/android/finsky/remoting/protos/ChallengeProtos$AuthenticationChallenge;->getGaiaDescriptionTextHtml()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :cond_0
    const v8, 0x7f08014d

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    iput-object v8, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    iget-object v8, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v8, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    new-instance v9, Lcom/google/android/finsky/fragments/GaiaAuthFragment$1;

    invoke-direct {v9, p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment$1;-><init>(Lcom/google/android/finsky/fragments/GaiaAuthFragment;)V

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    const v8, 0x7f08014f

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$AuthenticationChallenge;

    invoke-virtual {v8}, Lcom/google/android/finsky/remoting/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaFooterTextHtml()Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mChallenge:Lcom/google/android/finsky/remoting/protos/ChallengeProtos$AuthenticationChallenge;

    invoke-virtual {v8}, Lcom/google/android/finsky/remoting/protos/ChallengeProtos$AuthenticationChallenge;->getGaiaFooterTextHtml()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-boolean v8, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mShowWarning:Z

    if-eqz v8, :cond_1

    const v8, 0x7f08014e

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const v8, 0x7f070212

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    sget-object v11, Lcom/google/android/finsky/config/G;->gaiaOptOutLearnMoreLink:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v11}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    const v8, 0x7f08003b

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    const v8, 0x7f0701da

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setText(I)V

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v8, 0x7f08003c

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v8, 0x7f070058

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setText(I)V

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v7

    :cond_2
    const/16 v8, 0x8

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthRequest:Lcom/android/volley/Request;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthRequest:Lcom/android/volley/Request;

    invoke-virtual {v0}, Lcom/android/volley/Request;->cancel()V

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "GaiaAuthFragment_retryCount"

    iget v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mRetryCount:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public setListener(Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

    iput-object p1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mListener:Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

    return-void
.end method
