.class public Lcom/google/android/finsky/layout/ExplorePanel;
.super Landroid/widget/FrameLayout;
.source "ExplorePanel.java"


# instance fields
.field private mDocument:Lcom/google/android/finsky/api/model/Document;

.field private mExploreButton:Landroid/view/View;

.field private mFragment:Landroid/support/v4/app/Fragment;

.field private final mHalfSeparatorThickness:I

.field private mPageUrl:Ljava/lang/String;

.field private final mSeparatorPaint:Landroid/graphics/Paint;

.field private mWifiView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/ExplorePanel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/ExplorePanel;->setWillNotDraw(Z)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0015

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/lit8 v2, v1, 0x1

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mHalfSeparatorThickness:I

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mSeparatorPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mSeparatorPaint:Landroid/graphics/Paint;

    const v3, 0x7f0a000e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mSeparatorPaint:Landroid/graphics/Paint;

    int-to-float v3, v1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/ExplorePanel;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/layout/ExplorePanel;

    invoke-direct {p0}, Lcom/google/android/finsky/layout/ExplorePanel;->doExplorerWifiAlert()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/ExplorePanel;)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/ExplorePanel;

    iget-object v0, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mFragment:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/ExplorePanel;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/ExplorePanel;

    iget-object v0, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mDocument:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/ExplorePanel;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/ExplorePanel;

    iget-object v0, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mPageUrl:Ljava/lang/String;

    return-object v0
.end method

.method private doExplorerWifiAlert()V
    .locals 4

    const v1, 0x7f07029d

    const v2, 0x7f0701f2

    const v3, 0x7f070058

    invoke-static {v1, v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(III)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mFragment:Landroid/support/v4/app/Fragment;

    const/4 v2, 0x5

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    iget-object v1, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "explorer_wifi"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public configure(Lcom/google/android/finsky/api/model/Document;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Landroid/support/v4/app/Fragment;
    .param p3    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mDocument:Lcom/google/android/finsky/api/model/Document;

    iput-object p2, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mFragment:Landroid/support/v4/app/Fragment;

    iput-object p3, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mPageUrl:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ExplorePanel;->updateButtonState()V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ExplorePanel;->requestLayout()V

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1    # Landroid/graphics/Canvas;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ExplorePanel;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mHalfSeparatorThickness:I

    sub-int v6, v0, v1

    const/4 v1, 0x0

    int-to-float v2, v6

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ExplorePanel;->getWidth()I

    move-result v0

    int-to-float v3, v0

    int-to-float v4, v6

    iget-object v5, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mSeparatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f080128

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ExplorePanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mWifiView:Landroid/view/View;

    const v0, 0x7f080127

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ExplorePanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mExploreButton:Landroid/view/View;

    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/high16 v4, 0x40000000

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    iget-object v2, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mExploreButton:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mExploreButton:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mWifiView:Landroid/view/View;

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    return-void
.end method

.method public updateButtonState()V
    .locals 6

    const/16 v4, 0x8

    const/4 v0, 0x1

    const/4 v3, 0x0

    sget-object v2, Lcom/google/android/finsky/config/G;->explorerEnabled:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    const/4 v5, 0x2

    if-ne v2, v5, :cond_2

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/ExplorePanel;->setVisibility(I)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    const-string v5, "connectivity"

    invoke-virtual {v2, v5}, Lcom/google/android/finsky/FinskyApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    invoke-virtual {v2, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mWifiView:Landroid/view/View;

    if-eqz v0, :cond_0

    move v3, v4

    :cond_0
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mWifiView:Landroid/view/View;

    new-instance v3, Lcom/google/android/finsky/layout/ExplorePanel$1;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/layout/ExplorePanel$1;-><init>(Lcom/google/android/finsky/layout/ExplorePanel;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/google/android/finsky/layout/ExplorePanel;->mExploreButton:Landroid/view/View;

    new-instance v3, Lcom/google/android/finsky/layout/ExplorePanel$2;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/layout/ExplorePanel$2;-><init>(Lcom/google/android/finsky/layout/ExplorePanel;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    return-void

    :cond_1
    move v0, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/ExplorePanel;->setVisibility(I)V

    goto :goto_1
.end method
