.class public Lcom/google/android/finsky/layout/OwnedActions;
.super Lcom/google/android/finsky/layout/SeparatorLinearLayout;
.source "OwnedActions.java"


# instance fields
.field private mDocument:Lcom/google/android/finsky/api/model/Document;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mLibraries:Lcom/google/android/finsky/library/Libraries;

.field private mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/OwnedActions;->mInflater:Landroid/view/LayoutInflater;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/OwnedActions;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    return-void
.end method

.method private configureRateReviewSection(Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/support/v4/app/Fragment;Z)V
    .locals 4
    .param p1    # Lcom/google/android/finsky/api/model/DfeDetails;
    .param p2    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3    # Landroid/support/v4/app/Fragment;
    .param p4    # Z

    iget-object v1, p0, Lcom/google/android/finsky/layout/OwnedActions;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f0400f1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/RateReviewSection;

    iput-object v1, p0, Lcom/google/android/finsky/layout/OwnedActions;->mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

    iget-object v1, p0, Lcom/google/android/finsky/layout/OwnedActions;->mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/OwnedActions;->addView(Landroid/view/View;)V

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/DfeDetails;->getUserReview()Lcom/google/android/finsky/remoting/protos/Rev$Review;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/OwnedActions;->mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

    iget-object v2, p0, Lcom/google/android/finsky/layout/OwnedActions;->mDocument:Lcom/google/android/finsky/api/model/Document;

    iget-object v3, p0, Lcom/google/android/finsky/layout/OwnedActions;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v1, v2, v0, p3, v3}, Lcom/google/android/finsky/layout/RateReviewSection;->configure(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/remoting/protos/Rev$Review;Landroid/support/v4/app/Fragment;Lcom/google/android/finsky/library/Libraries;)V

    return-void
.end method

.method private updateVisibility()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/finsky/layout/OwnedActions;->mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

    iget-object v2, p0, Lcom/google/android/finsky/layout/OwnedActions;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    iget-object v3, p0, Lcom/google/android/finsky/layout/OwnedActions;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/layout/RateReviewSection;->updateVisibility(Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/api/model/Document;)V

    iget-object v1, p0, Lcom/google/android/finsky/layout/OwnedActions;->mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/RateReviewSection;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/OwnedActions;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-void
.end method

.method public setDocument(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/support/v4/app/Fragment;Z)V
    .locals 1
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Lcom/google/android/finsky/api/model/DfeDetails;
    .param p3    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4    # Landroid/support/v4/app/Fragment;
    .param p5    # Z

    iput-object p1, p0, Lcom/google/android/finsky/layout/OwnedActions;->mDocument:Lcom/google/android/finsky/api/model/Document;

    if-eqz p5, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OwnedActions;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/OwnedActions;->removeAllViews()V

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/android/finsky/layout/OwnedActions;->configureRateReviewSection(Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/support/v4/app/Fragment;Z)V

    invoke-direct {p0}, Lcom/google/android/finsky/layout/OwnedActions;->updateVisibility()V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public updateRating(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/finsky/layout/OwnedActions;->mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/RateReviewSection;->updateRating(I)V

    return-void
.end method
