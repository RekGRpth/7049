.class public Lcom/google/android/finsky/layout/ReviewsControlContainer;
.super Landroid/widget/LinearLayout;
.source "ReviewsControlContainer.java"


# instance fields
.field private mFilterBox:Landroid/widget/TextView;

.field private mSortSpinner:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private setSortAdapter()V
    .locals 3

    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ReviewsControlContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040104

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ReviewsControlContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f07018b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ReviewsControlContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f07018c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ReviewsControlContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f07018d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/finsky/layout/ReviewsControlContainer;->mSortSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f080215

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewsControlContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewsControlContainer;->mSortSpinner:Landroid/widget/Spinner;

    const v0, 0x7f080216

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewsControlContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewsControlContainer;->mFilterBox:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/google/android/finsky/layout/ReviewsControlContainer;->setSortAdapter()V

    return-void
.end method

.method public setData(Lcom/google/android/finsky/api/model/DfeReviews;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/api/model/DfeReviews;

    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewsControlContainer;->mSortSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/google/android/finsky/layout/ReviewsControlContainer$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/layout/ReviewsControlContainer$1;-><init>(Lcom/google/android/finsky/layout/ReviewsControlContainer;Lcom/google/android/finsky/api/model/DfeReviews;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/DfeReviews;->getSortType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewsControlContainer;->mSortSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewsControlContainer;->mSortSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewsControlContainer;->mSortSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public setFilterHandler(Lcom/google/android/finsky/adapters/ReviewsAdapter$ChooseFilterOptionsHandler;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/adapters/ReviewsAdapter$ChooseFilterOptionsHandler;

    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewsControlContainer;->mFilterBox:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/finsky/layout/ReviewsControlContainer$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/layout/ReviewsControlContainer$2;-><init>(Lcom/google/android/finsky/layout/ReviewsControlContainer;Lcom/google/android/finsky/adapters/ReviewsAdapter$ChooseFilterOptionsHandler;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
