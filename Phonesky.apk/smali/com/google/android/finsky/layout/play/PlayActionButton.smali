.class public Lcom/google/android/finsky/layout/play/PlayActionButton;
.super Landroid/widget/Button;
.source "PlayActionButton.java"


# instance fields
.field private final mDrawAsLabel:Z

.field private final mIgnoreCorpusColor:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v1, Lcom/android/vending/R$styleable;->PlayActionButton:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayActionButton;->mDrawAsLabel:Z

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayActionButton;->mIgnoreCorpusColor:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public configure(ILandroid/view/View$OnClickListener;)V
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/View$OnClickListener;

    const/4 v7, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getPaddingRight()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getPaddingTop()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getPaddingBottom()I

    move-result v3

    if-eqz p2, :cond_0

    move v2, v7

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setFocusable(Z)V

    invoke-virtual {p0, p2}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setClickable(Z)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v7, p0, Lcom/google/android/finsky/layout/play/PlayActionButton;->mIgnoreCorpusColor:Z

    if-eqz v7, :cond_2

    :goto_2
    iget-boolean v7, p0, Lcom/google/android/finsky/layout/play/PlayActionButton;->mDrawAsLabel:Z

    if-eqz v7, :cond_3

    invoke-static {v1, v0}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setTextColor(I)V

    :goto_3
    invoke-virtual {p0, v4, v6, v5, v3}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setPadding(IIII)V

    return-void

    :cond_0
    move v2, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setFocusable(Z)V

    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setClickable(Z)V

    goto :goto_1

    :cond_2
    move v0, p1

    goto :goto_2

    :cond_3
    if-eqz v2, :cond_4

    invoke-static {v1, v0}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPlayActionButtonBackgroundDrawable(Landroid/content/Context;I)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setBackgroundResource(I)V

    goto :goto_3

    :cond_4
    invoke-static {v1, v0}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setBackgroundColor(I)V

    goto :goto_3
.end method
