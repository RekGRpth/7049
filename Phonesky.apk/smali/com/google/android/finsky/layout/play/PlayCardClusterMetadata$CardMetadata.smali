.class public Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
.super Ljava/lang/Object;
.source "PlayCardClusterMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CardMetadata"
.end annotation


# instance fields
.field private final mHSpan:I

.field private final mLayoutId:I

.field private final mLayoutIdNoReason:I

.field private final mThumbnailAspectRatio:F

.field private final mVSpan:I


# direct methods
.method public constructor <init>(IIIF)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # F

    const/4 v2, -0x1

    move-object v0, p0

    move v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;-><init>(IIIIF)V

    return-void
.end method

.method public constructor <init>(IIIIF)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mLayoutId:I

    iput p2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mLayoutIdNoReason:I

    iput p3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mHSpan:I

    iput p4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mVSpan:I

    iput p5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mThumbnailAspectRatio:F

    return-void
.end method


# virtual methods
.method public getHSpan()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mHSpan:I

    return v0
.end method

.method public getLayoutId()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mLayoutId:I

    return v0
.end method

.method public getNoReasonLayoutId()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mLayoutIdNoReason:I

    return v0
.end method

.method public getThumbnailAspectRatio()F
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mThumbnailAspectRatio:F

    return v0
.end method

.method public getVSpan()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mVSpan:I

    return v0
.end method

.method public hasNoReasonLayoutId()Z
    .locals 2

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mLayoutIdNoReason:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
