.class Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction$1;
.super Ljava/lang/Object;
.source "PlayCardOverflowView.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->onActionSelected()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;

    # getter for: Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->mOnDismissListener:Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;
    invoke-static {v0}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->access$100(Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;)Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;

    # getter for: Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->mDoc:Lcom/google/android/finsky/api/model/Document;
    invoke-static {v1}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->access$000(Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;->onDismissDocument(Lcom/google/android/finsky/api/model/Document;)V

    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction$1;->onResponse(Lcom/google/android/finsky/remoting/protos/RateSuggestedContentResponse;)V

    return-void
.end method
