.class public Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;
.super Lcom/google/android/finsky/layout/play/PlayEditorialCardView;
.source "PlayEditorialAppCardView.java"


# static fields
.field private static final IMAGE_TYPES:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->IMAGE_TYPES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x4
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayEditorialCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected getThumbnailImageTypes()[I
    .locals 1

    sget-object v0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->IMAGE_TYPES:[I

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Lcom/google/android/finsky/layout/play/PlayEditorialCardView;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/DecoratedTextView;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/DecoratedTextView;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/DecoratedTextView;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1    # I
    .param p2    # I

    const/high16 v10, 0x40000000

    const/high16 v8, 0x3f000000

    iput v8, p0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->mThumbnailAspectRatio:F

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->measureThumbnailSpanningWidth(I)V

    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayEditorialCardView;->onMeasure(II)V

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/DecoratedTextView;->getMeasuredWidth()I

    move-result v3

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getMeasuredWidth()I

    move-result v5

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getMeasuredHeight()I

    move-result v4

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    add-int v8, v5, v3

    if-lt v8, v7, :cond_0

    if-ge v6, v4, :cond_0

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/DecoratedTextView;->getMeasuredHeight()I

    move-result v1

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/DecoratedTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    sub-int v8, v7, v5

    iget v9, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    sub-int v2, v8, v9

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-static {v2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-static {v1, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/google/android/finsky/layout/DecoratedTextView;->measure(II)V

    :cond_0
    return-void
.end method
