.class public Lcom/google/android/finsky/layout/play/PlayTabContainer;
.super Landroid/widget/HorizontalScrollView;
.source "PlayTabContainer.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field private mLastScrollTo:I

.field private mScrollState:I

.field private mTabStrip:Lcom/google/android/finsky/layout/play/PlayTabStrip;

.field private final mTitleOffset:I

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayTabContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->setupBackground()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->setHorizontalScrollBarEnabled(Z)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b005d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mTitleOffset:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/play/PlayTabContainer;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/play/PlayTabContainer;

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/play/PlayTabContainer;II)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/layout/play/PlayTabContainer;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->scrollToChild(II)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/play/PlayTabContainer;)Lcom/google/android/finsky/layout/play/PlayTabStrip;
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/play/PlayTabContainer;

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mTabStrip:Lcom/google/android/finsky/layout/play/PlayTabStrip;

    return-object v0
.end method

.method private scrollToChild(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mTabStrip:Lcom/google/android/finsky/layout/play/PlayTabStrip;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayTabStrip;->getChildCount()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mTabStrip:Lcom/google/android/finsky/layout/play/PlayTabStrip;

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/layout/play/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v0

    add-int v1, v0, p2

    if-gtz p1, :cond_2

    if-lez p2, :cond_3

    :cond_2
    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mTitleOffset:I

    sub-int/2addr v1, v2

    :cond_3
    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mLastScrollTo:I

    if-eq v1, v2, :cond_0

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mLastScrollTo:I

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->scrollTo(II)V

    goto :goto_0
.end method

.method private setupBackground()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    sget-object v1, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onFinishInflate()V

    const v0, 0x7f0801c4

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayTabStrip;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mTabStrip:Lcom/google/android/finsky/layout/play/PlayTabStrip;

    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mScrollState:I

    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # I

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mTabStrip:Lcom/google/android/finsky/layout/play/PlayTabStrip;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayTabStrip;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mTabStrip:Lcom/google/android/finsky/layout/play/PlayTabStrip;

    invoke-virtual {v2, p1, p2, p3}, Lcom/google/android/finsky/layout/play/PlayTabStrip;->onPageScrolled(IFI)V

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mTabStrip:Lcom/google/android/finsky/layout/play/PlayTabStrip;

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/layout/play/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p2

    float-to-int v0, v2

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->scrollToChild(II)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mScrollState:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mTabStrip:Lcom/google/android/finsky/layout/play/PlayTabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/play/PlayTabStrip;->onPageSelected(I)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->scrollToChild(II)V

    :cond_0
    return-void
.end method

.method public setSelectedIndicatorColor(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mTabStrip:Lcom/google/android/finsky/layout/play/PlayTabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/play/PlayTabStrip;->setSelectedIndicatorColor(I)V

    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 8
    .param p1    # Landroid/support/v4/view/ViewPager;

    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v5}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTabContainer;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v5

    if-ge v1, v5, :cond_0

    const v5, 0x7f0400e1

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mTabStrip:Lcom/google/android/finsky/layout/play/PlayTabStrip;

    const/4 v7, 0x0

    invoke-virtual {v3, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/PagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v2, v1

    new-instance v5, Lcom/google/android/finsky/layout/play/PlayTabContainer$1;

    invoke-direct {v5, p0, v2}, Lcom/google/android/finsky/layout/play/PlayTabContainer$1;-><init>(Lcom/google/android/finsky/layout/play/PlayTabContainer;I)V

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mTabStrip:Lcom/google/android/finsky/layout/play/PlayTabStrip;

    invoke-virtual {v5, v4}, Lcom/google/android/finsky/layout/play/PlayTabStrip;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayTabContainer;->mTabStrip:Lcom/google/android/finsky/layout/play/PlayTabStrip;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayTabStrip;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v5

    new-instance v6, Lcom/google/android/finsky/layout/play/PlayTabContainer$2;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/layout/play/PlayTabContainer$2;-><init>(Lcom/google/android/finsky/layout/play/PlayTabContainer;)V

    invoke-virtual {v5, v6}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method
