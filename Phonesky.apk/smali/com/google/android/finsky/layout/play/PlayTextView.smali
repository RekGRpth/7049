.class public Lcom/google/android/finsky/layout/play/PlayTextView;
.super Landroid/widget/TextView;
.source "PlayTextView.java"


# instance fields
.field private final mCompactPercentage:I

.field private mLastLineFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

.field private mLastLineFadeOutSize:I

.field private mLastLineOverdrawPaint:Landroid/graphics/Paint;

.field private final mToDrawOverLastLineIfNecessary:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v3, Lcom/android/vending/R$styleable;->PlayTextView:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    invoke-virtual {v2, v7, v7}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mCompactPercentage:I

    invoke-virtual {v2, v8}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mToDrawOverLastLineIfNecessary:Z

    iget-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mToDrawOverLastLineIfNecessary:Z

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a0001

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v8, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const v3, 0x7f0b0075

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineFadeOutSize:I

    new-instance v3, Landroid/graphics/drawable/GradientDrawable;

    sget-object v4, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v5, 0x2

    new-array v5, v5, [I

    const v6, 0xffffff

    and-int/2addr v6, v0

    aput v6, v5, v7

    aput v0, v5, v8

    invoke-direct {v3, v4, v5}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    iput-object v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

    :cond_0
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mCompactPercentage:I

    if-lez v3, :cond_1

    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mCompactPercentage:I

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getTextSize()F

    move-result v4

    mul-float/2addr v3, v4

    const/high16 v4, 0x42c80000

    div-float/2addr v3, v4

    const/high16 v4, 0x3f800000

    invoke-virtual {p0, v3, v4}, Lcom/google/android/finsky/layout/play/PlayTextView;->setLineSpacing(FF)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mToDrawOverLastLineIfNecessary:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getLayout()Landroid/text/Layout;

    move-result-object v9

    if-eqz v9, :cond_0

    const/4 v13, -0x1

    const/4 v11, -0x1

    const/4 v10, 0x0

    :goto_1
    invoke-virtual {v9}, Landroid/text/Layout;->getLineCount()I

    move-result v0

    if-ge v10, v0, :cond_0

    invoke-virtual {v9, v10}, Landroid/text/Layout;->getLineTop(I)I

    move-result v7

    invoke-virtual {v9, v10}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v6

    if-ge v7, v8, :cond_2

    if-le v6, v8, :cond_2

    const/4 v1, 0x0

    int-to-float v2, v7

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    int-to-float v4, v8

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    if-lez v10, :cond_0

    add-int/lit8 v0, v10, -0x1

    invoke-virtual {v9, v0}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v0

    float-to-int v12, v0

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineFadeOutSize:I

    sub-int v1, v12, v1

    invoke-virtual {v0, v1, v13, v12, v11}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_2
    move v13, v7

    move v11, v6

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mCompactPercentage:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, 0x40000000

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getMeasuredHeight()I

    move-result v1

    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mCompactPercentage:I

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getLineHeight()I

    move-result v3

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/layout/play/PlayTextView;->setMeasuredDimension(II)V

    goto :goto_0
.end method
