.class public Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;
.super Landroid/widget/RelativeLayout;
.source "PlayWarmWelcomeBannerView.java"


# instance fields
.field private mColumnCount:I

.field private mOverlapGradient:Landroid/view/View;

.field private mOverlapGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

.field private mPromoFill:Landroid/view/View;

.field private mPromoImage:Lcom/google/android/finsky/layout/FifeImageView;

.field private mTexts:Landroid/view/ViewGroup;

.field private mTextsMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mColumnCount:I

    return-void
.end method

.method private getPromoImageWindowWidth(I)I
    .locals 3
    .param p1    # I

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mColumnCount:I

    if-ne v2, v1, :cond_0

    const/4 v1, 0x2

    :cond_0
    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mColumnCount:I

    sub-int v0, v2, v1

    mul-int v2, v0, p1

    return v2
.end method


# virtual methods
.method public bind(ILcom/google/android/finsky/utils/BitmapLoader;)V
    .locals 11
    .param p1    # I
    .param p2    # Lcom/google/android/finsky/utils/BitmapLoader;

    const/4 v10, 0x1

    const/4 v9, 0x2

    const/4 v5, 0x0

    if-gtz p1, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Warm welcome banner doesn\'t support non-positive number of columns: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " passed"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mColumnCount:I

    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mColumnCount:I

    if-le v4, v9, :cond_2

    move v1, v5

    :goto_1
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v4, v1}, Lcom/google/android/finsky/layout/FifeImageView;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mOverlapGradient:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    sget-object v4, Lcom/google/android/finsky/config/G;->warmWelcomeFillColor:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v1, :cond_1

    new-instance v3, Lcom/google/android/finsky/remoting/protos/Doc$Image;

    invoke-direct {v3}, Lcom/google/android/finsky/remoting/protos/Doc$Image;-><init>()V

    sget-object v4, Lcom/google/android/finsky/config/G;->warmWelcomeGraphicUrl:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->setImageUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Doc$Image;

    invoke-virtual {v3, v10}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->setSupportsFifeUrlOptions(Z)Lcom/google/android/finsky/remoting/protos/Doc$Image;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v4, v3, p2}, Lcom/google/android/finsky/layout/FifeImageView;->setImage(Lcom/google/android/finsky/remoting/protos/Doc$Image;Lcom/google/android/finsky/utils/BitmapLoader;)V

    :cond_1
    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mColumnCount:I

    if-ne v4, v9, :cond_3

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoFill:Landroid/view/View;

    const v6, 0x7f020011

    invoke-virtual {v4, v6}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_2
    new-instance v4, Landroid/graphics/drawable/GradientDrawable;

    sget-object v6, Landroid/graphics/drawable/GradientDrawable$Orientation;->RIGHT_LEFT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v7, v9, [I

    const v8, 0xffffff

    and-int/2addr v8, v0

    aput v8, v7, v5

    aput v0, v7, v10

    invoke-direct {v4, v6, v7}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    iput-object v4, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mOverlapGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mOverlapGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/GradientDrawable;->setDither(Z)V

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mOverlapGradient:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mOverlapGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mColumnCount:I

    if-ne v4, v9, :cond_4

    const v2, 0x7f0b0074

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mTextsMargin:I

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoFill:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_2

    :cond_4
    const v2, 0x7f0b0073

    goto :goto_3
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const v0, 0x7f0801c5

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoFill:Landroid/view/View;

    const v0, 0x7f0801c6

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoImage:Lcom/google/android/finsky/layout/FifeImageView;

    const v0, 0x7f0801b9

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mOverlapGradient:Landroid/view/View;

    const v0, 0x7f0801c7

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mTexts:Landroid/view/ViewGroup;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 17
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->getWidth()I

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->getPaddingTop()I

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->getPaddingLeft()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mTexts:Landroid/view/ViewGroup;

    invoke-virtual {v13}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v10

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mTextsMargin:I

    add-int v9, v6, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mTexts:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mTextsMargin:I

    add-int/2addr v14, v5

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mTextsMargin:I

    add-int/2addr v15, v5

    add-int/2addr v15, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mTexts:Landroid/view/ViewGroup;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v16

    add-int v16, v16, v9

    move/from16 v0, v16

    invoke-virtual {v13, v14, v9, v15, v0}, Landroid/view/ViewGroup;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/FifeImageView;->getVisibility()I

    move-result v13

    if-nez v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/FifeImageView;->getMeasuredWidth()I

    move-result v7

    if-lez v7, :cond_2

    div-int/lit8 v3, v7, 0x2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mOverlapGradient:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mOverlapGradient:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mTextsMargin:I

    sub-int v13, v12, v13

    sub-int v11, v13, v10

    div-int/lit8 v13, v11, 0x2

    sub-int v13, v12, v13

    sub-int v4, v13, v3

    add-int v8, v4, v7

    if-ge v8, v12, :cond_0

    sub-int v13, v12, v8

    add-int/2addr v4, v13

    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoImage:Lcom/google/android/finsky/layout/FifeImageView;

    add-int v14, v4, v7

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v15}, Lcom/google/android/finsky/layout/FifeImageView;->getMeasuredHeight()I

    move-result v15

    add-int/2addr v15, v6

    invoke-virtual {v13, v4, v6, v14, v15}, Lcom/google/android/finsky/layout/FifeImageView;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mOverlapGradient:Landroid/view/View;

    add-int v14, v4, v2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mOverlapGradient:Landroid/view/View;

    invoke-virtual {v15}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    add-int/2addr v15, v6

    invoke-virtual {v13, v4, v6, v14, v15}, Landroid/view/View;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mOverlapGradientDrawable:Landroid/graphics/drawable/GradientDrawable;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15, v2, v1}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoFill:Landroid/view/View;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoFill:Landroid/view/View;

    invoke-virtual {v15}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    add-int/2addr v15, v6

    invoke-virtual {v13, v14, v6, v12, v15}, Landroid/view/View;->layout(IIII)V

    return-void

    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoImage:Lcom/google/android/finsky/layout/FifeImageView;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v15}, Lcom/google/android/finsky/layout/FifeImageView;->getMeasuredHeight()I

    move-result v15

    add-int/2addr v15, v6

    invoke-virtual {v13, v14, v6, v7, v15}, Lcom/google/android/finsky/layout/FifeImageView;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 13
    .param p1    # I
    .param p2    # I

    const/high16 v12, 0x40000000

    const/4 v9, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v10, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mColumnCount:I

    if-gtz v10, :cond_0

    invoke-virtual {p0, v0, v9}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v10}, Lcom/google/android/finsky/layout/FifeImageView;->getVisibility()I

    move-result v10

    if-nez v10, :cond_2

    const/4 v6, 0x1

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->getPaddingLeft()I

    move-result v10

    sub-int v10, v0, v10

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->getPaddingRight()I

    move-result v11

    sub-int v8, v10, v11

    if-eqz v6, :cond_3

    iget v10, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mColumnCount:I

    div-int v1, v0, v10

    iget v10, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mTextsMargin:I

    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->getPromoImageWindowWidth(I)I

    move-result v11

    add-int/2addr v10, v11

    sub-int/2addr v8, v10

    :goto_2
    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mTexts:Landroid/view/ViewGroup;

    invoke-static {v8, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v10, v11, v9}, Landroid/view/ViewGroup;->measure(II)V

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mTexts:Landroid/view/ViewGroup;

    invoke-virtual {v10}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v10

    iget v11, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mTextsMargin:I

    mul-int/lit8 v11, v11, 0x2

    add-int v3, v10, v11

    invoke-static {v3, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v10}, Lcom/google/android/finsky/layout/FifeImageView;->getVisibility()I

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v10}, Lcom/google/android/finsky/layout/FifeImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-nez v2, :cond_4

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-static {v9, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v10, v9, v4}, Lcom/google/android/finsky/layout/FifeImageView;->measure(II)V

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mOverlapGradient:Landroid/view/View;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_3
    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoFill:Landroid/view/View;

    invoke-virtual {v9, p1, v4}, Landroid/view/View;->measure(II)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->getPaddingTop()I

    move-result v9

    add-int/2addr v9, v3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->getPaddingBottom()I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {p0, v0, v9}, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->setMeasuredDimension(II)V

    goto :goto_0

    :cond_2
    move v6, v9

    goto :goto_1

    :cond_3
    iget v10, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mTextsMargin:I

    mul-int/lit8 v10, v10, 0x2

    sub-int/2addr v8, v10

    goto :goto_2

    :cond_4
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v11

    int-to-float v11, v11

    div-float v5, v10, v11

    int-to-float v10, v3

    div-float/2addr v10, v5

    float-to-int v7, v10

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mPromoImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-static {v7, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v10, v11, v4}, Lcom/google/android/finsky/layout/FifeImageView;->measure(II)V

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mOverlapGradient:Landroid/view/View;

    div-int/lit8 v11, v7, 0x4

    invoke-static {v11, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v10, v11, v4}, Landroid/view/View;->measure(II)V

    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayWarmWelcomeBannerView;->mOverlapGradient:Landroid/view/View;

    invoke-virtual {v10, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method
