.class public Lcom/google/android/finsky/layout/play/PopupMenuFactory;
.super Ljava/lang/Object;
.source "PopupMenuFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;Landroid/view/View;)Lcom/google/android/finsky/layout/play/PlayPopupMenu;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/view/View;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/google/android/finsky/layout/play/NativePopupMenu;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/layout/play/NativePopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/layout/play/LegacyPopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0
.end method
