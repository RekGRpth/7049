.class public final Lcom/google/android/finsky/remoting/protos/Common$Offer;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Offer"
.end annotation


# instance fields
.field private cachedSize:I

.field private checkoutFlowRequired_:Z

.field private convertedPrice_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/Common$Offer;",
            ">;"
        }
    .end annotation
.end field

.field private currencyCode_:Ljava/lang/String;

.field private formattedAmount_:Ljava/lang/String;

.field private formattedDescription_:Ljava/lang/String;

.field private formattedFullAmount_:Ljava/lang/String;

.field private formattedName_:Ljava/lang/String;

.field private fullPriceMicros_:J

.field private hasCheckoutFlowRequired:Z

.field private hasCurrencyCode:Z

.field private hasFormattedAmount:Z

.field private hasFormattedDescription:Z

.field private hasFormattedFullAmount:Z

.field private hasFormattedName:Z

.field private hasFullPriceMicros:Z

.field private hasLicensedOfferType:Z

.field private hasMicros:Z

.field private hasOfferType:Z

.field private hasOnSaleDate:Z

.field private hasOnSaleDateDisplayTimeZoneOffsetMsec:Z

.field private hasPreorder:Z

.field private hasRentalTerms:Z

.field private hasSubscriptionContentTerms:Z

.field private hasSubscriptionTerms:Z

.field private licensedOfferType_:I

.field private micros_:J

.field private offerType_:I

.field private onSaleDateDisplayTimeZoneOffsetMsec_:I

.field private onSaleDate_:J

.field private preorder_:Z

.field private promotionLabel_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private rentalTerms_:Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

.field private subscriptionContentTerms_:Lcom/google/android/finsky/remoting/protos/Common$SubscriptionContentTerms;

.field private subscriptionTerms_:Lcom/google/android/finsky/remoting/protos/Common$SubscriptionTerms;


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v5, 0x1

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-wide v3, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->micros_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->currencyCode_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->formattedAmount_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->formattedName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->formattedDescription_:Ljava/lang/String;

    iput-wide v3, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->fullPriceMicros_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->formattedFullAmount_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->convertedPrice_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->checkoutFlowRequired_:Z

    iput v5, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->offerType_:I

    iput v5, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->licensedOfferType_:I

    iput-object v2, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->rentalTerms_:Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

    iput-object v2, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->subscriptionTerms_:Lcom/google/android/finsky/remoting/protos/Common$SubscriptionTerms;

    iput-object v2, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->subscriptionContentTerms_:Lcom/google/android/finsky/remoting/protos/Common$SubscriptionContentTerms;

    iput-boolean v1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->preorder_:Z

    iput-wide v3, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->onSaleDate_:J

    iput v1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->onSaleDateDisplayTimeZoneOffsetMsec_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->promotionLabel_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addConvertedPrice(Lcom/google/android/finsky/remoting/protos/Common$Offer;)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Common$Offer;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->convertedPrice_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->convertedPrice_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->convertedPrice_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPromotionLabel(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->promotionLabel_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->promotionLabel_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->promotionLabel_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->cachedSize:I

    return v0
.end method

.method public getCheckoutFlowRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->checkoutFlowRequired_:Z

    return v0
.end method

.method public getConvertedPriceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/Common$Offer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->convertedPrice_:Ljava/util/List;

    return-object v0
.end method

.method public getCurrencyCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->currencyCode_:Ljava/lang/String;

    return-object v0
.end method

.method public getFormattedAmount()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->formattedAmount_:Ljava/lang/String;

    return-object v0
.end method

.method public getFormattedDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->formattedDescription_:Ljava/lang/String;

    return-object v0
.end method

.method public getFormattedFullAmount()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->formattedFullAmount_:Ljava/lang/String;

    return-object v0
.end method

.method public getFormattedName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->formattedName_:Ljava/lang/String;

    return-object v0
.end method

.method public getFullPriceMicros()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->fullPriceMicros_:J

    return-wide v0
.end method

.method public getLicensedOfferType()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->licensedOfferType_:I

    return v0
.end method

.method public getMicros()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->micros_:J

    return-wide v0
.end method

.method public getOfferType()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->offerType_:I

    return v0
.end method

.method public getOnSaleDate()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->onSaleDate_:J

    return-wide v0
.end method

.method public getOnSaleDateDisplayTimeZoneOffsetMsec()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->onSaleDateDisplayTimeZoneOffsetMsec_:I

    return v0
.end method

.method public getPreorder()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->preorder_:Z

    return v0
.end method

.method public getPromotionLabelList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->promotionLabel_:Ljava/util/List;

    return-object v0
.end method

.method public getRentalTerms()Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->rentalTerms_:Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasMicros()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getMicros()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasCurrencyCode()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getCurrencyCode()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedAmount()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getFormattedAmount()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getConvertedPriceList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/remoting/protos/Common$Offer;

    const/4 v4, 0x4

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasCheckoutFlowRequired()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getCheckoutFlowRequired()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFullPriceMicros()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getFullPriceMicros()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedFullAmount()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getFormattedFullAmount()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasOfferType()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getOfferType()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasRentalTerms()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getRentalTerms()Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasOnSaleDate()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getOnSaleDate()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getPromotionLabelList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1

    :cond_a
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getPromotionLabelList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasSubscriptionTerms()Z

    move-result v4

    if-eqz v4, :cond_b

    const/16 v4, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getSubscriptionTerms()Lcom/google/android/finsky/remoting/protos/Common$SubscriptionTerms;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedName()Z

    move-result v4

    if-eqz v4, :cond_c

    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getFormattedName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedDescription()Z

    move-result v4

    if-eqz v4, :cond_d

    const/16 v4, 0xe

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getFormattedDescription()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasPreorder()Z

    move-result v4

    if-eqz v4, :cond_e

    const/16 v4, 0xf

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getPreorder()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v3, v4

    :cond_e
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasOnSaleDateDisplayTimeZoneOffsetMsec()Z

    move-result v4

    if-eqz v4, :cond_f

    const/16 v4, 0x10

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getOnSaleDateDisplayTimeZoneOffsetMsec()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasLicensedOfferType()Z

    move-result v4

    if-eqz v4, :cond_10

    const/16 v4, 0x11

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getLicensedOfferType()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    :cond_10
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasSubscriptionContentTerms()Z

    move-result v4

    if-eqz v4, :cond_11

    const/16 v4, 0x12

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getSubscriptionContentTerms()Lcom/google/android/finsky/remoting/protos/Common$SubscriptionContentTerms;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_11
    iput v3, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->cachedSize:I

    return v3
.end method

.method public getSubscriptionContentTerms()Lcom/google/android/finsky/remoting/protos/Common$SubscriptionContentTerms;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->subscriptionContentTerms_:Lcom/google/android/finsky/remoting/protos/Common$SubscriptionContentTerms;

    return-object v0
.end method

.method public getSubscriptionTerms()Lcom/google/android/finsky/remoting/protos/Common$SubscriptionTerms;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->subscriptionTerms_:Lcom/google/android/finsky/remoting/protos/Common$SubscriptionTerms;

    return-object v0
.end method

.method public hasCheckoutFlowRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasCheckoutFlowRequired:Z

    return v0
.end method

.method public hasCurrencyCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasCurrencyCode:Z

    return v0
.end method

.method public hasFormattedAmount()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedAmount:Z

    return v0
.end method

.method public hasFormattedDescription()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedDescription:Z

    return v0
.end method

.method public hasFormattedFullAmount()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedFullAmount:Z

    return v0
.end method

.method public hasFormattedName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedName:Z

    return v0
.end method

.method public hasFullPriceMicros()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFullPriceMicros:Z

    return v0
.end method

.method public hasLicensedOfferType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasLicensedOfferType:Z

    return v0
.end method

.method public hasMicros()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasMicros:Z

    return v0
.end method

.method public hasOfferType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasOfferType:Z

    return v0
.end method

.method public hasOnSaleDate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasOnSaleDate:Z

    return v0
.end method

.method public hasOnSaleDateDisplayTimeZoneOffsetMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasOnSaleDateDisplayTimeZoneOffsetMsec:Z

    return v0
.end method

.method public hasPreorder()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasPreorder:Z

    return v0
.end method

.method public hasRentalTerms()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasRentalTerms:Z

    return v0
.end method

.method public hasSubscriptionContentTerms()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasSubscriptionContentTerms:Z

    return v0
.end method

.method public hasSubscriptionTerms()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasSubscriptionTerms:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setMicros(J)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setCurrencyCode(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setFormattedAmount(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Common$Offer;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Common$Offer;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->addConvertedPrice(Lcom/google/android/finsky/remoting/protos/Common$Offer;)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setCheckoutFlowRequired(Z)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setFullPriceMicros(J)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setFormattedFullAmount(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setOfferType(I)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setRentalTerms(Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setOnSaleDate(J)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->addPromotionLabel(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Common$SubscriptionTerms;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Common$SubscriptionTerms;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setSubscriptionTerms(Lcom/google/android/finsky/remoting/protos/Common$SubscriptionTerms;)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setFormattedName(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setFormattedDescription(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setPreorder(Z)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setOnSaleDateDisplayTimeZoneOffsetMsec(I)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setLicensedOfferType(I)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto/16 :goto_0

    :sswitch_12
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Common$SubscriptionContentTerms;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Common$SubscriptionContentTerms;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->setSubscriptionContentTerms(Lcom/google/android/finsky/remoting/protos/Common$SubscriptionContentTerms;)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x92 -> :sswitch_12
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Common$Offer;

    move-result-object v0

    return-object v0
.end method

.method public setCheckoutFlowRequired(Z)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasCheckoutFlowRequired:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->checkoutFlowRequired_:Z

    return-object p0
.end method

.method public setCurrencyCode(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasCurrencyCode:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->currencyCode_:Ljava/lang/String;

    return-object p0
.end method

.method public setFormattedAmount(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedAmount:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->formattedAmount_:Ljava/lang/String;

    return-object p0
.end method

.method public setFormattedDescription(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedDescription:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->formattedDescription_:Ljava/lang/String;

    return-object p0
.end method

.method public setFormattedFullAmount(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedFullAmount:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->formattedFullAmount_:Ljava/lang/String;

    return-object p0
.end method

.method public setFormattedName(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedName:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->formattedName_:Ljava/lang/String;

    return-object p0
.end method

.method public setFullPriceMicros(J)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFullPriceMicros:Z

    iput-wide p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->fullPriceMicros_:J

    return-object p0
.end method

.method public setLicensedOfferType(I)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasLicensedOfferType:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->licensedOfferType_:I

    return-object p0
.end method

.method public setMicros(J)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasMicros:Z

    iput-wide p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->micros_:J

    return-object p0
.end method

.method public setOfferType(I)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasOfferType:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->offerType_:I

    return-object p0
.end method

.method public setOnSaleDate(J)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasOnSaleDate:Z

    iput-wide p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->onSaleDate_:J

    return-object p0
.end method

.method public setOnSaleDateDisplayTimeZoneOffsetMsec(I)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasOnSaleDateDisplayTimeZoneOffsetMsec:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->onSaleDateDisplayTimeZoneOffsetMsec_:I

    return-object p0
.end method

.method public setPreorder(Z)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasPreorder:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->preorder_:Z

    return-object p0
.end method

.method public setRentalTerms(Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasRentalTerms:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->rentalTerms_:Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

    return-object p0
.end method

.method public setSubscriptionContentTerms(Lcom/google/android/finsky/remoting/protos/Common$SubscriptionContentTerms;)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Common$SubscriptionContentTerms;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasSubscriptionContentTerms:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->subscriptionContentTerms_:Lcom/google/android/finsky/remoting/protos/Common$SubscriptionContentTerms;

    return-object p0
.end method

.method public setSubscriptionTerms(Lcom/google/android/finsky/remoting/protos/Common$SubscriptionTerms;)Lcom/google/android/finsky/remoting/protos/Common$Offer;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Common$SubscriptionTerms;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasSubscriptionTerms:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Common$Offer;->subscriptionTerms_:Lcom/google/android/finsky/remoting/protos/Common$SubscriptionTerms;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasMicros()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getMicros()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasCurrencyCode()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getCurrencyCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedAmount()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getFormattedAmount()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getConvertedPriceList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/Common$Offer;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasCheckoutFlowRequired()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getCheckoutFlowRequired()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFullPriceMicros()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getFullPriceMicros()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedFullAmount()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getFormattedFullAmount()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasOfferType()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getOfferType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasRentalTerms()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getRentalTerms()Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasOnSaleDate()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getOnSaleDate()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getPromotionLabelList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_1

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasSubscriptionTerms()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getSubscriptionTerms()Lcom/google/android/finsky/remoting/protos/Common$SubscriptionTerms;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedName()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getFormattedName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasFormattedDescription()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getFormattedDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasPreorder()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getPreorder()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasOnSaleDateDisplayTimeZoneOffsetMsec()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getOnSaleDateDisplayTimeZoneOffsetMsec()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasLicensedOfferType()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getLicensedOfferType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->hasSubscriptionContentTerms()Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Common$Offer;->getSubscriptionContentTerms()Lcom/google/android/finsky/remoting/protos/Common$SubscriptionContentTerms;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_11
    return-void
.end method
