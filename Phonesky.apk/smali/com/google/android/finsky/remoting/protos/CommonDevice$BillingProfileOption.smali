.class public final Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "CommonDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/CommonDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BillingProfileOption"
.end annotation


# instance fields
.field private cachedSize:I

.field private carrierBillingInstrumentStatus_:Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;

.field private displayTitle_:Ljava/lang/String;

.field private externalInstrumentId_:Ljava/lang/String;

.field private hasCarrierBillingInstrumentStatus:Z

.field private hasDisplayTitle:Z

.field private hasExternalInstrumentId:Z

.field private hasTopupInfo:Z

.field private hasType:Z

.field private topupInfo_:Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;

.field private type_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->type_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->displayTitle_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->externalInstrumentId_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->topupInfo_:Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->carrierBillingInstrumentStatus_:Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->cachedSize:I

    return v0
.end method

.method public getCarrierBillingInstrumentStatus()Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->carrierBillingInstrumentStatus_:Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;

    return-object v0
.end method

.method public getDisplayTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->displayTitle_:Ljava/lang/String;

    return-object v0
.end method

.method public getExternalInstrumentId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->externalInstrumentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasType()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasDisplayTitle()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getDisplayTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasExternalInstrumentId()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getExternalInstrumentId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasTopupInfo()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getTopupInfo()Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasCarrierBillingInstrumentStatus()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getCarrierBillingInstrumentStatus()Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->cachedSize:I

    return v0
.end method

.method public getTopupInfo()Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->topupInfo_:Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->type_:I

    return v0
.end method

.method public hasCarrierBillingInstrumentStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasCarrierBillingInstrumentStatus:Z

    return v0
.end method

.method public hasDisplayTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasDisplayTitle:Z

    return v0
.end method

.method public hasExternalInstrumentId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasExternalInstrumentId:Z

    return v0
.end method

.method public hasTopupInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasTopupInfo:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasType:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->setType(I)Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->setDisplayTitle(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->setExternalInstrumentId(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->setTopupInfo(Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;)Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->setCarrierBillingInstrumentStatus(Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;)Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;

    move-result-object v0

    return-object v0
.end method

.method public setCarrierBillingInstrumentStatus(Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;)Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasCarrierBillingInstrumentStatus:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->carrierBillingInstrumentStatus_:Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;

    return-object p0
.end method

.method public setDisplayTitle(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasDisplayTitle:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->displayTitle_:Ljava/lang/String;

    return-object p0
.end method

.method public setExternalInstrumentId(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasExternalInstrumentId:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->externalInstrumentId_:Ljava/lang/String;

    return-object p0
.end method

.method public setTopupInfo(Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;)Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasTopupInfo:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->topupInfo_:Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;

    return-object p0
.end method

.method public setType(I)Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasType:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->type_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasType()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasDisplayTitle()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getDisplayTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasExternalInstrumentId()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getExternalInstrumentId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasTopupInfo()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getTopupInfo()Lcom/google/android/finsky/remoting/protos/CommonDevice$TopupInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->hasCarrierBillingInstrumentStatus()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/CommonDevice$BillingProfileOption;->getCarrierBillingInstrumentStatus()Lcom/google/android/finsky/remoting/protos/CommonDevice$CarrierBillingInstrumentStatus;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    return-void
.end method
