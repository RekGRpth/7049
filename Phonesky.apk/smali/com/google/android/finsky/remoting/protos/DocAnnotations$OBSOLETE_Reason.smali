.class public final Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "DocAnnotations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/DocAnnotations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OBSOLETE_Reason"
.end annotation


# instance fields
.field private briefReason_:Ljava/lang/String;

.field private cachedSize:I

.field private hasBriefReason:Z

.field private hasOBSOLETEDetailedReason:Z

.field private hasUniqueId:Z

.field private oBSOLETEDetailedReason_:Ljava/lang/String;

.field private uniqueId_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->briefReason_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->oBSOLETEDetailedReason_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->uniqueId_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBriefReason()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->briefReason_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->cachedSize:I

    return v0
.end method

.method public getOBSOLETEDetailedReason()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->oBSOLETEDetailedReason_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->hasBriefReason()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->getBriefReason()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->hasOBSOLETEDetailedReason()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->getOBSOLETEDetailedReason()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->hasUniqueId()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->cachedSize:I

    return v0
.end method

.method public getUniqueId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->uniqueId_:Ljava/lang/String;

    return-object v0
.end method

.method public hasBriefReason()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->hasBriefReason:Z

    return v0
.end method

.method public hasOBSOLETEDetailedReason()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->hasOBSOLETEDetailedReason:Z

    return v0
.end method

.method public hasUniqueId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->hasUniqueId:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->setBriefReason(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->setOBSOLETEDetailedReason(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->setUniqueId(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;

    move-result-object v0

    return-object v0
.end method

.method public setBriefReason(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->hasBriefReason:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->briefReason_:Ljava/lang/String;

    return-object p0
.end method

.method public setOBSOLETEDetailedReason(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->hasOBSOLETEDetailedReason:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->oBSOLETEDetailedReason_:Ljava/lang/String;

    return-object p0
.end method

.method public setUniqueId(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->hasUniqueId:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->uniqueId_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->hasBriefReason()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->getBriefReason()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->hasOBSOLETEDetailedReason()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->getOBSOLETEDetailedReason()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->hasUniqueId()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;->getUniqueId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    return-void
.end method
