.class public final Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "DocAnnotations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/DocAnnotations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Reason"
.end annotation


# instance fields
.field private cachedSize:I

.field private descriptionHtml_:Ljava/lang/String;

.field private dismissal_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$Dismissal;

.field private hasDescriptionHtml:Z

.field private hasDismissal:Z

.field private hasReasonPlusProfiles:Z

.field private hasReasonReview:Z

.field private reasonPlusProfiles_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonPlusProfiles;

.field private reasonReview_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonReview;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->dismissal_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$Dismissal;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->descriptionHtml_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->reasonPlusProfiles_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonPlusProfiles;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->reasonReview_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonReview;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->cachedSize:I

    return v0
.end method

.method public getDescriptionHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->descriptionHtml_:Ljava/lang/String;

    return-object v0
.end method

.method public getDismissal()Lcom/google/android/finsky/remoting/protos/DocAnnotations$Dismissal;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->dismissal_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$Dismissal;

    return-object v0
.end method

.method public getReasonPlusProfiles()Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonPlusProfiles;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->reasonPlusProfiles_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonPlusProfiles;

    return-object v0
.end method

.method public getReasonReview()Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonReview;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->reasonReview_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonReview;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasDescriptionHtml()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->getDescriptionHtml()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasReasonPlusProfiles()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->getReasonPlusProfiles()Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonPlusProfiles;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasReasonReview()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->getReasonReview()Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonReview;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasDismissal()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->getDismissal()Lcom/google/android/finsky/remoting/protos/DocAnnotations$Dismissal;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->cachedSize:I

    return v0
.end method

.method public hasDescriptionHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasDescriptionHtml:Z

    return v0
.end method

.method public hasDismissal()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasDismissal:Z

    return v0
.end method

.method public hasReasonPlusProfiles()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasReasonPlusProfiles:Z

    return v0
.end method

.method public hasReasonReview()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasReasonReview:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->setDescriptionHtml(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonPlusProfiles;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonPlusProfiles;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->setReasonPlusProfiles(Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonPlusProfiles;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonReview;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonReview;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->setReasonReview(Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonReview;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Dismissal;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Dismissal;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->setDismissal(Lcom/google/android/finsky/remoting/protos/DocAnnotations$Dismissal;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
        0x3a -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;

    move-result-object v0

    return-object v0
.end method

.method public setDescriptionHtml(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasDescriptionHtml:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->descriptionHtml_:Ljava/lang/String;

    return-object p0
.end method

.method public setDismissal(Lcom/google/android/finsky/remoting/protos/DocAnnotations$Dismissal;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$Dismissal;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasDismissal:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->dismissal_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$Dismissal;

    return-object p0
.end method

.method public setReasonPlusProfiles(Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonPlusProfiles;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonPlusProfiles;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasReasonPlusProfiles:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->reasonPlusProfiles_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonPlusProfiles;

    return-object p0
.end method

.method public setReasonReview(Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonReview;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonReview;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasReasonReview:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->reasonReview_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonReview;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasDescriptionHtml()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->getDescriptionHtml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasReasonPlusProfiles()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->getReasonPlusProfiles()Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonPlusProfiles;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasReasonReview()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->getReasonReview()Lcom/google/android/finsky/remoting/protos/DocAnnotations$ReasonReview;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasDismissal()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->getDismissal()Lcom/google/android/finsky/remoting/protos/DocAnnotations$Dismissal;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    return-void
.end method
