.class public final Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DocumentDetails"
.end annotation


# instance fields
.field private albumDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$AlbumDetails;

.field private appDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;

.field private artistDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistDetails;

.field private bookDetails_:Lcom/google/android/finsky/remoting/protos/BookInfo$BookDetails;

.field private cachedSize:I

.field private hasAlbumDetails:Z

.field private hasAppDetails:Z

.field private hasArtistDetails:Z

.field private hasBookDetails:Z

.field private hasMagazineDetails:Z

.field private hasNewsDetails:Z

.field private hasSongDetails:Z

.field private hasSubscriptionDetails:Z

.field private hasTvEpisodeDetails:Z

.field private hasTvSeasonDetails:Z

.field private hasTvShowDetails:Z

.field private hasVideoDetails:Z

.field private magazineDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$MagazineDetails;

.field private newsDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$NewsDetails;

.field private songDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;

.field private subscriptionDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$SubscriptionDetails;

.field private tvEpisodeDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;

.field private tvSeasonDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;

.field private tvShowDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$TvShowDetails;

.field private videoDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$VideoDetails;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->appDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->albumDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$AlbumDetails;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->artistDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistDetails;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->songDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->bookDetails_:Lcom/google/android/finsky/remoting/protos/BookInfo$BookDetails;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->videoDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$VideoDetails;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->subscriptionDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$SubscriptionDetails;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->magazineDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$MagazineDetails;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->tvShowDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$TvShowDetails;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->tvSeasonDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->tvEpisodeDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->newsDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$NewsDetails;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAlbumDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$AlbumDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->albumDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$AlbumDetails;

    return-object v0
.end method

.method public getAppDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->appDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;

    return-object v0
.end method

.method public getArtistDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->artistDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistDetails;

    return-object v0
.end method

.method public getBookDetails()Lcom/google/android/finsky/remoting/protos/BookInfo$BookDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->bookDetails_:Lcom/google/android/finsky/remoting/protos/BookInfo$BookDetails;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->cachedSize:I

    return v0
.end method

.method public getMagazineDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$MagazineDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->magazineDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$MagazineDetails;

    return-object v0
.end method

.method public getNewsDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$NewsDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->newsDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$NewsDetails;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasAppDetails()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getAppDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasAlbumDetails()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getAlbumDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$AlbumDetails;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasArtistDetails()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getArtistDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistDetails;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasSongDetails()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getSongDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasBookDetails()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getBookDetails()Lcom/google/android/finsky/remoting/protos/BookInfo$BookDetails;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasVideoDetails()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getVideoDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$VideoDetails;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasSubscriptionDetails()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getSubscriptionDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$SubscriptionDetails;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasMagazineDetails()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getMagazineDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$MagazineDetails;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasTvShowDetails()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getTvShowDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$TvShowDetails;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasTvSeasonDetails()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getTvSeasonDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasTvEpisodeDetails()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getTvEpisodeDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasNewsDetails()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getNewsDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$NewsDetails;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->cachedSize:I

    return v0
.end method

.method public getSongDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->songDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;

    return-object v0
.end method

.method public getSubscriptionDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$SubscriptionDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->subscriptionDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$SubscriptionDetails;

    return-object v0
.end method

.method public getTvEpisodeDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->tvEpisodeDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;

    return-object v0
.end method

.method public getTvSeasonDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->tvSeasonDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;

    return-object v0
.end method

.method public getTvShowDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$TvShowDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->tvShowDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$TvShowDetails;

    return-object v0
.end method

.method public getVideoDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$VideoDetails;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->videoDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$VideoDetails;

    return-object v0
.end method

.method public hasAlbumDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasAlbumDetails:Z

    return v0
.end method

.method public hasAppDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasAppDetails:Z

    return v0
.end method

.method public hasArtistDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasArtistDetails:Z

    return v0
.end method

.method public hasBookDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasBookDetails:Z

    return v0
.end method

.method public hasMagazineDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasMagazineDetails:Z

    return v0
.end method

.method public hasNewsDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasNewsDetails:Z

    return v0
.end method

.method public hasSongDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasSongDetails:Z

    return v0
.end method

.method public hasSubscriptionDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasSubscriptionDetails:Z

    return v0
.end method

.method public hasTvEpisodeDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasTvEpisodeDetails:Z

    return v0
.end method

.method public hasTvSeasonDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasTvSeasonDetails:Z

    return v0
.end method

.method public hasTvShowDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasTvShowDetails:Z

    return v0
.end method

.method public hasVideoDetails()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasVideoDetails:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->setAppDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocDetails$AlbumDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$AlbumDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->setAlbumDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$AlbumDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->setArtistDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->setSongDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/android/finsky/remoting/protos/BookInfo$BookDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/BookInfo$BookDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->setBookDetails(Lcom/google/android/finsky/remoting/protos/BookInfo$BookDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocDetails$VideoDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$VideoDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->setVideoDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$VideoDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocDetails$SubscriptionDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$SubscriptionDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->setSubscriptionDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$SubscriptionDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocDetails$MagazineDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$MagazineDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->setMagazineDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$MagazineDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocDetails$TvShowDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvShowDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->setTvShowDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$TvShowDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->setTvSeasonDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;

    goto/16 :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->setTvEpisodeDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;

    goto/16 :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocDetails$NewsDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$NewsDetails;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->setNewsDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$NewsDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;

    move-result-object v0

    return-object v0
.end method

.method public setAlbumDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$AlbumDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocDetails$AlbumDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasAlbumDetails:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->albumDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$AlbumDetails;

    return-object p0
.end method

.method public setAppDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasAppDetails:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->appDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;

    return-object p0
.end method

.method public setArtistDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasArtistDetails:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->artistDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistDetails;

    return-object p0
.end method

.method public setBookDetails(Lcom/google/android/finsky/remoting/protos/BookInfo$BookDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/BookInfo$BookDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasBookDetails:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->bookDetails_:Lcom/google/android/finsky/remoting/protos/BookInfo$BookDetails;

    return-object p0
.end method

.method public setMagazineDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$MagazineDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocDetails$MagazineDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasMagazineDetails:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->magazineDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$MagazineDetails;

    return-object p0
.end method

.method public setNewsDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$NewsDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocDetails$NewsDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasNewsDetails:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->newsDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$NewsDetails;

    return-object p0
.end method

.method public setSongDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasSongDetails:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->songDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;

    return-object p0
.end method

.method public setSubscriptionDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$SubscriptionDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocDetails$SubscriptionDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasSubscriptionDetails:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->subscriptionDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$SubscriptionDetails;

    return-object p0
.end method

.method public setTvEpisodeDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasTvEpisodeDetails:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->tvEpisodeDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;

    return-object p0
.end method

.method public setTvSeasonDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasTvSeasonDetails:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->tvSeasonDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;

    return-object p0
.end method

.method public setTvShowDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$TvShowDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocDetails$TvShowDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasTvShowDetails:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->tvShowDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$TvShowDetails;

    return-object p0
.end method

.method public setVideoDetails(Lcom/google/android/finsky/remoting/protos/DocDetails$VideoDetails;)Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocDetails$VideoDetails;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasVideoDetails:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->videoDetails_:Lcom/google/android/finsky/remoting/protos/DocDetails$VideoDetails;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasAppDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getAppDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$AppDetails;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasAlbumDetails()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getAlbumDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$AlbumDetails;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasArtistDetails()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getArtistDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$ArtistDetails;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasSongDetails()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getSongDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$SongDetails;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasBookDetails()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getBookDetails()Lcom/google/android/finsky/remoting/protos/BookInfo$BookDetails;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasVideoDetails()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getVideoDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$VideoDetails;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasSubscriptionDetails()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getSubscriptionDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$SubscriptionDetails;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasMagazineDetails()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getMagazineDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$MagazineDetails;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasTvShowDetails()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getTvShowDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$TvShowDetails;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasTvSeasonDetails()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getTvSeasonDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$TvSeasonDetails;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasTvEpisodeDetails()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getTvEpisodeDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->hasNewsDetails()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocDetails$DocumentDetails;->getNewsDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$NewsDetails;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_b
    return-void
.end method
