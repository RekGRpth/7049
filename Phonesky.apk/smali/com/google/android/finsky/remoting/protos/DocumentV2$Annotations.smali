.class public final Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Annotations"
.end annotation


# instance fields
.field private badgeForCreator_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;",
            ">;"
        }
    .end annotation
.end field

.field private badgeForDoc_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private hasLink:Z

.field private hasOBSOLETEReason:Z

.field private hasOfferNote:Z

.field private hasPlusOneData:Z

.field private hasPrivacyPolicyUrl:Z

.field private hasSectionBodyOfWork:Z

.field private hasSectionCoreContent:Z

.field private hasSectionCrossSell:Z

.field private hasSectionMoreBy:Z

.field private hasSectionRelated:Z

.field private hasSectionRelatedDocType:Z

.field private hasSuggestionReasons:Z

.field private hasTemplate:Z

.field private link_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$Link;

.field private oBSOLETEReason_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;

.field private offerNote_:Ljava/lang/String;

.field private plusOneData_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$PlusOneData;

.field private privacyPolicyUrl_:Ljava/lang/String;

.field private promotedDoc_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/DocAnnotations$PromotedDoc;",
            ">;"
        }
    .end annotation
.end field

.field private sectionBodyOfWork_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

.field private sectionCoreContent_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

.field private sectionCrossSell_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

.field private sectionMoreBy_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

.field private sectionRelatedDocType_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

.field private sectionRelated_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

.field private subscription_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/DocumentV2$DocV2;",
            ">;"
        }
    .end annotation
.end field

.field private suggestionReasons_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;

.field private template_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$Template;

.field private warning_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/DocAnnotations$Warning;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionRelated_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionRelatedDocType_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionMoreBy_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionBodyOfWork_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionCoreContent_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionCrossSell_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->plusOneData_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$PlusOneData;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->warning_:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->link_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$Link;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->template_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$Template;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->badgeForCreator_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->badgeForDoc_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->promotedDoc_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->offerNote_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->subscription_:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->suggestionReasons_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->oBSOLETEReason_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->privacyPolicyUrl_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addBadgeForCreator(Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->badgeForCreator_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->badgeForCreator_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->badgeForCreator_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addBadgeForDoc(Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->badgeForDoc_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->badgeForDoc_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->badgeForDoc_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addPromotedDoc(Lcom/google/android/finsky/remoting/protos/DocAnnotations$PromotedDoc;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$PromotedDoc;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->promotedDoc_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->promotedDoc_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->promotedDoc_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSubscription(Lcom/google/android/finsky/remoting/protos/DocumentV2$DocV2;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocumentV2$DocV2;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->subscription_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->subscription_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->subscription_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addWarning(Lcom/google/android/finsky/remoting/protos/DocAnnotations$Warning;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$Warning;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->warning_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->warning_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->warning_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getBadgeForCreator(I)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->badgeForCreator_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;

    return-object v0
.end method

.method public getBadgeForCreatorCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->badgeForCreator_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getBadgeForCreatorList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->badgeForCreator_:Ljava/util/List;

    return-object v0
.end method

.method public getBadgeForDoc(I)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->badgeForDoc_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;

    return-object v0
.end method

.method public getBadgeForDocCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->badgeForDoc_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getBadgeForDocList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->badgeForDoc_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->cachedSize:I

    return v0
.end method

.method public getLink()Lcom/google/android/finsky/remoting/protos/DocAnnotations$Link;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->link_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$Link;

    return-object v0
.end method

.method public getOBSOLETEReason()Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->oBSOLETEReason_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;

    return-object v0
.end method

.method public getOfferNote()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->offerNote_:Ljava/lang/String;

    return-object v0
.end method

.method public getPlusOneData()Lcom/google/android/finsky/remoting/protos/DocAnnotations$PlusOneData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->plusOneData_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$PlusOneData;

    return-object v0
.end method

.method public getPrivacyPolicyUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->privacyPolicyUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getPromotedDocList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/DocAnnotations$PromotedDoc;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->promotedDoc_:Ljava/util/List;

    return-object v0
.end method

.method public getSectionBodyOfWork()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionBodyOfWork_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    return-object v0
.end method

.method public getSectionCoreContent()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionCoreContent_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    return-object v0
.end method

.method public getSectionCrossSell()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionCrossSell_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    return-object v0
.end method

.method public getSectionMoreBy()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionMoreBy_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    return-object v0
.end method

.method public getSectionRelated()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionRelated_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    return-object v0
.end method

.method public getSectionRelatedDocType()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionRelatedDocType_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionRelated()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSectionRelated()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionMoreBy()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSectionMoreBy()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasPlusOneData()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getPlusOneData()Lcom/google/android/finsky/remoting/protos/DocAnnotations$PlusOneData;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getWarningList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Warning;

    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionBodyOfWork()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSectionBodyOfWork()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionCoreContent()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSectionCoreContent()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasTemplate()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getTemplate()Lcom/google/android/finsky/remoting/protos/DocAnnotations$Template;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getBadgeForCreatorList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;

    const/16 v3, 0x8

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getBadgeForDocList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;

    const/16 v3, 0x9

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_2

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasLink()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getLink()Lcom/google/android/finsky/remoting/protos/DocAnnotations$Link;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionCrossSell()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSectionCrossSell()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionRelatedDocType()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSectionRelatedDocType()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getPromotedDocList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$PromotedDoc;

    const/16 v3, 0xd

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_3

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasOfferNote()Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v3, 0xe

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getOfferNote()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSubscriptionList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/DocumentV2$DocV2;

    const/16 v3, 0x10

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_4

    :cond_e
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasOBSOLETEReason()Z

    move-result v3

    if-eqz v3, :cond_f

    const/16 v3, 0x11

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getOBSOLETEReason()Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl()Z

    move-result v3

    if-eqz v3, :cond_10

    const/16 v3, 0x12

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getPrivacyPolicyUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_10
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSuggestionReasons()Z

    move-result v3

    if-eqz v3, :cond_11

    const/16 v3, 0x13

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSuggestionReasons()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_11
    iput v2, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->cachedSize:I

    return v2
.end method

.method public getSubscriptionCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->subscription_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSubscriptionList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/DocumentV2$DocV2;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->subscription_:Ljava/util/List;

    return-object v0
.end method

.method public getSuggestionReasons()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->suggestionReasons_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;

    return-object v0
.end method

.method public getTemplate()Lcom/google/android/finsky/remoting/protos/DocAnnotations$Template;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->template_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$Template;

    return-object v0
.end method

.method public getWarning(I)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Warning;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->warning_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Warning;

    return-object v0
.end method

.method public getWarningCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->warning_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getWarningList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/remoting/protos/DocAnnotations$Warning;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->warning_:Ljava/util/List;

    return-object v0
.end method

.method public hasLink()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasLink:Z

    return v0
.end method

.method public hasOBSOLETEReason()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasOBSOLETEReason:Z

    return v0
.end method

.method public hasOfferNote()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasOfferNote:Z

    return v0
.end method

.method public hasPlusOneData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasPlusOneData:Z

    return v0
.end method

.method public hasPrivacyPolicyUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl:Z

    return v0
.end method

.method public hasSectionBodyOfWork()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionBodyOfWork:Z

    return v0
.end method

.method public hasSectionCoreContent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionCoreContent:Z

    return v0
.end method

.method public hasSectionCrossSell()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionCrossSell:Z

    return v0
.end method

.method public hasSectionMoreBy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionMoreBy:Z

    return v0
.end method

.method public hasSectionRelated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionRelated:Z

    return v0
.end method

.method public hasSectionRelatedDocType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionRelatedDocType:Z

    return v0
.end method

.method public hasSuggestionReasons()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSuggestionReasons:Z

    return v0
.end method

.method public hasTemplate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasTemplate:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->setSectionRelated(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->setSectionMoreBy(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$PlusOneData;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$PlusOneData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->setPlusOneData(Lcom/google/android/finsky/remoting/protos/DocAnnotations$PlusOneData;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Warning;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Warning;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->addWarning(Lcom/google/android/finsky/remoting/protos/DocAnnotations$Warning;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->setSectionBodyOfWork(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->setSectionCoreContent(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Template;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Template;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->setTemplate(Lcom/google/android/finsky/remoting/protos/DocAnnotations$Template;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->addBadgeForCreator(Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->addBadgeForDoc(Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Link;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Link;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->setLink(Lcom/google/android/finsky/remoting/protos/DocAnnotations$Link;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto/16 :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->setSectionCrossSell(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto/16 :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->setSectionRelatedDocType(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto/16 :goto_0

    :sswitch_d
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$PromotedDoc;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$PromotedDoc;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->addPromotedDoc(Lcom/google/android/finsky/remoting/protos/DocAnnotations$PromotedDoc;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->setOfferNote(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto/16 :goto_0

    :sswitch_f
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocumentV2$DocV2;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$DocV2;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->addSubscription(Lcom/google/android/finsky/remoting/protos/DocumentV2$DocV2;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto/16 :goto_0

    :sswitch_10
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->setOBSOLETEReason(Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->setPrivacyPolicyUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto/16 :goto_0

    :sswitch_12
    new-instance v1, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->setSuggestionReasons(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;

    move-result-object v0

    return-object v0
.end method

.method public setLink(Lcom/google/android/finsky/remoting/protos/DocAnnotations$Link;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$Link;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasLink:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->link_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$Link;

    return-object p0
.end method

.method public setOBSOLETEReason(Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasOBSOLETEReason:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->oBSOLETEReason_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;

    return-object p0
.end method

.method public setOfferNote(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasOfferNote:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->offerNote_:Ljava/lang/String;

    return-object p0
.end method

.method public setPlusOneData(Lcom/google/android/finsky/remoting/protos/DocAnnotations$PlusOneData;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$PlusOneData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasPlusOneData:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->plusOneData_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$PlusOneData;

    return-object p0
.end method

.method public setPrivacyPolicyUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->privacyPolicyUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setSectionBodyOfWork(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionBodyOfWork:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionBodyOfWork_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    return-object p0
.end method

.method public setSectionCoreContent(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionCoreContent:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionCoreContent_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    return-object p0
.end method

.method public setSectionCrossSell(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionCrossSell:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionCrossSell_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    return-object p0
.end method

.method public setSectionMoreBy(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionMoreBy:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionMoreBy_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    return-object p0
.end method

.method public setSectionRelated(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionRelated:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionRelated_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    return-object p0
.end method

.method public setSectionRelatedDocType(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionRelatedDocType:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->sectionRelatedDocType_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    return-object p0
.end method

.method public setSuggestionReasons(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSuggestionReasons:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->suggestionReasons_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;

    return-object p0
.end method

.method public setTemplate(Lcom/google/android/finsky/remoting/protos/DocAnnotations$Template;)Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$Template;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasTemplate:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->template_:Lcom/google/android/finsky/remoting/protos/DocAnnotations$Template;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionRelated()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSectionRelated()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionMoreBy()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSectionMoreBy()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasPlusOneData()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getPlusOneData()Lcom/google/android/finsky/remoting/protos/DocAnnotations$PlusOneData;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getWarningList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Warning;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionBodyOfWork()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSectionBodyOfWork()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionCoreContent()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSectionCoreContent()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasTemplate()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getTemplate()Lcom/google/android/finsky/remoting/protos/DocAnnotations$Template;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getBadgeForCreatorList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;

    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getBadgeForDocList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;

    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_2

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasLink()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getLink()Lcom/google/android/finsky/remoting/protos/DocAnnotations$Link;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionCrossSell()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSectionCrossSell()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSectionRelatedDocType()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSectionRelatedDocType()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SectionMetadata;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getPromotedDocList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/DocAnnotations$PromotedDoc;

    const/16 v2, 0xd

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_3

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasOfferNote()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getOfferNote()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSubscriptionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/DocumentV2$DocV2;

    const/16 v2, 0x10

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_4

    :cond_e
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasOBSOLETEReason()Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getOBSOLETEReason()Lcom/google/android/finsky/remoting/protos/DocAnnotations$OBSOLETE_Reason;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x12

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getPrivacyPolicyUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->hasSuggestionReasons()Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x13

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocumentV2$Annotations;->getSuggestionReasons()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_11
    return-void
.end method
