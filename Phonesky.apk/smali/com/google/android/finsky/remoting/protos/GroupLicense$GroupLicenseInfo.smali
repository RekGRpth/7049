.class public final Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "GroupLicense.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/GroupLicense;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GroupLicenseInfo"
.end annotation


# instance fields
.field private cachedSize:I

.field private gaiaGroupId_:J

.field private hasGaiaGroupId:Z

.field private hasLicensedOfferType:Z

.field private licensedOfferType_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->licensedOfferType_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->gaiaGroupId_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->cachedSize:I

    return v0
.end method

.method public getGaiaGroupId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->gaiaGroupId_:J

    return-wide v0
.end method

.method public getLicensedOfferType()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->licensedOfferType_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->hasLicensedOfferType()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->getLicensedOfferType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->hasGaiaGroupId()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->getGaiaGroupId()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeFixed64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->cachedSize:I

    return v0
.end method

.method public hasGaiaGroupId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->hasGaiaGroupId:Z

    return v0
.end method

.method public hasLicensedOfferType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->hasLicensedOfferType:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->setLicensedOfferType(I)Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readFixed64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->setGaiaGroupId(J)Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;

    move-result-object v0

    return-object v0
.end method

.method public setGaiaGroupId(J)Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->hasGaiaGroupId:Z

    iput-wide p1, p0, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->gaiaGroupId_:J

    return-object p0
.end method

.method public setLicensedOfferType(I)Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->hasLicensedOfferType:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->licensedOfferType_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->hasLicensedOfferType()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->getLicensedOfferType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->hasGaiaGroupId()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;->getGaiaGroupId()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeFixed64(IJ)V

    :cond_1
    return-void
.end method
