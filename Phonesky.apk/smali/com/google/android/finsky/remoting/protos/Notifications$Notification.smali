.class public final Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Notifications.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/Notifications;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Notification"
.end annotation


# instance fields
.field private appData_:Lcom/google/android/finsky/remoting/protos/Notifications$AndroidAppNotificationData;

.field private appDeliveryData_:Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;

.field private cachedSize:I

.field private docTitle_:Ljava/lang/String;

.field private docid_:Lcom/google/android/finsky/remoting/protos/Common$Docid;

.field private hasAppData:Z

.field private hasAppDeliveryData:Z

.field private hasDocTitle:Z

.field private hasDocid:Z

.field private hasInAppNotificationData:Z

.field private hasLibraryDirtyData:Z

.field private hasLibraryUpdate:Z

.field private hasNotificationId:Z

.field private hasNotificationType:Z

.field private hasPurchaseDeclinedData:Z

.field private hasPurchaseRemovalData:Z

.field private hasTimestamp:Z

.field private hasUserEmail:Z

.field private hasUserNotificationData:Z

.field private inAppNotificationData_:Lcom/google/android/finsky/remoting/protos/Notifications$InAppNotificationData;

.field private libraryDirtyData_:Lcom/google/android/finsky/remoting/protos/Notifications$LibraryDirtyData;

.field private libraryUpdate_:Lcom/google/android/finsky/remoting/protos/Library$LibraryUpdate;

.field private notificationId_:Ljava/lang/String;

.field private notificationType_:I

.field private purchaseDeclinedData_:Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseDeclinedData;

.field private purchaseRemovalData_:Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseRemovalData;

.field private timestamp_:J

.field private userEmail_:Ljava/lang/String;

.field private userNotificationData_:Lcom/google/android/finsky/remoting/protos/Notifications$UserNotificationData;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->notificationType_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->notificationId_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->timestamp_:J

    iput-object v2, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->docid_:Lcom/google/android/finsky/remoting/protos/Common$Docid;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->docTitle_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->userEmail_:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->libraryUpdate_:Lcom/google/android/finsky/remoting/protos/Library$LibraryUpdate;

    iput-object v2, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->appData_:Lcom/google/android/finsky/remoting/protos/Notifications$AndroidAppNotificationData;

    iput-object v2, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->appDeliveryData_:Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    iput-object v2, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->purchaseRemovalData_:Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseRemovalData;

    iput-object v2, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->purchaseDeclinedData_:Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseDeclinedData;

    iput-object v2, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->userNotificationData_:Lcom/google/android/finsky/remoting/protos/Notifications$UserNotificationData;

    iput-object v2, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->inAppNotificationData_:Lcom/google/android/finsky/remoting/protos/Notifications$InAppNotificationData;

    iput-object v2, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->libraryDirtyData_:Lcom/google/android/finsky/remoting/protos/Notifications$LibraryDirtyData;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->cachedSize:I

    return-void
.end method

.method public static parseFrom([B)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    invoke-direct {v0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    check-cast v0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    return-object v0
.end method


# virtual methods
.method public getAppData()Lcom/google/android/finsky/remoting/protos/Notifications$AndroidAppNotificationData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->appData_:Lcom/google/android/finsky/remoting/protos/Notifications$AndroidAppNotificationData;

    return-object v0
.end method

.method public getAppDeliveryData()Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->appDeliveryData_:Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->cachedSize:I

    return v0
.end method

.method public getDocTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->docTitle_:Ljava/lang/String;

    return-object v0
.end method

.method public getDocid()Lcom/google/android/finsky/remoting/protos/Common$Docid;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->docid_:Lcom/google/android/finsky/remoting/protos/Common$Docid;

    return-object v0
.end method

.method public getInAppNotificationData()Lcom/google/android/finsky/remoting/protos/Notifications$InAppNotificationData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->inAppNotificationData_:Lcom/google/android/finsky/remoting/protos/Notifications$InAppNotificationData;

    return-object v0
.end method

.method public getLibraryDirtyData()Lcom/google/android/finsky/remoting/protos/Notifications$LibraryDirtyData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->libraryDirtyData_:Lcom/google/android/finsky/remoting/protos/Notifications$LibraryDirtyData;

    return-object v0
.end method

.method public getLibraryUpdate()Lcom/google/android/finsky/remoting/protos/Library$LibraryUpdate;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->libraryUpdate_:Lcom/google/android/finsky/remoting/protos/Library$LibraryUpdate;

    return-object v0
.end method

.method public getNotificationId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->notificationId_:Ljava/lang/String;

    return-object v0
.end method

.method public getNotificationType()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->notificationType_:I

    return v0
.end method

.method public getPurchaseDeclinedData()Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseDeclinedData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->purchaseDeclinedData_:Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseDeclinedData;

    return-object v0
.end method

.method public getPurchaseRemovalData()Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseRemovalData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->purchaseRemovalData_:Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseRemovalData;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasNotificationType()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getNotificationType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasTimestamp()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getTimestamp()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasDocid()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getDocid()Lcom/google/android/finsky/remoting/protos/Common$Docid;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasDocTitle()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getDocTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasUserEmail()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getUserEmail()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasAppData()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getAppData()Lcom/google/android/finsky/remoting/protos/Notifications$AndroidAppNotificationData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasAppDeliveryData()Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getAppDeliveryData()Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasPurchaseRemovalData()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getPurchaseRemovalData()Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseRemovalData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasUserNotificationData()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getUserNotificationData()Lcom/google/android/finsky/remoting/protos/Notifications$UserNotificationData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasInAppNotificationData()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getInAppNotificationData()Lcom/google/android/finsky/remoting/protos/Notifications$InAppNotificationData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasPurchaseDeclinedData()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getPurchaseDeclinedData()Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseDeclinedData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasNotificationId()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getNotificationId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasLibraryUpdate()Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v1, 0xe

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getLibraryUpdate()Lcom/google/android/finsky/remoting/protos/Library$LibraryUpdate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasLibraryDirtyData()Z

    move-result v1

    if-eqz v1, :cond_d

    const/16 v1, 0xf

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getLibraryDirtyData()Lcom/google/android/finsky/remoting/protos/Notifications$LibraryDirtyData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->cachedSize:I

    return v0
.end method

.method public getTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->timestamp_:J

    return-wide v0
.end method

.method public getUserEmail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->userEmail_:Ljava/lang/String;

    return-object v0
.end method

.method public getUserNotificationData()Lcom/google/android/finsky/remoting/protos/Notifications$UserNotificationData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->userNotificationData_:Lcom/google/android/finsky/remoting/protos/Notifications$UserNotificationData;

    return-object v0
.end method

.method public hasAppData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasAppData:Z

    return v0
.end method

.method public hasAppDeliveryData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasAppDeliveryData:Z

    return v0
.end method

.method public hasDocTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasDocTitle:Z

    return v0
.end method

.method public hasDocid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasDocid:Z

    return v0
.end method

.method public hasInAppNotificationData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasInAppNotificationData:Z

    return v0
.end method

.method public hasLibraryDirtyData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasLibraryDirtyData:Z

    return v0
.end method

.method public hasLibraryUpdate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasLibraryUpdate:Z

    return v0
.end method

.method public hasNotificationId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasNotificationId:Z

    return v0
.end method

.method public hasNotificationType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasNotificationType:Z

    return v0
.end method

.method public hasPurchaseDeclinedData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasPurchaseDeclinedData:Z

    return v0
.end method

.method public hasPurchaseRemovalData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasPurchaseRemovalData:Z

    return v0
.end method

.method public hasTimestamp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasTimestamp:Z

    return v0
.end method

.method public hasUserEmail()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasUserEmail:Z

    return v0
.end method

.method public hasUserNotificationData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasUserNotificationData:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->setNotificationType(I)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->setTimestamp(J)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Common$Docid;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Common$Docid;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->setDocid(Lcom/google/android/finsky/remoting/protos/Common$Docid;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->setDocTitle(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->setUserEmail(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Notifications$AndroidAppNotificationData;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Notifications$AndroidAppNotificationData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->setAppData(Lcom/google/android/finsky/remoting/protos/Notifications$AndroidAppNotificationData;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->setAppDeliveryData(Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseRemovalData;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseRemovalData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->setPurchaseRemovalData(Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseRemovalData;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Notifications$UserNotificationData;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Notifications$UserNotificationData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->setUserNotificationData(Lcom/google/android/finsky/remoting/protos/Notifications$UserNotificationData;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Notifications$InAppNotificationData;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Notifications$InAppNotificationData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->setInAppNotificationData(Lcom/google/android/finsky/remoting/protos/Notifications$InAppNotificationData;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    goto :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseDeclinedData;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseDeclinedData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->setPurchaseDeclinedData(Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseDeclinedData;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->setNotificationId(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    goto/16 :goto_0

    :sswitch_d
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Library$LibraryUpdate;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Library$LibraryUpdate;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->setLibraryUpdate(Lcom/google/android/finsky/remoting/protos/Library$LibraryUpdate;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    goto/16 :goto_0

    :sswitch_e
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Notifications$LibraryDirtyData;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Notifications$LibraryDirtyData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->setLibraryDirtyData(Lcom/google/android/finsky/remoting/protos/Notifications$LibraryDirtyData;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;

    move-result-object v0

    return-object v0
.end method

.method public setAppData(Lcom/google/android/finsky/remoting/protos/Notifications$AndroidAppNotificationData;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Notifications$AndroidAppNotificationData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasAppData:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->appData_:Lcom/google/android/finsky/remoting/protos/Notifications$AndroidAppNotificationData;

    return-object p0
.end method

.method public setAppDeliveryData(Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasAppDeliveryData:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->appDeliveryData_:Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    return-object p0
.end method

.method public setDocTitle(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasDocTitle:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->docTitle_:Ljava/lang/String;

    return-object p0
.end method

.method public setDocid(Lcom/google/android/finsky/remoting/protos/Common$Docid;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Common$Docid;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasDocid:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->docid_:Lcom/google/android/finsky/remoting/protos/Common$Docid;

    return-object p0
.end method

.method public setInAppNotificationData(Lcom/google/android/finsky/remoting/protos/Notifications$InAppNotificationData;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Notifications$InAppNotificationData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasInAppNotificationData:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->inAppNotificationData_:Lcom/google/android/finsky/remoting/protos/Notifications$InAppNotificationData;

    return-object p0
.end method

.method public setLibraryDirtyData(Lcom/google/android/finsky/remoting/protos/Notifications$LibraryDirtyData;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Notifications$LibraryDirtyData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasLibraryDirtyData:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->libraryDirtyData_:Lcom/google/android/finsky/remoting/protos/Notifications$LibraryDirtyData;

    return-object p0
.end method

.method public setLibraryUpdate(Lcom/google/android/finsky/remoting/protos/Library$LibraryUpdate;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Library$LibraryUpdate;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasLibraryUpdate:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->libraryUpdate_:Lcom/google/android/finsky/remoting/protos/Library$LibraryUpdate;

    return-object p0
.end method

.method public setNotificationId(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasNotificationId:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->notificationId_:Ljava/lang/String;

    return-object p0
.end method

.method public setNotificationType(I)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasNotificationType:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->notificationType_:I

    return-object p0
.end method

.method public setPurchaseDeclinedData(Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseDeclinedData;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseDeclinedData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasPurchaseDeclinedData:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->purchaseDeclinedData_:Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseDeclinedData;

    return-object p0
.end method

.method public setPurchaseRemovalData(Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseRemovalData;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseRemovalData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasPurchaseRemovalData:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->purchaseRemovalData_:Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseRemovalData;

    return-object p0
.end method

.method public setTimestamp(J)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasTimestamp:Z

    iput-wide p1, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->timestamp_:J

    return-object p0
.end method

.method public setUserEmail(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasUserEmail:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->userEmail_:Ljava/lang/String;

    return-object p0
.end method

.method public setUserNotificationData(Lcom/google/android/finsky/remoting/protos/Notifications$UserNotificationData;)Lcom/google/android/finsky/remoting/protos/Notifications$Notification;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Notifications$UserNotificationData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasUserNotificationData:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->userNotificationData_:Lcom/google/android/finsky/remoting/protos/Notifications$UserNotificationData;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasNotificationType()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getNotificationType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasTimestamp()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getTimestamp()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasDocid()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getDocid()Lcom/google/android/finsky/remoting/protos/Common$Docid;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasDocTitle()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getDocTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasUserEmail()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getUserEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasAppData()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getAppData()Lcom/google/android/finsky/remoting/protos/Notifications$AndroidAppNotificationData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasAppDeliveryData()Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getAppDeliveryData()Lcom/google/android/finsky/remoting/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasPurchaseRemovalData()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getPurchaseRemovalData()Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseRemovalData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasUserNotificationData()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getUserNotificationData()Lcom/google/android/finsky/remoting/protos/Notifications$UserNotificationData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasInAppNotificationData()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getInAppNotificationData()Lcom/google/android/finsky/remoting/protos/Notifications$InAppNotificationData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasPurchaseDeclinedData()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getPurchaseDeclinedData()Lcom/google/android/finsky/remoting/protos/Notifications$PurchaseDeclinedData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasNotificationId()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getNotificationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasLibraryUpdate()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getLibraryUpdate()Lcom/google/android/finsky/remoting/protos/Library$LibraryUpdate;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->hasLibraryDirtyData()Z

    move-result v0

    if-eqz v0, :cond_d

    const/16 v0, 0xf

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Notifications$Notification;->getLibraryDirtyData()Lcom/google/android/finsky/remoting/protos/Notifications$LibraryDirtyData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_d
    return-void
.end method
