.class public final Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Ownership.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/Ownership;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OwnershipInfo"
.end annotation


# instance fields
.field private autoRenewing_:Z

.field private cachedSize:I

.field private developerPurchaseInfo_:Lcom/google/android/finsky/remoting/protos/Common$SignedData;

.field private groupLicenseInfo_:Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;

.field private hasAutoRenewing:Z

.field private hasDeveloperPurchaseInfo:Z

.field private hasGroupLicenseInfo:Z

.field private hasHidden:Z

.field private hasInitiationTimestampMsec:Z

.field private hasLicensedDocumentInfo:Z

.field private hasPostDeliveryRefundWindowMsec:Z

.field private hasPreordered:Z

.field private hasQuantity:Z

.field private hasRefundTimeoutTimestampMsec:Z

.field private hasRentalTerms:Z

.field private hasValidUntilTimestampMsec:Z

.field private hidden_:Z

.field private initiationTimestampMsec_:J

.field private licensedDocumentInfo_:Lcom/google/android/finsky/remoting/protos/GroupLicense$LicensedDocumentInfo;

.field private postDeliveryRefundWindowMsec_:J

.field private preordered_:Z

.field private quantity_:I

.field private refundTimeoutTimestampMsec_:J

.field private rentalTerms_:Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

.field private validUntilTimestampMsec_:J


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-wide v1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->initiationTimestampMsec_:J

    iput-wide v1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->validUntilTimestampMsec_:J

    iput-boolean v3, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->autoRenewing_:Z

    iput-wide v1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->refundTimeoutTimestampMsec_:J

    iput-wide v1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->postDeliveryRefundWindowMsec_:J

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->developerPurchaseInfo_:Lcom/google/android/finsky/remoting/protos/Common$SignedData;

    iput-boolean v3, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->preordered_:Z

    iput-boolean v3, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hidden_:Z

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->rentalTerms_:Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->groupLicenseInfo_:Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->licensedDocumentInfo_:Lcom/google/android/finsky/remoting/protos/GroupLicense$LicensedDocumentInfo;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->quantity_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAutoRenewing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->autoRenewing_:Z

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->cachedSize:I

    return v0
.end method

.method public getDeveloperPurchaseInfo()Lcom/google/android/finsky/remoting/protos/Common$SignedData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->developerPurchaseInfo_:Lcom/google/android/finsky/remoting/protos/Common$SignedData;

    return-object v0
.end method

.method public getGroupLicenseInfo()Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->groupLicenseInfo_:Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;

    return-object v0
.end method

.method public getHidden()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hidden_:Z

    return v0
.end method

.method public getInitiationTimestampMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->initiationTimestampMsec_:J

    return-wide v0
.end method

.method public getLicensedDocumentInfo()Lcom/google/android/finsky/remoting/protos/GroupLicense$LicensedDocumentInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->licensedDocumentInfo_:Lcom/google/android/finsky/remoting/protos/GroupLicense$LicensedDocumentInfo;

    return-object v0
.end method

.method public getPostDeliveryRefundWindowMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->postDeliveryRefundWindowMsec_:J

    return-wide v0
.end method

.method public getPreordered()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->preordered_:Z

    return v0
.end method

.method public getQuantity()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->quantity_:I

    return v0
.end method

.method public getRefundTimeoutTimestampMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->refundTimeoutTimestampMsec_:J

    return-wide v0
.end method

.method public getRentalTerms()Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->rentalTerms_:Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasInitiationTimestampMsec()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getInitiationTimestampMsec()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasValidUntilTimestampMsec()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getValidUntilTimestampMsec()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasAutoRenewing()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getAutoRenewing()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasRefundTimeoutTimestampMsec()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getRefundTimeoutTimestampMsec()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasPostDeliveryRefundWindowMsec()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getPostDeliveryRefundWindowMsec()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasDeveloperPurchaseInfo()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getDeveloperPurchaseInfo()Lcom/google/android/finsky/remoting/protos/Common$SignedData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasPreordered()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getPreordered()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasHidden()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getHidden()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasRentalTerms()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getRentalTerms()Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasGroupLicenseInfo()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getGroupLicenseInfo()Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasLicensedDocumentInfo()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getLicensedDocumentInfo()Lcom/google/android/finsky/remoting/protos/GroupLicense$LicensedDocumentInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasQuantity()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getQuantity()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->cachedSize:I

    return v0
.end method

.method public getValidUntilTimestampMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->validUntilTimestampMsec_:J

    return-wide v0
.end method

.method public hasAutoRenewing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasAutoRenewing:Z

    return v0
.end method

.method public hasDeveloperPurchaseInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasDeveloperPurchaseInfo:Z

    return v0
.end method

.method public hasGroupLicenseInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasGroupLicenseInfo:Z

    return v0
.end method

.method public hasHidden()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasHidden:Z

    return v0
.end method

.method public hasInitiationTimestampMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasInitiationTimestampMsec:Z

    return v0
.end method

.method public hasLicensedDocumentInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasLicensedDocumentInfo:Z

    return v0
.end method

.method public hasPostDeliveryRefundWindowMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasPostDeliveryRefundWindowMsec:Z

    return v0
.end method

.method public hasPreordered()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasPreordered:Z

    return v0
.end method

.method public hasQuantity()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasQuantity:Z

    return v0
.end method

.method public hasRefundTimeoutTimestampMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasRefundTimeoutTimestampMsec:Z

    return v0
.end method

.method public hasRentalTerms()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasRentalTerms:Z

    return v0
.end method

.method public hasValidUntilTimestampMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasValidUntilTimestampMsec:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->setInitiationTimestampMsec(J)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->setValidUntilTimestampMsec(J)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->setAutoRenewing(Z)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->setRefundTimeoutTimestampMsec(J)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->setPostDeliveryRefundWindowMsec(J)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Common$SignedData;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Common$SignedData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->setDeveloperPurchaseInfo(Lcom/google/android/finsky/remoting/protos/Common$SignedData;)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->setPreordered(Z)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->setHidden(Z)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->setRentalTerms(Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->setGroupLicenseInfo(Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;

    goto :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/android/finsky/remoting/protos/GroupLicense$LicensedDocumentInfo;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/GroupLicense$LicensedDocumentInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->setLicensedDocumentInfo(Lcom/google/android/finsky/remoting/protos/GroupLicense$LicensedDocumentInfo;)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->setQuantity(I)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;

    move-result-object v0

    return-object v0
.end method

.method public setAutoRenewing(Z)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasAutoRenewing:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->autoRenewing_:Z

    return-object p0
.end method

.method public setDeveloperPurchaseInfo(Lcom/google/android/finsky/remoting/protos/Common$SignedData;)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Common$SignedData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasDeveloperPurchaseInfo:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->developerPurchaseInfo_:Lcom/google/android/finsky/remoting/protos/Common$SignedData;

    return-object p0
.end method

.method public setGroupLicenseInfo(Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasGroupLicenseInfo:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->groupLicenseInfo_:Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;

    return-object p0
.end method

.method public setHidden(Z)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasHidden:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hidden_:Z

    return-object p0
.end method

.method public setInitiationTimestampMsec(J)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasInitiationTimestampMsec:Z

    iput-wide p1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->initiationTimestampMsec_:J

    return-object p0
.end method

.method public setLicensedDocumentInfo(Lcom/google/android/finsky/remoting/protos/GroupLicense$LicensedDocumentInfo;)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/GroupLicense$LicensedDocumentInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasLicensedDocumentInfo:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->licensedDocumentInfo_:Lcom/google/android/finsky/remoting/protos/GroupLicense$LicensedDocumentInfo;

    return-object p0
.end method

.method public setPostDeliveryRefundWindowMsec(J)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasPostDeliveryRefundWindowMsec:Z

    iput-wide p1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->postDeliveryRefundWindowMsec_:J

    return-object p0
.end method

.method public setPreordered(Z)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasPreordered:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->preordered_:Z

    return-object p0
.end method

.method public setQuantity(I)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasQuantity:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->quantity_:I

    return-object p0
.end method

.method public setRefundTimeoutTimestampMsec(J)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasRefundTimeoutTimestampMsec:Z

    iput-wide p1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->refundTimeoutTimestampMsec_:J

    return-object p0
.end method

.method public setRentalTerms(Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasRentalTerms:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->rentalTerms_:Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

    return-object p0
.end method

.method public setValidUntilTimestampMsec(J)Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasValidUntilTimestampMsec:Z

    iput-wide p1, p0, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->validUntilTimestampMsec_:J

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasInitiationTimestampMsec()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getInitiationTimestampMsec()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasValidUntilTimestampMsec()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getValidUntilTimestampMsec()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasAutoRenewing()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getAutoRenewing()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasRefundTimeoutTimestampMsec()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getRefundTimeoutTimestampMsec()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasPostDeliveryRefundWindowMsec()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getPostDeliveryRefundWindowMsec()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasDeveloperPurchaseInfo()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getDeveloperPurchaseInfo()Lcom/google/android/finsky/remoting/protos/Common$SignedData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasPreordered()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getPreordered()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasHidden()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getHidden()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasRentalTerms()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getRentalTerms()Lcom/google/android/finsky/remoting/protos/Common$RentalTerms;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasGroupLicenseInfo()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getGroupLicenseInfo()Lcom/google/android/finsky/remoting/protos/GroupLicense$GroupLicenseInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasLicensedDocumentInfo()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getLicensedDocumentInfo()Lcom/google/android/finsky/remoting/protos/GroupLicense$LicensedDocumentInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->hasQuantity()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Ownership$OwnershipInfo;->getQuantity()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_b
    return-void
.end method
