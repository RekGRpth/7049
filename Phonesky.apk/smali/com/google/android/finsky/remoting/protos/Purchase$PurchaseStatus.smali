.class public final Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Purchase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/Purchase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PurchaseStatus"
.end annotation


# instance fields
.field private cachedSize:I

.field private errorMessageHtml_:Ljava/lang/String;

.field private hasErrorMessageHtml:Z

.field private hasPermissionError:Z

.field private hasStatusCode:Z

.field private permissionError_:I

.field private statusCode_:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->statusCode_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->errorMessageHtml_:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->permissionError_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->cachedSize:I

    return v0
.end method

.method public getErrorMessageHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->errorMessageHtml_:Ljava/lang/String;

    return-object v0
.end method

.method public getPermissionError()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->permissionError_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->hasStatusCode()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->getStatusCode()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->hasErrorMessageHtml()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->getErrorMessageHtml()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->hasPermissionError()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->getPermissionError()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->cachedSize:I

    return v0
.end method

.method public getStatusCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->statusCode_:I

    return v0
.end method

.method public hasErrorMessageHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->hasErrorMessageHtml:Z

    return v0
.end method

.method public hasPermissionError()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->hasPermissionError:Z

    return v0
.end method

.method public hasStatusCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->hasStatusCode:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->setStatusCode(I)Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->setErrorMessageHtml(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->setPermissionError(I)Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;

    move-result-object v0

    return-object v0
.end method

.method public setErrorMessageHtml(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->hasErrorMessageHtml:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->errorMessageHtml_:Ljava/lang/String;

    return-object p0
.end method

.method public setPermissionError(I)Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->hasPermissionError:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->permissionError_:I

    return-object p0
.end method

.method public setStatusCode(I)Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->hasStatusCode:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->statusCode_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->hasStatusCode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->getStatusCode()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->hasErrorMessageHtml()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->getErrorMessageHtml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->hasPermissionError()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Purchase$PurchaseStatus;->getPermissionError()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    return-void
.end method
