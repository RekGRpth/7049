.class public final Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Rev.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/Rev;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReviewResponse"
.end annotation


# instance fields
.field private cachedSize:I

.field private getResponse_:Lcom/google/android/finsky/remoting/protos/Rev$GetReviewsResponse;

.field private hasGetResponse:Z

.field private hasNextPageUrl:Z

.field private hasUpdatedReview:Z

.field private nextPageUrl_:Ljava/lang/String;

.field private updatedReview_:Lcom/google/android/finsky/remoting/protos/Rev$Review;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->getResponse_:Lcom/google/android/finsky/remoting/protos/Rev$GetReviewsResponse;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->nextPageUrl_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->updatedReview_:Lcom/google/android/finsky/remoting/protos/Rev$Review;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->cachedSize:I

    return v0
.end method

.method public getGetResponse()Lcom/google/android/finsky/remoting/protos/Rev$GetReviewsResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->getResponse_:Lcom/google/android/finsky/remoting/protos/Rev$GetReviewsResponse;

    return-object v0
.end method

.method public getNextPageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->nextPageUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->hasGetResponse()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->getGetResponse()Lcom/google/android/finsky/remoting/protos/Rev$GetReviewsResponse;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->hasNextPageUrl()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->getNextPageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->hasUpdatedReview()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->getUpdatedReview()Lcom/google/android/finsky/remoting/protos/Rev$Review;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->cachedSize:I

    return v0
.end method

.method public getUpdatedReview()Lcom/google/android/finsky/remoting/protos/Rev$Review;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->updatedReview_:Lcom/google/android/finsky/remoting/protos/Rev$Review;

    return-object v0
.end method

.method public hasGetResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->hasGetResponse:Z

    return v0
.end method

.method public hasNextPageUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->hasNextPageUrl:Z

    return v0
.end method

.method public hasUpdatedReview()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->hasUpdatedReview:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Rev$GetReviewsResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Rev$GetReviewsResponse;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->setGetResponse(Lcom/google/android/finsky/remoting/protos/Rev$GetReviewsResponse;)Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->setNextPageUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/finsky/remoting/protos/Rev$Review;

    invoke-direct {v1}, Lcom/google/android/finsky/remoting/protos/Rev$Review;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->setUpdatedReview(Lcom/google/android/finsky/remoting/protos/Rev$Review;)Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;

    move-result-object v0

    return-object v0
.end method

.method public setGetResponse(Lcom/google/android/finsky/remoting/protos/Rev$GetReviewsResponse;)Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Rev$GetReviewsResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->hasGetResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->getResponse_:Lcom/google/android/finsky/remoting/protos/Rev$GetReviewsResponse;

    return-object p0
.end method

.method public setNextPageUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->hasNextPageUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->nextPageUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setUpdatedReview(Lcom/google/android/finsky/remoting/protos/Rev$Review;)Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;
    .locals 1
    .param p1    # Lcom/google/android/finsky/remoting/protos/Rev$Review;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->hasUpdatedReview:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->updatedReview_:Lcom/google/android/finsky/remoting/protos/Rev$Review;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->hasGetResponse()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->getGetResponse()Lcom/google/android/finsky/remoting/protos/Rev$GetReviewsResponse;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->hasNextPageUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->getNextPageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->hasUpdatedReview()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Rev$ReviewResponse;->getUpdatedReview()Lcom/google/android/finsky/remoting/protos/Rev$Review;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    return-void
.end method
