.class public final Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Toc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/remoting/protos/Toc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CarrierBillingConfig"
.end annotation


# instance fields
.field private apiVersion_:I

.field private cachedSize:I

.field private credentialsUrl_:Ljava/lang/String;

.field private hasApiVersion:Z

.field private hasCredentialsUrl:Z

.field private hasId:Z

.field private hasName:Z

.field private hasPerTransactionCredentialsRequired:Z

.field private hasProvisioningUrl:Z

.field private hasSendSubscriberIdWithCarrierBillingRequests:Z

.field private hasTosRequired:Z

.field private id_:Ljava/lang/String;

.field private name_:Ljava/lang/String;

.field private perTransactionCredentialsRequired_:Z

.field private provisioningUrl_:Ljava/lang/String;

.field private sendSubscriberIdWithCarrierBillingRequests_:Z

.field private tosRequired_:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->id_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->name_:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->apiVersion_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->provisioningUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->credentialsUrl_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->tosRequired_:Z

    iput-boolean v1, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->perTransactionCredentialsRequired_:Z

    iput-boolean v1, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->sendSubscriberIdWithCarrierBillingRequests_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getApiVersion()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->apiVersion_:I

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->cachedSize:I

    return v0
.end method

.method public getCredentialsUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->credentialsUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getPerTransactionCredentialsRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->perTransactionCredentialsRequired_:Z

    return v0
.end method

.method public getProvisioningUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->provisioningUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getSendSubscriberIdWithCarrierBillingRequests()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->sendSubscriberIdWithCarrierBillingRequests_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasId()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasName()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasApiVersion()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getApiVersion()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasProvisioningUrl()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getProvisioningUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasCredentialsUrl()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getCredentialsUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasTosRequired()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getTosRequired()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasPerTransactionCredentialsRequired()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getPerTransactionCredentialsRequired()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasSendSubscriberIdWithCarrierBillingRequests()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getSendSubscriberIdWithCarrierBillingRequests()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->cachedSize:I

    return v0
.end method

.method public getTosRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->tosRequired_:Z

    return v0
.end method

.method public hasApiVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasApiVersion:Z

    return v0
.end method

.method public hasCredentialsUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasCredentialsUrl:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasId:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasName:Z

    return v0
.end method

.method public hasPerTransactionCredentialsRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasPerTransactionCredentialsRequired:Z

    return v0
.end method

.method public hasProvisioningUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasProvisioningUrl:Z

    return v0
.end method

.method public hasSendSubscriberIdWithCarrierBillingRequests()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasSendSubscriberIdWithCarrierBillingRequests:Z

    return v0
.end method

.method public hasTosRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasTosRequired:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->setId(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->setName(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->setApiVersion(I)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->setProvisioningUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->setCredentialsUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->setTosRequired(Z)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->setPerTransactionCredentialsRequired(Z)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->setSendSubscriberIdWithCarrierBillingRequests(Z)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;

    move-result-object v0

    return-object v0
.end method

.method public setApiVersion(I)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasApiVersion:Z

    iput p1, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->apiVersion_:I

    return-object p0
.end method

.method public setCredentialsUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasCredentialsUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->credentialsUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasId:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->id_:Ljava/lang/String;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasName:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public setPerTransactionCredentialsRequired(Z)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasPerTransactionCredentialsRequired:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->perTransactionCredentialsRequired_:Z

    return-object p0
.end method

.method public setProvisioningUrl(Ljava/lang/String;)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasProvisioningUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->provisioningUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setSendSubscriberIdWithCarrierBillingRequests(Z)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasSendSubscriberIdWithCarrierBillingRequests:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->sendSubscriberIdWithCarrierBillingRequests_:Z

    return-object p0
.end method

.method public setTosRequired(Z)Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasTosRequired:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->tosRequired_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasName()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasApiVersion()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getApiVersion()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasProvisioningUrl()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getProvisioningUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasCredentialsUrl()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getCredentialsUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasTosRequired()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getTosRequired()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasPerTransactionCredentialsRequired()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getPerTransactionCredentialsRequired()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->hasSendSubscriberIdWithCarrierBillingRequests()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/Toc$CarrierBillingConfig;->getSendSubscriberIdWithCarrierBillingRequests()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_7
    return-void
.end method
