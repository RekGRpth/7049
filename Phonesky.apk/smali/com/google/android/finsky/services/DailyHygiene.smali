.class public Lcom/google/android/finsky/services/DailyHygiene;
.super Landroid/app/Service;
.source "DailyHygiene.java"


# static fields
.field public static final BOOT_DELAY_MS:J

.field public static final IMMEDIATE_DELAY_MS:J

.field public static final REGULAR_INTERVAL_MS:J

.field private static final RETRY_INTERVALS:[I

.field public static final RETRY_INTERVAL_MS:J

.field private static final UPDATE_CHECK:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneImmediateDelayMs:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/finsky/services/DailyHygiene;->IMMEDIATE_DELAY_MS:J

    sget-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneBootDelayMs:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/finsky/services/DailyHygiene;->BOOT_DELAY_MS:J

    sget-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneRegularIntervalMs:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/finsky/services/DailyHygiene;->REGULAR_INTERVAL_MS:J

    sget-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneRetryIntervalMs:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/finsky/services/DailyHygiene;->RETRY_INTERVAL_MS:J

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/finsky/services/DailyHygiene;->RETRY_INTERVALS:[I

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/services/DailyHygiene;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sput-object v0, Lcom/google/android/finsky/services/DailyHygiene;->UPDATE_CHECK:Landroid/content/Intent;

    return-void

    :array_0
    .array-data 4
        0x1
        0x3
        0x9
        0x1b
        0x51
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/services/DailyHygiene;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/services/DailyHygiene;

    invoke-direct {p0}, Lcom/google/android/finsky/services/DailyHygiene;->onLoaded()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/services/DailyHygiene;Z)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/services/DailyHygiene;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/DailyHygiene;->flushEventLogs(Z)V

    return-void
.end method

.method private flushEventLogs(Z)V
    .locals 9
    .param p1    # Z

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/finsky/api/AccountHandler;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    move-object v2, v1

    array-length v4, v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v0, v2, v3

    const-string v5, "Flushing event logs for %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/DailyHygiene;->reschedule(Z)V

    return-void
.end method

.method public static goMakeHygieneIfDirty(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/android/finsky/services/DailyHygiene;->needsDailyHygiene()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/finsky/services/DailyHygiene;->hasDatabaseVersionChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "Dirty, need more hygiene."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-wide v0, Lcom/google/android/finsky/services/DailyHygiene;->IMMEDIATE_DELAY_MS:J

    invoke-static {p0, v0, v1}, Lcom/google/android/finsky/services/DailyHygiene;->schedule(Landroid/content/Context;J)V

    :goto_0
    return-void

    :cond_1
    const-string v0, "No need to run daily hygiene."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static hasDatabaseVersionChanged()Z
    .locals 3

    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->lastReplicatedDatabaseVersion:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {}, Lcom/google/android/finsky/library/SQLiteLibrary;->getVersion()I

    move-result v0

    if-eq v1, v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static needsDailyHygiene()Z
    .locals 6

    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneLastSuccessMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v4, Lcom/google/android/finsky/services/DailyHygiene;->REGULAR_INTERVAL_MS:J

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onLoaded()V
    .locals 8

    invoke-static {}, Lcom/google/android/finsky/services/ContentSyncService;->get()Lcom/google/android/finsky/services/ContentSyncService$Facade;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/finsky/services/ContentSyncService$Facade;->scheduleSync()V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    new-instance v0, Lcom/google/android/finsky/appstate/UpdateChecker;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v3

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstallPolicies()Lcom/google/android/finsky/installer/InstallPolicies;

    move-result-object v5

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v6

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v7

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/appstate/UpdateChecker;-><init>(Landroid/content/Context;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/api/DfeApiProvider;Lcom/google/android/finsky/installer/InstallPolicies;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/utils/Notifier;)V

    invoke-virtual {v2}, Lcom/google/android/finsky/library/Libraries;->cleanup()V

    new-instance v1, Lcom/google/android/finsky/services/DailyHygiene$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/services/DailyHygiene$2;-><init>(Lcom/google/android/finsky/services/DailyHygiene;)V

    new-instance v3, Lcom/google/android/finsky/services/DailyHygiene$3;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/services/DailyHygiene$3;-><init>(Lcom/google/android/finsky/services/DailyHygiene;)V

    invoke-virtual {v0, v1, v3}, Lcom/google/android/finsky/appstate/UpdateChecker;->checkForUpdates(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    return-void
.end method

.method private reschedule(Z)V
    .locals 11
    .param p1    # Z

    const/4 v10, 0x1

    const/4 v9, 0x0

    if-eqz p1, :cond_1

    sget-object v3, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneLastSuccessMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v3}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    sget-wide v3, Lcom/google/android/finsky/services/DailyHygiene;->REGULAR_INTERVAL_MS:J

    long-to-double v3, v3

    const-wide/high16 v5, 0x3fe0000000000000L

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v7

    add-double/2addr v5, v7

    mul-double/2addr v3, v5

    double-to-long v1, v3

    const-string v3, "Scheduling first run in %1.1f hours"

    new-array v4, v10, [Ljava/lang/Object;

    long-to-float v5, v1

    const v6, 0x4a5bba00

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    sget-object v3, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneLastSuccessMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    sget-object v3, Lcom/google/android/finsky/utils/FinskyPreferences;->lastReplicatedDatabaseVersion:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {}, Lcom/google/android/finsky/library/SQLiteLibrary;->getVersion()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    :goto_1
    invoke-static {p0, v1, v2}, Lcom/google/android/finsky/services/DailyHygiene;->schedule(Landroid/content/Context;J)V

    invoke-virtual {p0}, Lcom/google/android/finsky/services/DailyHygiene;->stopSelf()V

    return-void

    :cond_0
    sget-wide v1, Lcom/google/android/finsky/services/DailyHygiene;->REGULAR_INTERVAL_MS:J

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneFailedCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v3}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v0, v3, 0x1

    sget-object v3, Lcom/google/android/finsky/services/DailyHygiene;->RETRY_INTERVALS:[I

    array-length v3, v3

    if-gt v0, v3, :cond_2

    sget-object v3, Lcom/google/android/finsky/services/DailyHygiene;->RETRY_INTERVALS:[I

    add-int/lit8 v4, v0, -0x1

    aget v3, v3, v4

    int-to-long v3, v3

    sget-wide v5, Lcom/google/android/finsky/services/DailyHygiene;->RETRY_INTERVAL_MS:J

    mul-long v1, v3, v5

    const-string v3, "Scheduling new run in %d minutes (failures=%d)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const-wide/32 v5, 0xea60

    div-long v5, v1, v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v10

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v3, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneFailedCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    const-string v3, "Giving up. (failures=%d)"

    new-array v4, v10, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v3, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneFailedCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v3}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    sget-wide v1, Lcom/google/android/finsky/services/DailyHygiene;->REGULAR_INTERVAL_MS:J

    goto :goto_1
.end method

.method public static schedule(Landroid/content/Context;J)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # J

    const/4 v4, 0x0

    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    add-long/2addr v1, p1

    sget-object v3, Lcom/google/android/finsky/services/DailyHygiene;->UPDATE_CHECK:Landroid/content/Intent;

    invoke-static {p0, v4, v3, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    new-instance v0, Lcom/google/android/finsky/services/DailyHygiene$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/services/DailyHygiene$1;-><init>(Lcom/google/android/finsky/services/DailyHygiene;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/library/Libraries;->load(Ljava/lang/Runnable;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraryReplicators()Lcom/google/android/finsky/library/LibraryReplicators;

    move-result-object v1

    const-string v2, "daily-hygiene"

    invoke-interface {v1, v0, v2}, Lcom/google/android/finsky/library/LibraryReplicators;->replicateAllAccounts(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/appstate/AppStates;->load(Ljava/lang/Runnable;)Z

    const/4 v1, 0x2

    return v1
.end method
