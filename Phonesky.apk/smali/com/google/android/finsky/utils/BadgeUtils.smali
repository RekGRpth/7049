.class public Lcom/google/android/finsky/utils/BadgeUtils;
.super Ljava/lang/Object;
.source "BadgeUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static configureCreatorBadge(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/layout/DecoratedTextView;I)V
    .locals 5
    .param p0    # Lcom/google/android/finsky/api/model/Document;
    .param p1    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p2    # Lcom/google/android/finsky/layout/DecoratedTextView;
    .param p3    # I

    const/4 v4, 0x0

    const/4 v3, -0x1

    if-eq p3, v3, :cond_1

    invoke-virtual {p2, p3}, Lcom/google/android/finsky/layout/DecoratedTextView;->loadDecoration(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasCreatorBadges()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getFirstCreatorBadge()Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/finsky/layout/DecoratedTextView;->getTextSize()F

    move-result v3

    float-to-int v1, v3

    invoke-virtual {p2}, Lcom/google/android/finsky/layout/DecoratedTextView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x6

    invoke-static {v3, v2, v4, v1}, Lcom/google/android/finsky/utils/BadgeUtils;->getImageUrl(Landroid/content/Context;Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/finsky/layout/DecoratedTextView;->loadDecoration(Lcom/google/android/finsky/utils/BitmapLoader;Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p2, v4, v4, v4, v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static configureItemBadge(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/layout/DecoratedTextView;I)V
    .locals 5
    .param p0    # Lcom/google/android/finsky/api/model/Document;
    .param p1    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p2    # Lcom/google/android/finsky/layout/DecoratedTextView;
    .param p3    # I

    const/4 v4, 0x0

    const/4 v3, -0x1

    if-eq p3, v3, :cond_1

    invoke-virtual {p2, p3}, Lcom/google/android/finsky/layout/DecoratedTextView;->loadDecoration(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasItemBadges()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getFirstItemBadge()Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/finsky/layout/DecoratedTextView;->getTextSize()F

    move-result v3

    float-to-int v1, v3

    invoke-virtual {p2}, Lcom/google/android/finsky/layout/DecoratedTextView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x6

    invoke-static {v3, v2, v4, v1}, Lcom/google/android/finsky/utils/BadgeUtils;->getImageUrl(Landroid/content/Context;Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/finsky/layout/DecoratedTextView;->loadDecoration(Lcom/google/android/finsky/utils/BitmapLoader;Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p2, v4, v4, v4, v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static configureRatingItemSection(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Landroid/widget/RatingBar;Lcom/google/android/finsky/layout/DecoratedTextView;)V
    .locals 9
    .param p0    # Lcom/google/android/finsky/api/model/Document;
    .param p1    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p2    # Landroid/widget/RatingBar;
    .param p3    # Lcom/google/android/finsky/layout/DecoratedTextView;

    const/4 v8, 0x0

    if-eqz p2, :cond_0

    const/4 v4, 0x4

    invoke-virtual {p2, v4}, Landroid/widget/RatingBar;->setVisibility(I)V

    :cond_0
    if-eqz p3, :cond_1

    const/16 v4, 0x8

    invoke-virtual {p3, v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    :cond_1
    if-eqz p3, :cond_4

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasItemBadges()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p3, v8}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getFirstItemBadge()Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;

    move-result-object v2

    invoke-virtual {p3}, Lcom/google/android/finsky/layout/DecoratedTextView;->getTextSize()F

    move-result v4

    float-to-int v1, v4

    invoke-virtual {p3}, Lcom/google/android/finsky/layout/DecoratedTextView;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x6

    invoke-static {v4, v2, v5, v1}, Lcom/google/android/finsky/utils/BadgeUtils;->getImageUrl(Landroid/content/Context;Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p3, p1, v0, v1}, Lcom/google/android/finsky/layout/DecoratedTextView;->loadDecoration(Lcom/google/android/finsky/utils/BitmapLoader;Ljava/lang/String;I)V

    :cond_2
    invoke-virtual {v2}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f0a0056

    invoke-virtual {p3, v4, v8}, Lcom/google/android/finsky/layout/DecoratedTextView;->setContentColorStateListId(IZ)V

    invoke-virtual {p3}, Lcom/google/android/finsky/layout/DecoratedTextView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p3}, Lcom/google/android/finsky/layout/DecoratedTextView;->getPaddingBottom()I

    move-result v5

    invoke-virtual {p3, v8, v4, v8, v5}, Lcom/google/android/finsky/layout/DecoratedTextView;->setPadding(IIII)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    if-eqz p3, :cond_6

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasCensoring()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasReleaseType()Z

    move-result v4

    if-eqz v4, :cond_6

    :cond_5
    invoke-static {p0, p3}, Lcom/google/android/finsky/utils/BadgeUtils;->configureTipperSticker(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/DecoratedTextView;)V

    invoke-virtual {p3}, Lcom/google/android/finsky/layout/DecoratedTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0065

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p3}, Lcom/google/android/finsky/layout/DecoratedTextView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p3}, Lcom/google/android/finsky/layout/DecoratedTextView;->getPaddingBottom()I

    move-result v5

    invoke-virtual {p3, v3, v4, v3, v5}, Lcom/google/android/finsky/layout/DecoratedTextView;->setPadding(IIII)V

    goto :goto_0

    :cond_6
    if-eqz p3, :cond_7

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    const/16 v5, 0x14

    if-ne v4, v5, :cond_7

    invoke-static {p0, p3}, Lcom/google/android/finsky/utils/BadgeUtils;->configureReleaseDate(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/DecoratedTextView;)V

    invoke-virtual {p3}, Lcom/google/android/finsky/layout/DecoratedTextView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p3}, Lcom/google/android/finsky/layout/DecoratedTextView;->getPaddingBottom()I

    move-result v5

    invoke-virtual {p3, v8, v4, v8, v5}, Lcom/google/android/finsky/layout/DecoratedTextView;->setPadding(IIII)V

    goto :goto_0

    :cond_7
    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasRating()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getRatingCount()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getStarRating()F

    move-result v4

    invoke-virtual {p2, v4}, Landroid/widget/RatingBar;->setRating(F)V

    invoke-virtual {p2, v8}, Landroid/widget/RatingBar;->setVisibility(I)V

    goto :goto_0
.end method

.method private static configureReleaseDate(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/DecoratedTextView;)V
    .locals 2
    .param p0    # Lcom/google/android/finsky/api/model/Document;
    .param p1    # Lcom/google/android/finsky/layout/DecoratedTextView;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getTvEpisodeDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getTvEpisodeDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;->hasReleaseDate()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    const v0, 0x7f0a0058

    invoke-virtual {p1, v0, v1}, Lcom/google/android/finsky/layout/DecoratedTextView;->setContentColorStateListId(IZ)V

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getTvEpisodeDetails()Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/DocDetails$TvEpisodeDetails;->getReleaseDate()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public static configureTipperSticker(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/DecoratedTextView;)V
    .locals 6
    .param p0    # Lcom/google/android/finsky/api/model/Document;
    .param p1    # Lcom/google/android/finsky/layout/DecoratedTextView;

    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v2, -0x1

    const v0, 0x7f0a0028

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasCensoring()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getCensoring()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    if-ne v2, v5, :cond_1

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasReleaseType()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getReleaseType()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    :cond_1
    :goto_1
    if-le v2, v5, :cond_2

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/google/android/finsky/layout/DecoratedTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x1

    invoke-virtual {p1, v0, v3}, Lcom/google/android/finsky/layout/DecoratedTextView;->setContentColorId(IZ)V

    invoke-virtual {p1, v4, v4, v4, v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :cond_2
    return-void

    :pswitch_0
    const v2, 0x7f070292

    const v0, 0x7f0a0027

    goto :goto_0

    :pswitch_1
    const v2, 0x7f070293

    goto :goto_0

    :pswitch_2
    const v2, 0x7f070295

    goto :goto_1

    :pswitch_3
    const v2, 0x7f070296

    goto :goto_1

    :pswitch_4
    const v2, 0x7f070294

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getImageUrl(Landroid/content/Context;Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;II)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Badge;->getImageList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/remoting/protos/Doc$Image;

    invoke-virtual {v1}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getImageType()I

    move-result v3

    if-ne v3, p2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getImageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/finsky/remoting/protos/Doc$Image;->getSupportsFifeUrlOptions()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    return-object v2

    :cond_1
    invoke-static {p0, v2, p3, p3}, Lcom/google/android/finsky/utils/ThumbnailUtils;->buildFifeUrlWithScaling(Landroid/content/Context;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
