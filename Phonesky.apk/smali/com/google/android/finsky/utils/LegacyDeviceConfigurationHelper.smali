.class public Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;
.super Ljava/lang/Object;
.source "LegacyDeviceConfigurationHelper.java"


# static fields
.field private static sDeviceConfiguration:Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static customizeDeviceConfiguration(Landroid/content/Context;Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;)V
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/pm/PackageManager;->getSystemAvailableFeatures()[Landroid/content/pm/FeatureInfo;

    move-result-object v5

    if-eqz v5, :cond_1

    move-object v0, v5

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    iget-object v6, v1, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, v1, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v6}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->addSystemAvailableFeature(Ljava/lang/String;)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    sget-object v6, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {p1, v6}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->addNativePlatform(Ljava/lang/String;)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    sget-object v6, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    const-string v7, "unknown"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    sget-object v6, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-virtual {p1, v6}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->addNativePlatform(Ljava/lang/String;)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    :cond_2
    return-void
.end method

.method public static declared-synchronized getDeviceConfiguration()Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;
    .locals 18

    const-class v16, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;

    monitor-enter v16

    :try_start_0
    sget-object v15, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    if-nez v15, :cond_5

    new-instance v15, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    invoke-direct {v15}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;-><init>()V

    sput-object v15, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    const-string v15, "activity"

    invoke-virtual {v5, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    invoke-virtual {v1}, Landroid/app/ActivityManager;->getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;

    move-result-object v3

    invoke-static {v5}, Lcom/google/android/finsky/utils/VendingUtils;->getScreenDimensions(Landroid/content/Context;)Landroid/util/Pair;

    move-result-object v13

    const-string v15, "window"

    invoke-virtual {v5, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/view/WindowManager;

    new-instance v12, Landroid/util/DisplayMetrics;

    invoke-direct {v12}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v14}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v15

    invoke-virtual {v15, v12}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    sget-object v15, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    iget v0, v3, Landroid/content/pm/ConfigurationInfo;->reqTouchScreen:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->getTouchScreenId(I)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->setTouchScreen(I)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    move-result-object v15

    iget v0, v3, Landroid/content/pm/ConfigurationInfo;->reqKeyboardType:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->getKeyboardConfigId(I)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->setKeyboard(I)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    move-result-object v15

    iget v0, v3, Landroid/content/pm/ConfigurationInfo;->reqNavigation:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->getNavigationId(I)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->setNavigation(I)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    move-result-object v15

    iget v0, v3, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->setGlEsVersion(I)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    move-result-object v17

    iget-object v15, v13, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->setScreenWidth(I)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    move-result-object v17

    iget-object v15, v13, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->setScreenHeight(I)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    move-result-object v15

    iget v0, v12, Landroid/util/DisplayMetrics;->densityDpi:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->setScreenDensity(I)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    sget-object v17, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    iget v15, v3, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    and-int/lit8 v15, v15, 0x1

    if-lez v15, :cond_0

    const/4 v15, 0x1

    :goto_0
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->setHasHardKeyboard(Z)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    sget-object v17, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    iget v15, v3, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    and-int/lit8 v15, v15, 0x2

    if-lez v15, :cond_1

    const/4 v15, 0x1

    :goto_1
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->setHasFiveWayNavigation(Z)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    sget-object v15, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    iget v0, v4, Landroid/content/res/Configuration;->screenLayout:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->getScreenLayoutSizeId(I)I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->setScreenLayout(I)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/pm/PackageManager;->getSystemSharedLibraryNames()[Ljava/lang/String;

    move-result-object v2

    array-length v9, v2

    const/4 v8, 0x0

    :goto_2
    if-ge v8, v9, :cond_2

    aget-object v10, v2, v8

    sget-object v15, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    invoke-virtual {v15, v10}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->addSystemSharedLibrary(Ljava/lang/String;)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_0
    const/4 v15, 0x0

    goto :goto_0

    :cond_1
    const/4 v15, 0x0

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/android/finsky/FinskyApp;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/res/AssetManager;->getLocales()[Ljava/lang/String;

    move-result-object v2

    array-length v9, v2

    const/4 v8, 0x0

    :goto_3
    if-ge v8, v9, :cond_3

    aget-object v11, v2, v8

    sget-object v15, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    invoke-virtual {v15, v11}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->addSystemSupportedLocale(Ljava/lang/String;)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    :cond_3
    new-instance v7, Lcom/google/android/finsky/utils/GlExtensionReader;

    invoke-direct {v7}, Lcom/google/android/finsky/utils/GlExtensionReader;-><init>()V

    invoke-virtual {v7}, Lcom/google/android/finsky/utils/GlExtensionReader;->getGlExtensions()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    sget-object v15, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    invoke-virtual {v15, v6}, Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;->addGlExtension(Ljava/lang/String;)Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v15

    monitor-exit v16

    throw v15

    :cond_4
    :try_start_1
    sget-object v15, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;

    invoke-static {v5, v15}, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->customizeDeviceConfiguration(Landroid/content/Context;Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;)V

    :cond_5
    sget-object v15, Lcom/google/android/finsky/utils/LegacyDeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/vending/remoting/protos/DeviceConfigurationProto;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v16

    return-object v15
.end method

.method private static getKeyboardConfigId(I)I
    .locals 1
    .param p0    # I

    const/4 v0, 0x0

    packed-switch p0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static getNavigationId(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static getScreenLayoutSizeId(I)I
    .locals 2
    .param p0    # I

    and-int/lit8 v0, p0, 0xf

    packed-switch v0, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static getTouchScreenId(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
