.class public Lcom/google/android/finsky/utils/PlayUtils;
.super Ljava/lang/Object;
.source "PlayUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static findHighestPriorityReason(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;
    .locals 1
    .param p0    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/PlayUtils;->findHighestPriorityReason(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;

    move-result-object v0

    return-object v0
.end method

.method public static findHighestPriorityReason(Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;)Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;
    .locals 4
    .param p0    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;
    .param p1    # Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;

    if-nez p0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;->getReasonList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;

    if-eq v2, p1, :cond_2

    invoke-virtual {v2}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasReasonReview()Z

    move-result v3

    if-eqz v3, :cond_3

    move-object v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasReasonPlusProfiles()Z

    move-result v3

    if-eqz v3, :cond_4

    move-object v0, v2

    goto :goto_1

    :cond_4
    invoke-virtual {v2}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasDescriptionHtml()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$Reason;->hasReasonPlusProfiles()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_5
    move-object v0, v2

    goto :goto_1
.end method

.method public static getFeaturedGridColumnCount(Landroid/content/res/Resources;D)I
    .locals 4
    .param p0    # Landroid/content/res/Resources;
    .param p1    # D

    const v2, 0x7f090008

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    const v2, 0x7f0b004a

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-double v2, v1

    mul-double/2addr v2, p1

    double-to-int v2, v2

    div-int/2addr v2, v0

    const/4 v3, 0x6

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    :goto_0
    return v2

    :cond_0
    const v2, 0x7f0c000b

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    goto :goto_0
.end method

.method public static getItalicSafeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getRegularGridColumnCount(Landroid/content/res/Resources;)I
    .locals 4
    .param p0    # Landroid/content/res/Resources;

    const v2, 0x7f090008

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    const v2, 0x7f0b004a

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    div-int v2, v1, v0

    const/4 v3, 0x6

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    :goto_0
    return v2

    :cond_0
    const v2, 0x7f0c000c

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    goto :goto_0
.end method

.method public static getStreamQuickLinkColumnCount(Landroid/content/res/Resources;II)I
    .locals 10
    .param p0    # Landroid/content/res/Resources;
    .param p1    # I
    .param p2    # I

    const/4 v9, 0x2

    const/4 v8, 0x1

    const-wide/high16 v6, 0x3ff0000000000000L

    invoke-static {p0, v6, v7}, Lcom/google/android/finsky/utils/PlayUtils;->getFeaturedGridColumnCount(Landroid/content/res/Resources;D)I

    move-result v0

    if-le p1, v0, :cond_2

    move v3, v0

    :goto_0
    rem-int v1, p1, v3

    if-nez v1, :cond_1

    :cond_0
    :goto_1
    return v3

    :cond_1
    sub-int v5, v3, v1

    if-le v5, v8, :cond_0

    if-le v3, v9, :cond_0

    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_2
    add-int v4, p1, p2

    if-ne v4, v8, :cond_3

    if-ne v0, v9, :cond_3

    move v3, v0

    goto :goto_1

    :cond_3
    int-to-float v6, v0

    const/high16 v7, 0x40000000

    div-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v2, v6

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    goto :goto_1
.end method

.method public static isDismissable(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 1
    .param p0    # Lcom/google/android/finsky/api/model/Document;

    invoke-static {p0}, Lcom/google/android/finsky/utils/PlayUtils;->shouldDrawWithReasons(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasNeutralDismissal()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static shouldDrawWithReasons(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 1
    .param p0    # Lcom/google/android/finsky/api/model/Document;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getSuggestionReasons()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getSuggestionReasons()Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/remoting/protos/DocAnnotations$SuggestionReasons;->getReasonCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
