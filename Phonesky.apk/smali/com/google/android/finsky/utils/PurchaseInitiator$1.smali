.class final Lcom/google/android/finsky/utils/PurchaseInitiator$1;
.super Ljava/lang/Object;
.source "PurchaseInitiator.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/PurchaseInitiator;->createFreePurchaseListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;)Lcom/android/volley/Response$Listener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$continueUrl:Ljava/lang/String;

.field final synthetic val$doc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$externalReferrer:Ljava/lang/String;

.field final synthetic val$offerType:I

.field final synthetic val$referrerCookie:Ljava/lang/String;

.field final synthetic val$successListener:Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;


# direct methods
.method constructor <init>(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$account:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iput p3, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$offerType:I

    iput-object p4, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$externalReferrer:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$referrerCookie:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$continueUrl:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$successListener:Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;)V
    .locals 15
    .param p1    # Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseResponse()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPurchaseResponse()Lcom/google/android/finsky/remoting/protos/Buy$PurchaseNotificationResponse;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/finsky/remoting/protos/Buy$PurchaseNotificationResponse;->getStatus()I

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasPurchaseStatusResponse()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$account:Landroid/accounts/Account;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->hasServerLogsCookie()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v5

    :goto_0
    const/16 v1, 0x12d

    iget-object v2, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$offerType:I

    const/4 v4, 0x0

    const-wide/16 v6, -0x1

    const-wide/16 v8, -0x1

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPurchaseBackgroundEvent(ILjava/lang/String;ILjava/lang/String;Lcom/google/protobuf/micro/ByteStringMicro;JJ)V

    iget-object v6, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$account:Landroid/accounts/Account;

    iget-object v7, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iget-object v8, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$externalReferrer:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$referrerCookie:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$continueUrl:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;->getPurchaseStatusResponse()Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;

    move-result-object v11

    const/4 v12, 0x1

    const-string v13, "free_purchase"

    invoke-static/range {v6 .. v13}, Lcom/google/android/finsky/utils/PurchaseInitiator;->processPurchaseStatusResponse(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/remoting/protos/Buy$PurchaseStatusResponse;ZLjava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$successListener:Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$successListener:Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;

    iget-object v2, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$account:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-interface {v1, v2, v3}, Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;->onFreePurchaseSuccess(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    :cond_2
    const-string v1, "Expected PurchaseStatusResponse."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const v2, 0x7f070124

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v14}, Lcom/google/android/finsky/remoting/protos/Buy$PurchaseNotificationResponse;->getLocalizedErrorMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v10

    iget-object v1, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v11

    move-object v9, v8

    invoke-interface/range {v6 .. v11}, Lcom/google/android/finsky/utils/Notifier;->showPurchaseErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    const-string v1, "Expected PurchaseResponse."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/utils/PurchaseInitiator$1;->onResponse(Lcom/google/android/finsky/remoting/protos/Buy$BuyResponse;)V

    return-void
.end method
