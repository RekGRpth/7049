.class public Lcom/google/android/finsky/utils/SessionStatsLogger;
.super Ljava/lang/Object;
.source "SessionStatsLogger.java"


# static fields
.field private static sHaveLoggedSessionStats:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static logSessionStatsIfNecessary(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-boolean v1, Lcom/google/android/finsky/utils/SessionStatsLogger;->sHaveLoggedSessionStats:Z

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/finsky/config/G;->enableSessionStatsLog:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/finsky/utils/SessionStatsLogger;->sHaveLoggedSessionStats:Z

    :try_start_0
    invoke-static {p0}, Lcom/google/android/finsky/utils/SessionStatsLogger;->logSessionStatsImpl(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Fatal exception while logging session stats"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static logSessionStatsImpl(Landroid/content/Context;)V
    .locals 7
    .param p0    # Landroid/content/Context;

    new-instance v5, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    invoke-direct {v5}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;-><init>()V

    sget-object v6, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v6}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v5, v4}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setGlobalAutoUpdateEnabled(Z)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    sget-object v6, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v6}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setGlobalAutoUpdateOverWifiOnly(Z)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    sget-object v6, Lcom/google/android/finsky/utils/FinskyPreferences;->autoUpdateMigrationDialogShownCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v6}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setAutoUpdateCleanupDialogNumTimesShown(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    invoke-static {p0}, Lcom/google/android/finsky/api/AccountHandler;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v6, v0

    invoke-virtual {v5, v6}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setNumAccountsOnDevice(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    :cond_0
    const-string v6, "connectivity"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setNetworkType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setNetworkSubType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    sget-object v6, Lcom/google/android/finsky/utils/FinskyPreferences;->isGaiaAuthOptedOut:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-virtual {v6, v2}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setGaiaPasswordAuthOptedOut(Z)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    :cond_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logSessionData(Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;)V

    return-void
.end method
