.class public abstract Lcom/google/android/gms/common/internal/GmsClient;
.super Ljava/lang/Object;
.source "GmsClient.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/internal/GmsClient$GmsCallbacks;,
        Lcom/google/android/gms/common/internal/GmsClient$PostInitCallback;,
        Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;,
        Lcom/google/android/gms/common/internal/GmsClient$CallbackHandler;,
        Lcom/google/android/gms/common/internal/GmsClient$GmsServiceConnection;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/os/IInterface;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/GooglePlayServicesClient;"
    }
.end annotation


# static fields
.field public static final GOOGLE_PLUS_REQUIRED_FEATURES:[Ljava/lang/String;


# instance fields
.field private final mCallbackProxyList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/internal/GmsClient",
            "<TT;>.CallbackProxy<*>;>;"
        }
    .end annotation
.end field

.field private mConnection:Landroid/content/ServiceConnection;

.field private mConnectionFailedListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;",
            ">;"
        }
    .end annotation
.end field

.field private mConnectionListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;",
            ">;"
        }
    .end annotation
.end field

.field final mConnectionListenersRemoved:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field final mHandler:Landroid/os/Handler;

.field private mIsProcessingConnectionCallback:Z

.field private mIsProcessingOnConnectionFailed:Z

.field mPerformConnectionCallbacks:Z

.field private final mScopes:[Ljava/lang/String;

.field private mService:Landroid/os/IInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "service_esmobile"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "service_googleme"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/common/internal/GmsClient;->GOOGLE_PLUS_REQUIRED_FEATURES:[Ljava/lang/String;

    return-void
.end method

.method protected varargs constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p3    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
    .param p4    # [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListenersRemoved:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/google/android/gms/common/internal/GmsClient;->mIsProcessingConnectionCallback:Z

    iput-boolean v1, p0, Lcom/google/android/gms/common/internal/GmsClient;->mIsProcessingOnConnectionFailed:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mCallbackProxyList:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/google/android/gms/common/internal/GmsClient;->mPerformConnectionCallbacks:Z

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Clients must be created on the UI thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mContext:Landroid/content/Context;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-static {p2}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionFailedListeners:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionFailedListeners:Ljava/util/ArrayList;

    invoke-static {p3}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/common/internal/GmsClient$CallbackHandler;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/internal/GmsClient$CallbackHandler;-><init>(Lcom/google/android/gms/common/internal/GmsClient;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mHandler:Landroid/os/Handler;

    iput-object p4, p0, Lcom/google/android/gms/common/internal/GmsClient;->mScopes:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gms/common/internal/GmsClient;)Landroid/os/IInterface;
    .locals 1
    .param p0    # Lcom/google/android/gms/common/internal/GmsClient;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mService:Landroid/os/IInterface;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/gms/common/internal/GmsClient;Landroid/os/IInterface;)Landroid/os/IInterface;
    .locals 0
    .param p0    # Lcom/google/android/gms/common/internal/GmsClient;
    .param p1    # Landroid/os/IInterface;

    iput-object p1, p0, Lcom/google/android/gms/common/internal/GmsClient;->mService:Landroid/os/IInterface;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/gms/common/internal/GmsClient;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/gms/common/internal/GmsClient;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/gms/common/internal/GmsClient;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/gms/common/internal/GmsClient;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mCallbackProxyList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/gms/common/internal/GmsClient;)Landroid/content/ServiceConnection;
    .locals 1
    .param p0    # Lcom/google/android/gms/common/internal/GmsClient;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/gms/common/internal/GmsClient;Landroid/content/ServiceConnection;)Landroid/content/ServiceConnection;
    .locals 0
    .param p0    # Lcom/google/android/gms/common/internal/GmsClient;
    .param p1    # Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnection:Landroid/content/ServiceConnection;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/gms/common/internal/GmsClient;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/gms/common/internal/GmsClient;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected final checkConnected()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/GmsClient;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public connect()V
    .locals 7

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mPerformConnectionCallbacks:Z

    iget-object v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/gms/common/internal/GmsClient;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/GmsClient;->getStartServiceAction()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnection:Landroid/content/ServiceConnection;

    if-eqz v3, :cond_1

    const-string v3, "GmsClient"

    const-string v4, "Calling connect() while still connected, missing disconnect()."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mService:Landroid/os/IInterface;

    iget-object v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v3, v4}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_1
    new-instance v3, Lcom/google/android/gms/common/internal/GmsClient$GmsServiceConnection;

    invoke-direct {v3, p0}, Lcom/google/android/gms/common/internal/GmsClient$GmsServiceConnection;-><init>(Lcom/google/android/gms/common/internal/GmsClient;)V

    iput-object v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnection:Landroid/content/ServiceConnection;

    iget-object v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnection:Landroid/content/ServiceConnection;

    const/16 v5, 0x81

    invoke-virtual {v3, v1, v4, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    const-string v3, "GmsClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connect: bindService returned "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected abstract createServiceInterface(Landroid/os/IBinder;)Landroid/os/IInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            ")TT;"
        }
    .end annotation
.end method

.method public disconnect()V
    .locals 5

    const/4 v4, 0x0

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/common/internal/GmsClient;->mPerformConnectionCallbacks:Z

    iget-object v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mCallbackProxyList:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/common/internal/GmsClient;->mCallbackProxyList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/common/internal/GmsClient;->mCallbackProxyList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;

    invoke-virtual {v2}, Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;->removeListener()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/common/internal/GmsClient;->mCallbackProxyList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v4, p0, Lcom/google/android/gms/common/internal/GmsClient;->mService:Landroid/os/IInterface;

    iget-object v2, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnection:Landroid/content/ServiceConnection;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/GmsClient;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    iput-object v4, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnection:Landroid/content/ServiceConnection;

    :cond_1
    return-void

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public final doCallback(Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/internal/GmsClient",
            "<TT;>.CallbackProxy<*>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/GmsClient;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected final getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method protected final getScopes()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mScopes:[Ljava/lang/String;

    return-object v0
.end method

.method protected final getService()Landroid/os/IInterface;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/GmsClient;->checkConnected()V

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mService:Landroid/os/IInterface;

    return-object v0
.end method

.method protected abstract getServiceDescriptor()Ljava/lang/String;
.end method

.method protected abstract getServiceFromBroker(Lcom/google/android/gms/common/internal/IGmsServiceBroker;Lcom/google/android/gms/common/internal/GmsClient$GmsCallbacks;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/internal/IGmsServiceBroker;",
            "Lcom/google/android/gms/common/internal/GmsClient",
            "<TT;>.GmsCallbacks;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method protected abstract getStartServiceAction()Ljava/lang/String;
.end method

.method public isConnected()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mService:Landroid/os/IInterface;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final onConnectionFailure(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 6
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    iget-object v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v4, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionFailedListeners:Ljava/util/ArrayList;

    monitor-enter v4

    const/4 v3, 0x1

    :try_start_0
    iput-boolean v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mIsProcessingOnConnectionFailed:Z

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionFailedListeners:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_2

    iget-boolean v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mPerformConnectionCallbacks:Z

    if-nez v3, :cond_0

    monitor-exit v4

    :goto_1
    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionFailedListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    invoke-interface {v3, p1}, Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;->onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mIsProcessingOnConnectionFailed:Z

    monitor-exit v4

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method protected final onConnectionSuccess()V
    .locals 9

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    monitor-enter v7

    :try_start_0
    iget-boolean v6, p0, Lcom/google/android/gms/common/internal/GmsClient;->mIsProcessingConnectionCallback:Z

    if-nez v6, :cond_1

    move v6, v4

    :goto_0
    invoke-static {v6}, Lcom/google/android/gms/common/internal/Preconditions;->checkState(Z)V

    iget-object v6, p0, Lcom/google/android/gms/common/internal/GmsClient;->mHandler:Landroid/os/Handler;

    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/google/android/gms/common/internal/GmsClient;->mIsProcessingConnectionCallback:Z

    iget-object v6, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListenersRemoved:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_2

    :goto_1
    invoke-static {v4}, Lcom/google/android/gms/common/internal/Preconditions;->checkState(Z)V

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    :goto_2
    if-ge v1, v3, :cond_0

    iget-boolean v4, p0, Lcom/google/android/gms/common/internal/GmsClient;->mPerformConnectionCallbacks:Z

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/GmsClient;->isConnected()Z

    move-result v4

    if-nez v4, :cond_3

    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListenersRemoved:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/android/gms/common/internal/GmsClient;->mIsProcessingConnectionCallback:Z

    monitor-exit v7

    return-void

    :cond_1
    move v6, v5

    goto :goto_0

    :cond_2
    move v4, v5

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListenersRemoved:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget-object v4, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListenersRemoved:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;

    invoke-interface {v4}, Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;->onConnected()V

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :catchall_0
    move-exception v4

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method protected final onDisconnection()V
    .locals 6

    iget-object v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v4, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    monitor-enter v4

    const/4 v3, 0x1

    :try_start_0
    iput-boolean v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mIsProcessingConnectionCallback:Z

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_0

    iget-boolean v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mPerformConnectionCallbacks:Z

    if-nez v3, :cond_1

    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mIsProcessingConnectionCallback:Z

    monitor-exit v4

    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;

    invoke-interface {v3}, Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;->onDisconnected()V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method protected final onServiceBrokerBound(Landroid/os/IBinder;)V
    .locals 4
    .param p1    # Landroid/os/IBinder;

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/common/internal/IGmsServiceBroker$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/gms/common/internal/IGmsServiceBroker;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/common/internal/GmsClient$GmsCallbacks;

    invoke-direct {v2, p0}, Lcom/google/android/gms/common/internal/GmsClient$GmsCallbacks;-><init>(Lcom/google/android/gms/common/internal/GmsClient;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/common/internal/GmsClient;->getServiceFromBroker(Lcom/google/android/gms/common/internal/IGmsServiceBroker;Lcom/google/android/gms/common/internal/GmsClient$GmsCallbacks;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "GmsClient"

    const-string v3, "service died"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public registerConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;

    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GmsClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerConnectionListener(): listener "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is already registered"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/GmsClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/GmsClient;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void

    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mIsProcessingConnectionCallback:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V
    .locals 5
    .param p1    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;

    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/gms/common/internal/GmsClient;->mIsProcessingConnectionCallback:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v1, "GmsClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unregisterConnectionListener(): listener "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not found"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    monitor-exit v2

    return-void

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/common/internal/GmsClient;->mIsProcessingConnectionCallback:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListenersRemoved:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/common/internal/GmsClient;->mConnectionListenersRemoved:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
