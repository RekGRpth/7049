.class public final Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ClientAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/ClientAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LogRequest"
.end annotation


# instance fields
.field private cachedSize:I

.field private clientInfo_:Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

.field private hasClientInfo:Z

.field private hasLogSource:Z

.field private hasRequestTimeMs:Z

.field private hasSerializedLogEvents:Z

.field private logEvent_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;",
            ">;"
        }
    .end annotation
.end field

.field private logSource_:I

.field private requestTimeMs_:J

.field private serializedLogEvents_:Lcom/google/protobuf/micro/ByteStringMicro;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->requestTimeMs_:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->clientInfo_:Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSource_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent_:Ljava/util/List;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents_:Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addLogEvent(Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->cachedSize:I

    return v0
.end method

.method public getClientInfo()Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->clientInfo_:Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    return-object v0
.end method

.method public getLogEventList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logEvent_:Ljava/util/List;

    return-object v0
.end method

.method public getLogSource()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSource_:I

    return v0
.end method

.method public getRequestTimeMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->requestTimeMs_:J

    return-wide v0
.end method

.method public getSerializedLogEvents()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasClientInfo()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->getClientInfo()Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasLogSource()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->getLogSource()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->getLogEventList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasRequestTimeMs()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->getRequestTimeMs()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasSerializedLogEvents()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->getSerializedLogEvents()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    iput v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->cachedSize:I

    return v2
.end method

.method public hasClientInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasClientInfo:Z

    return v0
.end method

.method public hasLogSource()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasLogSource:Z

    return v0
.end method

.method public hasRequestTimeMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasRequestTimeMs:Z

    return v0
.end method

.method public hasSerializedLogEvents()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasSerializedLogEvents:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    invoke-direct {v1}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->setClientInfo(Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->setLogSource(I)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    invoke-direct {v1}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->addLogEvent(Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->setRequestTimeMs(J)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->setSerializedLogEvents(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;

    move-result-object v0

    return-object v0
.end method

.method public setClientInfo(Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasClientInfo:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->clientInfo_:Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    return-object p0
.end method

.method public setLogSource(I)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasLogSource:Z

    iput p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->logSource_:I

    return-object p0
.end method

.method public setRequestTimeMs(J)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasRequestTimeMs:Z

    iput-wide p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->requestTimeMs_:J

    return-object p0
.end method

.method public setSerializedLogEvents(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasSerializedLogEvents:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->serializedLogEvents_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasClientInfo()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->getClientInfo()Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasLogSource()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->getLogSource()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->getLogEventList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasRequestTimeMs()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->getRequestTimeMs()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->hasSerializedLogEvents()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->getSerializedLogEvents()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_4
    return-void
.end method
