.class public final Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayStoreSessionData"
.end annotation


# instance fields
.field private autoUpdateCleanupDialogNumTimesShown_:I

.field private cachedSize:I

.field private gaiaPasswordAuthOptedOut_:Z

.field private globalAutoUpdateEnabled_:Z

.field private globalAutoUpdateOverWifiOnly_:Z

.field private hasAutoUpdateCleanupDialogNumTimesShown:Z

.field private hasGaiaPasswordAuthOptedOut:Z

.field private hasGlobalAutoUpdateEnabled:Z

.field private hasGlobalAutoUpdateOverWifiOnly:Z

.field private hasNetworkSubType:Z

.field private hasNetworkType:Z

.field private hasNumAccountsOnDevice:Z

.field private hasNumAutoUpdatingInstalledApps:Z

.field private hasNumInstalledApps:Z

.field private hasNumInstalledAppsNotAutoUpdating:Z

.field private networkSubType_:I

.field private networkType_:I

.field private numAccountsOnDevice_:I

.field private numAutoUpdatingInstalledApps_:I

.field private numInstalledAppsNotAutoUpdating_:I

.field private numInstalledApps_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateEnabled_:Z

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateOverWifiOnly_:Z

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->autoUpdateCleanupDialogNumTimesShown_:I

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->networkType_:I

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->networkSubType_:I

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->numAccountsOnDevice_:I

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->numInstalledApps_:I

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->numAutoUpdatingInstalledApps_:I

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->numInstalledAppsNotAutoUpdating_:I

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->gaiaPasswordAuthOptedOut_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAutoUpdateCleanupDialogNumTimesShown()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->autoUpdateCleanupDialogNumTimesShown_:I

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->cachedSize:I

    return v0
.end method

.method public getGaiaPasswordAuthOptedOut()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->gaiaPasswordAuthOptedOut_:Z

    return v0
.end method

.method public getGlobalAutoUpdateEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateEnabled_:Z

    return v0
.end method

.method public getGlobalAutoUpdateOverWifiOnly()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateOverWifiOnly_:Z

    return v0
.end method

.method public getNetworkSubType()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->networkSubType_:I

    return v0
.end method

.method public getNetworkType()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->networkType_:I

    return v0
.end method

.method public getNumAccountsOnDevice()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->numAccountsOnDevice_:I

    return v0
.end method

.method public getNumAutoUpdatingInstalledApps()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->numAutoUpdatingInstalledApps_:I

    return v0
.end method

.method public getNumInstalledApps()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->numInstalledApps_:I

    return v0
.end method

.method public getNumInstalledAppsNotAutoUpdating()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->numInstalledAppsNotAutoUpdating_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getGlobalAutoUpdateEnabled()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateOverWifiOnly()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getGlobalAutoUpdateOverWifiOnly()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasAutoUpdateCleanupDialogNumTimesShown()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getAutoUpdateCleanupDialogNumTimesShown()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNetworkType()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getNetworkType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNetworkSubType()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getNetworkSubType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumAccountsOnDevice()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getNumAccountsOnDevice()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledApps()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getNumInstalledApps()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumAutoUpdatingInstalledApps()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getNumAutoUpdatingInstalledApps()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledAppsNotAutoUpdating()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getNumInstalledAppsNotAutoUpdating()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasGaiaPasswordAuthOptedOut()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getGaiaPasswordAuthOptedOut()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->cachedSize:I

    return v0
.end method

.method public hasAutoUpdateCleanupDialogNumTimesShown()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasAutoUpdateCleanupDialogNumTimesShown:Z

    return v0
.end method

.method public hasGaiaPasswordAuthOptedOut()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasGaiaPasswordAuthOptedOut:Z

    return v0
.end method

.method public hasGlobalAutoUpdateEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateEnabled:Z

    return v0
.end method

.method public hasGlobalAutoUpdateOverWifiOnly()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateOverWifiOnly:Z

    return v0
.end method

.method public hasNetworkSubType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNetworkSubType:Z

    return v0
.end method

.method public hasNetworkType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNetworkType:Z

    return v0
.end method

.method public hasNumAccountsOnDevice()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumAccountsOnDevice:Z

    return v0
.end method

.method public hasNumAutoUpdatingInstalledApps()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumAutoUpdatingInstalledApps:Z

    return v0
.end method

.method public hasNumInstalledApps()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledApps:Z

    return v0
.end method

.method public hasNumInstalledAppsNotAutoUpdating()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledAppsNotAutoUpdating:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setGlobalAutoUpdateEnabled(Z)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setGlobalAutoUpdateOverWifiOnly(Z)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setAutoUpdateCleanupDialogNumTimesShown(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setNetworkType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setNetworkSubType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setNumAccountsOnDevice(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setNumInstalledApps(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setNumAutoUpdatingInstalledApps(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setNumInstalledAppsNotAutoUpdating(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setGaiaPasswordAuthOptedOut(Z)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    move-result-object v0

    return-object v0
.end method

.method public setAutoUpdateCleanupDialogNumTimesShown(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasAutoUpdateCleanupDialogNumTimesShown:Z

    iput p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->autoUpdateCleanupDialogNumTimesShown_:I

    return-object p0
.end method

.method public setGaiaPasswordAuthOptedOut(Z)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasGaiaPasswordAuthOptedOut:Z

    iput-boolean p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->gaiaPasswordAuthOptedOut_:Z

    return-object p0
.end method

.method public setGlobalAutoUpdateEnabled(Z)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateEnabled:Z

    iput-boolean p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateEnabled_:Z

    return-object p0
.end method

.method public setGlobalAutoUpdateOverWifiOnly(Z)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateOverWifiOnly:Z

    iput-boolean p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->globalAutoUpdateOverWifiOnly_:Z

    return-object p0
.end method

.method public setNetworkSubType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNetworkSubType:Z

    iput p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->networkSubType_:I

    return-object p0
.end method

.method public setNetworkType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNetworkType:Z

    iput p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->networkType_:I

    return-object p0
.end method

.method public setNumAccountsOnDevice(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumAccountsOnDevice:Z

    iput p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->numAccountsOnDevice_:I

    return-object p0
.end method

.method public setNumAutoUpdatingInstalledApps(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumAutoUpdatingInstalledApps:Z

    iput p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->numAutoUpdatingInstalledApps_:I

    return-object p0
.end method

.method public setNumInstalledApps(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledApps:Z

    iput p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->numInstalledApps_:I

    return-object p0
.end method

.method public setNumInstalledAppsNotAutoUpdating(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledAppsNotAutoUpdating:Z

    iput p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->numInstalledAppsNotAutoUpdating_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getGlobalAutoUpdateEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasGlobalAutoUpdateOverWifiOnly()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getGlobalAutoUpdateOverWifiOnly()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasAutoUpdateCleanupDialogNumTimesShown()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getAutoUpdateCleanupDialogNumTimesShown()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNetworkType()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getNetworkType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNetworkSubType()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getNetworkSubType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumAccountsOnDevice()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getNumAccountsOnDevice()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledApps()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getNumInstalledApps()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumAutoUpdatingInstalledApps()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getNumAutoUpdatingInstalledApps()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasNumInstalledAppsNotAutoUpdating()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getNumInstalledAppsNotAutoUpdating()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->hasGaiaPasswordAuthOptedOut()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->getGaiaPasswordAuthOptedOut()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_9
    return-void
.end method
