.class public final Lcom/google/android/play/analytics/PlayStore;
.super Ljava/lang/Object;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/analytics/PlayStore$AppData;,
        Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;,
        Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;,
        Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;,
        Lcom/google/android/play/analytics/PlayStore$PlayStoreDeepLinkEvent;,
        Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;,
        Lcom/google/android/play/analytics/PlayStore$PlayStoreClickEvent;,
        Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;,
        Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;,
        Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
