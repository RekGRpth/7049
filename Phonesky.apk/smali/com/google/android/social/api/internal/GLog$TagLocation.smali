.class public final enum Lcom/google/android/social/api/internal/GLog$TagLocation;
.super Ljava/lang/Enum;
.source "GLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/internal/GLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TagLocation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/social/api/internal/GLog$TagLocation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/social/api/internal/GLog$TagLocation;

.field public static final enum MESSAGE_PREFIX:Lcom/google/android/social/api/internal/GLog$TagLocation;

.field public static final enum NO_TAG:Lcom/google/android/social/api/internal/GLog$TagLocation;

.field public static final enum TAG_SUFFIX:Lcom/google/android/social/api/internal/GLog$TagLocation;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/social/api/internal/GLog$TagLocation;

    const-string v1, "TAG_SUFFIX"

    invoke-direct {v0, v1, v2}, Lcom/google/android/social/api/internal/GLog$TagLocation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/social/api/internal/GLog$TagLocation;->TAG_SUFFIX:Lcom/google/android/social/api/internal/GLog$TagLocation;

    new-instance v0, Lcom/google/android/social/api/internal/GLog$TagLocation;

    const-string v1, "MESSAGE_PREFIX"

    invoke-direct {v0, v1, v3}, Lcom/google/android/social/api/internal/GLog$TagLocation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/social/api/internal/GLog$TagLocation;->MESSAGE_PREFIX:Lcom/google/android/social/api/internal/GLog$TagLocation;

    new-instance v0, Lcom/google/android/social/api/internal/GLog$TagLocation;

    const-string v1, "NO_TAG"

    invoke-direct {v0, v1, v4}, Lcom/google/android/social/api/internal/GLog$TagLocation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/social/api/internal/GLog$TagLocation;->NO_TAG:Lcom/google/android/social/api/internal/GLog$TagLocation;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/social/api/internal/GLog$TagLocation;

    sget-object v1, Lcom/google/android/social/api/internal/GLog$TagLocation;->TAG_SUFFIX:Lcom/google/android/social/api/internal/GLog$TagLocation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/social/api/internal/GLog$TagLocation;->MESSAGE_PREFIX:Lcom/google/android/social/api/internal/GLog$TagLocation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/social/api/internal/GLog$TagLocation;->NO_TAG:Lcom/google/android/social/api/internal/GLog$TagLocation;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/social/api/internal/GLog$TagLocation;->$VALUES:[Lcom/google/android/social/api/internal/GLog$TagLocation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/social/api/internal/GLog$TagLocation;
    .locals 1

    const-class v0, Lcom/google/android/social/api/internal/GLog$TagLocation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/internal/GLog$TagLocation;

    return-object v0
.end method

.method public static values()[Lcom/google/android/social/api/internal/GLog$TagLocation;
    .locals 1

    sget-object v0, Lcom/google/android/social/api/internal/GLog$TagLocation;->$VALUES:[Lcom/google/android/social/api/internal/GLog$TagLocation;

    invoke-virtual {v0}, [Lcom/google/android/social/api/internal/GLog$TagLocation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/social/api/internal/GLog$TagLocation;

    return-object v0
.end method
