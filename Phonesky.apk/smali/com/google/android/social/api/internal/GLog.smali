.class public Lcom/google/android/social/api/internal/GLog;
.super Ljava/lang/Object;
.source "GLog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/internal/GLog$Tag;,
        Lcom/google/android/social/api/internal/GLog$LogListener;,
        Lcom/google/android/social/api/internal/GLog$TagLocation;
    }
.end annotation


# static fields
.field private static final configRegex:Ljava/util/regex/Pattern;

.field private static listener:Lcom/google/android/social/api/internal/GLog$LogListener;

.field private static masterTag:Ljava/lang/String;

.field private static final minLevelMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static minLogLevel:I

.field private static tagLocation:Lcom/google/android/social/api/internal/GLog$TagLocation;


# instance fields
.field private final tag:Lcom/google/android/social/api/internal/GLog$Tag;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    sput-object v1, Lcom/google/android/social/api/internal/GLog;->masterTag:Ljava/lang/String;

    const/4 v0, 0x4

    sput v0, Lcom/google/android/social/api/internal/GLog;->minLogLevel:I

    sget-object v0, Lcom/google/android/social/api/internal/GLog$TagLocation;->TAG_SUFFIX:Lcom/google/android/social/api/internal/GLog$TagLocation;

    sput-object v0, Lcom/google/android/social/api/internal/GLog;->tagLocation:Lcom/google/android/social/api/internal/GLog$TagLocation;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/social/api/internal/GLog;->minLevelMap:Ljava/util/HashMap;

    sput-object v1, Lcom/google/android/social/api/internal/GLog;->listener:Lcom/google/android/social/api/internal/GLog$LogListener;

    const-string v0, "(?:\\s*(?:(.+?)\\s*:\\s*(.+?)|(\\[\\s*.+?\\s*\\]))\\s*(?:#.+?)?(?:;|$))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/internal/GLog;->configRegex:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/social/api/internal/GLog$Tag;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/social/api/internal/GLog$Tag;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/social/api/internal/GLog;->tag:Lcom/google/android/social/api/internal/GLog$Tag;

    return-void
.end method

.method private static buildTag(Lcom/google/android/social/api/internal/GLog$Tag;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/google/android/social/api/internal/GLog$Tag;

    sget-object v0, Lcom/google/android/social/api/internal/GLog;->masterTag:Ljava/lang/String;

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    const-string v0, "Google"

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/social/api/internal/GLog;->masterTag:Ljava/lang/String;

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/social/api/internal/GLog;->tagLocation:Lcom/google/android/social/api/internal/GLog$TagLocation;

    sget-object v1, Lcom/google/android/social/api/internal/GLog$TagLocation;->TAG_SUFFIX:Lcom/google/android/social/api/internal/GLog$TagLocation;

    if-eq v0, v1, :cond_1

    const-string v0, "Google"

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/social/api/internal/GLog;->masterTag:Ljava/lang/String;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/social/api/internal/GLog$Tag;->getFullTag()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/social/api/internal/GLog;->tagLocation:Lcom/google/android/social/api/internal/GLog$TagLocation;

    sget-object v1, Lcom/google/android/social/api/internal/GLog$TagLocation;->TAG_SUFFIX:Lcom/google/android/social/api/internal/GLog$TagLocation;

    if-ne v0, v1, :cond_3

    if-nez p0, :cond_4

    :cond_3
    sget-object v0, Lcom/google/android/social/api/internal/GLog;->masterTag:Ljava/lang/String;

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/social/api/internal/GLog;->masterTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/social/api/internal/GLog$Tag;->getFullTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static varargs debug(Lcom/google/android/social/api/internal/GLog$Tag;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p0    # Lcom/google/android/social/api/internal/GLog$Tag;
    .param p1    # Ljava/lang/Throwable;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/Object;

    const/4 v0, 0x3

    invoke-static {v0, p0, p1, p2, p3}, Lcom/google/android/social/api/internal/GLog;->log(ILcom/google/android/social/api/internal/GLog$Tag;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static varargs log(ILcom/google/android/social/api/internal/GLog$Tag;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 7
    .param p0    # I
    .param p1    # Lcom/google/android/social/api/internal/GLog$Tag;
    .param p2    # Ljava/lang/Throwable;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/Object;

    invoke-static {p1, p0}, Lcom/google/android/social/api/internal/GLog;->shouldLog(Lcom/google/android/social/api/internal/GLog$Tag;I)Z

    move-result v3

    sget-object v0, Lcom/google/android/social/api/internal/GLog;->listener:Lcom/google/android/social/api/internal/GLog$LogListener;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/social/api/internal/GLog;->listener:Lcom/google/android/social/api/internal/GLog$LogListener;

    move v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-interface/range {v0 .. v6}, Lcom/google/android/social/api/internal/GLog$LogListener;->log(ILcom/google/android/social/api/internal/GLog$Tag;ZLjava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    if-nez p3, :cond_1

    const-string p3, ""

    :cond_1
    if-eqz p4, :cond_2

    array-length v0, p4

    if-lez v0, :cond_2

    invoke-static {p3, p4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    :cond_2
    if-eqz v3, :cond_3

    invoke-static {p1}, Lcom/google/android/social/api/internal/GLog;->buildTag(Lcom/google/android/social/api/internal/GLog$Tag;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lcom/google/android/social/api/internal/GLog;->tagLocation:Lcom/google/android/social/api/internal/GLog$TagLocation;

    sget-object v4, Lcom/google/android/social/api/internal/GLog$TagLocation;->MESSAGE_PREFIX:Lcom/google/android/social/api/internal/GLog$TagLocation;

    if-ne v0, v4, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/social/api/internal/GLog$Tag;->getFullTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ": "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void

    :cond_4
    const-string v0, ""

    goto :goto_0
.end method

.method public static shouldLog(Lcom/google/android/social/api/internal/GLog$Tag;I)Z
    .locals 4
    .param p0    # Lcom/google/android/social/api/internal/GLog$Tag;
    .param p1    # I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/social/api/internal/GLog;->minLogLevel:I

    if-le v2, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    sget-object v2, Lcom/google/android/social/api/internal/GLog;->minLevelMap:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/google/android/social/api/internal/GLog$Tag;->getFullTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gt v2, p1, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public varargs debug(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/social/api/internal/GLog;->tag:Lcom/google/android/social/api/internal/GLog$Tag;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/social/api/internal/GLog;->debug(Lcom/google/android/social/api/internal/GLog$Tag;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public debug(Ljava/lang/Throwable;)V
    .locals 3
    .param p1    # Ljava/lang/Throwable;

    iget-object v0, p0, Lcom/google/android/social/api/internal/GLog;->tag:Lcom/google/android/social/api/internal/GLog$Tag;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, Lcom/google/android/social/api/internal/GLog;->debug(Lcom/google/android/social/api/internal/GLog$Tag;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
