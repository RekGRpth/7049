.class public final Lcom/google/android/social/api/logging/Aspen$Action;
.super Ljava/lang/Object;
.source "Aspen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/logging/Aspen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Action"
.end annotation


# static fields
.field public static final PEOPLE_SEARCH_TEXT_CLEARED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

.field public static final PEOPLE_SEARCH_TEXT_ENTERED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    # invokes: Lcom/google/android/social/api/logging/Aspen;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/Aspen;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/Aspen$Action;->PEOPLE_SEARCH_TEXT_ENTERED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    const/4 v0, 0x2

    # invokes: Lcom/google/android/social/api/logging/Aspen;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/Aspen;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/Aspen$Action;->PEOPLE_SEARCH_TEXT_CLEARED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
