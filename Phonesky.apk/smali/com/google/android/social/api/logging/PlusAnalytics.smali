.class public final Lcom/google/android/social/api/logging/PlusAnalytics;
.super Ljava/lang/Object;
.source "PlusAnalytics.java"


# static fields
.field private static final DEFAULT_RHS_OZ_EVENT:Lcom/google/api/services/plus/model/OzEvent;


# instance fields
.field final action:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

.field final clientTimeMsec:J

.field final durationMsec:I

.field final endView:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

.field final startView:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/api/services/plus/model/OzEvent;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/OzEvent;-><init>()V

    sput-object v0, Lcom/google/android/social/api/logging/PlusAnalytics;->DEFAULT_RHS_OZ_EVENT:Lcom/google/api/services/plus/model/OzEvent;

    sget-object v0, Lcom/google/android/social/api/logging/PlusAnalytics;->DEFAULT_RHS_OZ_EVENT:Lcom/google/api/services/plus/model/OzEvent;

    new-instance v1, Lcom/google/api/services/plus/model/ActionTarget;

    invoke-direct {v1}, Lcom/google/api/services/plus/model/ActionTarget;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plus/model/OzEvent;->actionTarget:Lcom/google/api/services/plus/model/ActionTarget;

    sget-object v0, Lcom/google/android/social/api/logging/PlusAnalytics;->DEFAULT_RHS_OZ_EVENT:Lcom/google/api/services/plus/model/OzEvent;

    iget-object v0, v0, Lcom/google/api/services/plus/model/OzEvent;->actionTarget:Lcom/google/api/services/plus/model/ActionTarget;

    const-string v1, "android.aspen.playstore.suggestedfriends"

    iput-object v1, v0, Lcom/google/api/services/plus/model/ActionTarget;->actionSource:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;)V
    .locals 2
    .param p1    # Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .param p2    # Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .param p3    # Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p1, :cond_1

    invoke-static {p3}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/google/android/social/api/logging/PlusAnalytics;->action:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    iput-object p2, p0, Lcom/google/android/social/api/logging/PlusAnalytics;->startView:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    iput-object p3, p0, Lcom/google/android/social/api/logging/PlusAnalytics;->endView:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/social/api/logging/PlusAnalytics;->durationMsec:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/social/api/logging/PlusAnalytics;->clientTimeMsec:J

    return-void

    :cond_1
    if-nez p3, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static createCircleMemberShipChangeAction(ZLcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plus/model/ClientOzEvent;
    .locals 10
    .param p0    # Z
    .param p1    # Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v9, 0x1

    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/android/social/api/logging/SocialGraph$Action;->ADD_CIRCLE_MEMBERS:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    :goto_0
    new-instance v7, Lcom/google/android/social/api/logging/PlusAnalytics;

    const/4 v8, 0x0

    invoke-direct {v7, v0, p1, v8}, Lcom/google/android/social/api/logging/PlusAnalytics;-><init>(Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;)V

    invoke-direct {v7}, Lcom/google/android/social/api/logging/PlusAnalytics;->toClientOzEvent()Lcom/google/api/services/plus/model/ClientOzEvent;

    move-result-object v6

    new-instance v5, Lcom/google/api/services/plus/model/ClientActionData;

    invoke-direct {v5}, Lcom/google/api/services/plus/model/ClientActionData;-><init>()V

    new-instance v1, Lcom/google/api/services/plus/model/ClientLoggedCircle;

    invoke-direct {v1}, Lcom/google/api/services/plus/model/ClientLoggedCircle;-><init>()V

    iput-object p3, v1, Lcom/google/api/services/plus/model/ClientLoggedCircle;->circleId:Ljava/lang/String;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v9}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v4, v5, Lcom/google/api/services/plus/model/ClientActionData;->circle:Ljava/util/List;

    new-instance v2, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;

    invoke-direct {v2}, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;-><init>()V

    iput-object p2, v2, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;->obfuscatedGaiaId:Ljava/lang/String;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v9}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v3, v5, Lcom/google/api/services/plus/model/ClientActionData;->circleMember:Ljava/util/List;

    iput-object v5, v6, Lcom/google/api/services/plus/model/ClientOzEvent;->actionData:Lcom/google/api/services/plus/model/ClientActionData;

    return-object v6

    :cond_0
    sget-object v0, Lcom/google/android/social/api/logging/SocialGraph$Action;->REMOVE_CIRCLE_MEMBERS:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    goto :goto_0
.end method

.method private static createDefaultSuggestionSummary(Ljava/lang/Integer;)Lcom/google/api/services/plus/model/ClientLoggedSuggestionSummaryInfo;
    .locals 1
    .param p0    # Ljava/lang/Integer;

    new-instance v0, Lcom/google/api/services/plus/model/ClientLoggedSuggestionSummaryInfo;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientLoggedSuggestionSummaryInfo;-><init>()V

    if-eqz p0, :cond_0

    iput-object p0, v0, Lcom/google/api/services/plus/model/ClientLoggedSuggestionSummaryInfo;->numberOfSuggestionsLoaded:Ljava/lang/Integer;

    :cond_0
    return-object v0
.end method

.method private static createFavaDiagnostics(Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;I)Lcom/google/api/services/plus/model/FavaDiagnostics;
    .locals 2
    .param p0    # Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .param p1    # Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .param p2    # Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .param p3    # I

    new-instance v0, Lcom/google/api/services/plus/model/FavaDiagnostics;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/FavaDiagnostics;-><init>()V

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plus/model/FavaDiagnostics;->totalTimeMs:Ljava/lang/Integer;

    if-eqz p0, :cond_0

    iput-object p0, v0, Lcom/google/api/services/plus/model/FavaDiagnostics;->actionType:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    :cond_0
    iput-object p1, v0, Lcom/google/api/services/plus/model/FavaDiagnostics;->startView:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    if-eqz p2, :cond_1

    iput-object p2, v0, Lcom/google/api/services/plus/model/FavaDiagnostics;->endView:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    :cond_1
    return-object v0
.end method

.method private static varargs createShownSuggestionsItemList([Ljava/util/List;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;",
            ">;"
        }
    .end annotation

    const/4 v11, 0x1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    array-length v9, p0

    if-ge v0, v9, :cond_1

    aget-object v4, p0, v0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_0

    new-instance v1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;

    invoke-direct {v1}, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;-><init>()V

    add-int/lit8 v9, v0, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iput-object v9, v1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;->row:Ljava/lang/Integer;

    new-instance v8, Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfo;

    invoke-direct {v8}, Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfo;-><init>()V

    add-int/lit8 v9, v3, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iput-object v9, v8, Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfo;->placement:Ljava/lang/Integer;

    const-wide/16 v9, 0x0

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    iput-object v9, v8, Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfo;->score:Ljava/lang/Double;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iput-object v9, v8, Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfo;->suggestionType:Ljava/lang/Integer;

    iput-object v8, v1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;->suggestionInfo:Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfo;

    new-instance v7, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;

    invoke-direct {v7}, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;-><init>()V

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    iput-object v9, v7, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;->obfuscatedGaiaId:Ljava/lang/String;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v11}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v6, v1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;->person:Ljava/util/List;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private static createSuggestionItemList(Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;",
            ">;"
        }
    .end annotation

    const/4 v7, 0x1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;-><init>()V

    new-instance v4, Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfo;

    invoke-direct {v4}, Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfo;-><init>()V

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    iput-object v5, v4, Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfo;->score:Ljava/lang/Double;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfo;->suggestionType:Ljava/lang/Integer;

    iput-object v4, v0, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;->suggestionInfo:Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfo;

    new-instance v3, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;

    invoke-direct {v3}, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;-><init>()V

    iput-object p0, v3, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;->obfuscatedGaiaId:Ljava/lang/String;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v7}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v2, v0, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;->person:Ljava/util/List;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v1
.end method

.method public static toAddCircleMemberClientOzEvent(Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plus/model/ClientOzEvent;
    .locals 1
    .param p0    # Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0, p0, p1, p2}, Lcom/google/android/social/api/logging/PlusAnalytics;->createCircleMemberShipChangeAction(ZLcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plus/model/ClientOzEvent;

    move-result-object v0

    return-object v0
.end method

.method private toClientOzEvent()Lcom/google/api/services/plus/model/ClientOzEvent;
    .locals 7

    iget-object v3, p0, Lcom/google/android/social/api/logging/PlusAnalytics;->action:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    iget-object v4, p0, Lcom/google/android/social/api/logging/PlusAnalytics;->startView:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    iget-object v5, p0, Lcom/google/android/social/api/logging/PlusAnalytics;->endView:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    iget v6, p0, Lcom/google/android/social/api/logging/PlusAnalytics;->durationMsec:I

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/social/api/logging/PlusAnalytics;->createFavaDiagnostics(Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;I)Lcom/google/api/services/plus/model/FavaDiagnostics;

    move-result-object v1

    new-instance v2, Lcom/google/api/services/plus/model/OzEvent;

    invoke-direct {v2}, Lcom/google/api/services/plus/model/OzEvent;-><init>()V

    iput-object v1, v2, Lcom/google/api/services/plus/model/OzEvent;->favaDiagnostics:Lcom/google/api/services/plus/model/FavaDiagnostics;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plus/model/OzEvent;->isNativePlatformEvent:Ljava/lang/Boolean;

    new-instance v0, Lcom/google/api/services/plus/model/ClientOzEvent;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientOzEvent;-><init>()V

    iput-object v2, v0, Lcom/google/api/services/plus/model/ClientOzEvent;->ozEvent:Lcom/google/api/services/plus/model/OzEvent;

    iget-wide v3, p0, Lcom/google/android/social/api/logging/PlusAnalytics;->clientTimeMsec:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plus/model/ClientOzEvent;->clientTimeMsec:Ljava/lang/Long;

    return-object v0
.end method

.method public static toPeopleSuggestionClientOzEvent(Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Ljava/lang/String;)Lcom/google/api/services/plus/model/ClientOzEvent;
    .locals 6
    .param p0    # Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v3, Lcom/google/android/social/api/logging/Rhs$Action;->ITEMS_SHOWN:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    invoke-virtual {v3, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    const-string v4, "ITEMS_SHOWN is an invalid argument. Use toPeopleSuggestionsShownClientOzEvent instead"

    invoke-static {v3, v4}, Lcom/google/android/gms/common/internal/Preconditions;->checkState(ZLjava/lang/Object;)V

    new-instance v3, Lcom/google/android/social/api/logging/PlusAnalytics;

    sget-object v4, Lcom/google/android/social/api/logging/Aspen$View;->PEOPLE_SUGGEST_WIDGET:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    invoke-direct {v3, p0, v4, v5}, Lcom/google/android/social/api/logging/PlusAnalytics;-><init>(Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;)V

    invoke-direct {v3}, Lcom/google/android/social/api/logging/PlusAnalytics;->toClientOzEvent()Lcom/google/api/services/plus/model/ClientOzEvent;

    move-result-object v1

    sget-object v3, Lcom/google/android/social/api/logging/PlusAnalytics;->DEFAULT_RHS_OZ_EVENT:Lcom/google/api/services/plus/model/OzEvent;

    iput-object v3, v1, Lcom/google/api/services/plus/model/ClientOzEvent;->ozEvent:Lcom/google/api/services/plus/model/OzEvent;

    new-instance v2, Lcom/google/api/services/plus/model/ClientLoggedRhsComponent;

    invoke-direct {v2}, Lcom/google/api/services/plus/model/ClientLoggedRhsComponent;-><init>()V

    invoke-static {p1}, Lcom/google/android/social/api/logging/PlusAnalytics;->createSuggestionItemList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plus/model/ClientLoggedRhsComponent;->item:Ljava/util/List;

    invoke-static {v5}, Lcom/google/android/social/api/logging/PlusAnalytics;->createDefaultSuggestionSummary(Ljava/lang/Integer;)Lcom/google/api/services/plus/model/ClientLoggedSuggestionSummaryInfo;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plus/model/ClientLoggedRhsComponent;->suggestionSummaryInfo:Lcom/google/api/services/plus/model/ClientLoggedSuggestionSummaryInfo;

    new-instance v0, Lcom/google/api/services/plus/model/ClientActionData;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientActionData;-><init>()V

    iput-object v2, v0, Lcom/google/api/services/plus/model/ClientActionData;->rhsComponent:Lcom/google/api/services/plus/model/ClientLoggedRhsComponent;

    iput-object v0, v1, Lcom/google/api/services/plus/model/ClientOzEvent;->actionData:Lcom/google/api/services/plus/model/ClientActionData;

    return-object v1

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static varargs toPeopleSuggestionsShownClientOzEvent(I[Ljava/util/List;)Lcom/google/api/services/plus/model/ClientOzEvent;
    .locals 7
    .param p0    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/api/services/plus/model/ClientOzEvent;"
        }
    .end annotation

    new-instance v3, Lcom/google/android/social/api/logging/PlusAnalytics;

    sget-object v4, Lcom/google/android/social/api/logging/Rhs$Action;->ITEMS_SHOWN:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    sget-object v5, Lcom/google/android/social/api/logging/Aspen$View;->PEOPLE_SUGGEST_WIDGET:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/social/api/logging/PlusAnalytics;-><init>(Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;)V

    invoke-direct {v3}, Lcom/google/android/social/api/logging/PlusAnalytics;->toClientOzEvent()Lcom/google/api/services/plus/model/ClientOzEvent;

    move-result-object v1

    new-instance v2, Lcom/google/api/services/plus/model/ClientLoggedRhsComponent;

    invoke-direct {v2}, Lcom/google/api/services/plus/model/ClientLoggedRhsComponent;-><init>()V

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/social/api/logging/PlusAnalytics;->createDefaultSuggestionSummary(Ljava/lang/Integer;)Lcom/google/api/services/plus/model/ClientLoggedSuggestionSummaryInfo;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plus/model/ClientLoggedRhsComponent;->suggestionSummaryInfo:Lcom/google/api/services/plus/model/ClientLoggedSuggestionSummaryInfo;

    invoke-static {p1}, Lcom/google/android/social/api/logging/PlusAnalytics;->createShownSuggestionsItemList([Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plus/model/ClientLoggedRhsComponent;->item:Ljava/util/List;

    new-instance v0, Lcom/google/api/services/plus/model/ClientActionData;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientActionData;-><init>()V

    iput-object v2, v0, Lcom/google/api/services/plus/model/ClientActionData;->rhsComponent:Lcom/google/api/services/plus/model/ClientLoggedRhsComponent;

    iput-object v0, v1, Lcom/google/api/services/plus/model/ClientOzEvent;->actionData:Lcom/google/api/services/plus/model/ClientActionData;

    sget-object v3, Lcom/google/android/social/api/logging/PlusAnalytics;->DEFAULT_RHS_OZ_EVENT:Lcom/google/api/services/plus/model/OzEvent;

    iput-object v3, v1, Lcom/google/api/services/plus/model/ClientOzEvent;->ozEvent:Lcom/google/api/services/plus/model/OzEvent;

    return-object v1
.end method

.method public static toPlusActionClientOzEvent(Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;)Lcom/google/api/services/plus/model/ClientOzEvent;
    .locals 2
    .param p0    # Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .param p1    # Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    new-instance v0, Lcom/google/android/social/api/logging/PlusAnalytics;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/social/api/logging/PlusAnalytics;-><init>(Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;)V

    invoke-direct {v0}, Lcom/google/android/social/api/logging/PlusAnalytics;->toClientOzEvent()Lcom/google/api/services/plus/model/ClientOzEvent;

    move-result-object v0

    return-object v0
.end method

.method public static toRemoveCircleMemberClientOzEvent(Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plus/model/ClientOzEvent;
    .locals 1
    .param p0    # Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v0, p0, p1, p2}, Lcom/google/android/social/api/logging/PlusAnalytics;->createCircleMemberShipChangeAction(ZLcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plus/model/ClientOzEvent;

    move-result-object v0

    return-object v0
.end method
