.class public final Lcom/google/android/social/api/logging/Rhs$Action;
.super Ljava/lang/Object;
.source "Rhs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/logging/Rhs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Action"
.end annotation


# static fields
.field public static final HOVERED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

.field public static final ITEMS_SHOWN:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

.field public static final ITEM_ACCEPTED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

.field public static final ITEM_CLICKED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

.field public static final ITEM_REJECTED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

.field public static final VIEW_ALL_CLICKED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    # invokes: Lcom/google/android/social/api/logging/Rhs;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/Rhs;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/Rhs$Action;->ITEMS_SHOWN:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    const/4 v0, 0x2

    # invokes: Lcom/google/android/social/api/logging/Rhs;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/Rhs;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/Rhs$Action;->HOVERED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    const/4 v0, 0x3

    # invokes: Lcom/google/android/social/api/logging/Rhs;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/Rhs;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/Rhs$Action;->ITEM_CLICKED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    const/4 v0, 0x4

    # invokes: Lcom/google/android/social/api/logging/Rhs;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/Rhs;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/Rhs$Action;->ITEM_ACCEPTED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    const/4 v0, 0x5

    # invokes: Lcom/google/android/social/api/logging/Rhs;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/Rhs;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/Rhs$Action;->ITEM_REJECTED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    const/4 v0, 0x6

    # invokes: Lcom/google/android/social/api/logging/Rhs;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/Rhs;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/Rhs$Action;->VIEW_ALL_CLICKED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
