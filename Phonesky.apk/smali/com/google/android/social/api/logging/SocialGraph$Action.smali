.class public final Lcom/google/android/social/api/logging/SocialGraph$Action;
.super Ljava/lang/Object;
.source "SocialGraph.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/logging/SocialGraph;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Action"
.end annotation


# static fields
.field public static final ADD_CIRCLE_MEMBERS:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

.field public static final CHANGE_CIRCLE_DESCRIPTION:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

.field public static final CHANGE_CIRCLE_NAME:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

.field public static final CREATE_CIRCLE:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

.field public static final CREATE_CIRCLE_CANCELED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

.field public static final DELETE_CIRCLE:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

.field public static final DELETE_CIRCLE_CANCELED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

.field public static final REMOVE_CIRCLE_MEMBERS:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    # invokes: Lcom/google/android/social/api/logging/SocialGraph;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/SocialGraph;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/SocialGraph$Action;->CREATE_CIRCLE:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    const/4 v0, 0x4

    # invokes: Lcom/google/android/social/api/logging/SocialGraph;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/SocialGraph;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/SocialGraph$Action;->CREATE_CIRCLE_CANCELED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    const/4 v0, 0x5

    # invokes: Lcom/google/android/social/api/logging/SocialGraph;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/SocialGraph;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/SocialGraph$Action;->DELETE_CIRCLE:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    const/4 v0, 0x6

    # invokes: Lcom/google/android/social/api/logging/SocialGraph;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/SocialGraph;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/SocialGraph$Action;->DELETE_CIRCLE_CANCELED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    const/16 v0, 0xc

    # invokes: Lcom/google/android/social/api/logging/SocialGraph;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/SocialGraph;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/SocialGraph$Action;->ADD_CIRCLE_MEMBERS:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    const/16 v0, 0xd

    # invokes: Lcom/google/android/social/api/logging/SocialGraph;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/SocialGraph;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/SocialGraph$Action;->REMOVE_CIRCLE_MEMBERS:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    const/16 v0, 0xe

    # invokes: Lcom/google/android/social/api/logging/SocialGraph;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/SocialGraph;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/SocialGraph$Action;->CHANGE_CIRCLE_NAME:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    const/16 v0, 0xf

    # invokes: Lcom/google/android/social/api/logging/SocialGraph;->build(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    invoke-static {v0}, Lcom/google/android/social/api/logging/SocialGraph;->access$000(I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/logging/SocialGraph$Action;->CHANGE_CIRCLE_DESCRIPTION:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
