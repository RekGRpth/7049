.class public abstract Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;
.super Lcom/google/android/social/api/network/ApiaryHttpRequest;
.source "ApiaryJsonHttpRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$VolleyJsonRequest;,
        Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Data:",
        "Ljava/lang/Object;",
        "RequestType:",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        "ResponseType:",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        ">",
        "Lcom/google/android/social/api/network/ApiaryHttpRequest",
        "<TData;>;"
    }
.end annotation


# static fields
.field private static final log:Lcom/google/android/social/api/internal/GLog;


# instance fields
.field private final configuration:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration",
            "<TRequestType;TResponseType;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/social/api/internal/GLog;

    invoke-direct {v0}, Lcom/google/android/social/api/internal/GLog;-><init>()V

    sput-object v0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->log:Lcom/google/android/social/api/internal/GLog;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration",
            "<TRequestType;TResponseType;>;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;

    invoke-direct {v0}, Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p4    # Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration",
            "<TRequestType;TResponseType;>;",
            "Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/social/api/network/ApiaryHttpRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;)V

    iput-object p3, p0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->configuration:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
    .locals 1
    .param p0    # Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;

    iget-object v0, p0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->configuration:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;

    invoke-direct {p0}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->getRequestString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200()Lcom/google/android/social/api/internal/GLog;
    .locals 1

    sget-object v0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->log:Lcom/google/android/social/api/internal/GLog;

    return-object v0
.end method

.method private getRequestString()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->buildRequest()Lcom/google/android/apps/plus/json/GenericJson;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->configuration:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    iget-object v1, v1, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;->jsonWriter:Lcom/google/android/apps/plus/json/EsJson;

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->configuration:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    iget-object v1, v1, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;->jsonWriter:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/json/EsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method protected buildRequest()Lcom/google/android/apps/plus/json/GenericJson;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRequestType;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getRequest(Lcom/google/android/social/api/network/RequestFuture;)Lcom/android/volley/Request;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/social/api/network/RequestFuture",
            "<TData;>;)",
            "Lcom/android/volley/Request",
            "<TData;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$VolleyJsonRequest;

    invoke-direct {v0, p0, p1}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$VolleyJsonRequest;-><init>(Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;Lcom/google/android/social/api/network/RequestFuture;)V

    return-object v0
.end method

.method protected abstract processResponseData(Lcom/google/android/apps/plus/json/GenericJson;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResponseType;)TData;"
        }
    .end annotation
.end method

.method protected processResponseData(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")TData;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->configuration:Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    iget-object v0, v0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;->jsonReader:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/json/EsJson;->fromInputStream(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/json/GenericJson;

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;->processResponseData(Lcom/google/android/apps/plus/json/GenericJson;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
