.class Lcom/google/android/social/api/people/activities/CircleSelectionActivity$HiddenCircleAwareEditAudienceAdapter;
.super Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;
.source "CircleSelectionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/activities/CircleSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HiddenCircleAwareEditAudienceAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/social/api/people/activities/CircleSelectionActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/people/activities/CircleSelectionActivity;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity$HiddenCircleAwareEditAudienceAdapter;->this$0:Lcom/google/android/social/api/people/activities/CircleSelectionActivity;

    invoke-direct {p0, p2}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic getCircleView(Lcom/google/android/social/api/people/model/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity$HiddenCircleAwareEditAudienceAdapter;->getCircleView(Lcom/google/android/social/api/people/model/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;

    move-result-object v0

    return-object v0
.end method

.method protected getCircleView(Lcom/google/android/social/api/people/model/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;
    .locals 3
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getCircleView(Lcom/google/android/social/api/people/model/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/AudienceMember;->isHidden()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0200ea

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;->setIcon(I)V

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity$HiddenCircleAwareEditAudienceAdapter;->this$0:Lcom/google/android/social/api/people/activities/CircleSelectionActivity;

    invoke-virtual {v1}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a004d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;->setBackgroundColor(I)V

    :goto_0
    return-object v0

    :cond_0
    const v1, 0x7f0200e9

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;->setIcon(I)V

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity$HiddenCircleAwareEditAudienceAdapter;->this$0:Lcom/google/android/social/api/people/activities/CircleSelectionActivity;

    invoke-virtual {v1}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a004c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method protected swapCircles(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    # getter for: Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->CIRCLE_COMPARER:Ljava/util/Comparator;
    invoke-static {}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->access$100()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-super {p0, p1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->swapCircles(Ljava/util/List;)V

    return-void
.end method
