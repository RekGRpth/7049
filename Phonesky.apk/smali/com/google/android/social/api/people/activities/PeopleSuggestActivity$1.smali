.class Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$1;
.super Landroid/os/AsyncTask;
.source "PeopleSuggestActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->shouldPopupShow(Lcom/google/android/social/api/people/model/Person;I)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;


# direct methods
.method constructor <init>(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$1;->this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1    # [Ljava/lang/Void;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$1;->this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    const-string v1, "plus_people_suggest_preferences"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "suggest_popup_shown"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v0, 0x0

    return-object v0
.end method
