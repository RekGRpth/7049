.class final Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$AddToCirclesClientRequest;
.super Ljava/lang/Object;
.source "PeopleSuggestActivity.java"

# interfaces
.implements Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusInternalClientRequest;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AddToCirclesClientRequest"
.end annotation


# instance fields
.field private final personBeingEdited:Lcom/google/android/social/api/people/model/Person;

.field private final selectedCirclesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/people/model/Person;Ljava/util/ArrayList;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/model/Person;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/social/api/people/model/Person;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$AddToCirclesClientRequest;->personBeingEdited:Lcom/google/android/social/api/people/model/Person;

    iput-object p2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$AddToCirclesClientRequest;->selectedCirclesList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public execute(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Lcom/google/android/social/api/service/PlusInternalClient;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;
    .param p2    # Lcom/google/android/social/api/service/PlusInternalClient;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$AddToCirclesClientRequest;->personBeingEdited:Lcom/google/android/social/api/people/model/Person;

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$AddToCirclesClientRequest;->selectedCirclesList:Ljava/util/ArrayList;

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/social/api/service/PlusInternalClient;->addToCircles(Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;Lcom/google/android/social/api/people/model/Person;Ljava/util/ArrayList;)V

    return-void
.end method
