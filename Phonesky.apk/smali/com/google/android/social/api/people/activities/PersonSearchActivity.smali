.class public Lcom/google/android/social/api/people/activities/PersonSearchActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "PersonSearchActivity.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
.implements Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;


# instance fields
.field private accountName:Ljava/lang/String;

.field private apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

.field private currentSearchQuery:Ljava/lang/String;

.field private pendingSearchQuery:Ljava/lang/String;

.field private searchAdapter:Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;

.field private searchList:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/social/api/people/activities/PersonSearchActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/social/api/people/activities/PersonSearchActivity;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->currentSearchQuery:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/social/api/people/activities/PersonSearchActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/social/api/people/activities/PersonSearchActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->search(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/social/api/people/activities/PersonSearchActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/social/api/people/activities/PersonSearchActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->pendingSearchQuery:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/social/api/people/activities/PersonSearchActivity;)Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;
    .locals 1
    .param p0    # Lcom/google/android/social/api/people/activities/PersonSearchActivity;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->searchAdapter:Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;

    return-object v0
.end method

.method private search(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->currentSearchQuery:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/social/api/service/PlusInternalClient;->searchAudience(Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onConnected()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->currentSearchQuery:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->currentSearchQuery:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->search(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f0400ed

    invoke-virtual {p0, v1}, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->accountName:Ljava/lang/String;

    const v1, 0x7f0801e9

    invoke-virtual {p0, v1}, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/social/api/people/activities/PersonSearchActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/social/api/people/activities/PersonSearchActivity$1;-><init>(Lcom/google/android/social/api/people/activities/PersonSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v1, Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;-><init>(Landroid/view/LayoutInflater;)V

    iput-object v1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->searchAdapter:Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;

    const v1, 0x7f0801dc

    invoke-virtual {p0, v1}, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->searchList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->searchList:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->searchList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->searchAdapter:Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->searchList:Landroid/widget/ListView;

    new-instance v2, Lcom/google/android/social/api/people/activities/PersonSearchActivity$2;

    invoke-direct {v2, p0}, Lcom/google/android/social/api/people/activities/PersonSearchActivity$2;-><init>(Lcom/google/android/social/api/people/activities/PersonSearchActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->accountName:Ljava/lang/String;

    const-string v2, "Account name must not be null."

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/social/api/service/PlusInternalClient;

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->accountName:Ljava/lang/String;

    invoke-direct {v1, p0, p0, p0, v2}, Lcom/google/android/social/api/service/PlusInternalClient;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    return-void
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method public onPeopleSearch(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/social/api/people/model/PersonList;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->currentSearchQuery:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->searchAdapter:Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/social/api/people/adapters/PersonSearchAdapter;->setSearchResults(Lcom/google/android/social/api/people/model/PersonList;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->pendingSearchQuery:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->pendingSearchQuery:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->search(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->pendingSearchQuery:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->connect()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->disconnect()V

    return-void
.end method
