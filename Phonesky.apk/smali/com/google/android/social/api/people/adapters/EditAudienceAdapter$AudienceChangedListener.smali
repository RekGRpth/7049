.class public interface abstract Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;
.super Ljava/lang/Object;
.source "EditAudienceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AudienceChangedListener"
.end annotation


# virtual methods
.method public abstract audienceMemberAdded(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;Lcom/google/android/social/api/people/model/AudienceMember;)V
.end method

.method public abstract audienceMemberRemoved(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;Lcom/google/android/social/api/people/model/AudienceMember;)V
.end method
