.class final Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;
.super Ljava/lang/Object;
.source "EditAudienceAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CheckedListener"
.end annotation


# instance fields
.field private final audienceMember:Lcom/google/android/social/api/people/model/AudienceMember;

.field final synthetic this$0:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;Lcom/google/android/social/api/people/model/AudienceMember;)V
    .locals 0
    .param p2    # Lcom/google/android/social/api/people/model/AudienceMember;

    iput-object p1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->audienceMember:Lcom/google/android/social/api/people/model/AudienceMember;

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    # getter for: Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mListener:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;
    invoke-static {v0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->access$200(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;)Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    # getter for: Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->access$300(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->audienceMember:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    # getter for: Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->access$300(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->audienceMember:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    # getter for: Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mListener:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;
    invoke-static {v0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->access$200(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;)Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    iget-object v2, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->audienceMember:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-interface {v0, v1, v2}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;->audienceMemberAdded(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;Lcom/google/android/social/api/people/model/AudienceMember;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    # getter for: Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->access$300(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->audienceMember:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    # getter for: Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->access$300(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->audienceMember:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    # getter for: Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mListener:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;
    invoke-static {v0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->access$200(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;)Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->this$0:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    iget-object v2, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;->audienceMember:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-interface {v0, v1, v2}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;->audienceMemberRemoved(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;Lcom/google/android/social/api/people/model/AudienceMember;)V

    goto :goto_0
.end method
