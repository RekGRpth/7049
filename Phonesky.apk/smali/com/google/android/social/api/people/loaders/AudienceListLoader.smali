.class public Lcom/google/android/social/api/people/loaders/AudienceListLoader;
.super Lcom/google/android/social/api/loaders/PlusApiLoader;
.source "AudienceListLoader.java"

# interfaces
.implements Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;


# instance fields
.field private audience:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private final includeCircles:Z

.field private final includeGroups:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZZ)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/social/api/loaders/PlusApiLoader;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-boolean p3, p0, Lcom/google/android/social/api/people/loaders/AudienceListLoader;->includeGroups:Z

    iput-boolean p4, p0, Lcom/google/android/social/api/people/loaders/AudienceListLoader;->includeCircles:Z

    return-void
.end method


# virtual methods
.method public getAudience()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/people/loaders/AudienceListLoader;->audience:Ljava/util/ArrayList;

    return-object v0
.end method

.method public onAudiencesList(Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    iput-object p2, p0, Lcom/google/android/social/api/people/loaders/AudienceListLoader;->audience:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/loaders/AudienceListLoader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/loaders/AudienceListLoader;->deliverResult(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method protected onConnected(Lcom/google/android/social/api/service/PlusInternalClient;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/service/PlusInternalClient;

    iget-boolean v0, p0, Lcom/google/android/social/api/people/loaders/AudienceListLoader;->includeGroups:Z

    iget-boolean v1, p0, Lcom/google/android/social/api/people/loaders/AudienceListLoader;->includeCircles:Z

    invoke-virtual {p1, p0, v0, v1}, Lcom/google/android/social/api/service/PlusInternalClient;->loadSocialNetwork(Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;ZZ)V

    return-void
.end method
