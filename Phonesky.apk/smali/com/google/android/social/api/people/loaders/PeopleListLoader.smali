.class public Lcom/google/android/social/api/people/loaders/PeopleListLoader;
.super Lcom/google/android/social/api/loaders/PlusApiLoader;
.source "PeopleListLoader.java"

# interfaces
.implements Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;


# instance fields
.field private personList:Lcom/google/android/social/api/people/model/PersonList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/social/api/loaders/PlusApiLoader;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getPersonList()Lcom/google/android/social/api/people/model/PersonList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/loaders/PeopleListLoader;->personList:Lcom/google/android/social/api/people/model/PersonList;

    return-object v0
.end method

.method protected onConnected(Lcom/google/android/social/api/service/PlusInternalClient;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {p1, p0}, Lcom/google/android/social/api/service/PlusInternalClient;->peopleList(Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;)V

    return-void
.end method

.method public onPeopleList(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2    # Lcom/google/android/social/api/people/model/PersonList;

    iput-object p2, p0, Lcom/google/android/social/api/people/loaders/PeopleListLoader;->personList:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/loaders/PeopleListLoader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/loaders/PeopleListLoader;->deliverResult(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
