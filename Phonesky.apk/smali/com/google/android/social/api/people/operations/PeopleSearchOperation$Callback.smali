.class public interface abstract Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;
.super Ljava/lang/Object;
.source "PeopleSearchOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/operations/PeopleSearchOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation


# virtual methods
.method public abstract onPeopleSearch(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V
.end method
