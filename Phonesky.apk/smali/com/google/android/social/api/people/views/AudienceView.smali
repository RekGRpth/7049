.class public Lcom/google/android/social/api/people/views/AudienceView;
.super Landroid/widget/FrameLayout;
.source "AudienceView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;,
        Lcom/google/android/social/api/people/views/AudienceView$AudienceChangedListener;
    }
.end annotation


# instance fields
.field private final mAudience:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mViewEditIcon:Landroid/widget/ImageView;

.field private final mViewList:Lcom/google/android/social/api/people/views/MultiLineLayoutView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/social/api/people/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/social/api/people/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/social/api/people/views/AudienceView;->mAudience:Ljava/util/LinkedHashMap;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/google/android/social/api/people/views/AudienceView;->mInflater:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/google/android/social/api/people/views/AudienceView;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f0400e7

    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0801d7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/social/api/people/views/MultiLineLayoutView;

    iput-object v1, p0, Lcom/google/android/social/api/people/views/AudienceView;->mViewList:Lcom/google/android/social/api/people/views/MultiLineLayoutView;

    const v1, 0x7f0801d8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/social/api/people/views/AudienceView;->mViewEditIcon:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/social/api/people/views/AudienceView;->setSaveEnabled(Z)V

    return-void
.end method


# virtual methods
.method public add(Lcom/google/android/social/api/people/model/AudienceMember;)V
    .locals 3
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "AudienceMember can not be null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/social/api/people/views/AudienceView;->mAudience:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/social/api/people/views/AudienceView;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p0, p1, v1}, Lcom/google/android/social/api/people/views/AudienceView;->getChipView(Lcom/google/android/social/api/people/model/AudienceMember;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/social/api/people/views/AudienceView;->mAudience:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/social/api/people/views/AudienceView;->mViewList:Lcom/google/android/social/api/people/views/MultiLineLayoutView;

    invoke-virtual {v1, v0}, Lcom/google/android/social/api/people/views/MultiLineLayoutView;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/views/AudienceView;->invalidate()V

    :cond_1
    return-void
.end method

.method public addAll(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {p0, v1}, Lcom/google/android/social/api/people/views/AudienceView;->add(Lcom/google/android/social/api/people/model/AudienceMember;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/views/AudienceView;->mAudience:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    iget-object v0, p0, Lcom/google/android/social/api/people/views/AudienceView;->mViewList:Lcom/google/android/social/api/people/views/MultiLineLayoutView;

    invoke-virtual {v0}, Lcom/google/android/social/api/people/views/MultiLineLayoutView;->removeAllViews()V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/views/AudienceView;->invalidate()V

    return-void
.end method

.method public final getAudience()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/social/api/people/views/AudienceView;->mAudience:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method protected getChipBackground(Lcom/google/android/social/api/people/model/AudienceMember;)I
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;

    sget-object v0, Lcom/google/android/social/api/people/model/AudienceMember;->DOMAIN:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/social/api/people/model/AudienceMember;->EXTENDED_CIRCLES:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/social/api/people/model/AudienceMember;->PUBLIC:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const v0, 0x7f0200de

    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/google/android/social/api/people/model/AudienceMember;->ONLY_YOU:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0200df

    goto :goto_0

    :cond_2
    const v0, 0x7f0200dc

    goto :goto_0
.end method

.method protected getChipBorderBottom(Lcom/google/android/social/api/people/model/AudienceMember;)I
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;

    const/4 v0, 0x0

    return v0
.end method

.method protected getChipBorderLeft(Lcom/google/android/social/api/people/model/AudienceMember;)I
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;

    sget-object v0, Lcom/google/android/social/api/people/model/AudienceMember;->DOMAIN:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0200e2

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/social/api/people/model/AudienceMember;->PUBLIC:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0200e5

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/social/api/people/model/AudienceMember;->YOUR_CIRCLES:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0200e1

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/social/api/people/model/AudienceMember;->EXTENDED_CIRCLES:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0200e3

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/social/api/people/model/AudienceMember;->ONLY_YOU:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f0200e4

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getChipBorderRight(Lcom/google/android/social/api/people/model/AudienceMember;)I
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;

    const/4 v0, 0x0

    return v0
.end method

.method protected getChipBorderTop(Lcom/google/android/social/api/people/model/AudienceMember;)I
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;

    const/4 v0, 0x0

    return v0
.end method

.method protected getChipText(Lcom/google/android/social/api/people/model/AudienceMember;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/android/social/api/people/model/AudienceMember;->PUBLIC:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {p1, v0}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/social/api/people/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/social/api/people/model/AudienceMember;->EXTENDED_CIRCLES:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {p1, v0}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/social/api/people/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/social/api/people/model/AudienceMember;->YOUR_CIRCLES:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {p1, v0}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/social/api/people/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/social/api/people/model/AudienceMember;->ONLY_YOU:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {p1, v0}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/social/api/people/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected getChipView(Lcom/google/android/social/api/people/model/AudienceMember;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 6
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;
    .param p2    # Landroid/view/LayoutInflater;

    const v2, 0x7f0400e8

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0801d9

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/views/AudienceView;->getChipText(Lcom/google/android/social/api/people/model/AudienceMember;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/views/AudienceView;->getChipBackground(Lcom/google/android/social/api/people/model/AudienceMember;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/views/AudienceView;->getChipBorderLeft(Lcom/google/android/social/api/people/model/AudienceMember;)I

    move-result v2

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/views/AudienceView;->getChipBorderTop(Lcom/google/android/social/api/people/model/AudienceMember;)I

    move-result v3

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/views/AudienceView;->getChipBorderRight(Lcom/google/android/social/api/people/model/AudienceMember;)I

    move-result v4

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/views/AudienceView;->getChipBorderBottom(Lcom/google/android/social/api/people/model/AudienceMember;)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    return-object v0
.end method

.method public final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/views/AudienceView;->mAudience:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "parent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v1, "audience"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/social/api/people/views/AudienceView;->set(Ljava/util/List;)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "parent"

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "audience"

    invoke-virtual {p0}, Lcom/google/android/social/api/people/views/AudienceView;->getAudience()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-object v0
.end method

.method public remove(Lcom/google/android/social/api/people/model/AudienceMember;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;

    iget-object v0, p0, Lcom/google/android/social/api/people/views/AudienceView;->mAudience:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/people/views/AudienceView;->mViewList:Lcom/google/android/social/api/people/views/MultiLineLayoutView;

    iget-object v0, p0, Lcom/google/android/social/api/people/views/AudienceView;->mAudience:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Lcom/google/android/social/api/people/views/MultiLineLayoutView;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/social/api/people/views/AudienceView;->mAudience:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/views/AudienceView;->invalidate()V

    :cond_0
    return-void
.end method

.method public set(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/social/api/people/views/AudienceView;->clear()V

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/views/AudienceView;->addAll(Ljava/util/List;)V

    return-void
.end method

.method public setClickable(Z)V
    .locals 0
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setClickable(Z)V

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/views/AudienceView;->setShowEditIcon(Z)V

    return-void
.end method

.method public setShowEditIcon(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/social/api/people/views/AudienceView;->mViewEditIcon:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
