.class public Lcom/google/android/social/api/service/OperationsList;
.super Ljava/lang/Object;
.source "OperationsList.java"


# static fields
.field private static instance:Lcom/google/android/social/api/service/OperationsList;


# instance fields
.field private final operations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/operations/PlusApiOperation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/service/OperationsList;->operations:Ljava/util/List;

    return-void
.end method

.method public static getInstance()Lcom/google/android/social/api/service/OperationsList;
    .locals 1

    sget-object v0, Lcom/google/android/social/api/service/OperationsList;->instance:Lcom/google/android/social/api/service/OperationsList;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/social/api/service/OperationsList;

    invoke-direct {v0}, Lcom/google/android/social/api/service/OperationsList;-><init>()V

    sput-object v0, Lcom/google/android/social/api/service/OperationsList;->instance:Lcom/google/android/social/api/service/OperationsList;

    :cond_0
    sget-object v0, Lcom/google/android/social/api/service/OperationsList;->instance:Lcom/google/android/social/api/service/OperationsList;

    return-object v0
.end method


# virtual methods
.method public addOperation(Lcom/google/android/social/api/operations/PlusApiOperation;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/operations/PlusApiOperation;

    iget-object v0, p0, Lcom/google/android/social/api/service/OperationsList;->operations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getAndClearOperations()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/operations/PlusApiOperation;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/social/api/service/OperationsList;->operations:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lcom/google/android/social/api/service/OperationsList;->operations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    return-object v0
.end method
