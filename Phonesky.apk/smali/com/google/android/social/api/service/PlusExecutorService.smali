.class public abstract Lcom/google/android/social/api/service/PlusExecutorService;
.super Landroid/app/Service;
.source "PlusExecutorService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/service/PlusExecutorService$ServiceRunnable;
    }
.end annotation


# instance fields
.field private final executorService:Ljava/util/concurrent/ExecutorService;

.field private final startedTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/google/android/social/api/service/PlusExecutorService;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/atomic/AtomicInteger;)V

    return-void
.end method

.method protected constructor <init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/atomic/AtomicInteger;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
    .param p2    # Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/service/PlusExecutorService;->executorService:Ljava/util/concurrent/ExecutorService;

    iput-object p2, p0, Lcom/google/android/social/api/service/PlusExecutorService;->startedTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method


# virtual methods
.method protected doStopSelf()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/social/api/service/PlusExecutorService;->stopSelf()V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusExecutorService;->executorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    return-void
.end method

.method protected abstract onHandleIntent(Landroid/content/Intent;)V
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusExecutorService;->startedTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusExecutorService;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/social/api/service/PlusExecutorService$ServiceRunnable;

    invoke-direct {v1, p0, p1}, Lcom/google/android/social/api/service/PlusExecutorService$ServiceRunnable;-><init>(Lcom/google/android/social/api/service/PlusExecutorService;Landroid/content/Intent;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    const/4 v0, 0x2

    return v0
.end method

.method runComplete()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusExecutorService;->startedTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/social/api/service/PlusExecutorService;->doStopSelf()V

    :cond_0
    return-void
.end method
