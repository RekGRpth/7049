.class final Lcom/google/android/social/api/service/PlusInternalClient$CirclePersonBinderCallbacks;
.super Lcom/google/android/social/api/service/IPlusInternalCallbacks$Stub;
.source "PlusInternalClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/service/PlusInternalClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "CirclePersonBinderCallbacks"
.end annotation


# instance fields
.field private final listener:Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;

.field final synthetic this$0:Lcom/google/android/social/api/service/PlusInternalClient;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;)V
    .locals 0
    .param p2    # Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;

    iput-object p1, p0, Lcom/google/android/social/api/service/PlusInternalClient$CirclePersonBinderCallbacks;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-direct {p0}, Lcom/google/android/social/api/service/IPlusInternalCallbacks$Stub;-><init>()V

    iput-object p2, p0, Lcom/google/android/social/api/service/PlusInternalClient$CirclePersonBinderCallbacks;->listener:Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;

    return-void
.end method


# virtual methods
.method public onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 6
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Landroid/os/Bundle;

    const-string v1, "person"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/model/Person;

    iget-object v1, p0, Lcom/google/android/social/api/service/PlusInternalClient$CirclePersonBinderCallbacks;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    new-instance v2, Lcom/google/android/social/api/service/PlusInternalClient$CirclePersonCallback;

    iget-object v3, p0, Lcom/google/android/social/api/service/PlusInternalClient$CirclePersonBinderCallbacks;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    iget-object v4, p0, Lcom/google/android/social/api/service/PlusInternalClient$CirclePersonBinderCallbacks;->listener:Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;

    invoke-static {p1, p2}, Lcom/google/android/social/api/service/Results;->getConnectionResult(ILandroid/os/Bundle;)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/google/android/social/api/service/PlusInternalClient$CirclePersonCallback;-><init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/Person;)V

    invoke-virtual {v1, v2}, Lcom/google/android/social/api/service/PlusInternalClient;->doCallback(Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;)V

    return-void
.end method
