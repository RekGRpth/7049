.class public Lcom/google/android/social/api/service/PlusInternalClient;
.super Lcom/google/android/gms/common/internal/GmsClient;
.source "PlusInternalClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/service/PlusInternalClient$CirclePersonBinderCallbacks;,
        Lcom/google/android/social/api/service/PlusInternalClient$CirclePersonCallback;,
        Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionBinderCallbacks;,
        Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionCallback;,
        Lcom/google/android/social/api/service/PlusInternalClient$PeopleListBinderCallbacks;,
        Lcom/google/android/social/api/service/PlusInternalClient$PeopleListCallback;,
        Lcom/google/android/social/api/service/PlusInternalClient$SearchAudienceBinderCallbacks;,
        Lcom/google/android/social/api/service/PlusInternalClient$SearchAudienceCallback;,
        Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogCallback;,
        Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogBinderCallbacks;,
        Lcom/google/android/social/api/service/PlusInternalClient$AudiencesListBinderCallbacks;,
        Lcom/google/android/social/api/service/PlusInternalClient$AudiencesListCallback;,
        Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageBinderCallbacks;,
        Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/GmsClient",
        "<",
        "Lcom/google/android/social/api/service/IPlusInternalService;",
        ">;"
    }
.end annotation


# static fields
.field public static final STATUS_INTERNAL_ERROR:Lcom/google/android/gms/common/ConnectionResult;


# instance fields
.field private final accountName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    sput-object v0, Lcom/google/android/social/api/service/PlusInternalClient;->STATUS_INTERNAL_ERROR:Lcom/google/android/gms/common/ConnectionResult;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
    .param p3    # Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
    .param p4    # Ljava/lang/String;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/common/internal/GmsClient;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    iput-object p4, p0, Lcom/google/android/social/api/service/PlusInternalClient;->accountName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/social/api/service/PlusInternalClient;Ljava/lang/String;II)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/social/api/service/PlusInternalClient;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/social/api/service/PlusInternalClient;->makeImageKey(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/social/api/service/PlusInternalClient;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {p0}, Lcom/google/android/social/api/service/PlusInternalClient;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static getAction(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/content/Context;

    const-string v0, "%s.%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "GOOGLE_PLUS_API"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private makeImageKey(Ljava/lang/String;II)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    const-string v0, "%s:%d:%d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private postInsertLog(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;Lcom/google/api/services/plus/model/ClientOzEvent;)V
    .locals 5
    .param p1    # Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;
    .param p2    # Lcom/google/api/services/plus/model/ClientOzEvent;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-static {}, Lcom/google/api/services/plus/model/ClientOzEventJson;->getInstance()Lcom/google/api/services/plus/model/ClientOzEventJson;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/google/api/services/plus/model/ClientOzEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v1

    const-string v3, "client_logs"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/social/api/service/PlusInternalClient;->getService()Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/google/android/social/api/service/IPlusInternalService;

    new-instance v4, Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogBinderCallbacks;

    invoke-direct {v4, p0, p1}, Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogBinderCallbacks;-><init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;)V

    invoke-interface {v3, v4, v0}, Lcom/google/android/social/api/service/IPlusInternalService;->postInsertLog(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v2

    sget-object v3, Lcom/google/android/social/api/service/PlusInternalClient;->STATUS_INTERNAL_ERROR:Lcom/google/android/gms/common/ConnectionResult;

    invoke-interface {p1, v3}, Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;->onPostInsertLog(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0
.end method


# virtual methods
.method public addToCircles(Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;Lcom/google/android/social/api/people/model/Person;Ljava/util/ArrayList;)V
    .locals 4
    .param p1    # Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;
    .param p2    # Lcom/google/android/social/api/people/model/Person;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;",
            "Lcom/google/android/social/api/people/model/Person;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "person"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "circles"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/social/api/service/PlusInternalClient;->getService()Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/google/android/social/api/service/IPlusInternalService;

    new-instance v3, Lcom/google/android/social/api/service/PlusInternalClient$CirclePersonBinderCallbacks;

    invoke-direct {v3, p0, p1}, Lcom/google/android/social/api/service/PlusInternalClient$CirclePersonBinderCallbacks;-><init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;)V

    invoke-interface {v2, v3, v0}, Lcom/google/android/social/api/service/IPlusInternalService;->addToCircles(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    sget-object v2, Lcom/google/android/social/api/service/PlusInternalClient;->STATUS_INTERNAL_ERROR:Lcom/google/android/gms/common/ConnectionResult;

    invoke-interface {p1, v2, p2}, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;->onCirclesAddRemovePeople(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/Person;)V

    goto :goto_0
.end method

.method public clearCache()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/social/api/service/PlusInternalClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/social/api/internal/PlusApiContextUtil;->getPlusApiContext(Landroid/content/Context;)Lcom/google/android/social/api/PlusApiContext;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/social/api/PlusApiContext;->clearImageCache()V

    return-void
.end method

.method protected bridge synthetic createServiceInterface(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1
    .param p1    # Landroid/os/IBinder;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/service/PlusInternalClient;->createServiceInterface(Landroid/os/IBinder;)Lcom/google/android/social/api/service/IPlusInternalService;

    move-result-object v0

    return-object v0
.end method

.method protected createServiceInterface(Landroid/os/IBinder;)Lcom/google/android/social/api/service/IPlusInternalService;
    .locals 1
    .param p1    # Landroid/os/IBinder;

    invoke-static {p1}, Lcom/google/android/social/api/service/IPlusInternalService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/social/api/service/IPlusInternalService;

    move-result-object v0

    return-object v0
.end method

.method public downloadImage(Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;Ljava/lang/String;II)V
    .locals 17
    .param p1    # Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/social/api/service/PlusInternalClient;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/social/api/internal/PlusApiContextUtil;->getPlusApiContext(Landroid/content/Context;)Lcom/google/android/social/api/PlusApiContext;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/social/api/service/PlusInternalClient;->makeImageKey(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/social/api/PlusApiContext;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v9

    if-eqz v9, :cond_0

    sget-object v5, Lcom/google/android/gms/common/ConnectionResult;->RESULT_SUCCESS:Lcom/google/android/gms/common/ConnectionResult;

    move-object/from16 v4, p1

    move-object/from16 v6, p2

    move/from16 v7, p3

    move/from16 v8, p4

    invoke-interface/range {v4 .. v9}, Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;->onDownloadImage(Lcom/google/android/gms/common/ConnectionResult;Ljava/lang/String;IILandroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/social/api/service/PlusInternalClient;->getService()Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/google/android/social/api/service/IPlusInternalService;

    new-instance v5, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageBinderCallbacks;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v5, v0, v1}, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageBinderCallbacks;-><init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;)V

    move-object/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-interface {v4, v5, v0, v1, v2}, Lcom/google/android/social/api/service/IPlusInternalService;->downloadImage(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Ljava/lang/String;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v16

    sget-object v11, Lcom/google/android/social/api/service/PlusInternalClient;->STATUS_INTERNAL_ERROR:Lcom/google/android/gms/common/ConnectionResult;

    const/4 v15, 0x0

    move-object/from16 v10, p1

    move-object/from16 v12, p2

    move/from16 v13, p3

    move/from16 v14, p4

    invoke-interface/range {v10 .. v15}, Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;->onDownloadImage(Lcom/google/android/gms/common/ConnectionResult;Ljava/lang/String;IILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected getServiceDescriptor()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.social.api.service.IPlusInternalService"

    return-object v0
.end method

.method protected getServiceFromBroker(Lcom/google/android/gms/common/internal/IGmsServiceBroker;Lcom/google/android/gms/common/internal/GmsClient$GmsCallbacks;)V
    .locals 8
    .param p1    # Lcom/google/android/gms/common/internal/IGmsServiceBroker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/internal/IGmsServiceBroker;",
            "Lcom/google/android/gms/common/internal/GmsClient",
            "<",
            "Lcom/google/android/social/api/service/IPlusInternalService;",
            ">.GmsCallbacks;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x1

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v0, "skip_oob"

    invoke-virtual {v7, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/google/android/social/api/service/PlusInternalClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/social/api/service/PlusInternalClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/social/api/service/PlusInternalClient;->getScopes()[Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/social/api/service/PlusInternalClient;->accountName:Ljava/lang/String;

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/common/internal/IGmsServiceBroker;->getPlusService(Lcom/google/android/gms/common/internal/IGmsCallbacks;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method protected getStartServiceAction()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/social/api/service/PlusInternalClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->getAction(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ignoreSuggestion(Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/social/api/service/PlusInternalClient;->getService()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/social/api/service/IPlusInternalService;

    new-instance v2, Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionBinderCallbacks;

    invoke-direct {v2, p0, p1}, Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionBinderCallbacks;-><init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;)V

    invoke-interface {v1, v2, p2, p3}, Lcom/google/android/social/api/service/IPlusInternalService;->ignoreSuggestion(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v2, Lcom/google/android/social/api/service/PlusInternalClient;->STATUS_INTERNAL_ERROR:Lcom/google/android/gms/common/ConnectionResult;

    if-nez p3, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-interface {p1, v2, v1}, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;->onIgnoreSuggestion(Lcom/google/android/gms/common/ConnectionResult;Z)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public loadSocialNetwork(Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;ZZ)V
    .locals 3
    .param p1    # Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;
    .param p2    # Z
    .param p3    # Z

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/social/api/service/PlusInternalClient;->getService()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/social/api/service/IPlusInternalService;

    new-instance v2, Lcom/google/android/social/api/service/PlusInternalClient$AudiencesListBinderCallbacks;

    invoke-direct {v2, p0, p1}, Lcom/google/android/social/api/service/PlusInternalClient$AudiencesListBinderCallbacks;-><init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;)V

    invoke-interface {v1, v2, p2, p3}, Lcom/google/android/social/api/service/IPlusInternalService;->audiencesList(Lcom/google/android/social/api/service/IPlusInternalCallbacks;ZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/social/api/service/PlusInternalClient;->STATUS_INTERNAL_ERROR:Lcom/google/android/gms/common/ConnectionResult;

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;->onAudiencesList(Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public peopleList(Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;)V
    .locals 4
    .param p1    # Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/social/api/service/PlusInternalClient;->getService()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/social/api/service/IPlusInternalService;

    new-instance v2, Lcom/google/android/social/api/service/PlusInternalClient$PeopleListBinderCallbacks;

    invoke-direct {v2, p0, p1}, Lcom/google/android/social/api/service/PlusInternalClient$PeopleListBinderCallbacks;-><init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;)V

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-interface {v1, v2, v3}, Lcom/google/android/social/api/service/IPlusInternalService;->peopleList(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/social/api/service/PlusInternalClient;->STATUS_INTERNAL_ERROR:Lcom/google/android/gms/common/ConnectionResult;

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;->onPeopleList(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V

    goto :goto_0
.end method

.method public postInsertLogAction(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;
    .param p2    # Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .param p3    # Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    invoke-static {p2, p3}, Lcom/google/android/social/api/logging/PlusAnalytics;->toPlusActionClientOzEvent(Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;)Lcom/google/api/services/plus/model/ClientOzEvent;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/social/api/service/PlusInternalClient;->postInsertLog(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;Lcom/google/api/services/plus/model/ClientOzEvent;)V

    return-void
.end method

.method public postInsertLogPeopleSuggestionAction(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;
    .param p2    # Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .param p3    # Ljava/lang/String;

    invoke-static {p2, p3}, Lcom/google/android/social/api/logging/PlusAnalytics;->toPeopleSuggestionClientOzEvent(Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Ljava/lang/String;)Lcom/google/api/services/plus/model/ClientOzEvent;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/social/api/service/PlusInternalClient;->postInsertLog(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;Lcom/google/api/services/plus/model/ClientOzEvent;)V

    return-void
.end method

.method public varargs postInsertLogPeopleSuggestionsShown(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;I[Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;",
            "I[",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-static {p2, p3}, Lcom/google/android/social/api/logging/PlusAnalytics;->toPeopleSuggestionsShownClientOzEvent(I[Ljava/util/List;)Lcom/google/api/services/plus/model/ClientOzEvent;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/social/api/service/PlusInternalClient;->postInsertLog(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;Lcom/google/api/services/plus/model/ClientOzEvent;)V

    return-void
.end method

.method public searchAudience(Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;
    .param p2    # Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/social/api/service/PlusInternalClient;->getService()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/social/api/service/IPlusInternalService;

    new-instance v2, Lcom/google/android/social/api/service/PlusInternalClient$SearchAudienceBinderCallbacks;

    invoke-direct {v2, p0, p1}, Lcom/google/android/social/api/service/PlusInternalClient$SearchAudienceBinderCallbacks;-><init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;)V

    invoke-interface {v1, v2, p2}, Lcom/google/android/social/api/service/IPlusInternalService;->searchAudience(Lcom/google/android/social/api/service/IPlusInternalCallbacks;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/social/api/service/PlusInternalClient;->STATUS_INTERNAL_ERROR:Lcom/google/android/gms/common/ConnectionResult;

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;->onPeopleSearch(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V

    goto :goto_0
.end method
