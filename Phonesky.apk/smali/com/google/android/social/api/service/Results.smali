.class public final Lcom/google/android/social/api/service/Results;
.super Ljava/lang/Object;
.source "Results.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getConnectionResult(ILandroid/os/Bundle;)Lcom/google/android/gms/common/ConnectionResult;
    .locals 2
    .param p0    # I
    .param p1    # Landroid/os/Bundle;

    if-nez p0, :cond_0

    sget-object v0, Lcom/google/android/gms/common/ConnectionResult;->RESULT_SUCCESS:Lcom/google/android/gms/common/ConnectionResult;

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/common/ConnectionResult;

    const-string v0, "pendingIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public static getResolutionBundle(Lcom/google/android/gms/common/ConnectionResult;)Landroid/os/Bundle;
    .locals 3
    .param p0    # Lcom/google/android/gms/common/ConnectionResult;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "pendingIntent"

    invoke-virtual {p0}, Lcom/google/android/gms/common/ConnectionResult;->getResolution()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v0
.end method
