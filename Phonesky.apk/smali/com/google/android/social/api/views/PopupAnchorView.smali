.class public Lcom/google/android/social/api/views/PopupAnchorView;
.super Landroid/widget/FrameLayout;
.source "PopupAnchorView.java"


# instance fields
.field private anchorLayout:I

.field private anchorPopup:I

.field private caretLayout:Landroid/widget/LinearLayout$LayoutParams;

.field private window:Landroid/widget/PopupWindow;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    iput-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->caretLayout:Landroid/widget/LinearLayout$LayoutParams;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    iput-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->caretLayout:Landroid/widget/LinearLayout$LayoutParams;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    iput-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->caretLayout:Landroid/widget/LinearLayout$LayoutParams;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/social/api/views/PopupAnchorView;)V
    .locals 0
    .param p0    # Lcom/google/android/social/api/views/PopupAnchorView;

    invoke-direct {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->adjustWindow()V

    return-void
.end method

.method private adjustWindow()V
    .locals 8

    const/16 v7, 0x10

    const/4 v6, 0x5

    const/4 v5, 0x1

    const/4 v4, -0x1

    iget-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_4

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->anchorLayout:I

    and-int/lit8 v0, v0, 0x7

    if-ne v0, v5, :cond_5

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v2, v0

    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->anchorLayout:I

    and-int/lit8 v0, v0, 0x70

    if-ne v0, v7, :cond_6

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int/2addr v3, v0

    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->anchorPopup:I

    and-int/lit8 v0, v0, 0x7

    if-ne v0, v5, :cond_7

    iget-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int/2addr v2, v0

    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->anchorPopup:I

    and-int/lit8 v0, v0, 0x70

    if-ne v0, v7, :cond_8

    iget-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int/2addr v3, v0

    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->caretLayout:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->getWidth()I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iget-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    move-object v1, p0

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    :cond_4
    return-void

    :cond_5
    iget v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->anchorLayout:I

    and-int/lit8 v0, v0, 0x7

    if-ne v0, v6, :cond_0

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->getWidth()I

    move-result v0

    add-int/2addr v2, v0

    goto :goto_0

    :cond_6
    iget v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->anchorLayout:I

    and-int/lit8 v0, v0, 0x70

    const/16 v1, 0x30

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->getHeight()I

    move-result v0

    sub-int/2addr v3, v0

    goto :goto_1

    :cond_7
    iget v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->anchorPopup:I

    and-int/lit8 v0, v0, 0x7

    if-ne v0, v6, :cond_2

    iget-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    sub-int/2addr v2, v0

    goto :goto_2

    :cond_8
    iget v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->anchorPopup:I

    and-int/lit8 v0, v0, 0x70

    const/16 v1, 0x50

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    sub-int/2addr v3, v0

    goto :goto_3
.end method

.method private showPopup(Landroid/widget/PopupWindow;)V
    .locals 3
    .param p1    # Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->isPopupShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    iget-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x106000d

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    iget-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p0}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->adjustWindow()V

    goto :goto_0
.end method


# virtual methods
.method public dismissPopup()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->isPopupShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    :cond_0
    return-void
.end method

.method public isPopupShowing()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/views/PopupAnchorView;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->dismissPopup()V

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    invoke-direct {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->adjustWindow()V

    return-void
.end method

.method public setAnchorLayout(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/social/api/views/PopupAnchorView;->anchorLayout:I

    return-void
.end method

.method public setAnchorPopup(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/social/api/views/PopupAnchorView;->anchorPopup:I

    return-void
.end method

.method public showPopup(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->isPopupShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/views/PopupAnchorView;->showPopup(Landroid/view/View;)V

    goto :goto_0
.end method

.method public showPopup(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v6, -0x2

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->isPopupShowing()Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    iput-object v4, p0, Lcom/google/android/social/api/views/PopupAnchorView;->caretLayout:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lcom/google/android/social/api/views/PopupAnchorView;->caretLayout:Landroid/widget/LinearLayout$LayoutParams;

    iget v5, p0, Lcom/google/android/social/api/views/PopupAnchorView;->anchorPopup:I

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0200ef

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/google/android/social/api/views/PopupAnchorView;->caretLayout:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0200f0

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    new-instance v3, Lcom/google/android/social/api/views/PopupAnchorView$1;

    invoke-virtual {p0}, Lcom/google/android/social/api/views/PopupAnchorView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/google/android/social/api/views/PopupAnchorView$1;-><init>(Lcom/google/android/social/api/views/PopupAnchorView;Landroid/content/Context;)V

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v4, Landroid/widget/PopupWindow;

    invoke-direct {v4, v3, v6, v6}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    invoke-direct {p0, v4}, Lcom/google/android/social/api/views/PopupAnchorView;->showPopup(Landroid/widget/PopupWindow;)V

    goto :goto_0
.end method
