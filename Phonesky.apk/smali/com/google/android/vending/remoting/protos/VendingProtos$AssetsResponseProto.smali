.class public final Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/vending/remoting/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AssetsResponseProto"
.end annotation


# instance fields
.field private altAsset_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;",
            ">;"
        }
    .end annotation
.end field

.field private asset_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private correctedQuery_:Ljava/lang/String;

.field private hasCorrectedQuery:Z

.field private hasHeader:Z

.field private hasListType:Z

.field private hasNumCorrectedEntries:Z

.field private hasNumTotalEntries:Z

.field private header_:Ljava/lang/String;

.field private listType_:I

.field private numCorrectedEntries_:J

.field private numTotalEntries_:J


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->asset_:Ljava/util/List;

    iput-wide v1, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->numTotalEntries_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->correctedQuery_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->altAsset_:Ljava/util/List;

    iput-wide v1, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->numCorrectedEntries_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->header_:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->listType_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addAltAsset(Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;
    .locals 1
    .param p1    # Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->altAsset_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->altAsset_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->altAsset_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addAsset(Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;
    .locals 1
    .param p1    # Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->asset_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->asset_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->asset_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAltAssetList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->altAsset_:Ljava/util/List;

    return-object v0
.end method

.method public getAssetList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->asset_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->cachedSize:I

    return v0
.end method

.method public getCorrectedQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->correctedQuery_:Ljava/lang/String;

    return-object v0
.end method

.method public getHeader()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->header_:Ljava/lang/String;

    return-object v0
.end method

.method public getListType()I
    .locals 1

    iget v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->listType_:I

    return v0
.end method

.method public getNumCorrectedEntries()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->numCorrectedEntries_:J

    return-wide v0
.end method

.method public getNumTotalEntries()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->numTotalEntries_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->getAssetList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;

    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasNumTotalEntries()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->getNumTotalEntries()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasCorrectedQuery()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->getCorrectedQuery()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->getAltAssetList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;

    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasNumCorrectedEntries()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->getNumCorrectedEntries()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasHeader()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->getHeader()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasListType()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->getListType()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    iput v2, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->cachedSize:I

    return v2
.end method

.method public hasCorrectedQuery()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasCorrectedQuery:Z

    return v0
.end method

.method public hasHeader()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasHeader:Z

    return v0
.end method

.method public hasListType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasListType:Z

    return v0
.end method

.method public hasNumCorrectedEntries()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasNumCorrectedEntries:Z

    return v0
.end method

.method public hasNumTotalEntries()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasNumTotalEntries:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;

    invoke-direct {v1}, Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->addAsset(Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->setNumTotalEntries(J)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->setCorrectedQuery(Ljava/lang/String;)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;

    invoke-direct {v1}, Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->addAltAsset(Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->setNumCorrectedEntries(J)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->setHeader(Ljava/lang/String;)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->setListType(I)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;

    move-result-object v0

    return-object v0
.end method

.method public setCorrectedQuery(Ljava/lang/String;)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasCorrectedQuery:Z

    iput-object p1, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->correctedQuery_:Ljava/lang/String;

    return-object p0
.end method

.method public setHeader(Ljava/lang/String;)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasHeader:Z

    iput-object p1, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->header_:Ljava/lang/String;

    return-object p0
.end method

.method public setListType(I)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasListType:Z

    iput p1, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->listType_:I

    return-object p0
.end method

.method public setNumCorrectedEntries(J)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasNumCorrectedEntries:Z

    iput-wide p1, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->numCorrectedEntries_:J

    return-object p0
.end method

.method public setNumTotalEntries(J)Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasNumTotalEntries:Z

    iput-wide p1, p0, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->numTotalEntries_:J

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->getAssetList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;

    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasNumTotalEntries()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->getNumTotalEntries()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasCorrectedQuery()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->getCorrectedQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->getAltAssetList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/vending/remoting/protos/VendingProtos$ExternalAssetProto;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasNumCorrectedEntries()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->getNumCorrectedEntries()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasHeader()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->getHeader()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->hasListType()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/android/vending/remoting/protos/VendingProtos$AssetsResponseProto;->getListType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_6
    return-void
.end method
