.class public Lcom/google/android/vending/verifier/api/PackageVerificationResult;
.super Ljava/lang/Object;
.source "PackageVerificationResult.java"


# instance fields
.field public final description:Ljava/lang/String;

.field public final moreInfoUri:Landroid/net/Uri;

.field public final token:[B

.field public final verdict:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Landroid/net/Uri;[B)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/net/Uri;
    .param p4    # [B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/vending/verifier/api/PackageVerificationResult;->verdict:I

    iput-object p2, p0, Lcom/google/android/vending/verifier/api/PackageVerificationResult;->description:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/vending/verifier/api/PackageVerificationResult;->moreInfoUri:Landroid/net/Uri;

    iput-object p4, p0, Lcom/google/android/vending/verifier/api/PackageVerificationResult;->token:[B

    return-void
.end method

.method public static fromResponse(Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;)Lcom/google/android/vending/verifier/api/PackageVerificationResult;
    .locals 6
    .param p0    # Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;

    const/4 v0, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasMoreInfo()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->getMoreInfo()Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;->hasDescription()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;->getDescription()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;->hasUrl()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse$MoreInfo;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->hasToken()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->getToken()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/protobuf/micro/ByteStringMicro;->toByteArray()[B

    move-result-object v2

    :cond_2
    new-instance v4, Lcom/google/android/vending/verifier/api/PackageVerificationResult;

    invoke-virtual {p0}, Lcom/google/android/vending/verifier/protos/CsdClient$ClientDownloadResponse;->getVerdict()I

    move-result v5

    invoke-direct {v4, v5, v0, v3, v2}, Lcom/google/android/vending/verifier/api/PackageVerificationResult;-><init>(ILjava/lang/String;Landroid/net/Uri;[B)V

    return-object v4
.end method
