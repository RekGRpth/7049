.class public final Lcom/google/api/services/plus/model/ActivityDetailsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ActivityDetailsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/ActivityDetails;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/ActivityDetailsJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ActivityDetailsJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ActivityDetailsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/ActivityDetailsJson;->INSTANCE:Lcom/google/api/services/plus/model/ActivityDetailsJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/ActivityDetails;

    const/16 v1, 0xf

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "popularUpdatePosition"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "metadataNamespace"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "explanationType"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "mediaUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "numComments"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "isPublic"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "isOwnerless"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "embedType"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "mediaType"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "numShares"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "isRead"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "originalPosition"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "sourceStreamId"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "numPlusones"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "itemCategory"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/ActivityDetailsJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/ActivityDetailsJson;->INSTANCE:Lcom/google/api/services/plus/model/ActivityDetailsJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/ActivityDetails;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/ActivityDetails;

    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActivityDetails;->popularUpdatePosition:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActivityDetails;->metadataNamespace:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActivityDetails;->explanationType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActivityDetails;->mediaUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActivityDetails;->numComments:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActivityDetails;->isPublic:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActivityDetails;->isOwnerless:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActivityDetails;->embedType:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActivityDetails;->mediaType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActivityDetails;->numShares:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActivityDetails;->isRead:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActivityDetails;->originalPosition:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActivityDetails;->sourceStreamId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActivityDetails;->numPlusones:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plus/model/ActivityDetails;->itemCategory:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/ActivityDetails;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/ActivityDetailsJson;->getValues(Lcom/google/api/services/plus/model/ActivityDetails;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/ActivityDetails;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ActivityDetails;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ActivityDetails;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/ActivityDetailsJson;->newInstance()Lcom/google/api/services/plus/model/ActivityDetails;

    move-result-object v0

    return-object v0
.end method
