.class public final Lcom/google/api/services/plus/model/AudiencesFeed;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "AudiencesFeed.java"


# instance fields
.field public etag:Ljava/lang/String;

.field public items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/Audience;",
            ">;"
        }
    .end annotation
.end field

.field public kind:Ljava/lang/String;

.field public nextPageToken:Ljava/lang/String;

.field public totalItems:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/api/services/plus/model/Audience;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
