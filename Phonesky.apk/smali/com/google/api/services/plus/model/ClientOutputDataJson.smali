.class public final Lcom/google/api/services/plus/model/ClientOutputDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ClientOutputDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/ClientOutputData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/ClientOutputDataJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientOutputDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientOutputDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/ClientOutputDataJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientOutputDataJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/ClientOutputData;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plus/model/ClientLoggedCircleJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "circle"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plus/model/ClientLoggedCircleJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "streamFilterCircle"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "suggestionInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plus/model/ClientLoggedCircleMemberJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "circleMember"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plus/model/ClientUserInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "userInfo"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/ClientOutputDataJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/ClientOutputDataJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientOutputDataJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/ClientOutputData;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/ClientOutputData;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientOutputData;->circle:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientOutputData;->streamFilterCircle:Lcom/google/api/services/plus/model/ClientLoggedCircle;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientOutputData;->suggestionInfo:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientOutputData;->circleMember:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientOutputData;->userInfo:Ljava/util/List;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/ClientOutputData;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/ClientOutputDataJson;->getValues(Lcom/google/api/services/plus/model/ClientOutputData;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/ClientOutputData;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientOutputData;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientOutputData;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/ClientOutputDataJson;->newInstance()Lcom/google/api/services/plus/model/ClientOutputData;

    move-result-object v0

    return-object v0
.end method
