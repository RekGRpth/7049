.class public final Lcom/google/api/services/plus/model/FavaDiagnosticsRequestStat;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "FavaDiagnosticsRequestStat.java"


# instance fields
.field public failedAttemptsTime:Ljava/lang/Integer;

.field public networkTime:Ljava/lang/Integer;

.field public numFailedAttempts:Ljava/lang/Integer;

.field public requestPath:Ljava/lang/String;

.field public serverTime:Ljava/lang/Integer;

.field public totalContributingTime:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
