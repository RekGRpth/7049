.class public final Lcom/google/api/services/plus/model/LoggedCircleJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LoggedCircleJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/LoggedCircle;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/LoggedCircleJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/LoggedCircleJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/LoggedCircleJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/LoggedCircleJson;->INSTANCE:Lcom/google/api/services/plus/model/LoggedCircleJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/LoggedCircle;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/api/services/plus/model/LoggedCircleJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "focusId"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/google/api/services/plus/model/LoggedCircleJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "gaiaId"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "circleSize"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "circleType"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/LoggedCircleJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/LoggedCircleJson;->INSTANCE:Lcom/google/api/services/plus/model/LoggedCircleJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/LoggedCircle;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/LoggedCircle;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedCircle;->focusId:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedCircle;->gaiaId:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedCircle;->circleSize:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedCircle;->circleType:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/LoggedCircle;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/LoggedCircleJson;->getValues(Lcom/google/api/services/plus/model/LoggedCircle;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/LoggedCircle;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/LoggedCircle;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/LoggedCircle;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/LoggedCircleJson;->newInstance()Lcom/google/api/services/plus/model/LoggedCircle;

    move-result-object v0

    return-object v0
.end method
