.class public final Lcom/google/api/services/plus/model/LoggedPhotoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LoggedPhotoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/LoggedPhoto;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/LoggedPhotoJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/LoggedPhotoJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/LoggedPhotoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/LoggedPhotoJson;->INSTANCE:Lcom/google/api/services/plus/model/LoggedPhotoJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/LoggedPhoto;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/api/services/plus/model/LoggedPhotoJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "photoId"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "photoUrl"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/LoggedPhotoJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/LoggedPhotoJson;->INSTANCE:Lcom/google/api/services/plus/model/LoggedPhotoJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/LoggedPhoto;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/LoggedPhoto;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedPhoto;->photoId:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedPhoto;->photoUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/LoggedPhoto;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/LoggedPhotoJson;->getValues(Lcom/google/api/services/plus/model/LoggedPhoto;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/LoggedPhoto;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/LoggedPhoto;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/LoggedPhoto;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/LoggedPhotoJson;->newInstance()Lcom/google/api/services/plus/model/LoggedPhoto;

    move-result-object v0

    return-object v0
.end method
