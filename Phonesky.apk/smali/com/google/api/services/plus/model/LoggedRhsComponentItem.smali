.class public final Lcom/google/api/services/plus/model/LoggedRhsComponentItem;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "LoggedRhsComponentItem.java"


# instance fields
.field public circle:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/LoggedCircle;",
            ">;"
        }
    .end annotation
.end field

.field public col:Ljava/lang/Integer;

.field public connectSiteId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public gamesLabelId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/math/BigInteger;",
            ">;"
        }
    .end annotation
.end field

.field public person:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/LoggedCircleMember;",
            ">;"
        }
    .end annotation
.end field

.field public row:Ljava/lang/Integer;

.field public suggestionInfo:Lcom/google/api/services/plus/model/LoggedSuggestionInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/api/services/plus/model/LoggedCircleMember;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    const-class v0, Lcom/google/api/services/plus/model/LoggedCircle;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
