.class public final Lcom/google/api/services/plus/model/OzEvent;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "OzEvent.java"


# instance fields
.field public actionTarget:Lcom/google/api/services/plus/model/ActionTarget;

.field public background:Ljava/lang/Boolean;

.field public badTiming:Ljava/lang/Boolean;

.field public endViewData:Lcom/google/api/services/plus/model/OutputData;

.field public favaDiagnostics:Lcom/google/api/services/plus/model/FavaDiagnostics;

.field public inputData:Lcom/google/api/services/plus/model/InputData;

.field public interstitialRedirectorData:Lcom/google/api/services/plus/model/InterstitialRedirectorData;

.field public isNativePlatformEvent:Ljava/lang/Boolean;

.field public overallUserSegment:Ljava/lang/Integer;

.field public startViewData:Lcom/google/api/services/plus/model/OutputData;

.field public timestampUsecDelta:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
