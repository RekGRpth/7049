.class public final Lcom/google/api/services/plus/model/Person;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "Person.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/api/services/plus/model/Person$Urls;,
        Lcom/google/api/services/plus/model/Person$Cover;,
        Lcom/google/api/services/plus/model/Person$Name;,
        Lcom/google/api/services/plus/model/Person$Organizations;,
        Lcom/google/api/services/plus/model/Person$Emails;,
        Lcom/google/api/services/plus/model/Person$StatusForViewer;,
        Lcom/google/api/services/plus/model/Person$PlacesLived;,
        Lcom/google/api/services/plus/model/Person$Image;
    }
.end annotation


# instance fields
.field public aboutMe:Ljava/lang/String;

.field public ageRange:Ljava/lang/String;

.field public birthday:Ljava/lang/String;

.field public cover:Lcom/google/api/services/plus/model/Person$Cover;

.field public currentLocation:Ljava/lang/String;

.field public displayName:Ljava/lang/String;

.field public emails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/Person$Emails;",
            ">;"
        }
    .end annotation
.end field

.field public etag:Ljava/lang/String;

.field public gender:Ljava/lang/String;

.field public hasApp:Ljava/lang/Boolean;

.field public id:Ljava/lang/String;

.field public image:Lcom/google/api/services/plus/model/Person$Image;

.field public isPlusUser:Ljava/lang/Boolean;

.field public kind:Ljava/lang/String;

.field public language:Ljava/lang/String;

.field public name:Lcom/google/api/services/plus/model/Person$Name;

.field public nickname:Ljava/lang/String;

.field public objectType:Ljava/lang/String;

.field public organizations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/Person$Organizations;",
            ">;"
        }
    .end annotation
.end field

.field public placesLived:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/Person$PlacesLived;",
            ">;"
        }
    .end annotation
.end field

.field public relationshipStatus:Ljava/lang/String;

.field public statusForViewer:Lcom/google/api/services/plus/model/Person$StatusForViewer;

.field public tagline:Ljava/lang/String;

.field public url:Ljava/lang/String;

.field public urls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/Person$Urls;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/api/services/plus/model/Person$PlacesLived;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    const-class v0, Lcom/google/api/services/plus/model/Person$Emails;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    const-class v0, Lcom/google/api/services/plus/model/Person$Organizations;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    const-class v0, Lcom/google/api/services/plus/model/Person$Urls;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
