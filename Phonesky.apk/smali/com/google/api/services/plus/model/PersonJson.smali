.class public final Lcom/google/api/services/plus/model/PersonJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PersonJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/Person;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/PersonJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/PersonJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/PersonJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/PersonJson;->INSTANCE:Lcom/google/api/services/plus/model/PersonJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/Person;

    const/16 v1, 0x21

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plus/model/Person_ImageJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "image"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "hasApp"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "objectType"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "tagline"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "currentLocation"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "etag"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "aboutMe"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "kind"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plus/model/Person_PlacesLivedJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "placesLived"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "birthday"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lcom/google/api/services/plus/model/Person_StatusForViewerJson;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "statusForViewer"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "nickname"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-class v3, Lcom/google/api/services/plus/model/Person_EmailsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "emails"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-class v3, Lcom/google/api/services/plus/model/Person_OrganizationsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "organizations"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "relationshipStatus"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "displayName"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-class v3, Lcom/google/api/services/plus/model/Person_NameJson;

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string v3, "name"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string v3, "language"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-string v3, "url"

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string v3, "gender"

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-class v3, Lcom/google/api/services/plus/model/Person_CoverJson;

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-string v3, "cover"

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-string v3, "isPlusUser"

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const-class v3, Lcom/google/api/services/plus/model/Person_UrlsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const-string v3, "urls"

    aput-object v3, v1, v2

    const/16 v2, 0x20

    const-string v3, "ageRange"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/PersonJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/PersonJson;->INSTANCE:Lcom/google/api/services/plus/model/PersonJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/Person;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/Person;

    const/16 v0, 0x19

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->image:Lcom/google/api/services/plus/model/Person$Image;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->hasApp:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->id:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->objectType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->tagline:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->currentLocation:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->etag:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->aboutMe:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->kind:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->placesLived:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->birthday:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->statusForViewer:Lcom/google/api/services/plus/model/Person$StatusForViewer;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->nickname:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->emails:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->organizations:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->relationshipStatus:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->displayName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->name:Lcom/google/api/services/plus/model/Person$Name;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->language:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->url:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->gender:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->cover:Lcom/google/api/services/plus/model/Person$Cover;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->isPlusUser:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->urls:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person;->ageRange:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/Person;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/PersonJson;->getValues(Lcom/google/api/services/plus/model/Person;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/Person;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/Person;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/Person;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/PersonJson;->newInstance()Lcom/google/api/services/plus/model/Person;

    move-result-object v0

    return-object v0
.end method
