.class public final Lcom/google/api/services/plus/model/Person_NameJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "Person_NameJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/Person$Name;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/Person_NameJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/Person_NameJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/Person_NameJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/Person_NameJson;->INSTANCE:Lcom/google/api/services/plus/model/Person_NameJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/Person$Name;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "honorificPrefix"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "middleName"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "familyName"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "formatted"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "givenName"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "honorificSuffix"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/Person_NameJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/Person_NameJson;->INSTANCE:Lcom/google/api/services/plus/model/Person_NameJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/Person$Name;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/Person$Name;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Name;->honorificPrefix:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Name;->middleName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Name;->familyName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Name;->formatted:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Name;->givenName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Name;->honorificSuffix:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/Person$Name;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/Person_NameJson;->getValues(Lcom/google/api/services/plus/model/Person$Name;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/Person$Name;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/Person$Name;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/Person$Name;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/Person_NameJson;->newInstance()Lcom/google/api/services/plus/model/Person$Name;

    move-result-object v0

    return-object v0
.end method
