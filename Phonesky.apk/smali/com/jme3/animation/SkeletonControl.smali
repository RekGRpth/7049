.class public Lcom/jme3/animation/SkeletonControl;
.super Lcom/jme3/scene/control/AbstractControl;
.source "SkeletonControl.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private skeleton:Lcom/jme3/animation/Skeleton;

.field private targets:[Lcom/jme3/scene/Mesh;

.field private wasMeshUpdated:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/jme3/scene/control/AbstractControl;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/animation/SkeletonControl;->wasMeshUpdated:Z

    return-void
.end method

.method public constructor <init>(Lcom/jme3/animation/Skeleton;)V
    .locals 1
    .param p1    # Lcom/jme3/animation/Skeleton;

    invoke-direct {p0}, Lcom/jme3/scene/control/AbstractControl;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/animation/SkeletonControl;->wasMeshUpdated:Z

    iput-object p1, p0, Lcom/jme3/animation/SkeletonControl;->skeleton:Lcom/jme3/animation/Skeleton;

    return-void
.end method

.method constructor <init>([Lcom/jme3/scene/Mesh;Lcom/jme3/animation/Skeleton;)V
    .locals 1
    .param p1    # [Lcom/jme3/scene/Mesh;
    .param p2    # Lcom/jme3/animation/Skeleton;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-direct {p0}, Lcom/jme3/scene/control/AbstractControl;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/animation/SkeletonControl;->wasMeshUpdated:Z

    iput-object p2, p0, Lcom/jme3/animation/SkeletonControl;->skeleton:Lcom/jme3/animation/Skeleton;

    iput-object p1, p0, Lcom/jme3/animation/SkeletonControl;->targets:[Lcom/jme3/scene/Mesh;

    return-void
.end method

.method private applySkinning(Lcom/jme3/scene/Mesh;[Lcom/jme3/math/Matrix4f;)V
    .locals 41
    .param p1    # Lcom/jme3/scene/Mesh;
    .param p2    # [Lcom/jme3/math/Matrix4f;

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/Mesh;->getMaxNumWeights()I

    move-result v15

    if-gtz v15, :cond_0

    new-instance v39, Ljava/lang/IllegalStateException;

    const-string v40, "Max weights per vert is incorrectly set!"

    invoke-direct/range {v39 .. v40}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v39

    :cond_0
    rsub-int/lit8 v4, v15, 0x4

    sget-object v39, Lcom/jme3/scene/VertexBuffer$Type;->Position:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v5

    check-cast v5, Ljava/nio/FloatBuffer;

    invoke-virtual {v5}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    sget-object v39, Lcom/jme3/scene/VertexBuffer$Type;->Normal:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v3

    check-cast v3, Ljava/nio/FloatBuffer;

    invoke-virtual {v3}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    sget-object v39, Lcom/jme3/scene/VertexBuffer$Type;->BoneIndex:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v7

    check-cast v7, Ljava/nio/ByteBuffer;

    sget-object v39, Lcom/jme3/scene/VertexBuffer$Type;->BoneWeight:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v36

    check-cast v36, Ljava/nio/FloatBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual/range {v36 .. v36}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual/range {v36 .. v36}, Ljava/nio/FloatBuffer;->array()[F

    move-result-object v38

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v12

    const/4 v10, 0x0

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v28

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/jme3/util/TempVars;->skinPositions:[F

    move-object/from16 v21, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/jme3/util/TempVars;->skinNormals:[F

    move-object/from16 v20, v0

    invoke-virtual {v5}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v39

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    div-float v39, v39, v40

    invoke-static/range {v39 .. v39}, Lcom/jme3/math/FastMath;->ceil(F)F

    move-result v39

    move/from16 v0, v39

    float-to-int v13, v0

    move-object/from16 v0, v21

    array-length v2, v0

    add-int/lit8 v6, v13, -0x1

    :goto_0
    if-ltz v6, :cond_3

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v39, v0

    invoke-virtual {v5}, Ljava/nio/FloatBuffer;->remaining()I

    move-result v40

    invoke-static/range {v39 .. v40}, Ljava/lang/Math;->min(II)I

    move-result v2

    const/16 v39, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v39

    invoke-virtual {v5, v0, v1, v2}, Ljava/nio/FloatBuffer;->get([FII)Ljava/nio/FloatBuffer;

    const/16 v39, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v39

    invoke-virtual {v3, v0, v1, v2}, Ljava/nio/FloatBuffer;->get([FII)Ljava/nio/FloatBuffer;

    div-int/lit8 v31, v2, 0x3

    const/4 v8, 0x0

    add-int/lit8 v30, v31, -0x1

    move v9, v8

    :goto_1
    if-ltz v30, :cond_2

    aget v17, v20, v9

    add-int/lit8 v8, v9, 0x1

    aget v32, v21, v9

    aget v18, v20, v8

    add-int/lit8 v9, v8, 0x1

    aget v33, v21, v8

    aget v19, v20, v9

    add-int/lit8 v8, v9, 0x1

    aget v34, v21, v9

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    add-int/lit8 v35, v15, -0x1

    move v11, v10

    :goto_2
    if-ltz v35, :cond_1

    aget v37, v38, v11

    add-int/lit8 v10, v11, 0x1

    aget-byte v39, v12, v11

    aget-object v14, p2, v39

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v39, v0

    mul-float v39, v39, v32

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v40, v0

    mul-float v40, v40, v33

    add-float v39, v39, v40

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v40, v0

    mul-float v40, v40, v34

    add-float v39, v39, v40

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v40, v0

    add-float v39, v39, v40

    mul-float v39, v39, v37

    add-float v25, v25, v39

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v39, v0

    mul-float v39, v39, v32

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v40, v0

    mul-float v40, v40, v33

    add-float v39, v39, v40

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v40, v0

    mul-float v40, v40, v34

    add-float v39, v39, v40

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v40, v0

    add-float v39, v39, v40

    mul-float v39, v39, v37

    add-float v26, v26, v39

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v39, v0

    mul-float v39, v39, v32

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v40, v0

    mul-float v40, v40, v33

    add-float v39, v39, v40

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v40, v0

    mul-float v40, v40, v34

    add-float v39, v39, v40

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v40, v0

    add-float v39, v39, v40

    mul-float v39, v39, v37

    add-float v27, v27, v39

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v39, v0

    mul-float v39, v39, v17

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v40, v0

    mul-float v40, v40, v18

    add-float v39, v39, v40

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v40, v0

    mul-float v40, v40, v19

    add-float v39, v39, v40

    mul-float v39, v39, v37

    add-float v22, v22, v39

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v39, v0

    mul-float v39, v39, v17

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v40, v0

    mul-float v40, v40, v18

    add-float v39, v39, v40

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v40, v0

    mul-float v40, v40, v19

    add-float v39, v39, v40

    mul-float v39, v39, v37

    add-float v23, v23, v39

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v39, v0

    mul-float v39, v39, v17

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v40, v0

    mul-float v40, v40, v18

    add-float v39, v39, v40

    iget v0, v14, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v40, v0

    mul-float v40, v40, v19

    add-float v39, v39, v40

    mul-float v39, v39, v37

    add-float v24, v24, v39

    add-int/lit8 v35, v35, -0x1

    move v11, v10

    goto/16 :goto_2

    :cond_1
    add-int v10, v11, v4

    add-int/lit8 v8, v8, -0x3

    aput v22, v20, v8

    add-int/lit8 v9, v8, 0x1

    aput v25, v21, v8

    aput v23, v20, v9

    add-int/lit8 v8, v9, 0x1

    aput v26, v21, v9

    aput v24, v20, v8

    add-int/lit8 v9, v8, 0x1

    aput v27, v21, v8

    add-int/lit8 v30, v30, -0x1

    goto/16 :goto_1

    :cond_2
    invoke-virtual {v5}, Ljava/nio/FloatBuffer;->position()I

    move-result v39

    sub-int v39, v39, v2

    move/from16 v0, v39

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    const/16 v39, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v39

    invoke-virtual {v5, v0, v1, v2}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    invoke-virtual {v3}, Ljava/nio/FloatBuffer;->position()I

    move-result v39

    sub-int v39, v39, v2

    move/from16 v0, v39

    invoke-virtual {v3, v0}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    const/16 v39, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v39

    invoke-virtual {v3, v0, v1, v2}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    add-int/lit8 v6, v6, -0x1

    goto/16 :goto_0

    :cond_3
    invoke-virtual/range {v28 .. v28}, Lcom/jme3/util/TempVars;->release()V

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Lcom/jme3/scene/VertexBuffer;->updateData(Ljava/nio/Buffer;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/jme3/scene/VertexBuffer;->updateData(Ljava/nio/Buffer;)V

    return-void
.end method

.method private applySkinningTangents(Lcom/jme3/scene/Mesh;[Lcom/jme3/math/Matrix4f;Lcom/jme3/scene/VertexBuffer;)V
    .locals 53
    .param p1    # Lcom/jme3/scene/Mesh;
    .param p2    # [Lcom/jme3/math/Matrix4f;
    .param p3    # Lcom/jme3/scene/VertexBuffer;

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/Mesh;->getMaxNumWeights()I

    move-result v19

    if-gtz v19, :cond_0

    new-instance v51, Ljava/lang/IllegalStateException;

    const-string v52, "Max weights per vert is incorrectly set!"

    invoke-direct/range {v51 .. v52}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v51

    :cond_0
    rsub-int/lit8 v5, v19, 0x4

    sget-object v51, Lcom/jme3/scene/VertexBuffer$Type;->Position:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v7

    check-cast v7, Ljava/nio/FloatBuffer;

    invoke-virtual {v7}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    sget-object v51, Lcom/jme3/scene/VertexBuffer$Type;->Normal:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v4

    check-cast v4, Ljava/nio/FloatBuffer;

    invoke-virtual {v4}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual/range {p3 .. p3}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v6

    check-cast v6, Ljava/nio/FloatBuffer;

    invoke-virtual {v6}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    sget-object v51, Lcom/jme3/scene/VertexBuffer$Type;->BoneIndex:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v9

    check-cast v9, Ljava/nio/ByteBuffer;

    sget-object v51, Lcom/jme3/scene/VertexBuffer$Type;->BoneWeight:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v48

    check-cast v48, Ljava/nio/FloatBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual/range {v48 .. v48}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual/range {v48 .. v48}, Ljava/nio/FloatBuffer;->array()[F

    move-result-object v50

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v16

    const/4 v14, 0x0

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v40

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/jme3/util/TempVars;->skinPositions:[F

    move-object/from16 v25, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/jme3/util/TempVars;->skinNormals:[F

    move-object/from16 v24, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/jme3/util/TempVars;->skinTangents:[F

    move-object/from16 v35, v0

    invoke-virtual {v7}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v51

    move/from16 v0, v51

    int-to-float v0, v0

    move/from16 v51, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-float v0, v0

    move/from16 v52, v0

    div-float v51, v51, v52

    invoke-static/range {v51 .. v51}, Lcom/jme3/math/FastMath;->ceil(F)F

    move-result v51

    move/from16 v0, v51

    float-to-int v0, v0

    move/from16 v17, v0

    const/4 v3, 0x0

    const/16 v36, 0x0

    add-int/lit8 v8, v17, -0x1

    :goto_0
    if-ltz v8, :cond_3

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v51, v0

    invoke-virtual {v7}, Ljava/nio/FloatBuffer;->remaining()I

    move-result v52

    invoke-static/range {v51 .. v52}, Ljava/lang/Math;->min(II)I

    move-result v3

    move-object/from16 v0, v35

    array-length v0, v0

    move/from16 v51, v0

    invoke-virtual {v6}, Ljava/nio/FloatBuffer;->remaining()I

    move-result v52

    invoke-static/range {v51 .. v52}, Ljava/lang/Math;->min(II)I

    move-result v36

    const/16 v51, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v51

    invoke-virtual {v7, v0, v1, v3}, Ljava/nio/FloatBuffer;->get([FII)Ljava/nio/FloatBuffer;

    const/16 v51, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v51

    invoke-virtual {v4, v0, v1, v3}, Ljava/nio/FloatBuffer;->get([FII)Ljava/nio/FloatBuffer;

    const/16 v51, 0x0

    move-object/from16 v0, v35

    move/from16 v1, v51

    move/from16 v2, v36

    invoke-virtual {v6, v0, v1, v2}, Ljava/nio/FloatBuffer;->get([FII)Ljava/nio/FloatBuffer;

    div-int/lit8 v43, v3, 0x3

    const/4 v10, 0x0

    const/4 v12, 0x0

    add-int/lit8 v42, v43, -0x1

    move v13, v12

    move v11, v10

    :goto_1
    if-ltz v42, :cond_2

    aget v21, v24, v11

    add-int/lit8 v10, v11, 0x1

    aget v44, v25, v11

    aget v22, v24, v10

    add-int/lit8 v11, v10, 0x1

    aget v45, v25, v10

    aget v23, v24, v11

    add-int/lit8 v10, v11, 0x1

    aget v46, v25, v11

    add-int/lit8 v12, v13, 0x1

    aget v37, v35, v13

    add-int/lit8 v13, v12, 0x1

    aget v38, v35, v12

    add-int/lit8 v12, v13, 0x1

    aget v39, v35, v13

    add-int/lit8 v12, v12, 0x1

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    add-int/lit8 v47, v19, -0x1

    move v15, v14

    :goto_2
    if-ltz v47, :cond_1

    aget v49, v50, v15

    add-int/lit8 v14, v15, 0x1

    aget-byte v51, v16, v15

    aget-object v18, p2, v51

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v51, v0

    mul-float v51, v51, v44

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v52, v0

    mul-float v52, v52, v45

    add-float v51, v51, v52

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v52, v0

    mul-float v52, v52, v46

    add-float v51, v51, v52

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m03:F

    move/from16 v52, v0

    add-float v51, v51, v52

    mul-float v51, v51, v49

    add-float v32, v32, v51

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v51, v0

    mul-float v51, v51, v44

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v52, v0

    mul-float v52, v52, v45

    add-float v51, v51, v52

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v52, v0

    mul-float v52, v52, v46

    add-float v51, v51, v52

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m13:F

    move/from16 v52, v0

    add-float v51, v51, v52

    mul-float v51, v51, v49

    add-float v33, v33, v51

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v51, v0

    mul-float v51, v51, v44

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v52, v0

    mul-float v52, v52, v45

    add-float v51, v51, v52

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v52, v0

    mul-float v52, v52, v46

    add-float v51, v51, v52

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m23:F

    move/from16 v52, v0

    add-float v51, v51, v52

    mul-float v51, v51, v49

    add-float v34, v34, v51

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v51, v0

    mul-float v51, v51, v21

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v52, v0

    mul-float v52, v52, v22

    add-float v51, v51, v52

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v52, v0

    mul-float v52, v52, v23

    add-float v51, v51, v52

    mul-float v51, v51, v49

    add-float v26, v26, v51

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v51, v0

    mul-float v51, v51, v21

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v52, v0

    mul-float v52, v52, v22

    add-float v51, v51, v52

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v52, v0

    mul-float v52, v52, v23

    add-float v51, v51, v52

    mul-float v51, v51, v49

    add-float v27, v27, v51

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v51, v0

    mul-float v51, v51, v21

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v52, v0

    mul-float v52, v52, v22

    add-float v51, v51, v52

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v52, v0

    mul-float v52, v52, v23

    add-float v51, v51, v52

    mul-float v51, v51, v49

    add-float v28, v28, v51

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m00:F

    move/from16 v51, v0

    mul-float v51, v51, v37

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m01:F

    move/from16 v52, v0

    mul-float v52, v52, v38

    add-float v51, v51, v52

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m02:F

    move/from16 v52, v0

    mul-float v52, v52, v39

    add-float v51, v51, v52

    mul-float v51, v51, v49

    add-float v29, v29, v51

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m10:F

    move/from16 v51, v0

    mul-float v51, v51, v37

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m11:F

    move/from16 v52, v0

    mul-float v52, v52, v38

    add-float v51, v51, v52

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m12:F

    move/from16 v52, v0

    mul-float v52, v52, v39

    add-float v51, v51, v52

    mul-float v51, v51, v49

    add-float v30, v30, v51

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m20:F

    move/from16 v51, v0

    mul-float v51, v51, v37

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m21:F

    move/from16 v52, v0

    mul-float v52, v52, v38

    add-float v51, v51, v52

    move-object/from16 v0, v18

    iget v0, v0, Lcom/jme3/math/Matrix4f;->m22:F

    move/from16 v52, v0

    mul-float v52, v52, v39

    add-float v51, v51, v52

    mul-float v51, v51, v49

    add-float v31, v31, v51

    add-int/lit8 v47, v47, -0x1

    move v15, v14

    goto/16 :goto_2

    :cond_1
    add-int v14, v15, v5

    add-int/lit8 v10, v10, -0x3

    aput v26, v24, v10

    add-int/lit8 v11, v10, 0x1

    aput v32, v25, v10

    aput v27, v24, v11

    add-int/lit8 v10, v11, 0x1

    aput v33, v25, v11

    aput v28, v24, v10

    add-int/lit8 v11, v10, 0x1

    aput v34, v25, v10

    add-int/lit8 v12, v12, -0x4

    add-int/lit8 v13, v12, 0x1

    aput v29, v35, v12

    add-int/lit8 v12, v13, 0x1

    aput v30, v35, v13

    add-int/lit8 v13, v12, 0x1

    aput v31, v35, v12

    add-int/lit8 v12, v13, 0x1

    add-int/lit8 v42, v42, -0x1

    move v13, v12

    goto/16 :goto_1

    :cond_2
    invoke-virtual {v7}, Ljava/nio/FloatBuffer;->position()I

    move-result v51

    sub-int v51, v51, v3

    move/from16 v0, v51

    invoke-virtual {v7, v0}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    const/16 v51, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v51

    invoke-virtual {v7, v0, v1, v3}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    invoke-virtual {v4}, Ljava/nio/FloatBuffer;->position()I

    move-result v51

    sub-int v51, v51, v3

    move/from16 v0, v51

    invoke-virtual {v4, v0}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    const/16 v51, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v51

    invoke-virtual {v4, v0, v1, v3}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    invoke-virtual {v6}, Ljava/nio/FloatBuffer;->position()I

    move-result v51

    sub-int v51, v51, v36

    move/from16 v0, v51

    invoke-virtual {v6, v0}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    const/16 v51, 0x0

    move-object/from16 v0, v35

    move/from16 v1, v51

    move/from16 v2, v36

    invoke-virtual {v6, v0, v1, v2}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    add-int/lit8 v8, v8, -0x1

    goto/16 :goto_0

    :cond_3
    invoke-virtual/range {v40 .. v40}, Lcom/jme3/util/TempVars;->release()V

    move-object/from16 v0, v41

    invoke-virtual {v0, v7}, Lcom/jme3/scene/VertexBuffer;->updateData(Ljava/nio/Buffer;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/jme3/scene/VertexBuffer;->updateData(Ljava/nio/Buffer;)V

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Lcom/jme3/scene/VertexBuffer;->updateData(Ljava/nio/Buffer;)V

    return-void
.end method

.method private findTargets(Lcom/jme3/scene/Node;)[Lcom/jme3/scene/Mesh;
    .locals 10
    .param p1    # Lcom/jme3/scene/Node;

    const/4 v6, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/jme3/scene/Node;->getChildren()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/scene/Spatial;

    instance-of v7, v1, Lcom/jme3/scene/Geometry;

    if-eqz v7, :cond_0

    move-object v3, v1

    check-cast v3, Lcom/jme3/scene/Geometry;

    const-string v7, "JmeSharedMesh"

    invoke-virtual {v3, v7}, Lcom/jme3/scene/Geometry;->getUserData(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/scene/Mesh;

    if-eqz v2, :cond_2

    invoke-direct {p0, v2}, Lcom/jme3/animation/SkeletonControl;->isMeshAnimated(Lcom/jme3/scene/Mesh;)Z

    move-result v7

    if-eqz v7, :cond_0

    if-nez v6, :cond_1

    move-object v6, v2

    goto :goto_0

    :cond_1
    if-eq v6, v2, :cond_0

    new-instance v7, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Two conflicting shared meshes for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_2
    invoke-virtual {v3}, Lcom/jme3/scene/Geometry;->getMesh()Lcom/jme3/scene/Mesh;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/jme3/animation/SkeletonControl;->isMeshAnimated(Lcom/jme3/scene/Mesh;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    if-eqz v6, :cond_4

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v7, v7, [Lcom/jme3/scene/Mesh;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lcom/jme3/scene/Mesh;

    return-object v7
.end method

.method private isMeshAnimated(Lcom/jme3/scene/Mesh;)Z
    .locals 1
    .param p1    # Lcom/jme3/scene/Mesh;

    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->BindPosePosition:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p1, v0}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private softwareSkinUpdate(Lcom/jme3/scene/Mesh;[Lcom/jme3/math/Matrix4f;)V
    .locals 2
    .param p1    # Lcom/jme3/scene/Mesh;
    .param p2    # [Lcom/jme3/math/Matrix4f;

    sget-object v1, Lcom/jme3/scene/VertexBuffer$Type;->Tangent:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p1, v1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/jme3/animation/SkeletonControl;->applySkinning(Lcom/jme3/scene/Mesh;[Lcom/jme3/math/Matrix4f;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2, v0}, Lcom/jme3/animation/SkeletonControl;->applySkinningTangents(Lcom/jme3/scene/Mesh;[Lcom/jme3/math/Matrix4f;Lcom/jme3/scene/VertexBuffer;)V

    goto :goto_0
.end method


# virtual methods
.method public cloneForSpatial(Lcom/jme3/scene/Spatial;)Lcom/jme3/scene/control/Control;
    .locals 6

    move-object v0, p1

    check-cast v0, Lcom/jme3/scene/Node;

    const-class v1, Lcom/jme3/animation/AnimControl;

    invoke-virtual {p1, v1}, Lcom/jme3/scene/Spatial;->getControl(Ljava/lang/Class;)Lcom/jme3/scene/control/Control;

    move-result-object v1

    check-cast v1, Lcom/jme3/animation/AnimControl;

    new-instance v4, Lcom/jme3/animation/SkeletonControl;

    invoke-direct {v4}, Lcom/jme3/animation/SkeletonControl;-><init>()V

    invoke-virtual {v4, v0}, Lcom/jme3/animation/SkeletonControl;->setSpatial(Lcom/jme3/scene/Spatial;)V

    invoke-virtual {v1}, Lcom/jme3/animation/AnimControl;->getSkeleton()Lcom/jme3/animation/Skeleton;

    move-result-object v1

    iput-object v1, v4, Lcom/jme3/animation/SkeletonControl;->skeleton:Lcom/jme3/animation/Skeleton;

    invoke-direct {p0, v0}, Lcom/jme3/animation/SkeletonControl;->findTargets(Lcom/jme3/scene/Node;)[Lcom/jme3/scene/Mesh;

    move-result-object v1

    iput-object v1, v4, Lcom/jme3/animation/SkeletonControl;->targets:[Lcom/jme3/scene/Mesh;

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    invoke-virtual {v0}, Lcom/jme3/scene/Node;->getQuantity()I

    move-result v1

    if-ge v3, v1, :cond_1

    invoke-virtual {v0, v3}, Lcom/jme3/scene/Node;->getChild(I)Lcom/jme3/scene/Spatial;

    move-result-object v1

    instance-of v2, v1, Lcom/jme3/scene/Node;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/jme3/scene/Node;

    const-string v2, "AttachedBone"

    invoke-virtual {v1, v2}, Lcom/jme3/scene/Node;->getUserData(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/animation/Bone;

    if-eqz v2, :cond_0

    iget-object v5, v4, Lcom/jme3/animation/SkeletonControl;->skeleton:Lcom/jme3/animation/Skeleton;

    invoke-virtual {v2}, Lcom/jme3/animation/Bone;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/jme3/animation/Skeleton;->getBone(Ljava/lang/String;)Lcom/jme3/animation/Bone;

    move-result-object v2

    const-string v5, "AttachedBone"

    invoke-virtual {v1, v5, v2}, Lcom/jme3/scene/Node;->setUserData(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2, v1}, Lcom/jme3/animation/Bone;->setAttachmentsNode(Lcom/jme3/scene/Node;)V

    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_1
    return-object v4
.end method

.method protected controlRender(Lcom/jme3/renderer/RenderManager;Lcom/jme3/renderer/ViewPort;)V
    .locals 3
    .param p1    # Lcom/jme3/renderer/RenderManager;
    .param p2    # Lcom/jme3/renderer/ViewPort;

    iget-boolean v2, p0, Lcom/jme3/animation/SkeletonControl;->wasMeshUpdated:Z

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/jme3/animation/SkeletonControl;->resetToBind()V

    iget-object v2, p0, Lcom/jme3/animation/SkeletonControl;->skeleton:Lcom/jme3/animation/Skeleton;

    invoke-virtual {v2}, Lcom/jme3/animation/Skeleton;->computeSkinningMatrices()[Lcom/jme3/math/Matrix4f;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/jme3/animation/SkeletonControl;->targets:[Lcom/jme3/scene/Mesh;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/jme3/animation/SkeletonControl;->targets:[Lcom/jme3/scene/Mesh;

    aget-object v2, v2, v0

    invoke-direct {p0, v2, v1}, Lcom/jme3/animation/SkeletonControl;->softwareSkinUpdate(Lcom/jme3/scene/Mesh;[Lcom/jme3/math/Matrix4f;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/jme3/animation/SkeletonControl;->wasMeshUpdated:Z

    :cond_1
    return-void
.end method

.method protected controlUpdate(F)V
    .locals 1
    .param p1    # F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/animation/SkeletonControl;->wasMeshUpdated:Z

    return-void
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 6
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/jme3/scene/control/AbstractControl;->read(Lcom/jme3/export/JmeImporter;)V

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v0

    const-string v2, "targets"

    invoke-interface {v0, v2, v5}, Lcom/jme3/export/InputCapsule;->readSavableArray(Ljava/lang/String;[Lcom/jme3/export/Savable;)[Lcom/jme3/export/Savable;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v1

    new-array v2, v2, [Lcom/jme3/scene/Mesh;

    iput-object v2, p0, Lcom/jme3/animation/SkeletonControl;->targets:[Lcom/jme3/scene/Mesh;

    iget-object v2, p0, Lcom/jme3/animation/SkeletonControl;->targets:[Lcom/jme3/scene/Mesh;

    array-length v3, v1

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    const-string v2, "skeleton"

    invoke-interface {v0, v2, v5}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v2

    check-cast v2, Lcom/jme3/animation/Skeleton;

    iput-object v2, p0, Lcom/jme3/animation/SkeletonControl;->skeleton:Lcom/jme3/animation/Skeleton;

    return-void
.end method

.method resetToBind()V
    .locals 20

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jme3/animation/SkeletonControl;->targets:[Lcom/jme3/scene/Mesh;

    array-length v11, v1

    const/4 v10, 0x0

    :goto_0
    if-ge v10, v11, :cond_3

    aget-object v12, v1, v10

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/jme3/animation/SkeletonControl;->isMeshAnimated(Lcom/jme3/scene/Mesh;)Z

    move-result v19

    if-eqz v19, :cond_2

    sget-object v19, Lcom/jme3/scene/VertexBuffer$Type;->BoneIndex:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v3

    check-cast v3, Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v19

    if-nez v19, :cond_0

    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/jme3/scene/Mesh;->prepareForAnim(Z)V

    :cond_0
    sget-object v19, Lcom/jme3/scene/VertexBuffer$Type;->BindPosePosition:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v5

    sget-object v19, Lcom/jme3/scene/VertexBuffer$Type;->BindPoseNormal:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v4

    sget-object v19, Lcom/jme3/scene/VertexBuffer$Type;->Position:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v16

    sget-object v19, Lcom/jme3/scene/VertexBuffer$Type;->Normal:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v14

    invoke-virtual/range {v16 .. v16}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v15

    check-cast v15, Ljava/nio/FloatBuffer;

    invoke-virtual {v14}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v13

    check-cast v13, Ljava/nio/FloatBuffer;

    invoke-virtual {v5}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v8

    check-cast v8, Ljava/nio/FloatBuffer;

    invoke-virtual {v4}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v7

    check-cast v7, Ljava/nio/FloatBuffer;

    invoke-virtual {v15}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    invoke-virtual {v13}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    invoke-virtual {v8}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    invoke-virtual {v7}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    sget-object v19, Lcom/jme3/scene/VertexBuffer$Type;->BindPoseTangent:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v6

    if-eqz v6, :cond_1

    sget-object v19, Lcom/jme3/scene/VertexBuffer$Type;->Tangent:Lcom/jme3/scene/VertexBuffer$Type;

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v18

    check-cast v18, Ljava/nio/FloatBuffer;

    invoke-virtual {v6}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v9

    check-cast v9, Ljava/nio/FloatBuffer;

    invoke-virtual/range {v18 .. v18}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    invoke-virtual {v9}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/nio/FloatBuffer;->put(Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    :cond_1
    invoke-virtual {v15, v8}, Ljava/nio/FloatBuffer;->put(Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    invoke-virtual {v13, v7}, Ljava/nio/FloatBuffer;->put(Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    :cond_3
    return-void
.end method

.method public setSpatial(Lcom/jme3/scene/Spatial;)V
    .locals 2
    .param p1    # Lcom/jme3/scene/Spatial;

    invoke-super {p0, p1}, Lcom/jme3/scene/control/AbstractControl;->setSpatial(Lcom/jme3/scene/Spatial;)V

    if-eqz p1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/jme3/scene/Node;

    invoke-direct {p0, v0}, Lcom/jme3/animation/SkeletonControl;->findTargets(Lcom/jme3/scene/Node;)[Lcom/jme3/scene/Mesh;

    move-result-object v1

    iput-object v1, p0, Lcom/jme3/animation/SkeletonControl;->targets:[Lcom/jme3/scene/Mesh;

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/jme3/animation/SkeletonControl;->targets:[Lcom/jme3/scene/Mesh;

    goto :goto_0
.end method
