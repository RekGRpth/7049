.class public Lcom/jme3/app/state/AbstractAppState;
.super Ljava/lang/Object;
.source "AbstractAppState.java"

# interfaces
.implements Lcom/jme3/app/state/AppState;


# instance fields
.field private enabled:Z

.field protected initialized:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/app/state/AbstractAppState;->initialized:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/app/state/AbstractAppState;->enabled:Z

    return-void
.end method


# virtual methods
.method public cleanup()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/app/state/AbstractAppState;->initialized:Z

    return-void
.end method

.method public initialize(Lcom/jme3/app/state/AppStateManager;Lcom/jme3/app/Application;)V
    .locals 1
    .param p1    # Lcom/jme3/app/state/AppStateManager;
    .param p2    # Lcom/jme3/app/Application;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/app/state/AbstractAppState;->initialized:Z

    return-void
.end method

.method public isEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/app/state/AbstractAppState;->enabled:Z

    return v0
.end method

.method public postRender()V
    .locals 0

    return-void
.end method

.method public render(Lcom/jme3/renderer/RenderManager;)V
    .locals 0
    .param p1    # Lcom/jme3/renderer/RenderManager;

    return-void
.end method

.method public stateAttached(Lcom/jme3/app/state/AppStateManager;)V
    .locals 0
    .param p1    # Lcom/jme3/app/state/AppStateManager;

    return-void
.end method

.method public stateDetached(Lcom/jme3/app/state/AppStateManager;)V
    .locals 0
    .param p1    # Lcom/jme3/app/state/AppStateManager;

    return-void
.end method

.method public update(F)V
    .locals 0
    .param p1    # F

    return-void
.end method
