.class public Lcom/jme3/asset/TextureKey;
.super Lcom/jme3/asset/AssetKey;
.source "TextureKey.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jme3/asset/AssetKey",
        "<",
        "Lcom/jme3/texture/Texture;",
        ">;"
    }
.end annotation


# instance fields
.field private anisotropy:I

.field private asCube:Z

.field private asTexture3D:Z

.field private flipY:Z

.field private generateMips:Z

.field private textureTypeHint:Lcom/jme3/texture/Texture$Type;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/jme3/asset/AssetKey;-><init>()V

    sget-object v0, Lcom/jme3/texture/Texture$Type;->TwoDimensional:Lcom/jme3/texture/Texture$Type;

    iput-object v0, p0, Lcom/jme3/asset/TextureKey;->textureTypeHint:Lcom/jme3/texture/Texture$Type;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/jme3/asset/AssetKey;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/jme3/texture/Texture$Type;->TwoDimensional:Lcom/jme3/texture/Texture$Type;

    iput-object v0, p0, Lcom/jme3/asset/TextureKey;->textureTypeHint:Lcom/jme3/texture/Texture$Type;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/asset/TextureKey;->flipY:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-direct {p0, p1}, Lcom/jme3/asset/AssetKey;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/jme3/texture/Texture$Type;->TwoDimensional:Lcom/jme3/texture/Texture$Type;

    iput-object v0, p0, Lcom/jme3/asset/TextureKey;->textureTypeHint:Lcom/jme3/texture/Texture$Type;

    iput-boolean p2, p0, Lcom/jme3/asset/TextureKey;->flipY:Z

    return-void
.end method


# virtual methods
.method public createClonedInstance(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;

    move-object v0, p1

    check-cast v0, Lcom/jme3/texture/Texture;

    invoke-virtual {v0}, Lcom/jme3/texture/Texture;->createSimpleClone()Lcom/jme3/texture/Texture;

    move-result-object v1

    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/jme3/asset/TextureKey;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/jme3/asset/AssetKey;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/jme3/asset/TextureKey;->isFlipY()Z

    move-result v1

    check-cast p1, Lcom/jme3/asset/TextureKey;

    invoke-virtual {p1}, Lcom/jme3/asset/TextureKey;->isFlipY()Z

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getAnisotropy()I
    .locals 1

    iget v0, p0, Lcom/jme3/asset/TextureKey;->anisotropy:I

    return v0
.end method

.method public isAsCube()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/asset/TextureKey;->asCube:Z

    return v0
.end method

.method public isAsTexture3D()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/asset/TextureKey;->asTexture3D:Z

    return v0
.end method

.method public isFlipY()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/asset/TextureKey;->flipY:Z

    return v0
.end method

.method public isGenerateMips()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/asset/TextureKey;->generateMips:Z

    return v0
.end method

.method public postProcess(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;

    const/4 v5, 0x3

    const/4 v4, 0x2

    move-object v0, p1

    check-cast v0, Lcom/jme3/texture/Image;

    if-nez v0, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p0}, Lcom/jme3/asset/TextureKey;->isAsCube()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/jme3/asset/TextureKey;->isFlipY()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, v4}, Lcom/jme3/texture/Image;->getData(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v0, v5}, Lcom/jme3/texture/Image;->getData(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v0, v4, v3}, Lcom/jme3/texture/Image;->setData(ILjava/nio/ByteBuffer;)V

    invoke-virtual {v0, v5, v1}, Lcom/jme3/texture/Image;->setData(ILjava/nio/ByteBuffer;)V

    :cond_1
    new-instance v2, Lcom/jme3/texture/TextureCubeMap;

    invoke-direct {v2}, Lcom/jme3/texture/TextureCubeMap;-><init>()V

    :goto_1
    invoke-virtual {v0}, Lcom/jme3/texture/Image;->hasMipmaps()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/jme3/asset/TextureKey;->isGenerateMips()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    sget-object v3, Lcom/jme3/texture/Texture$MinFilter;->Trilinear:Lcom/jme3/texture/Texture$MinFilter;

    invoke-virtual {v2, v3}, Lcom/jme3/texture/Texture;->setMinFilter(Lcom/jme3/texture/Texture$MinFilter;)V

    :cond_3
    invoke-virtual {p0}, Lcom/jme3/asset/TextureKey;->getAnisotropy()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/jme3/texture/Texture;->setAnisotropicFilter(I)V

    invoke-virtual {p0}, Lcom/jme3/asset/TextureKey;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/jme3/texture/Texture;->setName(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcom/jme3/texture/Texture;->setImage(Lcom/jme3/texture/Image;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/jme3/asset/TextureKey;->isAsTexture3D()Z

    move-result v3

    if-eqz v3, :cond_5

    new-instance v2, Lcom/jme3/texture/Texture3D;

    invoke-direct {v2}, Lcom/jme3/texture/Texture3D;-><init>()V

    goto :goto_1

    :cond_5
    new-instance v2, Lcom/jme3/texture/Texture2D;

    invoke-direct {v2}, Lcom/jme3/texture/Texture2D;-><init>()V

    goto :goto_1
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 3
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/jme3/asset/AssetKey;->read(Lcom/jme3/export/JmeImporter;)V

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v0

    const-string v1, "flip_y"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/jme3/asset/TextureKey;->flipY:Z

    const-string v1, "generate_mips"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/jme3/asset/TextureKey;->generateMips:Z

    const-string v1, "as_cubemap"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/jme3/asset/TextureKey;->asCube:Z

    const-string v1, "anisotropy"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/jme3/asset/TextureKey;->anisotropy:I

    return-void
.end method

.method public setAsCube(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jme3/asset/TextureKey;->asCube:Z

    return-void
.end method

.method public setGenerateMips(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jme3/asset/TextureKey;->generateMips:Z

    return-void
.end method

.method public setTextureTypeHint(Lcom/jme3/texture/Texture$Type;)V
    .locals 0
    .param p1    # Lcom/jme3/texture/Texture$Type;

    iput-object p1, p0, Lcom/jme3/asset/TextureKey;->textureTypeHint:Lcom/jme3/texture/Texture$Type;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/jme3/asset/TextureKey;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/jme3/asset/TextureKey;->flipY:Z

    if-eqz v0, :cond_0

    const-string v0, " (Flipped)"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/jme3/asset/TextureKey;->asCube:Z

    if-eqz v0, :cond_1

    const-string v0, " (Cube)"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/jme3/asset/TextureKey;->generateMips:Z

    if-eqz v0, :cond_2

    const-string v0, " (Mipmaped)"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1

    :cond_2
    const-string v0, ""

    goto :goto_2
.end method

.method public useSmartCache()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
