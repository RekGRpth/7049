.class public Lcom/jme3/audio/Listener;
.super Ljava/lang/Object;
.source "Listener.java"


# instance fields
.field private location:Lcom/jme3/math/Vector3f;

.field private renderer:Lcom/jme3/audio/AudioRenderer;

.field private rotation:Lcom/jme3/math/Quaternion;

.field private velocity:Lcom/jme3/math/Vector3f;

.field private volume:F


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/jme3/audio/Listener;->volume:F

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v0, p0, Lcom/jme3/audio/Listener;->location:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v0, p0, Lcom/jme3/audio/Listener;->velocity:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Quaternion;

    invoke-direct {v0}, Lcom/jme3/math/Quaternion;-><init>()V

    iput-object v0, p0, Lcom/jme3/audio/Listener;->rotation:Lcom/jme3/math/Quaternion;

    return-void
.end method


# virtual methods
.method public setRenderer(Lcom/jme3/audio/AudioRenderer;)V
    .locals 0
    .param p1    # Lcom/jme3/audio/AudioRenderer;

    iput-object p1, p0, Lcom/jme3/audio/Listener;->renderer:Lcom/jme3/audio/AudioRenderer;

    return-void
.end method
