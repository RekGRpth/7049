.class public Lcom/jme3/audio/android/AndroidAudioData;
.super Lcom/jme3/audio/AudioData;
.source "AndroidAudioData.java"


# instance fields
.field protected assetKey:Lcom/jme3/asset/AssetKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/asset/AssetKey",
            "<*>;"
        }
    .end annotation
.end field

.field protected currentVolume:F


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/jme3/audio/AudioData;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jme3/audio/android/AndroidAudioData;->currentVolume:F

    return-void
.end method

.method protected constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/jme3/audio/AudioData;-><init>(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jme3/audio/android/AndroidAudioData;->currentVolume:F

    return-void
.end method


# virtual methods
.method public createDestructableClone()Lcom/jme3/util/NativeObject;
    .locals 2

    new-instance v0, Lcom/jme3/audio/android/AndroidAudioData;

    iget v1, p0, Lcom/jme3/audio/android/AndroidAudioData;->id:I

    invoke-direct {v0, v1}, Lcom/jme3/audio/android/AndroidAudioData;-><init>(I)V

    return-object v0
.end method

.method public deleteObject(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/jme3/audio/AudioRenderer;

    invoke-interface {p1, p0}, Lcom/jme3/audio/AudioRenderer;->deleteAudioData(Lcom/jme3/audio/AudioData;)V

    return-void
.end method

.method public getCurrentVolume()F
    .locals 1

    iget v0, p0, Lcom/jme3/audio/android/AndroidAudioData;->currentVolume:F

    return v0
.end method

.method public resetObject()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/jme3/audio/android/AndroidAudioData;->id:I

    invoke-virtual {p0}, Lcom/jme3/audio/android/AndroidAudioData;->setUpdateNeeded()V

    return-void
.end method

.method public setAssetKey(Lcom/jme3/asset/AssetKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jme3/asset/AssetKey",
            "<*>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/jme3/audio/android/AndroidAudioData;->assetKey:Lcom/jme3/asset/AssetKey;

    return-void
.end method

.method public setCurrentVolume(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/jme3/audio/android/AndroidAudioData;->currentVolume:F

    return-void
.end method
