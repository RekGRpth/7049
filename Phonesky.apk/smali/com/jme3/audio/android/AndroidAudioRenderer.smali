.class public Lcom/jme3/audio/android/AndroidAudioRenderer;
.super Ljava/lang/Object;
.source "AndroidAudioRenderer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/SoundPool$OnLoadCompleteListener;
.implements Lcom/jme3/audio/AudioRenderer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/audio/android/AndroidAudioRenderer$1;
    }
.end annotation


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private final assetManager:Landroid/content/res/AssetManager;

.field private audioDisabled:Z

.field private final context:Landroid/content/Context;

.field private final distanceVector:Lcom/jme3/math/Vector3f;

.field private listener:Lcom/jme3/audio/Listener;

.field private final listenerPosition:Lcom/jme3/math/Vector3f;

.field private final manager:Landroid/media/AudioManager;

.field private final musicPlaying:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/jme3/audio/AudioNode;",
            "Landroid/media/MediaPlayer;",
            ">;"
        }
    .end annotation
.end field

.field private soundPool:Landroid/media/SoundPool;

.field private soundpoolStillLoading:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/jme3/audio/AudioNode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/audio/android/AndroidAudioRenderer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/audio/android/AndroidAudioRenderer;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->musicPlaying:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->soundPool:Landroid/media/SoundPool;

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v0, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->listenerPosition:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v0, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->distanceVector:Lcom/jme3/math/Vector3f;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->soundpoolStillLoading:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->audioDisabled:Z

    iput-object p1, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->context:Landroid/content/Context;

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->manager:Landroid/media/AudioManager;

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setVolumeControlStream(I)V

    invoke-virtual {p1}, Landroid/app/Activity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->assetManager:Landroid/content/res/AssetManager;

    return-void
.end method


# virtual methods
.method public cleanup()V
    .locals 4

    iget-object v3, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->soundPool:Landroid/media/SoundPool;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->soundPool:Landroid/media/SoundPool;

    invoke-virtual {v3}, Landroid/media/SoundPool;->release()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->soundPool:Landroid/media/SoundPool;

    :cond_0
    iget-object v3, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->musicPlaying:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/audio/AudioNode;

    iget-object v3, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->musicPlaying:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    sget-object v3, Lcom/jme3/audio/AudioNode$Status;->Stopped:Lcom/jme3/audio/AudioNode$Status;

    invoke-virtual {v2, v3}, Lcom/jme3/audio/AudioNode;->setStatus(Lcom/jme3/audio/AudioNode$Status;)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->musicPlaying:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public deleteAudioData(Lcom/jme3/audio/AudioData;)V
    .locals 6
    .param p1    # Lcom/jme3/audio/AudioData;

    const/4 v5, -0x1

    iget-object v3, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->musicPlaying:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/audio/AudioNode;

    invoke-virtual {v2}, Lcom/jme3/audio/AudioNode;->getAudioData()Lcom/jme3/audio/AudioData;

    move-result-object v3

    if-ne v3, p1, :cond_0

    iget-object v3, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->musicPlaying:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    sget-object v3, Lcom/jme3/audio/AudioNode$Status;->Stopped:Lcom/jme3/audio/AudioNode$Status;

    invoke-virtual {v2, v3}, Lcom/jme3/audio/AudioNode;->setStatus(Lcom/jme3/audio/AudioNode$Status;)V

    invoke-virtual {v2, v5}, Lcom/jme3/audio/AudioNode;->setChannel(I)V

    invoke-virtual {p1, v5}, Lcom/jme3/audio/AudioData;->setId(I)V

    :cond_1
    invoke-virtual {p1}, Lcom/jme3/audio/AudioData;->getId()I

    move-result v3

    if-lez v3, :cond_2

    iget-object v3, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->soundPool:Landroid/media/SoundPool;

    invoke-virtual {p1}, Lcom/jme3/audio/AudioData;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/media/SoundPool;->unload(I)Z

    invoke-virtual {p1, v5}, Lcom/jme3/audio/AudioData;->setId(I)V

    :cond_2
    return-void
.end method

.method public deleteFilter(Lcom/jme3/audio/Filter;)V
    .locals 0
    .param p1    # Lcom/jme3/audio/Filter;

    return-void
.end method

.method public initialize()V
    .locals 4

    new-instance v0, Landroid/media/SoundPool;

    const/16 v1, 0x10

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->soundPool:Landroid/media/SoundPool;

    iget-object v0, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->soundPool:Landroid/media/SoundPool;

    invoke-virtual {v0, p0}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/media/MediaPlayer;->seekTo(I)V

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->stop()V

    iget-object v2, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->musicPlaying:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/audio/AudioNode;

    iget-object v2, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->musicPlaying:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_0

    sget-object v2, Lcom/jme3/audio/AudioNode$Status;->Stopped:Lcom/jme3/audio/AudioNode$Status;

    invoke-virtual {v1, v2}, Lcom/jme3/audio/AudioNode;->setStatus(Lcom/jme3/audio/AudioNode$Status;)V

    :cond_1
    return-void
.end method

.method public onLoadComplete(Landroid/media/SoundPool;II)V
    .locals 10
    .param p1    # Landroid/media/SoundPool;
    .param p2    # I
    .param p3    # I

    const/high16 v2, 0x3f800000

    iget-object v0, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->soundpoolStillLoading:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/jme3/audio/AudioNode;

    if-nez v9, :cond_1

    sget-object v0, Lcom/jme3/audio/android/AndroidAudioRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "Something went terribly wrong! onLoadComplete had sampleId which was not in the HashMap of loading items"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v9}, Lcom/jme3/audio/AudioNode;->getAudioData()Lcom/jme3/audio/AudioData;

    move-result-object v7

    if-nez p3, :cond_0

    invoke-virtual {v7}, Lcom/jme3/audio/AudioData;->getId()I

    move-result v1

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p1

    move v3, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    move-result v8

    invoke-virtual {v9, v8}, Lcom/jme3/audio/AudioNode;->setChannel(I)V

    goto :goto_0
.end method

.method public pauseAll()V
    .locals 3

    iget-object v2, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->soundPool:Landroid/media/SoundPool;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->soundPool:Landroid/media/SoundPool;

    invoke-virtual {v2}, Landroid/media/SoundPool;->autoPause()V

    iget-object v2, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->musicPlaying:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->pause()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public resumeAll()V
    .locals 3

    iget-object v2, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->soundPool:Landroid/media/SoundPool;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->soundPool:Landroid/media/SoundPool;

    invoke-virtual {v2}, Landroid/media/SoundPool;->autoResume()V

    iget-object v2, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->musicPlaying:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setListener(Lcom/jme3/audio/Listener;)V
    .locals 2
    .param p1    # Lcom/jme3/audio/Listener;

    iget-boolean v0, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->audioDisabled:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->listener:Lcom/jme3/audio/Listener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->listener:Lcom/jme3/audio/Listener;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jme3/audio/Listener;->setRenderer(Lcom/jme3/audio/AudioRenderer;)V

    :cond_1
    iput-object p1, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->listener:Lcom/jme3/audio/Listener;

    iget-object v0, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->listener:Lcom/jme3/audio/Listener;

    invoke-virtual {v0, p0}, Lcom/jme3/audio/Listener;->setRenderer(Lcom/jme3/audio/AudioRenderer;)V

    goto :goto_0
.end method

.method public update(F)V
    .locals 8
    .param p1    # F

    iget-object v6, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->musicPlaying:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jme3/audio/AudioNode;

    iget-object v6, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->musicPlaying:Ljava/util/HashMap;

    invoke-virtual {v6, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/MediaPlayer;

    iget-object v6, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->distanceVector:Lcom/jme3/math/Vector3f;

    iget-object v7, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->listenerPosition:Lcom/jme3/math/Vector3f;

    invoke-virtual {v6, v7}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    iget-object v6, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->distanceVector:Lcom/jme3/math/Vector3f;

    invoke-virtual {v4}, Lcom/jme3/audio/AudioNode;->getLocalTranslation()Lcom/jme3/math/Vector3f;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/jme3/math/Vector3f;->subtractLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    iget-object v6, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->distanceVector:Lcom/jme3/math/Vector3f;

    invoke-virtual {v6}, Lcom/jme3/math/Vector3f;->length()F

    move-result v6

    invoke-static {v6}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v1

    invoke-virtual {v4}, Lcom/jme3/audio/AudioNode;->getRefDistance()F

    move-result v6

    cmpg-float v6, v1, v6

    if-gez v6, :cond_1

    invoke-virtual {v4}, Lcom/jme3/audio/AudioNode;->getRefDistance()F

    move-result v1

    :cond_1
    invoke-virtual {v4}, Lcom/jme3/audio/AudioNode;->getMaxDistance()F

    move-result v6

    cmpl-float v6, v1, v6

    if-lez v6, :cond_2

    invoke-virtual {v4}, Lcom/jme3/audio/AudioNode;->getMaxDistance()F

    move-result v1

    :cond_2
    invoke-virtual {v4}, Lcom/jme3/audio/AudioNode;->getRefDistance()F

    move-result v6

    div-float v5, v6, v1

    invoke-virtual {v4}, Lcom/jme3/audio/AudioNode;->getAudioData()Lcom/jme3/audio/AudioData;

    move-result-object v0

    check-cast v0, Lcom/jme3/audio/android/AndroidAudioData;

    invoke-virtual {v0}, Lcom/jme3/audio/android/AndroidAudioData;->getCurrentVolume()F

    move-result v6

    sub-float/2addr v6, v5

    invoke-static {v6}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v6

    const/high16 v7, 0x34000000

    cmpl-float v6, v6, v7

    if-lez v6, :cond_0

    invoke-virtual {v3, v5, v5}, Landroid/media/MediaPlayer;->setVolume(FF)V

    invoke-virtual {v0, v5}, Lcom/jme3/audio/android/AndroidAudioData;->setCurrentVolume(F)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public updateSourceParam(Lcom/jme3/audio/AudioNode;Lcom/jme3/audio/AudioParam;)V
    .locals 8
    .param p1    # Lcom/jme3/audio/AudioNode;
    .param p2    # Lcom/jme3/audio/AudioParam;

    iget-boolean v4, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->audioDisabled:Z

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->getChannel()I

    move-result v4

    if-ltz v4, :cond_0

    sget-object v4, Lcom/jme3/audio/android/AndroidAudioRenderer$1;->$SwitchMap$com$jme3$audio$AudioParam:[I

    invoke-virtual {p2}, Lcom/jme3/audio/AudioParam;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->isPositional()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->getWorldTranslation()Lcom/jme3/math/Vector3f;

    move-result-object v2

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->isPositional()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->getVelocity()Lcom/jme3/math/Vector3f;

    move-result-object v3

    goto :goto_0

    :pswitch_3
    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->isPositional()Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_0

    :pswitch_4
    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->isPositional()Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_0

    :pswitch_5
    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->isPositional()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->isReverbEnabled()Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_0

    :pswitch_6
    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->isPositional()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->isReverbEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/jme3/audio/AudioParam;->ReverbFilter:Lcom/jme3/audio/AudioParam;

    invoke-virtual {p0, p1, v4}, Lcom/jme3/audio/android/AndroidAudioRenderer;->updateSourceParam(Lcom/jme3/audio/AudioNode;Lcom/jme3/audio/AudioParam;)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->isDirectional()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->getDirection()Lcom/jme3/math/Vector3f;

    move-result-object v0

    goto :goto_0

    :pswitch_8
    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->isDirectional()Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_0

    :pswitch_9
    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->isDirectional()Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_0

    :pswitch_a
    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->isDirectional()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/jme3/audio/AudioParam;->Direction:Lcom/jme3/audio/AudioParam;

    invoke-virtual {p0, p1, v4}, Lcom/jme3/audio/android/AndroidAudioRenderer;->updateSourceParam(Lcom/jme3/audio/AudioNode;Lcom/jme3/audio/AudioParam;)V

    sget-object v4, Lcom/jme3/audio/AudioParam;->InnerAngle:Lcom/jme3/audio/AudioParam;

    invoke-virtual {p0, p1, v4}, Lcom/jme3/audio/android/AndroidAudioRenderer;->updateSourceParam(Lcom/jme3/audio/AudioNode;Lcom/jme3/audio/AudioParam;)V

    sget-object v4, Lcom/jme3/audio/AudioParam;->OuterAngle:Lcom/jme3/audio/AudioParam;

    invoke-virtual {p0, p1, v4}, Lcom/jme3/audio/android/AndroidAudioRenderer;->updateSourceParam(Lcom/jme3/audio/AudioNode;Lcom/jme3/audio/AudioParam;)V

    goto/16 :goto_0

    :pswitch_b
    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->getDryFilter()Lcom/jme3/audio/Filter;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->getDryFilter()Lcom/jme3/audio/Filter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jme3/audio/Filter;->isUpdateNeeded()Z

    move-result v4

    if-eqz v4, :cond_0

    goto/16 :goto_0

    :pswitch_c
    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->isLooping()Z

    move-result v4

    if-eqz v4, :cond_0

    goto/16 :goto_0

    :pswitch_d
    iget-object v4, p0, Lcom/jme3/audio/android/AndroidAudioRenderer;->soundPool:Landroid/media/SoundPool;

    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->getChannel()I

    move-result v5

    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->getVolume()F

    move-result v6

    invoke-virtual {p1}, Lcom/jme3/audio/AudioNode;->getVolume()F

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Landroid/media/SoundPool;->setVolume(IFF)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
