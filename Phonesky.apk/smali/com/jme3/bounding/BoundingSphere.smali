.class public Lcom/jme3/bounding/BoundingSphere;
.super Lcom/jme3/bounding/BoundingVolume;
.source "BoundingSphere.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/bounding/BoundingSphere$1;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field radius:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/bounding/BoundingSphere;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/jme3/bounding/BoundingSphere;->$assertionsDisabled:Z

    const-class v0, Lcom/jme3/bounding/BoundingSphere;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/bounding/BoundingSphere;->logger:Ljava/util/logging/Logger;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jme3/bounding/BoundingVolume;-><init>()V

    return-void
.end method

.method public constructor <init>(FLcom/jme3/math/Vector3f;)V
    .locals 1
    .param p1    # F
    .param p2    # Lcom/jme3/math/Vector3f;

    invoke-direct {p0}, Lcom/jme3/bounding/BoundingVolume;-><init>()V

    iget-object v0, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, p2}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    iput p1, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    return-void
.end method

.method private collideWithRay(Lcom/jme3/math/Ray;Lcom/jme3/collision/CollisionResults;)I
    .locals 15
    .param p1    # Lcom/jme3/math/Ray;
    .param p2    # Lcom/jme3/collision/CollisionResults;

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v10

    iget-object v11, v10, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/math/Ray;->getOrigin()Lcom/jme3/math/Vector3f;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v11

    iget-object v12, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v11, v12}, Lcom/jme3/math/Vector3f;->subtractLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v3

    invoke-virtual {v3, v3}, Lcom/jme3/math/Vector3f;->dot(Lcom/jme3/math/Vector3f;)F

    move-result v11

    invoke-virtual {p0}, Lcom/jme3/bounding/BoundingSphere;->getRadius()F

    move-result v12

    invoke-virtual {p0}, Lcom/jme3/bounding/BoundingSphere;->getRadius()F

    move-result v13

    mul-float/2addr v12, v13

    sub-float v1, v11, v12

    float-to-double v11, v1

    const-wide/16 v13, 0x0

    cmpg-double v11, v11, v13

    if-gtz v11, :cond_0

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-virtual {v11, v3}, Lcom/jme3/math/Vector3f;->dot(Lcom/jme3/math/Vector3f;)F

    move-result v2

    mul-float v11, v2, v2

    sub-float v4, v11, v1

    invoke-static {v4}, Lcom/jme3/math/FastMath;->sqrt(F)F

    move-result v9

    sub-float v6, v9, v2

    new-instance v11, Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-direct {v11, v12}, Lcom/jme3/math/Vector3f;-><init>(Lcom/jme3/math/Vector3f;)V

    invoke-virtual {v11, v6}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v11

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    invoke-virtual {v11, v12}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v7

    new-instance v8, Lcom/jme3/collision/CollisionResult;

    invoke-direct {v8, v7, v6}, Lcom/jme3/collision/CollisionResult;-><init>(Lcom/jme3/math/Vector3f;F)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Lcom/jme3/collision/CollisionResults;->addCollision(Lcom/jme3/collision/CollisionResult;)V

    invoke-virtual {v10}, Lcom/jme3/util/TempVars;->release()V

    const/4 v11, 0x1

    :goto_0
    return v11

    :cond_0
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-virtual {v11, v3}, Lcom/jme3/math/Vector3f;->dot(Lcom/jme3/math/Vector3f;)F

    move-result v2

    invoke-virtual {v10}, Lcom/jme3/util/TempVars;->release()V

    float-to-double v11, v2

    const-wide/16 v13, 0x0

    cmpl-double v11, v11, v13

    if-ltz v11, :cond_1

    const/4 v11, 0x0

    goto :goto_0

    :cond_1
    mul-float v11, v2, v2

    sub-float v4, v11, v1

    float-to-double v11, v4

    const-wide/16 v13, 0x0

    cmpg-double v11, v11, v13

    if-gez v11, :cond_2

    const/4 v11, 0x0

    goto :goto_0

    :cond_2
    const v11, 0x38d1b717

    cmpl-float v11, v4, v11

    if-ltz v11, :cond_3

    invoke-static {v4}, Lcom/jme3/math/FastMath;->sqrt(F)F

    move-result v9

    neg-float v11, v2

    sub-float v5, v11, v9

    new-instance v11, Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-direct {v11, v12}, Lcom/jme3/math/Vector3f;-><init>(Lcom/jme3/math/Vector3f;)V

    invoke-virtual {v11, v5}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v11

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    invoke-virtual {v11, v12}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v7

    new-instance v11, Lcom/jme3/collision/CollisionResult;

    invoke-direct {v11, v7, v5}, Lcom/jme3/collision/CollisionResult;-><init>(Lcom/jme3/math/Vector3f;F)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/jme3/collision/CollisionResults;->addCollision(Lcom/jme3/collision/CollisionResult;)V

    neg-float v11, v2

    add-float v5, v11, v9

    new-instance v11, Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-direct {v11, v12}, Lcom/jme3/math/Vector3f;-><init>(Lcom/jme3/math/Vector3f;)V

    invoke-virtual {v11, v5}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v11

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    invoke-virtual {v11, v12}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v7

    new-instance v11, Lcom/jme3/collision/CollisionResult;

    invoke-direct {v11, v7, v5}, Lcom/jme3/collision/CollisionResult;-><init>(Lcom/jme3/math/Vector3f;F)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/jme3/collision/CollisionResults;->addCollision(Lcom/jme3/collision/CollisionResult;)V

    const/4 v11, 0x2

    goto :goto_0

    :cond_3
    neg-float v5, v2

    new-instance v11, Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-direct {v11, v12}, Lcom/jme3/math/Vector3f;-><init>(Lcom/jme3/math/Vector3f;)V

    invoke-virtual {v11, v5}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v11

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    invoke-virtual {v11, v12}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v7

    new-instance v11, Lcom/jme3/collision/CollisionResult;

    invoke-direct {v11, v7, v5}, Lcom/jme3/collision/CollisionResult;-><init>(Lcom/jme3/math/Vector3f;F)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/jme3/collision/CollisionResults;->addCollision(Lcom/jme3/collision/CollisionResult;)V

    const/4 v11, 0x1

    goto/16 :goto_0
.end method

.method private getMaxAxis(Lcom/jme3/math/Vector3f;)F
    .locals 4
    .param p1    # Lcom/jme3/math/Vector3f;

    iget v3, p1, Lcom/jme3/math/Vector3f;->x:F

    invoke-static {v3}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    iget v3, p1, Lcom/jme3/math/Vector3f;->y:F

    invoke-static {v3}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v1

    iget v3, p1, Lcom/jme3/math/Vector3f;->z:F

    invoke-static {v3}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v2

    cmpl-float v3, v0, v1

    if-ltz v3, :cond_1

    cmpl-float v3, v0, v2

    if-ltz v3, :cond_0

    move v2, v0

    :cond_0
    :goto_0
    return v2

    :cond_1
    cmpl-float v3, v1, v2

    if-ltz v3, :cond_0

    move v2, v1

    goto :goto_0
.end method

.method private merge(FLcom/jme3/math/Vector3f;Lcom/jme3/bounding/BoundingSphere;)Lcom/jme3/bounding/BoundingVolume;
    .locals 10
    .param p1    # F
    .param p2    # Lcom/jme3/math/Vector3f;
    .param p3    # Lcom/jme3/bounding/BoundingSphere;

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v7

    iget-object v8, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    iget-object v9, v7, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    invoke-virtual {p2, v8, v9}, Lcom/jme3/math/Vector3f;->subtract(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jme3/math/Vector3f;->lengthSquared()F

    move-result v4

    iget v8, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    sub-float v6, p1, v8

    mul-float v2, v6, v6

    cmpl-float v8, v2, v4

    if-ltz v8, :cond_2

    const/4 v8, 0x0

    cmpg-float v8, v6, v8

    if-gtz v8, :cond_0

    invoke-virtual {v7}, Lcom/jme3/util/TempVars;->release()V

    move-object p3, p0

    :goto_0
    return-object p3

    :cond_0
    iget-object v5, p3, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    if-nez v5, :cond_1

    new-instance v5, Lcom/jme3/math/Vector3f;

    invoke-direct {v5}, Lcom/jme3/math/Vector3f;-><init>()V

    invoke-virtual {p3, v5}, Lcom/jme3/bounding/BoundingSphere;->setCenter(Lcom/jme3/math/Vector3f;)V

    :cond_1
    invoke-virtual {v5, p2}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    invoke-virtual {p3, p1}, Lcom/jme3/bounding/BoundingSphere;->setRadius(F)V

    invoke-virtual {v7}, Lcom/jme3/util/TempVars;->release()V

    goto :goto_0

    :cond_2
    float-to-double v8, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v3, v8

    iget-object v5, p3, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    if-nez v5, :cond_3

    new-instance v5, Lcom/jme3/math/Vector3f;

    invoke-direct {v5}, Lcom/jme3/math/Vector3f;-><init>()V

    invoke-virtual {p3, v5}, Lcom/jme3/bounding/BoundingSphere;->setCenter(Lcom/jme3/math/Vector3f;)V

    :cond_3
    const v8, 0x3f800054

    cmpl-float v8, v3, v8

    if-lez v8, :cond_4

    add-float v8, v3, v6

    const/high16 v9, 0x40000000

    mul-float/2addr v9, v3

    div-float v0, v8, v9

    iget-object v8, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v1, v0}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    :goto_1
    const/high16 v8, 0x3f000000

    iget v9, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    add-float/2addr v9, v3

    add-float/2addr v9, p1

    mul-float/2addr v8, v9

    invoke-virtual {p3, v8}, Lcom/jme3/bounding/BoundingSphere;->setRadius(F)V

    invoke-virtual {v7}, Lcom/jme3/util/TempVars;->release()V

    goto :goto_0

    :cond_4
    iget-object v8, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v5, v8}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    goto :goto_1
.end method

.method private recurseMini(Ljava/nio/FloatBuffer;III)V
    .locals 9
    .param p1    # Ljava/nio/FloatBuffer;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v7, 0x0

    new-instance v2, Lcom/jme3/math/Vector3f;

    invoke-direct {v2}, Lcom/jme3/math/Vector3f;-><init>()V

    new-instance v3, Lcom/jme3/math/Vector3f;

    invoke-direct {v3}, Lcom/jme3/math/Vector3f;-><init>()V

    new-instance v4, Lcom/jme3/math/Vector3f;

    invoke-direct {v4}, Lcom/jme3/math/Vector3f;-><init>()V

    new-instance v5, Lcom/jme3/math/Vector3f;

    invoke-direct {v5}, Lcom/jme3/math/Vector3f;-><init>()V

    packed-switch p3, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p2, :cond_0

    add-int v6, v0, p4

    invoke-static {v2, p1, v6}, Lcom/jme3/util/BufferUtils;->populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    iget-object v6, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v2, v6}, Lcom/jme3/math/Vector3f;->distanceSquared(Lcom/jme3/math/Vector3f;)F

    move-result v6

    iget v7, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    iget v8, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    const/high16 v7, 0x37280000

    cmpl-float v6, v6, v7

    if-lez v6, :cond_2

    move v1, v0

    :goto_2
    if-lez v1, :cond_1

    add-int v6, v1, p4

    invoke-static {v3, p1, v6}, Lcom/jme3/util/BufferUtils;->populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    add-int/lit8 v6, v1, -0x1

    add-int/2addr v6, p4

    invoke-static {v4, p1, v6}, Lcom/jme3/util/BufferUtils;->populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    add-int v6, v1, p4

    invoke-static {v4, p1, v6}, Lcom/jme3/util/BufferUtils;->setInBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    add-int/lit8 v6, v1, -0x1

    add-int/2addr v6, p4

    invoke-static {v3, p1, v6}, Lcom/jme3/util/BufferUtils;->setInBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :pswitch_0
    iput v7, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    iget-object v6, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v6, v7, v7, v7}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    goto :goto_0

    :pswitch_1
    const/high16 v6, -0x48d80000

    iput v6, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    iget-object v6, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    add-int/lit8 v7, p4, -0x1

    invoke-static {v6, p1, v7}, Lcom/jme3/util/BufferUtils;->populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    goto :goto_0

    :pswitch_2
    add-int/lit8 v6, p4, -0x1

    invoke-static {v2, p1, v6}, Lcom/jme3/util/BufferUtils;->populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    add-int/lit8 v6, p4, -0x2

    invoke-static {v3, p1, v6}, Lcom/jme3/util/BufferUtils;->populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    invoke-direct {p0, v2, v3}, Lcom/jme3/bounding/BoundingSphere;->setSphere(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V

    goto :goto_0

    :pswitch_3
    add-int/lit8 v6, p4, -0x1

    invoke-static {v2, p1, v6}, Lcom/jme3/util/BufferUtils;->populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    add-int/lit8 v6, p4, -0x2

    invoke-static {v3, p1, v6}, Lcom/jme3/util/BufferUtils;->populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    add-int/lit8 v6, p4, -0x3

    invoke-static {v4, p1, v6}, Lcom/jme3/util/BufferUtils;->populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    invoke-direct {p0, v2, v3, v4}, Lcom/jme3/bounding/BoundingSphere;->setSphere(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V

    goto :goto_0

    :pswitch_4
    add-int/lit8 v6, p4, -0x1

    invoke-static {v2, p1, v6}, Lcom/jme3/util/BufferUtils;->populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    add-int/lit8 v6, p4, -0x2

    invoke-static {v3, p1, v6}, Lcom/jme3/util/BufferUtils;->populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    add-int/lit8 v6, p4, -0x3

    invoke-static {v4, p1, v6}, Lcom/jme3/util/BufferUtils;->populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    add-int/lit8 v6, p4, -0x4

    invoke-static {v5, p1, v6}, Lcom/jme3/util/BufferUtils;->populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/jme3/bounding/BoundingSphere;->setSphere(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v6, p3, 0x1

    add-int/lit8 v7, p4, 0x1

    invoke-direct {p0, p1, v0, v6, v7}, Lcom/jme3/bounding/BoundingSphere;->recurseMini(Ljava/nio/FloatBuffer;III)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private setSphere(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V
    .locals 4
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    iget v0, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->x:F

    sub-float/2addr v0, v1

    iget v1, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->x:F

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    iget v1, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->y:F

    sub-float/2addr v1, v2

    iget v2, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v3, p1, Lcom/jme3/math/Vector3f;->y:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p2, Lcom/jme3/math/Vector3f;->z:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->z:F

    sub-float/2addr v1, v2

    iget v2, p2, Lcom/jme3/math/Vector3f;->z:F

    iget v3, p1, Lcom/jme3/math/Vector3f;->z:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    const/high16 v1, 0x40800000

    div-float/2addr v0, v1

    invoke-static {v0}, Lcom/jme3/math/FastMath;->sqrt(F)F

    move-result v0

    const v1, 0x3f800054

    add-float/2addr v0, v1

    const/high16 v1, 0x3f800000

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    iget-object v0, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    const/high16 v1, 0x3f000000

    invoke-virtual {v0, p1, p2, v1}, Lcom/jme3/math/Vector3f;->interpolate(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;F)Lcom/jme3/math/Vector3f;

    return-void
.end method

.method private setSphere(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V
    .locals 8
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;
    .param p3    # Lcom/jme3/math/Vector3f;

    const/4 v7, 0x0

    invoke-virtual {p2, p1}, Lcom/jme3/math/Vector3f;->subtract(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v1

    invoke-virtual {p3, p1}, Lcom/jme3/math/Vector3f;->subtract(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/jme3/math/Vector3f;->cross(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v2

    const/high16 v5, 0x40000000

    invoke-virtual {v2, v2}, Lcom/jme3/math/Vector3f;->dot(Lcom/jme3/math/Vector3f;)F

    move-result v6

    mul-float v0, v5, v6

    cmpl-float v5, v0, v7

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v5, v7, v7, v7}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    iput v7, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v2, v1}, Lcom/jme3/math/Vector3f;->cross(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v5

    invoke-virtual {v3}, Lcom/jme3/math/Vector3f;->lengthSquared()F

    move-result v6

    invoke-virtual {v5, v6}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v5

    invoke-virtual {v3, v2}, Lcom/jme3/math/Vector3f;->cross(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v6

    invoke-virtual {v1}, Lcom/jme3/math/Vector3f;->lengthSquared()F

    move-result v7

    invoke-virtual {v6, v7}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/jme3/math/Vector3f;->divideLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jme3/math/Vector3f;->length()F

    move-result v5

    const v6, 0x3f800054

    mul-float/2addr v5, v6

    iput v5, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    iget-object v5, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {p1, v4, v5}, Lcom/jme3/math/Vector3f;->add(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    goto :goto_0
.end method

.method private setSphere(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V
    .locals 11
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;
    .param p3    # Lcom/jme3/math/Vector3f;
    .param p4    # Lcom/jme3/math/Vector3f;

    invoke-virtual {p2, p1}, Lcom/jme3/math/Vector3f;->subtract(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v1

    invoke-virtual {p3, p1}, Lcom/jme3/math/Vector3f;->subtract(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v2

    invoke-virtual {p4, p1}, Lcom/jme3/math/Vector3f;->subtract(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v3

    const/high16 v5, 0x40000000

    iget v6, v1, Lcom/jme3/math/Vector3f;->x:F

    iget v7, v2, Lcom/jme3/math/Vector3f;->y:F

    iget v8, v3, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v7, v8

    iget v8, v3, Lcom/jme3/math/Vector3f;->y:F

    iget v9, v2, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    mul-float/2addr v6, v7

    iget v7, v2, Lcom/jme3/math/Vector3f;->x:F

    iget v8, v1, Lcom/jme3/math/Vector3f;->y:F

    iget v9, v3, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v8, v9

    iget v9, v3, Lcom/jme3/math/Vector3f;->y:F

    iget v10, v1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    iget v7, v3, Lcom/jme3/math/Vector3f;->x:F

    iget v8, v1, Lcom/jme3/math/Vector3f;->y:F

    iget v9, v2, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v8, v9

    iget v9, v2, Lcom/jme3/math/Vector3f;->y:F

    iget v10, v1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    mul-float v0, v5, v6

    const/4 v5, 0x0

    cmpl-float v5, v0, v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    const/4 v5, 0x0

    iput v5, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v1, v2}, Lcom/jme3/math/Vector3f;->cross(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v5

    invoke-virtual {v3}, Lcom/jme3/math/Vector3f;->lengthSquared()F

    move-result v6

    invoke-virtual {v5, v6}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v5

    invoke-virtual {v3, v1}, Lcom/jme3/math/Vector3f;->cross(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v6

    invoke-virtual {v2}, Lcom/jme3/math/Vector3f;->lengthSquared()F

    move-result v7

    invoke-virtual {v6, v7}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v5

    invoke-virtual {v2, v3}, Lcom/jme3/math/Vector3f;->cross(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v6

    invoke-virtual {v1}, Lcom/jme3/math/Vector3f;->lengthSquared()F

    move-result v7

    invoke-virtual {v6, v7}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/jme3/math/Vector3f;->divideLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jme3/math/Vector3f;->length()F

    move-result v5

    const v6, 0x3f800054

    mul-float/2addr v5, v6

    iput v5, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    iget-object v5, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {p1, v4, v5}, Lcom/jme3/math/Vector3f;->add(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    goto :goto_0
.end method


# virtual methods
.method public calcWelzl(Ljava/nio/FloatBuffer;)V
    .locals 3
    .param p1    # Ljava/nio/FloatBuffer;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    if-nez v1, :cond_0

    new-instance v1, Lcom/jme3/math/Vector3f;

    invoke-direct {v1}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v1, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    :cond_0
    invoke-virtual {p1}, Ljava/nio/FloatBuffer;->limit()I

    move-result v1

    invoke-static {v1}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {p1}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {v0, p1}, Ljava/nio/FloatBuffer;->put(Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->flip()Ljava/nio/Buffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->limit()I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    invoke-direct {p0, v0, v1, v2, v2}, Lcom/jme3/bounding/BoundingSphere;->recurseMini(Ljava/nio/FloatBuffer;III)V

    return-void
.end method

.method public clone(Lcom/jme3/bounding/BoundingVolume;)Lcom/jme3/bounding/BoundingVolume;
    .locals 3
    .param p1    # Lcom/jme3/bounding/BoundingVolume;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/jme3/bounding/BoundingVolume;->getType()Lcom/jme3/bounding/BoundingVolume$Type;

    move-result-object v1

    sget-object v2, Lcom/jme3/bounding/BoundingVolume$Type;->Sphere:Lcom/jme3/bounding/BoundingVolume$Type;

    if-ne v1, v2, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/jme3/bounding/BoundingSphere;

    iget-object v1, v0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    if-nez v1, :cond_0

    new-instance v1, Lcom/jme3/math/Vector3f;

    invoke-direct {v1}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v1, v0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    :cond_0
    iget-object v1, v0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    iget-object v2, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v1, v2}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    iget v1, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    iput v1, v0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    iget v1, p0, Lcom/jme3/bounding/BoundingSphere;->checkPlane:I

    iput v1, v0, Lcom/jme3/bounding/BoundingSphere;->checkPlane:I

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/jme3/bounding/BoundingSphere;

    iget v2, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    iget-object v1, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v1}, Lcom/jme3/math/Vector3f;->clone()Lcom/jme3/math/Vector3f;

    move-result-object v1

    :goto_1
    invoke-direct {v0, v2, v1}, Lcom/jme3/bounding/BoundingSphere;-><init>(FLcom/jme3/math/Vector3f;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public collideWith(Lcom/jme3/collision/Collidable;Lcom/jme3/collision/CollisionResults;)I
    .locals 9
    .param p1    # Lcom/jme3/collision/Collidable;
    .param p2    # Lcom/jme3/collision/CollisionResults;

    instance-of v7, p1, Lcom/jme3/math/Ray;

    if-eqz v7, :cond_0

    move-object v5, p1

    check-cast v5, Lcom/jme3/math/Ray;

    invoke-direct {p0, v5, p2}, Lcom/jme3/bounding/BoundingSphere;->collideWithRay(Lcom/jme3/math/Ray;Lcom/jme3/collision/CollisionResults;)I

    move-result v7

    :goto_0
    return v7

    :cond_0
    instance-of v7, p1, Lcom/jme3/math/Triangle;

    if-eqz v7, :cond_3

    move-object v6, p1

    check-cast v6, Lcom/jme3/math/Triangle;

    iget v7, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    iget v8, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    mul-float v4, v7, v8

    iget-object v7, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v6}, Lcom/jme3/math/Triangle;->get1()Lcom/jme3/math/Vector3f;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/jme3/math/Vector3f;->distanceSquared(Lcom/jme3/math/Vector3f;)F

    move-result v0

    iget-object v7, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v6}, Lcom/jme3/math/Triangle;->get2()Lcom/jme3/math/Vector3f;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/jme3/math/Vector3f;->distanceSquared(Lcom/jme3/math/Vector3f;)F

    move-result v1

    iget-object v7, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v6}, Lcom/jme3/math/Triangle;->get3()Lcom/jme3/math/Vector3f;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/jme3/math/Vector3f;->distanceSquared(Lcom/jme3/math/Vector3f;)F

    move-result v2

    cmpg-float v7, v0, v4

    if-lez v7, :cond_1

    cmpg-float v7, v1, v4

    if-lez v7, :cond_1

    cmpg-float v7, v2, v4

    if-gtz v7, :cond_2

    :cond_1
    new-instance v3, Lcom/jme3/collision/CollisionResult;

    invoke-direct {v3}, Lcom/jme3/collision/CollisionResult;-><init>()V

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v7

    invoke-static {v7, v2}, Ljava/lang/Math;->min(FF)F

    move-result v7

    invoke-static {v7}, Lcom/jme3/math/FastMath;->sqrt(F)F

    move-result v7

    iget v8, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    sub-float/2addr v7, v8

    invoke-virtual {v3, v7}, Lcom/jme3/collision/CollisionResult;->setDistance(F)V

    invoke-virtual {p2, v3}, Lcom/jme3/collision/CollisionResults;->addCollision(Lcom/jme3/collision/CollisionResult;)V

    const/4 v7, 0x1

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    goto :goto_0

    :cond_3
    new-instance v7, Lcom/jme3/collision/UnsupportedCollisionException;

    invoke-direct {v7}, Lcom/jme3/collision/UnsupportedCollisionException;-><init>()V

    throw v7
.end method

.method public computeFromPoints(Ljava/nio/FloatBuffer;)V
    .locals 0
    .param p1    # Ljava/nio/FloatBuffer;

    invoke-virtual {p0, p1}, Lcom/jme3/bounding/BoundingSphere;->calcWelzl(Ljava/nio/FloatBuffer;)V

    return-void
.end method

.method public distanceToEdge(Lcom/jme3/math/Vector3f;)F
    .locals 2
    .param p1    # Lcom/jme3/math/Vector3f;

    iget-object v0, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, p1}, Lcom/jme3/math/Vector3f;->distance(Lcom/jme3/math/Vector3f;)F

    move-result v0

    iget v1, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public getRadius()F
    .locals 1

    iget v0, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    return v0
.end method

.method public getType()Lcom/jme3/bounding/BoundingVolume$Type;
    .locals 1

    sget-object v0, Lcom/jme3/bounding/BoundingVolume$Type;->Sphere:Lcom/jme3/bounding/BoundingVolume$Type;

    return-object v0
.end method

.method public intersectsBoundingBox(Lcom/jme3/bounding/BoundingBox;)Z
    .locals 3
    .param p1    # Lcom/jme3/bounding/BoundingBox;

    sget-boolean v0, Lcom/jme3/bounding/BoundingSphere;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-static {v0}, Lcom/jme3/math/Vector3f;->isValidVector(Lcom/jme3/math/Vector3f;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-static {v0}, Lcom/jme3/math/Vector3f;->isValidVector(Lcom/jme3/math/Vector3f;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p1, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    iget-object v1, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->x:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    invoke-virtual {p0}, Lcom/jme3/bounding/BoundingSphere;->getRadius()F

    move-result v1

    iget v2, p1, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    add-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget-object v0, p1, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    iget-object v1, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->y:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    invoke-virtual {p0}, Lcom/jme3/bounding/BoundingSphere;->getRadius()F

    move-result v1

    iget v2, p1, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    add-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget-object v0, p1, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    iget-object v1, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    iget v1, v1, Lcom/jme3/math/Vector3f;->z:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v0

    invoke-virtual {p0}, Lcom/jme3/bounding/BoundingSphere;->getRadius()F

    move-result v1

    iget v2, p1, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    add-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public mergeLocal(Lcom/jme3/bounding/BoundingVolume;)Lcom/jme3/bounding/BoundingVolume;
    .locals 10
    .param p1    # Lcom/jme3/bounding/BoundingVolume;

    if-nez p1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    sget-object v7, Lcom/jme3/bounding/BoundingSphere$1;->$SwitchMap$com$jme3$bounding$BoundingVolume$Type:[I

    invoke-virtual {p1}, Lcom/jme3/bounding/BoundingVolume;->getType()Lcom/jme3/bounding/BoundingVolume$Type;

    move-result-object v8

    invoke-virtual {v8}, Lcom/jme3/bounding/BoundingVolume$Type;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    const/4 p0, 0x0

    goto :goto_0

    :pswitch_0
    move-object v3, p1

    check-cast v3, Lcom/jme3/bounding/BoundingSphere;

    invoke-virtual {v3}, Lcom/jme3/bounding/BoundingSphere;->getRadius()F

    move-result v5

    iget-object v4, v3, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-direct {p0, v5, v4, p0}, Lcom/jme3/bounding/BoundingSphere;->merge(FLcom/jme3/math/Vector3f;Lcom/jme3/bounding/BoundingSphere;)Lcom/jme3/bounding/BoundingVolume;

    move-result-object p0

    goto :goto_0

    :pswitch_1
    move-object v0, p1

    check-cast v0, Lcom/jme3/bounding/BoundingBox;

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v6

    iget-object v2, v6, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget v7, v0, Lcom/jme3/bounding/BoundingBox;->xExtent:F

    iget v8, v0, Lcom/jme3/bounding/BoundingBox;->yExtent:F

    iget v9, v0, Lcom/jme3/bounding/BoundingBox;->zExtent:F

    invoke-virtual {v2, v7, v8, v9}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    iget-object v4, v0, Lcom/jme3/bounding/BoundingBox;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v2}, Lcom/jme3/math/Vector3f;->length()F

    move-result v1

    invoke-virtual {v6}, Lcom/jme3/util/TempVars;->release()V

    invoke-direct {p0, v1, v4, p0}, Lcom/jme3/bounding/BoundingSphere;->merge(FLcom/jme3/math/Vector3f;Lcom/jme3/bounding/BoundingSphere;)Lcom/jme3/bounding/BoundingVolume;

    move-result-object p0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 6
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/jme3/bounding/BoundingVolume;->read(Lcom/jme3/export/JmeImporter;)V

    :try_start_0
    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v0

    const-string v1, "radius"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v5

    sget-object v0, Lcom/jme3/bounding/BoundingSphere;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "read(JMEImporter)"

    const-string v4, "Exception"

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public setRadius(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [Radius: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Center: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public transform(Lcom/jme3/math/Transform;Lcom/jme3/bounding/BoundingVolume;)Lcom/jme3/bounding/BoundingVolume;
    .locals 5
    .param p1    # Lcom/jme3/math/Transform;
    .param p2    # Lcom/jme3/bounding/BoundingVolume;

    const/high16 v4, 0x3f800000

    const/4 v3, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/jme3/bounding/BoundingVolume;->getType()Lcom/jme3/bounding/BoundingVolume$Type;

    move-result-object v1

    sget-object v2, Lcom/jme3/bounding/BoundingVolume$Type;->Sphere:Lcom/jme3/bounding/BoundingVolume$Type;

    if-eq v1, v2, :cond_1

    :cond_0
    new-instance v0, Lcom/jme3/bounding/BoundingSphere;

    new-instance v1, Lcom/jme3/math/Vector3f;

    invoke-direct {v1, v3, v3, v3}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    invoke-direct {v0, v4, v1}, Lcom/jme3/bounding/BoundingSphere;-><init>(FLcom/jme3/math/Vector3f;)V

    :goto_0
    iget-object v1, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {p1}, Lcom/jme3/math/Transform;->getScale()Lcom/jme3/math/Vector3f;

    move-result-object v2

    iget-object v3, v0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v1, v2, v3}, Lcom/jme3/math/Vector3f;->mult(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    invoke-virtual {p1}, Lcom/jme3/math/Transform;->getRotation()Lcom/jme3/math/Quaternion;

    move-result-object v1

    iget-object v2, v0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    iget-object v3, v0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v1, v2, v3}, Lcom/jme3/math/Quaternion;->mult(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    iget-object v1, v0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {p1}, Lcom/jme3/math/Transform;->getTranslation()Lcom/jme3/math/Vector3f;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    invoke-virtual {p1}, Lcom/jme3/math/Transform;->getScale()Lcom/jme3/math/Vector3f;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jme3/bounding/BoundingSphere;->getMaxAxis(Lcom/jme3/math/Vector3f;)F

    move-result v1

    iget v2, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    mul-float/2addr v1, v2

    invoke-static {v1}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v1

    const v2, 0x3f800054

    add-float/2addr v1, v2

    sub-float/2addr v1, v4

    iput v1, v0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    return-object v0

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/jme3/bounding/BoundingSphere;

    goto :goto_0
.end method

.method public whichSide(Lcom/jme3/math/Plane;)Lcom/jme3/math/Plane$Side;
    .locals 2
    .param p1    # Lcom/jme3/math/Plane;

    iget-object v1, p0, Lcom/jme3/bounding/BoundingSphere;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {p1, v1}, Lcom/jme3/math/Plane;->pseudoDistance(Lcom/jme3/math/Vector3f;)F

    move-result v0

    iget v1, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    neg-float v1, v1

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    sget-object v1, Lcom/jme3/math/Plane$Side;->Negative:Lcom/jme3/math/Plane$Side;

    :goto_0
    return-object v1

    :cond_0
    iget v1, p0, Lcom/jme3/bounding/BoundingSphere;->radius:F

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    sget-object v1, Lcom/jme3/math/Plane$Side;->Positive:Lcom/jme3/math/Plane$Side;

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/jme3/math/Plane$Side;->None:Lcom/jme3/math/Plane$Side;

    goto :goto_0
.end method
