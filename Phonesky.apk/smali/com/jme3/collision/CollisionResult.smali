.class public Lcom/jme3/collision/CollisionResult;
.super Ljava/lang/Object;
.source "CollisionResult.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/jme3/collision/CollisionResult;",
        ">;"
    }
.end annotation


# instance fields
.field private contactNormal:Lcom/jme3/math/Vector3f;

.field private contactPoint:Lcom/jme3/math/Vector3f;

.field private distance:F

.field private geometry:Lcom/jme3/scene/Geometry;

.field private triangleIndex:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/jme3/math/Vector3f;F)V
    .locals 0
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jme3/collision/CollisionResult;->contactPoint:Lcom/jme3/math/Vector3f;

    iput p2, p0, Lcom/jme3/collision/CollisionResult;->distance:F

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/jme3/collision/CollisionResult;)I
    .locals 2
    .param p1    # Lcom/jme3/collision/CollisionResult;

    iget v0, p0, Lcom/jme3/collision/CollisionResult;->distance:F

    iget v1, p1, Lcom/jme3/collision/CollisionResult;->distance:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/jme3/collision/CollisionResult;->distance:F

    iget v1, p1, Lcom/jme3/collision/CollisionResult;->distance:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/jme3/collision/CollisionResult;

    invoke-virtual {p0, p1}, Lcom/jme3/collision/CollisionResult;->compareTo(Lcom/jme3/collision/CollisionResult;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/jme3/collision/CollisionResult;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/jme3/collision/CollisionResult;

    invoke-virtual {p1, p0}, Lcom/jme3/collision/CollisionResult;->compareTo(Lcom/jme3/collision/CollisionResult;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getContactPoint()Lcom/jme3/math/Vector3f;
    .locals 1

    iget-object v0, p0, Lcom/jme3/collision/CollisionResult;->contactPoint:Lcom/jme3/math/Vector3f;

    return-object v0
.end method

.method public getDistance()F
    .locals 1

    iget v0, p0, Lcom/jme3/collision/CollisionResult;->distance:F

    return v0
.end method

.method public getGeometry()Lcom/jme3/scene/Geometry;
    .locals 1

    iget-object v0, p0, Lcom/jme3/collision/CollisionResult;->geometry:Lcom/jme3/scene/Geometry;

    return-object v0
.end method

.method public setContactNormal(Lcom/jme3/math/Vector3f;)V
    .locals 0
    .param p1    # Lcom/jme3/math/Vector3f;

    iput-object p1, p0, Lcom/jme3/collision/CollisionResult;->contactNormal:Lcom/jme3/math/Vector3f;

    return-void
.end method

.method public setDistance(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/jme3/collision/CollisionResult;->distance:F

    return-void
.end method

.method public setGeometry(Lcom/jme3/scene/Geometry;)V
    .locals 0
    .param p1    # Lcom/jme3/scene/Geometry;

    iput-object p1, p0, Lcom/jme3/collision/CollisionResult;->geometry:Lcom/jme3/scene/Geometry;

    return-void
.end method

.method public setTriangleIndex(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jme3/collision/CollisionResult;->triangleIndex:I

    return-void
.end method
