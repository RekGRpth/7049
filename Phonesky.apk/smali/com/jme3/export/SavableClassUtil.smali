.class public Lcom/jme3/export/SavableClassUtil;
.super Ljava/lang/Object;
.source "SavableClassUtil.java"


# static fields
.field private static final classRemappings:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/jme3/export/SavableClassUtil;->classRemappings:Ljava/util/HashMap;

    const-string v0, "com.jme3.effect.EmitterSphereShape"

    const-class v1, Lcom/jme3/effect/shapes/EmitterSphereShape;

    invoke-static {v0, v1}, Lcom/jme3/export/SavableClassUtil;->addRemapping(Ljava/lang/String;Ljava/lang/Class;)V

    const-string v0, "com.jme3.effect.EmitterBoxShape"

    const-class v1, Lcom/jme3/effect/shapes/EmitterBoxShape;

    invoke-static {v0, v1}, Lcom/jme3/export/SavableClassUtil;->addRemapping(Ljava/lang/String;Ljava/lang/Class;)V

    const-string v0, "com.jme3.effect.EmitterMeshConvexHullShape"

    const-class v1, Lcom/jme3/effect/shapes/EmitterMeshConvexHullShape;

    invoke-static {v0, v1}, Lcom/jme3/export/SavableClassUtil;->addRemapping(Ljava/lang/String;Ljava/lang/Class;)V

    const-string v0, "com.jme3.effect.EmitterMeshFaceShape"

    const-class v1, Lcom/jme3/effect/shapes/EmitterMeshFaceShape;

    invoke-static {v0, v1}, Lcom/jme3/export/SavableClassUtil;->addRemapping(Ljava/lang/String;Ljava/lang/Class;)V

    const-string v0, "com.jme3.effect.EmitterMeshVertexShape"

    const-class v1, Lcom/jme3/effect/shapes/EmitterMeshVertexShape;

    invoke-static {v0, v1}, Lcom/jme3/export/SavableClassUtil;->addRemapping(Ljava/lang/String;Ljava/lang/Class;)V

    const-string v0, "com.jme3.effect.EmitterPointShape"

    const-class v1, Lcom/jme3/effect/shapes/EmitterPointShape;

    invoke-static {v0, v1}, Lcom/jme3/export/SavableClassUtil;->addRemapping(Ljava/lang/String;Ljava/lang/Class;)V

    const-string v0, "com.jme3.material.Material$MatParamTexture"

    const-class v1, Lcom/jme3/material/MatParamTexture;

    invoke-static {v0, v1}, Lcom/jme3/export/SavableClassUtil;->addRemapping(Ljava/lang/String;Ljava/lang/Class;)V

    const-string v0, "com.jme3.animation.BoneAnimation"

    const-class v1, Lcom/jme3/animation/Animation;

    invoke-static {v0, v1}, Lcom/jme3/export/SavableClassUtil;->addRemapping(Ljava/lang/String;Ljava/lang/Class;)V

    const-string v0, "com.jme3.animation.SpatialAnimation"

    const-class v1, Lcom/jme3/animation/Animation;

    invoke-static {v0, v1}, Lcom/jme3/export/SavableClassUtil;->addRemapping(Ljava/lang/String;Ljava/lang/Class;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addRemapping(Ljava/lang/String;Ljava/lang/Class;)V
    .locals 2
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/jme3/export/Savable;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lcom/jme3/export/SavableClassUtil;->classRemappings:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static fromName(Ljava/lang/String;)Lcom/jme3/export/Savable;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/ClassNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jme3/export/SavableClassUtil;->remapClass(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/Savable;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    const-class v2, Lcom/jme3/export/SavableClassUtil;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v4, "Could not access constructor of class \'\'{0}\'\'! \nSome types need to have the BinaryImporter set up in a special way. Please doublecheck the setup."

    invoke-virtual {v2, v3, v4, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    throw v0

    :catch_1
    move-exception v0

    const-class v1, Lcom/jme3/export/SavableClassUtil;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v1

    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "{0} \nSome types need to have the BinaryImporter set up in a special way. Please doublecheck the setup."

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    throw v0
.end method

.method public static fromName(Ljava/lang/String;Ljava/util/List;)Lcom/jme3/export/Savable;
    .locals 4
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/ClassLoader;",
            ">;)",
            "Lcom/jme3/export/Savable;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InstantiationException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/ClassNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p1, :cond_0

    invoke-static {p0}, Lcom/jme3/export/SavableClassUtil;->fromName(Ljava/lang/String;)Lcom/jme3/export/Savable;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_0
    invoke-static {p0}, Lcom/jme3/export/SavableClassUtil;->remapClass(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    monitor-enter p1

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ClassLoader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0, v2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jme3/export/Savable;
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit p1

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    :cond_1
    :try_start_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {p0}, Lcom/jme3/export/SavableClassUtil;->fromName(Ljava/lang/String;)Lcom/jme3/export/Savable;

    move-result-object v3

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_1

    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method public static getSavedSavableVersion(Ljava/lang/Object;Ljava/lang/Class;[II)I
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p2    # [I
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/jme3/export/Savable;",
            ">;[II)I"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eq v1, p1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/jme3/export/SavableClassUtil;->isImplementingSavable(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-nez v1, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " does not extend "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    array-length v2, p2

    if-lt v0, v2, :cond_3

    const/4 v2, 0x1

    if-gt p3, v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    return v2

    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " cannot access version of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " because it doesn\'t implement Savable"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    aget v2, p2, v0

    goto :goto_1
.end method

.method public static isImplementingSavable(Ljava/lang/Class;)Z
    .locals 1

    const-class v0, Lcom/jme3/export/Savable;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method private static remapClass(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    sget-object v1, Lcom/jme3/export/SavableClassUtil;->classRemappings:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    move-object p0, v0

    goto :goto_0
.end method
