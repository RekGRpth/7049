.class public Lcom/jme3/font/plugins/BitmapFontLoader;
.super Ljava/lang/Object;
.source "BitmapFontLoader.java"

# interfaces
.implements Lcom/jme3/asset/AssetLoader;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private load(Lcom/jme3/asset/AssetManager;Ljava/lang/String;Ljava/io/InputStream;)Lcom/jme3/font/BitmapFont;
    .locals 22
    .param p1    # Lcom/jme3/asset/AssetManager;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v20, Lcom/jme3/asset/AssetKey;

    const-string v21, "Common/MatDefs/Misc/Unshaded.j3md"

    invoke-direct/range {v20 .. v21}, Lcom/jme3/asset/AssetKey;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/jme3/asset/AssetManager;->loadAsset(Lcom/jme3/asset/AssetKey;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/jme3/material/MaterialDef;

    new-instance v4, Lcom/jme3/font/BitmapCharacterSet;

    invoke-direct {v4}, Lcom/jme3/font/BitmapCharacterSet;-><init>()V

    const/4 v12, 0x0

    new-instance v6, Lcom/jme3/font/BitmapFont;

    invoke-direct {v6}, Lcom/jme3/font/BitmapFont;-><init>()V

    new-instance v13, Ljava/io/BufferedReader;

    new-instance v20, Ljava/io/InputStreamReader;

    move-object/from16 v0, v20

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v20

    invoke-direct {v13, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const-string v14, "[\\s=]+"

    invoke-virtual {v6, v4}, Lcom/jme3/font/BitmapFont;->setCharSet(Lcom/jme3/font/BitmapCharacterSet;)V

    :cond_0
    :goto_0
    invoke-virtual {v13}, Ljava/io/BufferedReader;->ready()Z

    move-result v20

    if-eqz v20, :cond_1c

    invoke-virtual {v13}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x0

    aget-object v20, v19, v20

    const-string v21, "info"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    const/4 v7, 0x1

    :goto_1
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v7, v0, :cond_0

    aget-object v20, v19, v7

    const-string v21, "size"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/jme3/font/BitmapCharacterSet;->setRenderedSize(I)V

    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_2
    const/16 v20, 0x0

    aget-object v20, v19, v20

    const-string v21, "common"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    const/4 v7, 0x1

    :goto_2
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v7, v0, :cond_0

    aget-object v18, v19, v7

    const-string v20, "lineHeight"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/jme3/font/BitmapCharacterSet;->setLineHeight(I)V

    :cond_3
    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_4
    const-string v20, "base"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/jme3/font/BitmapCharacterSet;->setBase(I)V

    goto :goto_3

    :cond_5
    const-string v20, "scaleW"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/jme3/font/BitmapCharacterSet;->setWidth(I)V

    goto :goto_3

    :cond_6
    const-string v20, "scaleH"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/jme3/font/BitmapCharacterSet;->setHeight(I)V

    goto :goto_3

    :cond_7
    const-string v20, "pages"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    new-array v12, v0, [Lcom/jme3/material/Material;

    invoke-virtual {v6, v12}, Lcom/jme3/font/BitmapFont;->setPages([Lcom/jme3/material/Material;)V

    goto :goto_3

    :cond_8
    const/16 v20, 0x0

    aget-object v20, v19, v20

    const-string v21, "page"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_d

    const/4 v8, -0x1

    const/16 v17, 0x0

    const/4 v7, 0x1

    :goto_4
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v7, v0, :cond_c

    aget-object v18, v19, v7

    const-string v20, "id"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    :cond_9
    :goto_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    :cond_a
    const-string v20, "file"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    add-int/lit8 v20, v7, 0x1

    aget-object v5, v19, v20

    const-string v20, "\""

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_b

    const/16 v20, 0x1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v21

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    :cond_b
    new-instance v9, Lcom/jme3/asset/TextureKey;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v9, v0, v1}, Lcom/jme3/asset/TextureKey;-><init>(Ljava/lang/String;Z)V

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/jme3/asset/TextureKey;->setGenerateMips(Z)V

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Lcom/jme3/asset/AssetManager;->loadTexture(Lcom/jme3/asset/TextureKey;)Lcom/jme3/texture/Texture;

    move-result-object v17

    sget-object v20, Lcom/jme3/texture/Texture$MagFilter;->Bilinear:Lcom/jme3/texture/Texture$MagFilter;

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/jme3/texture/Texture;->setMagFilter(Lcom/jme3/texture/Texture$MagFilter;)V

    sget-object v20, Lcom/jme3/texture/Texture$MinFilter;->BilinearNoMipMaps:Lcom/jme3/texture/Texture$MinFilter;

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/jme3/texture/Texture;->setMinFilter(Lcom/jme3/texture/Texture$MinFilter;)V

    goto :goto_5

    :cond_c
    if-ltz v8, :cond_0

    if-eqz v17, :cond_0

    new-instance v11, Lcom/jme3/material/Material;

    move-object/from16 v0, v16

    invoke-direct {v11, v0}, Lcom/jme3/material/Material;-><init>(Lcom/jme3/material/MaterialDef;)V

    const-string v20, "ColorMap"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v11, v0, v1}, Lcom/jme3/material/Material;->setTexture(Ljava/lang/String;Lcom/jme3/texture/Texture;)V

    const-string v20, "VertexColor"

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v11, v0, v1}, Lcom/jme3/material/Material;->setBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v11}, Lcom/jme3/material/Material;->getAdditionalRenderState()Lcom/jme3/material/RenderState;

    move-result-object v20

    sget-object v21, Lcom/jme3/material/RenderState$BlendMode;->Alpha:Lcom/jme3/material/RenderState$BlendMode;

    invoke-virtual/range {v20 .. v21}, Lcom/jme3/material/RenderState;->setBlendMode(Lcom/jme3/material/RenderState$BlendMode;)V

    aput-object v11, v12, v8

    goto/16 :goto_0

    :cond_d
    const/16 v20, 0x0

    aget-object v20, v19, v20

    const-string v21, "char"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_17

    const/4 v3, 0x0

    const/4 v7, 0x1

    :goto_6
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v7, v0, :cond_0

    aget-object v18, v19, v7

    const-string v20, "id"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_f

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    new-instance v3, Lcom/jme3/font/BitmapCharacter;

    invoke-direct {v3}, Lcom/jme3/font/BitmapCharacter;-><init>()V

    invoke-virtual {v4, v8, v3}, Lcom/jme3/font/BitmapCharacterSet;->addCharacter(ILcom/jme3/font/BitmapCharacter;)V

    :cond_e
    :goto_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    :cond_f
    const-string v20, "x"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_10

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/jme3/font/BitmapCharacter;->setX(I)V

    goto :goto_7

    :cond_10
    const-string v20, "y"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_11

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/jme3/font/BitmapCharacter;->setY(I)V

    goto :goto_7

    :cond_11
    const-string v20, "width"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_12

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/jme3/font/BitmapCharacter;->setWidth(I)V

    goto :goto_7

    :cond_12
    const-string v20, "height"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_13

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/jme3/font/BitmapCharacter;->setHeight(I)V

    goto :goto_7

    :cond_13
    const-string v20, "xoffset"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_14

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/jme3/font/BitmapCharacter;->setXOffset(I)V

    goto/16 :goto_7

    :cond_14
    const-string v20, "yoffset"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_15

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/jme3/font/BitmapCharacter;->setYOffset(I)V

    goto/16 :goto_7

    :cond_15
    const-string v20, "xadvance"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_16

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/jme3/font/BitmapCharacter;->setXAdvance(I)V

    goto/16 :goto_7

    :cond_16
    const-string v20, "page"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_e

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/jme3/font/BitmapCharacter;->setPage(I)V

    goto/16 :goto_7

    :cond_17
    const/16 v20, 0x0

    aget-object v20, v19, v20

    const-string v21, "kerning"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_0

    const/4 v8, 0x0

    const/4 v15, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    :goto_8
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v7, v0, :cond_1b

    aget-object v20, v19, v7

    const-string v21, "first"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_19

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    :cond_18
    :goto_9
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    :cond_19
    aget-object v20, v19, v7

    const-string v21, "second"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1a

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    goto :goto_9

    :cond_1a
    aget-object v20, v19, v7

    const-string v21, "amount"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_18

    add-int/lit8 v20, v7, 0x1

    aget-object v20, v19, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_9

    :cond_1b
    invoke-virtual {v4, v8}, Lcom/jme3/font/BitmapCharacterSet;->getCharacter(I)Lcom/jme3/font/BitmapCharacter;

    move-result-object v3

    invoke-virtual {v3, v15, v2}, Lcom/jme3/font/BitmapCharacter;->addKerning(II)V

    goto/16 :goto_0

    :cond_1c
    return-object v6
.end method


# virtual methods
.method public load(Lcom/jme3/asset/AssetInfo;)Ljava/lang/Object;
    .locals 4
    .param p1    # Lcom/jme3/asset/AssetInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->openStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getManager()Lcom/jme3/asset/AssetManager;

    move-result-object v2

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getKey()Lcom/jme3/asset/AssetKey;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jme3/asset/AssetKey;->getFolder()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3, v1}, Lcom/jme3/font/plugins/BitmapFontLoader;->load(Lcom/jme3/asset/AssetManager;Ljava/lang/String;Ljava/io/InputStream;)Lcom/jme3/font/BitmapFont;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_0
    return-object v0

    :catchall_0
    move-exception v2

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v2
.end method
