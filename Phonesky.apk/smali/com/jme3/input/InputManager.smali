.class public Lcom/jme3/input/InputManager;
.super Ljava/lang/Object;
.source "InputManager.java"

# interfaces
.implements Lcom/jme3/input/RawInputListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/input/InputManager$Mapping;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private axisDeadZone:F

.field private final axisValues:Lcom/jme3/util/IntMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/IntMap",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final bindings:Lcom/jme3/util/IntMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/IntMap",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jme3/input/InputManager$Mapping;",
            ">;>;"
        }
    .end annotation
.end field

.field private cursorPos:Lcom/jme3/math/Vector2f;

.field private eventsPermitted:Z

.field private firstTime:J

.field private frameDelta:J

.field private frameTPF:F

.field private inputQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jme3/input/event/InputEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final joystick:Lcom/jme3/input/JoyInput;

.field private joysticks:[Lcom/jme3/input/Joystick;

.field private final keys:Lcom/jme3/input/KeyInput;

.field private lastLastUpdateTime:J

.field private lastUpdateTime:J

.field private final mappings:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/jme3/input/InputManager$Mapping;",
            ">;"
        }
    .end annotation
.end field

.field private final mouse:Lcom/jme3/input/MouseInput;

.field private mouseVisible:Z

.field private final pressedButtons:Lcom/jme3/util/IntMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/IntMap",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private rawListenerArray:[Lcom/jme3/input/RawInputListener;

.field private rawListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jme3/input/RawInputListener;",
            ">;"
        }
    .end annotation
.end field

.field private safeMode:Z

.field private final touch:Lcom/jme3/input/TouchInput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/input/InputManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/jme3/input/InputManager;->$assertionsDisabled:Z

    const-class v0, Lcom/jme3/input/InputManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/input/InputManager;->logger:Ljava/util/logging/Logger;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/jme3/input/MouseInput;Lcom/jme3/input/KeyInput;Lcom/jme3/input/JoyInput;Lcom/jme3/input/TouchInput;)V
    .locals 3
    .param p1    # Lcom/jme3/input/MouseInput;
    .param p2    # Lcom/jme3/input/KeyInput;
    .param p3    # Lcom/jme3/input/JoyInput;
    .param p4    # Lcom/jme3/input/TouchInput;

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/jme3/input/InputManager;->lastLastUpdateTime:J

    iput-wide v0, p0, Lcom/jme3/input/InputManager;->lastUpdateTime:J

    iput-wide v0, p0, Lcom/jme3/input/InputManager;->frameDelta:J

    iput-wide v0, p0, Lcom/jme3/input/InputManager;->firstTime:J

    iput-boolean v2, p0, Lcom/jme3/input/InputManager;->eventsPermitted:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/input/InputManager;->mouseVisible:Z

    iput-boolean v2, p0, Lcom/jme3/input/InputManager;->safeMode:Z

    const v0, 0x3d4ccccd

    iput v0, p0, Lcom/jme3/input/InputManager;->axisDeadZone:F

    new-instance v0, Lcom/jme3/math/Vector2f;

    invoke-direct {v0}, Lcom/jme3/math/Vector2f;-><init>()V

    iput-object v0, p0, Lcom/jme3/input/InputManager;->cursorPos:Lcom/jme3/math/Vector2f;

    new-instance v0, Lcom/jme3/util/IntMap;

    invoke-direct {v0}, Lcom/jme3/util/IntMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/input/InputManager;->bindings:Lcom/jme3/util/IntMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/input/InputManager;->mappings:Ljava/util/HashMap;

    new-instance v0, Lcom/jme3/util/IntMap;

    invoke-direct {v0}, Lcom/jme3/util/IntMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/input/InputManager;->pressedButtons:Lcom/jme3/util/IntMap;

    new-instance v0, Lcom/jme3/util/IntMap;

    invoke-direct {v0}, Lcom/jme3/util/IntMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/input/InputManager;->axisValues:Lcom/jme3/util/IntMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jme3/input/InputManager;->rawListeners:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jme3/input/InputManager;->rawListenerArray:[Lcom/jme3/input/RawInputListener;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jme3/input/InputManager;->inputQueue:Ljava/util/ArrayList;

    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Mouse or keyboard cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p2, p0, Lcom/jme3/input/InputManager;->keys:Lcom/jme3/input/KeyInput;

    iput-object p1, p0, Lcom/jme3/input/InputManager;->mouse:Lcom/jme3/input/MouseInput;

    iput-object p3, p0, Lcom/jme3/input/InputManager;->joystick:Lcom/jme3/input/JoyInput;

    iput-object p4, p0, Lcom/jme3/input/InputManager;->touch:Lcom/jme3/input/TouchInput;

    invoke-interface {p2, p0}, Lcom/jme3/input/KeyInput;->setInputListener(Lcom/jme3/input/RawInputListener;)V

    invoke-interface {p1, p0}, Lcom/jme3/input/MouseInput;->setInputListener(Lcom/jme3/input/RawInputListener;)V

    if-eqz p3, :cond_2

    invoke-interface {p3, p0}, Lcom/jme3/input/JoyInput;->setInputListener(Lcom/jme3/input/RawInputListener;)V

    invoke-interface {p3, p0}, Lcom/jme3/input/JoyInput;->loadJoysticks(Lcom/jme3/input/InputManager;)[Lcom/jme3/input/Joystick;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/input/InputManager;->joysticks:[Lcom/jme3/input/Joystick;

    :cond_2
    if-eqz p4, :cond_3

    invoke-interface {p4, p0}, Lcom/jme3/input/TouchInput;->setInputListener(Lcom/jme3/input/RawInputListener;)V

    :cond_3
    invoke-interface {p2}, Lcom/jme3/input/KeyInput;->getInputTimeNanos()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/jme3/input/InputManager;->firstTime:J

    return-void
.end method

.method private computeAnalogValue(J)F
    .locals 5
    .param p1    # J

    const/high16 v0, 0x3f800000

    iget-boolean v1, p0, Lcom/jme3/input/InputManager;->safeMode:Z

    if-nez v1, :cond_0

    iget-wide v1, p0, Lcom/jme3/input/InputManager;->frameDelta:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    long-to-float v1, p1

    iget-wide v2, p0, Lcom/jme3/input/InputManager;->frameDelta:J

    long-to-float v2, v2

    div-float/2addr v1, v2

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lcom/jme3/math/FastMath;->clamp(FFF)F

    move-result v0

    goto :goto_0
.end method

.method private getRawListenerArray()[Lcom/jme3/input/RawInputListener;
    .locals 2

    iget-object v0, p0, Lcom/jme3/input/InputManager;->rawListenerArray:[Lcom/jme3/input/RawInputListener;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jme3/input/InputManager;->rawListeners:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/jme3/input/InputManager;->rawListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/jme3/input/RawInputListener;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jme3/input/RawInputListener;

    iput-object v0, p0, Lcom/jme3/input/InputManager;->rawListenerArray:[Lcom/jme3/input/RawInputListener;

    :cond_0
    iget-object v0, p0, Lcom/jme3/input/InputManager;->rawListenerArray:[Lcom/jme3/input/RawInputListener;

    return-object v0
.end method

.method private invokeActions(IZ)V
    .locals 10
    .param p1    # I
    .param p2    # Z

    iget-object v8, p0, Lcom/jme3/input/InputManager;->bindings:Lcom/jme3/util/IntMap;

    invoke-virtual {v8, p1}, Lcom/jme3/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    if-nez v6, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v0, v7, -0x1

    :goto_0
    if-ltz v0, :cond_0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jme3/input/InputManager$Mapping;

    # getter for: Lcom/jme3/input/InputManager$Mapping;->listeners:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/jme3/input/InputManager$Mapping;->access$000(Lcom/jme3/input/InputManager$Mapping;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    :goto_1
    if-ltz v1, :cond_3

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/input/controls/InputListener;

    instance-of v8, v2, Lcom/jme3/input/controls/ActionListener;

    if-eqz v8, :cond_2

    check-cast v2, Lcom/jme3/input/controls/ActionListener;

    # getter for: Lcom/jme3/input/InputManager$Mapping;->name:Ljava/lang/String;
    invoke-static {v5}, Lcom/jme3/input/InputManager$Mapping;->access$100(Lcom/jme3/input/InputManager$Mapping;)Ljava/lang/String;

    move-result-object v8

    iget v9, p0, Lcom/jme3/input/InputManager;->frameTPF:F

    invoke-interface {v2, v8, p2, v9}, Lcom/jme3/input/controls/ActionListener;->onAction(Ljava/lang/String;ZF)V

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private invokeAnalogs(IFZ)V
    .locals 10
    .param p1    # I
    .param p2    # F
    .param p3    # Z

    iget-object v8, p0, Lcom/jme3/input/InputManager;->bindings:Lcom/jme3/util/IntMap;

    invoke-virtual {v8, p1}, Lcom/jme3/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    if-nez v6, :cond_1

    :cond_0
    return-void

    :cond_1
    if-nez p3, :cond_2

    iget v8, p0, Lcom/jme3/input/InputManager;->frameTPF:F

    mul-float/2addr p2, v8

    :cond_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v0, v7, -0x1

    :goto_0
    if-ltz v0, :cond_0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jme3/input/InputManager$Mapping;

    # getter for: Lcom/jme3/input/InputManager$Mapping;->listeners:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/jme3/input/InputManager$Mapping;->access$000(Lcom/jme3/input/InputManager$Mapping;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    :goto_1
    if-ltz v1, :cond_4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/input/controls/InputListener;

    instance-of v8, v2, Lcom/jme3/input/controls/AnalogListener;

    if-eqz v8, :cond_3

    check-cast v2, Lcom/jme3/input/controls/AnalogListener;

    # getter for: Lcom/jme3/input/InputManager$Mapping;->name:Ljava/lang/String;
    invoke-static {v5}, Lcom/jme3/input/InputManager$Mapping;->access$100(Lcom/jme3/input/InputManager$Mapping;)Ljava/lang/String;

    move-result-object v8

    iget v9, p0, Lcom/jme3/input/InputManager;->frameTPF:F

    invoke-interface {v2, v8, p2, v9}, Lcom/jme3/input/controls/AnalogListener;->onAnalog(Ljava/lang/String;FF)V

    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private invokeAnalogsAndActions(IFZ)V
    .locals 13
    .param p1    # I
    .param p2    # F
    .param p3    # Z

    iget v9, p0, Lcom/jme3/input/InputManager;->axisDeadZone:F

    cmpg-float v9, p2, v9

    if-gez v9, :cond_2

    if-nez p3, :cond_1

    const/4 v9, 0x1

    :goto_0
    invoke-direct {p0, p1, p2, v9}, Lcom/jme3/input/InputManager;->invokeAnalogs(IFZ)V

    :cond_0
    return-void

    :cond_1
    const/4 v9, 0x0

    goto :goto_0

    :cond_2
    iget-object v9, p0, Lcom/jme3/input/InputManager;->bindings:Lcom/jme3/util/IntMap;

    invoke-virtual {v9, p1}, Lcom/jme3/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    iget-object v9, p0, Lcom/jme3/input/InputManager;->axisValues:Lcom/jme3/util/IntMap;

    invoke-virtual {v9, p1}, Lcom/jme3/util/IntMap;->containsKey(I)Z

    move-result v9

    if-nez v9, :cond_6

    const/4 v8, 0x1

    :goto_1
    if-eqz p3, :cond_3

    iget v9, p0, Lcom/jme3/input/InputManager;->frameTPF:F

    mul-float/2addr p2, v9

    :cond_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v0, v7, -0x1

    :goto_2
    if-ltz v0, :cond_0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jme3/input/InputManager$Mapping;

    # getter for: Lcom/jme3/input/InputManager$Mapping;->listeners:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/jme3/input/InputManager$Mapping;->access$000(Lcom/jme3/input/InputManager$Mapping;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    :goto_3
    if-ltz v1, :cond_7

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/input/controls/InputListener;

    instance-of v9, v2, Lcom/jme3/input/controls/ActionListener;

    if-eqz v9, :cond_4

    if-eqz v8, :cond_4

    move-object v9, v2

    check-cast v9, Lcom/jme3/input/controls/ActionListener;

    # getter for: Lcom/jme3/input/InputManager$Mapping;->name:Ljava/lang/String;
    invoke-static {v5}, Lcom/jme3/input/InputManager$Mapping;->access$100(Lcom/jme3/input/InputManager$Mapping;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    iget v12, p0, Lcom/jme3/input/InputManager;->frameTPF:F

    invoke-interface {v9, v10, v11, v12}, Lcom/jme3/input/controls/ActionListener;->onAction(Ljava/lang/String;ZF)V

    :cond_4
    instance-of v9, v2, Lcom/jme3/input/controls/AnalogListener;

    if-eqz v9, :cond_5

    check-cast v2, Lcom/jme3/input/controls/AnalogListener;

    # getter for: Lcom/jme3/input/InputManager$Mapping;->name:Ljava/lang/String;
    invoke-static {v5}, Lcom/jme3/input/InputManager$Mapping;->access$100(Lcom/jme3/input/InputManager$Mapping;)Ljava/lang/String;

    move-result-object v9

    iget v10, p0, Lcom/jme3/input/InputManager;->frameTPF:F

    invoke-interface {v2, v9, p2, v10}, Lcom/jme3/input/controls/AnalogListener;->onAnalog(Ljava/lang/String;FF)V

    :cond_5
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    :cond_6
    const/4 v8, 0x0

    goto :goto_1

    :cond_7
    add-int/lit8 v0, v0, -0x1

    goto :goto_2
.end method

.method private invokeTimedActions(IJZ)V
    .locals 11
    .param p1    # I
    .param p2    # J
    .param p4    # Z

    iget-object v9, p0, Lcom/jme3/input/InputManager;->bindings:Lcom/jme3/util/IntMap;

    invoke-virtual {v9, p1}, Lcom/jme3/util/IntMap;->containsKey(I)Z

    move-result v9

    if-nez v9, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p4, :cond_2

    iget-object v9, p0, Lcom/jme3/input/InputManager;->pressedButtons:Lcom/jme3/util/IntMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, p1, v10}, Lcom/jme3/util/IntMap;->put(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget-object v9, p0, Lcom/jme3/input/InputManager;->pressedButtons:Lcom/jme3/util/IntMap;

    invoke-virtual {v9, p1}, Lcom/jme3/util/IntMap;->remove(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-wide v0, p0, Lcom/jme3/input/InputManager;->lastLastUpdateTime:J

    move-wide v5, p2

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v9

    sub-long v7, v5, v9

    const-wide/16 v9, 0x0

    cmp-long v9, v7, v9

    if-lez v9, :cond_0

    invoke-direct {p0, v7, v8}, Lcom/jme3/input/InputManager;->computeAnalogValue(J)F

    move-result v9

    const/4 v10, 0x0

    invoke-direct {p0, p1, v9, v10}, Lcom/jme3/input/InputManager;->invokeAnalogs(IFZ)V

    goto :goto_0
.end method

.method private invokeUpdateActions()V
    .locals 13

    iget-object v9, p0, Lcom/jme3/input/InputManager;->pressedButtons:Lcom/jme3/util/IntMap;

    invoke-virtual {v9}, Lcom/jme3/util/IntMap;->size()I

    move-result v9

    if-lez v9, :cond_1

    iget-object v9, p0, Lcom/jme3/input/InputManager;->pressedButtons:Lcom/jme3/util/IntMap;

    invoke-virtual {v9}, Lcom/jme3/util/IntMap;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jme3/util/IntMap$Entry;

    invoke-virtual {v5}, Lcom/jme3/util/IntMap$Entry;->getKey()I

    move-result v1

    invoke-virtual {v5}, Lcom/jme3/util/IntMap$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-wide v9, p0, Lcom/jme3/input/InputManager;->lastUpdateTime:J

    iget-wide v11, p0, Lcom/jme3/input/InputManager;->lastLastUpdateTime:J

    invoke-static {v11, v12, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v11

    sub-long v6, v9, v11

    const-wide/16 v9, 0x0

    cmp-long v9, v6, v9

    if-lez v9, :cond_0

    invoke-direct {p0, v6, v7}, Lcom/jme3/input/InputManager;->computeAnalogValue(J)F

    move-result v9

    const/4 v10, 0x0

    invoke-direct {p0, v1, v9, v10}, Lcom/jme3/input/InputManager;->invokeAnalogs(IFZ)V

    goto :goto_0

    :cond_1
    iget-object v9, p0, Lcom/jme3/input/InputManager;->axisValues:Lcom/jme3/util/IntMap;

    invoke-virtual {v9}, Lcom/jme3/util/IntMap;->size()I

    move-result v9

    if-lez v9, :cond_2

    iget-object v9, p0, Lcom/jme3/input/InputManager;->axisValues:Lcom/jme3/util/IntMap;

    invoke-virtual {v9}, Lcom/jme3/util/IntMap;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/util/IntMap$Entry;

    invoke-virtual {v0}, Lcom/jme3/util/IntMap$Entry;->getKey()I

    move-result v1

    invoke-virtual {v0}, Lcom/jme3/util/IntMap$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v8

    iget v9, p0, Lcom/jme3/input/InputManager;->frameTPF:F

    mul-float/2addr v9, v8

    const/4 v10, 0x1

    invoke-direct {p0, v1, v9, v10}, Lcom/jme3/input/InputManager;->invokeAnalogs(IFZ)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private onJoyAxisEventQueued(Lcom/jme3/input/event/JoyAxisEvent;)V
    .locals 12
    .param p1    # Lcom/jme3/input/event/JoyAxisEvent;

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {p1}, Lcom/jme3/input/event/JoyAxisEvent;->getJoyIndex()I

    move-result v4

    invoke-virtual {p1}, Lcom/jme3/input/event/JoyAxisEvent;->getAxisIndex()I

    move-result v0

    invoke-virtual {p1}, Lcom/jme3/input/event/JoyAxisEvent;->getValue()F

    move-result v8

    iget v9, p0, Lcom/jme3/input/InputManager;->axisDeadZone:F

    cmpg-float v9, v8, v9

    if-gez v9, :cond_2

    iget v9, p0, Lcom/jme3/input/InputManager;->axisDeadZone:F

    neg-float v9, v9

    cmpl-float v9, v8, v9

    if-lez v9, :cond_2

    invoke-static {v4, v0, v10}, Lcom/jme3/input/controls/JoyAxisTrigger;->joyAxisHash(IIZ)I

    move-result v2

    invoke-static {v4, v0, v11}, Lcom/jme3/input/controls/JoyAxisTrigger;->joyAxisHash(IIZ)I

    move-result v3

    iget-object v9, p0, Lcom/jme3/input/InputManager;->axisValues:Lcom/jme3/util/IntMap;

    invoke-virtual {v9, v2}, Lcom/jme3/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    iget-object v9, p0, Lcom/jme3/input/InputManager;->axisValues:Lcom/jme3/util/IntMap;

    invoke-virtual {v9, v3}, Lcom/jme3/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v9

    iget v10, p0, Lcom/jme3/input/InputManager;->axisDeadZone:F

    cmpl-float v9, v9, v10

    if-lez v9, :cond_0

    invoke-direct {p0, v2, v11}, Lcom/jme3/input/InputManager;->invokeActions(IZ)V

    :cond_0
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v9

    iget v10, p0, Lcom/jme3/input/InputManager;->axisDeadZone:F

    cmpl-float v9, v9, v10

    if-lez v9, :cond_1

    invoke-direct {p0, v3, v11}, Lcom/jme3/input/InputManager;->invokeActions(IZ)V

    :cond_1
    iget-object v9, p0, Lcom/jme3/input/InputManager;->axisValues:Lcom/jme3/util/IntMap;

    invoke-virtual {v9, v2}, Lcom/jme3/util/IntMap;->remove(I)Ljava/lang/Object;

    iget-object v9, p0, Lcom/jme3/input/InputManager;->axisValues:Lcom/jme3/util/IntMap;

    invoke-virtual {v9, v3}, Lcom/jme3/util/IntMap;->remove(I)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_2
    const/4 v9, 0x0

    cmpg-float v9, v8, v9

    if-gez v9, :cond_3

    invoke-static {v4, v0, v10}, Lcom/jme3/input/controls/JoyAxisTrigger;->joyAxisHash(IIZ)I

    move-result v1

    invoke-static {v4, v0, v11}, Lcom/jme3/input/controls/JoyAxisTrigger;->joyAxisHash(IIZ)I

    move-result v5

    neg-float v9, v8

    invoke-direct {p0, v1, v9, v10}, Lcom/jme3/input/InputManager;->invokeAnalogsAndActions(IFZ)V

    iget-object v9, p0, Lcom/jme3/input/InputManager;->axisValues:Lcom/jme3/util/IntMap;

    neg-float v10, v8

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v9, v1, v10}, Lcom/jme3/util/IntMap;->put(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v9, p0, Lcom/jme3/input/InputManager;->axisValues:Lcom/jme3/util/IntMap;

    invoke-virtual {v9, v5}, Lcom/jme3/util/IntMap;->remove(I)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    invoke-static {v4, v0, v11}, Lcom/jme3/input/controls/JoyAxisTrigger;->joyAxisHash(IIZ)I

    move-result v1

    invoke-static {v4, v0, v10}, Lcom/jme3/input/controls/JoyAxisTrigger;->joyAxisHash(IIZ)I

    move-result v5

    invoke-direct {p0, v1, v8, v10}, Lcom/jme3/input/InputManager;->invokeAnalogsAndActions(IFZ)V

    iget-object v9, p0, Lcom/jme3/input/InputManager;->axisValues:Lcom/jme3/util/IntMap;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v9, v1, v10}, Lcom/jme3/util/IntMap;->put(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v9, p0, Lcom/jme3/input/InputManager;->axisValues:Lcom/jme3/util/IntMap;

    invoke-virtual {v9, v5}, Lcom/jme3/util/IntMap;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method private onJoyButtonEventQueued(Lcom/jme3/input/event/JoyButtonEvent;)V
    .locals 4
    .param p1    # Lcom/jme3/input/event/JoyButtonEvent;

    invoke-virtual {p1}, Lcom/jme3/input/event/JoyButtonEvent;->getJoyIndex()I

    move-result v1

    invoke-virtual {p1}, Lcom/jme3/input/event/JoyButtonEvent;->getButtonIndex()I

    move-result v2

    invoke-static {v1, v2}, Lcom/jme3/input/controls/JoyButtonTrigger;->joyButtonHash(II)I

    move-result v0

    invoke-virtual {p1}, Lcom/jme3/input/event/JoyButtonEvent;->isPressed()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/jme3/input/InputManager;->invokeActions(IZ)V

    invoke-virtual {p1}, Lcom/jme3/input/event/JoyButtonEvent;->getTime()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/jme3/input/event/JoyButtonEvent;->isPressed()Z

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/jme3/input/InputManager;->invokeTimedActions(IJZ)V

    return-void
.end method

.method private onKeyEventQueued(Lcom/jme3/input/event/KeyInputEvent;)V
    .locals 4
    .param p1    # Lcom/jme3/input/event/KeyInputEvent;

    invoke-virtual {p1}, Lcom/jme3/input/event/KeyInputEvent;->isRepeating()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/jme3/input/event/KeyInputEvent;->getKeyCode()I

    move-result v1

    invoke-static {v1}, Lcom/jme3/input/controls/KeyTrigger;->keyHash(I)I

    move-result v0

    invoke-virtual {p1}, Lcom/jme3/input/event/KeyInputEvent;->isPressed()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/jme3/input/InputManager;->invokeActions(IZ)V

    invoke-virtual {p1}, Lcom/jme3/input/event/KeyInputEvent;->getTime()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/jme3/input/event/KeyInputEvent;->isPressed()Z

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/jme3/input/InputManager;->invokeTimedActions(IJZ)V

    goto :goto_0
.end method

.method private onMouseButtonEventQueued(Lcom/jme3/input/event/MouseButtonEvent;)V
    .locals 4
    .param p1    # Lcom/jme3/input/event/MouseButtonEvent;

    invoke-virtual {p1}, Lcom/jme3/input/event/MouseButtonEvent;->getButtonIndex()I

    move-result v1

    invoke-static {v1}, Lcom/jme3/input/controls/MouseButtonTrigger;->mouseButtonHash(I)I

    move-result v0

    invoke-virtual {p1}, Lcom/jme3/input/event/MouseButtonEvent;->isPressed()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/jme3/input/InputManager;->invokeActions(IZ)V

    invoke-virtual {p1}, Lcom/jme3/input/event/MouseButtonEvent;->getTime()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/jme3/input/event/MouseButtonEvent;->isPressed()Z

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/jme3/input/InputManager;->invokeTimedActions(IJZ)V

    return-void
.end method

.method private onMouseMotionEventQueued(Lcom/jme3/input/event/MouseMotionEvent;)V
    .locals 5
    .param p1    # Lcom/jme3/input/event/MouseMotionEvent;

    const/high16 v4, 0x44800000

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/jme3/input/event/MouseMotionEvent;->getDX()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/jme3/input/event/MouseMotionEvent;->getDX()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    div-float v0, v1, v4

    invoke-virtual {p1}, Lcom/jme3/input/event/MouseMotionEvent;->getDX()I

    move-result v1

    if-gez v1, :cond_3

    move v1, v2

    :goto_0
    invoke-static {v3, v1}, Lcom/jme3/input/controls/MouseAxisTrigger;->mouseAxisHash(IZ)I

    move-result v1

    invoke-direct {p0, v1, v0, v3}, Lcom/jme3/input/InputManager;->invokeAnalogsAndActions(IFZ)V

    :cond_0
    invoke-virtual {p1}, Lcom/jme3/input/event/MouseMotionEvent;->getDY()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/jme3/input/event/MouseMotionEvent;->getDY()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    div-float v0, v1, v4

    invoke-virtual {p1}, Lcom/jme3/input/event/MouseMotionEvent;->getDY()I

    move-result v1

    if-gez v1, :cond_4

    move v1, v2

    :goto_1
    invoke-static {v2, v1}, Lcom/jme3/input/controls/MouseAxisTrigger;->mouseAxisHash(IZ)I

    move-result v1

    invoke-direct {p0, v1, v0, v3}, Lcom/jme3/input/InputManager;->invokeAnalogsAndActions(IFZ)V

    :cond_1
    invoke-virtual {p1}, Lcom/jme3/input/event/MouseMotionEvent;->getDeltaWheel()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/jme3/input/event/MouseMotionEvent;->getDeltaWheel()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v4, 0x42c80000

    div-float v0, v1, v4

    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/jme3/input/event/MouseMotionEvent;->getDeltaWheel()I

    move-result v4

    if-gez v4, :cond_5

    :goto_2
    invoke-static {v1, v2}, Lcom/jme3/input/controls/MouseAxisTrigger;->mouseAxisHash(IZ)I

    move-result v1

    invoke-direct {p0, v1, v0, v3}, Lcom/jme3/input/InputManager;->invokeAnalogsAndActions(IFZ)V

    :cond_2
    return-void

    :cond_3
    move v1, v3

    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_1

    :cond_5
    move v2, v3

    goto :goto_2
.end method

.method private processQueue()V
    .locals 10

    iget-object v9, p0, Lcom/jme3/input/InputManager;->inputQueue:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-direct {p0}, Lcom/jme3/input/InputManager;->getRawListenerArray()[Lcom/jme3/input/RawInputListener;

    move-result-object v1

    move-object v0, v1

    array-length v6, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v6, :cond_9

    aget-object v7, v0, v4

    invoke-interface {v7}, Lcom/jme3/input/RawInputListener;->beginInput()V

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v8, :cond_8

    iget-object v9, p0, Lcom/jme3/input/InputManager;->inputQueue:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/input/event/InputEvent;

    invoke-virtual {v2}, Lcom/jme3/input/event/InputEvent;->isConsumed()Z

    move-result v9

    if-eqz v9, :cond_1

    :cond_0
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_1
    instance-of v9, v2, Lcom/jme3/input/event/MouseMotionEvent;

    if-eqz v9, :cond_2

    check-cast v2, Lcom/jme3/input/event/MouseMotionEvent;

    invoke-interface {v7, v2}, Lcom/jme3/input/RawInputListener;->onMouseMotionEvent(Lcom/jme3/input/event/MouseMotionEvent;)V

    goto :goto_2

    :cond_2
    instance-of v9, v2, Lcom/jme3/input/event/KeyInputEvent;

    if-eqz v9, :cond_3

    check-cast v2, Lcom/jme3/input/event/KeyInputEvent;

    invoke-interface {v7, v2}, Lcom/jme3/input/RawInputListener;->onKeyEvent(Lcom/jme3/input/event/KeyInputEvent;)V

    goto :goto_2

    :cond_3
    instance-of v9, v2, Lcom/jme3/input/event/MouseButtonEvent;

    if-eqz v9, :cond_4

    check-cast v2, Lcom/jme3/input/event/MouseButtonEvent;

    invoke-interface {v7, v2}, Lcom/jme3/input/RawInputListener;->onMouseButtonEvent(Lcom/jme3/input/event/MouseButtonEvent;)V

    goto :goto_2

    :cond_4
    instance-of v9, v2, Lcom/jme3/input/event/JoyAxisEvent;

    if-eqz v9, :cond_5

    check-cast v2, Lcom/jme3/input/event/JoyAxisEvent;

    invoke-interface {v7, v2}, Lcom/jme3/input/RawInputListener;->onJoyAxisEvent(Lcom/jme3/input/event/JoyAxisEvent;)V

    goto :goto_2

    :cond_5
    instance-of v9, v2, Lcom/jme3/input/event/JoyButtonEvent;

    if-eqz v9, :cond_6

    check-cast v2, Lcom/jme3/input/event/JoyButtonEvent;

    invoke-interface {v7, v2}, Lcom/jme3/input/RawInputListener;->onJoyButtonEvent(Lcom/jme3/input/event/JoyButtonEvent;)V

    goto :goto_2

    :cond_6
    instance-of v9, v2, Lcom/jme3/input/event/TouchEvent;

    if-eqz v9, :cond_7

    check-cast v2, Lcom/jme3/input/event/TouchEvent;

    invoke-interface {v7, v2}, Lcom/jme3/input/RawInputListener;->onTouchEvent(Lcom/jme3/input/event/TouchEvent;)V

    goto :goto_2

    :cond_7
    sget-boolean v9, Lcom/jme3/input/InputManager;->$assertionsDisabled:Z

    if-nez v9, :cond_0

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    :cond_8
    invoke-interface {v7}, Lcom/jme3/input/RawInputListener;->endInput()V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_9
    const/4 v3, 0x0

    :goto_3
    if-ge v3, v8, :cond_12

    iget-object v9, p0, Lcom/jme3/input/InputManager;->inputQueue:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/input/event/InputEvent;

    invoke-virtual {v2}, Lcom/jme3/input/event/InputEvent;->isConsumed()Z

    move-result v9

    if-eqz v9, :cond_a

    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_a
    instance-of v9, v2, Lcom/jme3/input/event/MouseMotionEvent;

    if-eqz v9, :cond_c

    move-object v9, v2

    check-cast v9, Lcom/jme3/input/event/MouseMotionEvent;

    invoke-direct {p0, v9}, Lcom/jme3/input/InputManager;->onMouseMotionEventQueued(Lcom/jme3/input/event/MouseMotionEvent;)V

    :cond_b
    :goto_5
    invoke-virtual {v2}, Lcom/jme3/input/event/InputEvent;->setConsumed()V

    goto :goto_4

    :cond_c
    instance-of v9, v2, Lcom/jme3/input/event/KeyInputEvent;

    if-eqz v9, :cond_d

    move-object v9, v2

    check-cast v9, Lcom/jme3/input/event/KeyInputEvent;

    invoke-direct {p0, v9}, Lcom/jme3/input/InputManager;->onKeyEventQueued(Lcom/jme3/input/event/KeyInputEvent;)V

    goto :goto_5

    :cond_d
    instance-of v9, v2, Lcom/jme3/input/event/MouseButtonEvent;

    if-eqz v9, :cond_e

    move-object v9, v2

    check-cast v9, Lcom/jme3/input/event/MouseButtonEvent;

    invoke-direct {p0, v9}, Lcom/jme3/input/InputManager;->onMouseButtonEventQueued(Lcom/jme3/input/event/MouseButtonEvent;)V

    goto :goto_5

    :cond_e
    instance-of v9, v2, Lcom/jme3/input/event/JoyAxisEvent;

    if-eqz v9, :cond_f

    move-object v9, v2

    check-cast v9, Lcom/jme3/input/event/JoyAxisEvent;

    invoke-direct {p0, v9}, Lcom/jme3/input/InputManager;->onJoyAxisEventQueued(Lcom/jme3/input/event/JoyAxisEvent;)V

    goto :goto_5

    :cond_f
    instance-of v9, v2, Lcom/jme3/input/event/JoyButtonEvent;

    if-eqz v9, :cond_10

    move-object v9, v2

    check-cast v9, Lcom/jme3/input/event/JoyButtonEvent;

    invoke-direct {p0, v9}, Lcom/jme3/input/InputManager;->onJoyButtonEventQueued(Lcom/jme3/input/event/JoyButtonEvent;)V

    goto :goto_5

    :cond_10
    instance-of v9, v2, Lcom/jme3/input/event/TouchEvent;

    if-eqz v9, :cond_11

    move-object v9, v2

    check-cast v9, Lcom/jme3/input/event/TouchEvent;

    invoke-virtual {p0, v9}, Lcom/jme3/input/InputManager;->onTouchEventQueued(Lcom/jme3/input/event/TouchEvent;)V

    goto :goto_5

    :cond_11
    sget-boolean v9, Lcom/jme3/input/InputManager;->$assertionsDisabled:Z

    if-nez v9, :cond_b

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    :cond_12
    iget-object v9, p0, Lcom/jme3/input/InputManager;->inputQueue:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    return-void
.end method


# virtual methods
.method public varargs addListener(Lcom/jme3/input/controls/InputListener;[Ljava/lang/String;)V
    .locals 6
    .param p1    # Lcom/jme3/input/controls/InputListener;
    .param p2    # [Ljava/lang/String;

    move-object v0, p2

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v4, v0, v1

    iget-object v5, p0, Lcom/jme3/input/InputManager;->mappings:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jme3/input/InputManager$Mapping;

    if-nez v3, :cond_0

    new-instance v3, Lcom/jme3/input/InputManager$Mapping;

    invoke-direct {v3, v4}, Lcom/jme3/input/InputManager$Mapping;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/jme3/input/InputManager;->mappings:Ljava/util/HashMap;

    invoke-virtual {v5, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    # getter for: Lcom/jme3/input/InputManager$Mapping;->listeners:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/jme3/input/InputManager$Mapping;->access$000(Lcom/jme3/input/InputManager$Mapping;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    # getter for: Lcom/jme3/input/InputManager$Mapping;->listeners:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/jme3/input/InputManager$Mapping;->access$000(Lcom/jme3/input/InputManager$Mapping;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public varargs addMapping(Ljava/lang/String;[Lcom/jme3/input/controls/Trigger;)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # [Lcom/jme3/input/controls/Trigger;

    iget-object v7, p0, Lcom/jme3/input/InputManager;->mappings:Ljava/util/HashMap;

    invoke-virtual {v7, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jme3/input/InputManager$Mapping;

    if-nez v4, :cond_0

    new-instance v4, Lcom/jme3/input/InputManager$Mapping;

    invoke-direct {v4, p1}, Lcom/jme3/input/InputManager$Mapping;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/jme3/input/InputManager;->mappings:Ljava/util/HashMap;

    invoke-virtual {v7, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    move-object v0, p2

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_3

    aget-object v6, v0, v2

    invoke-interface {v6}, Lcom/jme3/input/controls/Trigger;->triggerHashCode()I

    move-result v1

    iget-object v7, p0, Lcom/jme3/input/InputManager;->bindings:Lcom/jme3/util/IntMap;

    invoke-virtual {v7, v1}, Lcom/jme3/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    if-nez v5, :cond_1

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v7, p0, Lcom/jme3/input/InputManager;->bindings:Lcom/jme3/util/IntMap;

    invoke-virtual {v7, v1, v5}, Lcom/jme3/util/IntMap;->put(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    # getter for: Lcom/jme3/input/InputManager$Mapping;->triggers:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/jme3/input/InputManager$Mapping;->access$200(Lcom/jme3/input/InputManager$Mapping;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    sget-object v7, Lcom/jme3/input/InputManager;->logger:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v9, "Attempted to add mapping \"{0}\" twice to trigger."

    invoke-virtual {v7, v8, v9, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public beginInput()V
    .locals 0

    return-void
.end method

.method public endInput()V
    .locals 0

    return-void
.end method

.method public onJoyAxisEvent(Lcom/jme3/input/event/JoyAxisEvent;)V
    .locals 2
    .param p1    # Lcom/jme3/input/event/JoyAxisEvent;

    iget-boolean v0, p0, Lcom/jme3/input/InputManager;->eventsPermitted:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "JoyInput has raised an event at an illegal time."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/jme3/input/InputManager;->inputQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onJoyButtonEvent(Lcom/jme3/input/event/JoyButtonEvent;)V
    .locals 2
    .param p1    # Lcom/jme3/input/event/JoyButtonEvent;

    iget-boolean v0, p0, Lcom/jme3/input/InputManager;->eventsPermitted:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "JoyInput has raised an event at an illegal time."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/jme3/input/InputManager;->inputQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onKeyEvent(Lcom/jme3/input/event/KeyInputEvent;)V
    .locals 2
    .param p1    # Lcom/jme3/input/event/KeyInputEvent;

    iget-boolean v0, p0, Lcom/jme3/input/InputManager;->eventsPermitted:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "KeyInput has raised an event at an illegal time."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/jme3/input/InputManager;->inputQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onMouseButtonEvent(Lcom/jme3/input/event/MouseButtonEvent;)V
    .locals 3
    .param p1    # Lcom/jme3/input/event/MouseButtonEvent;

    iget-boolean v0, p0, Lcom/jme3/input/InputManager;->eventsPermitted:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "MouseInput has raised an event at an illegal time."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/jme3/input/InputManager;->cursorPos:Lcom/jme3/math/Vector2f;

    invoke-virtual {p1}, Lcom/jme3/input/event/MouseButtonEvent;->getX()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/jme3/input/event/MouseButtonEvent;->getY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/jme3/math/Vector2f;->set(FF)Lcom/jme3/math/Vector2f;

    iget-object v0, p0, Lcom/jme3/input/InputManager;->inputQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onMouseMotionEvent(Lcom/jme3/input/event/MouseMotionEvent;)V
    .locals 3
    .param p1    # Lcom/jme3/input/event/MouseMotionEvent;

    iget-boolean v0, p0, Lcom/jme3/input/InputManager;->eventsPermitted:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "MouseInput has raised an event at an illegal time."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/jme3/input/InputManager;->cursorPos:Lcom/jme3/math/Vector2f;

    invoke-virtual {p1}, Lcom/jme3/input/event/MouseMotionEvent;->getX()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/jme3/input/event/MouseMotionEvent;->getY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/jme3/math/Vector2f;->set(FF)Lcom/jme3/math/Vector2f;

    iget-object v0, p0, Lcom/jme3/input/InputManager;->inputQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onTouchEvent(Lcom/jme3/input/event/TouchEvent;)V
    .locals 2
    .param p1    # Lcom/jme3/input/event/TouchEvent;

    iget-boolean v0, p0, Lcom/jme3/input/InputManager;->eventsPermitted:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "TouchInput has raised an event at an illegal time."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/jme3/input/InputManager;->inputQueue:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onTouchEventQueued(Lcom/jme3/input/event/TouchEvent;)V
    .locals 10
    .param p1    # Lcom/jme3/input/event/TouchEvent;

    iget-object v8, p0, Lcom/jme3/input/InputManager;->bindings:Lcom/jme3/util/IntMap;

    invoke-virtual {p1}, Lcom/jme3/input/event/TouchEvent;->getKeyCode()I

    move-result v9

    invoke-static {v9}, Lcom/jme3/input/controls/TouchTrigger;->touchHash(I)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/jme3/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    if-nez v6, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v0, v7, -0x1

    :goto_0
    if-ltz v0, :cond_0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jme3/input/InputManager$Mapping;

    # getter for: Lcom/jme3/input/InputManager$Mapping;->listeners:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/jme3/input/InputManager$Mapping;->access$000(Lcom/jme3/input/InputManager$Mapping;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    :goto_1
    if-ltz v1, :cond_3

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/input/controls/InputListener;

    instance-of v8, v2, Lcom/jme3/input/controls/TouchListener;

    if-eqz v8, :cond_2

    check-cast v2, Lcom/jme3/input/controls/TouchListener;

    # getter for: Lcom/jme3/input/InputManager$Mapping;->name:Ljava/lang/String;
    invoke-static {v5}, Lcom/jme3/input/InputManager$Mapping;->access$100(Lcom/jme3/input/InputManager$Mapping;)Ljava/lang/String;

    move-result-object v8

    iget v9, p0, Lcom/jme3/input/InputManager;->frameTPF:F

    invoke-interface {v2, v8, p1, v9}, Lcom/jme3/input/controls/TouchListener;->onTouch(Ljava/lang/String;Lcom/jme3/input/event/TouchEvent;F)V

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public update(F)V
    .locals 7
    .param p1    # F

    const/4 v3, 0x1

    const/4 v4, 0x0

    iput p1, p0, Lcom/jme3/input/InputManager;->frameTPF:F

    const v2, 0x3c75c28f

    cmpg-float v2, p1, v2

    if-gez v2, :cond_2

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/jme3/input/InputManager;->safeMode:Z

    iget-object v2, p0, Lcom/jme3/input/InputManager;->keys:Lcom/jme3/input/KeyInput;

    invoke-interface {v2}, Lcom/jme3/input/KeyInput;->getInputTimeNanos()J

    move-result-wide v0

    iget-wide v5, p0, Lcom/jme3/input/InputManager;->lastUpdateTime:J

    sub-long v5, v0, v5

    iput-wide v5, p0, Lcom/jme3/input/InputManager;->frameDelta:J

    iput-boolean v3, p0, Lcom/jme3/input/InputManager;->eventsPermitted:Z

    iget-object v2, p0, Lcom/jme3/input/InputManager;->keys:Lcom/jme3/input/KeyInput;

    invoke-interface {v2}, Lcom/jme3/input/KeyInput;->update()V

    iget-object v2, p0, Lcom/jme3/input/InputManager;->mouse:Lcom/jme3/input/MouseInput;

    invoke-interface {v2}, Lcom/jme3/input/MouseInput;->update()V

    iget-object v2, p0, Lcom/jme3/input/InputManager;->joystick:Lcom/jme3/input/JoyInput;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jme3/input/InputManager;->joystick:Lcom/jme3/input/JoyInput;

    invoke-interface {v2}, Lcom/jme3/input/JoyInput;->update()V

    :cond_0
    iget-object v2, p0, Lcom/jme3/input/InputManager;->touch:Lcom/jme3/input/TouchInput;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/jme3/input/InputManager;->touch:Lcom/jme3/input/TouchInput;

    invoke-interface {v2}, Lcom/jme3/input/TouchInput;->update()V

    :cond_1
    iput-boolean v4, p0, Lcom/jme3/input/InputManager;->eventsPermitted:Z

    invoke-direct {p0}, Lcom/jme3/input/InputManager;->processQueue()V

    invoke-direct {p0}, Lcom/jme3/input/InputManager;->invokeUpdateActions()V

    iget-wide v2, p0, Lcom/jme3/input/InputManager;->lastUpdateTime:J

    iput-wide v2, p0, Lcom/jme3/input/InputManager;->lastLastUpdateTime:J

    iput-wide v0, p0, Lcom/jme3/input/InputManager;->lastUpdateTime:J

    return-void

    :cond_2
    move v2, v4

    goto :goto_0
.end method
