.class public Lcom/jme3/material/RenderState;
.super Ljava/lang/Object;
.source "RenderState.java"

# interfaces
.implements Lcom/jme3/export/Savable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/material/RenderState$StencilOperation;,
        Lcom/jme3/material/RenderState$FaceCullMode;,
        Lcom/jme3/material/RenderState$BlendMode;,
        Lcom/jme3/material/RenderState$TestFunction;
    }
.end annotation


# static fields
.field public static final ADDITIONAL:Lcom/jme3/material/RenderState;

.field public static final DEFAULT:Lcom/jme3/material/RenderState;

.field public static final NULL:Lcom/jme3/material/RenderState;


# instance fields
.field alphaFallOff:F

.field alphaTest:Z

.field applyAlphaFallOff:Z

.field applyAlphaTest:Z

.field applyBlendMode:Z

.field applyColorWrite:Z

.field applyCullMode:Z

.field applyDepthTest:Z

.field applyDepthWrite:Z

.field applyPointSprite:Z

.field applyPolyOffset:Z

.field applyStencilTest:Z

.field applyWireFrame:Z

.field backStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

.field backStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

.field backStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

.field backStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

.field blendMode:Lcom/jme3/material/RenderState$BlendMode;

.field colorWrite:Z

.field cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

.field depthTest:Z

.field depthWrite:Z

.field frontStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

.field frontStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

.field frontStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

.field frontStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

.field offsetEnabled:Z

.field offsetFactor:F

.field offsetUnits:F

.field pointSprite:Z

.field stencilTest:Z

.field wireframe:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/jme3/material/RenderState;

    invoke-direct {v0}, Lcom/jme3/material/RenderState;-><init>()V

    sput-object v0, Lcom/jme3/material/RenderState;->DEFAULT:Lcom/jme3/material/RenderState;

    new-instance v0, Lcom/jme3/material/RenderState;

    invoke-direct {v0}, Lcom/jme3/material/RenderState;-><init>()V

    sput-object v0, Lcom/jme3/material/RenderState;->NULL:Lcom/jme3/material/RenderState;

    new-instance v0, Lcom/jme3/material/RenderState;

    invoke-direct {v0}, Lcom/jme3/material/RenderState;-><init>()V

    sput-object v0, Lcom/jme3/material/RenderState;->ADDITIONAL:Lcom/jme3/material/RenderState;

    sget-object v0, Lcom/jme3/material/RenderState;->NULL:Lcom/jme3/material/RenderState;

    sget-object v1, Lcom/jme3/material/RenderState$FaceCullMode;->Off:Lcom/jme3/material/RenderState$FaceCullMode;

    iput-object v1, v0, Lcom/jme3/material/RenderState;->cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

    sget-object v0, Lcom/jme3/material/RenderState;->NULL:Lcom/jme3/material/RenderState;

    iput-boolean v2, v0, Lcom/jme3/material/RenderState;->depthTest:Z

    sget-object v0, Lcom/jme3/material/RenderState;->ADDITIONAL:Lcom/jme3/material/RenderState;

    iput-boolean v2, v0, Lcom/jme3/material/RenderState;->applyPointSprite:Z

    sget-object v0, Lcom/jme3/material/RenderState;->ADDITIONAL:Lcom/jme3/material/RenderState;

    iput-boolean v2, v0, Lcom/jme3/material/RenderState;->applyWireFrame:Z

    sget-object v0, Lcom/jme3/material/RenderState;->ADDITIONAL:Lcom/jme3/material/RenderState;

    iput-boolean v2, v0, Lcom/jme3/material/RenderState;->applyCullMode:Z

    sget-object v0, Lcom/jme3/material/RenderState;->ADDITIONAL:Lcom/jme3/material/RenderState;

    iput-boolean v2, v0, Lcom/jme3/material/RenderState;->applyDepthWrite:Z

    sget-object v0, Lcom/jme3/material/RenderState;->ADDITIONAL:Lcom/jme3/material/RenderState;

    iput-boolean v2, v0, Lcom/jme3/material/RenderState;->applyDepthTest:Z

    sget-object v0, Lcom/jme3/material/RenderState;->ADDITIONAL:Lcom/jme3/material/RenderState;

    iput-boolean v2, v0, Lcom/jme3/material/RenderState;->applyColorWrite:Z

    sget-object v0, Lcom/jme3/material/RenderState;->ADDITIONAL:Lcom/jme3/material/RenderState;

    iput-boolean v2, v0, Lcom/jme3/material/RenderState;->applyBlendMode:Z

    sget-object v0, Lcom/jme3/material/RenderState;->ADDITIONAL:Lcom/jme3/material/RenderState;

    iput-boolean v2, v0, Lcom/jme3/material/RenderState;->applyAlphaTest:Z

    sget-object v0, Lcom/jme3/material/RenderState;->ADDITIONAL:Lcom/jme3/material/RenderState;

    iput-boolean v2, v0, Lcom/jme3/material/RenderState;->applyAlphaFallOff:Z

    sget-object v0, Lcom/jme3/material/RenderState;->ADDITIONAL:Lcom/jme3/material/RenderState;

    iput-boolean v2, v0, Lcom/jme3/material/RenderState;->applyPolyOffset:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/jme3/material/RenderState;->pointSprite:Z

    iput-boolean v1, p0, Lcom/jme3/material/RenderState;->applyPointSprite:Z

    iput-boolean v2, p0, Lcom/jme3/material/RenderState;->wireframe:Z

    iput-boolean v1, p0, Lcom/jme3/material/RenderState;->applyWireFrame:Z

    sget-object v0, Lcom/jme3/material/RenderState$FaceCullMode;->Back:Lcom/jme3/material/RenderState$FaceCullMode;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

    iput-boolean v1, p0, Lcom/jme3/material/RenderState;->applyCullMode:Z

    iput-boolean v1, p0, Lcom/jme3/material/RenderState;->depthWrite:Z

    iput-boolean v1, p0, Lcom/jme3/material/RenderState;->applyDepthWrite:Z

    iput-boolean v1, p0, Lcom/jme3/material/RenderState;->depthTest:Z

    iput-boolean v1, p0, Lcom/jme3/material/RenderState;->applyDepthTest:Z

    iput-boolean v1, p0, Lcom/jme3/material/RenderState;->colorWrite:Z

    iput-boolean v1, p0, Lcom/jme3/material/RenderState;->applyColorWrite:Z

    sget-object v0, Lcom/jme3/material/RenderState$BlendMode;->Off:Lcom/jme3/material/RenderState$BlendMode;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->blendMode:Lcom/jme3/material/RenderState$BlendMode;

    iput-boolean v1, p0, Lcom/jme3/material/RenderState;->applyBlendMode:Z

    iput-boolean v2, p0, Lcom/jme3/material/RenderState;->alphaTest:Z

    iput-boolean v1, p0, Lcom/jme3/material/RenderState;->applyAlphaTest:Z

    iput v3, p0, Lcom/jme3/material/RenderState;->alphaFallOff:F

    iput-boolean v1, p0, Lcom/jme3/material/RenderState;->applyAlphaFallOff:Z

    iput v3, p0, Lcom/jme3/material/RenderState;->offsetFactor:F

    iput v3, p0, Lcom/jme3/material/RenderState;->offsetUnits:F

    iput-boolean v2, p0, Lcom/jme3/material/RenderState;->offsetEnabled:Z

    iput-boolean v1, p0, Lcom/jme3/material/RenderState;->applyPolyOffset:Z

    iput-boolean v2, p0, Lcom/jme3/material/RenderState;->stencilTest:Z

    iput-boolean v2, p0, Lcom/jme3/material/RenderState;->applyStencilTest:Z

    sget-object v0, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->frontStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v0, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->frontStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v0, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->frontStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v0, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->backStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v0, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->backStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v0, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->backStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v0, Lcom/jme3/material/RenderState$TestFunction;->Always:Lcom/jme3/material/RenderState$TestFunction;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->frontStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    sget-object v0, Lcom/jme3/material/RenderState$TestFunction;->Always:Lcom/jme3/material/RenderState$TestFunction;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->backStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    return-void
.end method


# virtual methods
.method public clone()Lcom/jme3/material/RenderState;
    .locals 2

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/material/RenderState;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/material/RenderState;->clone()Lcom/jme3/material/RenderState;

    move-result-object v0

    return-object v0
.end method

.method public copyMergedTo(Lcom/jme3/material/RenderState;Lcom/jme3/material/RenderState;)Lcom/jme3/material/RenderState;
    .locals 1
    .param p1    # Lcom/jme3/material/RenderState;
    .param p2    # Lcom/jme3/material/RenderState;

    if-nez p1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->applyPointSprite:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->pointSprite:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->pointSprite:Z

    :goto_1
    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->applyWireFrame:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->wireframe:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->wireframe:Z

    :goto_2
    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->applyCullMode:Z

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/jme3/material/RenderState;->cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

    :goto_3
    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->applyDepthWrite:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->depthWrite:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->depthWrite:Z

    :goto_4
    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->applyDepthTest:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->depthTest:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->depthTest:Z

    :goto_5
    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->applyColorWrite:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->colorWrite:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->colorWrite:Z

    :goto_6
    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->applyBlendMode:Z

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/jme3/material/RenderState;->blendMode:Lcom/jme3/material/RenderState$BlendMode;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->blendMode:Lcom/jme3/material/RenderState$BlendMode;

    :goto_7
    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->applyAlphaTest:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->alphaTest:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->alphaTest:Z

    :goto_8
    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->applyAlphaFallOff:Z

    if-eqz v0, :cond_9

    iget v0, p1, Lcom/jme3/material/RenderState;->alphaFallOff:F

    iput v0, p2, Lcom/jme3/material/RenderState;->alphaFallOff:F

    :goto_9
    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->applyPolyOffset:Z

    if-eqz v0, :cond_a

    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->offsetEnabled:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->offsetEnabled:Z

    iget v0, p1, Lcom/jme3/material/RenderState;->offsetFactor:F

    iput v0, p2, Lcom/jme3/material/RenderState;->offsetFactor:F

    iget v0, p1, Lcom/jme3/material/RenderState;->offsetUnits:F

    iput v0, p2, Lcom/jme3/material/RenderState;->offsetUnits:F

    :goto_a
    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->applyStencilTest:Z

    if-eqz v0, :cond_b

    iget-boolean v0, p1, Lcom/jme3/material/RenderState;->stencilTest:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->stencilTest:Z

    iget-object v0, p1, Lcom/jme3/material/RenderState;->frontStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->frontStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iget-object v0, p1, Lcom/jme3/material/RenderState;->frontStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->frontStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iget-object v0, p1, Lcom/jme3/material/RenderState;->frontStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->frontStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iget-object v0, p1, Lcom/jme3/material/RenderState;->backStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->backStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iget-object v0, p1, Lcom/jme3/material/RenderState;->backStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->backStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iget-object v0, p1, Lcom/jme3/material/RenderState;->backStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->backStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iget-object v0, p1, Lcom/jme3/material/RenderState;->frontStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->frontStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    iget-object v0, p1, Lcom/jme3/material/RenderState;->backStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->backStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    :goto_b
    move-object p0, p2

    goto/16 :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/jme3/material/RenderState;->pointSprite:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->pointSprite:Z

    goto :goto_1

    :cond_2
    iget-boolean v0, p0, Lcom/jme3/material/RenderState;->wireframe:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->wireframe:Z

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/jme3/material/RenderState;->cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

    goto :goto_3

    :cond_4
    iget-boolean v0, p0, Lcom/jme3/material/RenderState;->depthWrite:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->depthWrite:Z

    goto :goto_4

    :cond_5
    iget-boolean v0, p0, Lcom/jme3/material/RenderState;->depthTest:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->depthTest:Z

    goto :goto_5

    :cond_6
    iget-boolean v0, p0, Lcom/jme3/material/RenderState;->colorWrite:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->colorWrite:Z

    goto :goto_6

    :cond_7
    iget-object v0, p0, Lcom/jme3/material/RenderState;->blendMode:Lcom/jme3/material/RenderState$BlendMode;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->blendMode:Lcom/jme3/material/RenderState$BlendMode;

    goto :goto_7

    :cond_8
    iget-boolean v0, p0, Lcom/jme3/material/RenderState;->alphaTest:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->alphaTest:Z

    goto :goto_8

    :cond_9
    iget v0, p0, Lcom/jme3/material/RenderState;->alphaFallOff:F

    iput v0, p2, Lcom/jme3/material/RenderState;->alphaFallOff:F

    goto :goto_9

    :cond_a
    iget-boolean v0, p0, Lcom/jme3/material/RenderState;->offsetEnabled:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->offsetEnabled:Z

    iget v0, p0, Lcom/jme3/material/RenderState;->offsetFactor:F

    iput v0, p2, Lcom/jme3/material/RenderState;->offsetFactor:F

    iget v0, p0, Lcom/jme3/material/RenderState;->offsetUnits:F

    iput v0, p2, Lcom/jme3/material/RenderState;->offsetUnits:F

    goto :goto_a

    :cond_b
    iget-boolean v0, p0, Lcom/jme3/material/RenderState;->stencilTest:Z

    iput-boolean v0, p2, Lcom/jme3/material/RenderState;->stencilTest:Z

    iget-object v0, p0, Lcom/jme3/material/RenderState;->frontStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->frontStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iget-object v0, p0, Lcom/jme3/material/RenderState;->frontStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->frontStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iget-object v0, p0, Lcom/jme3/material/RenderState;->frontStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->frontStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iget-object v0, p0, Lcom/jme3/material/RenderState;->backStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->backStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iget-object v0, p0, Lcom/jme3/material/RenderState;->backStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->backStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iget-object v0, p0, Lcom/jme3/material/RenderState;->backStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->backStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    iget-object v0, p0, Lcom/jme3/material/RenderState;->frontStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->frontStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    iget-object v0, p0, Lcom/jme3/material/RenderState;->backStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    iput-object v0, p2, Lcom/jme3/material/RenderState;->backStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    goto :goto_b
.end method

.method public getBlendMode()Lcom/jme3/material/RenderState$BlendMode;
    .locals 1

    iget-object v0, p0, Lcom/jme3/material/RenderState;->blendMode:Lcom/jme3/material/RenderState$BlendMode;

    return-object v0
.end method

.method public getFaceCullMode()Lcom/jme3/material/RenderState$FaceCullMode;
    .locals 1

    iget-object v0, p0, Lcom/jme3/material/RenderState;->cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

    return-object v0
.end method

.method public getPolyOffsetFactor()F
    .locals 1

    iget v0, p0, Lcom/jme3/material/RenderState;->offsetFactor:F

    return v0
.end method

.method public getPolyOffsetUnits()F
    .locals 1

    iget v0, p0, Lcom/jme3/material/RenderState;->offsetUnits:F

    return v0
.end method

.method public isAlphaTest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/material/RenderState;->alphaTest:Z

    return v0
.end method

.method public isColorWrite()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/material/RenderState;->colorWrite:Z

    return v0
.end method

.method public isDepthTest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/material/RenderState;->depthTest:Z

    return v0
.end method

.method public isDepthWrite()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/material/RenderState;->depthWrite:Z

    return v0
.end method

.method public isPointSprite()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/material/RenderState;->pointSprite:Z

    return v0
.end method

.method public isPolyOffset()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/material/RenderState;->offsetEnabled:Z

    return v0
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v1

    const-string v0, "pointSprite"

    invoke-interface {v1, v0, v5}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->pointSprite:Z

    const-string v0, "wireframe"

    invoke-interface {v1, v0, v5}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->wireframe:Z

    const-string v0, "cullMode"

    const-class v2, Lcom/jme3/material/RenderState$FaceCullMode;

    sget-object v3, Lcom/jme3/material/RenderState$FaceCullMode;->Back:Lcom/jme3/material/RenderState$FaceCullMode;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/material/RenderState$FaceCullMode;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

    const-string v0, "depthWrite"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->depthWrite:Z

    const-string v0, "depthTest"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->depthTest:Z

    const-string v0, "colorWrite"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->colorWrite:Z

    const-string v0, "blendMode"

    const-class v2, Lcom/jme3/material/RenderState$BlendMode;

    sget-object v3, Lcom/jme3/material/RenderState$BlendMode;->Off:Lcom/jme3/material/RenderState$BlendMode;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/material/RenderState$BlendMode;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->blendMode:Lcom/jme3/material/RenderState$BlendMode;

    const-string v0, "alphaTest"

    invoke-interface {v1, v0, v5}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->alphaTest:Z

    const-string v0, "alphaFallOff"

    invoke-interface {v1, v0, v6}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/jme3/material/RenderState;->alphaFallOff:F

    const-string v0, "offsetEnabled"

    invoke-interface {v1, v0, v5}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->offsetEnabled:Z

    const-string v0, "offsetFactor"

    invoke-interface {v1, v0, v6}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/jme3/material/RenderState;->offsetFactor:F

    const-string v0, "offsetUnits"

    invoke-interface {v1, v0, v6}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/jme3/material/RenderState;->offsetUnits:F

    const-string v0, "stencilTest"

    invoke-interface {v1, v0, v5}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->stencilTest:Z

    const-string v0, "frontStencilStencilFailOperation"

    const-class v2, Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v3, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->frontStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    const-string v0, "frontStencilDepthFailOperation"

    const-class v2, Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v3, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->frontStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    const-string v0, "frontStencilDepthPassOperation"

    const-class v2, Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v3, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->frontStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    const-string v0, "backStencilStencilFailOperation"

    const-class v2, Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v3, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->backStencilStencilFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    const-string v0, "backStencilDepthFailOperation"

    const-class v2, Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v3, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->backStencilDepthFailOperation:Lcom/jme3/material/RenderState$StencilOperation;

    const-string v0, "backStencilDepthPassOperation"

    const-class v2, Lcom/jme3/material/RenderState$StencilOperation;

    sget-object v3, Lcom/jme3/material/RenderState$StencilOperation;->Keep:Lcom/jme3/material/RenderState$StencilOperation;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/material/RenderState$StencilOperation;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->backStencilDepthPassOperation:Lcom/jme3/material/RenderState$StencilOperation;

    const-string v0, "frontStencilFunction"

    const-class v2, Lcom/jme3/material/RenderState$TestFunction;

    sget-object v3, Lcom/jme3/material/RenderState$TestFunction;->Always:Lcom/jme3/material/RenderState$TestFunction;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/material/RenderState$TestFunction;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->frontStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    const-string v0, "backStencilFunction"

    const-class v2, Lcom/jme3/material/RenderState$TestFunction;

    sget-object v3, Lcom/jme3/material/RenderState$TestFunction;->Always:Lcom/jme3/material/RenderState$TestFunction;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/material/RenderState$TestFunction;

    iput-object v0, p0, Lcom/jme3/material/RenderState;->backStencilFunction:Lcom/jme3/material/RenderState$TestFunction;

    const-string v0, "applyPointSprite"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyPointSprite:Z

    const-string v0, "applyWireFrame"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyWireFrame:Z

    const-string v0, "applyCullMode"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyCullMode:Z

    const-string v0, "applyDepthWrite"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyDepthWrite:Z

    const-string v0, "applyDepthTest"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyDepthTest:Z

    const-string v0, "applyColorWrite"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyColorWrite:Z

    const-string v0, "applyBlendMode"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyBlendMode:Z

    const-string v0, "applyAlphaTest"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyAlphaTest:Z

    const-string v0, "applyAlphaFallOff"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyAlphaFallOff:Z

    const-string v0, "applyPolyOffset"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyPolyOffset:Z

    return-void
.end method

.method public setAlphaFallOff(F)V
    .locals 1
    .param p1    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyAlphaFallOff:Z

    iput p1, p0, Lcom/jme3/material/RenderState;->alphaFallOff:F

    return-void
.end method

.method public setAlphaTest(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyAlphaTest:Z

    iput-boolean p1, p0, Lcom/jme3/material/RenderState;->alphaTest:Z

    return-void
.end method

.method public setBlendMode(Lcom/jme3/material/RenderState$BlendMode;)V
    .locals 1
    .param p1    # Lcom/jme3/material/RenderState$BlendMode;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyBlendMode:Z

    iput-object p1, p0, Lcom/jme3/material/RenderState;->blendMode:Lcom/jme3/material/RenderState$BlendMode;

    return-void
.end method

.method public setColorWrite(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyColorWrite:Z

    iput-boolean p1, p0, Lcom/jme3/material/RenderState;->colorWrite:Z

    return-void
.end method

.method public setDepthTest(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyDepthTest:Z

    iput-boolean p1, p0, Lcom/jme3/material/RenderState;->depthTest:Z

    return-void
.end method

.method public setDepthWrite(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyDepthWrite:Z

    iput-boolean p1, p0, Lcom/jme3/material/RenderState;->depthWrite:Z

    return-void
.end method

.method public setFaceCullMode(Lcom/jme3/material/RenderState$FaceCullMode;)V
    .locals 1
    .param p1    # Lcom/jme3/material/RenderState$FaceCullMode;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyCullMode:Z

    iput-object p1, p0, Lcom/jme3/material/RenderState;->cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

    return-void
.end method

.method public setPointSprite(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyPointSprite:Z

    iput-boolean p1, p0, Lcom/jme3/material/RenderState;->pointSprite:Z

    return-void
.end method

.method public setPolyOffset(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyPolyOffset:Z

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->offsetEnabled:Z

    iput p1, p0, Lcom/jme3/material/RenderState;->offsetFactor:F

    iput p2, p0, Lcom/jme3/material/RenderState;->offsetUnits:F

    return-void
.end method

.method public setWireframe(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/material/RenderState;->applyWireFrame:Z

    iput-boolean p1, p0, Lcom/jme3/material/RenderState;->wireframe:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RenderState[\npointSprite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->pointSprite:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\napplyPointSprite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->applyPointSprite:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nwireframe="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->wireframe:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\napplyWireFrame="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->applyWireFrame:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\ncullMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/material/RenderState;->cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\napplyCullMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->applyCullMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\ndepthWrite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->depthWrite:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\napplyDepthWrite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->applyDepthWrite:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\ndepthTest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->depthTest:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\napplyDepthTest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->applyDepthTest:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\ncolorWrite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->colorWrite:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\napplyColorWrite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->applyColorWrite:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nblendMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/material/RenderState;->blendMode:Lcom/jme3/material/RenderState$BlendMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\napplyBlendMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->applyBlendMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nalphaTest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->alphaTest:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\napplyAlphaTest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->applyAlphaTest:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nalphaFallOff="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/material/RenderState;->alphaFallOff:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\napplyAlphaFallOff="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->applyAlphaFallOff:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\noffsetEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->offsetEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\napplyPolyOffset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/material/RenderState;->applyPolyOffset:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\noffsetFactor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/material/RenderState;->offsetFactor:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\noffsetUnits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/material/RenderState;->offsetUnits:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
