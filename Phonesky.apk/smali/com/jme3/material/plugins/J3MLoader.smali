.class public Lcom/jme3/material/plugins/J3MLoader;
.super Ljava/lang/Object;
.source "J3MLoader.java"

# interfaces
.implements Lcom/jme3/asset/AssetLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/material/plugins/J3MLoader$1;
    }
.end annotation


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private assetManager:Lcom/jme3/asset/AssetManager;

.field private fragName:Ljava/lang/String;

.field private key:Lcom/jme3/asset/AssetKey;

.field private material:Lcom/jme3/material/Material;

.field private materialDef:Lcom/jme3/material/MaterialDef;

.field private renderState:Lcom/jme3/material/RenderState;

.field private shaderLang:Ljava/lang/String;

.field private technique:Lcom/jme3/material/TechniqueDef;

.field private vertName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/material/plugins/J3MLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/material/plugins/J3MLoader;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private loadFromRoot(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/jme3/util/blockparser/Statement;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v11

    const/4 v12, 0x2

    if-ne v11, v12, :cond_1

    const/4 v11, 0x0

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/util/blockparser/Statement;

    invoke-virtual {v1}, Lcom/jme3/util/blockparser/Statement;->getLine()Ljava/lang/String;

    move-result-object v5

    const-string v11, "Exception"

    invoke-virtual {v5, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    new-instance v11, Lcom/jme3/asset/AssetLoadException;

    const-string v12, "Exception "

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {v5, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/jme3/asset/AssetLoadException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_0
    new-instance v11, Ljava/io/IOException;

    const-string v12, "In multiroot material, expected first statement to be \'Exception\'"

    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v11

    const/4 v12, 0x1

    if-eq v11, v12, :cond_2

    new-instance v11, Ljava/io/IOException;

    const-string v12, "Too many roots in J3M/J3MD file"

    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_2
    const/4 v3, 0x0

    const/4 v11, 0x0

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/jme3/util/blockparser/Statement;

    invoke-virtual {v7}, Lcom/jme3/util/blockparser/Statement;->getLine()Ljava/lang/String;

    move-result-object v6

    const-string v11, "MaterialDef"

    invoke-virtual {v6, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    const-string v11, "MaterialDef "

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v6, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const/4 v3, 0x0

    :goto_0
    const-string v11, ":"

    const/4 v12, 0x2

    invoke-virtual {v6, v11, v12}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v8

    const-string v11, ""

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    new-instance v11, Ljava/io/IOException;

    const-string v12, "Material name cannot be empty"

    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_3
    const-string v11, "Material"

    invoke-virtual {v6, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    const-string v11, "Material "

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v6, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const/4 v3, 0x1

    goto :goto_0

    :cond_4
    new-instance v11, Ljava/io/IOException;

    const-string v12, "Specified file is not a Material file"

    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_5
    array-length v11, v8

    const/4 v12, 0x2

    if-ne v11, v12, :cond_9

    if-nez v3, :cond_6

    new-instance v11, Ljava/io/IOException;

    const-string v12, "Must use \'Material\' when extending."

    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_6
    const/4 v11, 0x1

    aget-object v11, v8, v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iget-object v11, p0, Lcom/jme3/material/plugins/J3MLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    new-instance v12, Lcom/jme3/asset/AssetKey;

    invoke-direct {v12, v2}, Lcom/jme3/asset/AssetKey;-><init>(Ljava/lang/String;)V

    invoke-interface {v11, v12}, Lcom/jme3/asset/AssetManager;->loadAsset(Lcom/jme3/asset/AssetKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/material/MaterialDef;

    if-nez v0, :cond_7

    new-instance v11, Ljava/io/IOException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Extended material "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " cannot be found."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_7
    new-instance v11, Lcom/jme3/material/Material;

    invoke-direct {v11, v0}, Lcom/jme3/material/Material;-><init>(Lcom/jme3/material/MaterialDef;)V

    iput-object v11, p0, Lcom/jme3/material/plugins/J3MLoader;->material:Lcom/jme3/material/Material;

    iget-object v11, p0, Lcom/jme3/material/plugins/J3MLoader;->material:Lcom/jme3/material/Material;

    iget-object v12, p0, Lcom/jme3/material/plugins/J3MLoader;->key:Lcom/jme3/asset/AssetKey;

    invoke-virtual {v11, v12}, Lcom/jme3/material/Material;->setKey(Lcom/jme3/asset/AssetKey;)V

    :goto_1
    invoke-virtual {v7}, Lcom/jme3/util/blockparser/Statement;->getContents()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_8
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_11

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/jme3/util/blockparser/Statement;

    invoke-virtual {v10}, Lcom/jme3/util/blockparser/Statement;->getLine()Ljava/lang/String;

    move-result-object v11

    const-string v12, "[ \\{]"

    invoke-virtual {v11, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    const/4 v11, 0x0

    aget-object v9, v8, v11

    if-eqz v3, :cond_e

    const-string v11, "MaterialParameters"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    invoke-virtual {v10}, Lcom/jme3/util/blockparser/Statement;->getContents()Ljava/util/List;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/jme3/material/plugins/J3MLoader;->readExtendingMaterialParams(Ljava/util/List;)V

    goto :goto_2

    :cond_9
    array-length v11, v8

    const/4 v12, 0x1

    if-ne v11, v12, :cond_b

    if-eqz v3, :cond_a

    new-instance v11, Ljava/io/IOException;

    const-string v12, "Expected \':\', got \'{\'"

    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_a
    new-instance v11, Lcom/jme3/material/MaterialDef;

    iget-object v12, p0, Lcom/jme3/material/plugins/J3MLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    invoke-direct {v11, v12, v6}, Lcom/jme3/material/MaterialDef;-><init>(Lcom/jme3/asset/AssetManager;Ljava/lang/String;)V

    iput-object v11, p0, Lcom/jme3/material/plugins/J3MLoader;->materialDef:Lcom/jme3/material/MaterialDef;

    iget-object v11, p0, Lcom/jme3/material/plugins/J3MLoader;->materialDef:Lcom/jme3/material/MaterialDef;

    iget-object v12, p0, Lcom/jme3/material/plugins/J3MLoader;->key:Lcom/jme3/asset/AssetKey;

    invoke-virtual {v12}, Lcom/jme3/asset/AssetKey;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/jme3/material/MaterialDef;->setAssetName(Ljava/lang/String;)V

    goto :goto_1

    :cond_b
    new-instance v11, Ljava/io/IOException;

    const-string v12, "Cannot use colon in material name/path"

    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_c
    const-string v11, "AdditionalRenderState"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_d

    invoke-virtual {v10}, Lcom/jme3/util/blockparser/Statement;->getContents()Ljava/util/List;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/jme3/material/plugins/J3MLoader;->readAdditionalRenderState(Ljava/util/List;)V

    goto :goto_2

    :cond_d
    const-string v11, "Transparent"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    invoke-virtual {v10}, Lcom/jme3/util/blockparser/Statement;->getLine()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/jme3/material/plugins/J3MLoader;->readTransparentStatement(Ljava/lang/String;)V

    goto :goto_2

    :cond_e
    const-string v11, "Technique"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    invoke-direct {p0, v10}, Lcom/jme3/material/plugins/J3MLoader;->readTechnique(Lcom/jme3/util/blockparser/Statement;)V

    goto/16 :goto_2

    :cond_f
    const-string v11, "MaterialParameters"

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_10

    invoke-virtual {v10}, Lcom/jme3/util/blockparser/Statement;->getContents()Ljava/util/List;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/jme3/material/plugins/J3MLoader;->readMaterialParams(Ljava/util/List;)V

    goto/16 :goto_2

    :cond_10
    new-instance v11, Ljava/io/IOException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Expected material statement, got \'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_11
    return-void
.end method

.method private parseBoolean(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, "On"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private readAdditionalRenderState(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/jme3/util/blockparser/Statement;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lcom/jme3/material/plugins/J3MLoader;->material:Lcom/jme3/material/Material;

    invoke-virtual {v2}, Lcom/jme3/material/Material;->getAdditionalRenderState()Lcom/jme3/material/RenderState;

    move-result-object v2

    iput-object v2, p0, Lcom/jme3/material/plugins/J3MLoader;->renderState:Lcom/jme3/material/RenderState;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/util/blockparser/Statement;

    invoke-virtual {v1}, Lcom/jme3/util/blockparser/Statement;->getLine()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/jme3/material/plugins/J3MLoader;->readRenderStateStatement(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/jme3/material/plugins/J3MLoader;->renderState:Lcom/jme3/material/RenderState;

    return-void
.end method

.method private readDefine(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string v1, ":"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/jme3/material/plugins/J3MLoader;->technique:Lcom/jme3/material/TechniqueDef;

    aget-object v2, v0, v3

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/jme3/shader/VarType;->Boolean:Lcom/jme3/shader/VarType;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/jme3/material/TechniqueDef;->addShaderPresetDefine(Ljava/lang/String;Lcom/jme3/shader/VarType;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    array-length v1, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/jme3/material/plugins/J3MLoader;->technique:Lcom/jme3/material/TechniqueDef;

    aget-object v2, v0, v4

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    aget-object v3, v0, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/jme3/material/TechniqueDef;->addShaderParamDefine(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Define syntax incorrect"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private readDefines(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/jme3/util/blockparser/Statement;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/util/blockparser/Statement;

    invoke-virtual {v1}, Lcom/jme3/util/blockparser/Statement;->getLine()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/jme3/material/plugins/J3MLoader;->readDefine(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private readExtendingMaterialParams(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/jme3/util/blockparser/Statement;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/util/blockparser/Statement;

    invoke-virtual {v1}, Lcom/jme3/util/blockparser/Statement;->getLine()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/jme3/material/plugins/J3MLoader;->readValueParam(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private readLightMode(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v2, "\\p{javaWhitespace}+"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    new-instance v2, Ljava/io/IOException;

    const-string v3, "LightMode statement syntax incorrect"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/jme3/material/TechniqueDef$LightMode;->valueOf(Ljava/lang/String;)Lcom/jme3/material/TechniqueDef$LightMode;

    move-result-object v0

    iget-object v2, p0, Lcom/jme3/material/plugins/J3MLoader;->technique:Lcom/jme3/material/TechniqueDef;

    invoke-virtual {v2, v0}, Lcom/jme3/material/TechniqueDef;->setLightMode(Lcom/jme3/material/TechniqueDef$LightMode;)V

    return-void
.end method

.method private readMaterialParams(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/jme3/util/blockparser/Statement;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/util/blockparser/Statement;

    invoke-virtual {v1}, Lcom/jme3/util/blockparser/Statement;->getLine()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/jme3/material/plugins/J3MLoader;->readParam(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private readParam(Ljava/lang/String;)V
    .locals 14
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v11, 0x2

    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v1, 0x0

    const/4 v5, 0x0

    const-string v10, ":"

    invoke-virtual {p1, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    array-length v10, v7

    if-ne v10, v13, :cond_1

    :goto_0
    const-string v10, "("

    invoke-virtual {p1, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    const/4 v10, -0x1

    if-eq v8, v10, :cond_0

    const-string v10, ")"

    invoke-virtual {p1, v10, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    add-int/lit8 v10, v8, 0x1

    invoke-virtual {p1, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Lcom/jme3/material/FixedFuncBinding;->valueOf(Ljava/lang/String;)Lcom/jme3/material/FixedFuncBinding;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    invoke-virtual {p1, v12, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :cond_0
    const-string v10, "\\p{javaWhitespace}+"

    invoke-virtual {p1, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    array-length v10, v7

    if-eq v10, v11, :cond_3

    new-instance v10, Ljava/io/IOException;

    const-string v11, "Parameter statement syntax incorrect"

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_1
    array-length v10, v7

    if-eq v10, v11, :cond_2

    new-instance v10, Ljava/io/IOException;

    const-string v11, "Parameter statement syntax incorrect"

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_2
    aget-object v10, v7, v12

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    aget-object v10, v7, v13

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v4

    new-instance v10, Ljava/io/IOException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FixedFuncBinding \'"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v7, v13

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\' does not exist!"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_3
    aget-object v10, v7, v12

    const-string v11, "Color"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    sget-object v9, Lcom/jme3/shader/VarType;->Vector4:Lcom/jme3/shader/VarType;

    :goto_1
    aget-object v6, v7, v13

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    invoke-direct {p0, v9, v1}, Lcom/jme3/material/plugins/J3MLoader;->readValue(Lcom/jme3/shader/VarType;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    :cond_4
    iget-object v10, p0, Lcom/jme3/material/plugins/J3MLoader;->materialDef:Lcom/jme3/material/MaterialDef;

    invoke-virtual {v10, v9, v6, v2, v5}, Lcom/jme3/material/MaterialDef;->addMaterialParam(Lcom/jme3/shader/VarType;Ljava/lang/String;Ljava/lang/Object;Lcom/jme3/material/FixedFuncBinding;)V

    return-void

    :cond_5
    aget-object v10, v7, v12

    invoke-static {v10}, Lcom/jme3/shader/VarType;->valueOf(Ljava/lang/String;)Lcom/jme3/shader/VarType;

    move-result-object v9

    goto :goto_1
.end method

.method private readRenderState(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/jme3/util/blockparser/Statement;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Lcom/jme3/material/RenderState;

    invoke-direct {v2}, Lcom/jme3/material/RenderState;-><init>()V

    iput-object v2, p0, Lcom/jme3/material/plugins/J3MLoader;->renderState:Lcom/jme3/material/RenderState;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/util/blockparser/Statement;

    invoke-virtual {v1}, Lcom/jme3/util/blockparser/Statement;->getLine()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/jme3/material/plugins/J3MLoader;->readRenderStateStatement(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/jme3/material/plugins/J3MLoader;->technique:Lcom/jme3/material/TechniqueDef;

    iget-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->renderState:Lcom/jme3/material/RenderState;

    invoke-virtual {v2, v3}, Lcom/jme3/material/TechniqueDef;->setRenderState(Lcom/jme3/material/RenderState;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/jme3/material/plugins/J3MLoader;->renderState:Lcom/jme3/material/RenderState;

    return-void
.end method

.method private readRenderStateStatement(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v3, "\\p{javaWhitespace}+"

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v3, v1, v5

    const-string v4, "Wireframe"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->renderState:Lcom/jme3/material/RenderState;

    aget-object v4, v1, v6

    invoke-direct {p0, v4}, Lcom/jme3/material/plugins/J3MLoader;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/jme3/material/RenderState;->setWireframe(Z)V

    :goto_0
    return-void

    :cond_0
    aget-object v3, v1, v5

    const-string v4, "FaceCull"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->renderState:Lcom/jme3/material/RenderState;

    aget-object v4, v1, v6

    invoke-static {v4}, Lcom/jme3/material/RenderState$FaceCullMode;->valueOf(Ljava/lang/String;)Lcom/jme3/material/RenderState$FaceCullMode;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/jme3/material/RenderState;->setFaceCullMode(Lcom/jme3/material/RenderState$FaceCullMode;)V

    goto :goto_0

    :cond_1
    aget-object v3, v1, v5

    const-string v4, "DepthWrite"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->renderState:Lcom/jme3/material/RenderState;

    aget-object v4, v1, v6

    invoke-direct {p0, v4}, Lcom/jme3/material/plugins/J3MLoader;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/jme3/material/RenderState;->setDepthWrite(Z)V

    goto :goto_0

    :cond_2
    aget-object v3, v1, v5

    const-string v4, "DepthTest"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->renderState:Lcom/jme3/material/RenderState;

    aget-object v4, v1, v6

    invoke-direct {p0, v4}, Lcom/jme3/material/plugins/J3MLoader;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/jme3/material/RenderState;->setDepthTest(Z)V

    goto :goto_0

    :cond_3
    aget-object v3, v1, v5

    const-string v4, "Blend"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->renderState:Lcom/jme3/material/RenderState;

    aget-object v4, v1, v6

    invoke-static {v4}, Lcom/jme3/material/RenderState$BlendMode;->valueOf(Ljava/lang/String;)Lcom/jme3/material/RenderState$BlendMode;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/jme3/material/RenderState;->setBlendMode(Lcom/jme3/material/RenderState$BlendMode;)V

    goto :goto_0

    :cond_4
    aget-object v3, v1, v5

    const-string v4, "AlphaTestFalloff"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->renderState:Lcom/jme3/material/RenderState;

    invoke-virtual {v3, v6}, Lcom/jme3/material/RenderState;->setAlphaTest(Z)V

    iget-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->renderState:Lcom/jme3/material/RenderState;

    aget-object v4, v1, v6

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/jme3/material/RenderState;->setAlphaFallOff(F)V

    goto :goto_0

    :cond_5
    aget-object v3, v1, v5

    const-string v4, "PolyOffset"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    aget-object v3, v1, v6

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    iget-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->renderState:Lcom/jme3/material/RenderState;

    invoke-virtual {v3, v0, v2}, Lcom/jme3/material/RenderState;->setPolyOffset(FF)V

    goto/16 :goto_0

    :cond_6
    aget-object v3, v1, v5

    const-string v4, "ColorWrite"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->renderState:Lcom/jme3/material/RenderState;

    aget-object v4, v1, v6

    invoke-direct {p0, v4}, Lcom/jme3/material/plugins/J3MLoader;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/jme3/material/RenderState;->setColorWrite(Z)V

    goto/16 :goto_0

    :cond_7
    aget-object v3, v1, v5

    const-string v4, "PointSprite"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->renderState:Lcom/jme3/material/RenderState;

    aget-object v4, v1, v6

    invoke-direct {p0, v4}, Lcom/jme3/material/plugins/J3MLoader;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/jme3/material/RenderState;->setPointSprite(Z)V

    goto/16 :goto_0

    :cond_8
    const/4 v3, 0x0

    aget-object v4, v1, v5

    invoke-direct {p0, v3, v4}, Lcom/jme3/material/plugins/J3MLoader;->throwIfNequal(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private readShaderStatement(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v2, ":"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v2, v0

    if-eq v2, v6, :cond_0

    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Shader statement syntax incorrect"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    aget-object v2, v0, v4

    const-string v3, "\\p{javaWhitespace}+"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    if-eq v2, v6, :cond_1

    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Shader statement syntax incorrect: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    aget-object v2, v1, v5

    iput-object v2, p0, Lcom/jme3/material/plugins/J3MLoader;->shaderLang:Ljava/lang/String;

    aget-object v2, v1, v4

    const-string v3, "VertexShader"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    aget-object v2, v0, v5

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/jme3/material/plugins/J3MLoader;->vertName:Ljava/lang/String;

    :cond_2
    :goto_0
    return-void

    :cond_3
    aget-object v2, v1, v4

    const-string v3, "FragmentShader"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    aget-object v2, v0, v5

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/jme3/material/plugins/J3MLoader;->fragName:Ljava/lang/String;

    goto :goto_0
.end method

.method private readShadowMode(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v2, "\\p{javaWhitespace}+"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    new-instance v2, Ljava/io/IOException;

    const-string v3, "ShadowMode statement syntax incorrect"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/jme3/material/TechniqueDef$ShadowMode;->valueOf(Ljava/lang/String;)Lcom/jme3/material/TechniqueDef$ShadowMode;

    move-result-object v0

    iget-object v2, p0, Lcom/jme3/material/plugins/J3MLoader;->technique:Lcom/jme3/material/TechniqueDef;

    invoke-virtual {v2, v0}, Lcom/jme3/material/TechniqueDef;->setShadowMode(Lcom/jme3/material/TechniqueDef$ShadowMode;)V

    return-void
.end method

.method private readTechnique(Lcom/jme3/util/blockparser/Statement;)V
    .locals 8
    .param p1    # Lcom/jme3/util/blockparser/Statement;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x1

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/jme3/util/blockparser/Statement;->getLine()Ljava/lang/String;

    move-result-object v3

    const-string v4, "\\p{javaWhitespace}+"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v3, v1

    if-ne v3, v5, :cond_0

    new-instance v3, Lcom/jme3/material/TechniqueDef;

    invoke-direct {v3, v7}, Lcom/jme3/material/TechniqueDef;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->technique:Lcom/jme3/material/TechniqueDef;

    :goto_0
    invoke-virtual {p1}, Lcom/jme3/util/blockparser/Statement;->getContents()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/util/blockparser/Statement;

    invoke-direct {p0, v2}, Lcom/jme3/material/plugins/J3MLoader;->readTechniqueStatement(Lcom/jme3/util/blockparser/Statement;)V

    goto :goto_1

    :cond_0
    array-length v3, v1

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    new-instance v3, Lcom/jme3/material/TechniqueDef;

    aget-object v4, v1, v5

    invoke-direct {v3, v4}, Lcom/jme3/material/TechniqueDef;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->technique:Lcom/jme3/material/TechniqueDef;

    goto :goto_0

    :cond_1
    new-instance v3, Ljava/io/IOException;

    const-string v4, "Technique statement syntax incorrect"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    iget-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->vertName:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->fragName:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->technique:Lcom/jme3/material/TechniqueDef;

    iget-object v4, p0, Lcom/jme3/material/plugins/J3MLoader;->vertName:Ljava/lang/String;

    iget-object v5, p0, Lcom/jme3/material/plugins/J3MLoader;->fragName:Ljava/lang/String;

    iget-object v6, p0, Lcom/jme3/material/plugins/J3MLoader;->shaderLang:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6}, Lcom/jme3/material/TechniqueDef;->setShaderFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v3, p0, Lcom/jme3/material/plugins/J3MLoader;->materialDef:Lcom/jme3/material/MaterialDef;

    iget-object v4, p0, Lcom/jme3/material/plugins/J3MLoader;->technique:Lcom/jme3/material/TechniqueDef;

    invoke-virtual {v3, v4}, Lcom/jme3/material/MaterialDef;->addTechniqueDef(Lcom/jme3/material/TechniqueDef;)V

    iput-object v7, p0, Lcom/jme3/material/plugins/J3MLoader;->technique:Lcom/jme3/material/TechniqueDef;

    iput-object v7, p0, Lcom/jme3/material/plugins/J3MLoader;->vertName:Ljava/lang/String;

    iput-object v7, p0, Lcom/jme3/material/plugins/J3MLoader;->fragName:Ljava/lang/String;

    iput-object v7, p0, Lcom/jme3/material/plugins/J3MLoader;->shaderLang:Ljava/lang/String;

    return-void
.end method

.method private readTechniqueStatement(Lcom/jme3/util/blockparser/Statement;)V
    .locals 4
    .param p1    # Lcom/jme3/util/blockparser/Statement;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/jme3/util/blockparser/Statement;->getLine()Ljava/lang/String;

    move-result-object v1

    const-string v2, "[ \\{]"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v1, v0, v3

    const-string v2, "VertexShader"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    aget-object v1, v0, v3

    const-string v2, "FragmentShader"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/jme3/util/blockparser/Statement;->getLine()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jme3/material/plugins/J3MLoader;->readShaderStatement(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    aget-object v1, v0, v3

    const-string v2, "LightMode"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/jme3/util/blockparser/Statement;->getLine()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jme3/material/plugins/J3MLoader;->readLightMode(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    aget-object v1, v0, v3

    const-string v2, "ShadowMode"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/jme3/util/blockparser/Statement;->getLine()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jme3/material/plugins/J3MLoader;->readShadowMode(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    aget-object v1, v0, v3

    const-string v2, "WorldParameters"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcom/jme3/util/blockparser/Statement;->getContents()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jme3/material/plugins/J3MLoader;->readWorldParams(Ljava/util/List;)V

    goto :goto_0

    :cond_4
    aget-object v1, v0, v3

    const-string v2, "RenderState"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Lcom/jme3/util/blockparser/Statement;->getContents()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jme3/material/plugins/J3MLoader;->readRenderState(Ljava/util/List;)V

    goto :goto_0

    :cond_5
    aget-object v1, v0, v3

    const-string v2, "Defines"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p1}, Lcom/jme3/util/blockparser/Statement;->getContents()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jme3/material/plugins/J3MLoader;->readDefines(Ljava/util/List;)V

    goto :goto_0

    :cond_6
    const/4 v1, 0x0

    aget-object v2, v0, v3

    invoke-direct {p0, v1, v2}, Lcom/jme3/material/plugins/J3MLoader;->throwIfNequal(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private readTransparentStatement(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v1, "\\p{javaWhitespace}+"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Transparent statement syntax incorrect"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/jme3/material/plugins/J3MLoader;->material:Lcom/jme3/material/Material;

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-direct {p0, v2}, Lcom/jme3/material/plugins/J3MLoader;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jme3/material/Material;->setTransparent(Z)V

    return-void
.end method

.method private readValue(Lcom/jme3/shader/VarType;Ljava/lang/String;)Ljava/lang/Object;
    .locals 13
    .param p1    # Lcom/jme3/shader/VarType;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v11, 0x3

    const/4 v12, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    invoke-virtual {p1}, Lcom/jme3/shader/VarType;->isTextureType()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string v7, "Flip Repeat "

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/16 v7, 0xc

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const/4 v1, 0x1

    const/4 v2, 0x1

    :cond_0
    :goto_0
    new-instance v5, Lcom/jme3/asset/TextureKey;

    invoke-direct {v5, v6, v1}, Lcom/jme3/asset/TextureKey;-><init>(Ljava/lang/String;Z)V

    sget-object v7, Lcom/jme3/shader/VarType;->TextureCubeMap:Lcom/jme3/shader/VarType;

    if-ne p1, v7, :cond_4

    move v7, v8

    :goto_1
    invoke-virtual {v5, v7}, Lcom/jme3/asset/TextureKey;->setAsCube(Z)V

    invoke-virtual {v5, v8}, Lcom/jme3/asset/TextureKey;->setGenerateMips(Z)V

    :try_start_0
    iget-object v7, p0, Lcom/jme3/material/plugins/J3MLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    invoke-interface {v7, v5}, Lcom/jme3/asset/AssetManager;->loadTexture(Lcom/jme3/asset/TextureKey;)Lcom/jme3/texture/Texture;
    :try_end_0
    .catch Lcom/jme3/asset/AssetNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    :goto_2
    if-eqz v4, :cond_5

    if-eqz v2, :cond_1

    sget-object v7, Lcom/jme3/texture/Texture$WrapMode;->Repeat:Lcom/jme3/texture/Texture$WrapMode;

    invoke-virtual {v4, v7}, Lcom/jme3/texture/Texture;->setWrap(Lcom/jme3/texture/Texture$WrapMode;)V

    :cond_1
    :goto_3
    return-object v4

    :cond_2
    const-string v7, "Flip "

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    const-string v7, "Repeat "

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v7, 0x7

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const/4 v2, 0x1

    goto :goto_0

    :cond_4
    move v7, v9

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v7, Lcom/jme3/material/plugins/J3MLoader;->logger:Ljava/util/logging/Logger;

    sget-object v10, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v11, "Cannot locate {0} for material {1}"

    new-array v12, v12, [Ljava/lang/Object;

    aput-object v5, v12, v9

    iget-object v9, p0, Lcom/jme3/material/plugins/J3MLoader;->key:Lcom/jme3/asset/AssetKey;

    aput-object v9, v12, v8

    invoke-virtual {v7, v10, v11, v12}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v4, 0x0

    goto :goto_2

    :cond_5
    new-instance v4, Lcom/jme3/texture/Texture2D;

    invoke-static {}, Lcom/jme3/util/PlaceholderAssets;->getPlaceholderImage()Lcom/jme3/texture/Image;

    move-result-object v7

    invoke-direct {v4, v7}, Lcom/jme3/texture/Texture2D;-><init>(Lcom/jme3/texture/Image;)V

    goto :goto_3

    :cond_6
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v10, "\\p{javaWhitespace}+"

    invoke-virtual {v7, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    sget-object v7, Lcom/jme3/material/plugins/J3MLoader$1;->$SwitchMap$com$jme3$shader$VarType:[I

    invoke-virtual {p1}, Lcom/jme3/shader/VarType;->ordinal()I

    move-result v10

    aget v7, v7, v10

    packed-switch v7, :pswitch_data_0

    new-instance v7, Ljava/lang/UnsupportedOperationException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unknown type: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v7

    :pswitch_0
    array-length v7, v3

    if-eq v7, v8, :cond_7

    new-instance v7, Ljava/io/IOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Float value parameter must have 1 entry: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_7
    aget-object v7, v3, v9

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    goto/16 :goto_3

    :pswitch_1
    array-length v7, v3

    if-eq v7, v12, :cond_8

    new-instance v7, Ljava/io/IOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Vector2 value parameter must have 2 entries: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_8
    new-instance v4, Lcom/jme3/math/Vector2f;

    aget-object v7, v3, v9

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    aget-object v8, v3, v8

    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    invoke-direct {v4, v7, v8}, Lcom/jme3/math/Vector2f;-><init>(FF)V

    goto/16 :goto_3

    :pswitch_2
    array-length v7, v3

    if-eq v7, v11, :cond_9

    new-instance v7, Ljava/io/IOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Vector3 value parameter must have 3 entries: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_9
    new-instance v4, Lcom/jme3/math/Vector3f;

    aget-object v7, v3, v9

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    aget-object v8, v3, v8

    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    aget-object v9, v3, v12

    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v9

    invoke-direct {v4, v7, v8, v9}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    goto/16 :goto_3

    :pswitch_3
    array-length v7, v3

    const/4 v10, 0x4

    if-eq v7, v10, :cond_a

    new-instance v7, Ljava/io/IOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Vector4 value parameter must have 4 entries: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_a
    new-instance v4, Lcom/jme3/math/ColorRGBA;

    aget-object v7, v3, v9

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    aget-object v8, v3, v8

    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    aget-object v9, v3, v12

    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v9

    aget-object v10, v3, v11

    invoke-static {v10}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v10

    invoke-direct {v4, v7, v8, v9, v10}, Lcom/jme3/math/ColorRGBA;-><init>(FFFF)V

    goto/16 :goto_3

    :pswitch_4
    array-length v7, v3

    if-eq v7, v8, :cond_b

    new-instance v7, Ljava/io/IOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Int value parameter must have 1 entry: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_b
    aget-object v7, v3, v9

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/16 :goto_3

    :pswitch_5
    array-length v7, v3

    if-eq v7, v8, :cond_c

    new-instance v7, Ljava/io/IOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Boolean value parameter must have 1 entry: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_c
    aget-object v7, v3, v9

    invoke-static {v7}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private readValueParam(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x2

    const-string v4, ":"

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    array-length v4, v2

    if-eq v4, v5, :cond_0

    new-instance v4, Ljava/io/IOException;

    const-string v5, "Value parameter statement syntax incorrect"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    const/4 v4, 0x0

    aget-object v4, v2, v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/jme3/material/plugins/J3MLoader;->material:Lcom/jme3/material/Material;

    invoke-virtual {v4}, Lcom/jme3/material/Material;->getMaterialDef()Lcom/jme3/material/MaterialDef;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/jme3/material/MaterialDef;->getMaterialParam(Ljava/lang/String;)Lcom/jme3/material/MatParam;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "The material parameter: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is undefined."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    invoke-virtual {v1}, Lcom/jme3/material/MatParam;->getVarType()Lcom/jme3/shader/VarType;

    move-result-object v4

    const/4 v5, 0x1

    aget-object v5, v2, v5

    invoke-direct {p0, v4, v5}, Lcom/jme3/material/plugins/J3MLoader;->readValue(Lcom/jme3/shader/VarType;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1}, Lcom/jme3/material/MatParam;->getVarType()Lcom/jme3/shader/VarType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jme3/shader/VarType;->isTextureType()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/jme3/material/plugins/J3MLoader;->material:Lcom/jme3/material/Material;

    invoke-virtual {v1}, Lcom/jme3/material/MatParam;->getVarType()Lcom/jme3/shader/VarType;

    move-result-object v5

    check-cast v3, Lcom/jme3/texture/Texture;

    invoke-virtual {v4, v0, v5, v3}, Lcom/jme3/material/Material;->setTextureParam(Ljava/lang/String;Lcom/jme3/shader/VarType;Lcom/jme3/texture/Texture;)V

    :goto_0
    return-void

    :cond_2
    iget-object v4, p0, Lcom/jme3/material/plugins/J3MLoader;->material:Lcom/jme3/material/Material;

    invoke-virtual {v1}, Lcom/jme3/material/MatParam;->getVarType()Lcom/jme3/shader/VarType;

    move-result-object v5

    invoke-virtual {v4, v0, v5, v3}, Lcom/jme3/material/Material;->setParam(Ljava/lang/String;Lcom/jme3/shader/VarType;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private readWorldParams(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/jme3/util/blockparser/Statement;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/util/blockparser/Statement;

    iget-object v2, p0, Lcom/jme3/material/plugins/J3MLoader;->technique:Lcom/jme3/material/TechniqueDef;

    invoke-virtual {v1}, Lcom/jme3/util/blockparser/Statement;->getLine()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/jme3/material/TechniqueDef;->addWorldParam(Ljava/lang/String;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private throwIfNequal(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a statement, got \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\', got \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method


# virtual methods
.method public load(Lcom/jme3/asset/AssetInfo;)Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/jme3/asset/AssetInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getManager()Lcom/jme3/asset/AssetManager;

    move-result-object v1

    iput-object v1, p0, Lcom/jme3/material/plugins/J3MLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->openStream()Ljava/io/InputStream;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getKey()Lcom/jme3/asset/AssetKey;

    move-result-object v1

    iput-object v1, p0, Lcom/jme3/material/plugins/J3MLoader;->key:Lcom/jme3/asset/AssetKey;

    invoke-static {v0}, Lcom/jme3/util/blockparser/BlockLanguageParser;->parse(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jme3/material/plugins/J3MLoader;->loadFromRoot(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_0
    iget-object v1, p0, Lcom/jme3/material/plugins/J3MLoader;->material:Lcom/jme3/material/Material;

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getKey()Lcom/jme3/asset/AssetKey;

    move-result-object v1

    instance-of v1, v1, Lcom/jme3/asset/MaterialKey;

    if-nez v1, :cond_2

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Material instances must be loaded via MaterialKey"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v1

    :cond_2
    iget-object v1, p0, Lcom/jme3/material/plugins/J3MLoader;->material:Lcom/jme3/material/Material;

    :goto_0
    return-object v1

    :cond_3
    iget-object v1, p0, Lcom/jme3/material/plugins/J3MLoader;->materialDef:Lcom/jme3/material/MaterialDef;

    goto :goto_0
.end method
