.class public final Lcom/jme3/math/Quaternion;
.super Ljava/lang/Object;
.source "Quaternion.java"

# interfaces
.implements Lcom/jme3/export/Savable;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final DIRECTION_Z:Lcom/jme3/math/Quaternion;

.field public static final IDENTITY:Lcom/jme3/math/Quaternion;

.field public static final ZERO:Lcom/jme3/math/Quaternion;

.field private static final logger:Ljava/util/logging/Logger;

.field static final serialVersionUID:J = 0x1L


# instance fields
.field protected w:F

.field protected x:F

.field protected y:F

.field protected z:F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    const-class v0, Lcom/jme3/math/Quaternion;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/math/Quaternion;->logger:Ljava/util/logging/Logger;

    new-instance v0, Lcom/jme3/math/Quaternion;

    invoke-direct {v0}, Lcom/jme3/math/Quaternion;-><init>()V

    sput-object v0, Lcom/jme3/math/Quaternion;->IDENTITY:Lcom/jme3/math/Quaternion;

    new-instance v0, Lcom/jme3/math/Quaternion;

    invoke-direct {v0}, Lcom/jme3/math/Quaternion;-><init>()V

    sput-object v0, Lcom/jme3/math/Quaternion;->DIRECTION_Z:Lcom/jme3/math/Quaternion;

    new-instance v0, Lcom/jme3/math/Quaternion;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/jme3/math/Quaternion;-><init>(FFFF)V

    sput-object v0, Lcom/jme3/math/Quaternion;->ZERO:Lcom/jme3/math/Quaternion;

    sget-object v0, Lcom/jme3/math/Quaternion;->DIRECTION_Z:Lcom/jme3/math/Quaternion;

    sget-object v1, Lcom/jme3/math/Vector3f;->UNIT_X:Lcom/jme3/math/Vector3f;

    sget-object v2, Lcom/jme3/math/Vector3f;->UNIT_Y:Lcom/jme3/math/Vector3f;

    sget-object v3, Lcom/jme3/math/Vector3f;->UNIT_Z:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, v1, v2, v3}, Lcom/jme3/math/Quaternion;->fromAxes(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Quaternion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/jme3/math/Quaternion;->x:F

    iput v0, p0, Lcom/jme3/math/Quaternion;->y:F

    iput v0, p0, Lcom/jme3/math/Quaternion;->z:F

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/jme3/math/Quaternion;->w:F

    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jme3/math/Quaternion;->x:F

    iput p2, p0, Lcom/jme3/math/Quaternion;->y:F

    iput p3, p0, Lcom/jme3/math/Quaternion;->z:F

    iput p4, p0, Lcom/jme3/math/Quaternion;->w:F

    return-void
.end method


# virtual methods
.method public clone()Lcom/jme3/math/Quaternion;
    .locals 2

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/math/Quaternion;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/math/Quaternion;->clone()Lcom/jme3/math/Quaternion;

    move-result-object v0

    return-object v0
.end method

.method public dot(Lcom/jme3/math/Quaternion;)F
    .locals 3
    .param p1    # Lcom/jme3/math/Quaternion;

    iget v0, p0, Lcom/jme3/math/Quaternion;->w:F

    iget v1, p1, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v2, p1, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v2, p1, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v2, p1, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v2, 0x1

    const/4 v1, 0x0

    instance-of v3, p1, Lcom/jme3/math/Quaternion;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-ne p0, p1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/jme3/math/Quaternion;

    iget v3, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v4, v0, Lcom/jme3/math/Quaternion;->x:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-nez v3, :cond_0

    iget v3, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v4, v0, Lcom/jme3/math/Quaternion;->y:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-nez v3, :cond_0

    iget v3, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v4, v0, Lcom/jme3/math/Quaternion;->z:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-nez v3, :cond_0

    iget v3, p0, Lcom/jme3/math/Quaternion;->w:F

    iget v4, v0, Lcom/jme3/math/Quaternion;->w:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public fromAngleAxis(FLcom/jme3/math/Vector3f;)Lcom/jme3/math/Quaternion;
    .locals 1
    .param p1    # F
    .param p2    # Lcom/jme3/math/Vector3f;

    invoke-virtual {p2}, Lcom/jme3/math/Vector3f;->normalize()Lcom/jme3/math/Vector3f;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/jme3/math/Quaternion;->fromAngleNormalAxis(FLcom/jme3/math/Vector3f;)Lcom/jme3/math/Quaternion;

    return-object p0
.end method

.method public fromAngleNormalAxis(FLcom/jme3/math/Vector3f;)Lcom/jme3/math/Quaternion;
    .locals 4
    .param p1    # F
    .param p2    # Lcom/jme3/math/Vector3f;

    const/4 v3, 0x0

    iget v2, p2, Lcom/jme3/math/Vector3f;->x:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p2, Lcom/jme3/math/Vector3f;->y:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p2, Lcom/jme3/math/Vector3f;->z:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/jme3/math/Quaternion;->loadIdentity()V

    :goto_0
    return-object p0

    :cond_0
    const/high16 v2, 0x3f000000

    mul-float v0, v2, p1

    invoke-static {v0}, Lcom/jme3/math/FastMath;->sin(F)F

    move-result v1

    invoke-static {v0}, Lcom/jme3/math/FastMath;->cos(F)F

    move-result v2

    iput v2, p0, Lcom/jme3/math/Quaternion;->w:F

    iget v2, p2, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v2, v1

    iput v2, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v2, p2, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v2, v1

    iput v2, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v2, p2, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v2, v1

    iput v2, p0, Lcom/jme3/math/Quaternion;->z:F

    goto :goto_0
.end method

.method public fromAngles(FFF)Lcom/jme3/math/Quaternion;
    .locals 13
    .param p1    # F
    .param p2    # F
    .param p3    # F

    const/high16 v11, 0x3f000000

    mul-float v0, p3, v11

    invoke-static {v0}, Lcom/jme3/math/FastMath;->sin(F)F

    move-result v6

    invoke-static {v0}, Lcom/jme3/math/FastMath;->cos(F)F

    move-result v1

    const/high16 v11, 0x3f000000

    mul-float v0, p2, v11

    invoke-static {v0}, Lcom/jme3/math/FastMath;->sin(F)F

    move-result v7

    invoke-static {v0}, Lcom/jme3/math/FastMath;->cos(F)F

    move-result v2

    const/high16 v11, 0x3f000000

    mul-float v0, p1, v11

    invoke-static {v0}, Lcom/jme3/math/FastMath;->sin(F)F

    move-result v10

    invoke-static {v0}, Lcom/jme3/math/FastMath;->cos(F)F

    move-result v5

    mul-float v3, v2, v1

    mul-float v9, v7, v6

    mul-float v4, v2, v6

    mul-float v8, v7, v1

    mul-float v11, v3, v5

    mul-float v12, v9, v10

    sub-float/2addr v11, v12

    iput v11, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float v11, v3, v10

    mul-float v12, v9, v5

    add-float/2addr v11, v12

    iput v11, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float v11, v8, v5

    mul-float v12, v4, v10

    add-float/2addr v11, v12

    iput v11, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float v11, v4, v5

    mul-float v12, v8, v10

    sub-float/2addr v11, v12

    iput v11, p0, Lcom/jme3/math/Quaternion;->z:F

    invoke-virtual {p0}, Lcom/jme3/math/Quaternion;->normalize()V

    return-object p0
.end method

.method public fromAxes(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Quaternion;
    .locals 10
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;
    .param p3    # Lcom/jme3/math/Vector3f;

    iget v1, p1, Lcom/jme3/math/Vector3f;->x:F

    iget v2, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v3, p3, Lcom/jme3/math/Vector3f;->x:F

    iget v4, p1, Lcom/jme3/math/Vector3f;->y:F

    iget v5, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v6, p3, Lcom/jme3/math/Vector3f;->y:F

    iget v7, p1, Lcom/jme3/math/Vector3f;->z:F

    iget v8, p2, Lcom/jme3/math/Vector3f;->z:F

    iget v9, p3, Lcom/jme3/math/Vector3f;->z:F

    move-object v0, p0

    invoke-virtual/range {v0 .. v9}, Lcom/jme3/math/Quaternion;->fromRotationMatrix(FFFFFFFFF)Lcom/jme3/math/Quaternion;

    move-result-object v0

    return-object v0
.end method

.method public fromRotationMatrix(FFFFFFFFF)Lcom/jme3/math/Quaternion;
    .locals 5
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # F
    .param p6    # F
    .param p7    # F
    .param p8    # F
    .param p9    # F

    const/high16 v4, 0x3f800000

    const/high16 v3, 0x3f000000

    add-float v2, p1, p5

    add-float v1, v2, p9

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_0

    add-float v2, v1, v4

    invoke-static {v2}, Lcom/jme3/math/FastMath;->sqrt(F)F

    move-result v0

    mul-float v2, v3, v0

    iput v2, p0, Lcom/jme3/math/Quaternion;->w:F

    div-float v0, v3, v0

    sub-float v2, p8, p6

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/jme3/math/Quaternion;->x:F

    sub-float v2, p3, p7

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/jme3/math/Quaternion;->y:F

    sub-float v2, p4, p2

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/jme3/math/Quaternion;->z:F

    :goto_0
    return-object p0

    :cond_0
    cmpl-float v2, p1, p5

    if-lez v2, :cond_1

    cmpl-float v2, p1, p9

    if-lez v2, :cond_1

    add-float v2, v4, p1

    sub-float/2addr v2, p5

    sub-float/2addr v2, p9

    invoke-static {v2}, Lcom/jme3/math/FastMath;->sqrt(F)F

    move-result v0

    mul-float v2, v0, v3

    iput v2, p0, Lcom/jme3/math/Quaternion;->x:F

    div-float v0, v3, v0

    add-float v2, p4, p2

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/jme3/math/Quaternion;->y:F

    add-float v2, p3, p7

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/jme3/math/Quaternion;->z:F

    sub-float v2, p8, p6

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/jme3/math/Quaternion;->w:F

    goto :goto_0

    :cond_1
    cmpl-float v2, p5, p9

    if-lez v2, :cond_2

    add-float v2, v4, p5

    sub-float/2addr v2, p1

    sub-float/2addr v2, p9

    invoke-static {v2}, Lcom/jme3/math/FastMath;->sqrt(F)F

    move-result v0

    mul-float v2, v0, v3

    iput v2, p0, Lcom/jme3/math/Quaternion;->y:F

    div-float v0, v3, v0

    add-float v2, p4, p2

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/jme3/math/Quaternion;->x:F

    add-float v2, p8, p6

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/jme3/math/Quaternion;->z:F

    sub-float v2, p3, p7

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/jme3/math/Quaternion;->w:F

    goto :goto_0

    :cond_2
    add-float v2, v4, p9

    sub-float/2addr v2, p1

    sub-float/2addr v2, p5

    invoke-static {v2}, Lcom/jme3/math/FastMath;->sqrt(F)F

    move-result v0

    mul-float v2, v0, v3

    iput v2, p0, Lcom/jme3/math/Quaternion;->z:F

    div-float v0, v3, v0

    add-float v2, p3, p7

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/jme3/math/Quaternion;->x:F

    add-float v2, p8, p6

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/jme3/math/Quaternion;->y:F

    sub-float v2, p4, p2

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/jme3/math/Quaternion;->w:F

    goto :goto_0
.end method

.method public getRotationColumn(I)Lcom/jme3/math/Vector3f;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/jme3/math/Quaternion;->getRotationColumn(ILcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v0

    return-object v0
.end method

.method public getRotationColumn(ILcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 13
    .param p1    # I
    .param p2    # Lcom/jme3/math/Vector3f;

    if-nez p2, :cond_0

    new-instance p2, Lcom/jme3/math/Vector3f;

    invoke-direct {p2}, Lcom/jme3/math/Vector3f;-><init>()V

    :cond_0
    invoke-virtual {p0}, Lcom/jme3/math/Quaternion;->norm()F

    move-result v0

    const/high16 v10, 0x3f800000

    cmpl-float v10, v0, v10

    if-eqz v10, :cond_1

    invoke-static {v0}, Lcom/jme3/math/FastMath;->invSqrt(F)F

    move-result v0

    :cond_1
    iget v10, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v11, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v10, v11

    mul-float v2, v10, v0

    iget v10, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v11, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v10, v11

    mul-float v3, v10, v0

    iget v10, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v11, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v10, v11

    mul-float v4, v10, v0

    iget v10, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v11, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v10, v11

    mul-float v1, v10, v0

    iget v10, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v11, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v10, v11

    mul-float v6, v10, v0

    iget v10, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v11, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v10, v11

    mul-float v7, v10, v0

    iget v10, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v11, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v10, v11

    mul-float v5, v10, v0

    iget v10, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v11, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v10, v11

    mul-float v9, v10, v0

    iget v10, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v11, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v10, v11

    mul-float v8, v10, v0

    packed-switch p1, :pswitch_data_0

    sget-object v10, Lcom/jme3/math/Quaternion;->logger:Ljava/util/logging/Logger;

    const-string v11, "Invalid column index."

    invoke-virtual {v10, v11}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Invalid column index. "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    :pswitch_0
    const/high16 v10, 0x3f800000

    const/high16 v11, 0x40000000

    add-float v12, v6, v9

    mul-float/2addr v11, v12

    sub-float/2addr v10, v11

    iput v10, p2, Lcom/jme3/math/Vector3f;->x:F

    const/high16 v10, 0x40000000

    add-float v11, v3, v8

    mul-float/2addr v10, v11

    iput v10, p2, Lcom/jme3/math/Vector3f;->y:F

    const/high16 v10, 0x40000000

    sub-float v11, v4, v5

    mul-float/2addr v10, v11

    iput v10, p2, Lcom/jme3/math/Vector3f;->z:F

    :goto_0
    return-object p2

    :pswitch_1
    const/high16 v10, 0x40000000

    sub-float v11, v3, v8

    mul-float/2addr v10, v11

    iput v10, p2, Lcom/jme3/math/Vector3f;->x:F

    const/high16 v10, 0x3f800000

    const/high16 v11, 0x40000000

    add-float v12, v2, v9

    mul-float/2addr v11, v12

    sub-float/2addr v10, v11

    iput v10, p2, Lcom/jme3/math/Vector3f;->y:F

    const/high16 v10, 0x40000000

    add-float v11, v7, v1

    mul-float/2addr v10, v11

    iput v10, p2, Lcom/jme3/math/Vector3f;->z:F

    goto :goto_0

    :pswitch_2
    const/high16 v10, 0x40000000

    add-float v11, v4, v5

    mul-float/2addr v10, v11

    iput v10, p2, Lcom/jme3/math/Vector3f;->x:F

    const/high16 v10, 0x40000000

    sub-float v11, v7, v1

    mul-float/2addr v10, v11

    iput v10, p2, Lcom/jme3/math/Vector3f;->y:F

    const/high16 v10, 0x3f800000

    const/high16 v11, 0x40000000

    add-float v12, v2, v6

    mul-float/2addr v11, v12

    sub-float/2addr v10, v11

    iput v10, p2, Lcom/jme3/math/Vector3f;->z:F

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getW()F
    .locals 1

    iget v0, p0, Lcom/jme3/math/Quaternion;->w:F

    return v0
.end method

.method public getX()F
    .locals 1

    iget v0, p0, Lcom/jme3/math/Quaternion;->x:F

    return v0
.end method

.method public getY()F
    .locals 1

    iget v0, p0, Lcom/jme3/math/Quaternion;->y:F

    return v0
.end method

.method public getZ()F
    .locals 1

    iget v0, p0, Lcom/jme3/math/Quaternion;->z:F

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x25

    iget v1, p0, Lcom/jme3/math/Quaternion;->x:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/lit16 v0, v1, 0x559

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Quaternion;->y:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Quaternion;->z:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Quaternion;->w:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    return v0
.end method

.method public inverse()Lcom/jme3/math/Quaternion;
    .locals 7

    invoke-virtual {p0}, Lcom/jme3/math/Quaternion;->norm()F

    move-result v1

    float-to-double v2, v1

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    const/high16 v2, 0x3f800000

    div-float v0, v2, v1

    new-instance v2, Lcom/jme3/math/Quaternion;

    iget v3, p0, Lcom/jme3/math/Quaternion;->x:F

    neg-float v3, v3

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    neg-float v4, v4

    mul-float/2addr v4, v0

    iget v5, p0, Lcom/jme3/math/Quaternion;->z:F

    neg-float v5, v5

    mul-float/2addr v5, v0

    iget v6, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v6, v0

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/jme3/math/Quaternion;-><init>(FFFF)V

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public inverseLocal()Lcom/jme3/math/Quaternion;
    .locals 6

    invoke-virtual {p0}, Lcom/jme3/math/Quaternion;->norm()F

    move-result v1

    float-to-double v2, v1

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    const/high16 v2, 0x3f800000

    div-float v0, v2, v1

    iget v2, p0, Lcom/jme3/math/Quaternion;->x:F

    neg-float v3, v0

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v2, p0, Lcom/jme3/math/Quaternion;->y:F

    neg-float v3, v0

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v2, p0, Lcom/jme3/math/Quaternion;->z:F

    neg-float v3, v0

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v2, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/jme3/math/Quaternion;->w:F

    :cond_0
    return-object p0
.end method

.method public loadIdentity()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/jme3/math/Quaternion;->z:F

    iput v0, p0, Lcom/jme3/math/Quaternion;->y:F

    iput v0, p0, Lcom/jme3/math/Quaternion;->x:F

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/jme3/math/Quaternion;->w:F

    return-void
.end method

.method public mult(Lcom/jme3/math/Quaternion;Lcom/jme3/math/Quaternion;)Lcom/jme3/math/Quaternion;
    .locals 6
    .param p1    # Lcom/jme3/math/Quaternion;
    .param p2    # Lcom/jme3/math/Quaternion;

    if-nez p2, :cond_0

    new-instance p2, Lcom/jme3/math/Quaternion;

    invoke-direct {p2}, Lcom/jme3/math/Quaternion;-><init>()V

    :cond_0
    iget v0, p1, Lcom/jme3/math/Quaternion;->w:F

    iget v1, p1, Lcom/jme3/math/Quaternion;->x:F

    iget v2, p1, Lcom/jme3/math/Quaternion;->y:F

    iget v3, p1, Lcom/jme3/math/Quaternion;->z:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v4, v0

    iget v5, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v5, v3

    add-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v5, v2

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v5, v1

    add-float/2addr v4, v5

    iput v4, p2, Lcom/jme3/math/Quaternion;->x:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->x:F

    neg-float v4, v4

    mul-float/2addr v4, v3

    iget v5, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v5, v1

    add-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v5, v2

    add-float/2addr v4, v5

    iput v4, p2, Lcom/jme3/math/Quaternion;->y:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v4, v2

    iget v5, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v5, v1

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v5, v3

    add-float/2addr v4, v5

    iput v4, p2, Lcom/jme3/math/Quaternion;->z:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->x:F

    neg-float v4, v4

    mul-float/2addr v4, v1

    iget v5, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v5, v2

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v5, v3

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    iput v4, p2, Lcom/jme3/math/Quaternion;->w:F

    return-object p2
.end method

.method public mult(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 7
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    const/4 v4, 0x0

    const/high16 v6, 0x40000000

    if-nez p2, :cond_0

    new-instance p2, Lcom/jme3/math/Vector3f;

    invoke-direct {p2}, Lcom/jme3/math/Vector3f;-><init>()V

    :cond_0
    iget v3, p1, Lcom/jme3/math/Vector3f;->x:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_1

    iget v3, p1, Lcom/jme3/math/Vector3f;->y:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_1

    iget v3, p1, Lcom/jme3/math/Vector3f;->z:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_1

    invoke-virtual {p2, v4, v4, v4}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    :goto_0
    return-object p2

    :cond_1
    iget v0, p1, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->z:F

    iget v3, p0, Lcom/jme3/math/Quaternion;->w:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v3, v4

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v4, v6

    iget v5, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v4, v6

    iget v5, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v1

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v5, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v0

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v4, v6

    iget v5, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v4, v6

    iget v5, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v5, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v0

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v5, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v0

    sub-float/2addr v3, v4

    iput v3, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v3, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v3, v6

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v3, v4

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v5, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v4, v6

    iget v5, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v4, v6

    iget v5, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v0

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v5, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v1

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->w:F

    iget v5, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v4, v6

    iget v5, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v2

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v5, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v1

    sub-float/2addr v3, v4

    iput v3, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v3, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v3, v6

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v3, v4

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v4, v6

    iget v5, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v5, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v4, v6

    iget v5, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v0

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v5, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v2

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v4, v6

    iget v5, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v5, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v2

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->w:F

    iget v5, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v4, v5

    mul-float/2addr v4, v2

    add-float/2addr v3, v4

    iput v3, p2, Lcom/jme3/math/Vector3f;->z:F

    goto/16 :goto_0
.end method

.method public multLocal(Lcom/jme3/math/Quaternion;)Lcom/jme3/math/Quaternion;
    .locals 6
    .param p1    # Lcom/jme3/math/Quaternion;

    iget v3, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v4, p1, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v5, p1, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v5, p1, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->w:F

    iget v5, p1, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v4, v5

    add-float v0, v3, v4

    iget v3, p0, Lcom/jme3/math/Quaternion;->x:F

    neg-float v3, v3

    iget v4, p1, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v5, p1, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v5, p1, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->w:F

    iget v5, p1, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v4, v5

    add-float v1, v3, v4

    iget v3, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v4, p1, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v5, p1, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v5, p1, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->w:F

    iget v5, p1, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v4, v5

    add-float v2, v3, v4

    iget v3, p0, Lcom/jme3/math/Quaternion;->x:F

    neg-float v3, v3

    iget v4, p1, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v5, p1, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v5, p1, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/jme3/math/Quaternion;->w:F

    iget v5, p1, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, p0, Lcom/jme3/math/Quaternion;->w:F

    iput v0, p0, Lcom/jme3/math/Quaternion;->x:F

    iput v1, p0, Lcom/jme3/math/Quaternion;->y:F

    iput v2, p0, Lcom/jme3/math/Quaternion;->z:F

    return-object p0
.end method

.method public multLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 6
    .param p1    # Lcom/jme3/math/Vector3f;

    const/high16 v5, 0x40000000

    iget v2, p0, Lcom/jme3/math/Quaternion;->w:F

    iget v3, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v2, v3

    iget v3, p1, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v3, v5

    iget v4, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v3, v5

    iget v4, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v3, v5

    iget v4, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v3, v5

    iget v4, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v3, v4

    sub-float v0, v2, v3

    iget v2, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v2, v5

    iget v3, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v2, v3

    iget v3, p1, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v3, v5

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v3, v5

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->w:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v3, v5

    iget v4, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v3, v4

    sub-float v1, v2, v3

    iget v2, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v2, v5

    iget v3, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v3, v5

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v3, v5

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v3, v5

    iget v4, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Quaternion;->w:F

    iget v4, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, p1, Lcom/jme3/math/Vector3f;->z:F

    iput v0, p1, Lcom/jme3/math/Vector3f;->x:F

    iput v1, p1, Lcom/jme3/math/Vector3f;->y:F

    return-object p1
.end method

.method public nlerp(Lcom/jme3/math/Quaternion;F)V
    .locals 4
    .param p1    # Lcom/jme3/math/Quaternion;
    .param p2    # F

    invoke-virtual {p0, p1}, Lcom/jme3/math/Quaternion;->dot(Lcom/jme3/math/Quaternion;)F

    move-result v1

    const/high16 v2, 0x3f800000

    sub-float v0, v2, p2

    const/4 v2, 0x0

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    iget v2, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v2, v0

    iget v3, p1, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v3, p2

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v2, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v2, v0

    iget v3, p1, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v3, p2

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v2, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v2, v0

    iget v3, p1, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v3, p2

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v2, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v2, v0

    iget v3, p1, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v3, p2

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/jme3/math/Quaternion;->w:F

    :goto_0
    invoke-virtual {p0}, Lcom/jme3/math/Quaternion;->normalizeLocal()V

    return-void

    :cond_0
    iget v2, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v2, v0

    iget v3, p1, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    iput v2, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v2, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v2, v0

    iget v3, p1, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    iput v2, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v2, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v2, v0

    iget v3, p1, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    iput v2, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v2, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v2, v0

    iget v3, p1, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    iput v2, p0, Lcom/jme3/math/Quaternion;->w:F

    goto :goto_0
.end method

.method public norm()F
    .locals 3

    iget v0, p0, Lcom/jme3/math/Quaternion;->w:F

    iget v1, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v2, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v2, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v2, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public normalize()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/math/Quaternion;->norm()F

    move-result v1

    invoke-static {v1}, Lcom/jme3/math/FastMath;->invSqrt(F)F

    move-result v0

    iget v1, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v1, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v1, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v1, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/jme3/math/Quaternion;->w:F

    return-void
.end method

.method public normalizeLocal()V
    .locals 2

    invoke-virtual {p0}, Lcom/jme3/math/Quaternion;->norm()F

    move-result v1

    invoke-static {v1}, Lcom/jme3/math/FastMath;->invSqrt(F)F

    move-result v0

    iget v1, p0, Lcom/jme3/math/Quaternion;->x:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v1, p0, Lcom/jme3/math/Quaternion;->y:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v1, p0, Lcom/jme3/math/Quaternion;->z:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v1, p0, Lcom/jme3/math/Quaternion;->w:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/jme3/math/Quaternion;->w:F

    return-void
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 3
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v0

    const-string v1, "x"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Quaternion;->x:F

    const-string v1, "y"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Quaternion;->y:F

    const-string v1, "z"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Quaternion;->z:F

    const-string v1, "w"

    const/high16 v2, 0x3f800000

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Quaternion;->w:F

    return-void
.end method

.method public set(FFFF)Lcom/jme3/math/Quaternion;
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F

    iput p1, p0, Lcom/jme3/math/Quaternion;->x:F

    iput p2, p0, Lcom/jme3/math/Quaternion;->y:F

    iput p3, p0, Lcom/jme3/math/Quaternion;->z:F

    iput p4, p0, Lcom/jme3/math/Quaternion;->w:F

    return-object p0
.end method

.method public set(Lcom/jme3/math/Quaternion;)Lcom/jme3/math/Quaternion;
    .locals 1
    .param p1    # Lcom/jme3/math/Quaternion;

    iget v0, p1, Lcom/jme3/math/Quaternion;->x:F

    iput v0, p0, Lcom/jme3/math/Quaternion;->x:F

    iget v0, p1, Lcom/jme3/math/Quaternion;->y:F

    iput v0, p0, Lcom/jme3/math/Quaternion;->y:F

    iget v0, p1, Lcom/jme3/math/Quaternion;->z:F

    iput v0, p0, Lcom/jme3/math/Quaternion;->z:F

    iget v0, p1, Lcom/jme3/math/Quaternion;->w:F

    iput v0, p0, Lcom/jme3/math/Quaternion;->w:F

    return-object p0
.end method

.method public toRotationMatrix(Lcom/jme3/math/Matrix3f;)Lcom/jme3/math/Matrix3f;
    .locals 17
    .param p1    # Lcom/jme3/math/Matrix3f;

    invoke-virtual/range {p0 .. p0}, Lcom/jme3/math/Quaternion;->norm()F

    move-result v1

    const/high16 v15, 0x3f800000

    cmpl-float v15, v1, v15

    if-nez v15, :cond_0

    const/high16 v2, 0x40000000

    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->x:F

    mul-float v3, v15, v2

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->y:F

    mul-float v8, v15, v2

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->z:F

    mul-float v12, v15, v2

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->x:F

    mul-float v5, v15, v3

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->x:F

    mul-float v6, v15, v8

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->x:F

    mul-float v7, v15, v12

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->w:F

    mul-float v4, v15, v3

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->y:F

    mul-float v10, v15, v8

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->y:F

    mul-float v11, v15, v12

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->w:F

    mul-float v9, v15, v8

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->z:F

    mul-float v14, v15, v12

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->w:F

    mul-float v13, v15, v12

    const/high16 v15, 0x3f800000

    add-float v16, v10, v14

    sub-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix3f;->m00:F

    sub-float v15, v6, v13

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix3f;->m01:F

    add-float v15, v7, v9

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix3f;->m02:F

    add-float v15, v6, v13

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix3f;->m10:F

    const/high16 v15, 0x3f800000

    add-float v16, v5, v14

    sub-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix3f;->m11:F

    sub-float v15, v11, v4

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix3f;->m12:F

    sub-float v15, v7, v9

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix3f;->m20:F

    add-float v15, v11, v4

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix3f;->m21:F

    const/high16 v15, 0x3f800000

    add-float v16, v5, v10

    sub-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix3f;->m22:F

    return-object p1

    :cond_0
    const/4 v15, 0x0

    cmpl-float v15, v1, v15

    if-lez v15, :cond_1

    const/high16 v15, 0x40000000

    div-float v2, v15, v1

    goto/16 :goto_0

    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public toRotationMatrix(Lcom/jme3/math/Matrix4f;)Lcom/jme3/math/Matrix4f;
    .locals 17
    .param p1    # Lcom/jme3/math/Matrix4f;

    invoke-virtual/range {p0 .. p0}, Lcom/jme3/math/Quaternion;->norm()F

    move-result v1

    const/high16 v15, 0x3f800000

    cmpl-float v15, v1, v15

    if-nez v15, :cond_0

    const/high16 v2, 0x40000000

    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->x:F

    mul-float v3, v15, v2

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->y:F

    mul-float v8, v15, v2

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->z:F

    mul-float v12, v15, v2

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->x:F

    mul-float v5, v15, v3

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->x:F

    mul-float v6, v15, v8

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->x:F

    mul-float v7, v15, v12

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->w:F

    mul-float v4, v15, v3

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->y:F

    mul-float v10, v15, v8

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->y:F

    mul-float v11, v15, v12

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->w:F

    mul-float v9, v15, v8

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->z:F

    mul-float v14, v15, v12

    move-object/from16 v0, p0

    iget v15, v0, Lcom/jme3/math/Quaternion;->w:F

    mul-float v13, v15, v12

    const/high16 v15, 0x3f800000

    add-float v16, v10, v14

    sub-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m00:F

    sub-float v15, v6, v13

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m01:F

    add-float v15, v7, v9

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m02:F

    add-float v15, v6, v13

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m10:F

    const/high16 v15, 0x3f800000

    add-float v16, v5, v14

    sub-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m11:F

    sub-float v15, v11, v4

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m12:F

    sub-float v15, v7, v9

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m20:F

    add-float v15, v11, v4

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m21:F

    const/high16 v15, 0x3f800000

    add-float v16, v5, v10

    sub-float v15, v15, v16

    move-object/from16 v0, p1

    iput v15, v0, Lcom/jme3/math/Matrix4f;->m22:F

    return-object p1

    :cond_0
    const/4 v15, 0x0

    cmpl-float v15, v1, v15

    if-lez v15, :cond_1

    const/high16 v15, 0x40000000

    div-float v2, v15, v1

    goto/16 :goto_0

    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/math/Quaternion;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/math/Quaternion;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/math/Quaternion;->z:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/math/Quaternion;->w:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
