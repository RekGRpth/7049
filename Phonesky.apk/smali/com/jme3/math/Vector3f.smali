.class public final Lcom/jme3/math/Vector3f;
.super Ljava/lang/Object;
.source "Vector3f.java"

# interfaces
.implements Lcom/jme3/export/Savable;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final NAN:Lcom/jme3/math/Vector3f;

.field public static final NEGATIVE_INFINITY:Lcom/jme3/math/Vector3f;

.field public static final POSITIVE_INFINITY:Lcom/jme3/math/Vector3f;

.field public static final UNIT_X:Lcom/jme3/math/Vector3f;

.field public static final UNIT_XYZ:Lcom/jme3/math/Vector3f;

.field public static final UNIT_Y:Lcom/jme3/math/Vector3f;

.field public static final UNIT_Z:Lcom/jme3/math/Vector3f;

.field public static final ZERO:Lcom/jme3/math/Vector3f;

.field private static final logger:Ljava/util/logging/Logger;

.field static final serialVersionUID:J = 0x1L


# instance fields
.field public x:F

.field public y:F

.field public z:F


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/high16 v5, 0x7fc00000

    const/high16 v4, 0x7f800000

    const/high16 v3, -0x800000

    const/high16 v2, 0x3f800000

    const/4 v1, 0x0

    const-class v0, Lcom/jme3/math/Vector3f;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/math/Vector3f;->logger:Ljava/util/logging/Logger;

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0, v1, v1, v1}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    sput-object v0, Lcom/jme3/math/Vector3f;->ZERO:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0, v5, v5, v5}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    sput-object v0, Lcom/jme3/math/Vector3f;->NAN:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0, v2, v1, v1}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    sput-object v0, Lcom/jme3/math/Vector3f;->UNIT_X:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0, v1, v2, v1}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    sput-object v0, Lcom/jme3/math/Vector3f;->UNIT_Y:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0, v1, v1, v2}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    sput-object v0, Lcom/jme3/math/Vector3f;->UNIT_Z:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0, v2, v2, v2}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    sput-object v0, Lcom/jme3/math/Vector3f;->UNIT_XYZ:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0, v4, v4, v4}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    sput-object v0, Lcom/jme3/math/Vector3f;->POSITIVE_INFINITY:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0, v3, v3, v3}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    sput-object v0, Lcom/jme3/math/Vector3f;->NEGATIVE_INFINITY:Lcom/jme3/math/Vector3f;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jme3/math/Vector3f;->z:F

    iput v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jme3/math/Vector3f;->x:F

    iput p2, p0, Lcom/jme3/math/Vector3f;->y:F

    iput p3, p0, Lcom/jme3/math/Vector3f;->z:F

    return-void
.end method

.method public constructor <init>(Lcom/jme3/math/Vector3f;)V
    .locals 0
    .param p1    # Lcom/jme3/math/Vector3f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    return-void
.end method

.method public static isValidVector(Lcom/jme3/math/Vector3f;)Z
    .locals 2
    .param p0    # Lcom/jme3/math/Vector3f;

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/jme3/math/Vector3f;->x:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/jme3/math/Vector3f;->y:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/jme3/math/Vector3f;->z:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/jme3/math/Vector3f;->x:F

    invoke-static {v1}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/jme3/math/Vector3f;->y:F

    invoke-static {v1}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/jme3/math/Vector3f;->z:F

    invoke-static {v1}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 5
    .param p1    # Lcom/jme3/math/Vector3f;

    if-nez p1, :cond_0

    sget-object v0, Lcom/jme3/math/Vector3f;->logger:Ljava/util/logging/Logger;

    const-string v1, "Provided vector is null, null returned."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/jme3/math/Vector3f;

    iget v1, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->x:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v3, p1, Lcom/jme3/math/Vector3f;->y:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Vector3f;->z:F

    iget v4, p1, Lcom/jme3/math/Vector3f;->z:F

    add-float/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    goto :goto_0
.end method

.method public add(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 2
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->x:F

    add-float/2addr v0, v1

    iput v0, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    add-float/2addr v0, v1

    iput v0, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->z:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->z:F

    add-float/2addr v0, v1

    iput v0, p2, Lcom/jme3/math/Vector3f;->z:F

    return-object p2
.end method

.method public addLocal(FFF)Lcom/jme3/math/Vector3f;
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->y:F

    add-float/2addr v0, p2

    iput v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->z:F

    add-float/2addr v0, p3

    iput v0, p0, Lcom/jme3/math/Vector3f;->z:F

    return-object p0
.end method

.method public addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 2
    .param p1    # Lcom/jme3/math/Vector3f;

    if-nez p1, :cond_0

    sget-object v0, Lcom/jme3/math/Vector3f;->logger:Ljava/util/logging/Logger;

    const-string v1, "Provided vector is null, null returned."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    const/4 p0, 0x0

    :goto_0
    return-object p0

    :cond_0
    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->z:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->z:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->z:F

    goto :goto_0
.end method

.method public angleBetween(Lcom/jme3/math/Vector3f;)F
    .locals 2
    .param p1    # Lcom/jme3/math/Vector3f;

    invoke-virtual {p0, p1}, Lcom/jme3/math/Vector3f;->dot(Lcom/jme3/math/Vector3f;)F

    move-result v1

    invoke-static {v1}, Lcom/jme3/math/FastMath;->acos(F)F

    move-result v0

    return v0
.end method

.method public clone()Lcom/jme3/math/Vector3f;
    .locals 2

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/math/Vector3f;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/math/Vector3f;->clone()Lcom/jme3/math/Vector3f;

    move-result-object v0

    return-object v0
.end method

.method public cross(FFFLcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 5
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # Lcom/jme3/math/Vector3f;

    if-nez p4, :cond_0

    new-instance p4, Lcom/jme3/math/Vector3f;

    invoke-direct {p4}, Lcom/jme3/math/Vector3f;-><init>()V

    :cond_0
    iget v3, p0, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v3, p3

    iget v4, p0, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v4, p2

    sub-float v0, v3, v4

    iget v3, p0, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v3, p1

    iget v4, p0, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v4, p3

    sub-float v1, v3, v4

    iget v3, p0, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v3, p2

    iget v4, p0, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v4, p1

    sub-float v2, v3, v4

    invoke-virtual {p4, v0, v1, v2}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    return-object p4
.end method

.method public cross(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 1
    .param p1    # Lcom/jme3/math/Vector3f;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/jme3/math/Vector3f;->cross(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v0

    return-object v0
.end method

.method public cross(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 3
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    iget v0, p1, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->z:F

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/jme3/math/Vector3f;->cross(FFFLcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v0

    return-object v0
.end method

.method public crossLocal(FFF)Lcom/jme3/math/Vector3f;
    .locals 4
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iget v2, p0, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v2, p3

    iget v3, p0, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v3, p2

    sub-float v0, v2, v3

    iget v2, p0, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v2, p1

    iget v3, p0, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v3, p3

    sub-float v1, v2, v3

    iget v2, p0, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v2, p2

    iget v3, p0, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v3, p1

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/jme3/math/Vector3f;->z:F

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iput v1, p0, Lcom/jme3/math/Vector3f;->y:F

    return-object p0
.end method

.method public crossLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 3
    .param p1    # Lcom/jme3/math/Vector3f;

    iget v0, p1, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->z:F

    invoke-virtual {p0, v0, v1, v2}, Lcom/jme3/math/Vector3f;->crossLocal(FFF)Lcom/jme3/math/Vector3f;

    move-result-object v0

    return-object v0
.end method

.method public distance(Lcom/jme3/math/Vector3f;)F
    .locals 1
    .param p1    # Lcom/jme3/math/Vector3f;

    invoke-virtual {p0, p1}, Lcom/jme3/math/Vector3f;->distanceSquared(Lcom/jme3/math/Vector3f;)F

    move-result v0

    invoke-static {v0}, Lcom/jme3/math/FastMath;->sqrt(F)F

    move-result v0

    return v0
.end method

.method public distanceSquared(Lcom/jme3/math/Vector3f;)F
    .locals 10
    .param p1    # Lcom/jme3/math/Vector3f;

    iget v6, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v7, p1, Lcom/jme3/math/Vector3f;->x:F

    sub-float/2addr v6, v7

    float-to-double v0, v6

    iget v6, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v7, p1, Lcom/jme3/math/Vector3f;->y:F

    sub-float/2addr v6, v7

    float-to-double v2, v6

    iget v6, p0, Lcom/jme3/math/Vector3f;->z:F

    iget v7, p1, Lcom/jme3/math/Vector3f;->z:F

    sub-float/2addr v6, v7

    float-to-double v4, v6

    mul-double v6, v0, v0

    mul-double v8, v2, v2

    add-double/2addr v6, v8

    mul-double v8, v4, v4

    add-double/2addr v6, v8

    double-to-float v6, v6

    return v6
.end method

.method public divideLocal(F)Lcom/jme3/math/Vector3f;
    .locals 1
    .param p1    # F

    const/high16 v0, 0x3f800000

    div-float p1, v0, p1

    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Vector3f;->z:F

    return-object p0
.end method

.method public divideLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 2
    .param p1    # Lcom/jme3/math/Vector3f;

    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->x:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->z:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->z:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->z:F

    return-object p0
.end method

.method public dot(Lcom/jme3/math/Vector3f;)F
    .locals 3
    .param p1    # Lcom/jme3/math/Vector3f;

    if-nez p1, :cond_0

    sget-object v0, Lcom/jme3/math/Vector3f;->logger:Ljava/util/logging/Logger;

    const-string v1, "Provided vector is null, 0 returned."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/jme3/math/Vector3f;->z:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v2, 0x1

    const/4 v1, 0x0

    instance-of v3, p1, Lcom/jme3/math/Vector3f;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-ne p0, p1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/jme3/math/Vector3f;

    iget v3, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v4, v0, Lcom/jme3/math/Vector3f;->x:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-nez v3, :cond_0

    iget v3, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v4, v0, Lcom/jme3/math/Vector3f;->y:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-nez v3, :cond_0

    iget v3, p0, Lcom/jme3/math/Vector3f;->z:F

    iget v4, v0, Lcom/jme3/math/Vector3f;->z:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public get(I)F
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "index must be either 0, 1 or 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    :goto_0
    return v0

    :pswitch_1
    iget v0, p0, Lcom/jme3/math/Vector3f;->y:F

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/jme3/math/Vector3f;->z:F

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getX()F
    .locals 1

    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    return v0
.end method

.method public getY()F
    .locals 1

    iget v0, p0, Lcom/jme3/math/Vector3f;->y:F

    return v0
.end method

.method public getZ()F
    .locals 1

    iget v0, p0, Lcom/jme3/math/Vector3f;->z:F

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x25

    iget v1, p0, Lcom/jme3/math/Vector3f;->x:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/lit16 v1, v1, 0x559

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Vector3f;->y:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Vector3f;->z:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public interpolate(Lcom/jme3/math/Vector3f;F)Lcom/jme3/math/Vector3f;
    .locals 3
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # F

    const/high16 v2, 0x3f800000

    sub-float v0, v2, p2

    iget v1, p0, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v0, v1

    iget v1, p1, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    sub-float v0, v2, p2

    iget v1, p0, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v0, v1

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->y:F

    sub-float v0, v2, p2

    iget v1, p0, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->z:F

    return-object p0
.end method

.method public interpolate(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;F)Lcom/jme3/math/Vector3f;
    .locals 3
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;
    .param p3    # F

    const/high16 v2, 0x3f800000

    sub-float v0, v2, p3

    iget v1, p1, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v0, v1

    iget v1, p2, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    sub-float v0, v2, p3

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v0, v1

    iget v1, p2, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->y:F

    sub-float v0, v2, p3

    iget v1, p1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v0, v1

    iget v1, p2, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->z:F

    return-object p0
.end method

.method public isUnitVector()Z
    .locals 2

    invoke-virtual {p0}, Lcom/jme3/math/Vector3f;->length()F

    move-result v0

    const v1, 0x3f7d70a4

    cmpg-float v1, v1, v0

    if-gez v1, :cond_0

    const v1, 0x3f8147ae

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public length()F
    .locals 1

    invoke-virtual {p0}, Lcom/jme3/math/Vector3f;->lengthSquared()F

    move-result v0

    invoke-static {v0}, Lcom/jme3/math/FastMath;->sqrt(F)F

    move-result v0

    return v0
.end method

.method public lengthSquared()F
    .locals 3

    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p0, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p0, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/jme3/math/Vector3f;->z:F

    iget v2, p0, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public mult(F)Lcom/jme3/math/Vector3f;
    .locals 4
    .param p1    # F

    new-instance v0, Lcom/jme3/math/Vector3f;

    iget v1, p0, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v1, p1

    iget v2, p0, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v2, p1

    iget v3, p0, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    return-object v0
.end method

.method public mult(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 4
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    if-nez p1, :cond_0

    sget-object v0, Lcom/jme3/math/Vector3f;->logger:Ljava/util/logging/Logger;

    const-string v1, "Provided vector is null, null returned."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    if-nez p2, :cond_1

    new-instance p2, Lcom/jme3/math/Vector3f;

    invoke-direct {p2}, Lcom/jme3/math/Vector3f;-><init>()V

    :cond_1
    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/jme3/math/Vector3f;->z:F

    iget v3, p1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v2, v3

    invoke-virtual {p2, v0, v1, v2}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    move-result-object v0

    goto :goto_0
.end method

.method public multLocal(F)Lcom/jme3/math/Vector3f;
    .locals 1
    .param p1    # F

    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Vector3f;->z:F

    return-object p0
.end method

.method public multLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 2
    .param p1    # Lcom/jme3/math/Vector3f;

    if-nez p1, :cond_0

    sget-object v0, Lcom/jme3/math/Vector3f;->logger:Ljava/util/logging/Logger;

    const-string v1, "Provided vector is null, null returned."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    const/4 p0, 0x0

    :goto_0
    return-object p0

    :cond_0
    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->z:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->z:F

    goto :goto_0
.end method

.method public negateLocal()Lcom/jme3/math/Vector3f;
    .locals 1

    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    neg-float v0, v0

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->y:F

    neg-float v0, v0

    iput v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->z:F

    neg-float v0, v0

    iput v0, p0, Lcom/jme3/math/Vector3f;->z:F

    return-object p0
.end method

.method public normalize()Lcom/jme3/math/Vector3f;
    .locals 5

    const/high16 v4, 0x3f800000

    iget v1, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v2, p0, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v3, p0, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/jme3/math/Vector3f;->z:F

    iget v3, p0, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v2, v3

    add-float v0, v1, v2

    cmpl-float v1, v0, v4

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/jme3/math/FastMath;->sqrt(F)F

    move-result v1

    div-float v0, v4, v1

    new-instance v1, Lcom/jme3/math/Vector3f;

    iget v2, p0, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v2, v0

    iget v3, p0, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v4, v0

    invoke-direct {v1, v2, v3, v4}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/jme3/math/Vector3f;->clone()Lcom/jme3/math/Vector3f;

    move-result-object v1

    goto :goto_0
.end method

.method public normalizeLocal()Lcom/jme3/math/Vector3f;
    .locals 5

    const/high16 v4, 0x3f800000

    iget v1, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v2, p0, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v3, p0, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/jme3/math/Vector3f;->z:F

    iget v3, p0, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v2, v3

    add-float v0, v1, v2

    cmpl-float v1, v0, v4

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/jme3/math/FastMath;->sqrt(F)F

    move-result v1

    div-float v0, v4, v1

    iget v1, p0, Lcom/jme3/math/Vector3f;->x:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p0, Lcom/jme3/math/Vector3f;->y:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v1, p0, Lcom/jme3/math/Vector3f;->z:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/jme3/math/Vector3f;->z:F

    :cond_0
    return-object p0
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 3
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v0

    const-string v1, "x"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Vector3f;->x:F

    const-string v1, "y"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Vector3f;->y:F

    const-string v1, "z"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Vector3f;->z:F

    return-void
.end method

.method public set(FFF)Lcom/jme3/math/Vector3f;
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iput p1, p0, Lcom/jme3/math/Vector3f;->x:F

    iput p2, p0, Lcom/jme3/math/Vector3f;->y:F

    iput p3, p0, Lcom/jme3/math/Vector3f;->z:F

    return-object p0
.end method

.method public set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 1
    .param p1    # Lcom/jme3/math/Vector3f;

    iget v0, p1, Lcom/jme3/math/Vector3f;->x:F

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v0, p1, Lcom/jme3/math/Vector3f;->y:F

    iput v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v0, p1, Lcom/jme3/math/Vector3f;->z:F

    iput v0, p0, Lcom/jme3/math/Vector3f;->z:F

    return-object p0
.end method

.method public set(IF)V
    .locals 2
    .param p1    # I
    .param p2    # F

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "index must be either 0, 1 or 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iput p2, p0, Lcom/jme3/math/Vector3f;->x:F

    :goto_0
    return-void

    :pswitch_1
    iput p2, p0, Lcom/jme3/math/Vector3f;->y:F

    goto :goto_0

    :pswitch_2
    iput p2, p0, Lcom/jme3/math/Vector3f;->z:F

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setX(F)Lcom/jme3/math/Vector3f;
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/jme3/math/Vector3f;->x:F

    return-object p0
.end method

.method public setY(F)Lcom/jme3/math/Vector3f;
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/jme3/math/Vector3f;->y:F

    return-object p0
.end method

.method public subtract(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 5
    .param p1    # Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Vector3f;

    iget v1, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v2, p1, Lcom/jme3/math/Vector3f;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v3, p1, Lcom/jme3/math/Vector3f;->y:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/jme3/math/Vector3f;->z:F

    iget v4, p1, Lcom/jme3/math/Vector3f;->z:F

    sub-float/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    return-object v0
.end method

.method public subtract(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 2
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    if-nez p2, :cond_0

    new-instance p2, Lcom/jme3/math/Vector3f;

    invoke-direct {p2}, Lcom/jme3/math/Vector3f;-><init>()V

    :cond_0
    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->x:F

    sub-float/2addr v0, v1

    iput v0, p2, Lcom/jme3/math/Vector3f;->x:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    sub-float/2addr v0, v1

    iput v0, p2, Lcom/jme3/math/Vector3f;->y:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->z:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->z:F

    sub-float/2addr v0, v1

    iput v0, p2, Lcom/jme3/math/Vector3f;->z:F

    return-object p2
.end method

.method public subtractLocal(FFF)Lcom/jme3/math/Vector3f;
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    sub-float/2addr v0, p1

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->y:F

    sub-float/2addr v0, p2

    iput v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->z:F

    sub-float/2addr v0, p3

    iput v0, p0, Lcom/jme3/math/Vector3f;->z:F

    return-object p0
.end method

.method public subtractLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 2
    .param p1    # Lcom/jme3/math/Vector3f;

    if-nez p1, :cond_0

    sget-object v0, Lcom/jme3/math/Vector3f;->logger:Ljava/util/logging/Logger;

    const-string v1, "Provided vector is null, null returned."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    const/4 p0, 0x0

    :goto_0
    return-object p0

    :cond_0
    iget v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->x:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->y:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->y:F

    iget v0, p0, Lcom/jme3/math/Vector3f;->z:F

    iget v1, p1, Lcom/jme3/math/Vector3f;->z:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/math/Vector3f;->z:F

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/math/Vector3f;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/math/Vector3f;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/math/Vector3f;->z:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
