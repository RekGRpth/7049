.class public final Lcom/jme3/math/Vector4f;
.super Ljava/lang/Object;
.source "Vector4f.java"

# interfaces
.implements Lcom/jme3/export/Savable;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final NAN:Lcom/jme3/math/Vector4f;

.field public static final NEGATIVE_INFINITY:Lcom/jme3/math/Vector4f;

.field public static final POSITIVE_INFINITY:Lcom/jme3/math/Vector4f;

.field public static final UNIT_W:Lcom/jme3/math/Vector4f;

.field public static final UNIT_X:Lcom/jme3/math/Vector4f;

.field public static final UNIT_XYZW:Lcom/jme3/math/Vector4f;

.field public static final UNIT_Y:Lcom/jme3/math/Vector4f;

.field public static final UNIT_Z:Lcom/jme3/math/Vector4f;

.field public static final ZERO:Lcom/jme3/math/Vector4f;

.field private static final logger:Ljava/util/logging/Logger;

.field static final serialVersionUID:J = 0x1L


# instance fields
.field public w:F

.field public x:F

.field public y:F

.field public z:F


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/high16 v5, 0x7fc00000

    const/high16 v4, 0x7f800000

    const/high16 v3, -0x800000

    const/high16 v2, 0x3f800000

    const/4 v1, 0x0

    const-class v0, Lcom/jme3/math/Vector4f;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/math/Vector4f;->logger:Ljava/util/logging/Logger;

    new-instance v0, Lcom/jme3/math/Vector4f;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/jme3/math/Vector4f;-><init>(FFFF)V

    sput-object v0, Lcom/jme3/math/Vector4f;->ZERO:Lcom/jme3/math/Vector4f;

    new-instance v0, Lcom/jme3/math/Vector4f;

    invoke-direct {v0, v5, v5, v5, v5}, Lcom/jme3/math/Vector4f;-><init>(FFFF)V

    sput-object v0, Lcom/jme3/math/Vector4f;->NAN:Lcom/jme3/math/Vector4f;

    new-instance v0, Lcom/jme3/math/Vector4f;

    invoke-direct {v0, v2, v1, v1, v1}, Lcom/jme3/math/Vector4f;-><init>(FFFF)V

    sput-object v0, Lcom/jme3/math/Vector4f;->UNIT_X:Lcom/jme3/math/Vector4f;

    new-instance v0, Lcom/jme3/math/Vector4f;

    invoke-direct {v0, v1, v2, v1, v1}, Lcom/jme3/math/Vector4f;-><init>(FFFF)V

    sput-object v0, Lcom/jme3/math/Vector4f;->UNIT_Y:Lcom/jme3/math/Vector4f;

    new-instance v0, Lcom/jme3/math/Vector4f;

    invoke-direct {v0, v1, v1, v2, v1}, Lcom/jme3/math/Vector4f;-><init>(FFFF)V

    sput-object v0, Lcom/jme3/math/Vector4f;->UNIT_Z:Lcom/jme3/math/Vector4f;

    new-instance v0, Lcom/jme3/math/Vector4f;

    invoke-direct {v0, v1, v1, v1, v2}, Lcom/jme3/math/Vector4f;-><init>(FFFF)V

    sput-object v0, Lcom/jme3/math/Vector4f;->UNIT_W:Lcom/jme3/math/Vector4f;

    new-instance v0, Lcom/jme3/math/Vector4f;

    invoke-direct {v0, v2, v2, v2, v2}, Lcom/jme3/math/Vector4f;-><init>(FFFF)V

    sput-object v0, Lcom/jme3/math/Vector4f;->UNIT_XYZW:Lcom/jme3/math/Vector4f;

    new-instance v0, Lcom/jme3/math/Vector4f;

    invoke-direct {v0, v4, v4, v4, v4}, Lcom/jme3/math/Vector4f;-><init>(FFFF)V

    sput-object v0, Lcom/jme3/math/Vector4f;->POSITIVE_INFINITY:Lcom/jme3/math/Vector4f;

    new-instance v0, Lcom/jme3/math/Vector4f;

    invoke-direct {v0, v3, v3, v3, v3}, Lcom/jme3/math/Vector4f;-><init>(FFFF)V

    sput-object v0, Lcom/jme3/math/Vector4f;->NEGATIVE_INFINITY:Lcom/jme3/math/Vector4f;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jme3/math/Vector4f;->w:F

    iput v0, p0, Lcom/jme3/math/Vector4f;->z:F

    iput v0, p0, Lcom/jme3/math/Vector4f;->y:F

    iput v0, p0, Lcom/jme3/math/Vector4f;->x:F

    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jme3/math/Vector4f;->x:F

    iput p2, p0, Lcom/jme3/math/Vector4f;->y:F

    iput p3, p0, Lcom/jme3/math/Vector4f;->z:F

    iput p4, p0, Lcom/jme3/math/Vector4f;->w:F

    return-void
.end method


# virtual methods
.method public clone()Lcom/jme3/math/Vector4f;
    .locals 2

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/math/Vector4f;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/math/Vector4f;->clone()Lcom/jme3/math/Vector4f;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v2, 0x1

    const/4 v1, 0x0

    instance-of v3, p1, Lcom/jme3/math/Vector4f;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-ne p0, p1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/jme3/math/Vector4f;

    iget v3, p0, Lcom/jme3/math/Vector4f;->x:F

    iget v4, v0, Lcom/jme3/math/Vector4f;->x:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-nez v3, :cond_0

    iget v3, p0, Lcom/jme3/math/Vector4f;->y:F

    iget v4, v0, Lcom/jme3/math/Vector4f;->y:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-nez v3, :cond_0

    iget v3, p0, Lcom/jme3/math/Vector4f;->z:F

    iget v4, v0, Lcom/jme3/math/Vector4f;->z:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-nez v3, :cond_0

    iget v3, p0, Lcom/jme3/math/Vector4f;->w:F

    iget v4, v0, Lcom/jme3/math/Vector4f;->w:F

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getW()F
    .locals 1

    iget v0, p0, Lcom/jme3/math/Vector4f;->w:F

    return v0
.end method

.method public getX()F
    .locals 1

    iget v0, p0, Lcom/jme3/math/Vector4f;->x:F

    return v0
.end method

.method public getY()F
    .locals 1

    iget v0, p0, Lcom/jme3/math/Vector4f;->y:F

    return v0
.end method

.method public getZ()F
    .locals 1

    iget v0, p0, Lcom/jme3/math/Vector4f;->z:F

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x25

    iget v1, p0, Lcom/jme3/math/Vector4f;->x:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/lit16 v1, v1, 0x559

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Vector4f;->y:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Vector4f;->z:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x25

    iget v2, p0, Lcom/jme3/math/Vector4f;->w:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 3
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v0

    const-string v1, "x"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Vector4f;->x:F

    const-string v1, "y"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Vector4f;->y:F

    const-string v1, "z"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Vector4f;->z:F

    const-string v1, "w"

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/math/Vector4f;->w:F

    return-void
.end method

.method public set(FFFF)Lcom/jme3/math/Vector4f;
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F

    iput p1, p0, Lcom/jme3/math/Vector4f;->x:F

    iput p2, p0, Lcom/jme3/math/Vector4f;->y:F

    iput p3, p0, Lcom/jme3/math/Vector4f;->z:F

    iput p4, p0, Lcom/jme3/math/Vector4f;->w:F

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/math/Vector4f;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/math/Vector4f;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/math/Vector4f;->z:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/math/Vector4f;->w:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
