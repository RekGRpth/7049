.class public Lcom/jme3/renderer/android/OGLESShaderRenderer;
.super Ljava/lang/Object;
.source "OGLESShaderRenderer.java"

# interfaces
.implements Lcom/jme3/renderer/Renderer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/renderer/android/OGLESShaderRenderer$1;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private boundShader:Lcom/jme3/shader/Shader;

.field private final caps:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/jme3/renderer/Caps;",
            ">;"
        }
    .end annotation
.end field

.field private checkErrors:Z

.field private clipH:I

.field private clipW:I

.field private clipX:I

.field private clipY:I

.field private final context:Lcom/jme3/renderer/RenderContext;

.field private fragTextureUnits:I

.field private glslVer:I

.field private final intBuf1:Ljava/nio/IntBuffer;

.field private final intBuf16:Ljava/nio/IntBuffer;

.field private lastFb:Lcom/jme3/texture/FrameBuffer;

.field private maxCubeTexSize:I

.field private maxTexSize:I

.field private final nameBuf:Ljava/nio/ByteBuffer;

.field private final objManager:Lcom/jme3/util/NativeObjectManager;

.field private powerOf2:Z

.field private powerVr:Z

.field private final statistics:Lcom/jme3/renderer/Statistics;

.field private final stringBuf:Ljava/lang/StringBuilder;

.field private tdc:Z

.field private useVBO:Z

.field private verboseLogging:Z

.field private vertexAttribs:I

.field private vertexTextureUnits:I

.field private vpH:I

.field private vpW:I

.field private vpX:I

.field private vpY:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->$assertionsDisabled:Z

    const-class v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 4

    const/16 v3, 0xfa

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {v3}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->nameBuf:Ljava/nio/ByteBuffer;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->stringBuf:Ljava/lang/StringBuilder;

    invoke-static {v2}, Lcom/jme3/util/BufferUtils;->createIntBuffer(I)Ljava/nio/IntBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/jme3/util/BufferUtils;->createIntBuffer(I)Ljava/nio/IntBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    new-instance v0, Lcom/jme3/renderer/RenderContext;

    invoke-direct {v0}, Lcom/jme3/renderer/RenderContext;-><init>()V

    iput-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    new-instance v0, Lcom/jme3/util/NativeObjectManager;

    invoke-direct {v0}, Lcom/jme3/util/NativeObjectManager;-><init>()V

    iput-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->objManager:Lcom/jme3/util/NativeObjectManager;

    const-class v0, Lcom/jme3/renderer/Caps;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->caps:Ljava/util/EnumSet;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->lastFb:Lcom/jme3/texture/FrameBuffer;

    new-instance v0, Lcom/jme3/renderer/Statistics;

    invoke-direct {v0}, Lcom/jme3/renderer/Statistics;-><init>()V

    iput-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    iput-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->powerVr:Z

    iput-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->powerOf2:Z

    iput-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    iput-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->useVBO:Z

    iput-boolean v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkErrors:Z

    return-void
.end method

.method private checkGLError()V
    .locals 4

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkErrors:Z

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/jme3/renderer/RendererException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OpenGL Error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/jme3/renderer/RendererException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private convertFormat(Lcom/jme3/scene/VertexBuffer$Format;)I
    .locals 2

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer$1;->$SwitchMap$com$jme3$scene$VertexBuffer$Format:[I

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer$Format;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown buffer format."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/16 v0, 0x1400

    :goto_0
    return v0

    :pswitch_1
    const/16 v0, 0x1401

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x1402

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x1403

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x1404

    goto :goto_0

    :pswitch_5
    const/16 v0, 0x1405

    goto :goto_0

    :pswitch_6
    const/16 v0, 0x1406

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private convertMagFilter(Lcom/jme3/texture/Texture$MagFilter;)I
    .locals 3

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer$1;->$SwitchMap$com$jme3$texture$Texture$MagFilter:[I

    invoke-virtual {p1}, Lcom/jme3/texture/Texture$MagFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown mag filter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/16 v0, 0x2601

    :goto_0
    return v0

    :pswitch_1
    const/16 v0, 0x2600

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private convertMinFilter(Lcom/jme3/texture/Texture$MinFilter;)I
    .locals 3

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer$1;->$SwitchMap$com$jme3$texture$Texture$MinFilter:[I

    invoke-virtual {p1}, Lcom/jme3/texture/Texture$MinFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown min filter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/16 v0, 0x2703

    :goto_0
    return v0

    :pswitch_1
    const/16 v0, 0x2701

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x2702

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x2700

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x2601

    goto :goto_0

    :pswitch_5
    const/16 v0, 0x2600

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private convertTextureType(Lcom/jme3/texture/Texture$Type;)I
    .locals 3

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer$1;->$SwitchMap$com$jme3$texture$Texture$Type:[I

    invoke-virtual {p1}, Lcom/jme3/texture/Texture$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown texture type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/16 v0, 0xde1

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x8513

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private convertUsage(Lcom/jme3/scene/VertexBuffer$Usage;)I
    .locals 2

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer$1;->$SwitchMap$com$jme3$scene$VertexBuffer$Usage:[I

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer$Usage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown usage type."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const v0, 0x88e4

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x88e8

    goto :goto_0

    :pswitch_2
    const v0, 0x88e0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private convertWrapMode(Lcom/jme3/texture/Texture$WrapMode;)I
    .locals 3

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer$1;->$SwitchMap$com$jme3$texture$Texture$WrapMode:[I

    invoke-virtual {p1}, Lcom/jme3/texture/Texture$WrapMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown wrap mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const v0, 0x812f

    :goto_0
    return v0

    :pswitch_1
    const/16 v0, 0x2901

    goto :goto_0

    :pswitch_2
    const v0, 0x8370

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private renderMeshDefault(Lcom/jme3/scene/Mesh;II)V
    .locals 11

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "renderMeshDefault({0}, {1}, {2})"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v10

    invoke-virtual {v0, v1, v2, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->InterleavedData:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p1, v0}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/jme3/scene/VertexBuffer;->isUpdateNeeded()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v4}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->updateBufferData(Lcom/jme3/scene/VertexBuffer;)V

    :cond_1
    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getNumLodLevels()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {p1, p2}, Lcom/jme3/scene/Mesh;->getLodLevel(I)Lcom/jme3/scene/VertexBuffer;

    move-result-object v0

    move-object v1, v0

    :goto_0
    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getBufferList()Lcom/jme3/util/SafeArrayList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jme3/util/SafeArrayList;->getArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jme3/scene/VertexBuffer;

    array-length v5, v0

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_6

    aget-object v6, v0, v2

    invoke-virtual {v6}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v7

    sget-object v8, Lcom/jme3/scene/VertexBuffer$Type;->InterleavedData:Lcom/jme3/scene/VertexBuffer$Type;

    if-eq v7, v8, :cond_2

    invoke-virtual {v6}, Lcom/jme3/scene/VertexBuffer;->getUsage()Lcom/jme3/scene/VertexBuffer$Usage;

    move-result-object v7

    sget-object v8, Lcom/jme3/scene/VertexBuffer$Usage;->CpuOnly:Lcom/jme3/scene/VertexBuffer$Usage;

    if-eq v7, v8, :cond_2

    invoke-virtual {v6}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v7

    sget-object v8, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    if-ne v7, v8, :cond_4

    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p1, v0}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_4
    invoke-virtual {v6}, Lcom/jme3/scene/VertexBuffer;->getStride()I

    move-result v7

    if-nez v7, :cond_5

    invoke-virtual {p0, v6}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->setVertexAttrib(Lcom/jme3/scene/VertexBuffer;)V

    goto :goto_2

    :cond_5
    invoke-virtual {p0, v6, v4}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->setVertexAttrib(Lcom/jme3/scene/VertexBuffer;Lcom/jme3/scene/VertexBuffer;)V

    goto :goto_2

    :cond_6
    if-eqz v1, :cond_7

    invoke-virtual {p0, v1, p1, p3}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->drawTriangleList(Lcom/jme3/scene/VertexBuffer;Lcom/jme3/scene/Mesh;I)V

    :goto_3
    invoke-virtual {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->clearVertexAttribs()V

    invoke-virtual {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->clearTextureUnits()V

    return-void

    :cond_7
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_8

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "GLES20.glDrawArrays({0}, 0, {1})"

    new-array v4, v10, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getMode()Lcom/jme3/scene/Mesh$Mode;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertElementMode(Lcom/jme3/scene/Mesh$Mode;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getVertexCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-virtual {v0, v1, v2, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_8
    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getMode()Lcom/jme3/scene/Mesh$Mode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertElementMode(Lcom/jme3/scene/Mesh$Mode;)I

    move-result v0

    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getVertexCount()I

    move-result v1

    invoke-static {v0, v3, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    goto :goto_3
.end method

.method private renderMeshVertexArray(Lcom/jme3/scene/Mesh;II)V
    .locals 7

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "renderMeshVertexArray"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getBufferList()Lcom/jme3/util/SafeArrayList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jme3/util/SafeArrayList;->getArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jme3/scene/VertexBuffer;

    array-length v3, v0

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v0, v1

    invoke-virtual {v4}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v5

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Type;->InterleavedData:Lcom/jme3/scene/VertexBuffer$Type;

    if-eq v5, v6, :cond_1

    invoke-virtual {v4}, Lcom/jme3/scene/VertexBuffer;->getUsage()Lcom/jme3/scene/VertexBuffer$Usage;

    move-result-object v5

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Usage;->CpuOnly:Lcom/jme3/scene/VertexBuffer$Usage;

    if-eq v5, v6, :cond_1

    invoke-virtual {v4}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v5

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    if-ne v5, v6, :cond_2

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Lcom/jme3/scene/VertexBuffer;->getStride()I

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {p0, v4}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->setVertexAttrib_Array(Lcom/jme3/scene/VertexBuffer;)V

    goto :goto_1

    :cond_3
    sget-object v5, Lcom/jme3/scene/VertexBuffer$Type;->InterleavedData:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p1, v5}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->setVertexAttrib_Array(Lcom/jme3/scene/VertexBuffer;Lcom/jme3/scene/VertexBuffer;)V

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getNumLodLevels()I

    move-result v0

    if-lez v0, :cond_5

    invoke-virtual {p1, p2}, Lcom/jme3/scene/Mesh;->getLodLevel(I)Lcom/jme3/scene/VertexBuffer;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_6

    invoke-virtual {p0, v0, p1, p3}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->drawTriangleList_Array(Lcom/jme3/scene/VertexBuffer;Lcom/jme3/scene/Mesh;I)V

    :goto_3
    invoke-virtual {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->clearVertexAttribs()V

    invoke-virtual {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->clearTextureUnits()V

    return-void

    :cond_5
    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p1, v0}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v0

    goto :goto_2

    :cond_6
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_7

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v3, "GLES20.glDrawArrays({0}, {1}, {2})"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getMode()Lcom/jme3/scene/Mesh$Mode;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v5, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getVertexCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v1, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_7
    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getMode()Lcom/jme3/scene/Mesh$Mode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertElementMode(Lcom/jme3/scene/Mesh$Mode;)I

    move-result v0

    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getVertexCount()I

    move-result v1

    invoke-static {v0, v2, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    goto :goto_3
.end method

.method private setupTextureParams(Lcom/jme3/texture/Texture;)V
    .locals 6

    invoke-virtual {p1}, Lcom/jme3/texture/Texture;->getType()Lcom/jme3/texture/Texture$Type;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertTextureType(Lcom/jme3/texture/Texture$Type;)I

    move-result v0

    invoke-virtual {p1}, Lcom/jme3/texture/Texture;->getMinFilter()Lcom/jme3/texture/Texture$MinFilter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertMinFilter(Lcom/jme3/texture/Texture$MinFilter;)I

    move-result v1

    invoke-virtual {p1}, Lcom/jme3/texture/Texture;->getMagFilter()Lcom/jme3/texture/Texture$MagFilter;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertMagFilter(Lcom/jme3/texture/Texture$MagFilter;)I

    move-result v2

    iget-boolean v3, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v3, :cond_0

    sget-object v3, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glTexParameteri("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", GLES20.GL_TEXTURE_MIN_FILTER, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_0
    const/16 v3, 0x2801

    invoke-static {v0, v3, v1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GLES20.glTexParameteri("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", GLES20.GL_TEXTURE_MAG_FILTER, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_1
    const/16 v1, 0x2800

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer$1;->$SwitchMap$com$jme3$texture$Texture$Type:[I

    invoke-virtual {p1}, Lcom/jme3/texture/Texture;->getType()Lcom/jme3/texture/Texture$Type;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jme3/texture/Texture$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown texture type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jme3/texture/Texture;->getType()Lcom/jme3/texture/Texture$Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLES20.glTexParameteri("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", GLES20.GL_TEXTURE_WRAP_T, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/jme3/texture/Texture$WrapAxis;->T:Lcom/jme3/texture/Texture$WrapAxis;

    invoke-virtual {p1, v3}, Lcom/jme3/texture/Texture;->getWrap(Lcom/jme3/texture/Texture$WrapAxis;)Lcom/jme3/texture/Texture$WrapMode;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertWrapMode(Lcom/jme3/texture/Texture$WrapMode;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_2
    const/16 v1, 0x2803

    sget-object v2, Lcom/jme3/texture/Texture$WrapAxis;->T:Lcom/jme3/texture/Texture$WrapAxis;

    invoke-virtual {p1, v2}, Lcom/jme3/texture/Texture;->getWrap(Lcom/jme3/texture/Texture$WrapAxis;)Lcom/jme3/texture/Texture$WrapMode;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertWrapMode(Lcom/jme3/texture/Texture$WrapMode;)I

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_3

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLES20.glTexParameteri("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", GLES20.GL_TEXTURE_WRAP_S, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/jme3/texture/Texture$WrapAxis;->S:Lcom/jme3/texture/Texture$WrapAxis;

    invoke-virtual {p1, v3}, Lcom/jme3/texture/Texture;->getWrap(Lcom/jme3/texture/Texture$WrapAxis;)Lcom/jme3/texture/Texture$WrapMode;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertWrapMode(Lcom/jme3/texture/Texture$WrapMode;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_3
    const/16 v1, 0x2802

    sget-object v2, Lcom/jme3/texture/Texture$WrapAxis;->S:Lcom/jme3/texture/Texture$WrapAxis;

    invoke-virtual {p1, v2}, Lcom/jme3/texture/Texture;->getWrap(Lcom/jme3/texture/Texture$WrapAxis;)Lcom/jme3/texture/Texture$WrapMode;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertWrapMode(Lcom/jme3/texture/Texture$WrapMode;)I

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public applyRenderState(Lcom/jme3/material/RenderState;)V
    .locals 8
    .param p1    # Lcom/jme3/material/RenderState;

    const/16 v7, 0x302

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->isDepthTest()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-boolean v0, v0, Lcom/jme3/renderer/RenderContext;->depthTestEnabled:Z

    if-nez v0, :cond_d

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glEnable(GLES20.GL_DEPTH_TEST)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_0
    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glDepthFunc(GLES20.GL_LEQUAL)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_1
    const/16 v0, 0x203

    invoke-static {v0}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput-boolean v5, v0, Lcom/jme3/renderer/RenderContext;->depthTestEnabled:Z

    :cond_2
    :goto_0
    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->isAlphaTest()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-boolean v0, v0, Lcom/jme3/renderer/RenderContext;->alphaTestEnabled:Z

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput-boolean v5, v0, Lcom/jme3/renderer/RenderContext;->alphaTestEnabled:Z

    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->isDepthWrite()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-boolean v0, v0, Lcom/jme3/renderer/RenderContext;->depthWriteEnabled:Z

    if-nez v0, :cond_10

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glDepthMask(true)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_4
    invoke-static {v5}, Landroid/opengl/GLES20;->glDepthMask(Z)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput-boolean v5, v0, Lcom/jme3/renderer/RenderContext;->depthWriteEnabled:Z

    :cond_5
    :goto_2
    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->isColorWrite()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-boolean v0, v0, Lcom/jme3/renderer/RenderContext;->colorWriteEnabled:Z

    if-nez v0, :cond_12

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_6

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glColorMask(true, true, true, true)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_6
    invoke-static {v5, v5, v5, v5}, Landroid/opengl/GLES20;->glColorMask(ZZZZ)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput-boolean v5, v0, Lcom/jme3/renderer/RenderContext;->colorWriteEnabled:Z

    :cond_7
    :goto_3
    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->isPointSprite()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-boolean v0, v0, Lcom/jme3/renderer/RenderContext;->pointSprite:Z

    if-nez v0, :cond_14

    :cond_8
    :goto_4
    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->isPolyOffset()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-boolean v0, v0, Lcom/jme3/renderer/RenderContext;->polyOffsetEnabled:Z

    if-nez v0, :cond_15

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_9

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glEnable(GLES20.GL_POLYGON_OFFSET_FILL)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_9
    const v0, 0x8037

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_a

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "GLES20.glPolygonOffset({0}, {1})"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getPolyOffsetFactor()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getPolyOffsetUnits()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_a
    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getPolyOffsetFactor()F

    move-result v0

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getPolyOffsetUnits()F

    move-result v1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glPolygonOffset(FF)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput-boolean v5, v0, Lcom/jme3/renderer/RenderContext;->polyOffsetEnabled:Z

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getPolyOffsetFactor()F

    move-result v1

    iput v1, v0, Lcom/jme3/renderer/RenderContext;->polyOffsetFactor:F

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getPolyOffsetUnits()F

    move-result v1

    iput v1, v0, Lcom/jme3/renderer/RenderContext;->polyOffsetUnits:F

    :cond_b
    :goto_5
    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getFaceCullMode()Lcom/jme3/material/RenderState$FaceCullMode;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-object v1, v1, Lcom/jme3/renderer/RenderContext;->cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

    if-eq v0, v1, :cond_1d

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getFaceCullMode()Lcom/jme3/material/RenderState$FaceCullMode;

    move-result-object v0

    sget-object v1, Lcom/jme3/material/RenderState$FaceCullMode;->Off:Lcom/jme3/material/RenderState$FaceCullMode;

    if-ne v0, v1, :cond_1a

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_c

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glDisable(GLES20.GL_CULL_FACE)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_c
    const/16 v0, 0xb44

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    :goto_6
    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer$1;->$SwitchMap$com$jme3$material$RenderState$FaceCullMode:[I

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getFaceCullMode()Lcom/jme3/material/RenderState$FaceCullMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jme3/material/RenderState$FaceCullMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized face cull mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getFaceCullMode()Lcom/jme3/material/RenderState$FaceCullMode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->isDepthTest()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-boolean v0, v0, Lcom/jme3/renderer/RenderContext;->depthTestEnabled:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_e

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glDisable(GLES20.GL_DEPTH_TEST)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_e
    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput-boolean v6, v0, Lcom/jme3/renderer/RenderContext;->depthTestEnabled:Z

    goto/16 :goto_0

    :cond_f
    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->isAlphaTest()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-boolean v0, v0, Lcom/jme3/renderer/RenderContext;->alphaTestEnabled:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput-boolean v6, v0, Lcom/jme3/renderer/RenderContext;->alphaTestEnabled:Z

    goto/16 :goto_1

    :cond_10
    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->isDepthWrite()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-boolean v0, v0, Lcom/jme3/renderer/RenderContext;->depthWriteEnabled:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_11

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glDepthMask(false)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_11
    invoke-static {v6}, Landroid/opengl/GLES20;->glDepthMask(Z)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput-boolean v6, v0, Lcom/jme3/renderer/RenderContext;->depthWriteEnabled:Z

    goto/16 :goto_2

    :cond_12
    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->isColorWrite()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-boolean v0, v0, Lcom/jme3/renderer/RenderContext;->colorWriteEnabled:Z

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_13

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glColorMask(false, false, false, false)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_13
    invoke-static {v6, v6, v6, v6}, Landroid/opengl/GLES20;->glColorMask(ZZZZ)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput-boolean v6, v0, Lcom/jme3/renderer/RenderContext;->colorWriteEnabled:Z

    goto/16 :goto_3

    :cond_14
    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->isPointSprite()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-boolean v0, v0, Lcom/jme3/renderer/RenderContext;->pointSprite:Z

    if-eqz v0, :cond_8

    goto/16 :goto_4

    :cond_15
    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getPolyOffsetFactor()F

    move-result v0

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget v1, v1, Lcom/jme3/renderer/RenderContext;->polyOffsetFactor:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_16

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getPolyOffsetUnits()F

    move-result v0

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget v1, v1, Lcom/jme3/renderer/RenderContext;->polyOffsetUnits:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_b

    :cond_16
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_17

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "GLES20.glPolygonOffset({0}, {1})"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getPolyOffsetFactor()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getPolyOffsetUnits()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_17
    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getPolyOffsetFactor()F

    move-result v0

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getPolyOffsetUnits()F

    move-result v1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glPolygonOffset(FF)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getPolyOffsetFactor()F

    move-result v1

    iput v1, v0, Lcom/jme3/renderer/RenderContext;->polyOffsetFactor:F

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getPolyOffsetUnits()F

    move-result v1

    iput v1, v0, Lcom/jme3/renderer/RenderContext;->polyOffsetUnits:F

    goto/16 :goto_5

    :cond_18
    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-boolean v0, v0, Lcom/jme3/renderer/RenderContext;->polyOffsetEnabled:Z

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_19

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glDisable(GLES20.GL_POLYGON_OFFSET_FILL)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_19
    const v0, 0x8037

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput-boolean v6, v0, Lcom/jme3/renderer/RenderContext;->polyOffsetEnabled:Z

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput v2, v0, Lcom/jme3/renderer/RenderContext;->polyOffsetFactor:F

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput v2, v0, Lcom/jme3/renderer/RenderContext;->polyOffsetUnits:F

    goto/16 :goto_5

    :cond_1a
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_1b

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glEnable(GLES20.GL_CULL_FACE)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_1b
    const/16 v0, 0xb44

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    goto/16 :goto_6

    :pswitch_0
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_1c

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glCullFace(GLES20.GL_BACK)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_1c
    const/16 v0, 0x405

    invoke-static {v0}, Landroid/opengl/GLES20;->glCullFace(I)V

    :goto_7
    :pswitch_1
    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getFaceCullMode()Lcom/jme3/material/RenderState$FaceCullMode;

    move-result-object v1

    iput-object v1, v0, Lcom/jme3/renderer/RenderContext;->cullMode:Lcom/jme3/material/RenderState$FaceCullMode;

    :cond_1d
    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getBlendMode()Lcom/jme3/material/RenderState$BlendMode;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-object v1, v1, Lcom/jme3/renderer/RenderContext;->blendMode:Lcom/jme3/material/RenderState$BlendMode;

    if-eq v0, v1, :cond_1f

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getBlendMode()Lcom/jme3/material/RenderState$BlendMode;

    move-result-object v0

    sget-object v1, Lcom/jme3/material/RenderState$BlendMode;->Off:Lcom/jme3/material/RenderState$BlendMode;

    if-ne v0, v1, :cond_22

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_1e

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glDisable(GLES20.GL_BLEND)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_1e
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    :goto_8
    :pswitch_2
    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getBlendMode()Lcom/jme3/material/RenderState$BlendMode;

    move-result-object v1

    iput-object v1, v0, Lcom/jme3/renderer/RenderContext;->blendMode:Lcom/jme3/material/RenderState$BlendMode;

    :cond_1f
    return-void

    :pswitch_3
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_20

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glCullFace(GLES20.GL_FRONT)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_20
    const/16 v0, 0x404

    invoke-static {v0}, Landroid/opengl/GLES20;->glCullFace(I)V

    goto :goto_7

    :pswitch_4
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_21

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glCullFace(GLES20.GL_FRONT_AND_BACK)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_21
    const/16 v0, 0x408

    invoke-static {v0}, Landroid/opengl/GLES20;->glCullFace(I)V

    goto :goto_7

    :cond_22
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_23

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glEnable(GLES20.GL_BLEND)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_23
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer$1;->$SwitchMap$com$jme3$material$RenderState$BlendMode:[I

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getBlendMode()Lcom/jme3/material/RenderState$BlendMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jme3/material/RenderState$BlendMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized blend mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jme3/material/RenderState;->getBlendMode()Lcom/jme3/material/RenderState$BlendMode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_5
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_24

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_24
    invoke-static {v5, v5}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    goto :goto_8

    :pswitch_6
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_25

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_25
    invoke-static {v7, v5}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    goto/16 :goto_8

    :pswitch_7
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_26

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE_MINUS_SRC_COLOR)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_26
    const/16 v0, 0x301

    invoke-static {v5, v0}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    goto/16 :goto_8

    :pswitch_8
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_27

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_27
    const/16 v0, 0x303

    invoke-static {v7, v0}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    goto/16 :goto_8

    :pswitch_9
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_28

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE_MINUS_SRC_ALPHA)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_28
    const/16 v0, 0x303

    invoke-static {v5, v0}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    goto/16 :goto_8

    :pswitch_a
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_29

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glBlendFunc(GLES20.GL_DST_COLOR, GLES20.GL_ZERO)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_29
    const/16 v0, 0x306

    invoke-static {v0, v6}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    goto/16 :goto_8

    :pswitch_b
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_2a

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glBlendFunc(GLES20.GL_DST_COLOR, GLES20.GL_SRC_COLOR)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_2a
    const/16 v0, 0x306

    const/16 v1, 0x300

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    goto/16 :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public cleanup()V
    .locals 1

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->objManager:Lcom/jme3/util/NativeObjectManager;

    invoke-virtual {v0, p0}, Lcom/jme3/util/NativeObjectManager;->deleteAllObjects(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    invoke-virtual {v0}, Lcom/jme3/renderer/Statistics;->clearMemory()V

    return-void
.end method

.method public clearBuffers(ZZZ)V
    .locals 7
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const/16 v0, 0x4000

    :cond_0
    if-eqz p2, :cond_1

    or-int/lit16 v0, v0, 0x100

    :cond_1
    if-eqz p3, :cond_2

    or-int/lit16 v0, v0, 0x400

    :cond_2
    if-eqz v0, :cond_4

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_3

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v3, "GLES20.glClear(color={0}, depth={1}, stencil={2})"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    :cond_4
    return-void
.end method

.method public clearTextureUnits()V
    .locals 5

    iget-object v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-object v2, v4, Lcom/jme3/renderer/RenderContext;->textureIndexList:Lcom/jme3/renderer/IDList;

    iget-object v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-object v3, v4, Lcom/jme3/renderer/RenderContext;->boundTextures:[Lcom/jme3/texture/Image;

    const/4 v0, 0x0

    :goto_0
    iget v4, v2, Lcom/jme3/renderer/IDList;->oldLen:I

    if-ge v0, v4, :cond_0

    iget-object v4, v2, Lcom/jme3/renderer/IDList;->oldList:[I

    aget v1, v4, v0

    const/4 v4, 0x0

    aput-object v4, v3, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-object v4, v4, Lcom/jme3/renderer/RenderContext;->textureIndexList:Lcom/jme3/renderer/IDList;

    invoke-virtual {v4}, Lcom/jme3/renderer/IDList;->copyNewToOld()V

    return-void
.end method

.method public clearVertexAttribs()V
    .locals 6

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-object v1, v0, Lcom/jme3/renderer/RenderContext;->attribIndexList:Lcom/jme3/renderer/IDList;

    const/4 v0, 0x0

    :goto_0
    iget v2, v1, Lcom/jme3/renderer/IDList;->oldLen:I

    if-ge v0, v2, :cond_1

    iget-object v2, v1, Lcom/jme3/renderer/IDList;->oldList:[I

    aget v2, v2, v0

    iget-boolean v3, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v3, :cond_0

    sget-object v3, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glDisableVertexAttribArray("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_0
    invoke-static {v2}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    iget-object v3, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-object v3, v3, Lcom/jme3/renderer/RenderContext;->boundAttribs:[Lcom/jme3/scene/VertexBuffer;

    const/4 v4, 0x0

    aput-object v4, v3, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-object v0, v0, Lcom/jme3/renderer/RenderContext;->attribIndexList:Lcom/jme3/renderer/IDList;

    invoke-virtual {v0}, Lcom/jme3/renderer/IDList;->copyNewToOld()V

    return-void
.end method

.method public convertElementMode(Lcom/jme3/scene/Mesh$Mode;)I
    .locals 3

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer$1;->$SwitchMap$com$jme3$scene$Mesh$Mode:[I

    invoke-virtual {p1}, Lcom/jme3/scene/Mesh$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized mesh mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x6

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public convertShaderType(Lcom/jme3/shader/Shader$ShaderType;)I
    .locals 2

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer$1;->$SwitchMap$com$jme3$shader$Shader$ShaderType:[I

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unrecognized shader type."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const v0, 0x8b30

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x8b31

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public deleteBuffer(Lcom/jme3/scene/VertexBuffer;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-virtual {v1, v2, v0}, Ljava/nio/IntBuffer;->put(II)Ljava/nio/IntBuffer;

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/nio/Buffer;->limit(I)Ljava/nio/Buffer;

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glDeleteBuffers(1, buffer)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-static {v3, v0}, Landroid/opengl/GLES20;->glDeleteBuffers(ILjava/nio/IntBuffer;)V

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->resetObject()V

    :cond_1
    return-void
.end method

.method public deleteFrameBuffer(Lcom/jme3/texture/FrameBuffer;)V
    .locals 2

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "deleteFrameBuffer is not supported."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    return-void
.end method

.method public deleteImage(Lcom/jme3/texture/Image;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/jme3/texture/Image;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-virtual {v1, v2, v0}, Ljava/nio/IntBuffer;->put(II)Ljava/nio/IntBuffer;

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/nio/Buffer;->limit(I)Ljava/nio/Buffer;

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glDeleteTexture(1, buffer)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-static {v3, v0}, Landroid/opengl/GLES20;->glDeleteTextures(ILjava/nio/IntBuffer;)V

    invoke-virtual {p1}, Lcom/jme3/texture/Image;->resetObject()V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    invoke-virtual {v0}, Lcom/jme3/renderer/Statistics;->onDeleteTexture()V

    :cond_1
    return-void
.end method

.method public deleteShader(Lcom/jme3/shader/Shader;)V
    .locals 6

    const/4 v5, -0x1

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getId()I

    move-result v0

    if-ne v0, v5, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "Shader is not uploaded to GPU, cannot delete."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getSources()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/shader/Shader$ShaderSource;

    invoke-virtual {v0}, Lcom/jme3/shader/Shader$ShaderSource;->getId()I

    move-result v2

    if-eq v2, v5, :cond_1

    iget-boolean v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GLES20.glDetachShader("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/jme3/shader/Shader$ShaderSource;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getId()I

    move-result v2

    invoke-virtual {v0}, Lcom/jme3/shader/Shader$ShaderSource;->getId()I

    move-result v0

    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glDetachShader(II)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->resetSources()V

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GLES20.glDeleteProgram("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getId()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    invoke-virtual {v0}, Lcom/jme3/renderer/Statistics;->onDeleteShader()V

    goto/16 :goto_0
.end method

.method public deleteShaderSource(Lcom/jme3/shader/Shader$ShaderSource;)V
    .locals 3

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getId()I

    move-result v0

    if-gez v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "Shader source is not uploaded to GPU, cannot delete."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/jme3/shader/Shader$ShaderSource;->setUsable(Z)V

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->clearUpdateNeeded()V

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GLES20.glDeleteShader("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getId()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->resetObject()V

    goto :goto_0
.end method

.method public drawTriangleList(Lcom/jme3/scene/VertexBuffer;Lcom/jme3/scene/Mesh;I)V
    .locals 17

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "drawTriangleList("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v1

    sget-object v2, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    if-eq v1, v2, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Only index buffers are allowed as triangle lists."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->isUpdateNeeded()Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v2, "updateBufferData for indexBuf."

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_2
    invoke-virtual/range {p0 .. p1}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->updateBufferData(Lcom/jme3/scene/VertexBuffer;)V

    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getId()I

    move-result v1

    sget-boolean v2, Lcom/jme3/renderer/android/OGLESShaderRenderer;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    const/4 v2, -0x1

    if-ne v1, v2, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_4
    const/4 v2, -0x1

    if-ne v1, v2, :cond_5

    sget-object v2, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v3, "invalid buffer id!"

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget v2, v2, Lcom/jme3/renderer/RenderContext;->boundElementArrayVBO:I

    if-eq v2, v1, :cond_7

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v2, :cond_6

    sget-object v2, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v4, "GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, {0})"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_6
    const v2, 0x8893

    invoke-static {v2, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput v1, v2, Lcom/jme3/renderer/RenderContext;->boundElementArrayVBO:I

    :cond_7
    invoke-virtual/range {p2 .. p2}, Lcom/jme3/scene/Mesh;->getVertexCount()I

    move-result v2

    const/4 v1, 0x1

    move/from16 v0, p3

    if-le v0, v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->caps:Ljava/util/EnumSet;

    sget-object v3, Lcom/jme3/renderer/Caps;->MeshInstancing:Lcom/jme3/renderer/Caps;

    invoke-virtual {v1, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/jme3/scene/Mesh;->getMode()Lcom/jme3/scene/Mesh$Mode;

    move-result-object v4

    sget-object v5, Lcom/jme3/scene/Mesh$Mode;->Hybrid:Lcom/jme3/scene/Mesh$Mode;

    if-ne v4, v5, :cond_d

    invoke-virtual/range {p2 .. p2}, Lcom/jme3/scene/Mesh;->getModeStart()[I

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/jme3/scene/Mesh;->getElementLengths()[I

    move-result-object v5

    sget-object v3, Lcom/jme3/scene/Mesh$Mode;->Triangles:Lcom/jme3/scene/Mesh$Mode;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertElementMode(Lcom/jme3/scene/Mesh$Mode;)I

    move-result v4

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertFormat(Lcom/jme3/scene/VertexBuffer$Format;)I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jme3/scene/VertexBuffer$Format;->getComponentSize()I

    move-result v7

    const/4 v3, 0x0

    aget v3, v2, v3

    const/4 v3, 0x1

    aget v8, v2, v3

    const/4 v3, 0x2

    aget v9, v2, v3

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_1
    array-length v10, v5

    if-ge v2, v10, :cond_10

    if-ne v2, v8, :cond_a

    sget-object v4, Lcom/jme3/scene/Mesh$Mode;->TriangleStrip:Lcom/jme3/scene/Mesh$Mode;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertElementMode(Lcom/jme3/scene/Mesh$Mode;)I

    move-result v4

    :cond_8
    :goto_2
    aget v10, v5, v2

    if-eqz v1, :cond_b

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "instancing is not supported."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    const/4 v1, 0x0

    goto :goto_0

    :cond_a
    if-ne v2, v9, :cond_8

    sget-object v4, Lcom/jme3/scene/Mesh$Mode;->TriangleStrip:Lcom/jme3/scene/Mesh$Mode;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertElementMode(Lcom/jme3/scene/Mesh$Mode;)I

    move-result v4

    goto :goto_2

    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/nio/Buffer;->position(I)Ljava/nio/Buffer;

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v11, :cond_c

    sget-object v11, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v12, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v13, "glDrawElements(): {0}, {1}"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v11, v12, v13, v14}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v11

    invoke-static {v4, v10, v6, v11}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    mul-int/2addr v10, v7

    add-int/2addr v3, v10

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_d
    if-eqz v1, :cond_e

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "instancing is not supported."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_e
    invoke-virtual {v3}, Ljava/nio/Buffer;->clear()Ljava/nio/Buffer;

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_f

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v4, "glDrawElements(), indexBuf.capacity ({0}), vertCount ({1})"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/nio/Buffer;->capacity()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {v1, v3, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_f
    invoke-virtual/range {p2 .. p2}, Lcom/jme3/scene/Mesh;->getMode()Lcom/jme3/scene/Mesh$Mode;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertElementMode(Lcom/jme3/scene/Mesh$Mode;)I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/Buffer;->capacity()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertFormat(Lcom/jme3/scene/VertexBuffer$Format;)I

    move-result v3

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Landroid/opengl/GLES20;->glDrawElements(IIII)V

    :cond_10
    return-void
.end method

.method public drawTriangleList_Array(Lcom/jme3/scene/VertexBuffer;Lcom/jme3/scene/Mesh;I)V
    .locals 16

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v3, "drawTriangleList_Array(Count = {0})"

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v1

    sget-object v2, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    if-eq v1, v2, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Only index buffers are allowed as triangle lists."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    const/4 v1, 0x1

    move/from16 v0, p3

    if-le v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->caps:Ljava/util/EnumSet;

    sget-object v2, Lcom/jme3/renderer/Caps;->MeshInstancing:Lcom/jme3/renderer/Caps;

    invoke-virtual {v1, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Caps.MeshInstancing is not supported."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/jme3/scene/Mesh;->getVertexCount()I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/Buffer;->clear()Ljava/nio/Buffer;

    invoke-virtual/range {p2 .. p2}, Lcom/jme3/scene/Mesh;->getMode()Lcom/jme3/scene/Mesh$Mode;

    move-result-object v2

    sget-object v3, Lcom/jme3/scene/Mesh$Mode;->Hybrid:Lcom/jme3/scene/Mesh$Mode;

    if-ne v2, v3, :cond_7

    invoke-virtual/range {p2 .. p2}, Lcom/jme3/scene/Mesh;->getModeStart()[I

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/jme3/scene/Mesh;->getElementLengths()[I

    move-result-object v4

    sget-object v2, Lcom/jme3/scene/Mesh$Mode;->Triangles:Lcom/jme3/scene/Mesh$Mode;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertElementMode(Lcom/jme3/scene/Mesh$Mode;)I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertFormat(Lcom/jme3/scene/VertexBuffer$Format;)I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jme3/scene/VertexBuffer$Format;->getComponentSize()I

    move-result v6

    const/4 v2, 0x0

    aget v2, v1, v2

    const/4 v2, 0x1

    aget v7, v1, v2

    const/4 v2, 0x2

    aget v8, v1, v2

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_1
    array-length v9, v4

    if-ge v1, v9, :cond_9

    if-ne v1, v7, :cond_6

    sget-object v3, Lcom/jme3/scene/Mesh$Mode;->TriangleStrip:Lcom/jme3/scene/Mesh$Mode;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertElementMode(Lcom/jme3/scene/Mesh$Mode;)I

    move-result v3

    :cond_4
    :goto_2
    aget v9, v4, v1

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/nio/Buffer;->position(I)Ljava/nio/Buffer;

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v10, :cond_5

    sget-object v10, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v11, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v12, "glDrawElements(): {0}, {1}"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v10, v11, v12, v13}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v10

    invoke-static {v3, v9, v5, v10}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    mul-int/2addr v9, v6

    add-int/2addr v2, v9

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    if-ne v1, v8, :cond_4

    sget-object v3, Lcom/jme3/scene/Mesh$Mode;->TriangleStrip:Lcom/jme3/scene/Mesh$Mode;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertElementMode(Lcom/jme3/scene/Mesh$Mode;)I

    move-result v3

    goto :goto_2

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v2, :cond_8

    sget-object v2, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v4, "glDrawElements(), indexBuf.capacity ({0}), vertCount ({1})"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/nio/Buffer;->capacity()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_8
    invoke-virtual/range {p2 .. p2}, Lcom/jme3/scene/Mesh;->getMode()Lcom/jme3/scene/Mesh$Mode;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertElementMode(Lcom/jme3/scene/Mesh$Mode;)I

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/Buffer;->capacity()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertFormat(Lcom/jme3/scene/VertexBuffer$Format;)I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    :cond_9
    return-void
.end method

.method public getCaps()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/jme3/renderer/Caps;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->caps:Ljava/util/EnumSet;

    return-object v0
.end method

.method public getStatistics()Lcom/jme3/renderer/Statistics;
    .locals 1

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    return-object v0
.end method

.method public initialize()V
    .locals 14

    const/16 v13, 0x1f01

    const/4 v12, 0x1

    const/4 v11, 0x0

    sget-object v7, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v9, "Vendor: {0}"

    const/16 v10, 0x1f00

    invoke-static {v10}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    sget-object v7, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v9, "Renderer: {0}"

    invoke-static {v13}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    sget-object v7, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v9, "Version: {0}"

    const/16 v10, 0x1f02

    invoke-static {v10}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-static {v13}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "PowerVR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->powerVr:Z

    const v7, 0x8b8c

    invoke-static {v7}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v9, "GLES20.Shading Language Version: {0}"

    invoke-virtual {v7, v8, v9, v6}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    if-eqz v6, :cond_0

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    const/4 v7, -0x1

    iput v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->glslVer:I

    new-instance v7, Ljava/lang/UnsupportedOperationException;

    const-string v8, "GLSL and OpenGL2 is required for the OpenGL ES renderer!"

    invoke-direct {v7, v8}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_1
    const-string v2, "OpenGL ES GLSL ES "

    const-string v7, " "

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    if-lt v3, v12, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    :goto_0
    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    const/high16 v7, 0x42c80000

    mul-float/2addr v7, v5

    float-to-int v7, v7

    iput v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->glslVer:I

    iget v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->glslVer:I

    iget-object v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->caps:Ljava/util/EnumSet;

    sget-object v8, Lcom/jme3/renderer/Caps;->GLSL100:Lcom/jme3/renderer/Caps;

    invoke-virtual {v7, v8}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->caps:Ljava/util/EnumSet;

    sget-object v8, Lcom/jme3/renderer/Caps;->GLSL100:Lcom/jme3/renderer/Caps;

    invoke-virtual {v7, v8}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    sget-object v7, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v8, "Force-adding GLSL100 support, since OpenGL2 is supported."

    invoke-virtual {v7, v8}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->caps:Ljava/util/EnumSet;

    sget-object v8, Lcom/jme3/renderer/Caps;->GLSL100:Lcom/jme3/renderer/Caps;

    invoke-virtual {v7, v8}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_2
    const v7, 0x8b4c

    iget-object v8, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetIntegerv(ILjava/nio/IntBuffer;)V

    iget-object v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    invoke-virtual {v7, v11}, Ljava/nio/IntBuffer;->get(I)I

    move-result v7

    iput v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->vertexTextureUnits:I

    sget-object v7, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v9, "VTF Units: {0}"

    iget v10, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->vertexTextureUnits:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    iget v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->vertexTextureUnits:I

    if-lez v7, :cond_3

    iget-object v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->caps:Ljava/util/EnumSet;

    sget-object v8, Lcom/jme3/renderer/Caps;->VertexTextureFetch:Lcom/jme3/renderer/Caps;

    invoke-virtual {v7, v8}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    :cond_3
    const v7, 0x8872

    iget-object v8, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetIntegerv(ILjava/nio/IntBuffer;)V

    iget-object v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    invoke-virtual {v7, v11}, Ljava/nio/IntBuffer;->get(I)I

    move-result v7

    iput v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->fragTextureUnits:I

    sget-object v7, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v9, "Texture Units: {0}"

    iget v10, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->fragTextureUnits:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    const v7, 0x8869

    iget-object v8, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetIntegerv(ILjava/nio/IntBuffer;)V

    iget-object v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    invoke-virtual {v7, v11}, Ljava/nio/IntBuffer;->get(I)I

    move-result v7

    iput v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->vertexAttribs:I

    sget-object v7, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v9, "Vertex Attributes: {0}"

    iget v10, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->vertexAttribs:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    const/16 v7, 0xd50

    iget-object v8, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetIntegerv(ILjava/nio/IntBuffer;)V

    iget-object v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    invoke-virtual {v7, v11}, Ljava/nio/IntBuffer;->get(I)I

    move-result v4

    sget-object v7, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v9, "Subpixel Bits: {0}"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    const/16 v7, 0xd33

    iget-object v8, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetIntegerv(ILjava/nio/IntBuffer;)V

    iget-object v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    invoke-virtual {v7, v11}, Ljava/nio/IntBuffer;->get(I)I

    move-result v7

    iput v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->maxTexSize:I

    sget-object v7, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v9, "Maximum Texture Resolution: {0}"

    iget v10, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->maxTexSize:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    const v7, 0x851c

    iget-object v8, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetIntegerv(ILjava/nio/IntBuffer;)V

    iget-object v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    invoke-virtual {v7, v11}, Ljava/nio/IntBuffer;->get(I)I

    move-result v7

    iput v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->maxCubeTexSize:I

    sget-object v7, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v9, "Maximum CubeMap Resolution: {0}"

    iget v10, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->maxCubeTexSize:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    const/16 v7, 0x1f03

    invoke-static {v7}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v7, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v9, "GL_EXTENSIONS: {0}"

    invoke-virtual {v7, v8, v9, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    const v7, 0x86a3

    iget-object v8, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetIntegerv(ILjava/nio/IntBuffer;)V

    const/4 v1, 0x0

    :goto_1
    iget-object v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    invoke-virtual {v7}, Ljava/nio/IntBuffer;->limit()I

    move-result v7

    if-ge v1, v7, :cond_5

    sget-object v7, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v9, "Compressed Texture Formats: {0}"

    iget-object v10, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf16:Ljava/nio/IntBuffer;

    invoke-virtual {v10, v1}, Ljava/nio/IntBuffer;->get(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    :cond_5
    const-string v7, "GL_OES_texture_npot"

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_6

    iput-boolean v12, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->powerOf2:Z

    :cond_6
    sget-object v7, Lcom/jme3/material/RenderState;->DEFAULT:Lcom/jme3/material/RenderState;

    invoke-virtual {p0, v7}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->applyRenderState(Lcom/jme3/material/RenderState;)V

    const/16 v7, 0xbd0

    invoke-static {v7}, Landroid/opengl/GLES20;->glDisable(I)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iput-boolean v11, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->useVBO:Z

    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x9

    if-lt v7, v8, :cond_7

    iput-boolean v12, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->useVBO:Z

    :cond_7
    sget-object v7, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v9, "Caps: {0}"

    iget-object v10, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->caps:Ljava/util/EnumSet;

    invoke-virtual {v7, v8, v9, v10}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public onFrame()V
    .locals 4

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkErrors:Z

    if-nez v1, :cond_0

    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/jme3/renderer/RendererException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OpenGL Error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Enable error checking for more info."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/jme3/renderer/RendererException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->objManager:Lcom/jme3/util/NativeObjectManager;

    invoke-virtual {v1, p0}, Lcom/jme3/util/NativeObjectManager;->deleteUnused(Ljava/lang/Object;)V

    return-void
.end method

.method public renderMesh(Lcom/jme3/scene/Mesh;II)V
    .locals 4

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget v0, v0, Lcom/jme3/renderer/RenderContext;->pointSize:F

    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getPointSize()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "GLES10.glPointSize({0})"

    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getPointSize()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getPointSize()F

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES10;->glPointSize(F)V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getPointSize()F

    move-result v1

    iput v1, v0, Lcom/jme3/renderer/RenderContext;->pointSize:F

    :cond_1
    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget v0, v0, Lcom/jme3/renderer/RenderContext;->lineWidth:F

    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getLineWidth()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "GLES20.glLineWidth({0})"

    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getLineWidth()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_2
    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getLineWidth()F

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES20;->glLineWidth(F)V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    invoke-virtual {p1}, Lcom/jme3/scene/Mesh;->getLineWidth()F

    move-result v1

    iput v1, v0, Lcom/jme3/renderer/RenderContext;->lineWidth:F

    :cond_3
    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    invoke-virtual {v0, p1, p2}, Lcom/jme3/renderer/Statistics;->onMeshDrawn(Lcom/jme3/scene/Mesh;I)V

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->useVBO:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "RENDERING A MESH USING VertexBufferObject"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_4
    invoke-direct {p0, p1, p2, p3}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->renderMeshDefault(Lcom/jme3/scene/Mesh;II)V

    :goto_0
    return-void

    :cond_5
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_6

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "RENDERING A MESH USING VertexArray"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_6
    invoke-direct {p0, p1, p2, p3}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->renderMeshVertexArray(Lcom/jme3/scene/Mesh;II)V

    goto :goto_0
.end method

.method public resetGLObjects()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->objManager:Lcom/jme3/util/NativeObjectManager;

    invoke-virtual {v0}, Lcom/jme3/util/NativeObjectManager;->resetObjects()V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    invoke-virtual {v0}, Lcom/jme3/renderer/Statistics;->clearMemory()V

    iput-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->boundShader:Lcom/jme3/shader/Shader;

    iput-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->lastFb:Lcom/jme3/texture/FrameBuffer;

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    invoke-virtual {v0}, Lcom/jme3/renderer/RenderContext;->reset()V

    return-void
.end method

.method protected resetUniformLocations(Lcom/jme3/shader/Shader;)V
    .locals 4
    .param p1    # Lcom/jme3/shader/Shader;

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getUniformMap()Lcom/jme3/util/ListMap;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lcom/jme3/util/ListMap;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-virtual {v2, v0}, Lcom/jme3/util/ListMap;->getValue(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/shader/Uniform;

    invoke-virtual {v1}, Lcom/jme3/shader/Uniform;->reset()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setBackgroundColor(Lcom/jme3/math/ColorRGBA;)V
    .locals 6
    .param p1    # Lcom/jme3/math/ColorRGBA;

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "GLES20.glClearColor({0}, {1}, {2}, {3})"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p1, Lcom/jme3/math/ColorRGBA;->r:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p1, Lcom/jme3/math/ColorRGBA;->g:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget v5, p1, Lcom/jme3/math/ColorRGBA;->b:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget v5, p1, Lcom/jme3/math/ColorRGBA;->a:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget v0, p1, Lcom/jme3/math/ColorRGBA;->r:F

    iget v1, p1, Lcom/jme3/math/ColorRGBA;->g:F

    iget v2, p1, Lcom/jme3/math/ColorRGBA;->b:F

    iget v3, p1, Lcom/jme3/math/ColorRGBA;->a:F

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    return-void
.end method

.method public setClipRect(IIII)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-boolean v0, v0, Lcom/jme3/renderer/RenderContext;->clipRectEnabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glEnable(GLES20.GL_SCISSOR_TEST)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_0
    const/16 v0, 0xc11

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput-boolean v6, v0, Lcom/jme3/renderer/RenderContext;->clipRectEnabled:Z

    :cond_1
    iget v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->clipX:I

    if-ne v0, p1, :cond_2

    iget v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->clipY:I

    if-ne v0, p2, :cond_2

    iget v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->clipW:I

    if-ne v0, p3, :cond_2

    iget v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->clipH:I

    if-eq v0, p4, :cond_4

    :cond_2
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "GLES20.glScissor({0}, {1}, {2}, {3})"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES20;->glScissor(IIII)V

    iput p1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->clipX:I

    iput p2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->clipY:I

    iput p3, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->clipW:I

    iput p4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->clipH:I

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    :cond_4
    return-void
.end method

.method public setDepthRange(FF)V
    .locals 6
    .param p1    # F
    .param p2    # F

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "GLES20.glDepthRangef({0}, {1})"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glDepthRangef(FF)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    return-void
.end method

.method public setFrameBuffer(Lcom/jme3/texture/FrameBuffer;)V
    .locals 2

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "setFrameBuffer is not supported."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setLighting(Lcom/jme3/light/LightList;)V
    .locals 0
    .param p1    # Lcom/jme3/light/LightList;

    return-void
.end method

.method public setShader(Lcom/jme3/shader/Shader;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setShader("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_0
    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget v0, v0, Lcom/jme3/renderer/RenderContext;->boundShaderProgram:I

    if-lez v0, :cond_2

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glUseProgram(0)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_1
    invoke-static {v3}, Landroid/opengl/GLES20;->glUseProgram(I)V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    invoke-virtual {v0, v5, v4}, Lcom/jme3/renderer/Statistics;->onShaderUse(Lcom/jme3/shader/Shader;Z)V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput v3, v0, Lcom/jme3/renderer/RenderContext;->boundShaderProgram:I

    iput-object v5, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->boundShader:Lcom/jme3/shader/Shader;

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->isUpdateNeeded()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, p1}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->updateShaderData(Lcom/jme3/shader/Shader;)V

    :cond_4
    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->isUsable()Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "shader is not usable."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    sget-boolean v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getId()I

    move-result v0

    if-gtz v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_6
    invoke-virtual {p0, p1}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->updateShaderUniforms(Lcom/jme3/shader/Shader;)V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget v0, v0, Lcom/jme3/renderer/RenderContext;->boundShaderProgram:I

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getId()I

    move-result v1

    if-eq v0, v1, :cond_8

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_7

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GLES20.glUseProgram("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getId()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    invoke-virtual {v0, p1, v4}, Lcom/jme3/renderer/Statistics;->onShaderUse(Lcom/jme3/shader/Shader;Z)V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getId()I

    move-result v1

    iput v1, v0, Lcom/jme3/renderer/RenderContext;->boundShaderProgram:I

    iput-object p1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->boundShader:Lcom/jme3/shader/Shader;

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    invoke-virtual {v0, p1, v3}, Lcom/jme3/renderer/Statistics;->onShaderUse(Lcom/jme3/shader/Shader;Z)V

    goto :goto_0
.end method

.method public setTexture(ILcom/jme3/texture/Texture;)V
    .locals 7

    const/4 v3, -0x1

    invoke-virtual {p2}, Lcom/jme3/texture/Texture;->getImage()Lcom/jme3/texture/Image;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jme3/texture/Image;->isUpdateNeeded()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/jme3/texture/Texture;->getType()Lcom/jme3/texture/Texture$Type;

    move-result-object v1

    invoke-virtual {p2}, Lcom/jme3/texture/Texture;->getMinFilter()Lcom/jme3/texture/Texture$MinFilter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jme3/texture/Texture$MinFilter;->usesMipMapLevels()Z

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->updateTexImageData(Lcom/jme3/texture/Image;Lcom/jme3/texture/Texture$Type;Z)V

    :cond_0
    invoke-virtual {v0}, Lcom/jme3/texture/Image;->getId()I

    move-result v1

    sget-boolean v2, Lcom/jme3/renderer/android/OGLESShaderRenderer;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    if-ne v1, v3, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    if-ne v1, v3, :cond_2

    sget-object v2, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v3, "error: texture image has -1 id"

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-object v2, v2, Lcom/jme3/renderer/RenderContext;->boundTextures:[Lcom/jme3/texture/Image;

    invoke-virtual {p2}, Lcom/jme3/texture/Texture;->getType()Lcom/jme3/texture/Texture$Type;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertTextureType(Lcom/jme3/texture/Texture$Type;)I

    move-result v3

    iget-object v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-object v4, v4, Lcom/jme3/renderer/RenderContext;->textureIndexList:Lcom/jme3/renderer/IDList;

    invoke-virtual {v4, p1}, Lcom/jme3/renderer/IDList;->moveToNew(I)Z

    move-result v4

    if-nez v4, :cond_3

    :cond_3
    aget-object v4, v2, p1

    if-eq v4, v0, :cond_7

    iget-object v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget v4, v4, Lcom/jme3/renderer/RenderContext;->boundTextureUnit:I

    if-eq v4, p1, :cond_5

    iget-boolean v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v4, :cond_4

    sget-object v4, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_4
    const v4, 0x84c0

    add-int/2addr v4, p1

    invoke-static {v4}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    iget-object v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput p1, v4, Lcom/jme3/renderer/RenderContext;->boundTextureUnit:I

    :cond_5
    iget-boolean v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v4, :cond_6

    sget-object v4, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GLES20.glBindTexture("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_6
    invoke-static {v3, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    aput-object v0, v2, p1

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    invoke-virtual {p2}, Lcom/jme3/texture/Texture;->getImage()Lcom/jme3/texture/Image;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/jme3/renderer/Statistics;->onTextureUse(Lcom/jme3/texture/Image;Z)V

    :goto_0
    invoke-direct {p0, p2}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->setupTextureParams(Lcom/jme3/texture/Texture;)V

    return-void

    :cond_7
    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    invoke-virtual {p2}, Lcom/jme3/texture/Texture;->getImage()Lcom/jme3/texture/Image;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/jme3/renderer/Statistics;->onTextureUse(Lcom/jme3/texture/Image;Z)V

    goto :goto_0
.end method

.method public setUseVA(Z)V
    .locals 7
    .param p1    # Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v3, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v5, "use_VBO [{0}] -> [{1}]"

    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->useVBO:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v2

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-virtual {v3, v4, v5, v6}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    if-nez p1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->useVBO:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public setVerboseLogging(Z)V
    .locals 6
    .param p1    # Z

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "verboseLogging [{0}] -> [{1}]"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean p1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    return-void
.end method

.method public setVertexAttrib(Lcom/jme3/scene/VertexBuffer;)V
    .locals 1
    .param p1    # Lcom/jme3/scene/VertexBuffer;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->setVertexAttrib(Lcom/jme3/scene/VertexBuffer;Lcom/jme3/scene/VertexBuffer;)V

    return-void
.end method

.method public setVertexAttrib(Lcom/jme3/scene/VertexBuffer;Lcom/jme3/scene/VertexBuffer;)V
    .locals 8

    const/4 v7, -0x1

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVertexAttrib("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v0

    sget-object v1, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Index buffers not allowed to be set to vertex attrib"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->isUpdateNeeded()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p2, :cond_2

    invoke-virtual {p0, p1}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->updateBufferData(Lcom/jme3/scene/VertexBuffer;)V

    :cond_2
    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget v1, v0, Lcom/jme3/renderer/RenderContext;->boundShaderProgram:I

    if-lez v1, :cond_10

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->boundShader:Lcom/jme3/shader/Shader;

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/jme3/shader/Shader;->getAttribute(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/shader/Attribute;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jme3/shader/Attribute;->getLocation()I

    move-result v0

    if-ne v0, v7, :cond_4

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "location is invalid for attrib: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jme3/scene/VertexBuffer$Type;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    const/4 v3, -0x2

    if-ne v0, v3, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "in"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jme3/scene/VertexBuffer$Type;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-boolean v3, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v3, :cond_5

    sget-object v3, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glGetAttribLocation("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_5
    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    if-gez v0, :cond_6

    invoke-virtual {v2, v7}, Lcom/jme3/shader/Attribute;->setLocation(I)V

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "attribute is invalid in shader: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jme3/scene/VertexBuffer$Type;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    invoke-virtual {v2, v0}, Lcom/jme3/shader/Attribute;->setLocation(I)V

    :cond_7
    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-object v6, v1, Lcom/jme3/renderer/RenderContext;->boundAttribs:[Lcom/jme3/scene/VertexBuffer;

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-object v1, v1, Lcom/jme3/renderer/RenderContext;->attribIndexList:Lcom/jme3/renderer/IDList;

    invoke-virtual {v1, v0}, Lcom/jme3/renderer/IDList;->moveToNew(I)Z

    move-result v1

    if-nez v1, :cond_9

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_8

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLES20.glEnableVertexAttribArray("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_8
    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    :cond_9
    aget-object v1, v6, v0

    if-eq v1, p1, :cond_3

    if-eqz p2, :cond_a

    invoke-virtual {p2}, Lcom/jme3/scene/VertexBuffer;->getId()I

    move-result v1

    :goto_1
    sget-boolean v2, Lcom/jme3/renderer/android/OGLESShaderRenderer;->$assertionsDisabled:Z

    if-nez v2, :cond_b

    if-ne v1, v7, :cond_b

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_a
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getId()I

    move-result v1

    goto :goto_1

    :cond_b
    if-ne v1, v7, :cond_c

    sget-object v2, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v3, "invalid buffer id"

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    :cond_c
    iget-object v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget v2, v2, Lcom/jme3/renderer/RenderContext;->boundArrayVBO:I

    if-eq v2, v1, :cond_e

    iget-boolean v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v2, :cond_d

    sget-object v2, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GLES20.glBindBuffer(34962, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_d
    const v2, 0x8892

    invoke-static {v2, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    iget-object v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput v1, v2, Lcom/jme3/renderer/RenderContext;->boundArrayVBO:I

    :cond_e
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/Buffer;->clear()Ljava/nio/Buffer;

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_f

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLES20.glVertexAttribPointer(location="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "numComponents="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getNumComponents()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "format="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "isNormalized="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->isNormalized()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "stride="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getStride()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "data.capacity="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/Buffer;->capacity()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_f
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getNumComponents()I

    move-result v1

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertFormat(Lcom/jme3/scene/VertexBuffer$Format;)I

    move-result v2

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->isNormalized()Z

    move-result v3

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getStride()I

    move-result v4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/jme3/renderer/android/Android22Workaround;->glVertexAttribPointer(IIIZII)V

    aput-object p1, v6, v0

    goto/16 :goto_0

    :cond_10
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot render mesh without shader bound"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setVertexAttrib_Array(Lcom/jme3/scene/VertexBuffer;)V
    .locals 1
    .param p1    # Lcom/jme3/scene/VertexBuffer;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->setVertexAttrib_Array(Lcom/jme3/scene/VertexBuffer;Lcom/jme3/scene/VertexBuffer;)V

    return-void
.end method

.method public setVertexAttrib_Array(Lcom/jme3/scene/VertexBuffer;Lcom/jme3/scene/VertexBuffer;)V
    .locals 13

    const/4 v12, -0x1

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "setVertexAttrib_Array({0}, {1})"

    new-array v3, v11, [Ljava/lang/Object;

    aput-object p1, v3, v9

    aput-object p2, v3, v10

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v0

    sget-object v1, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Index buffers not allowed to be set to vertex attrib"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget v1, v0, Lcom/jme3/renderer/RenderContext;->boundShaderProgram:I

    if-lez v1, :cond_a

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-object v6, v0, Lcom/jme3/renderer/RenderContext;->boundAttribs:[Lcom/jme3/scene/VertexBuffer;

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->boundShader:Lcom/jme3/shader/Shader;

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/jme3/shader/Shader;->getAttribute(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/shader/Attribute;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jme3/shader/Attribute;->getLocation()I

    move-result v0

    if-ne v0, v12, :cond_3

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "attribute is invalid in shader: [{0}]"

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jme3/scene/VertexBuffer$Type;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    const/4 v3, -0x2

    if-ne v0, v3, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "in"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jme3/scene/VertexBuffer$Type;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-boolean v3, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v3, :cond_4

    sget-object v3, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v5, "GLES20.glGetAttribLocation({0}, {1})"

    new-array v7, v11, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    aput-object v0, v7, v10

    invoke-virtual {v3, v4, v5, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    if-gez v0, :cond_5

    invoke-virtual {v2, v12}, Lcom/jme3/shader/Attribute;->setLocation(I)V

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "attribute is invalid in shader: [{0}]"

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jme3/scene/VertexBuffer$Type;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v2, v0}, Lcom/jme3/shader/Attribute;->setLocation(I)V

    :cond_6
    aget-object v1, v6, v0

    if-ne v1, p1, :cond_7

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->isUpdateNeeded()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_7
    if-eqz p2, :cond_9

    :goto_1
    invoke-virtual {p2}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/Buffer;->clear()Ljava/nio/Buffer;

    invoke-virtual {p2}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getOffset()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/nio/Buffer;->position(I)Ljava/nio/Buffer;

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_8

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v3, "GLES20.glVertexAttribPointer(location={0}, numComponents={1}, format={2}, isNormalized={3}, stride={4}, data.capacity={5})"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getNumComponents()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v10

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v5

    aput-object v5, v4, v11

    const/4 v5, 0x3

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->isNormalized()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x4

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getStride()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x5

    invoke-virtual {p2}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/nio/Buffer;->capacity()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_8
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getNumComponents()I

    move-result v1

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertFormat(Lcom/jme3/scene/VertexBuffer$Format;)I

    move-result v2

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->isNormalized()Z

    move-result v3

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getStride()I

    move-result v4

    invoke-virtual {p2}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    aput-object p1, v6, v0

    goto/16 :goto_0

    :cond_9
    move-object p2, p1

    goto/16 :goto_1

    :cond_a
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot render mesh without shader bound"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setViewPort(IIII)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->vpX:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->vpY:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->vpW:I

    if-ne v0, p3, :cond_0

    iget v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->vpH:I

    if-eq v0, p4, :cond_2

    :cond_0
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "GLES20.glViewport({0}, {1}, {2}, {3})"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES20;->glViewport(IIII)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iput p1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->vpX:I

    iput p2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->vpY:I

    iput p3, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->vpW:I

    iput p4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->vpH:I

    :cond_2
    return-void
.end method

.method public setViewProjectionMatrices(Lcom/jme3/math/Matrix4f;Lcom/jme3/math/Matrix4f;)V
    .locals 0
    .param p1    # Lcom/jme3/math/Matrix4f;
    .param p2    # Lcom/jme3/math/Matrix4f;

    return-void
.end method

.method public setWorldMatrix(Lcom/jme3/math/Matrix4f;)V
    .locals 0
    .param p1    # Lcom/jme3/math/Matrix4f;

    return-void
.end method

.method public updateBufferData(Lcom/jme3/scene/VertexBuffer;)V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v2, :cond_0

    sget-object v2, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateBufferData("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getId()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_14

    iget-boolean v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v2, :cond_1

    sget-object v2, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v3, "GLES20.glGenBuffers(1, buffer)"

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glGenBuffers(ILjava/nio/IntBuffer;)V

    iget-object v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-virtual {v2, v1}, Ljava/nio/IntBuffer;->get(I)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/jme3/scene/VertexBuffer;->setId(I)V

    iget-object v3, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->objManager:Lcom/jme3/util/NativeObjectManager;

    invoke-virtual {v3, p1}, Lcom/jme3/util/NativeObjectManager;->registerForCleanup(Lcom/jme3/util/NativeObject;)V

    :goto_0
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getBufferType()Lcom/jme3/scene/VertexBuffer$Type;

    move-result-object v3

    sget-object v4, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    if-ne v3, v4, :cond_5

    const v3, 0x8893

    iget-boolean v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v4, :cond_2

    sget-object v4, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v5, "vb.getBufferType() == VertexBuffer.Type.Index"

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_2
    iget-object v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget v4, v4, Lcom/jme3/renderer/RenderContext;->boundElementArrayVBO:I

    if-eq v4, v2, :cond_8

    iget-boolean v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v4, :cond_3

    sget-object v4, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GLES20.glBindBuffer("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_3
    invoke-static {v3, v2}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    iget-object v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput v2, v4, Lcom/jme3/renderer/RenderContext;->boundElementArrayVBO:I

    move v2, v3

    :goto_1
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getUsage()Lcom/jme3/scene/VertexBuffer$Usage;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertUsage(Lcom/jme3/scene/VertexBuffer$Usage;)I

    move-result v3

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/Buffer;->clear()Ljava/nio/Buffer;

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->hasDataSizeChanged()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_4
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/Buffer;->capacity()I

    move-result v0

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jme3/scene/VertexBuffer$Format;->getComponentSize()I

    move-result v1

    mul-int/2addr v1, v0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer$1;->$SwitchMap$com$jme3$scene$VertexBuffer$Format:[I

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jme3/scene/VertexBuffer$Format;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown buffer format."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iget-boolean v3, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v3, :cond_6

    sget-object v3, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v4, "vb.getBufferType() != VertexBuffer.Type.Index"

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_6
    const v3, 0x8892

    iget-object v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget v4, v4, Lcom/jme3/renderer/RenderContext;->boundArrayVBO:I

    if-eq v4, v2, :cond_8

    iget-boolean v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v4, :cond_7

    sget-object v4, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GLES20.glBindBuffer("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_7
    invoke-static {v3, v2}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    iget-object v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput v2, v4, Lcom/jme3/renderer/RenderContext;->boundArrayVBO:I

    :cond_8
    move v2, v3

    goto/16 :goto_1

    :pswitch_0
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_9

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glBufferData("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", (data), "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-static {v2, v1, v0, v3}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    :goto_2
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->clearUpdateNeeded()V

    return-void

    :pswitch_1
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_a

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glBufferData("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", (data), "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/ShortBuffer;

    invoke-static {v2, v1, v0, v3}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    goto :goto_2

    :pswitch_2
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_b

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glBufferData("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", (data), "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/IntBuffer;

    invoke-static {v2, v1, v0, v3}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    goto/16 :goto_2

    :pswitch_3
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_c

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glBufferData("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", (data), "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/FloatBuffer;

    invoke-static {v2, v1, v0, v3}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    goto/16 :goto_2

    :pswitch_4
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_d

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glBufferData("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", (data), "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/DoubleBuffer;

    invoke-static {v2, v1, v0, v3}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    goto/16 :goto_2

    :cond_e
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/Buffer;->capacity()I

    move-result v0

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jme3/scene/VertexBuffer$Format;->getComponentSize()I

    move-result v3

    mul-int/2addr v3, v0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer$1;->$SwitchMap$com$jme3$scene$VertexBuffer$Format:[I

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jme3/scene/VertexBuffer$Format;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown buffer format."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_5
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_f

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glBufferSubData("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", 0, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", (data))"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_f
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-static {v2, v1, v3, v0}, Landroid/opengl/GLES20;->glBufferSubData(IIILjava/nio/Buffer;)V

    goto/16 :goto_2

    :pswitch_6
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_10

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glBufferSubData("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", 0, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", (data))"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_10
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/ShortBuffer;

    invoke-static {v2, v1, v3, v0}, Landroid/opengl/GLES20;->glBufferSubData(IIILjava/nio/Buffer;)V

    goto/16 :goto_2

    :pswitch_7
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_11

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glBufferSubData("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", 0, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", (data))"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_11
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/IntBuffer;

    invoke-static {v2, v1, v3, v0}, Landroid/opengl/GLES20;->glBufferSubData(IIILjava/nio/Buffer;)V

    goto/16 :goto_2

    :pswitch_8
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_12

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glBufferSubData("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", 0, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", (data))"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_12
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/FloatBuffer;

    invoke-static {v2, v1, v3, v0}, Landroid/opengl/GLES20;->glBufferSubData(IIILjava/nio/Buffer;)V

    goto/16 :goto_2

    :pswitch_9
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_13

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glBufferSubData("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", 0, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", (data))"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_13
    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/DoubleBuffer;

    invoke-static {v2, v1, v3, v0}, Landroid/opengl/GLES20;->glBufferSubData(IIILjava/nio/Buffer;)V

    goto/16 :goto_2

    :cond_14
    move v0, v1

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method protected updateNameBuffer()V
    .locals 4

    iget-object v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->stringBuf:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    iget-object v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->nameBuf:Ljava/nio/ByteBuffer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->nameBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->nameBuf:Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->stringBuf:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->nameBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    return-void
.end method

.method public updateShaderData(Lcom/jme3/shader/Shader;)V
    .locals 9

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_11

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glCreateProgram()"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v0

    if-gtz v0, :cond_1

    new-instance v0, Lcom/jme3/renderer/RendererException;

    const-string v1, "Invalid ID received when trying to create shader program."

    invoke-direct {v0, v1}, Lcom/jme3/renderer/RendererException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p1, v0}, Lcom/jme3/shader/Shader;->setId(I)V

    move v1, v2

    move v4, v0

    :goto_0
    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getSources()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/shader/Shader$ShaderSource;

    invoke-virtual {v0}, Lcom/jme3/shader/Shader$ShaderSource;->isUpdateNeeded()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v0, v6}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->updateShaderSourceData(Lcom/jme3/shader/Shader$ShaderSource;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/jme3/shader/Shader$ShaderSource;->isUsable()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {p1, v3}, Lcom/jme3/shader/Shader;->setUsable(Z)V

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->clearUpdateNeeded()V

    :goto_2
    return-void

    :cond_3
    iget-boolean v6, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v6, :cond_4

    sget-object v6, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GLES20.glAttachShader("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/jme3/shader/Shader$ShaderSource;->getId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lcom/jme3/shader/Shader$ShaderSource;->getId()I

    move-result v0

    invoke-static {v4, v0}, Landroid/opengl/GLES20;->glAttachShader(II)V

    goto :goto_1

    :cond_5
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_6

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GLES20.glLinkProgram("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_6
    invoke-static {v4}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_7

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GLES20.glGetProgramiv("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_7
    const v0, 0x8b82

    iget-object v5, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-static {v4, v0, v5}, Landroid/opengl/GLES20;->glGetProgramiv(IILjava/nio/IntBuffer;)V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/IntBuffer;->get(I)I

    move-result v0

    if-ne v0, v2, :cond_b

    move v5, v2

    :goto_3
    const/4 v0, 0x0

    if-nez v5, :cond_a

    iget-boolean v6, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v6, :cond_8

    sget-object v6, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GLES20.glGetProgramiv("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", GLES20.GL_INFO_LOG_LENGTH, buffer)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_8
    const v6, 0x8b84

    iget-object v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-static {v4, v6, v7}, Landroid/opengl/GLES20;->glGetProgramiv(IILjava/nio/IntBuffer;)V

    iget-object v6, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-virtual {v6, v3}, Ljava/nio/IntBuffer;->get(I)I

    move-result v6

    const/4 v7, 0x3

    if-le v6, v7, :cond_a

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_9

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GLES20.glGetProgramInfoLog("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_9
    invoke-static {v4}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v0

    :cond_a
    if-eqz v5, :cond_d

    if-eqz v0, :cond_c

    sget-object v4, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v7, "shader link success. \n{0}"

    invoke-virtual {v4, v6, v7, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    :goto_4
    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->clearUpdateNeeded()V

    if-nez v5, :cond_f

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->resetSources()V

    invoke-virtual {p1, v3}, Lcom/jme3/shader/Shader;->setUsable(Z)V

    invoke-virtual {p0, p1}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->deleteShader(Lcom/jme3/shader/Shader;)V

    goto/16 :goto_2

    :cond_b
    move v5, v3

    goto :goto_3

    :cond_c
    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v4, "shader link success"

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    goto :goto_4

    :cond_d
    if-eqz v0, :cond_e

    new-instance v1, Lcom/jme3/renderer/RendererException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Shader link failure, shader:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " info:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/jme3/renderer/RendererException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_e
    new-instance v0, Lcom/jme3/renderer/RendererException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Shader link failure, shader:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " info: <not provided>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jme3/renderer/RendererException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    invoke-virtual {p1, v2}, Lcom/jme3/shader/Shader;->setUsable(Z)V

    if-eqz v1, :cond_10

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->objManager:Lcom/jme3/util/NativeObjectManager;

    invoke-virtual {v0, p1}, Lcom/jme3/util/NativeObjectManager;->registerForCleanup(Lcom/jme3/util/NativeObject;)V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    invoke-virtual {v0}, Lcom/jme3/renderer/Statistics;->onNewShader()V

    goto/16 :goto_2

    :cond_10
    invoke-virtual {p0, p1}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->resetUniformLocations(Lcom/jme3/shader/Shader;)V

    goto/16 :goto_2

    :cond_11
    move v1, v3

    move v4, v0

    goto/16 :goto_0
.end method

.method public updateShaderSourceData(Lcom/jme3/shader/Shader$ShaderSource;Ljava/lang/String;)V
    .locals 8

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GLES20.glCreateShader("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getType()Lcom/jme3/shader/Shader$ShaderType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getType()Lcom/jme3/shader/Shader$ShaderType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertShaderType(Lcom/jme3/shader/Shader$ShaderType;)I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v0

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    if-gtz v0, :cond_1

    new-instance v0, Lcom/jme3/renderer/RendererException;

    const-string v1, "Invalid ID received when trying to create shader."

    invoke-direct {v0, v1}, Lcom/jme3/renderer/RendererException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p1, v0}, Lcom/jme3/shader/Shader$ShaderSource;->setId(I)V

    :cond_2
    new-array v1, v4, [B

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getDefines()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getSource()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    array-length v6, v1

    array-length v7, v2

    add-int/2addr v6, v7

    array-length v7, v5

    add-int/2addr v6, v7

    invoke-static {v6}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    invoke-virtual {v6, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_3

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glShaderSource("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_3
    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->powerVr:Z

    if-eqz v1, :cond_a

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getType()Lcom/jme3/shader/Shader$ShaderType;

    move-result-object v1

    sget-object v2, Lcom/jme3/shader/Shader$ShaderType;->Vertex:Lcom/jme3/shader/Shader$ShaderType;

    if-ne v1, v2, :cond_a

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getDefines()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getSource()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    :goto_0
    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_4

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glCompileShader("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_4
    invoke-static {v0}, Landroid/opengl/GLES20;->glCompileShader(I)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_5

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glGetShaderiv("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", GLES20.GL_COMPILE_STATUS)"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_5
    const v1, 0x8b81

    iget-object v2, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glGetShaderiv(IILjava/nio/IntBuffer;)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-virtual {v1, v4}, Ljava/nio/IntBuffer;->get(I)I

    move-result v1

    if-ne v1, v3, :cond_b

    move v2, v3

    :goto_1
    const/4 v1, 0x0

    if-nez v2, :cond_8

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_6

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v5, "GLES20.glGetShaderiv()"

    invoke-virtual {v1, v5}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_6
    const v1, 0x8b84

    iget-object v5, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-static {v0, v1, v5}, Landroid/opengl/GLES20;->glGetShaderiv(IILjava/nio/IntBuffer;)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_7

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GLES20.glGetShaderInfoLog("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_7
    invoke-static {v0}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v1

    sget-object v5, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Errooooooooooot("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/logging/Logger;->severe(Ljava/lang/String;)V

    :cond_8
    if-eqz v2, :cond_d

    if-eqz v1, :cond_c

    sget-object v3, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "compile success: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :goto_2
    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->clearUpdateNeeded()V

    invoke-virtual {p1, v2}, Lcom/jme3/shader/Shader$ShaderSource;->setUsable(Z)V

    if-nez v2, :cond_f

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_9

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLES20.glDeleteShader("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_9
    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    :goto_3
    return-void

    :cond_a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "precision mediump float;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getDefines()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getSource()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    move v2, v4

    goto/16 :goto_1

    :cond_c
    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "compile success: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_2

    :cond_d
    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "Bad compile of:\n{0}{1}"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getDefines()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-virtual {p1}, Lcom/jme3/shader/Shader$ShaderSource;->getSource()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3

    invoke-virtual {v0, v2, v5, v6}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz v1, :cond_e

    new-instance v0, Lcom/jme3/renderer/RendererException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "compile error in:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " error:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jme3/renderer/RendererException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    new-instance v0, Lcom/jme3/renderer/RendererException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "compile error in:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " error: <not provided>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jme3/renderer/RendererException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->objManager:Lcom/jme3/util/NativeObjectManager;

    invoke-virtual {v0, p1}, Lcom/jme3/util/NativeObjectManager;->registerForCleanup(Lcom/jme3/util/NativeObject;)V

    goto/16 :goto_3
.end method

.method protected updateShaderUniforms(Lcom/jme3/shader/Shader;)V
    .locals 4
    .param p1    # Lcom/jme3/shader/Shader;

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getUniformMap()Lcom/jme3/util/ListMap;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lcom/jme3/util/ListMap;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-virtual {v2, v0}, Lcom/jme3/util/ListMap;->getValue(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/shader/Uniform;

    invoke-virtual {v1}, Lcom/jme3/shader/Uniform;->isUpdateNeeded()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, p1, v1}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->updateUniform(Lcom/jme3/shader/Shader;Lcom/jme3/shader/Uniform;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public updateTexImageData(Lcom/jme3/texture/Image;Lcom/jme3/texture/Texture$Type;Z)V
    .locals 9

    const v8, 0x8515

    const/4 v7, 0x6

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/jme3/texture/Image;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glGenTexture(1, buffer)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGenTextures(ILjava/nio/IntBuffer;)V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->intBuf1:Ljava/nio/IntBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/IntBuffer;->get(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/jme3/texture/Image;->setId(I)V

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->objManager:Lcom/jme3/util/NativeObjectManager;

    invoke-virtual {v1, p1}, Lcom/jme3/util/NativeObjectManager;->registerForCleanup(Lcom/jme3/util/NativeObject;)V

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    invoke-virtual {v1}, Lcom/jme3/renderer/Statistics;->onNewTexture()V

    :cond_1
    invoke-direct {p0, p2}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->convertTextureType(Lcom/jme3/texture/Texture$Type;)I

    move-result v2

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-object v1, v1, Lcom/jme3/renderer/RenderContext;->boundTextures:[Lcom/jme3/texture/Image;

    aget-object v1, v1, v3

    if-eq v1, p1, :cond_5

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget v1, v1, Lcom/jme3/renderer/RenderContext;->boundTextureUnit:I

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v4, "GLES20.glActiveTexture(GLES20.GL_TEXTURE0)"

    invoke-virtual {v1, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_2
    const v1, 0x84c0

    invoke-static {v1}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput v3, v1, Lcom/jme3/renderer/RenderContext;->boundTextureUnit:I

    :cond_3
    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_4

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glBindTexture("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_4
    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget-object v0, v0, Lcom/jme3/renderer/RenderContext;->boundTextures:[Lcom/jme3/texture/Image;

    aput-object p1, v0, v3

    :cond_5
    const v0, 0x8513

    if-ne v2, v0, :cond_9

    invoke-virtual {p1}, Lcom/jme3/texture/Image;->getEfficentData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_7

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-eq v1, v7, :cond_6

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid texture: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Cubemap textures must contain 6 data units."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move v2, v3

    :goto_0
    if-ge v2, v7, :cond_c

    add-int v4, v8, v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/asset/AndroidImageInfo;

    invoke-virtual {v1}, Lcom/jme3/asset/AndroidImageInfo;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iget-boolean v5, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->powerOf2:Z

    invoke-static {v4, v1, v3, v5}, Lcom/jme3/renderer/android/TextureUtil;->uploadTextureBitmap(ILandroid/graphics/Bitmap;ZZ)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_7
    invoke-virtual {p1}, Lcom/jme3/texture/Image;->getData()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v7, :cond_8

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "Invalid texture: {0}\nCubemap textures must contain 6 data units."

    invoke-virtual {v0, v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_8
    move v2, v3

    :goto_2
    if-ge v2, v7, :cond_c

    add-int v1, v8, v2

    iget-boolean v4, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->tdc:Z

    iget-boolean v6, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->powerOf2:Z

    move-object v0, p1

    move v5, v3

    invoke-static/range {v0 .. v6}, Lcom/jme3/renderer/android/TextureUtil;->uploadTexture(Lcom/jme3/texture/Image;IIIZZZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_9
    iget-boolean v5, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->tdc:Z

    iget-boolean v7, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->powerOf2:Z

    move-object v1, p1

    move v4, v3

    move v6, v3

    invoke-static/range {v1 .. v7}, Lcom/jme3/renderer/android/TextureUtil;->uploadTexture(Lcom/jme3/texture/Image;IIIZZZ)V

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_a

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLES20.glTexParameteri("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "GLES11.GL_GENERATE_MIMAP, GLES20.GL_TRUE)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p1}, Lcom/jme3/texture/Image;->hasMipmaps()Z

    move-result v0

    if-nez v0, :cond_c

    if-eqz p3, :cond_c

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_b

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "GLES20.glGenerateMipmap(GLES20.GL_TEXTURE_2D)"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_b
    const/16 v0, 0xde1

    invoke-static {v0}, Landroid/opengl/GLES20;->glGenerateMipmap(I)V

    :cond_c
    invoke-virtual {p1}, Lcom/jme3/texture/Image;->clearUpdateNeeded()V

    goto :goto_1
.end method

.method protected updateUniform(Lcom/jme3/shader/Shader;Lcom/jme3/shader/Uniform;)V
    .locals 8

    const/4 v7, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getId()I

    move-result v0

    sget-boolean v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getId()I

    move-result v1

    if-gtz v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iget v1, v1, Lcom/jme3/renderer/RenderContext;->boundShaderProgram:I

    if-eq v1, v0, :cond_4

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v5, "GLES20.glUseProgram({0})"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v4, v5, v6}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_2
    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    invoke-virtual {v1, p1, v2}, Lcom/jme3/renderer/Statistics;->onShaderUse(Lcom/jme3/shader/Shader;Z)V

    iput-object p1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->boundShader:Lcom/jme3/shader/Shader;

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->context:Lcom/jme3/renderer/RenderContext;

    iput v0, v1, Lcom/jme3/renderer/RenderContext;->boundShaderProgram:I

    :goto_0
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getLocation()I

    move-result v0

    if-ne v0, v7, :cond_5

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "no location for uniform [{0}]"

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    invoke-virtual {v0, p1, v3}, Lcom/jme3/renderer/Statistics;->onShaderUse(Lcom/jme3/shader/Shader;Z)V

    goto :goto_0

    :cond_5
    const/4 v1, -0x2

    if-ne v0, v1, :cond_1a

    invoke-virtual {p0, p1, p2}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->updateUniformLocation(Lcom/jme3/shader/Shader;Lcom/jme3/shader/Uniform;)V

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getLocation()I

    move-result v0

    if-ne v0, v7, :cond_7

    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_6

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "not declared uniform: [{0}]"

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_6
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->clearUpdateNeeded()V

    goto :goto_1

    :cond_7
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getLocation()I

    move-result v0

    move v1, v0

    :goto_2
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getVarType()Lcom/jme3/shader/VarType;

    move-result-object v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    const-string v1, "value is not set yet."

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->statistics:Lcom/jme3/renderer/Statistics;

    invoke-virtual {v0}, Lcom/jme3/renderer/Statistics;->onUniformSet()V

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->clearUpdateNeeded()V

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer$1;->$SwitchMap$com$jme3$shader$VarType:[I

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getVarType()Lcom/jme3/shader/VarType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jme3/shader/VarType;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported uniform type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getVarType()Lcom/jme3/shader/VarType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_9

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLES20.glUniform1f set Float. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    :goto_3
    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    goto/16 :goto_1

    :pswitch_1
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_a

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLES20.glUniform2f set Vector2. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/math/Vector2f;

    invoke-virtual {v0}, Lcom/jme3/math/Vector2f;->getX()F

    move-result v2

    invoke-virtual {v0}, Lcom/jme3/math/Vector2f;->getY()F

    move-result v0

    invoke-static {v1, v2, v0}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    goto :goto_3

    :pswitch_2
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_b

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLES20.glUniform3f set Vector3. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/math/Vector3f;

    invoke-virtual {v0}, Lcom/jme3/math/Vector3f;->getX()F

    move-result v2

    invoke-virtual {v0}, Lcom/jme3/math/Vector3f;->getY()F

    move-result v3

    invoke-virtual {v0}, Lcom/jme3/math/Vector3f;->getZ()F

    move-result v0

    invoke-static {v1, v2, v3, v0}, Landroid/opengl/GLES20;->glUniform3f(IFFF)V

    goto :goto_3

    :pswitch_3
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_c

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLES20.glUniform4f set Vector4."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getValue()Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Lcom/jme3/math/ColorRGBA;

    if-eqz v2, :cond_d

    check-cast v0, Lcom/jme3/math/ColorRGBA;

    iget v2, v0, Lcom/jme3/math/ColorRGBA;->r:F

    iget v3, v0, Lcom/jme3/math/ColorRGBA;->g:F

    iget v4, v0, Lcom/jme3/math/ColorRGBA;->b:F

    iget v0, v0, Lcom/jme3/math/ColorRGBA;->a:F

    invoke-static {v1, v2, v3, v4, v0}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    goto/16 :goto_3

    :cond_d
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/math/Quaternion;

    invoke-virtual {v0}, Lcom/jme3/math/Quaternion;->getX()F

    move-result v2

    invoke-virtual {v0}, Lcom/jme3/math/Quaternion;->getY()F

    move-result v3

    invoke-virtual {v0}, Lcom/jme3/math/Quaternion;->getZ()F

    move-result v4

    invoke-virtual {v0}, Lcom/jme3/math/Quaternion;->getW()F

    move-result v0

    invoke-static {v1, v2, v3, v4, v0}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    goto/16 :goto_3

    :pswitch_4
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_e

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glUniform1i set Boolean."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_e
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_f

    move v0, v2

    :goto_4
    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glUniform1i(II)V

    goto/16 :goto_3

    :cond_f
    move v0, v3

    goto :goto_4

    :pswitch_5
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_10

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glUniformMatrix3fv set Matrix3."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_10
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/FloatBuffer;

    sget-boolean v4, Lcom/jme3/renderer/android/OGLESShaderRenderer;->$assertionsDisabled:Z

    if-nez v4, :cond_11

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->remaining()I

    move-result v4

    const/16 v5, 0x9

    if-eq v4, v5, :cond_11

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_11
    invoke-static {v1, v2, v3, v0}, Landroid/opengl/GLES20;->glUniformMatrix3fv(IIZLjava/nio/FloatBuffer;)V

    goto/16 :goto_3

    :pswitch_6
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_12

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GLES20.glUniformMatrix4fv set Matrix4."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_12
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/FloatBuffer;

    sget-boolean v4, Lcom/jme3/renderer/android/OGLESShaderRenderer;->$assertionsDisabled:Z

    if-nez v4, :cond_13

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->remaining()I

    move-result v4

    const/16 v5, 0x10

    if-eq v4, v5, :cond_13

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_13
    invoke-static {v1, v2, v3, v0}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZLjava/nio/FloatBuffer;)V

    goto/16 :goto_3

    :pswitch_7
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_14

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLES20.glUniform1fv set FloatArray."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_14
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v2

    invoke-static {v1, v2, v0}, Landroid/opengl/GLES20;->glUniform1fv(IILjava/nio/FloatBuffer;)V

    goto/16 :goto_3

    :pswitch_8
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_15

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLES20.glUniform2fv set Vector2Array."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_15
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-static {v1, v2, v0}, Landroid/opengl/GLES20;->glUniform2fv(IILjava/nio/FloatBuffer;)V

    goto/16 :goto_3

    :pswitch_9
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_16

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLES20.glUniform3fv set Vector3Array."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_16
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v2

    div-int/lit8 v2, v2, 0x3

    invoke-static {v1, v2, v0}, Landroid/opengl/GLES20;->glUniform3fv(IILjava/nio/FloatBuffer;)V

    goto/16 :goto_3

    :pswitch_a
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_17

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLES20.glUniform4fv set Vector4Array."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_17
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v2

    div-int/lit8 v2, v2, 0x4

    invoke-static {v1, v2, v0}, Landroid/opengl/GLES20;->glUniform4fv(IILjava/nio/FloatBuffer;)V

    goto/16 :goto_3

    :pswitch_b
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_18

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GLES20.glUniform4fv set Matrix4Array."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_18
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v2

    div-int/lit8 v2, v2, 0x10

    invoke-static {v1, v2, v3, v0}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZLjava/nio/FloatBuffer;)V

    goto/16 :goto_3

    :pswitch_c
    iget-boolean v0, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v0, :cond_19

    sget-object v0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLES20.glUniform1i set Int."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_19
    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glUniform1i(II)V

    goto/16 :goto_3

    :cond_1a
    move v1, v0

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method protected updateUniformLocation(Lcom/jme3/shader/Shader;Lcom/jme3/shader/Uniform;)V
    .locals 7
    .param p1    # Lcom/jme3/shader/Shader;
    .param p2    # Lcom/jme3/shader/Uniform;

    const/4 v6, 0x0

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->stringBuf:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->stringBuf:Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->updateNameBuffer()V

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v3, "GLES20.glGetUniformLocation({0}, {1})"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Lcom/jme3/shader/Shader;->getId()I

    move-result v1

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    invoke-direct {p0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->checkGLError()V

    if-gez v0, :cond_2

    const/4 v1, -0x1

    invoke-virtual {p2, v1}, Lcom/jme3/shader/Uniform;->setLocation(I)V

    iget-boolean v1, p0, Lcom/jme3/renderer/android/OGLESShaderRenderer;->verboseLogging:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/jme3/renderer/android/OGLESShaderRenderer;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Uniform [{0}] is not declared in shader."

    invoke-virtual {p2}, Lcom/jme3/shader/Uniform;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p2, v0}, Lcom/jme3/shader/Uniform;->setLocation(I)V

    goto :goto_0
.end method
