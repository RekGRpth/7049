.class public Lcom/jme3/scene/BatchNode;
.super Lcom/jme3/scene/Node;
.source "BatchNode.java"

# interfaces
.implements Lcom/jme3/export/Savable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/scene/BatchNode$Batch;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field protected batches:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/jme3/material/Material;",
            "Lcom/jme3/scene/BatchNode$Batch;",
            ">;"
        }
    .end annotation
.end field

.field maxVertCount:I

.field needsFullRebatch:Z

.field private tmpFloat:[F

.field private tmpFloatN:[F

.field private tmpFloatT:[F

.field useTangents:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/scene/BatchNode;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/jme3/scene/BatchNode;->$assertionsDisabled:Z

    const-class v0, Lcom/jme3/scene/BatchNode;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/scene/BatchNode;->logger:Ljava/util/logging/Logger;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/jme3/scene/Node;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/BatchNode;->batches:Ljava/util/Map;

    iput v1, p0, Lcom/jme3/scene/BatchNode;->maxVertCount:I

    iput-boolean v1, p0, Lcom/jme3/scene/BatchNode;->useTangents:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/scene/BatchNode;->needsFullRebatch:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/jme3/scene/Node;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/BatchNode;->batches:Ljava/util/Map;

    iput v1, p0, Lcom/jme3/scene/BatchNode;->maxVertCount:I

    iput-boolean v1, p0, Lcom/jme3/scene/BatchNode;->useTangents:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/scene/BatchNode;->needsFullRebatch:Z

    return-void
.end method

.method private doTransforms(Ljava/nio/FloatBuffer;Ljava/nio/FloatBuffer;IILcom/jme3/math/Matrix4f;)V
    .locals 10
    .param p1    # Ljava/nio/FloatBuffer;
    .param p2    # Ljava/nio/FloatBuffer;
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/jme3/math/Matrix4f;

    const/4 v9, 0x0

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v6

    iget-object v5, v6, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget-object v3, v6, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    sub-int v7, p4, p3

    mul-int/lit8 v2, v7, 0x3

    mul-int/lit8 v4, p3, 0x3

    invoke-virtual {p1, v4}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    invoke-virtual {p2, v4}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    invoke-virtual {p1, v7, v9, v2}, Ljava/nio/FloatBuffer;->get([FII)Ljava/nio/FloatBuffer;

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    invoke-virtual {p2, v7, v9, v2}, Ljava/nio/FloatBuffer;->get([FII)Ljava/nio/FloatBuffer;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    aget v7, v7, v1

    iput v7, v5, Lcom/jme3/math/Vector3f;->x:F

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    add-int/lit8 v0, v1, 0x1

    aget v7, v7, v1

    iput v7, v3, Lcom/jme3/math/Vector3f;->x:F

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    aget v7, v7, v0

    iput v7, v5, Lcom/jme3/math/Vector3f;->y:F

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    add-int/lit8 v1, v0, 0x1

    aget v7, v7, v0

    iput v7, v3, Lcom/jme3/math/Vector3f;->y:F

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    aget v7, v7, v1

    iput v7, v5, Lcom/jme3/math/Vector3f;->z:F

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    aget v7, v7, v1

    iput v7, v3, Lcom/jme3/math/Vector3f;->z:F

    invoke-virtual {p5, v5, v5}, Lcom/jme3/math/Matrix4f;->mult(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    invoke-virtual {p5, v3, v3}, Lcom/jme3/math/Matrix4f;->multNormal(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    add-int/lit8 v0, v1, -0x2

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    iget v8, v5, Lcom/jme3/math/Vector3f;->x:F

    aput v8, v7, v0

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    add-int/lit8 v1, v0, 0x1

    iget v8, v3, Lcom/jme3/math/Vector3f;->x:F

    aput v8, v7, v0

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    iget v8, v5, Lcom/jme3/math/Vector3f;->y:F

    aput v8, v7, v1

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    add-int/lit8 v0, v1, 0x1

    iget v8, v3, Lcom/jme3/math/Vector3f;->y:F

    aput v8, v7, v1

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    iget v8, v5, Lcom/jme3/math/Vector3f;->z:F

    aput v8, v7, v0

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    add-int/lit8 v1, v0, 0x1

    iget v8, v3, Lcom/jme3/math/Vector3f;->z:F

    aput v8, v7, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v6}, Lcom/jme3/util/TempVars;->release()V

    invoke-virtual {p1, v4}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    invoke-virtual {p1, v7, v9, v2}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    invoke-virtual {p2, v4}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v7, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    invoke-virtual {p2, v7, v9, v2}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    return-void
.end method

.method private doTransformsTangents(Ljava/nio/FloatBuffer;Ljava/nio/FloatBuffer;Ljava/nio/FloatBuffer;IILcom/jme3/math/Matrix4f;)V
    .locals 15
    .param p1    # Ljava/nio/FloatBuffer;
    .param p2    # Ljava/nio/FloatBuffer;
    .param p3    # Ljava/nio/FloatBuffer;
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/jme3/math/Matrix4f;

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v12

    iget-object v6, v12, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget-object v4, v12, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    iget-object v7, v12, Lcom/jme3/util/TempVars;->vect3:Lcom/jme3/math/Vector3f;

    sub-int v13, p5, p4

    mul-int/lit8 v3, v13, 0x3

    sub-int v13, p5, p4

    mul-int/lit8 v10, v13, 0x4

    mul-int/lit8 v5, p4, 0x3

    mul-int/lit8 v11, p4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14, v3}, Ljava/nio/FloatBuffer;->get([FII)Ljava/nio/FloatBuffer;

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14, v3}, Ljava/nio/FloatBuffer;->get([FII)Ljava/nio/FloatBuffer;

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatT:[F

    const/4 v14, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v13, v14, v10}, Ljava/nio/FloatBuffer;->get([FII)Ljava/nio/FloatBuffer;

    const/4 v1, 0x0

    const/4 v8, 0x0

    move v9, v8

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    aget v13, v13, v2

    iput v13, v6, Lcom/jme3/math/Vector3f;->x:F

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    add-int/lit8 v1, v2, 0x1

    aget v13, v13, v2

    iput v13, v4, Lcom/jme3/math/Vector3f;->x:F

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    aget v13, v13, v1

    iput v13, v6, Lcom/jme3/math/Vector3f;->y:F

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    add-int/lit8 v2, v1, 0x1

    aget v13, v13, v1

    iput v13, v4, Lcom/jme3/math/Vector3f;->y:F

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    aget v13, v13, v2

    iput v13, v6, Lcom/jme3/math/Vector3f;->z:F

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    aget v13, v13, v2

    iput v13, v4, Lcom/jme3/math/Vector3f;->z:F

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatT:[F

    add-int/lit8 v8, v9, 0x1

    aget v13, v13, v9

    iput v13, v7, Lcom/jme3/math/Vector3f;->x:F

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatT:[F

    add-int/lit8 v9, v8, 0x1

    aget v13, v13, v8

    iput v13, v7, Lcom/jme3/math/Vector3f;->y:F

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatT:[F

    add-int/lit8 v8, v9, 0x1

    aget v13, v13, v9

    iput v13, v7, Lcom/jme3/math/Vector3f;->z:F

    move-object/from16 v0, p6

    invoke-virtual {v0, v6, v6}, Lcom/jme3/math/Matrix4f;->mult(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4, v4}, Lcom/jme3/math/Matrix4f;->multNormal(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p6

    invoke-virtual {v0, v7, v7}, Lcom/jme3/math/Matrix4f;->multNormal(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    add-int/lit8 v1, v2, -0x2

    add-int/lit8 v8, v8, -0x3

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    iget v14, v6, Lcom/jme3/math/Vector3f;->x:F

    aput v14, v13, v1

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    add-int/lit8 v2, v1, 0x1

    iget v14, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v14, v13, v1

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    iget v14, v6, Lcom/jme3/math/Vector3f;->y:F

    aput v14, v13, v2

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    add-int/lit8 v1, v2, 0x1

    iget v14, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v14, v13, v2

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    iget v14, v6, Lcom/jme3/math/Vector3f;->z:F

    aput v14, v13, v1

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    add-int/lit8 v2, v1, 0x1

    iget v14, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v14, v13, v1

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatT:[F

    add-int/lit8 v9, v8, 0x1

    iget v14, v7, Lcom/jme3/math/Vector3f;->x:F

    aput v14, v13, v8

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatT:[F

    add-int/lit8 v8, v9, 0x1

    iget v14, v7, Lcom/jme3/math/Vector3f;->y:F

    aput v14, v13, v9

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatT:[F

    add-int/lit8 v9, v8, 0x1

    iget v14, v7, Lcom/jme3/math/Vector3f;->z:F

    aput v14, v13, v8

    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto/16 :goto_0

    :cond_0
    invoke-virtual {v12}, Lcom/jme3/util/TempVars;->release()V

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloat:[F

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14, v3}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatN:[F

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14, v3}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v13, p0, Lcom/jme3/scene/BatchNode;->tmpFloatT:[F

    const/4 v14, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v13, v14, v10}, Ljava/nio/FloatBuffer;->put([FII)Ljava/nio/FloatBuffer;

    return-void
.end method


# virtual methods
.method protected getTransforms(Lcom/jme3/scene/Geometry;)Lcom/jme3/math/Transform;
    .locals 1
    .param p1    # Lcom/jme3/scene/Geometry;

    invoke-virtual {p1}, Lcom/jme3/scene/Geometry;->getWorldTransform()Lcom/jme3/math/Transform;

    move-result-object v0

    return-object v0
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 1
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/jme3/scene/Node;->read(Lcom/jme3/export/JmeImporter;)V

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v0

    return-void
.end method

.method public setMaterial(Lcom/jme3/material/Material;)V
    .locals 2
    .param p1    # Lcom/jme3/material/Material;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unsupported for now, please set the material on the geoms before batching"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected setNeedsFullRebatch(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jme3/scene/BatchNode;->needsFullRebatch:Z

    return-void
.end method

.method public updateGeometricState()V
    .locals 6

    iget v5, p0, Lcom/jme3/scene/BatchNode;->refreshFlags:I

    and-int/lit8 v5, v5, 0x4

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/jme3/scene/BatchNode;->updateWorldLightList()V

    :cond_0
    iget v5, p0, Lcom/jme3/scene/BatchNode;->refreshFlags:I

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/jme3/scene/BatchNode;->updateWorldTransforms()V

    :cond_1
    iget-object v5, p0, Lcom/jme3/scene/BatchNode;->children:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v5}, Lcom/jme3/util/SafeArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/jme3/scene/BatchNode;->children:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v5}, Lcom/jme3/util/SafeArrayList;->getArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jme3/scene/Spatial;

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v2, v0, v3

    invoke-virtual {v2}, Lcom/jme3/scene/Spatial;->updateGeometricState()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/jme3/scene/BatchNode;->batches:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/scene/BatchNode$Batch;

    iget-boolean v5, v1, Lcom/jme3/scene/BatchNode$Batch;->needMeshUpdate:Z

    if-eqz v5, :cond_3

    iget-object v5, v1, Lcom/jme3/scene/BatchNode$Batch;->geometry:Lcom/jme3/scene/Geometry;

    invoke-virtual {v5}, Lcom/jme3/scene/Geometry;->getMesh()Lcom/jme3/scene/Mesh;

    move-result-object v5

    invoke-virtual {v5}, Lcom/jme3/scene/Mesh;->updateBound()V

    iget-object v5, v1, Lcom/jme3/scene/BatchNode$Batch;->geometry:Lcom/jme3/scene/Geometry;

    invoke-virtual {v5}, Lcom/jme3/scene/Geometry;->updateWorldBound()V

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/jme3/scene/BatchNode$Batch;->needMeshUpdate:Z

    goto :goto_1

    :cond_4
    iget v5, p0, Lcom/jme3/scene/BatchNode;->refreshFlags:I

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_5

    invoke-virtual {p0}, Lcom/jme3/scene/BatchNode;->updateWorldBound()V

    :cond_5
    sget-boolean v5, Lcom/jme3/scene/BatchNode;->$assertionsDisabled:Z

    if-nez v5, :cond_6

    iget v5, p0, Lcom/jme3/scene/BatchNode;->refreshFlags:I

    if-eqz v5, :cond_6

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    :cond_6
    return-void
.end method

.method protected updateSubBatch(Lcom/jme3/scene/Geometry;)V
    .locals 16
    .param p1    # Lcom/jme3/scene/Geometry;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jme3/scene/BatchNode;->batches:Ljava/util/Map;

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/Geometry;->getMaterial()Lcom/jme3/material/Material;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/jme3/scene/BatchNode$Batch;

    if-eqz v11, :cond_0

    iget-object v1, v11, Lcom/jme3/scene/BatchNode$Batch;->geometry:Lcom/jme3/scene/Geometry;

    invoke-virtual {v1}, Lcom/jme3/scene/Geometry;->getMesh()Lcom/jme3/scene/Mesh;

    move-result-object v12

    sget-object v1, Lcom/jme3/scene/VertexBuffer$Type;->Position:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {v12, v1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v14

    invoke-virtual {v14}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v2

    check-cast v2, Ljava/nio/FloatBuffer;

    sget-object v1, Lcom/jme3/scene/VertexBuffer$Type;->Normal:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {v12, v1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v13

    invoke-virtual {v13}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v3

    check-cast v3, Ljava/nio/FloatBuffer;

    sget-object v1, Lcom/jme3/scene/VertexBuffer$Type;->Tangent:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {v12, v1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/jme3/scene/VertexBuffer$Type;->Tangent:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {v12, v1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v15

    invoke-virtual {v15}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v4

    check-cast v4, Ljava/nio/FloatBuffer;

    move-object/from16 v0, p1

    iget v5, v0, Lcom/jme3/scene/Geometry;->startIndex:I

    move-object/from16 v0, p1

    iget v1, v0, Lcom/jme3/scene/Geometry;->startIndex:I

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/Geometry;->getVertexCount()I

    move-result v6

    add-int/2addr v6, v1

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/jme3/scene/Geometry;->cachedOffsetMat:Lcom/jme3/math/Matrix4f;

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/jme3/scene/BatchNode;->doTransformsTangents(Ljava/nio/FloatBuffer;Ljava/nio/FloatBuffer;Ljava/nio/FloatBuffer;IILcom/jme3/math/Matrix4f;)V

    invoke-virtual {v15, v4}, Lcom/jme3/scene/VertexBuffer;->updateData(Ljava/nio/Buffer;)V

    :goto_0
    invoke-virtual {v14, v2}, Lcom/jme3/scene/VertexBuffer;->updateData(Ljava/nio/Buffer;)V

    invoke-virtual {v13, v3}, Lcom/jme3/scene/VertexBuffer;->updateData(Ljava/nio/Buffer;)V

    const/4 v1, 0x1

    iput-boolean v1, v11, Lcom/jme3/scene/BatchNode$Batch;->needMeshUpdate:Z

    :cond_0
    return-void

    :cond_1
    move-object/from16 v0, p1

    iget v8, v0, Lcom/jme3/scene/Geometry;->startIndex:I

    move-object/from16 v0, p1

    iget v1, v0, Lcom/jme3/scene/Geometry;->startIndex:I

    invoke-virtual/range {p1 .. p1}, Lcom/jme3/scene/Geometry;->getVertexCount()I

    move-result v5

    add-int v9, v1, v5

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/jme3/scene/Geometry;->cachedOffsetMat:Lcom/jme3/math/Matrix4f;

    move-object/from16 v5, p0

    move-object v6, v2

    move-object v7, v3

    invoke-direct/range {v5 .. v10}, Lcom/jme3/scene/BatchNode;->doTransforms(Ljava/nio/FloatBuffer;Ljava/nio/FloatBuffer;IILcom/jme3/math/Matrix4f;)V

    goto :goto_0
.end method
