.class public abstract Lcom/jme3/scene/Spatial;
.super Ljava/lang/Object;
.source "Spatial.java"

# interfaces
.implements Lcom/jme3/asset/Asset;
.implements Lcom/jme3/collision/Collidable;
.implements Lcom/jme3/export/Savable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/scene/Spatial$BatchHint;,
        Lcom/jme3/scene/Spatial$CullHint;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field protected batchHint:Lcom/jme3/scene/Spatial$BatchHint;

.field protected controls:Lcom/jme3/util/SafeArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/SafeArrayList",
            "<",
            "Lcom/jme3/scene/control/Control;",
            ">;"
        }
    .end annotation
.end field

.field protected cullHint:Lcom/jme3/scene/Spatial$CullHint;

.field protected transient frustrumIntersects:Lcom/jme3/renderer/Camera$FrustumIntersect;

.field protected key:Lcom/jme3/asset/AssetKey;

.field protected localLights:Lcom/jme3/light/LightList;

.field protected localTransform:Lcom/jme3/math/Transform;

.field protected name:Ljava/lang/String;

.field protected transient parent:Lcom/jme3/scene/Node;

.field protected queueBucket:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

.field public transient queueDistance:F

.field protected transient refreshFlags:I

.field protected shadowMode:Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;

.field protected userData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/jme3/export/Savable;",
            ">;"
        }
    .end annotation
.end field

.field protected worldBound:Lcom/jme3/bounding/BoundingVolume;

.field protected transient worldLights:Lcom/jme3/light/LightList;

.field protected worldTransform:Lcom/jme3/math/Transform;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/scene/Spatial;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/jme3/scene/Spatial;->$assertionsDisabled:Z

    const-class v0, Lcom/jme3/scene/Spatial;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/scene/Spatial;->logger:Ljava/util/logging/Logger;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/jme3/scene/Spatial$CullHint;->Inherit:Lcom/jme3/scene/Spatial$CullHint;

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->cullHint:Lcom/jme3/scene/Spatial$CullHint;

    sget-object v0, Lcom/jme3/scene/Spatial$BatchHint;->Inherit:Lcom/jme3/scene/Spatial$BatchHint;

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->batchHint:Lcom/jme3/scene/Spatial$BatchHint;

    sget-object v0, Lcom/jme3/renderer/Camera$FrustumIntersect;->Intersects:Lcom/jme3/renderer/Camera$FrustumIntersect;

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->frustrumIntersects:Lcom/jme3/renderer/Camera$FrustumIntersect;

    sget-object v0, Lcom/jme3/renderer/queue/RenderQueue$Bucket;->Inherit:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->queueBucket:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    sget-object v0, Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;->Inherit:Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->shadowMode:Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;

    const/high16 v0, -0x800000

    iput v0, p0, Lcom/jme3/scene/Spatial;->queueDistance:F

    new-instance v0, Lcom/jme3/util/SafeArrayList;

    const-class v1, Lcom/jme3/scene/control/Control;

    invoke-direct {v0, v1}, Lcom/jme3/util/SafeArrayList;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->controls:Lcom/jme3/util/SafeArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->userData:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    new-instance v0, Lcom/jme3/math/Transform;

    invoke-direct {v0}, Lcom/jme3/math/Transform;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->localTransform:Lcom/jme3/math/Transform;

    new-instance v0, Lcom/jme3/math/Transform;

    invoke-direct {v0}, Lcom/jme3/math/Transform;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->worldTransform:Lcom/jme3/math/Transform;

    new-instance v0, Lcom/jme3/light/LightList;

    invoke-direct {v0, p0}, Lcom/jme3/light/LightList;-><init>(Lcom/jme3/scene/Spatial;)V

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->localLights:Lcom/jme3/light/LightList;

    new-instance v0, Lcom/jme3/light/LightList;

    invoke-direct {v0, p0}, Lcom/jme3/light/LightList;-><init>(Lcom/jme3/scene/Spatial;)V

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->worldLights:Lcom/jme3/light/LightList;

    iget v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/jme3/scene/Spatial;-><init>()V

    iput-object p1, p0, Lcom/jme3/scene/Spatial;->name:Ljava/lang/String;

    return-void
.end method

.method private runControlUpdate(F)V
    .locals 5
    .param p1    # F

    iget-object v4, p0, Lcom/jme3/scene/Spatial;->controls:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v4}, Lcom/jme3/util/SafeArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/jme3/scene/Spatial;->controls:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v4}, Lcom/jme3/util/SafeArrayList;->getArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jme3/scene/control/Control;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    invoke-interface {v1, p1}, Lcom/jme3/scene/control/Control;->update(F)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addControl(Lcom/jme3/scene/control/Control;)V
    .locals 1
    .param p1    # Lcom/jme3/scene/control/Control;

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->controls:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v0, p1}, Lcom/jme3/util/SafeArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p1, p0}, Lcom/jme3/scene/control/Control;->setSpatial(Lcom/jme3/scene/Spatial;)V

    return-void
.end method

.method public addLight(Lcom/jme3/light/Light;)V
    .locals 1
    .param p1    # Lcom/jme3/light/Light;

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->localLights:Lcom/jme3/light/LightList;

    invoke-virtual {v0, p1}, Lcom/jme3/light/LightList;->add(Lcom/jme3/light/Light;)V

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->setLightListRefresh()V

    return-void
.end method

.method public checkCulling(Lcom/jme3/renderer/Camera;)Z
    .locals 5
    .param p1    # Lcom/jme3/renderer/Camera;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget v1, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Scene graph is not properly updated for rendering.\nState was changed after rootNode.updateGeometricState() call. \nMake sure you do not modify the scene from another thread!\nProblem spatial name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->getCullHint()Lcom/jme3/scene/Spatial$CullHint;

    move-result-object v0

    sget-boolean v1, Lcom/jme3/scene/Spatial;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    sget-object v1, Lcom/jme3/scene/Spatial$CullHint;->Inherit:Lcom/jme3/scene/Spatial$CullHint;

    if-ne v0, v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_1
    sget-object v1, Lcom/jme3/scene/Spatial$CullHint;->Always:Lcom/jme3/scene/Spatial$CullHint;

    if-ne v0, v1, :cond_2

    sget-object v1, Lcom/jme3/renderer/Camera$FrustumIntersect;->Outside:Lcom/jme3/renderer/Camera$FrustumIntersect;

    invoke-virtual {p0, v1}, Lcom/jme3/scene/Spatial;->setLastFrustumIntersection(Lcom/jme3/renderer/Camera$FrustumIntersect;)V

    :goto_0
    return v3

    :cond_2
    sget-object v1, Lcom/jme3/scene/Spatial$CullHint;->Never:Lcom/jme3/scene/Spatial$CullHint;

    if-ne v0, v1, :cond_3

    sget-object v1, Lcom/jme3/renderer/Camera$FrustumIntersect;->Intersects:Lcom/jme3/renderer/Camera$FrustumIntersect;

    invoke-virtual {p0, v1}, Lcom/jme3/scene/Spatial;->setLastFrustumIntersection(Lcom/jme3/renderer/Camera$FrustumIntersect;)V

    move v3, v2

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    iget-object v1, v1, Lcom/jme3/scene/Node;->frustrumIntersects:Lcom/jme3/renderer/Camera$FrustumIntersect;

    :goto_1
    iput-object v1, p0, Lcom/jme3/scene/Spatial;->frustrumIntersects:Lcom/jme3/renderer/Camera$FrustumIntersect;

    iget-object v1, p0, Lcom/jme3/scene/Spatial;->frustrumIntersects:Lcom/jme3/renderer/Camera$FrustumIntersect;

    sget-object v4, Lcom/jme3/renderer/Camera$FrustumIntersect;->Intersects:Lcom/jme3/renderer/Camera$FrustumIntersect;

    if-ne v1, v4, :cond_6

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->getQueueBucket()Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    move-result-object v1

    sget-object v4, Lcom/jme3/renderer/queue/RenderQueue$Bucket;->Gui:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    if-ne v1, v4, :cond_5

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->getWorldBound()Lcom/jme3/bounding/BoundingVolume;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/jme3/renderer/Camera;->containsGui(Lcom/jme3/bounding/BoundingVolume;)Z

    move-result v3

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/jme3/renderer/Camera$FrustumIntersect;->Intersects:Lcom/jme3/renderer/Camera$FrustumIntersect;

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->getWorldBound()Lcom/jme3/bounding/BoundingVolume;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/jme3/renderer/Camera;->contains(Lcom/jme3/bounding/BoundingVolume;)Lcom/jme3/renderer/Camera$FrustumIntersect;

    move-result-object v1

    iput-object v1, p0, Lcom/jme3/scene/Spatial;->frustrumIntersects:Lcom/jme3/renderer/Camera$FrustumIntersect;

    :cond_6
    iget-object v1, p0, Lcom/jme3/scene/Spatial;->frustrumIntersects:Lcom/jme3/renderer/Camera$FrustumIntersect;

    sget-object v4, Lcom/jme3/renderer/Camera$FrustumIntersect;->Outside:Lcom/jme3/renderer/Camera$FrustumIntersect;

    if-eq v1, v4, :cond_7

    move v1, v2

    :goto_2
    move v3, v1

    goto :goto_0

    :cond_7
    move v1, v3

    goto :goto_2
.end method

.method checkDoBoundUpdate()V
    .locals 5

    iget v4, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    and-int/lit8 v4, v4, 0x2

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->checkDoTransformUpdate()V

    instance-of v4, p0, Lcom/jme3/scene/Node;

    if-eqz v4, :cond_1

    move-object v3, p0

    check-cast v3, Lcom/jme3/scene/Node;

    invoke-virtual {v3}, Lcom/jme3/scene/Node;->getQuantity()I

    move-result v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    invoke-virtual {v3, v1}, Lcom/jme3/scene/Node;->getChild(I)Lcom/jme3/scene/Spatial;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jme3/scene/Spatial;->checkDoBoundUpdate()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->updateWorldBound()V

    goto :goto_0
.end method

.method checkDoTransformUpdate()V
    .locals 8

    iget v6, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    and-int/lit8 v6, v6, 0x1

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/jme3/scene/Spatial;->worldTransform:Lcom/jme3/math/Transform;

    iget-object v7, p0, Lcom/jme3/scene/Spatial;->localTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v6, v7}, Lcom/jme3/math/Transform;->set(Lcom/jme3/math/Transform;)Lcom/jme3/math/Transform;

    iget v6, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    and-int/lit8 v6, v6, -0x2

    iput v6, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v5

    iget-object v4, v5, Lcom/jme3/util/TempVars;->spatialStack:[Lcom/jme3/scene/Spatial;

    move-object v3, p0

    const/4 v1, 0x0

    :goto_1
    iget-object v0, v3, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    if-nez v0, :cond_4

    iget-object v6, v3, Lcom/jme3/scene/Spatial;->worldTransform:Lcom/jme3/math/Transform;

    iget-object v7, v3, Lcom/jme3/scene/Spatial;->localTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v6, v7}, Lcom/jme3/math/Transform;->set(Lcom/jme3/math/Transform;)Lcom/jme3/math/Transform;

    iget v6, v3, Lcom/jme3/scene/Spatial;->refreshFlags:I

    and-int/lit8 v6, v6, -0x2

    iput v6, v3, Lcom/jme3/scene/Spatial;->refreshFlags:I

    add-int/lit8 v1, v1, -0x1

    :cond_3
    invoke-virtual {v5}, Lcom/jme3/util/TempVars;->release()V

    move v2, v1

    :goto_2
    if-ltz v2, :cond_0

    aget-object v3, v4, v2

    invoke-virtual {v3}, Lcom/jme3/scene/Spatial;->updateWorldTransforms()V

    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    :cond_4
    aput-object v3, v4, v1

    iget v6, v0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    and-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_3

    move-object v3, v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public clone()Lcom/jme3/scene/Spatial;
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/jme3/scene/Spatial;->clone(Z)Lcom/jme3/scene/Spatial;

    move-result-object v0

    return-object v0
.end method

.method public clone(Z)Lcom/jme3/scene/Spatial;
    .locals 6

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/scene/Spatial;

    iget-object v2, p0, Lcom/jme3/scene/Spatial;->worldBound:Lcom/jme3/bounding/BoundingVolume;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jme3/scene/Spatial;->worldBound:Lcom/jme3/bounding/BoundingVolume;

    invoke-virtual {v2}, Lcom/jme3/bounding/BoundingVolume;->clone()Lcom/jme3/bounding/BoundingVolume;

    move-result-object v2

    iput-object v2, v1, Lcom/jme3/scene/Spatial;->worldBound:Lcom/jme3/bounding/BoundingVolume;

    :cond_0
    iget-object v2, p0, Lcom/jme3/scene/Spatial;->worldLights:Lcom/jme3/light/LightList;

    invoke-virtual {v2}, Lcom/jme3/light/LightList;->clone()Lcom/jme3/light/LightList;

    move-result-object v2

    iput-object v2, v1, Lcom/jme3/scene/Spatial;->worldLights:Lcom/jme3/light/LightList;

    iget-object v2, p0, Lcom/jme3/scene/Spatial;->localLights:Lcom/jme3/light/LightList;

    invoke-virtual {v2}, Lcom/jme3/light/LightList;->clone()Lcom/jme3/light/LightList;

    move-result-object v2

    iput-object v2, v1, Lcom/jme3/scene/Spatial;->localLights:Lcom/jme3/light/LightList;

    iget-object v2, v1, Lcom/jme3/scene/Spatial;->localLights:Lcom/jme3/light/LightList;

    invoke-virtual {v2, v1}, Lcom/jme3/light/LightList;->setOwner(Lcom/jme3/scene/Spatial;)V

    iget-object v2, v1, Lcom/jme3/scene/Spatial;->worldLights:Lcom/jme3/light/LightList;

    invoke-virtual {v2, v1}, Lcom/jme3/light/LightList;->setOwner(Lcom/jme3/scene/Spatial;)V

    iget-object v2, p0, Lcom/jme3/scene/Spatial;->worldTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v2}, Lcom/jme3/math/Transform;->clone()Lcom/jme3/math/Transform;

    move-result-object v2

    iput-object v2, v1, Lcom/jme3/scene/Spatial;->worldTransform:Lcom/jme3/math/Transform;

    iget-object v2, p0, Lcom/jme3/scene/Spatial;->localTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v2}, Lcom/jme3/math/Transform;->clone()Lcom/jme3/math/Transform;

    move-result-object v2

    iput-object v2, v1, Lcom/jme3/scene/Spatial;->localTransform:Lcom/jme3/math/Transform;

    instance-of v2, v1, Lcom/jme3/scene/Node;

    if-eqz v2, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/jme3/scene/Node;

    move-object v2, v0

    move-object v0, v1

    check-cast v0, Lcom/jme3/scene/Node;

    move-object v3, v0

    new-instance v4, Lcom/jme3/util/SafeArrayList;

    const-class v5, Lcom/jme3/scene/Spatial;

    invoke-direct {v4, v5}, Lcom/jme3/util/SafeArrayList;-><init>(Ljava/lang/Class;)V

    iput-object v4, v3, Lcom/jme3/scene/Node;->children:Lcom/jme3/util/SafeArrayList;

    iget-object v2, v2, Lcom/jme3/scene/Node;->children:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v2}, Lcom/jme3/util/SafeArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/scene/Spatial;

    invoke-virtual {v2, p1}, Lcom/jme3/scene/Spatial;->clone(Z)Lcom/jme3/scene/Spatial;

    move-result-object v2

    iput-object v3, v2, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    iget-object v5, v3, Lcom/jme3/scene/Node;->children:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v5, v2}, Lcom/jme3/util/SafeArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_1
    const/4 v2, 0x0

    :try_start_1
    iput-object v2, v1, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    invoke-virtual {v1}, Lcom/jme3/scene/Spatial;->setBoundRefresh()V

    invoke-virtual {v1}, Lcom/jme3/scene/Spatial;->setTransformRefresh()V

    invoke-virtual {v1}, Lcom/jme3/scene/Spatial;->setLightListRefresh()V

    new-instance v2, Lcom/jme3/util/SafeArrayList;

    const-class v3, Lcom/jme3/scene/control/Control;

    invoke-direct {v2, v3}, Lcom/jme3/util/SafeArrayList;-><init>(Ljava/lang/Class;)V

    iput-object v2, v1, Lcom/jme3/scene/Spatial;->controls:Lcom/jme3/util/SafeArrayList;

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    iget-object v2, p0, Lcom/jme3/scene/Spatial;->controls:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v2}, Lcom/jme3/util/SafeArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    iget-object v4, v1, Lcom/jme3/scene/Spatial;->controls:Lcom/jme3/util/SafeArrayList;

    iget-object v2, p0, Lcom/jme3/scene/Spatial;->controls:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v2, v3}, Lcom/jme3/util/SafeArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/scene/control/Control;

    invoke-interface {v2, v1}, Lcom/jme3/scene/control/Control;->cloneForSpatial(Lcom/jme3/scene/Spatial;)Lcom/jme3/scene/control/Control;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/jme3/util/SafeArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/jme3/scene/Spatial;->userData:Ljava/util/HashMap;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/jme3/scene/Spatial;->userData:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    iput-object v2, v1, Lcom/jme3/scene/Spatial;->userData:Ljava/util/HashMap;
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_3
    return-object v1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->clone()Lcom/jme3/scene/Spatial;

    move-result-object v0

    return-object v0
.end method

.method public getControl(Ljava/lang/Class;)Lcom/jme3/scene/control/Control;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/jme3/scene/control/Control;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    iget-object v4, p0, Lcom/jme3/scene/Spatial;->controls:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v4}, Lcom/jme3/util/SafeArrayList;->getArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jme3/scene/control/Control;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    return-object v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getCullHint()Lcom/jme3/scene/Spatial$CullHint;
    .locals 2

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->cullHint:Lcom/jme3/scene/Spatial$CullHint;

    sget-object v1, Lcom/jme3/scene/Spatial$CullHint;->Inherit:Lcom/jme3/scene/Spatial$CullHint;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->cullHint:Lcom/jme3/scene/Spatial$CullHint;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    invoke-virtual {v0}, Lcom/jme3/scene/Node;->getCullHint()Lcom/jme3/scene/Spatial$CullHint;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/jme3/scene/Spatial$CullHint;->Dynamic:Lcom/jme3/scene/Spatial$CullHint;

    goto :goto_0
.end method

.method public getLocalTranslation()Lcom/jme3/math/Vector3f;
    .locals 1

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->localTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v0}, Lcom/jme3/math/Transform;->getTranslation()Lcom/jme3/math/Vector3f;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getParent()Lcom/jme3/scene/Node;
    .locals 1

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    return-object v0
.end method

.method public getQueueBucket()Lcom/jme3/renderer/queue/RenderQueue$Bucket;
    .locals 2

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->queueBucket:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    sget-object v1, Lcom/jme3/renderer/queue/RenderQueue$Bucket;->Inherit:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->queueBucket:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    invoke-virtual {v0}, Lcom/jme3/scene/Node;->getQueueBucket()Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/jme3/renderer/queue/RenderQueue$Bucket;->Opaque:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    goto :goto_0
.end method

.method public getShadowMode()Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;
    .locals 2

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->shadowMode:Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;

    sget-object v1, Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;->Inherit:Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->shadowMode:Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    invoke-virtual {v0}, Lcom/jme3/scene/Node;->getShadowMode()Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;->Off:Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;

    goto :goto_0
.end method

.method public getUserData(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/scene/Spatial;->userData:Ljava/util/HashMap;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/jme3/scene/Spatial;->userData:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/Savable;

    instance-of v1, v0, Lcom/jme3/scene/UserData;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/jme3/scene/UserData;

    invoke-virtual {v0}, Lcom/jme3/scene/UserData;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getWorldBound()Lcom/jme3/bounding/BoundingVolume;
    .locals 1

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->checkDoBoundUpdate()V

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->worldBound:Lcom/jme3/bounding/BoundingVolume;

    return-object v0
.end method

.method public getWorldLightList()Lcom/jme3/light/LightList;
    .locals 1

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->worldLights:Lcom/jme3/light/LightList;

    return-object v0
.end method

.method public getWorldRotation()Lcom/jme3/math/Quaternion;
    .locals 1

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->checkDoTransformUpdate()V

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->worldTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v0}, Lcom/jme3/math/Transform;->getRotation()Lcom/jme3/math/Quaternion;

    move-result-object v0

    return-object v0
.end method

.method public getWorldTransform()Lcom/jme3/math/Transform;
    .locals 1

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->checkDoTransformUpdate()V

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->worldTransform:Lcom/jme3/math/Transform;

    return-object v0
.end method

.method public getWorldTranslation()Lcom/jme3/math/Vector3f;
    .locals 1

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->checkDoTransformUpdate()V

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->worldTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v0}, Lcom/jme3/math/Transform;->getTranslation()Lcom/jme3/math/Vector3f;

    move-result-object v0

    return-object v0
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v1

    const-string v0, "name"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->name:Ljava/lang/String;

    const-string v0, "world_bound"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v0

    check-cast v0, Lcom/jme3/bounding/BoundingVolume;

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->worldBound:Lcom/jme3/bounding/BoundingVolume;

    const-string v0, "cull_mode"

    const-class v2, Lcom/jme3/scene/Spatial$CullHint;

    sget-object v3, Lcom/jme3/scene/Spatial$CullHint;->Inherit:Lcom/jme3/scene/Spatial$CullHint;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/scene/Spatial$CullHint;

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->cullHint:Lcom/jme3/scene/Spatial$CullHint;

    const-string v0, "batch_hint"

    const-class v2, Lcom/jme3/scene/Spatial$BatchHint;

    sget-object v3, Lcom/jme3/scene/Spatial$BatchHint;->Inherit:Lcom/jme3/scene/Spatial$BatchHint;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/scene/Spatial$BatchHint;

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->batchHint:Lcom/jme3/scene/Spatial$BatchHint;

    const-string v0, "queue"

    const-class v2, Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    sget-object v3, Lcom/jme3/renderer/queue/RenderQueue$Bucket;->Inherit:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->queueBucket:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    const-string v0, "shadow_mode"

    const-class v2, Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;

    sget-object v3, Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;->Inherit:Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->shadowMode:Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;

    const-string v0, "transform"

    sget-object v2, Lcom/jme3/math/Transform;->IDENTITY:Lcom/jme3/math/Transform;

    invoke-interface {v1, v0, v2}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v0

    check-cast v0, Lcom/jme3/math/Transform;

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->localTransform:Lcom/jme3/math/Transform;

    const-string v0, "lights"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v0

    check-cast v0, Lcom/jme3/light/LightList;

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->localLights:Lcom/jme3/light/LightList;

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->localLights:Lcom/jme3/light/LightList;

    invoke-virtual {v0, p0}, Lcom/jme3/light/LightList;->setOwner(Lcom/jme3/scene/Spatial;)V

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->controls:Lcom/jme3/util/SafeArrayList;

    const/4 v2, 0x0

    const-string v3, "controlsList"

    invoke-interface {v1, v3, v4}, Lcom/jme3/export/InputCapsule;->readSavableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/jme3/util/SafeArrayList;->addAll(ILjava/util/Collection;)Z

    const-string v0, "user_data"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readStringSavableMap(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->userData:Ljava/util/HashMap;

    return-void
.end method

.method public removeControl(Lcom/jme3/scene/control/Control;)Z
    .locals 2
    .param p1    # Lcom/jme3/scene/control/Control;

    iget-object v1, p0, Lcom/jme3/scene/Spatial;->controls:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v1, p1}, Lcom/jme3/util/SafeArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Lcom/jme3/scene/control/Control;->setSpatial(Lcom/jme3/scene/Spatial;)V

    :cond_0
    return v0
.end method

.method public removeFromParent()Z
    .locals 1

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    invoke-virtual {v0, p0}, Lcom/jme3/scene/Node;->detachChild(Lcom/jme3/scene/Spatial;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public runControlRender(Lcom/jme3/renderer/RenderManager;Lcom/jme3/renderer/ViewPort;)V
    .locals 5
    .param p1    # Lcom/jme3/renderer/RenderManager;
    .param p2    # Lcom/jme3/renderer/ViewPort;

    iget-object v4, p0, Lcom/jme3/scene/Spatial;->controls:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v4}, Lcom/jme3/util/SafeArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/jme3/scene/Spatial;->controls:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v4}, Lcom/jme3/util/SafeArrayList;->getArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jme3/scene/control/Control;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    invoke-interface {v1, p1, p2}, Lcom/jme3/scene/control/Control;->render(Lcom/jme3/renderer/RenderManager;Lcom/jme3/renderer/ViewPort;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected setBoundRefresh()V
    .locals 2

    iget v1, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    :goto_0
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget v1, v0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    iget-object v0, v0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    goto :goto_0
.end method

.method public setCullHint(Lcom/jme3/scene/Spatial$CullHint;)V
    .locals 0
    .param p1    # Lcom/jme3/scene/Spatial$CullHint;

    iput-object p1, p0, Lcom/jme3/scene/Spatial;->cullHint:Lcom/jme3/scene/Spatial$CullHint;

    return-void
.end method

.method public setKey(Lcom/jme3/asset/AssetKey;)V
    .locals 0
    .param p1    # Lcom/jme3/asset/AssetKey;

    iput-object p1, p0, Lcom/jme3/scene/Spatial;->key:Lcom/jme3/asset/AssetKey;

    return-void
.end method

.method public setLastFrustumIntersection(Lcom/jme3/renderer/Camera$FrustumIntersect;)V
    .locals 0
    .param p1    # Lcom/jme3/renderer/Camera$FrustumIntersect;

    iput-object p1, p0, Lcom/jme3/scene/Spatial;->frustrumIntersects:Lcom/jme3/renderer/Camera$FrustumIntersect;

    return-void
.end method

.method protected setLightListRefresh()V
    .locals 1

    iget v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    return-void
.end method

.method public setLocalRotation(Lcom/jme3/math/Quaternion;)V
    .locals 1
    .param p1    # Lcom/jme3/math/Quaternion;

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->localTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v0, p1}, Lcom/jme3/math/Transform;->setRotation(Lcom/jme3/math/Quaternion;)Lcom/jme3/math/Transform;

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->setTransformRefresh()V

    return-void
.end method

.method public setLocalScale(F)V
    .locals 1
    .param p1    # F

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->localTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v0, p1}, Lcom/jme3/math/Transform;->setScale(F)Lcom/jme3/math/Transform;

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->setTransformRefresh()V

    return-void
.end method

.method public setLocalScale(FFF)V
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->localTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jme3/math/Transform;->setScale(FFF)Lcom/jme3/math/Transform;

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->setTransformRefresh()V

    return-void
.end method

.method public setLocalScale(Lcom/jme3/math/Vector3f;)V
    .locals 1
    .param p1    # Lcom/jme3/math/Vector3f;

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->localTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v0, p1}, Lcom/jme3/math/Transform;->setScale(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Transform;

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->setTransformRefresh()V

    return-void
.end method

.method public setLocalTranslation(FFF)V
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->localTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jme3/math/Transform;->setTranslation(FFF)Lcom/jme3/math/Transform;

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->setTransformRefresh()V

    return-void
.end method

.method public setLocalTranslation(Lcom/jme3/math/Vector3f;)V
    .locals 1
    .param p1    # Lcom/jme3/math/Vector3f;

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->localTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v0, p1}, Lcom/jme3/math/Transform;->setTranslation(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Transform;

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->setTransformRefresh()V

    return-void
.end method

.method public setMaterial(Lcom/jme3/material/Material;)V
    .locals 0
    .param p1    # Lcom/jme3/material/Material;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jme3/scene/Spatial;->name:Ljava/lang/String;

    return-void
.end method

.method protected setParent(Lcom/jme3/scene/Node;)V
    .locals 0
    .param p1    # Lcom/jme3/scene/Node;

    iput-object p1, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    return-void
.end method

.method public setQueueBucket(Lcom/jme3/renderer/queue/RenderQueue$Bucket;)V
    .locals 0
    .param p1    # Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    iput-object p1, p0, Lcom/jme3/scene/Spatial;->queueBucket:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    return-void
.end method

.method protected setTransformRefresh()V
    .locals 1

    iget v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->setBoundRefresh()V

    return-void
.end method

.method public setUserData(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->userData:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/Spatial;->userData:Ljava/util/HashMap;

    :cond_0
    instance-of v0, p2, Lcom/jme3/export/Savable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->userData:Ljava/util/HashMap;

    check-cast p2, Lcom/jme3/export/Savable;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/jme3/scene/Spatial;->userData:Ljava/util/HashMap;

    new-instance v1, Lcom/jme3/scene/UserData;

    invoke-static {p2}, Lcom/jme3/scene/UserData;->getObjectType(Ljava/lang/Object;)B

    move-result v2

    invoke-direct {v1, v2, p2}, Lcom/jme3/scene/UserData;-><init>(BLjava/lang/Object;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/jme3/scene/Spatial;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateGeometricState()V
    .locals 1

    iget v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->updateWorldLightList()V

    :cond_0
    iget v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->updateWorldTransforms()V

    :cond_1
    iget v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/jme3/scene/Spatial;->updateWorldBound()V

    :cond_2
    sget-boolean v0, Lcom/jme3/scene/Spatial;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    return-void
.end method

.method public updateLogicalState(F)V
    .locals 0
    .param p1    # F

    invoke-direct {p0, p1}, Lcom/jme3/scene/Spatial;->runControlUpdate(F)V

    return-void
.end method

.method protected updateWorldBound()V
    .locals 1

    iget v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    return-void
.end method

.method protected updateWorldLightList()V
    .locals 3

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->worldLights:Lcom/jme3/light/LightList;

    iget-object v1, p0, Lcom/jme3/scene/Spatial;->localLights:Lcom/jme3/light/LightList;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/jme3/light/LightList;->update(Lcom/jme3/light/LightList;Lcom/jme3/light/LightList;)V

    iget v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    iget v0, v0, Lcom/jme3/scene/Node;->refreshFlags:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->worldLights:Lcom/jme3/light/LightList;

    iget-object v1, p0, Lcom/jme3/scene/Spatial;->localLights:Lcom/jme3/light/LightList;

    iget-object v2, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    iget-object v2, v2, Lcom/jme3/scene/Node;->worldLights:Lcom/jme3/light/LightList;

    invoke-virtual {v0, v1, v2}, Lcom/jme3/light/LightList;->update(Lcom/jme3/light/LightList;Lcom/jme3/light/LightList;)V

    iget v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    goto :goto_0

    :cond_2
    sget-boolean v0, Lcom/jme3/scene/Spatial;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method protected updateWorldTransforms()V
    .locals 2

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->worldTransform:Lcom/jme3/math/Transform;

    iget-object v1, p0, Lcom/jme3/scene/Spatial;->localTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v0, v1}, Lcom/jme3/math/Transform;->set(Lcom/jme3/math/Transform;)Lcom/jme3/math/Transform;

    iget v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lcom/jme3/scene/Spatial;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    iget v0, v0, Lcom/jme3/scene/Node;->refreshFlags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/jme3/scene/Spatial;->worldTransform:Lcom/jme3/math/Transform;

    iget-object v1, p0, Lcom/jme3/scene/Spatial;->localTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v0, v1}, Lcom/jme3/math/Transform;->set(Lcom/jme3/math/Transform;)Lcom/jme3/math/Transform;

    iget-object v0, p0, Lcom/jme3/scene/Spatial;->worldTransform:Lcom/jme3/math/Transform;

    iget-object v1, p0, Lcom/jme3/scene/Spatial;->parent:Lcom/jme3/scene/Node;

    iget-object v1, v1, Lcom/jme3/scene/Node;->worldTransform:Lcom/jme3/math/Transform;

    invoke-virtual {v0, v1}, Lcom/jme3/math/Transform;->combineWithParent(Lcom/jme3/math/Transform;)Lcom/jme3/math/Transform;

    iget v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/jme3/scene/Spatial;->refreshFlags:I

    goto :goto_0
.end method
