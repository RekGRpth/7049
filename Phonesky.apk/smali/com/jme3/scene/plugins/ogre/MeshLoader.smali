.class public Lcom/jme3/scene/plugins/ogre/MeshLoader;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "MeshLoader.java"

# interfaces
.implements Lcom/jme3/asset/AssetLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/scene/plugins/ogre/MeshLoader$1;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static AUTO_INTERLEAVE:Z

.field public static HARDWARE_SKINNING:Z

.field private static final TEXCOORD_TYPES:[Lcom/jme3/scene/VertexBuffer$Type;

.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private actuallyHasWeights:Z

.field private animData:Lcom/jme3/scene/plugins/ogre/AnimData;

.field private assetManager:Lcom/jme3/asset/AssetManager;

.field private fb:Ljava/nio/FloatBuffer;

.field private folderName:Ljava/lang/String;

.field private geom:Lcom/jme3/scene/Geometry;

.field private geoms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jme3/scene/Geometry;",
            ">;"
        }
    .end annotation
.end field

.field private ib:Ljava/nio/IntBuffer;

.field private ignoreUntilEnd:Ljava/lang/String;

.field private indicesData:Ljava/nio/ByteBuffer;

.field private key:Lcom/jme3/asset/AssetKey;

.field private lodLevels:Lcom/jme3/util/IntMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/IntMap",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/jme3/scene/VertexBuffer;",
            ">;>;"
        }
    .end annotation
.end field

.field private materialList:Lcom/jme3/material/MaterialList;

.field private mesh:Lcom/jme3/scene/Mesh;

.field private meshIndex:I

.field private meshName:Ljava/lang/String;

.field private sb:Ljava/nio/ShortBuffer;

.field private sharedMesh:Lcom/jme3/scene/Mesh;

.field private texCoordIndex:I

.field private usesBigIndices:Z

.field private usesSharedMesh:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private usesSharedVerts:Z

.field private vb:Lcom/jme3/scene/VertexBuffer;

.field private vertCount:I

.field private weightsFloatData:Ljava/nio/FloatBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-class v0, Lcom/jme3/scene/plugins/ogre/MeshLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->$assertionsDisabled:Z

    const-class v0, Lcom/jme3/scene/plugins/ogre/MeshLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->logger:Ljava/util/logging/Logger;

    sput-boolean v1, Lcom/jme3/scene/plugins/ogre/MeshLoader;->AUTO_INTERLEAVE:Z

    sput-boolean v2, Lcom/jme3/scene/plugins/ogre/MeshLoader;->HARDWARE_SKINNING:Z

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/jme3/scene/VertexBuffer$Type;

    sget-object v3, Lcom/jme3/scene/VertexBuffer$Type;->TexCoord:Lcom/jme3/scene/VertexBuffer$Type;

    aput-object v3, v0, v2

    sget-object v2, Lcom/jme3/scene/VertexBuffer$Type;->TexCoord2:Lcom/jme3/scene/VertexBuffer$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/jme3/scene/VertexBuffer$Type;->TexCoord3:Lcom/jme3/scene/VertexBuffer$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/jme3/scene/VertexBuffer$Type;->TexCoord4:Lcom/jme3/scene/VertexBuffer$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/jme3/scene/VertexBuffer$Type;->TexCoord5:Lcom/jme3/scene/VertexBuffer$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/jme3/scene/VertexBuffer$Type;->TexCoord6:Lcom/jme3/scene/VertexBuffer$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/jme3/scene/VertexBuffer$Type;->TexCoord7:Lcom/jme3/scene/VertexBuffer$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/jme3/scene/VertexBuffer$Type;->TexCoord8:Lcom/jme3/scene/VertexBuffer$Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->TEXCOORD_TYPES:[Lcom/jme3/scene/VertexBuffer$Type;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    iput-boolean v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->actuallyHasWeights:Z

    iput v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshIndex:I

    iput v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->texCoordIndex:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ignoreUntilEnd:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geoms:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->usesSharedMesh:Ljava/util/ArrayList;

    new-instance v0, Lcom/jme3/util/IntMap;

    invoke-direct {v0}, Lcom/jme3/util/IntMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->lodLevels:Lcom/jme3/util/IntMap;

    return-void
.end method

.method private applyMaterial(Lcom/jme3/scene/Geometry;Ljava/lang/String;)V
    .locals 8
    .param p1    # Lcom/jme3/scene/Geometry;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ".j3m"

    invoke-virtual {p2, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :try_start_0
    iget-object v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    invoke-interface {v2, p2}, Lcom/jme3/asset/AssetManager;->loadMaterial(Ljava/lang/String;)Lcom/jme3/material/Material;
    :try_end_0
    .catch Lcom/jme3/asset/AssetNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    if-nez v1, :cond_1

    sget-object v2, Lcom/jme3/scene/plugins/ogre/MeshLoader;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v4, "Cannot locate {0} for model {1}"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->key:Lcom/jme3/asset/AssetKey;

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    invoke-static {v2}, Lcom/jme3/util/PlaceholderAssets;->getPlaceholderMaterial(Lcom/jme3/asset/AssetManager;)Lcom/jme3/material/Material;

    move-result-object v1

    :cond_1
    invoke-virtual {v1}, Lcom/jme3/material/Material;->isTransparent()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/jme3/renderer/queue/RenderQueue$Bucket;->Transparent:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    invoke-virtual {p1, v2}, Lcom/jme3/scene/Geometry;->setQueueBucket(Lcom/jme3/renderer/queue/RenderQueue$Bucket;)V

    :cond_2
    invoke-virtual {p1, v1}, Lcom/jme3/scene/Geometry;->setMaterial(Lcom/jme3/material/Material;)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/jme3/asset/AssetNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    throw v0

    :cond_3
    iget-object v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->materialList:Lcom/jme3/material/MaterialList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->materialList:Lcom/jme3/material/MaterialList;

    invoke-virtual {v2, p2}, Lcom/jme3/material/MaterialList;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/material/Material;

    goto :goto_0
.end method

.method private compileModel()Lcom/jme3/scene/Node;
    .locals 6

    const/4 v2, 0x0

    new-instance v3, Lcom/jme3/scene/Node;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-ogremesh"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/jme3/scene/Node;-><init>(Ljava/lang/String;)V

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geoms:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geoms:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/scene/Geometry;

    invoke-virtual {v0}, Lcom/jme3/scene/Geometry;->getMesh()Lcom/jme3/scene/Mesh;

    move-result-object v4

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sharedMesh:Lcom/jme3/scene/Mesh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->usesSharedMesh:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sharedMesh:Lcom/jme3/scene/Mesh;

    invoke-virtual {v4, v0}, Lcom/jme3/scene/Mesh;->extractVertexData(Lcom/jme3/scene/Mesh;)V

    :cond_0
    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geoms:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/scene/Spatial;

    invoke-virtual {v3, v0}, Lcom/jme3/scene/Node;->attachChild(Lcom/jme3/scene/Spatial;)I

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->animData:Lcom/jme3/scene/plugins/ogre/AnimData;

    if-eqz v0, :cond_5

    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geoms:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geoms:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/scene/Geometry;

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geoms:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/scene/Geometry;

    invoke-virtual {v0}, Lcom/jme3/scene/Geometry;->getMesh()Lcom/jme3/scene/Mesh;

    move-result-object v4

    sget-boolean v0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->HARDWARE_SKINNING:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v4, v0}, Lcom/jme3/scene/Mesh;->generateBindPose(Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->animData:Lcom/jme3/scene/plugins/ogre/AnimData;

    iget-object v4, v0, Lcom/jme3/scene/plugins/ogre/AnimData;->anims:Ljava/util/ArrayList;

    :goto_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/animation/Animation;

    invoke-virtual {v0}, Lcom/jme3/animation/Animation;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    new-instance v0, Lcom/jme3/animation/AnimControl;

    iget-object v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->animData:Lcom/jme3/scene/plugins/ogre/AnimData;

    iget-object v2, v2, Lcom/jme3/scene/plugins/ogre/AnimData;->skeleton:Lcom/jme3/animation/Skeleton;

    invoke-direct {v0, v2}, Lcom/jme3/animation/AnimControl;-><init>(Lcom/jme3/animation/Skeleton;)V

    invoke-virtual {v0, v1}, Lcom/jme3/animation/AnimControl;->setAnimations(Ljava/util/HashMap;)V

    invoke-virtual {v3, v0}, Lcom/jme3/scene/Node;->addControl(Lcom/jme3/scene/control/Control;)V

    new-instance v0, Lcom/jme3/animation/SkeletonControl;

    iget-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->animData:Lcom/jme3/scene/plugins/ogre/AnimData;

    iget-object v1, v1, Lcom/jme3/scene/plugins/ogre/AnimData;->skeleton:Lcom/jme3/animation/Skeleton;

    invoke-direct {v0, v1}, Lcom/jme3/animation/SkeletonControl;-><init>(Lcom/jme3/animation/Skeleton;)V

    invoke-virtual {v3, v0}, Lcom/jme3/scene/Node;->addControl(Lcom/jme3/scene/control/Control;)V

    :cond_5
    return-object v3
.end method

.method private endBoneAssigns()V
    .locals 13

    const/high16 v12, 0x3f800000

    const/4 v11, 0x0

    const/4 v10, 0x0

    iget-boolean v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->actuallyHasWeights:Z

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    sget-object v9, Lcom/jme3/scene/VertexBuffer$Type;->BoneIndex:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {v8, v9}, Lcom/jme3/scene/Mesh;->clearBuffer(Lcom/jme3/scene/VertexBuffer$Type;)V

    iget-object v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    sget-object v9, Lcom/jme3/scene/VertexBuffer$Type;->BoneWeight:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {v8, v9}, Lcom/jme3/scene/Mesh;->clearBuffer(Lcom/jme3/scene/VertexBuffer$Type;)V

    iput-object v11, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    iput-object v11, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->indicesData:Ljava/nio/ByteBuffer;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iget-object v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    invoke-virtual {v8}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    const/4 v3, 0x0

    :goto_1
    iget v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vertCount:I

    if-ge v3, v8, :cond_6

    iget-object v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    invoke-virtual {v8}, Ljava/nio/FloatBuffer;->get()F

    move-result v4

    iget-object v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    invoke-virtual {v8}, Ljava/nio/FloatBuffer;->get()F

    move-result v5

    iget-object v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    invoke-virtual {v8}, Ljava/nio/FloatBuffer;->get()F

    move-result v6

    iget-object v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    invoke-virtual {v8}, Ljava/nio/FloatBuffer;->get()F

    move-result v7

    cmpl-float v8, v7, v10

    if-eqz v8, :cond_3

    const/4 v8, 0x4

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_1
    :goto_2
    add-float v8, v4, v5

    add-float/2addr v8, v6

    add-float v1, v8, v7

    cmpl-float v8, v1, v12

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    iget-object v9, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    invoke-virtual {v9}, Ljava/nio/FloatBuffer;->position()I

    move-result v9

    add-int/lit8 v9, v9, -0x4

    invoke-virtual {v8, v9}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    div-float v2, v12, v1

    iget-object v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    mul-float v9, v4, v2

    invoke-virtual {v8, v9}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    iget-object v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    mul-float v9, v5, v2

    invoke-virtual {v8, v9}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    iget-object v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    mul-float v9, v6, v2

    invoke-virtual {v8, v9}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    iget-object v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    mul-float v9, v7, v2

    invoke-virtual {v8, v9}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    cmpl-float v8, v6, v10

    if-eqz v8, :cond_4

    const/4 v8, 0x3

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2

    :cond_4
    cmpl-float v8, v5, v10

    if-eqz v8, :cond_5

    const/4 v8, 0x2

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2

    :cond_5
    cmpl-float v8, v4, v10

    if-eqz v8, :cond_1

    const/4 v8, 0x1

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2

    :cond_6
    iget-object v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    invoke-virtual {v8}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->actuallyHasWeights:Z

    iput-object v11, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    iput-object v11, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->indicesData:Ljava/nio/ByteBuffer;

    iget-object v8, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    invoke-virtual {v8, v0}, Lcom/jme3/scene/Mesh;->setMaxNumWeights(I)V

    goto/16 :goto_0
.end method

.method private endLevelOfDetail()V
    .locals 7

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->lodLevels:Lcom/jme3/util/IntMap;

    invoke-virtual {v5}, Lcom/jme3/util/IntMap;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/util/IntMap$Entry;

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geoms:Ljava/util/List;

    invoke-virtual {v0}, Lcom/jme3/util/IntMap$Entry;->getKey()I

    move-result v6

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jme3/scene/Geometry;

    invoke-virtual {v5}, Lcom/jme3/scene/Geometry;->getMesh()Lcom/jme3/scene/Mesh;

    move-result-object v4

    invoke-virtual {v0}, Lcom/jme3/util/IntMap$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    new-array v2, v5, [Lcom/jme3/scene/VertexBuffer;

    invoke-interface {v3, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-virtual {v4, v2}, Lcom/jme3/scene/Mesh;->setLodLevels([Lcom/jme3/scene/VertexBuffer;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private pushAttrib(Lcom/jme3/scene/VertexBuffer$Type;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1    # Lcom/jme3/scene/VertexBuffer$Type;
    .param p2    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    :try_start_0
    iget-object v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    invoke-virtual {v2, p1}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/FloatBuffer;

    const-string v2, "x"

    invoke-interface {p2, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v2

    const-string v3, "y"

    invoke-interface {p2, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v2

    const-string v3, "z"

    invoke-interface {p2, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v1

    new-instance v2, Lorg/xml/sax/SAXException;

    const-string v3, "Failed to push attrib"

    invoke-direct {v2, v3, v1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v2
.end method

.method private pushBoneAssign(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    const/4 v6, 0x0

    invoke-static {p1}, Lcom/jme3/util/xml/SAXUtil;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {p3}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v4

    invoke-static {p2}, Lcom/jme3/util/xml/SAXUtil;->parseInt(Ljava/lang/String;)I

    move-result v5

    int-to-byte v0, v5

    sget-boolean v5, Lcom/jme3/scene/plugins/ogre/MeshLoader;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    if-gez v0, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    :cond_0
    sget-boolean v5, Lcom/jme3/scene/plugins/ogre/MeshLoader;->$assertionsDisabled:Z

    if-nez v5, :cond_2

    if-ltz v3, :cond_1

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    invoke-virtual {v5}, Lcom/jme3/scene/Mesh;->getVertexCount()I

    move-result v5

    if-lt v3, v5, :cond_2

    :cond_1
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    :cond_2
    const/4 v2, 0x0

    mul-int/lit8 v1, v3, 0x4

    :goto_0
    mul-int/lit8 v5, v3, 0x4

    add-int/lit8 v5, v5, 0x4

    if-ge v1, v5, :cond_3

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    invoke-virtual {v5, v1}, Ljava/nio/FloatBuffer;->get(I)F

    move-result v2

    cmpl-float v5, v2, v6

    if-nez v5, :cond_4

    :cond_3
    cmpl-float v5, v2, v6

    if-eqz v5, :cond_5

    sget-object v5, Lcom/jme3/scene/plugins/ogre/MeshLoader;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v7, "Vertex {0} has more than 4 weights per vertex! Ignoring.."

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v6, v7, v8}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    invoke-virtual {v5, v1, v4}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->indicesData:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v1, v0}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->actuallyHasWeights:Z

    goto :goto_1
.end method

.method private pushColor(Lorg/xml/sax/Attributes;)V
    .locals 7
    .param p1    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    const/4 v6, 0x3

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Type;->Color:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {v4, v5}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/FloatBuffer;

    const-string v4, "value"

    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/jme3/util/xml/SAXUtil;->parseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\\s"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v4, v2

    if-eq v4, v6, :cond_0

    array-length v4, v2

    const/4 v5, 0x4

    if-eq v4, v5, :cond_0

    new-instance v4, Lorg/xml/sax/SAXException;

    const-string v5, "Color value must contain 3 or 4 components"

    invoke-direct {v4, v5}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    new-instance v1, Lcom/jme3/math/ColorRGBA;

    invoke-direct {v1}, Lcom/jme3/math/ColorRGBA;-><init>()V

    const/4 v4, 0x0

    aget-object v4, v2, v4

    invoke-static {v4}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v4

    iput v4, v1, Lcom/jme3/math/ColorRGBA;->r:F

    const/4 v4, 0x1

    aget-object v4, v2, v4

    invoke-static {v4}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v4

    iput v4, v1, Lcom/jme3/math/ColorRGBA;->g:F

    const/4 v4, 0x2

    aget-object v4, v2, v4

    invoke-static {v4}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v4

    iput v4, v1, Lcom/jme3/math/ColorRGBA;->b:F

    array-length v4, v2

    if-ne v4, v6, :cond_1

    const/high16 v4, 0x3f800000

    iput v4, v1, Lcom/jme3/math/ColorRGBA;->a:F

    :goto_0
    iget v4, v1, Lcom/jme3/math/ColorRGBA;->r:F

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v4

    iget v5, v1, Lcom/jme3/math/ColorRGBA;->g:F

    invoke-virtual {v4, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v4

    iget v5, v1, Lcom/jme3/math/ColorRGBA;->b:F

    invoke-virtual {v4, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v4

    iget v5, v1, Lcom/jme3/math/ColorRGBA;->a:F

    invoke-virtual {v4, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    return-void

    :cond_1
    aget-object v4, v2, v6

    invoke-static {v4}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v4

    iput v4, v1, Lcom/jme3/math/ColorRGBA;->a:F

    goto :goto_0
.end method

.method private pushFace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    sget-object v0, Lcom/jme3/scene/plugins/ogre/MeshLoader$1;->$SwitchMap$com$jme3$scene$Mesh$Mode:[I

    iget-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    invoke-virtual {v1}, Lcom/jme3/scene/Mesh;->getMode()Lcom/jme3/scene/Mesh$Mode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jme3/scene/Mesh$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-static {p1}, Lcom/jme3/util/xml/SAXUtil;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->pushIndex(I)V

    invoke-static {p2}, Lcom/jme3/util/xml/SAXUtil;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->pushIndex(I)V

    invoke-static {p3}, Lcom/jme3/util/xml/SAXUtil;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->pushIndex(I)V

    goto :goto_0

    :pswitch_1
    invoke-static {p1}, Lcom/jme3/util/xml/SAXUtil;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->pushIndex(I)V

    invoke-static {p2}, Lcom/jme3/util/xml/SAXUtil;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->pushIndex(I)V

    goto :goto_0

    :pswitch_2
    invoke-static {p1}, Lcom/jme3/util/xml/SAXUtil;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->pushIndex(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private pushIndex(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ib:Ljava/nio/IntBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ib:Ljava/nio/IntBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/IntBuffer;->put(I)Ljava/nio/IntBuffer;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sb:Ljava/nio/ShortBuffer;

    int-to-short v1, p1

    invoke-virtual {v0, v1}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    goto :goto_0
.end method

.method private pushTangent(Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    :try_start_0
    iget-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    sget-object v4, Lcom/jme3/scene/VertexBuffer$Type;->Tangent:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {v3, v4}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/FloatBuffer;

    const-string v3, "x"

    invoke-interface {p1, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v3

    const-string v4, "y"

    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v3

    const-string v4, "z"

    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    invoke-virtual {v2}, Lcom/jme3/scene/VertexBuffer;->getNumComponents()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    const-string v3, "w"

    invoke-interface {p1, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v1

    new-instance v3, Lorg/xml/sax/SAXException;

    const-string v4, "Failed to push attrib"

    invoke-direct {v3, v4, v1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v3
.end method

.method private pushTexCoord(Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    iget v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->texCoordIndex:I

    const/16 v4, 0x8

    if-lt v3, v4, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v3, Lcom/jme3/scene/plugins/ogre/MeshLoader;->TEXCOORD_TYPES:[Lcom/jme3/scene/VertexBuffer$Type;

    iget v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->texCoordIndex:I

    aget-object v2, v3, v4

    iget-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    invoke-virtual {v3, v2}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/FloatBuffer;

    const-string v3, "u"

    invoke-interface {p1, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    invoke-virtual {v1}, Lcom/jme3/scene/VertexBuffer;->getNumComponents()I

    move-result v3

    const/4 v4, 0x2

    if-lt v3, v4, :cond_1

    const-string v3, "v"

    invoke-interface {p1, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    invoke-virtual {v1}, Lcom/jme3/scene/VertexBuffer;->getNumComponents()I

    move-result v3

    const/4 v4, 0x3

    if-lt v3, v4, :cond_1

    const-string v3, "w"

    invoke-interface {p1, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    invoke-virtual {v1}, Lcom/jme3/scene/VertexBuffer;->getNumComponents()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    const-string v3, "x"

    invoke-interface {p1, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/jme3/util/xml/SAXUtil;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    :cond_1
    iget v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->texCoordIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->texCoordIndex:I

    goto :goto_0
.end method

.method private startBoneAssigns()V
    .locals 6

    const/4 v5, 0x4

    iget-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sharedMesh:Lcom/jme3/scene/Mesh;

    if-eq v3, v4, :cond_0

    iget-boolean v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->usesSharedVerts:Z

    if-eqz v3, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-boolean v3, Lcom/jme3/scene/plugins/ogre/MeshLoader;->HARDWARE_SKINNING:Z

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vertCount:I

    mul-int/lit8 v3, v3, 0x4

    invoke-static {v3}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v3

    iput-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    iget v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vertCount:I

    mul-int/lit8 v3, v3, 0x4

    invoke-static {v3}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    iput-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->indicesData:Ljava/nio/ByteBuffer;

    :goto_1
    new-instance v2, Lcom/jme3/scene/VertexBuffer;

    sget-object v3, Lcom/jme3/scene/VertexBuffer$Type;->BoneWeight:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-direct {v2, v3}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    new-instance v0, Lcom/jme3/scene/VertexBuffer;

    sget-object v3, Lcom/jme3/scene/VertexBuffer$Type;->BoneIndex:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-direct {v0, v3}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    sget-boolean v3, Lcom/jme3/scene/plugins/ogre/MeshLoader;->HARDWARE_SKINNING:Z

    if-eqz v3, :cond_2

    sget-object v1, Lcom/jme3/scene/VertexBuffer$Usage;->Static:Lcom/jme3/scene/VertexBuffer$Usage;

    :goto_2
    sget-object v3, Lcom/jme3/scene/VertexBuffer$Format;->Float:Lcom/jme3/scene/VertexBuffer$Format;

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    invoke-virtual {v2, v1, v5, v3, v4}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    sget-object v3, Lcom/jme3/scene/VertexBuffer$Format;->UnsignedByte:Lcom/jme3/scene/VertexBuffer$Format;

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->indicesData:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v5, v3, v4}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    iget-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    invoke-virtual {v3, v2}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    iget-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    invoke-virtual {v3, v0}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    goto :goto_0

    :cond_1
    iget v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vertCount:I

    mul-int/lit8 v3, v3, 0x4

    invoke-static {v3}, Ljava/nio/FloatBuffer;->allocate(I)Ljava/nio/FloatBuffer;

    move-result-object v3

    iput-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    iget v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vertCount:I

    mul-int/lit8 v3, v3, 0x4

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    iput-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->indicesData:Ljava/nio/ByteBuffer;

    goto :goto_1

    :cond_2
    sget-object v1, Lcom/jme3/scene/VertexBuffer$Usage;->CpuOnly:Lcom/jme3/scene/VertexBuffer$Usage;

    goto :goto_2
.end method

.method private startFaces(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    const/4 v5, 0x0

    invoke-static {p1}, Lcom/jme3/util/xml/SAXUtil;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v0, 0x0

    sget-object v3, Lcom/jme3/scene/plugins/ogre/MeshLoader$1;->$SwitchMap$com$jme3$scene$Mesh$Mode:[I

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    invoke-virtual {v4}, Lcom/jme3/scene/Mesh;->getMode()Lcom/jme3/scene/Mesh$Mode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jme3/scene/Mesh$Mode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    new-instance v3, Lorg/xml/sax/SAXException;

    const-string v4, "Strips or fans not supported!"

    invoke-direct {v3, v4}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_0
    const/4 v0, 0x3

    :goto_0
    mul-int v2, v0, v1

    new-instance v3, Lcom/jme3/scene/VertexBuffer;

    sget-object v4, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-direct {v3, v4}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    iput-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    iget-boolean v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->usesBigIndices:Z

    if-nez v3, :cond_0

    invoke-static {v2}, Lcom/jme3/util/BufferUtils;->createShortBuffer(I)Ljava/nio/ShortBuffer;

    move-result-object v3

    iput-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sb:Ljava/nio/ShortBuffer;

    iput-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ib:Ljava/nio/IntBuffer;

    iget-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    sget-object v4, Lcom/jme3/scene/VertexBuffer$Usage;->Static:Lcom/jme3/scene/VertexBuffer$Usage;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Format;->UnsignedShort:Lcom/jme3/scene/VertexBuffer$Format;

    iget-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sb:Ljava/nio/ShortBuffer;

    invoke-virtual {v3, v4, v0, v5, v6}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    :goto_1
    iget-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    invoke-virtual {v3, v4}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    return-void

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v2}, Lcom/jme3/util/BufferUtils;->createIntBuffer(I)Ljava/nio/IntBuffer;

    move-result-object v3

    iput-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ib:Ljava/nio/IntBuffer;

    iput-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sb:Ljava/nio/ShortBuffer;

    iget-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    sget-object v4, Lcom/jme3/scene/VertexBuffer$Usage;->Static:Lcom/jme3/scene/VertexBuffer$Usage;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Format;->UnsignedInt:Lcom/jme3/scene/VertexBuffer$Format;

    iget-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ib:Ljava/nio/IntBuffer;

    invoke-virtual {v3, v4, v0, v5, v6}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private startGeometry(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jme3/util/xml/SAXUtil;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vertCount:I

    return-void
.end method

.method private startLevelOfDetail(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method private startLodFaceList(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v8, 0x3

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geoms:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jme3/scene/Geometry;

    invoke-virtual {v4}, Lcom/jme3/scene/Geometry;->getMesh()Lcom/jme3/scene/Mesh;

    move-result-object v4

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {v4, v5}, Lcom/jme3/scene/Mesh;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v3

    new-instance v4, Lcom/jme3/scene/VertexBuffer;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-direct {v4, v5}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    invoke-virtual {v3}, Lcom/jme3/scene/VertexBuffer;->getFormat()Lcom/jme3/scene/VertexBuffer$Format;

    move-result-object v4

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Format;->UnsignedInt:Lcom/jme3/scene/VertexBuffer$Format;

    if-ne v4, v5, :cond_1

    mul-int/lit8 v4, v0, 0x3

    invoke-static {v4}, Lcom/jme3/util/BufferUtils;->createIntBuffer(I)Ljava/nio/IntBuffer;

    move-result-object v4

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ib:Ljava/nio/IntBuffer;

    iput-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sb:Ljava/nio/ShortBuffer;

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Usage;->Static:Lcom/jme3/scene/VertexBuffer$Usage;

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Format;->UnsignedInt:Lcom/jme3/scene/VertexBuffer$Format;

    iget-object v7, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ib:Ljava/nio/IntBuffer;

    invoke-virtual {v4, v5, v8, v6, v7}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    :goto_0
    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->lodLevels:Lcom/jme3/util/IntMap;

    invoke-virtual {v4, v1}, Lcom/jme3/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->lodLevels:Lcom/jme3/util/IntMap;

    invoke-virtual {v4, v1, v2}, Lcom/jme3/util/IntMap;->put(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_1
    mul-int/lit8 v4, v0, 0x3

    invoke-static {v4}, Lcom/jme3/util/BufferUtils;->createShortBuffer(I)Ljava/nio/ShortBuffer;

    move-result-object v4

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sb:Ljava/nio/ShortBuffer;

    iput-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ib:Ljava/nio/IntBuffer;

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Usage;->Static:Lcom/jme3/scene/VertexBuffer$Usage;

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Format;->UnsignedShort:Lcom/jme3/scene/VertexBuffer$Format;

    iget-object v7, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sb:Ljava/nio/ShortBuffer;

    invoke-virtual {v4, v5, v8, v6, v7}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    goto :goto_0
.end method

.method private startLodGenerated(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method private startSharedGeom(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    new-instance v0, Lcom/jme3/scene/Mesh;

    invoke-direct {v0}, Lcom/jme3/scene/Mesh;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sharedMesh:Lcom/jme3/scene/Mesh;

    invoke-static {p1}, Lcom/jme3/util/xml/SAXUtil;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vertCount:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->usesSharedVerts:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geom:Lcom/jme3/scene/Geometry;

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sharedMesh:Lcom/jme3/scene/Mesh;

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    return-void
.end method

.method private startSkeleton(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/jme3/asset/AssetKey;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->folderName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".xml"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/jme3/asset/AssetKey;-><init>(Ljava/lang/String;)V

    :try_start_0
    iget-object v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    invoke-interface {v2, v0}, Lcom/jme3/asset/AssetManager;->loadAsset(Lcom/jme3/asset/AssetKey;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/scene/plugins/ogre/AnimData;

    iput-object v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->animData:Lcom/jme3/scene/plugins/ogre/AnimData;
    :try_end_0
    .catch Lcom/jme3/asset/AssetNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    sget-object v2, Lcom/jme3/scene/plugins/ogre/MeshLoader;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v4, "Cannot locate {0} for model {1}"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->key:Lcom/jme3/asset/AssetKey;

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->animData:Lcom/jme3/scene/plugins/ogre/AnimData;

    goto :goto_0
.end method

.method private startSubMesh(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    const/4 v2, 0x0

    new-instance v0, Lcom/jme3/scene/Mesh;

    invoke-direct {v0}, Lcom/jme3/scene/Mesh;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    if-eqz p4, :cond_0

    const-string v0, "triangle_list"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    sget-object v1, Lcom/jme3/scene/Mesh$Mode;->Triangles:Lcom/jme3/scene/Mesh$Mode;

    invoke-virtual {v0, v1}, Lcom/jme3/scene/Mesh;->setMode(Lcom/jme3/scene/Mesh$Mode;)V

    :goto_0
    invoke-static {p3, v2}, Lcom/jme3/util/xml/SAXUtil;->parseBool(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->usesBigIndices:Z

    invoke-static {p2, v2}, Lcom/jme3/util/xml/SAXUtil;->parseBool(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->usesSharedVerts:Z

    iget-boolean v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->usesSharedVerts:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->usesSharedMesh:Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshName:Ljava/lang/String;

    if-nez v0, :cond_5

    new-instance v0, Lcom/jme3/scene/Geometry;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OgreSubmesh-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    invoke-direct {v0, v1, v2}, Lcom/jme3/scene/Geometry;-><init>(Ljava/lang/String;Lcom/jme3/scene/Mesh;)V

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geom:Lcom/jme3/scene/Geometry;

    :goto_2
    iget-boolean v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->usesSharedVerts:Z

    if-eqz v0, :cond_1

    :cond_1
    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geom:Lcom/jme3/scene/Geometry;

    invoke-direct {p0, v0, p1}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->applyMaterial(Lcom/jme3/scene/Geometry;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geoms:Ljava/util/List;

    iget-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geom:Lcom/jme3/scene/Geometry;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_2
    const-string v0, "line_list"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    sget-object v1, Lcom/jme3/scene/Mesh$Mode;->Lines:Lcom/jme3/scene/Mesh$Mode;

    invoke-virtual {v0, v1}, Lcom/jme3/scene/Mesh;->setMode(Lcom/jme3/scene/Mesh$Mode;)V

    goto :goto_0

    :cond_3
    new-instance v0, Lorg/xml/sax/SAXException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported operation type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->usesSharedMesh:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    new-instance v0, Lcom/jme3/scene/Geometry;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-geom-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    invoke-direct {v0, v1, v2}, Lcom/jme3/scene/Geometry;-><init>(Ljava/lang/String;Lcom/jme3/scene/Mesh;)V

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geom:Lcom/jme3/scene/Geometry;

    goto :goto_2
.end method

.method private startSubmeshName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geoms:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/scene/Geometry;

    invoke-virtual {v1, p2}, Lcom/jme3/scene/Geometry;->setName(Ljava/lang/String;)V

    return-void
.end method

.method private startVertex()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->texCoordIndex:I

    return-void
.end method

.method private startVertexBuffer(Lorg/xml/sax/Attributes;)V
    .locals 11
    .param p1    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x0

    const-string v4, "positions"

    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v8}, Lcom/jme3/util/xml/SAXUtil;->parseBool(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Lcom/jme3/scene/VertexBuffer;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Type;->Position:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-direct {v4, v5}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    iget v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vertCount:I

    mul-int/lit8 v4, v4, 0x3

    invoke-static {v4}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v4

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->fb:Ljava/nio/FloatBuffer;

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Usage;->Static:Lcom/jme3/scene/VertexBuffer$Usage;

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Format;->Float:Lcom/jme3/scene/VertexBuffer$Format;

    iget-object v7, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->fb:Ljava/nio/FloatBuffer;

    invoke-virtual {v4, v5, v9, v6, v7}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    invoke-virtual {v4, v5}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    :cond_0
    const-string v4, "normals"

    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v8}, Lcom/jme3/util/xml/SAXUtil;->parseBool(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v4, Lcom/jme3/scene/VertexBuffer;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Type;->Normal:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-direct {v4, v5}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    iget v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vertCount:I

    mul-int/lit8 v4, v4, 0x3

    invoke-static {v4}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v4

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->fb:Ljava/nio/FloatBuffer;

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Usage;->Static:Lcom/jme3/scene/VertexBuffer$Usage;

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Format;->Float:Lcom/jme3/scene/VertexBuffer$Format;

    iget-object v7, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->fb:Ljava/nio/FloatBuffer;

    invoke-virtual {v4, v5, v9, v6, v7}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    invoke-virtual {v4, v5}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    :cond_1
    const-string v4, "colours_diffuse"

    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v8}, Lcom/jme3/util/xml/SAXUtil;->parseBool(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v4, Lcom/jme3/scene/VertexBuffer;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Type;->Color:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-direct {v4, v5}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    iget v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vertCount:I

    mul-int/lit8 v4, v4, 0x4

    invoke-static {v4}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v4

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->fb:Ljava/nio/FloatBuffer;

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Usage;->Static:Lcom/jme3/scene/VertexBuffer$Usage;

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Format;->Float:Lcom/jme3/scene/VertexBuffer$Format;

    iget-object v7, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->fb:Ljava/nio/FloatBuffer;

    invoke-virtual {v4, v5, v10, v6, v7}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    invoke-virtual {v4, v5}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    :cond_2
    const-string v4, "tangents"

    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v8}, Lcom/jme3/util/xml/SAXUtil;->parseBool(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "tangent_dimensions"

    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v9}, Lcom/jme3/util/xml/SAXUtil;->parseInt(Ljava/lang/String;I)I

    move-result v0

    new-instance v4, Lcom/jme3/scene/VertexBuffer;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Type;->Tangent:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-direct {v4, v5}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    iget v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vertCount:I

    mul-int/2addr v4, v0

    invoke-static {v4}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v4

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->fb:Ljava/nio/FloatBuffer;

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Usage;->Static:Lcom/jme3/scene/VertexBuffer$Usage;

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Format;->Float:Lcom/jme3/scene/VertexBuffer$Format;

    iget-object v7, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->fb:Ljava/nio/FloatBuffer;

    invoke-virtual {v4, v5, v0, v6, v7}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    invoke-virtual {v4, v5}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    :cond_3
    const-string v4, "binormals"

    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v8}, Lcom/jme3/util/xml/SAXUtil;->parseBool(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_4

    new-instance v4, Lcom/jme3/scene/VertexBuffer;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Type;->Binormal:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-direct {v4, v5}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    iget v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vertCount:I

    mul-int/lit8 v4, v4, 0x3

    invoke-static {v4}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v4

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->fb:Ljava/nio/FloatBuffer;

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Usage;->Static:Lcom/jme3/scene/VertexBuffer$Usage;

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Format;->Float:Lcom/jme3/scene/VertexBuffer$Format;

    iget-object v7, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->fb:Ljava/nio/FloatBuffer;

    invoke-virtual {v4, v5, v9, v6, v7}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    invoke-virtual {v4, v5}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    :cond_4
    const-string v4, "texture_coords"

    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v8}, Lcom/jme3/util/xml/SAXUtil;->parseInt(Ljava/lang/String;I)I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "texture_coord_dimensions_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/jme3/util/xml/SAXUtil;->parseInt(Ljava/lang/String;I)I

    move-result v1

    const/4 v4, 0x1

    if-lt v1, v4, :cond_5

    if-le v1, v10, :cond_6

    :cond_5
    new-instance v4, Lorg/xml/sax/SAXException;

    const-string v5, "Texture coord dimensions must be 1 <= dims <= 4"

    invoke-direct {v4, v5}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_6
    const/4 v4, 0x7

    if-gt v2, v4, :cond_7

    new-instance v4, Lcom/jme3/scene/VertexBuffer;

    sget-object v5, Lcom/jme3/scene/plugins/ogre/MeshLoader;->TEXCOORD_TYPES:[Lcom/jme3/scene/VertexBuffer$Type;

    aget-object v5, v5, v2

    invoke-direct {v4, v5}, Lcom/jme3/scene/VertexBuffer;-><init>(Lcom/jme3/scene/VertexBuffer$Type;)V

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    iget v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vertCount:I

    mul-int/2addr v4, v1

    invoke-static {v4}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v4

    iput-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->fb:Ljava/nio/FloatBuffer;

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    sget-object v5, Lcom/jme3/scene/VertexBuffer$Usage;->Static:Lcom/jme3/scene/VertexBuffer$Usage;

    sget-object v6, Lcom/jme3/scene/VertexBuffer$Format;->Float:Lcom/jme3/scene/VertexBuffer$Format;

    iget-object v7, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->fb:Ljava/nio/FloatBuffer;

    invoke-virtual {v4, v5, v1, v6, v7}, Lcom/jme3/scene/VertexBuffer;->setupData(Lcom/jme3/scene/VertexBuffer$Usage;ILcom/jme3/scene/VertexBuffer$Format;Ljava/nio/Buffer;)V

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    invoke-virtual {v4, v5}, Lcom/jme3/scene/Mesh;->setBuffer(Lcom/jme3/scene/VertexBuffer;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_7
    new-instance v4, Lorg/xml/sax/SAXException;

    const-string v5, "More than 8 texture coordinates not supported"

    invoke-direct {v4, v5}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_8
    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 0
    .param p1    # [C
    .param p2    # I
    .param p3    # I

    return-void
.end method

.method public endDocument()V
    .locals 0

    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v6, 0x0

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ignoreUntilEnd:Ljava/lang/String;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ignoreUntilEnd:Ljava/lang/String;

    invoke-virtual {v5, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iput-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ignoreUntilEnd:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v5, "submesh"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iput-boolean v7, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->usesBigIndices:Z

    iput-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geom:Lcom/jme3/scene/Geometry;

    iput-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    goto :goto_0

    :cond_2
    const-string v5, "submeshes"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iput-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geom:Lcom/jme3/scene/Geometry;

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sharedMesh:Lcom/jme3/scene/Mesh;

    iput-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    iput-boolean v7, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->usesSharedVerts:Z

    goto :goto_0

    :cond_3
    const-string v5, "faces"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ib:Ljava/nio/IntBuffer;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ib:Ljava/nio/IntBuffer;

    invoke-virtual {v5}, Ljava/nio/IntBuffer;->flip()Ljava/nio/Buffer;

    :goto_1
    iput-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    iput-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ib:Ljava/nio/IntBuffer;

    iput-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sb:Ljava/nio/ShortBuffer;

    goto :goto_0

    :cond_4
    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sb:Ljava/nio/ShortBuffer;

    invoke-virtual {v5}, Ljava/nio/ShortBuffer;->flip()Ljava/nio/Buffer;

    goto :goto_1

    :cond_5
    const-string v5, "vertexbuffer"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    iput-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->fb:Ljava/nio/FloatBuffer;

    iput-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    goto :goto_0

    :cond_6
    const-string v5, "geometry"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    const-string v5, "sharedgeometry"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    :cond_7
    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    invoke-virtual {v5}, Lcom/jme3/scene/Mesh;->getBufferList()Lcom/jme3/util/SafeArrayList;

    move-result-object v5

    invoke-virtual {v5}, Lcom/jme3/util/SafeArrayList;->getArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jme3/scene/VertexBuffer;

    array-length v4, v0

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v4, :cond_9

    aget-object v1, v0, v3

    invoke-virtual {v1}, Lcom/jme3/scene/VertexBuffer;->getData()Ljava/nio/Buffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/Buffer;->position()I

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {v2}, Ljava/nio/Buffer;->flip()Ljava/nio/Buffer;

    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_9
    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    invoke-virtual {v5}, Lcom/jme3/scene/Mesh;->updateBound()V

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    invoke-virtual {v5}, Lcom/jme3/scene/Mesh;->setStatic()V

    const-string v5, "sharedgeometry"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iput-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geom:Lcom/jme3/scene/Geometry;

    iput-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    goto/16 :goto_0

    :cond_a
    const-string v5, "lodfacelist"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sb:Ljava/nio/ShortBuffer;

    invoke-virtual {v5}, Ljava/nio/ShortBuffer;->flip()Ljava/nio/Buffer;

    iput-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    iput-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sb:Ljava/nio/ShortBuffer;

    goto/16 :goto_0

    :cond_b
    const-string v5, "levelofdetail"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-direct {p0}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->endLevelOfDetail()V

    goto/16 :goto_0

    :cond_c
    const-string v5, "boneassignments"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-direct {p0}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->endBoneAssigns()V

    goto/16 :goto_0
.end method

.method public load(Lcom/jme3/asset/AssetInfo;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getKey()Lcom/jme3/asset/AssetKey;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->key:Lcom/jme3/asset/AssetKey;

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->key:Lcom/jme3/asset/AssetKey;

    invoke-virtual {v0}, Lcom/jme3/asset/AssetKey;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshName:Ljava/lang/String;

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->key:Lcom/jme3/asset/AssetKey;

    invoke-virtual {v0}, Lcom/jme3/asset/AssetKey;->getFolder()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->folderName:Ljava/lang/String;

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->key:Lcom/jme3/asset/AssetKey;

    invoke-virtual {v0}, Lcom/jme3/asset/AssetKey;->getExtension()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshName:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshName:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sub-int v0, v4, v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshName:Ljava/lang/String;

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->folderName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->folderName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshName:Ljava/lang/String;

    iget-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->folderName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshName:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getManager()Lcom/jme3/asset/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->key:Lcom/jme3/asset/AssetKey;

    instance-of v0, v0, Lcom/jme3/scene/plugins/ogre/OgreMeshKey;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->key:Lcom/jme3/asset/AssetKey;

    check-cast v0, Lcom/jme3/scene/plugins/ogre/OgreMeshKey;

    invoke-virtual {v0}, Lcom/jme3/scene/plugins/ogre/OgreMeshKey;->getMaterialList()Lcom/jme3/material/MaterialList;

    move-result-object v1

    iput-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->materialList:Lcom/jme3/material/MaterialList;

    invoke-virtual {v0}, Lcom/jme3/scene/plugins/ogre/OgreMeshKey;->getMaterialName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->materialList:Lcom/jme3/material/MaterialList;

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    new-instance v1, Lcom/jme3/scene/plugins/ogre/matext/OgreMaterialKey;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->folderName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".material"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/jme3/scene/plugins/ogre/matext/OgreMaterialKey;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    invoke-interface {v0, v1}, Lcom/jme3/asset/AssetManager;->loadAsset(Lcom/jme3/asset/AssetKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/material/MaterialList;

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->materialList:Lcom/jme3/material/MaterialList;
    :try_end_1
    .catch Lcom/jme3/asset/AssetNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_1
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->materialList:Lcom/jme3/material/MaterialList;

    if-nez v0, :cond_2

    new-instance v1, Lcom/jme3/scene/plugins/ogre/matext/OgreMaterialKey;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->folderName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".material"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/jme3/scene/plugins/ogre/matext/OgreMaterialKey;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/xml/sax/SAXException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_2 .. :try_end_2} :catch_2

    :try_start_3
    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    invoke-interface {v0, v1}, Lcom/jme3/asset/AssetManager;->loadAsset(Lcom/jme3/asset/AssetKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/material/MaterialList;

    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->materialList:Lcom/jme3/material/MaterialList;
    :try_end_3
    .catch Lcom/jme3/asset/AssetNotFoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lorg/xml/sax/SAXException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    :goto_1
    :try_start_4
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljavax/xml/parsers/SAXParserFactory;->setNamespaceAware(Z)V

    invoke-virtual {v0}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v0

    invoke-interface {v0, p0}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    invoke-interface {v0, p0}, Lorg/xml/sax/XMLReader;->setErrorHandler(Lorg/xml/sax/ErrorHandler;)V
    :try_end_4
    .catch Lorg/xml/sax/SAXException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_4 .. :try_end_4} :catch_2

    :try_start_5
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->openStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    new-instance v2, Lorg/xml/sax/InputSource;

    invoke-direct {v2, v1}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface {v0, v2}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v1, :cond_3

    :try_start_7
    invoke-virtual {v1}, Ljava/io/InputStreamReader;->close()V

    :cond_3
    invoke-direct {p0}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->compileModel()Lcom/jme3/scene/Node;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v4, "Cannot locate {0} for model {1}"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v1, 0x1

    iget-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->key:Lcom/jme3/asset/AssetKey;

    aput-object v6, v5, v1

    invoke-virtual {v0, v3, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catch Lorg/xml/sax/SAXException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Error while parsing Ogre3D mesh.xml"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1

    :cond_4
    const/4 v0, 0x0

    :try_start_8
    iput-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->materialList:Lcom/jme3/material/MaterialList;
    :try_end_8
    .catch Lorg/xml/sax/SAXException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Error while parsing Ogre3D mesh.xml"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1

    :catch_3
    move-exception v0

    :try_start_9
    sget-object v0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v4, "Cannot locate {0} for model {1}"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v1, 0x1

    iget-object v6, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->key:Lcom/jme3/asset/AssetKey;

    aput-object v6, v5, v1

    invoke-virtual {v0, v3, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/io/InputStreamReader;->close()V

    :cond_5
    throw v0
    :try_end_9
    .catch Lorg/xml/sax/SAXException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_9 .. :try_end_9} :catch_2

    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method public startDocument()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geoms:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->lodLevels:Lcom/jme3/util/IntMap;

    invoke-virtual {v0}, Lcom/jme3/util/IntMap;->clear()V

    iput-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sb:Ljava/nio/ShortBuffer;

    iput-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ib:Ljava/nio/IntBuffer;

    iput-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->fb:Ljava/nio/FloatBuffer;

    iput-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vb:Lcom/jme3/scene/VertexBuffer;

    iput-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->mesh:Lcom/jme3/scene/Mesh;

    iput-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->geom:Lcom/jme3/scene/Geometry;

    iput-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->sharedMesh:Lcom/jme3/scene/Mesh;

    iget-object v0, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->usesSharedMesh:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-boolean v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->usesSharedVerts:Z

    iput v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->vertCount:I

    iput v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->meshIndex:I

    iput v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->texCoordIndex:I

    iput-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ignoreUntilEnd:Ljava/lang/String;

    iput-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->animData:Lcom/jme3/scene/plugins/ogre/AnimData;

    iput-boolean v2, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->actuallyHasWeights:Z

    iput-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->indicesData:Ljava/nio/ByteBuffer;

    iput-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->weightsFloatData:Ljava/nio/FloatBuffer;

    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ignoreUntilEnd:Ljava/lang/String;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "texcoord"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p4}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->pushTexCoord(Lorg/xml/sax/Attributes;)V

    goto :goto_0

    :cond_2
    const-string v1, "vertexboneassignment"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "vertexindex"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "boneindex"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "weight"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->pushBoneAssign(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v1, "face"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "v1"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "v2"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "v3"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->pushFace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v1, "position"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lcom/jme3/scene/VertexBuffer$Type;->Position:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-direct {p0, v1, p4}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->pushAttrib(Lcom/jme3/scene/VertexBuffer$Type;Lorg/xml/sax/Attributes;)V

    goto :goto_0

    :cond_5
    const-string v1, "normal"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, Lcom/jme3/scene/VertexBuffer$Type;->Normal:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-direct {p0, v1, p4}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->pushAttrib(Lcom/jme3/scene/VertexBuffer$Type;Lorg/xml/sax/Attributes;)V

    goto :goto_0

    :cond_6
    const-string v1, "tangent"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-direct {p0, p4}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->pushTangent(Lorg/xml/sax/Attributes;)V

    goto :goto_0

    :cond_7
    const-string v1, "binormal"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object v1, Lcom/jme3/scene/VertexBuffer$Type;->Binormal:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-direct {p0, v1, p4}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->pushAttrib(Lcom/jme3/scene/VertexBuffer$Type;Lorg/xml/sax/Attributes;)V

    goto :goto_0

    :cond_8
    const-string v1, "colour_diffuse"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-direct {p0, p4}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->pushColor(Lorg/xml/sax/Attributes;)V

    goto/16 :goto_0

    :cond_9
    const-string v1, "vertex"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-direct {p0}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->startVertex()V

    goto/16 :goto_0

    :cond_a
    const-string v1, "faces"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "count"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->startFaces(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    const-string v1, "geometry"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    const-string v1, "vertexcount"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_c

    const-string v1, "count"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_c
    invoke-direct {p0, v0}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->startGeometry(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    const-string v1, "vertexbuffer"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-direct {p0, p4}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->startVertexBuffer(Lorg/xml/sax/Attributes;)V

    goto/16 :goto_0

    :cond_e
    const-string v1, "lodfacelist"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, "submeshindex"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "numfaces"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->startLodFaceList(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_f
    const-string v1, "lodgenerated"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    const-string v1, "fromdepthsquared"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->startLodGenerated(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_10
    const-string v1, "levelofdetail"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    const-string v1, "numlevels"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->startLevelOfDetail(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_11
    const-string v1, "boneassignments"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-direct {p0}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->startBoneAssigns()V

    goto/16 :goto_0

    :cond_12
    const-string v1, "submesh"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    const-string v1, "material"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "usesharedvertices"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "use32bitindexes"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "operationtype"

    invoke-interface {p4, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->startSubMesh(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_13
    const-string v1, "sharedgeometry"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    const-string v1, "vertexcount"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_14

    const-string v1, "count"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_14
    if-eqz v0, :cond_0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->startSharedGeom(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_15
    const-string v1, "submeshes"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "skeletonlink"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    const-string v1, "name"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->startSkeleton(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_16
    const-string v1, "submeshnames"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "submeshname"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    const-string v1, "index"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "name"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/jme3/scene/plugins/ogre/MeshLoader;->startSubmeshName(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_17
    const-string v1, "mesh"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/jme3/scene/plugins/ogre/MeshLoader;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Unknown tag: {0}. Ignoring."

    invoke-virtual {v1, v2, v3, p3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jme3/scene/plugins/ogre/MeshLoader;->ignoreUntilEnd:Ljava/lang/String;

    goto/16 :goto_0
.end method
