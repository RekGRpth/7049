.class public Lcom/jme3/scene/shape/Box;
.super Lcom/jme3/scene/shape/AbstractBox;
.source "Box.java"


# static fields
.field private static final GEOMETRY_INDICES_DATA:[S

.field private static final GEOMETRY_NORMALS_DATA:[F

.field private static final GEOMETRY_TEXTURE_DATA:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x24

    new-array v0, v0, [S

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jme3/scene/shape/Box;->GEOMETRY_INDICES_DATA:[S

    const/16 v0, 0x48

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/jme3/scene/shape/Box;->GEOMETRY_NORMALS_DATA:[F

    const/16 v0, 0x30

    new-array v0, v0, [F

    fill-array-data v0, :array_2

    sput-object v0, Lcom/jme3/scene/shape/Box;->GEOMETRY_TEXTURE_DATA:[F

    return-void

    :array_0
    .array-data 2
        0x2s
        0x1s
        0x0s
        0x3s
        0x2s
        0x0s
        0x6s
        0x5s
        0x4s
        0x7s
        0x6s
        0x4s
        0xas
        0x9s
        0x8s
        0xbs
        0xas
        0x8s
        0xes
        0xds
        0xcs
        0xfs
        0xes
        0xcs
        0x12s
        0x11s
        0x10s
        0x13s
        0x12s
        0x10s
        0x16s
        0x15s
        0x14s
        0x17s
        0x16s
        0x14s
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        -0x40800000
        0x0
        0x0
        -0x40800000
        0x0
        0x0
        -0x40800000
        0x0
        0x0
        -0x40800000
        0x3f800000
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x3f800000
        -0x40800000
        0x0
        0x0
        -0x40800000
        0x0
        0x0
        -0x40800000
        0x0
        0x0
        -0x40800000
        0x0
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        0x3f800000
        0x0
        0x0
        -0x40800000
        0x0
        0x0
        -0x40800000
        0x0
        0x0
        -0x40800000
        0x0
        0x0
        -0x40800000
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x3f800000
        0x3f800000
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x3f800000
        0x3f800000
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x3f800000
        0x3f800000
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x3f800000
        0x3f800000
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x3f800000
        0x3f800000
        0x3f800000
        0x0
        0x0
        0x0
        0x0
        0x3f800000
        0x3f800000
        0x3f800000
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jme3/scene/shape/AbstractBox;-><init>()V

    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # F

    invoke-direct {p0}, Lcom/jme3/scene/shape/AbstractBox;-><init>()V

    sget-object v0, Lcom/jme3/math/Vector3f;->ZERO:Lcom/jme3/math/Vector3f;

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/jme3/scene/shape/Box;->updateGeometry(Lcom/jme3/math/Vector3f;FFF)V

    return-void
.end method

.method public constructor <init>(Lcom/jme3/math/Vector3f;FFF)V
    .locals 0
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # F
    .param p3    # F
    .param p4    # F

    invoke-direct {p0}, Lcom/jme3/scene/shape/AbstractBox;-><init>()V

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/jme3/scene/shape/Box;->updateGeometry(Lcom/jme3/math/Vector3f;FFF)V

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Lcom/jme3/scene/Mesh;
    .locals 1

    invoke-virtual {p0}, Lcom/jme3/scene/shape/Box;->clone()Lcom/jme3/scene/shape/Box;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/jme3/scene/shape/Box;
    .locals 5

    new-instance v0, Lcom/jme3/scene/shape/Box;

    iget-object v1, p0, Lcom/jme3/scene/shape/Box;->center:Lcom/jme3/math/Vector3f;

    invoke-virtual {v1}, Lcom/jme3/math/Vector3f;->clone()Lcom/jme3/math/Vector3f;

    move-result-object v1

    iget v2, p0, Lcom/jme3/scene/shape/Box;->xExtent:F

    iget v3, p0, Lcom/jme3/scene/shape/Box;->yExtent:F

    iget v4, p0, Lcom/jme3/scene/shape/Box;->zExtent:F

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/jme3/scene/shape/Box;-><init>(Lcom/jme3/math/Vector3f;FFF)V

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/scene/shape/Box;->clone()Lcom/jme3/scene/shape/Box;

    move-result-object v0

    return-object v0
.end method

.method protected duUpdateGeometryIndices()V
    .locals 3

    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v0}, Lcom/jme3/scene/shape/Box;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    const/4 v1, 0x3

    sget-object v2, Lcom/jme3/scene/shape/Box;->GEOMETRY_INDICES_DATA:[S

    invoke-static {v2}, Lcom/jme3/util/BufferUtils;->createShortBuffer([S)Ljava/nio/ShortBuffer;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/jme3/scene/shape/Box;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILjava/nio/ShortBuffer;)V

    :cond_0
    return-void
.end method

.method protected duUpdateGeometryNormals()V
    .locals 3

    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->Normal:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v0}, Lcom/jme3/scene/shape/Box;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->Normal:Lcom/jme3/scene/VertexBuffer$Type;

    const/4 v1, 0x3

    sget-object v2, Lcom/jme3/scene/shape/Box;->GEOMETRY_NORMALS_DATA:[F

    invoke-static {v2}, Lcom/jme3/util/BufferUtils;->createFloatBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/jme3/scene/shape/Box;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILjava/nio/FloatBuffer;)V

    :cond_0
    return-void
.end method

.method protected duUpdateGeometryTextures()V
    .locals 3

    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->TexCoord:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v0}, Lcom/jme3/scene/shape/Box;->getBuffer(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/scene/VertexBuffer;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->TexCoord:Lcom/jme3/scene/VertexBuffer$Type;

    const/4 v1, 0x2

    sget-object v2, Lcom/jme3/scene/shape/Box;->GEOMETRY_TEXTURE_DATA:[F

    invoke-static {v2}, Lcom/jme3/util/BufferUtils;->createFloatBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/jme3/scene/shape/Box;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILjava/nio/FloatBuffer;)V

    :cond_0
    return-void
.end method

.method protected duUpdateGeometryVertices()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/16 v2, 0x18

    invoke-static {v2}, Lcom/jme3/util/BufferUtils;->createVector3Buffer(I)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jme3/scene/shape/Box;->computeVertices()[Lcom/jme3/math/Vector3f;

    move-result-object v1

    const/16 v2, 0x48

    new-array v2, v2, [F

    aget-object v3, v1, v6

    iget v3, v3, Lcom/jme3/math/Vector3f;->x:F

    aput v3, v2, v6

    aget-object v3, v1, v6

    iget v3, v3, Lcom/jme3/math/Vector3f;->y:F

    aput v3, v2, v7

    aget-object v3, v1, v6

    iget v3, v3, Lcom/jme3/math/Vector3f;->z:F

    aput v3, v2, v8

    aget-object v3, v1, v7

    iget v3, v3, Lcom/jme3/math/Vector3f;->x:F

    aput v3, v2, v5

    aget-object v3, v1, v7

    iget v3, v3, Lcom/jme3/math/Vector3f;->y:F

    aput v3, v2, v9

    const/4 v3, 0x5

    aget-object v4, v1, v7

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/4 v3, 0x6

    aget-object v4, v1, v8

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/4 v3, 0x7

    aget-object v4, v1, v8

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x8

    aget-object v4, v1, v8

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x9

    aget-object v4, v1, v5

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0xa

    aget-object v4, v1, v5

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0xb

    aget-object v4, v1, v5

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0xc

    aget-object v4, v1, v7

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0xd

    aget-object v4, v1, v7

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0xe

    aget-object v4, v1, v7

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0xf

    aget-object v4, v1, v9

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x10

    aget-object v4, v1, v9

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x11

    aget-object v4, v1, v9

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x12

    const/4 v4, 0x6

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x13

    const/4 v4, 0x6

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x14

    const/4 v4, 0x6

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x15

    aget-object v4, v1, v8

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x16

    aget-object v4, v1, v8

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x17

    aget-object v4, v1, v8

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x18

    aget-object v4, v1, v9

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x19

    aget-object v4, v1, v9

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x1a

    aget-object v4, v1, v9

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x1b

    const/4 v4, 0x5

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x1c

    const/4 v4, 0x5

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x1d

    const/4 v4, 0x5

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x1e

    const/4 v4, 0x7

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x1f

    const/4 v4, 0x7

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x20

    const/4 v4, 0x7

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x21

    const/4 v4, 0x6

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x22

    const/4 v4, 0x6

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x23

    const/4 v4, 0x6

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x24

    const/4 v4, 0x5

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x25

    const/4 v4, 0x5

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x26

    const/4 v4, 0x5

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x27

    aget-object v4, v1, v6

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x28

    aget-object v4, v1, v6

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x29

    aget-object v4, v1, v6

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x2a

    aget-object v4, v1, v5

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x2b

    aget-object v4, v1, v5

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x2c

    aget-object v4, v1, v5

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x2d

    const/4 v4, 0x7

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x2e

    const/4 v4, 0x7

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x2f

    const/4 v4, 0x7

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x30

    aget-object v4, v1, v8

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x31

    aget-object v4, v1, v8

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x32

    aget-object v4, v1, v8

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x33

    const/4 v4, 0x6

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x34

    const/4 v4, 0x6

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x35

    const/4 v4, 0x6

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x36

    const/4 v4, 0x7

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x37

    const/4 v4, 0x7

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x38

    const/4 v4, 0x7

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x39

    aget-object v4, v1, v5

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x3a

    aget-object v4, v1, v5

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x3b

    aget-object v4, v1, v5

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x3c

    aget-object v4, v1, v6

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x3d

    aget-object v4, v1, v6

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x3e

    aget-object v4, v1, v6

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x3f

    const/4 v4, 0x5

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x40

    const/4 v4, 0x5

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x41

    const/4 v4, 0x5

    aget-object v4, v1, v4

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x42

    aget-object v4, v1, v9

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x43

    aget-object v4, v1, v9

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x44

    aget-object v4, v1, v9

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    const/16 v3, 0x45

    aget-object v4, v1, v7

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    aput v4, v2, v3

    const/16 v3, 0x46

    aget-object v4, v1, v7

    iget v4, v4, Lcom/jme3/math/Vector3f;->y:F

    aput v4, v2, v3

    const/16 v3, 0x47

    aget-object v4, v1, v7

    iget v4, v4, Lcom/jme3/math/Vector3f;->z:F

    aput v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    sget-object v2, Lcom/jme3/scene/VertexBuffer$Type;->Position:Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p0, v2, v5, v0}, Lcom/jme3/scene/shape/Box;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;ILjava/nio/FloatBuffer;)V

    invoke-virtual {p0}, Lcom/jme3/scene/shape/Box;->updateBound()V

    return-void
.end method
