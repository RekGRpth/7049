.class public Lcom/jme3/shader/Shader$ShaderSource;
.super Lcom/jme3/util/NativeObject;
.source "Shader.java"

# interfaces
.implements Lcom/jme3/export/Savable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jme3/shader/Shader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShaderSource"
.end annotation


# instance fields
.field defines:Ljava/lang/String;

.field name:Ljava/lang/String;

.field shaderType:Lcom/jme3/shader/Shader$ShaderType;

.field source:Ljava/lang/String;

.field usable:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lcom/jme3/shader/Shader$ShaderSource;

    invoke-direct {p0, v0}, Lcom/jme3/util/NativeObject;-><init>(Ljava/lang/Class;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/shader/Shader$ShaderSource;->usable:Z

    iput-object v1, p0, Lcom/jme3/shader/Shader$ShaderSource;->name:Ljava/lang/String;

    iput-object v1, p0, Lcom/jme3/shader/Shader$ShaderSource;->source:Ljava/lang/String;

    iput-object v1, p0, Lcom/jme3/shader/Shader$ShaderSource;->defines:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/jme3/shader/Shader$ShaderSource;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const-class v0, Lcom/jme3/shader/Shader$ShaderSource;

    iget v1, p1, Lcom/jme3/shader/Shader$ShaderSource;->id:I

    invoke-direct {p0, v0, v1}, Lcom/jme3/util/NativeObject;-><init>(Ljava/lang/Class;I)V

    iput-boolean v3, p0, Lcom/jme3/shader/Shader$ShaderSource;->usable:Z

    iput-object v2, p0, Lcom/jme3/shader/Shader$ShaderSource;->name:Ljava/lang/String;

    iput-object v2, p0, Lcom/jme3/shader/Shader$ShaderSource;->source:Ljava/lang/String;

    iput-object v2, p0, Lcom/jme3/shader/Shader$ShaderSource;->defines:Ljava/lang/String;

    iget-object v0, p1, Lcom/jme3/shader/Shader$ShaderSource;->shaderType:Lcom/jme3/shader/Shader$ShaderType;

    iput-object v0, p0, Lcom/jme3/shader/Shader$ShaderSource;->shaderType:Lcom/jme3/shader/Shader$ShaderType;

    iput-boolean v3, p0, Lcom/jme3/shader/Shader$ShaderSource;->usable:Z

    iget-object v0, p1, Lcom/jme3/shader/Shader$ShaderSource;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/jme3/shader/Shader$ShaderSource;->name:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/jme3/shader/Shader$ShaderType;)V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lcom/jme3/shader/Shader$ShaderSource;

    invoke-direct {p0, v0}, Lcom/jme3/util/NativeObject;-><init>(Ljava/lang/Class;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/shader/Shader$ShaderSource;->usable:Z

    iput-object v1, p0, Lcom/jme3/shader/Shader$ShaderSource;->name:Ljava/lang/String;

    iput-object v1, p0, Lcom/jme3/shader/Shader$ShaderSource;->source:Ljava/lang/String;

    iput-object v1, p0, Lcom/jme3/shader/Shader$ShaderSource;->defines:Ljava/lang/String;

    iput-object p1, p0, Lcom/jme3/shader/Shader$ShaderSource;->shaderType:Lcom/jme3/shader/Shader$ShaderType;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "The shader type must be specified"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public createDestructableClone()Lcom/jme3/util/NativeObject;
    .locals 1

    new-instance v0, Lcom/jme3/shader/Shader$ShaderSource;

    invoke-direct {v0, p0}, Lcom/jme3/shader/Shader$ShaderSource;-><init>(Lcom/jme3/shader/Shader$ShaderSource;)V

    return-object v0
.end method

.method public deleteObject(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/jme3/renderer/Renderer;

    invoke-interface {p1, p0}, Lcom/jme3/renderer/Renderer;->deleteShaderSource(Lcom/jme3/shader/Shader$ShaderSource;)V

    return-void
.end method

.method public getDefines()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jme3/shader/Shader$ShaderSource;->defines:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jme3/shader/Shader$ShaderSource;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jme3/shader/Shader$ShaderSource;->source:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/jme3/shader/Shader$ShaderType;
    .locals 1

    iget-object v0, p0, Lcom/jme3/shader/Shader$ShaderSource;->shaderType:Lcom/jme3/shader/Shader$ShaderType;

    return-object v0
.end method

.method public isUsable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/shader/Shader$ShaderSource;->usable:Z

    return v0
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v1

    const-string v0, "shaderType"

    const-class v2, Lcom/jme3/shader/Shader$ShaderType;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/shader/Shader$ShaderType;

    iput-object v0, p0, Lcom/jme3/shader/Shader$ShaderSource;->shaderType:Lcom/jme3/shader/Shader$ShaderType;

    const-string v0, "name"

    invoke-interface {v1, v0, v3}, Lcom/jme3/export/InputCapsule;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/shader/Shader$ShaderSource;->name:Ljava/lang/String;

    const-string v0, "source"

    invoke-interface {v1, v0, v3}, Lcom/jme3/export/InputCapsule;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/shader/Shader$ShaderSource;->source:Ljava/lang/String;

    const-string v0, "defines"

    invoke-interface {v1, v0, v3}, Lcom/jme3/export/InputCapsule;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/shader/Shader$ShaderSource;->defines:Ljava/lang/String;

    return-void
.end method

.method public resetObject()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/jme3/shader/Shader$ShaderSource;->id:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/shader/Shader$ShaderSource;->usable:Z

    invoke-virtual {p0}, Lcom/jme3/shader/Shader$ShaderSource;->setUpdateNeeded()V

    return-void
.end method

.method public setDefines(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Shader defines cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/jme3/shader/Shader$ShaderSource;->defines:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/jme3/shader/Shader$ShaderSource;->setUpdateNeeded()V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jme3/shader/Shader$ShaderSource;->name:Ljava/lang/String;

    return-void
.end method

.method public setSource(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Shader source cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/jme3/shader/Shader$ShaderSource;->source:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/jme3/shader/Shader$ShaderSource;->setUpdateNeeded()V

    return-void
.end method

.method public setUsable(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jme3/shader/Shader$ShaderSource;->usable:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, ""

    iget-object v1, p0, Lcom/jme3/shader/Shader$ShaderSource;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jme3/shader/Shader$ShaderSource;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/jme3/shader/Shader$ShaderSource;->defines:Ljava/lang/String;

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "defines, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jme3/shader/Shader$ShaderSource;->shaderType:Lcom/jme3/shader/Shader$ShaderType;

    invoke-virtual {v2}, Lcom/jme3/shader/Shader$ShaderType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
