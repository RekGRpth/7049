.class public Lcom/jme3/system/NullRenderer;
.super Ljava/lang/Object;
.source "NullRenderer.java"

# interfaces
.implements Lcom/jme3/renderer/Renderer;


# static fields
.field private static final caps:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/jme3/renderer/Caps;",
            ">;"
        }
    .end annotation
.end field

.field private static final stats:Lcom/jme3/renderer/Statistics;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/renderer/Caps;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/jme3/system/NullRenderer;->caps:Ljava/util/EnumSet;

    new-instance v0, Lcom/jme3/renderer/Statistics;

    invoke-direct {v0}, Lcom/jme3/renderer/Statistics;-><init>()V

    sput-object v0, Lcom/jme3/system/NullRenderer;->stats:Lcom/jme3/renderer/Statistics;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public applyRenderState(Lcom/jme3/material/RenderState;)V
    .locals 0
    .param p1    # Lcom/jme3/material/RenderState;

    return-void
.end method

.method public clearBuffers(ZZZ)V
    .locals 0
    .param p1    # Z
    .param p2    # Z
    .param p3    # Z

    return-void
.end method

.method public deleteBuffer(Lcom/jme3/scene/VertexBuffer;)V
    .locals 0
    .param p1    # Lcom/jme3/scene/VertexBuffer;

    return-void
.end method

.method public deleteFrameBuffer(Lcom/jme3/texture/FrameBuffer;)V
    .locals 0
    .param p1    # Lcom/jme3/texture/FrameBuffer;

    return-void
.end method

.method public deleteImage(Lcom/jme3/texture/Image;)V
    .locals 0
    .param p1    # Lcom/jme3/texture/Image;

    return-void
.end method

.method public deleteShader(Lcom/jme3/shader/Shader;)V
    .locals 0
    .param p1    # Lcom/jme3/shader/Shader;

    return-void
.end method

.method public deleteShaderSource(Lcom/jme3/shader/Shader$ShaderSource;)V
    .locals 0
    .param p1    # Lcom/jme3/shader/Shader$ShaderSource;

    return-void
.end method

.method public getCaps()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/jme3/renderer/Caps;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/jme3/system/NullRenderer;->caps:Ljava/util/EnumSet;

    return-object v0
.end method

.method public getStatistics()Lcom/jme3/renderer/Statistics;
    .locals 1

    sget-object v0, Lcom/jme3/system/NullRenderer;->stats:Lcom/jme3/renderer/Statistics;

    return-object v0
.end method

.method public renderMesh(Lcom/jme3/scene/Mesh;II)V
    .locals 0
    .param p1    # Lcom/jme3/scene/Mesh;
    .param p2    # I
    .param p3    # I

    return-void
.end method

.method public setBackgroundColor(Lcom/jme3/math/ColorRGBA;)V
    .locals 0
    .param p1    # Lcom/jme3/math/ColorRGBA;

    return-void
.end method

.method public setClipRect(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public setDepthRange(FF)V
    .locals 0
    .param p1    # F
    .param p2    # F

    return-void
.end method

.method public setFrameBuffer(Lcom/jme3/texture/FrameBuffer;)V
    .locals 0
    .param p1    # Lcom/jme3/texture/FrameBuffer;

    return-void
.end method

.method public setLighting(Lcom/jme3/light/LightList;)V
    .locals 0
    .param p1    # Lcom/jme3/light/LightList;

    return-void
.end method

.method public setShader(Lcom/jme3/shader/Shader;)V
    .locals 0
    .param p1    # Lcom/jme3/shader/Shader;

    return-void
.end method

.method public setTexture(ILcom/jme3/texture/Texture;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/jme3/texture/Texture;

    return-void
.end method

.method public setViewPort(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public setViewProjectionMatrices(Lcom/jme3/math/Matrix4f;Lcom/jme3/math/Matrix4f;)V
    .locals 0
    .param p1    # Lcom/jme3/math/Matrix4f;
    .param p2    # Lcom/jme3/math/Matrix4f;

    return-void
.end method

.method public setWorldMatrix(Lcom/jme3/math/Matrix4f;)V
    .locals 0
    .param p1    # Lcom/jme3/math/Matrix4f;

    return-void
.end method
