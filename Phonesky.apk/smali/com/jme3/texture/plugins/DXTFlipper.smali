.class public Lcom/jme3/texture/plugins/DXTFlipper;
.super Ljava/lang/Object;
.source "DXTFlipper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/texture/plugins/DXTFlipper$1;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final bb:Ljava/nio/ByteBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/jme3/texture/plugins/DXTFlipper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/jme3/texture/plugins/DXTFlipper;->$assertionsDisabled:Z

    const/16 v0, 0x8

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sput-object v0, Lcom/jme3/texture/plugins/DXTFlipper;->bb:Ljava/nio/ByteBuffer;

    sget-object v0, Lcom/jme3/texture/plugins/DXTFlipper;->bb:Ljava/nio/ByteBuffer;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static flipDXT(Ljava/nio/ByteBuffer;IILcom/jme3/texture/Image$Format;)Ljava/nio/ByteBuffer;
    .locals 13
    .param p0    # Ljava/nio/ByteBuffer;
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/jme3/texture/Image$Format;

    int-to-float v11, p1

    const/high16 v12, 0x40800000

    div-float/2addr v11, v12

    invoke-static {v11}, Lcom/jme3/math/FastMath;->ceil(F)F

    move-result v11

    float-to-int v3, v11

    int-to-float v11, p2

    const/high16 v12, 0x40800000

    div-float/2addr v11, v12

    invoke-static {v11}, Lcom/jme3/math/FastMath;->ceil(F)F

    move-result v11

    float-to-int v4, v11

    sget-object v11, Lcom/jme3/texture/plugins/DXTFlipper$1;->$SwitchMap$com$jme3$texture$Image$Format:[I

    invoke-virtual/range {p3 .. p3}, Lcom/jme3/texture/Image$Format;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_0

    new-instance v11, Ljava/lang/IllegalArgumentException;

    invoke-direct {v11}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v11

    :pswitch_0
    const/4 v8, 0x1

    :goto_0
    const/4 v11, 0x1

    if-eq v8, v11, :cond_0

    const/4 v11, 0x5

    if-ne v8, v11, :cond_1

    :cond_0
    const/16 v5, 0x8

    :goto_1
    mul-int v11, v3, v4

    mul-int/2addr v11, v5

    invoke-static {v11}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    const/4 v11, 0x1

    if-ne p2, v11, :cond_2

    invoke-virtual {v7, p0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    :goto_2
    return-object v7

    :pswitch_1
    const/4 v8, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v8, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v8, 0x4

    goto :goto_0

    :pswitch_4
    const/4 v8, 0x5

    goto :goto_0

    :cond_1
    const/16 v5, 0x10

    goto :goto_1

    :cond_2
    const/4 v11, 0x2

    if-ne p2, v11, :cond_8

    const/16 v11, 0x8

    new-array v6, v11, [B

    const/4 v11, 0x1

    if-eq v8, v11, :cond_5

    const/4 v11, 0x5

    if-eq v8, v11, :cond_5

    const/16 v11, 0x8

    new-array v0, v11, [B

    :goto_3
    const/4 v9, 0x0

    :goto_4
    if-ge v9, v3, :cond_7

    mul-int v1, v9, v5

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    add-int v11, v1, v5

    invoke-virtual {p0, v11}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    invoke-virtual {p0, v6}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    const/4 v11, 0x4

    if-eq v8, v11, :cond_3

    const/4 v11, 0x5

    if-ne v8, v11, :cond_6

    :cond_3
    invoke-static {v6, p2}, Lcom/jme3/texture/plugins/DXTFlipper;->flipDXT5Block([BI)V

    :goto_5
    invoke-virtual {v7, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_4

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    packed-switch v8, :pswitch_data_1

    :goto_6
    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    :cond_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    :cond_6
    invoke-static {v6, p2}, Lcom/jme3/texture/plugins/DXTFlipper;->flipDXT1orDXTA3Block([BI)V

    goto :goto_5

    :pswitch_5
    invoke-static {v0, p2}, Lcom/jme3/texture/plugins/DXTFlipper;->flipDXT3Block([BI)V

    goto :goto_6

    :pswitch_6
    invoke-static {v0, p2}, Lcom/jme3/texture/plugins/DXTFlipper;->flipDXT5Block([BI)V

    goto :goto_6

    :cond_7
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    goto :goto_2

    :cond_8
    const/4 v11, 0x4

    if-lt p2, v11, :cond_f

    const/16 v11, 0x8

    new-array v6, v11, [B

    const/4 v11, 0x1

    if-eq v8, v11, :cond_b

    const/4 v11, 0x5

    if-eq v8, v11, :cond_b

    const/16 v11, 0x8

    new-array v0, v11, [B

    :goto_7
    const/4 v10, 0x0

    :goto_8
    if-ge v10, v4, :cond_e

    const/4 v9, 0x0

    :goto_9
    if-ge v9, v3, :cond_d

    mul-int v11, v10, v3

    add-int v2, v11, v9

    mul-int v1, v2, v5

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    add-int v11, v1, v5

    invoke-virtual {p0, v11}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    sub-int v11, v4, v10

    add-int/lit8 v11, v11, -0x1

    mul-int/2addr v11, v3

    add-int v2, v11, v9

    mul-int v1, v2, v5

    invoke-virtual {v7, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    add-int v11, v1, v5

    invoke-virtual {v7, v11}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    if-eqz v0, :cond_9

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    packed-switch v8, :pswitch_data_2

    :goto_a
    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    :cond_9
    invoke-virtual {p0, v6}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    const/4 v11, 0x4

    if-eq v8, v11, :cond_a

    const/4 v11, 0x5

    if-ne v8, v11, :cond_c

    :cond_a
    invoke-static {v6, p2}, Lcom/jme3/texture/plugins/DXTFlipper;->flipDXT5Block([BI)V

    :goto_b
    invoke-virtual {v7, v6}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    add-int/lit8 v9, v9, 0x1

    goto :goto_9

    :cond_b
    const/4 v0, 0x0

    goto :goto_7

    :pswitch_7
    invoke-static {v0, p2}, Lcom/jme3/texture/plugins/DXTFlipper;->flipDXT3Block([BI)V

    goto :goto_a

    :pswitch_8
    invoke-static {v0, p2}, Lcom/jme3/texture/plugins/DXTFlipper;->flipDXT5Block([BI)V

    goto :goto_a

    :cond_c
    invoke-static {v6, p2}, Lcom/jme3/texture/plugins/DXTFlipper;->flipDXT1orDXTA3Block([BI)V

    goto :goto_b

    :cond_d
    add-int/lit8 v10, v10, 0x1

    goto :goto_8

    :cond_e
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v11

    invoke-virtual {v7, v11}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    const/4 v11, 0x0

    invoke-virtual {v7, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/16 :goto_2

    :cond_f
    const/4 v7, 0x0

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_5
        :pswitch_6
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_7
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method private static flipDXT1orDXTA3Block([BI)V
    .locals 6
    .param p0    # [B
    .param p1    # I

    const/4 v5, 0x7

    const/4 v4, 0x6

    const/4 v3, 0x5

    const/4 v2, 0x4

    packed-switch p1, :pswitch_data_0

    aget-byte v0, p0, v5

    aget-byte v1, p0, v2

    aput-byte v1, p0, v5

    aput-byte v0, p0, v2

    aget-byte v0, p0, v4

    aget-byte v1, p0, v3

    aput-byte v1, p0, v4

    aput-byte v0, p0, v3

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    aget-byte v0, p0, v3

    aget-byte v1, p0, v2

    aput-byte v1, p0, v3

    aput-byte v0, p0, v2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static flipDXT3Block([BI)V
    .locals 8
    .param p0    # [B
    .param p1    # I

    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x2

    if-ne p1, v4, :cond_0

    :goto_0
    return-void

    :cond_0
    aget-byte v0, p0, v6

    aget-byte v1, p0, v4

    if-ne p1, v3, :cond_1

    aget-byte v2, p0, v3

    aput-byte v2, p0, v6

    aget-byte v2, p0, v5

    aput-byte v2, p0, v4

    aput-byte v0, p0, v3

    aput-byte v1, p0, v5

    goto :goto_0

    :cond_1
    const/4 v2, 0x6

    aget-byte v2, p0, v2

    aput-byte v2, p0, v6

    const/4 v2, 0x7

    aget-byte v2, p0, v2

    aput-byte v2, p0, v4

    const/4 v2, 0x6

    aput-byte v0, p0, v2

    const/4 v2, 0x7

    aput-byte v1, p0, v2

    aget-byte v0, p0, v3

    aget-byte v1, p0, v5

    aget-byte v2, p0, v7

    aput-byte v2, p0, v3

    const/4 v2, 0x5

    aget-byte v2, p0, v2

    aput-byte v2, p0, v5

    aput-byte v0, p0, v7

    const/4 v2, 0x5

    aput-byte v1, p0, v2

    goto :goto_0
.end method

.method private static flipDXT5Block([BI)V
    .locals 10
    .param p0    # [B
    .param p1    # I

    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x0

    aget-byte v6, p0, v2

    const/4 v2, 0x1

    aget-byte v7, p0, v2

    sget-object v2, Lcom/jme3/texture/plugins/DXTFlipper;->bb:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    sget-object v2, Lcom/jme3/texture/plugins/DXTFlipper;->bb:Ljava/nio/ByteBuffer;

    const/4 v3, 0x2

    const/4 v4, 0x6

    invoke-virtual {v2, p0, v3, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    sget-object v2, Lcom/jme3/texture/plugins/DXTFlipper;->bb:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    sget-object v2, Lcom/jme3/texture/plugins/DXTFlipper;->bb:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v8

    move-wide v0, v8

    const/4 v2, 0x2

    if-ne p1, v2, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x1

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x3

    const/4 v3, 0x1

    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    :goto_0
    sget-object v2, Lcom/jme3/texture/plugins/DXTFlipper;->bb:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    sget-object v2, Lcom/jme3/texture/plugins/DXTFlipper;->bb:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    sget-object v2, Lcom/jme3/texture/plugins/DXTFlipper;->bb:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    sget-object v2, Lcom/jme3/texture/plugins/DXTFlipper;->bb:Ljava/nio/ByteBuffer;

    const/4 v3, 0x2

    const/4 v4, 0x6

    invoke-virtual {v2, p0, v3, v4}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    sget-boolean v2, Lcom/jme3/texture/plugins/DXTFlipper;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    const/4 v2, 0x0

    aget-byte v2, p0, v2

    if-ne v6, v2, :cond_2

    const/4 v2, 0x1

    aget-byte v2, p0, v2

    if-eq v7, v2, :cond_0

    :cond_2
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_3
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x3

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x3

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x3

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x2

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x2

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x3

    const/4 v3, 0x1

    const/4 v4, 0x3

    const/4 v5, 0x2

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x2

    const/4 v3, 0x2

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x1

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x0

    const/4 v3, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x1

    const/4 v3, 0x3

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x2

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    const/4 v2, 0x3

    const/4 v3, 0x3

    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-static {v8, v9, v4, v5}, Lcom/jme3/texture/plugins/DXTFlipper;->readCode5(JII)J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/jme3/texture/plugins/DXTFlipper;->writeCode5(JIIJ)J

    move-result-wide v0

    goto/16 :goto_0
.end method

.method private static readCode5(JII)J
    .locals 7
    .param p0    # J
    .param p2    # I
    .param p3    # I

    mul-int/lit8 v6, p3, 0x4

    add-int/2addr v6, p2

    mul-int/lit8 v6, v6, 0x3

    int-to-long v4, v6

    const-wide/16 v2, 0x7

    long-to-int v6, v4

    shl-long/2addr v2, v6

    and-long v0, p0, v2

    long-to-int v6, v4

    shr-long/2addr v0, v6

    return-wide v0
.end method

.method private static writeCode5(JIIJ)J
    .locals 7
    .param p0    # J
    .param p2    # I
    .param p3    # I
    .param p4    # J

    mul-int/lit8 v4, p3, 0x4

    add-int/2addr v4, p2

    mul-int/lit8 v4, v4, 0x3

    int-to-long v2, v4

    const-wide/16 v0, 0x7

    and-long v4, p4, v0

    long-to-int v6, v2

    shl-long p4, v4, v6

    long-to-int v4, v2

    shl-long/2addr v0, v4

    const-wide/16 v4, -0x1

    xor-long/2addr v0, v4

    and-long/2addr p0, v0

    or-long/2addr p0, p4

    return-wide p0
.end method
