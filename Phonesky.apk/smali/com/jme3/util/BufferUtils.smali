.class public final Lcom/jme3/util/BufferUtils;
.super Ljava/lang/Object;
.source "BufferUtils.java"


# static fields
.field private static cleanMethod:Ljava/lang/reflect/Method;

.field private static cleanerMethod:Ljava/lang/reflect/Method;

.field private static freeMethod:Ljava/lang/reflect/Method;

.field private static final loadedMethods:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static final ref:Ljava/lang/Object;

.field private static final trackingHash:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/nio/Buffer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static viewedBufferMethod:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/jme3/util/BufferUtils;->trackingHash:Ljava/util/Map;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/jme3/util/BufferUtils;->ref:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/jme3/util/BufferUtils;->loadedMethods:Ljava/util/concurrent/atomic/AtomicBoolean;

    sput-object v2, Lcom/jme3/util/BufferUtils;->cleanerMethod:Ljava/lang/reflect/Method;

    sput-object v2, Lcom/jme3/util/BufferUtils;->cleanMethod:Ljava/lang/reflect/Method;

    sput-object v2, Lcom/jme3/util/BufferUtils;->viewedBufferMethod:Ljava/lang/reflect/Method;

    sput-object v2, Lcom/jme3/util/BufferUtils;->freeMethod:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clone(Ljava/nio/Buffer;)Ljava/nio/Buffer;
    .locals 1
    .param p0    # Ljava/nio/Buffer;

    instance-of v0, p0, Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/nio/FloatBuffer;

    invoke-static {p0}, Lcom/jme3/util/BufferUtils;->clone(Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p0, Ljava/nio/ShortBuffer;

    if-eqz v0, :cond_1

    check-cast p0, Ljava/nio/ShortBuffer;

    invoke-static {p0}, Lcom/jme3/util/BufferUtils;->clone(Ljava/nio/ShortBuffer;)Ljava/nio/ShortBuffer;

    move-result-object v0

    goto :goto_0

    :cond_1
    instance-of v0, p0, Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_2

    check-cast p0, Ljava/nio/ByteBuffer;

    invoke-static {p0}, Lcom/jme3/util/BufferUtils;->clone(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_0

    :cond_2
    instance-of v0, p0, Ljava/nio/IntBuffer;

    if-eqz v0, :cond_3

    check-cast p0, Ljava/nio/IntBuffer;

    invoke-static {p0}, Lcom/jme3/util/BufferUtils;->clone(Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;

    move-result-object v0

    goto :goto_0

    :cond_3
    instance-of v0, p0, Ljava/nio/DoubleBuffer;

    if-eqz v0, :cond_4

    check-cast p0, Ljava/nio/DoubleBuffer;

    invoke-static {p0}, Lcom/jme3/util/BufferUtils;->clone(Ljava/nio/DoubleBuffer;)Ljava/nio/DoubleBuffer;

    move-result-object v0

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static clone(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 2
    .param p0    # Ljava/nio/ByteBuffer;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    invoke-static {v1}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    :goto_1
    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_1
.end method

.method public static clone(Ljava/nio/DoubleBuffer;)Ljava/nio/DoubleBuffer;
    .locals 2
    .param p0    # Ljava/nio/DoubleBuffer;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/nio/DoubleBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {p0}, Ljava/nio/DoubleBuffer;->isDirect()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Ljava/nio/DoubleBuffer;->limit()I

    move-result v1

    invoke-static {v1}, Lcom/jme3/util/BufferUtils;->createDoubleBuffer(I)Ljava/nio/DoubleBuffer;

    move-result-object v0

    :goto_1
    invoke-virtual {v0, p0}, Ljava/nio/DoubleBuffer;->put(Ljava/nio/DoubleBuffer;)Ljava/nio/DoubleBuffer;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/nio/DoubleBuffer;->limit()I

    move-result v1

    invoke-static {v1}, Ljava/nio/DoubleBuffer;->allocate(I)Ljava/nio/DoubleBuffer;

    move-result-object v0

    goto :goto_1
.end method

.method public static clone(Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;
    .locals 2
    .param p0    # Ljava/nio/FloatBuffer;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {p0}, Ljava/nio/FloatBuffer;->isDirect()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Ljava/nio/FloatBuffer;->limit()I

    move-result v1

    invoke-static {v1}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v0

    :goto_1
    invoke-virtual {v0, p0}, Ljava/nio/FloatBuffer;->put(Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/nio/FloatBuffer;->limit()I

    move-result v1

    invoke-static {v1}, Ljava/nio/FloatBuffer;->allocate(I)Ljava/nio/FloatBuffer;

    move-result-object v0

    goto :goto_1
.end method

.method public static clone(Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;
    .locals 2
    .param p0    # Ljava/nio/IntBuffer;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/nio/IntBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {p0}, Ljava/nio/IntBuffer;->isDirect()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Ljava/nio/IntBuffer;->limit()I

    move-result v1

    invoke-static {v1}, Lcom/jme3/util/BufferUtils;->createIntBuffer(I)Ljava/nio/IntBuffer;

    move-result-object v0

    :goto_1
    invoke-virtual {v0, p0}, Ljava/nio/IntBuffer;->put(Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/nio/IntBuffer;->limit()I

    move-result v1

    invoke-static {v1}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    move-result-object v0

    goto :goto_1
.end method

.method public static clone(Ljava/nio/ShortBuffer;)Ljava/nio/ShortBuffer;
    .locals 2
    .param p0    # Ljava/nio/ShortBuffer;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/nio/ShortBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {p0}, Ljava/nio/ShortBuffer;->isDirect()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Ljava/nio/ShortBuffer;->limit()I

    move-result v1

    invoke-static {v1}, Lcom/jme3/util/BufferUtils;->createShortBuffer(I)Ljava/nio/ShortBuffer;

    move-result-object v0

    :goto_1
    invoke-virtual {v0, p0}, Ljava/nio/ShortBuffer;->put(Ljava/nio/ShortBuffer;)Ljava/nio/ShortBuffer;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/nio/ShortBuffer;->limit()I

    move-result v1

    invoke-static {v1}, Ljava/nio/ShortBuffer;->allocate(I)Ljava/nio/ShortBuffer;

    move-result-object v0

    goto :goto_1
.end method

.method public static createByteBuffer(I)Ljava/nio/ByteBuffer;
    .locals 3
    .param p0    # I

    invoke-static {p0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    invoke-static {v0}, Lcom/jme3/util/BufferUtils;->onBufferAllocated(Ljava/nio/Buffer;)V

    return-object v0
.end method

.method public static createDoubleBuffer(I)Ljava/nio/DoubleBuffer;
    .locals 3
    .param p0    # I

    mul-int/lit8 v1, p0, 0x8

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asDoubleBuffer()Ljava/nio/DoubleBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/DoubleBuffer;->clear()Ljava/nio/Buffer;

    invoke-static {v0}, Lcom/jme3/util/BufferUtils;->onBufferAllocated(Ljava/nio/Buffer;)V

    return-object v0
.end method

.method public static createFloatBuffer(I)Ljava/nio/FloatBuffer;
    .locals 3
    .param p0    # I

    mul-int/lit8 v1, p0, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    invoke-static {v0}, Lcom/jme3/util/BufferUtils;->onBufferAllocated(Ljava/nio/Buffer;)V

    return-object v0
.end method

.method public static varargs createFloatBuffer([F)Ljava/nio/FloatBuffer;
    .locals 2
    .param p0    # [F

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    array-length v1, p0

    invoke-static {v1}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    invoke-virtual {v0, p0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->flip()Ljava/nio/Buffer;

    goto :goto_0
.end method

.method public static varargs createFloatBuffer([Lcom/jme3/math/Quaternion;)Ljava/nio/FloatBuffer;
    .locals 5
    .param p0    # [Lcom/jme3/math/Quaternion;

    const/4 v4, 0x0

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    array-length v2, p0

    mul-int/lit8 v2, v2, 0x4

    invoke-static {v2}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v0

    const/4 v1, 0x0

    :goto_1
    array-length v2, p0

    if-ge v1, v2, :cond_2

    aget-object v2, p0, v1

    if-eqz v2, :cond_1

    aget-object v2, p0, v1

    invoke-virtual {v2}, Lcom/jme3/math/Quaternion;->getX()F

    move-result v2

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v2

    aget-object v3, p0, v1

    invoke-virtual {v3}, Lcom/jme3/math/Quaternion;->getY()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v2

    aget-object v3, p0, v1

    invoke-virtual {v3}, Lcom/jme3/math/Quaternion;->getZ()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v2

    aget-object v3, p0, v1

    invoke-virtual {v3}, Lcom/jme3/math/Quaternion;->getW()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    goto :goto_2

    :cond_2
    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->flip()Ljava/nio/Buffer;

    goto :goto_0
.end method

.method public static varargs createFloatBuffer([Lcom/jme3/math/Vector2f;)Ljava/nio/FloatBuffer;
    .locals 5
    .param p0    # [Lcom/jme3/math/Vector2f;

    const/4 v4, 0x0

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    array-length v2, p0

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v0

    const/4 v1, 0x0

    :goto_1
    array-length v2, p0

    if-ge v1, v2, :cond_2

    aget-object v2, p0, v1

    if-eqz v2, :cond_1

    aget-object v2, p0, v1

    iget v2, v2, Lcom/jme3/math/Vector2f;->x:F

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v2

    aget-object v3, p0, v1

    iget v3, v3, Lcom/jme3/math/Vector2f;->y:F

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    goto :goto_2

    :cond_2
    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->flip()Ljava/nio/Buffer;

    goto :goto_0
.end method

.method public static varargs createFloatBuffer([Lcom/jme3/math/Vector3f;)Ljava/nio/FloatBuffer;
    .locals 5
    .param p0    # [Lcom/jme3/math/Vector3f;

    const/4 v4, 0x0

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    array-length v2, p0

    mul-int/lit8 v2, v2, 0x3

    invoke-static {v2}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v0

    const/4 v1, 0x0

    :goto_1
    array-length v2, p0

    if-ge v1, v2, :cond_2

    aget-object v2, p0, v1

    if-eqz v2, :cond_1

    aget-object v2, p0, v1

    iget v2, v2, Lcom/jme3/math/Vector3f;->x:F

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v2

    aget-object v3, p0, v1

    iget v3, v3, Lcom/jme3/math/Vector3f;->y:F

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v2

    aget-object v3, p0, v1

    iget v3, v3, Lcom/jme3/math/Vector3f;->z:F

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    goto :goto_2

    :cond_2
    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->flip()Ljava/nio/Buffer;

    goto :goto_0
.end method

.method public static createIntBuffer(I)Ljava/nio/IntBuffer;
    .locals 3
    .param p0    # I

    mul-int/lit8 v1, p0, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->clear()Ljava/nio/Buffer;

    invoke-static {v0}, Lcom/jme3/util/BufferUtils;->onBufferAllocated(Ljava/nio/Buffer;)V

    return-object v0
.end method

.method public static createShortBuffer(I)Ljava/nio/ShortBuffer;
    .locals 3
    .param p0    # I

    mul-int/lit8 v1, p0, 0x2

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ShortBuffer;->clear()Ljava/nio/Buffer;

    invoke-static {v0}, Lcom/jme3/util/BufferUtils;->onBufferAllocated(Ljava/nio/Buffer;)V

    return-object v0
.end method

.method public static varargs createShortBuffer([S)Ljava/nio/ShortBuffer;
    .locals 2
    .param p0    # [S

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    array-length v1, p0

    invoke-static {v1}, Lcom/jme3/util/BufferUtils;->createShortBuffer(I)Ljava/nio/ShortBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ShortBuffer;->clear()Ljava/nio/Buffer;

    invoke-virtual {v0, p0}, Ljava/nio/ShortBuffer;->put([S)Ljava/nio/ShortBuffer;

    invoke-virtual {v0}, Ljava/nio/ShortBuffer;->flip()Ljava/nio/Buffer;

    goto :goto_0
.end method

.method public static createVector3Buffer(I)Ljava/nio/FloatBuffer;
    .locals 2
    .param p0    # I

    mul-int/lit8 v1, p0, 0x3

    invoke-static {v1}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static ensureLargeEnough(Ljava/nio/FloatBuffer;I)Ljava/nio/FloatBuffer;
    .locals 3
    .param p0    # Ljava/nio/FloatBuffer;
    .param p1    # I

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/nio/FloatBuffer;->remaining()I

    move-result v2

    if-ge v2, p1, :cond_2

    :cond_0
    if-eqz p0, :cond_3

    invoke-virtual {p0}, Ljava/nio/FloatBuffer;->position()I

    move-result v1

    :goto_0
    add-int v2, v1, p1

    invoke-static {v2}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v0

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {v0, p0}, Ljava/nio/FloatBuffer;->put(Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    :cond_1
    move-object p0, v0

    :cond_2
    return-object p0

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static onBufferAllocated(Ljava/nio/Buffer;)V
    .locals 0
    .param p0    # Ljava/nio/Buffer;

    return-void
.end method

.method public static populateFromBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V
    .locals 1
    .param p0    # Lcom/jme3/math/Vector3f;
    .param p1    # Ljava/nio/FloatBuffer;
    .param p2    # I

    mul-int/lit8 v0, p2, 0x3

    invoke-virtual {p1, v0}, Ljava/nio/FloatBuffer;->get(I)F

    move-result v0

    iput v0, p0, Lcom/jme3/math/Vector3f;->x:F

    mul-int/lit8 v0, p2, 0x3

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/nio/FloatBuffer;->get(I)F

    move-result v0

    iput v0, p0, Lcom/jme3/math/Vector3f;->y:F

    mul-int/lit8 v0, p2, 0x3

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {p1, v0}, Ljava/nio/FloatBuffer;->get(I)F

    move-result v0

    iput v0, p0, Lcom/jme3/math/Vector3f;->z:F

    return-void
.end method

.method public static setInBuffer(Lcom/jme3/math/Quaternion;Ljava/nio/FloatBuffer;I)V
    .locals 1
    .param p0    # Lcom/jme3/math/Quaternion;
    .param p1    # Ljava/nio/FloatBuffer;
    .param p2    # I

    mul-int/lit8 v0, p2, 0x4

    invoke-virtual {p1, v0}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    invoke-virtual {p0}, Lcom/jme3/math/Quaternion;->getX()F

    move-result v0

    invoke-virtual {p1, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    invoke-virtual {p0}, Lcom/jme3/math/Quaternion;->getY()F

    move-result v0

    invoke-virtual {p1, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    invoke-virtual {p0}, Lcom/jme3/math/Quaternion;->getZ()F

    move-result v0

    invoke-virtual {p1, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    invoke-virtual {p0}, Lcom/jme3/math/Quaternion;->getW()F

    move-result v0

    invoke-virtual {p1, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    return-void
.end method

.method public static setInBuffer(Lcom/jme3/math/Vector2f;Ljava/nio/FloatBuffer;I)V
    .locals 2
    .param p0    # Lcom/jme3/math/Vector2f;
    .param p1    # Ljava/nio/FloatBuffer;
    .param p2    # I

    mul-int/lit8 v0, p2, 0x2

    iget v1, p0, Lcom/jme3/math/Vector2f;->x:F

    invoke-virtual {p1, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    mul-int/lit8 v0, p2, 0x2

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/jme3/math/Vector2f;->y:F

    invoke-virtual {p1, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    return-void
.end method

.method public static setInBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V
    .locals 2
    .param p0    # Lcom/jme3/math/Vector3f;
    .param p1    # Ljava/nio/FloatBuffer;
    .param p2    # I

    const/4 v1, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p0, :cond_1

    mul-int/lit8 v0, p2, 0x3

    invoke-virtual {p1, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    mul-int/lit8 v0, p2, 0x3

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    mul-int/lit8 v0, p2, 0x3

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {p1, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    goto :goto_0

    :cond_1
    mul-int/lit8 v0, p2, 0x3

    iget v1, p0, Lcom/jme3/math/Vector3f;->x:F

    invoke-virtual {p1, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    mul-int/lit8 v0, p2, 0x3

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/jme3/math/Vector3f;->y:F

    invoke-virtual {p1, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    mul-int/lit8 v0, p2, 0x3

    add-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/jme3/math/Vector3f;->z:F

    invoke-virtual {p1, v0, v1}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    goto :goto_0
.end method
