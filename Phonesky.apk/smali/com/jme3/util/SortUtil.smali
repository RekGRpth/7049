.class public Lcom/jme3/util/SortUtil;
.super Ljava/lang/Object;
.source "SortUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static merge([Ljava/lang/Object;[Ljava/lang/Object;IIILjava/util/Comparator;)V
    .locals 9
    .param p0    # [Ljava/lang/Object;
    .param p1    # [Ljava/lang/Object;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/util/Comparator;

    add-int/lit8 v1, p3, -0x1

    move v5, p2

    sub-int v7, p4, p2

    add-int/lit8 v4, v7, 0x1

    move v6, v5

    move v3, p3

    move v2, p2

    :goto_0
    if-gt v2, v1, :cond_1

    if-gt v3, p4, :cond_1

    aget-object v7, p0, v2

    aget-object v8, p0, v3

    invoke-interface {p5, v7, v8}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v7

    if-gtz v7, :cond_0

    add-int/lit8 v5, v6, 0x1

    add-int/lit8 p2, v2, 0x1

    aget-object v7, p0, v2

    aput-object v7, p1, v6

    move v6, v5

    move v2, p2

    goto :goto_0

    :cond_0
    add-int/lit8 v5, v6, 0x1

    add-int/lit8 p3, v3, 0x1

    aget-object v7, p0, v3

    aput-object v7, p1, v6

    move v6, v5

    move v3, p3

    goto :goto_0

    :cond_1
    :goto_1
    if-gt v2, v1, :cond_2

    add-int/lit8 v5, v6, 0x1

    add-int/lit8 p2, v2, 0x1

    aget-object v7, p0, v2

    aput-object v7, p1, v6

    move v6, v5

    move v2, p2

    goto :goto_1

    :cond_2
    :goto_2
    if-gt v3, p4, :cond_3

    add-int/lit8 v5, v6, 0x1

    add-int/lit8 p3, v3, 0x1

    aget-object v7, p0, v3

    aput-object v7, p1, v6

    move v6, v5

    move v3, p3

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v4, :cond_4

    aget-object v7, p1, p4

    aput-object v7, p0, p4

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 p4, p4, -0x1

    goto :goto_3

    :cond_4
    return-void
.end method

.method public static msort([Ljava/lang/Object;[Ljava/lang/Object;IILjava/util/Comparator;)V
    .locals 7
    .param p0    # [Ljava/lang/Object;
    .param p1    # [Ljava/lang/Object;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/util/Comparator;

    if-ge p2, p3, :cond_0

    add-int v0, p2, p3

    div-int/lit8 v6, v0, 0x2

    invoke-static {p0, p1, p2, v6, p4}, Lcom/jme3/util/SortUtil;->msort([Ljava/lang/Object;[Ljava/lang/Object;IILjava/util/Comparator;)V

    add-int/lit8 v0, v6, 0x1

    invoke-static {p0, p1, v0, p3, p4}, Lcom/jme3/util/SortUtil;->msort([Ljava/lang/Object;[Ljava/lang/Object;IILjava/util/Comparator;)V

    add-int/lit8 v3, v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/jme3/util/SortUtil;->merge([Ljava/lang/Object;[Ljava/lang/Object;IIILjava/util/Comparator;)V

    :cond_0
    return-void
.end method
