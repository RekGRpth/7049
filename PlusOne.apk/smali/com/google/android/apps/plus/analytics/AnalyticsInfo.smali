.class public final Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
.super Ljava/lang/Object;
.source "AnalyticsInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final mCustomValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

.field private final mStartTime:J

.field private final mStartView:Lcom/google/android/apps/plus/analytics/OzViews;


# direct methods
.method constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v5

    move-object v0, p0

    move-object v2, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JLjava/util/Map;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/analytics/OzViews;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mStartView:Lcom/google/android/apps/plus/analytics/OzViews;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mStartTime:J

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mCustomValues:Ljava/util/HashMap;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;J)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/analytics/OzViews;
    .param p2    # Lcom/google/android/apps/plus/analytics/OzViews;
    .param p3    # J

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JLjava/util/Map;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JLjava/util/Map;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/analytics/OzViews;
    .param p2    # Lcom/google/android/apps/plus/analytics/OzViews;
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/analytics/OzViews;",
            "Lcom/google/android/apps/plus/analytics/OzViews;",
            "J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mStartView:Lcom/google/android/apps/plus/analytics/OzViews;

    iput-object p2, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

    iput-wide p3, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mStartTime:J

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p5}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mCustomValues:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public final getEndView()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final getStartTimeMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mStartTime:J

    return-wide v0
.end method

.method public final getStartView()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mStartView:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method
