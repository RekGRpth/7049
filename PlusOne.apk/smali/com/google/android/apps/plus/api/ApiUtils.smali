.class public final Lcom/google/android/apps/plus/api/ApiUtils;
.super Ljava/lang/Object;
.source "ApiUtils.java"


# static fields
.field private static final CIRCLE_ID_PREFIXES:[Ljava/lang/String;

.field public static final EMAIL_MENTION_PREFIX_LENGTH:I

.field public static final ID_MENTION_PREFIX_LENGTH:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "f."

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "g."

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/api/ApiUtils;->CIRCLE_ID_PREFIXES:[Ljava/lang/String;

    sput v3, Lcom/google/android/apps/plus/api/ApiUtils;->ID_MENTION_PREFIX_LENGTH:I

    sput v3, Lcom/google/android/apps/plus/api/ApiUtils;->EMAIL_MENTION_PREFIX_LENGTH:I

    return-void
.end method

.method public static buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;
    .locals 14
    .param p0    # Landroid/text/Spannable;

    const/4 v11, 0x0

    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v12

    const-class v13, Landroid/text/style/URLSpan;

    invoke-interface {p0, v11, v12, v13}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Landroid/text/style/URLSpan;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    array-length v11, v10

    if-ge v1, v11, :cond_3

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v3, 0x0

    :goto_1
    add-int/lit8 v11, v7, 0x1

    if-ge v3, v11, :cond_0

    aget-object v11, v10, v1

    invoke-static {v11}, Lcom/google/android/apps/plus/views/MentionSpan;->isMention(Landroid/text/style/URLSpan;)Z

    move-result v11

    if-eqz v11, :cond_2

    if-gt v7, v3, :cond_1

    aget-object v11, v10, v1

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-interface {p0, v11}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v11

    aget-object v12, v10, v1

    invoke-interface {p0, v12}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v12

    if-le v11, v12, :cond_2

    aget-object v11, v10, v1

    invoke-virtual {v8, v3, v11}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_2

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v9}, Ljava/util/ArrayList;-><init>(I)V

    new-array v6, v9, [I

    new-array v4, v9, [I

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v9, :cond_6

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/text/style/URLSpan;

    invoke-virtual {v11}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const-string v11, "g:"

    invoke-virtual {v2, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "+"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v12, Lcom/google/android/apps/plus/api/ApiUtils;->ID_MENTION_PREFIX_LENGTH:I

    invoke-virtual {v2, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_4
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-interface {p0, v11}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v11

    aput v11, v6, v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-interface {p0, v11}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v11

    aput v11, v4, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    const-string v11, "e:"

    invoke-virtual {v2, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "+"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v12, Lcom/google/android/apps/plus/api/ApiUtils;->EMAIL_MENTION_PREFIX_LENGTH:I

    invoke-virtual {v2, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_5
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "+"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v0, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v1, v9, -0x1

    :goto_5
    if-ltz v1, :cond_7

    aget v12, v6, v1

    aget v13, v4, v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v0, v12, v13, v11}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, -0x1

    goto :goto_5

    :cond_7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    return-object v11
.end method

.method public static prependProtocol(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "http:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static removeCircleIdNamespaces(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;
    .locals 15
    .param p0    # Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v12

    invoke-direct {v4, v12}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    array-length v8, v0

    const/4 v5, 0x0

    move v6, v5

    :goto_0
    if-ge v6, v8, :cond_2

    aget-object v2, v0, v6

    sget-object v1, Lcom/google/android/apps/plus/api/ApiUtils;->CIRCLE_ID_PREFIXES:[Ljava/lang/String;

    array-length v9, v1

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v9, :cond_0

    aget-object v10, v1, v5

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    new-instance v3, Lcom/google/android/apps/plus/content/CircleData;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v12

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getSize()I

    move-result v14

    invoke-direct {v3, v7, v12, v13, v14}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    move-object v2, v3

    :cond_0
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_0

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v12

    invoke-static {v12}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v11

    new-instance v12, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v12, v11, v4}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v12
.end method
