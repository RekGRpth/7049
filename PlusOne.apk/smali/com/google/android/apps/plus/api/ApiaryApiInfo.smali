.class public final Lcom/google/android/apps/plus/api/ApiaryApiInfo;
.super Ljava/lang/Object;
.source "ApiaryApiInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x2ba6046fa3ebca2aL


# instance fields
.field private final mApiKey:Ljava/lang/String;

.field private final mCertificate:Ljava/lang/String;

.field private final mClientId:Ljava/lang/String;

.field private final mPackageName:Ljava/lang/String;

.field private final mSdkVersion:Ljava/lang/String;

.field private final mSourceInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->mApiKey:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->mClientId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->mPackageName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->mCertificate:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->mSourceInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->mSdkVersion:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getApiKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->mApiKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getCertificate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->mCertificate:Ljava/lang/String;

    return-object v0
.end method

.method public final getClientId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->mClientId:Ljava/lang/String;

    return-object v0
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public final getSdkVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->mSdkVersion:Ljava/lang/String;

    return-object v0
.end method

.method public final getSourceInfo()Lcom/google/android/apps/plus/api/ApiaryApiInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->mSourceInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    return-object v0
.end method
