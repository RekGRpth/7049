.class public Lcom/google/android/apps/plus/api/ApiaryArticleActivity;
.super Lcom/google/android/apps/plus/api/ApiaryActivity;
.source "ApiaryArticleActivity.java"


# instance fields
.field private mContent:Ljava/lang/String;

.field private mDisplayName:Ljava/lang/String;

.field private mFavIconUrl:Ljava/lang/String;

.field private mImageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/api/ApiaryActivity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryArticleActivity;->mImageList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected final update(Lcom/google/api/services/plusi/model/MediaLayout;)V
    .locals 5
    .param p1    # Lcom/google/api/services/plusi/model/MediaLayout;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/api/ApiaryActivity;->update(Lcom/google/api/services/plusi/model/MediaLayout;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryArticleActivity;->mDisplayName:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryArticleActivity;->mContent:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryArticleActivity;->mImageList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/MediaLayout;->media:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/MediaItem;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ApiaryArticleActivity;->mImageList:Ljava/util/List;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p1, Lcom/google/api/services/plusi/model/MediaLayout;->title:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryArticleActivity;->mDisplayName:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/MediaLayout;->faviconUrl:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryArticleActivity;->mFavIconUrl:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryArticleActivity;->mFavIconUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "https:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ApiaryArticleActivity;->mFavIconUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryArticleActivity;->mFavIconUrl:Ljava/lang/String;

    :cond_1
    iget-object v1, p1, Lcom/google/api/services/plusi/model/MediaLayout;->description:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryArticleActivity;->mContent:Ljava/lang/String;

    return-void
.end method
