.class public final Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory;
.super Ljava/lang/Object;
.source "ApiaryAuthDataFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory$ApiaryAuthDataImpl;,
        Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory$ApiaryAuthData;
    }
.end annotation


# static fields
.field static final sAuthDatas:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory$ApiaryAuthData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory;->sAuthDatas:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAuthData(Ljava/lang/String;)Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory$ApiaryAuthData;
    .locals 3
    .param p0    # Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory;->sAuthDatas:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory;->sAuthDatas:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory$ApiaryAuthData;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory$ApiaryAuthDataImpl;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory$ApiaryAuthDataImpl;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory;->sAuthDatas:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method
