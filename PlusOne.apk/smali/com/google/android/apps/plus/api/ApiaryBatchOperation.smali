.class public Lcom/google/android/apps/plus/api/ApiaryBatchOperation;
.super Lcom/google/android/apps/plus/network/HttpOperation;
.source "ApiaryBatchOperation.java"


# static fields
.field private static final CONTENT_TYPE_APP_HTTP:[B

.field private static final CONTENT_TYPE_JSON:[B

.field private static final MULTIPART_BOUNDARY_END:[B

.field private static final MULTIPART_BOUNDARY_START:[B

.field private static final PATTERN_CONTENT_LENGTH:Ljava/util/regex/Pattern;

.field private static final PATTERN_ID:Ljava/util/regex/Pattern;

.field private static final PATTERN_RESPONSE_CODE:Ljava/util/regex/Pattern;


# instance fields
.field private mCurrentOperation:Lcom/google/android/apps/plus/network/HttpOperation;

.field private mOperations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/network/HttpOperation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "--MultiPartRequest\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->MULTIPART_BOUNDARY_START:[B

    const-string v0, "--MultiPartRequest--\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->MULTIPART_BOUNDARY_END:[B

    const-string v0, "Content-Type: application/http\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->CONTENT_TYPE_APP_HTTP:[B

    const-string v0, "Content-Type: application/json\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->CONTENT_TYPE_JSON:[B

    const-string v0, "Content-ID: <response-item:(.+)>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->PATTERN_ID:Ljava/util/regex/Pattern;

    const-string v0, "HTTP/1\\.1 (\\d+) (.*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->PATTERN_RESPONSE_CODE:Ljava/util/regex/Pattern;

    const-string v0, "Content-Length: (\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->PATTERN_CONTENT_LENGTH:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v6, "POST"

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_DOGFOOD_FEATURES:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_FRONTEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_FRONTEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    :goto_0
    const-string v1, "batch"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "batch"

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/api/PlusiOperation;->appendTracingToken(Landroid/net/Uri$Builder;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;

    const-string v3, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite https://www.googleapis.com/auth/plus.native"

    const/4 v4, 0x0

    const-string v5, "multipart/mixed; boundary=MultiPartRequest"

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, v6

    move-object v4, v8

    move-object v5, v0

    move-object v6, p2

    move-object v8, p3

    move-object v9, p4

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/plus/network/HttpOperation;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lcom/google/android/apps/plus/content/EsAccount;Ljava/io/OutputStream;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mOperations:Ljava/util/ArrayList;

    return-void

    :cond_0
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "https"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->PLUS_FRONTEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0
.end method

.method private readPartBody(Ljava/io/DataInputStream;)V
    .locals 14
    .param p1    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/16 v5, 0xc8

    const/4 v12, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLine()Ljava/lang/String;

    move-result-object v10

    const-string v0, ""

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->PATTERN_CONTENT_LENGTH:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v10}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v9, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->PATTERN_RESPONSE_CODE:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v10}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v7, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/4 v0, 0x2

    invoke-virtual {v7, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v12

    goto :goto_0

    :cond_2
    new-array v6, v3, [B

    const/4 v11, 0x0

    :goto_1
    array-length v0, v6

    if-ge v11, v0, :cond_3

    array-length v0, v6

    sub-int/2addr v0, v11

    invoke-virtual {p1, v6, v11, v0}, Ljava/io/DataInputStream;->read([BII)I

    move-result v0

    add-int/2addr v11, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLine()Ljava/lang/String;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const/4 v8, 0x0

    const/16 v0, 0xc8

    if-lt v5, v0, :cond_5

    const/16 v0, 0x12c

    if-ge v5, v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mCurrentOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/network/HttpOperation;->onHttpHandleContentFromStream$6508b088(Ljava/io/InputStream;)V

    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mCurrentOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    invoke-virtual {v0, v5, v12, v8}, Lcom/google/android/apps/plus/network/HttpOperation;->onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mCurrentOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    invoke-virtual {v0, v5, v12, v8}, Lcom/google/android/apps/plus/network/HttpOperation;->setErrorInfo(ILjava/lang/String;Ljava/lang/Exception;)V

    return-void

    :cond_5
    const-string v0, "HttpTransaction"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Error: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ["

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mCurrentOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const/16 v0, 0x191

    if-ne v5, v0, :cond_7

    new-instance v0, Lorg/apache/http/client/HttpResponseException;

    invoke-direct {v0, v5, v12}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    throw v0

    :cond_7
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mCurrentOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/network/HttpOperation;->onHttpReadErrorFromStream(Ljava/io/InputStream;Ljava/lang/String;I[Lorg/apache/http/Header;I)V
    :try_end_0
    .catch Lcom/google/android/apps/plus/api/OzServerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    if-nez v8, :cond_4

    new-instance v8, Lorg/apache/http/client/HttpResponseException;

    invoke-direct {v8, v5, v12}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    goto :goto_2

    :catch_0
    move-exception v13

    move-object v8, v13

    goto :goto_3
.end method

.method private readPartHeader(Ljava/io/DataInputStream;)I
    .locals 6
    .param p1    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readLine()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v3, -0x1

    :goto_1
    return v3

    :cond_1
    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_1

    :catch_0
    move-exception v3

    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid response format. Section ID = \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    sget-object v3, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->PATTERN_ID:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public final add(Lcom/google/android/apps/plus/network/HttpOperation;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/network/HttpOperation;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final createPostData()Lorg/apache/http/HttpEntity;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v9, 0xa

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v7, 0x400

    new-array v0, v7, [B

    const/4 v2, 0x0

    :goto_0
    iget-object v7, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_2

    iget-object v7, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/network/HttpOperation;

    sget-object v7, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->MULTIPART_BOUNDARY_START:[B

    invoke-virtual {v6, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V

    sget-object v7, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->CONTENT_TYPE_APP_HTTP:[B

    invoke-virtual {v6, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Content-ID: <item:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ">\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V

    invoke-virtual {v6, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/network/HttpOperation;->getMethod()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V

    const/16 v7, 0x20

    invoke-virtual {v6, v7}, Ljava/io/ByteArrayOutputStream;->write(I)V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/network/HttpOperation;->getUrl()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V

    invoke-virtual {v6, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/network/HttpOperation;->createPostData()Lorg/apache/http/HttpEntity;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :goto_1
    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    if-lez v5, :cond_0

    const/4 v7, 0x0

    invoke-virtual {v3, v0, v7, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_1

    :cond_0
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v5

    if-lez v5, :cond_1

    sget-object v7, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->CONTENT_TYPE_JSON:[B

    invoke-virtual {v6, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Content-Length: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V

    invoke-virtual {v6, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V

    invoke-virtual {v6, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    :cond_1
    invoke-virtual {v6, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_2
    sget-object v7, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->MULTIPART_BOUNDARY_END:[B

    invoke-virtual {v6, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V

    new-instance v7, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    invoke-direct {v7, v8}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    return-object v7
.end method

.method public final hasError()Z
    .locals 5

    const/4 v3, 0x1

    invoke-super {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/network/HttpOperation;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v4

    if-nez v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final logError(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    invoke-super {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/network/HttpOperation;->logError(Ljava/lang/String;)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/network/HttpOperation;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getException()Ljava/lang/Exception;

    move-result-object v3

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed due to exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getException()Ljava/lang/Exception;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getException()Ljava/lang/Exception;

    move-result-object v4

    invoke-static {p1, v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x4

    invoke-static {p1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed due to error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    return-void
.end method

.method public final onHttpHandleContentFromStream$6508b088(Ljava/io/InputStream;)V
    .locals 8
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v5, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v3, v5, [Z

    new-instance v4, Ljava/io/DataInputStream;

    invoke-direct {v4, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->readPartHeader(Ljava/io/DataInputStream;)I

    move-result v2

    const/4 v5, -0x1

    if-ne v2, v5, :cond_3

    const/4 v1, 0x0

    :goto_1
    array-length v5, v3

    if-ge v1, v5, :cond_2

    aget-boolean v5, v3, v1

    if-nez v5, :cond_1

    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Incomplete response. Response missing for: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_5

    new-instance v5, Lcom/google/android/apps/plus/api/SubOperationFailedException;

    invoke-direct {v5, v0}, Lcom/google/android/apps/plus/api/SubOperationFailedException;-><init>(Ljava/util/List;)V

    throw v5

    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/network/HttpOperation;

    iput-object v5, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mCurrentOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->readPartBody(Ljava/io/DataInputStream;)V

    const/4 v5, 0x1

    aput-boolean v5, v3, v2

    iget-object v5, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mCurrentOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v5

    if-eqz v5, :cond_0

    if-nez v0, :cond_4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_4
    iget-object v5, p0, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->mCurrentOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    return-void
.end method
