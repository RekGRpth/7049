.class public Lcom/google/android/apps/plus/api/ApiaryErrorResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ApiaryErrorResponse.java"


# static fields
.field public static final JSON:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<",
            "Lcom/google/android/apps/plus/api/ApiaryErrorResponse;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public error:Lcom/google/android/apps/plus/api/ApiaryError;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-class v0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/plus/api/ApiaryError;->JSON:Lcom/google/android/apps/plus/json/EsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "error"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/json/EsJson;->buildJson(Ljava/lang/Class;[Ljava/lang/Object;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->JSON:Lcom/google/android/apps/plus/json/EsJson;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method


# virtual methods
.method public getDebugInfo()Ljava/lang/String;
    .locals 5

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->debugInfo:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->debugInfo:Ljava/lang/String;

    :goto_0
    return-object v4

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->errors:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v3, v4, Lcom/google/android/apps/plus/api/ApiaryError;->errors:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_2

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v2, Lcom/google/android/apps/plus/api/ApiaryError;->debugInfo:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, v2, Lcom/google/android/apps/plus/api/ApiaryError;->debugInfo:Ljava/lang/String;

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 5

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->domain:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->domain:Ljava/lang/String;

    :goto_0
    return-object v4

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->errors:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v3, v4, Lcom/google/android/apps/plus/api/ApiaryError;->errors:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_2

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v2, Lcom/google/android/apps/plus/api/ApiaryError;->domain:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, v2, Lcom/google/android/apps/plus/api/ApiaryError;->domain:Ljava/lang/String;

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 5

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->message:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->message:Ljava/lang/String;

    :goto_0
    return-object v4

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->errors:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v3, v4, Lcom/google/android/apps/plus/api/ApiaryError;->errors:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_2

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v2, Lcom/google/android/apps/plus/api/ApiaryError;->message:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, v2, Lcom/google/android/apps/plus/api/ApiaryError;->message:Ljava/lang/String;

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public getErrorType()Ljava/lang/String;
    .locals 5

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->reason:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->reason:Ljava/lang/String;

    :goto_0
    return-object v4

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->errors:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v3, v4, Lcom/google/android/apps/plus/api/ApiaryError;->errors:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_2

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v2, Lcom/google/android/apps/plus/api/ApiaryError;->reason:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, v2, Lcom/google/android/apps/plus/api/ApiaryError;->reason:Ljava/lang/String;

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->JSON:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/json/EsJson;->toPrettyString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
