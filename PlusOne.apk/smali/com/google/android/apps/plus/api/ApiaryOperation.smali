.class public abstract Lcom/google/android/apps/plus/api/ApiaryOperation;
.super Lcom/google/android/apps/plus/network/HttpOperation;
.source "ApiaryOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Request:",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        "Response:",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        ">",
        "Lcom/google/android/apps/plus/network/HttpOperation;"
    }
.end annotation


# instance fields
.field protected final mRequestJson:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;"
        }
    .end annotation
.end field

.field protected final mResponseJson:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TResponse;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Ljava/lang/String;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p6    # Landroid/content/Intent;
    .param p7    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p8    # Lcom/google/android/apps/plus/network/HttpRequestConfiguration;
    .param p9    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TResponse;>;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            "Lcom/google/android/apps/plus/network/HttpRequestConfiguration;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p9

    move-object v3, p3

    move-object/from16 v4, p8

    move-object v5, p2

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/network/HttpOperation;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lcom/google/android/apps/plus/content/EsAccount;Ljava/io/OutputStream;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p4, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mRequestJson:Lcom/google/android/apps/plus/json/EsJson;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mResponseJson:Lcom/google/android/apps/plus/json/EsJson;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p8    # Landroid/content/Intent;
    .param p9    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TResponse;>;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            ")V"
        }
    .end annotation

    new-instance v12, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;

    move-object/from16 v0, p5

    move-object/from16 v1, p4

    invoke-direct {v12, p1, p2, v0, v1}, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v2 .. v12}, Lcom/google/android/apps/plus/api/ApiaryOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;)V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p8    # Landroid/content/Intent;
    .param p9    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p10    # Lcom/google/android/apps/plus/network/HttpRequestConfiguration;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TResponse;>;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            "Lcom/google/android/apps/plus/network/HttpRequestConfiguration;",
            ")V"
        }
    .end annotation

    const-string v9, "POST"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/api/ApiaryOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected createHttpEntity(Lcom/google/android/apps/plus/json/GenericJson;)Lcom/google/android/apps/plus/network/JsonHttpEntity;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRequest;)",
            "Lcom/google/android/apps/plus/network/JsonHttpEntity",
            "<TRequest;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mRequestJson:Lcom/google/android/apps/plus/json/EsJson;

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/plus/network/JsonHttpEntity;-><init>(Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/GenericJson;)V

    return-object v0
.end method

.method public createPostData()Lorg/apache/http/HttpEntity;
    .locals 5

    const/4 v4, 0x3

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mRequestJson:Lcom/google/android/apps/plus/json/EsJson;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/api/ApiaryOperation;->createHttpEntity(Lcom/google/android/apps/plus/json/GenericJson;)Lcom/google/android/apps/plus/network/JsonHttpEntity;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mRequestJson:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/json/EsJson;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/json/GenericJson;

    new-instance v1, Lcom/google/api/services/plusi/model/ApiaryFields;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/ApiaryFields;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/plus/network/ClientVersion;->from(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/ApiaryFields;->appVersion:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/ApiaryFields;->effectiveUser:Ljava/lang/String;

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mRequestJson:Lcom/google/android/apps/plus/json/EsJson;

    const-string v3, "commonFields"

    invoke-virtual {v2, v0, v3, v1}, Lcom/google/android/apps/plus/json/EsJson;->setField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/api/ApiaryOperation;->populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V

    const-string v1, "HttpTransaction"

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/ApiaryOperation;->getLogTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Apiary request: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mRequestJson:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/json/EsJson;->toPrettyString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->doWriteToLog(ILjava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/api/ApiaryOperation;->createHttpEntity(Lcom/google/android/apps/plus/json/GenericJson;)Lcom/google/android/apps/plus/network/JsonHttpEntity;

    move-result-object v1

    goto :goto_0
.end method

.method protected getLogTag()Ljava/lang/String;
    .locals 1

    const-string v0, "HttpTransaction"

    return-object v0
.end method

.method protected abstract handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResponse;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected final isAuthenticationError(Ljava/lang/Exception;)Z
    .locals 1
    .param p1    # Ljava/lang/Exception;

    instance-of v0, p1, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/network/HttpOperation;->isAuthenticationError(Ljava/lang/Exception;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected final isImmediatelyRetryableError(Ljava/lang/Exception;)Z
    .locals 1
    .param p1    # Ljava/lang/Exception;

    instance-of v0, p1, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/network/HttpOperation;->isImmediatelyRetryableError(Ljava/lang/Exception;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x6 -> :sswitch_0
    .end sparse-switch
.end method

.method public final onHttpHandleContentFromStream$6508b088(Ljava/io/InputStream;)V
    .locals 5
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/ApiaryOperation;->onStartResultProcessing()V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mResponseJson:Lcom/google/android/apps/plus/json/EsJson;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mResponseJson:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/json/EsJson;->fromInputStream(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/json/GenericJson;

    move-object v0, v1

    :goto_0
    const-string v1, "HttpTransaction"

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/ApiaryOperation;->getLogTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mResponseJson:Lcom/google/android/apps/plus/json/EsJson;

    if-eqz v1, :cond_3

    const-string v2, "HttpTransaction"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "Apiary response: "

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mResponseJson:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/json/EsJson;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/json/GenericJson;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mResponseJson:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/json/EsJson;->toPrettyString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v2, v1}, Lcom/google/android/apps/plus/util/EsLog;->doWriteToLog(ILjava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/api/ApiaryOperation;->handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const-string v1, "HttpTransaction"

    const-string v2, "Apiary response ignored"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onHttpReadErrorFromStream(Ljava/io/InputStream;Ljava/lang/String;I[Lorg/apache/http/Header;I)V
    .locals 10
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # [Lorg/apache/http/Header;
    .param p5    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v9, 0xa

    const/4 v8, 0x4

    const/4 v0, 0x0

    const/4 v3, 0x4

    const-string v6, "HttpTransaction"

    invoke-static {v6, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/ApiaryOperation;->getLogTag()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Apiary error response: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {p1, p3, v0}, Lcom/google/android/apps/plus/api/ApiaryOperation;->captureResponse(Ljava/io/InputStream;ILjava/lang/StringBuilder;)Ljava/io/InputStream;

    move-result-object p1

    :cond_1
    const/16 v6, 0x191

    if-ne p5, v6, :cond_4

    const-string v6, "HttpTransaction"

    invoke-static {v6, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/ApiaryOperation;->getLogTag()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_2
    const-string v6, "HttpTransaction"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-void

    :cond_4
    const/4 v4, 0x0

    :try_start_0
    sget-object v6, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->JSON:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v6, p1}, Lcom/google/android/apps/plus/json/EsJson;->fromInputStream(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->getErrorType()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    new-instance v5, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-direct {v5, v2}, Lcom/google/android/apps/plus/api/OzServerException;-><init>(Lcom/google/android/apps/plus/api/ApiaryErrorResponse;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :goto_0
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v6, "Apiary error response: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v6, "   domain: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->getDomain()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v6, "   reason: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->getErrorType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v6, "   message: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->getErrorMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->getDebugInfo()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    const-string v6, "\\n"

    const-string v7, "\n"

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v6, "\\t"

    const-string v7, "\t"

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v6, "   debugInfo: \n"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_5
    const/4 v3, 0x6

    move-object v4, v5

    :cond_6
    :goto_1
    if-eqz v0, :cond_8

    const-string v6, "HttpTransaction"

    invoke-static {v6, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-nez v6, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/ApiaryOperation;->getLogTag()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_8

    :cond_7
    const-string v6, "HttpTransaction"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    :cond_8
    if-eqz v4, :cond_3

    throw v4

    :pswitch_0
    :try_start_2
    iget-object v6, p0, Lcom/google/android/apps/plus/api/ApiaryOperation;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/apps/plus/service/AndroidNotification;->showUpgradeRequiredNotification(Landroid/content/Context;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v6

    move-object v4, v5

    goto :goto_1

    :catch_1
    move-exception v6

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method protected abstract populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRequest;)V"
        }
    .end annotation
.end method
