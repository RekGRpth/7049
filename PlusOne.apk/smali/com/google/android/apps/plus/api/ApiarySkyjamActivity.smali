.class public Lcom/google/android/apps/plus/api/ApiarySkyjamActivity;
.super Lcom/google/android/apps/plus/api/ApiaryActivity;
.source "ApiarySkyjamActivity.java"


# instance fields
.field private mAlbumName:Ljava/lang/String;

.field private mArtistName:Ljava/lang/String;

.field private mImage:Ljava/lang/String;

.field private mTrackName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/api/ApiaryActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final update(Lcom/google/api/services/plusi/model/MediaLayout;)V
    .locals 4
    .param p1    # Lcom/google/api/services/plusi/model/MediaLayout;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/api/ApiaryActivity;->update(Lcom/google/api/services/plusi/model/MediaLayout;)V

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MediaLayout;->media:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/MediaItem;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/MediaItem;->albumArtistHtml:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/api/ApiarySkyjamActivity;->mImage:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/MediaItem;->caption:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/api/ApiarySkyjamActivity;->mTrackName:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/MediaItem;->albumHtml:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/api/ApiarySkyjamActivity;->mAlbumName:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/MediaItem;->albumArtistHtml:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/api/ApiarySkyjamActivity;->mArtistName:Ljava/lang/String;

    goto :goto_0

    :cond_1
    return-void
.end method
