.class public final Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "CheckStreamChangeOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetActivitiesRequest;",
        "Lcom/google/api/services/plusi/model/GetActivitiesResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCircleId:Ljava/lang/String;

.field private final mFromWidget:Z

.field private final mGaiaId:Ljava/lang/String;

.field private final mSquareStreamId:Ljava/lang/String;

.field private mStreamHasChanged:Z

.field private final mView:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Z
    .param p8    # Landroid/content/Intent;
    .param p9    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "getactivities"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetActivitiesRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetActivitiesRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetActivitiesResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetActivitiesResponseJson;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput p3, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mView:I

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "f."

    invoke-virtual {p4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p4

    :cond_0
    iput-object p4, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mCircleId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mGaiaId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mSquareStreamId:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mFromWidget:Z

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;

    iget v0, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mView:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mGaiaId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mSquareStreamId:Ljava/lang/String;

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->buildSquareStreamKey(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Stream;->update:Ljava/util/List;

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->hasStreamChanged(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mStreamHasChanged:Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mGaiaId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mCircleId:Ljava/lang/String;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mView:I

    invoke-static {v0, v1, v2, v4, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final hasStreamChanged()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mStreamHasChanged:Z

    return v0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const/4 v1, 0x1

    const/4 v2, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;

    new-instance v0, Lcom/google/api/services/plusi/model/StreamParams;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/StreamParams;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget v0, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mView:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v0, "ALL"

    :goto_0
    iput-object v0, v3, Lcom/google/api/services/plusi/model/StreamParams;->viewType:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mView:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mView:I

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mCircleId:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mGaiaId:Ljava/lang/String;

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    const-string v3, "LATEST"

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->sort:Ljava/lang/String;

    :goto_1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mCircleId:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->focusGroupId:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mGaiaId:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->productionStreamOid:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mSquareStreamId:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->squareStreamId:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->continuesToken:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->maxNumUpdates:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    const-string v3, "MOBILE"

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->collapserType:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->maxComments:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->maxNumImages:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->maxResharers:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    new-instance v3, Lcom/google/api/services/plusi/model/FieldRequestOptions;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/FieldRequestOptions;-><init>()V

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v3, v0, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeLegacyMediaData:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v3, v0, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeEmbedsData:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    new-instance v3, Lcom/google/api/services/plusi/model/UpdateFilter;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/UpdateFilter;-><init>()V

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mFromWidget:Z

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPostsData;->getStreamNamespaces(Z)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/UpdateFilter;->includeNamespace:Ljava/util/List;

    iget v0, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mView:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mCircleId:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->mGaiaId:Ljava/lang/String;

    if-nez v0, :cond_3

    move v3, v1

    :goto_2
    if-nez v3, :cond_4

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->skipPopularMixin:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    new-instance v1, Lcom/google/api/services/plusi/model/UpdateMixinFilter;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/UpdateMixinFilter;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/StreamParams;->updateMixinFilter:Lcom/google/api/services/plusi/model/UpdateMixinFilter;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->updateMixinFilter:Lcom/google/api/services/plusi/model/UpdateMixinFilter;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPostsData;->getMixinsWhitelist(Z)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/UpdateMixinFilter;->mixinType:Ljava/util/List;

    new-instance v0, Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientEmbedOptions;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    invoke-static {}, Lcom/google/android/apps/plus/content/EsPostsData;->getEmbedsWhitelist()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ClientEmbedOptions;->includeType:Ljava/util/List;

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->isUserInitiated:Ljava/lang/Boolean;

    return-void

    :pswitch_1
    const-string v0, "CIRCLES"

    goto/16 :goto_0

    :pswitch_2
    const-string v0, "WHATS_HOT"

    goto/16 :goto_0

    :pswitch_3
    const-string v0, "SQUARES"

    goto/16 :goto_0

    :cond_2
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    const-string v3, "BEST"

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->sort:Ljava/lang/String;

    goto/16 :goto_1

    :cond_3
    move v3, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
