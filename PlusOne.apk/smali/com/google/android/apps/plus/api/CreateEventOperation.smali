.class public final Lcom/google/android/apps/plus/api/CreateEventOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "CreateEventOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PostActivityRequest;",
        "Lcom/google/api/services/plusi/model/GetActivitiesResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final EVENT_EMBED_TYPES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private final mExternalId:Ljava/lang/String;

.field private final mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "PLUS_EVENT"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "EVENT"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "THING"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/api/CreateEventOperation;->EVENT_EMBED_TYPES:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p4    # Lcom/google/android/apps/plus/content/AudienceData;
    .param p5    # Ljava/lang/String;
    .param p6    # Landroid/content/Intent;
    .param p7    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "postactivity"

    invoke-static {}, Lcom/google/api/services/plusi/model/PostActivityRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PostActivityRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetActivitiesResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetActivitiesResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/CreateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/CreateEventOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/CreateEventOperation;->mExternalId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Stream;->update:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Stream;->update:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Stream;->update:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/Update;

    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v3, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/api/CreateEventOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/CreateEventOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    invoke-static {v1, v2, v4, v3, v0}, Lcom/google/android/apps/plus/content/EsEventData;->insertEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/PostActivityRequest;

    new-instance v0, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestAttribution;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestAttribution;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->attribution:Lcom/google/api/services/plusi/model/RequestsPostActivityRequestAttribution;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->attribution:Lcom/google/api/services/plusi/model/RequestsPostActivityRequestAttribution;

    const-string v1, "Mobile"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestAttribution;->androidAppName:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/CreateEventOperation;->mExternalId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->externalId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/CreateEventOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->convertAudienceToSharingRoster(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/api/services/plusi/model/SharingRoster;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/CreateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->updateText:Ljava/lang/String;

    new-instance v0, Lcom/google/api/services/plusi/model/EmbedClientItem;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EmbedClientItem;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    sget-object v1, Lcom/google/android/apps/plus/api/CreateEventOperation;->EVENT_EMBED_TYPES:Ljava/util/List;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->type:Ljava/util/List;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/CreateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    return-void
.end method
