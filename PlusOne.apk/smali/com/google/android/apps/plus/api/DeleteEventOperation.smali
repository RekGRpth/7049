.class public final Lcom/google/android/apps/plus/api/DeleteEventOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "DeleteEventOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/DeleteEventRequest;",
        "Lcom/google/api/services/plusi/model/DeleteEventResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mAuthKey:Ljava/lang/String;

.field private mEventId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/content/Intent;
    .param p6    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "deleteevent"

    invoke-static {}, Lcom/google/api/services/plusi/model/DeleteEventRequestJson;->getInstance()Lcom/google/api/services/plusi/model/DeleteEventRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/DeleteEventResponseJson;->getInstance()Lcom/google/api/services/plusi/model/DeleteEventResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/DeleteEventOperation;->mEventId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/DeleteEventOperation;->mAuthKey:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/DeleteEventOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/DeleteEventOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/DeleteEventOperation;->mEventId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsEventData;->deleteEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/DeleteEventRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/DeleteEventOperation;->mEventId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/DeleteEventRequest;->eventId:Ljava/lang/String;

    new-instance v0, Lcom/google/api/services/plusi/model/EventSelector;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EventSelector;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/DeleteEventOperation;->mEventId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventSelector;->eventId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/DeleteEventOperation;->mAuthKey:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventSelector;->authKey:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/DeleteEventRequest;->eventSelector:Lcom/google/api/services/plusi/model/EventSelector;

    return-void
.end method
