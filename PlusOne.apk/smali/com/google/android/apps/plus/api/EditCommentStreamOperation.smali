.class public final Lcom/google/android/apps/plus/api/EditCommentStreamOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "EditCommentStreamOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/EditCommentRequest;",
        "Lcom/google/api/services/plusi/model/EditCommentResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivityId:Ljava/lang/String;

.field private final mCommentId:Ljava/lang/String;

.field private final mContent:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;

    const-string v3, "editcomment"

    invoke-static {}, Lcom/google/api/services/plusi/model/EditCommentRequestJson;->getInstance()Lcom/google/api/services/plusi/model/EditCommentRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/EditCommentResponseJson;->getInstance()Lcom/google/api/services/plusi/model/EditCommentResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mActivityId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mCommentId:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mContent:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/EditCommentResponse;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/EditCommentResponse;->comment:Lcom/google/api/services/plusi/model/Comment;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mActivityId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Comment;->updateId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mCommentId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Comment;->updateId:Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/content/EsPostsData;->updateComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/Comment;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/Comment;)V

    :cond_0
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/EditCommentRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mActivityId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EditCommentRequest;->activityId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mCommentId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EditCommentRequest;->commentId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mContent:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EditCommentRequest;->commentText:Ljava/lang/String;

    return-void
.end method
