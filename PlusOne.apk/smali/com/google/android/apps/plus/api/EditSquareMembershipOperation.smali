.class public final Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "EditSquareMembershipOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/EditSquareMembershipOzRequest;",
        "Lcom/google/api/services/plusi/model/EditSquareMembershipOzResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAction:Ljava/lang/String;

.field private mIsBlockingModerator:Z

.field private final mSquareId:Ljava/lang/String;

.field private final mTargetGaiaId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Landroid/content/Intent;
    .param p7    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "editsquaremembership"

    invoke-static {}, Lcom/google/api/services/plusi/model/EditSquareMembershipOzRequestJson;->getInstance()Lcom/google/api/services/plusi/model/EditSquareMembershipOzRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/EditSquareMembershipOzResponseJson;->getInstance()Lcom/google/api/services/plusi/model/EditSquareMembershipOzResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mSquareId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mTargetGaiaId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mAction:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getIsBlockingModerator()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mIsBlockingModerator:Z

    return v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/EditSquareMembershipOzResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/EditSquareMembershipOzResponse;->isViewerBlockingModerator:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mIsBlockingModerator:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mTargetGaiaId:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mSquareId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mAction:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsSquaresData;->updateSquareMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mSquareId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mTargetGaiaId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mAction:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsSquaresData;->updateTargetMembershipStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/EditSquareMembershipOzRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mSquareId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EditSquareMembershipOzRequest;->obfuscatedSquareId:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mTargetGaiaId:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EditSquareMembershipOzRequest;->targetObfuscatedGaiaId:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->mAction:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EditSquareMembershipOzRequest;->action:Ljava/lang/String;

    return-void
.end method
