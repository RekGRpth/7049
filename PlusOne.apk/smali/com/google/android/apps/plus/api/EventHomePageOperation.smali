.class public final Lcom/google/android/apps/plus/api/EventHomePageOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "EventHomePageOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/EventsHomeRequest;",
        "Lcom/google/api/services/plusi/model/EventsHomeResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "eventshome"

    invoke-static {}, Lcom/google/api/services/plusi/model/EventsHomeRequestJson;->getInstance()Lcom/google/api/services/plusi/model/EventsHomeRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/EventsHomeResponseJson;->getInstance()Lcom/google/api/services/plusi/model/EventsHomeResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/EventsHomeResponse;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventHomePageOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EventHomePageOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventsHomeResponse;->upcoming:Ljava/util/List;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/EventsHomeResponse;->declinedUpcoming:Ljava/util/List;

    iget-object v4, p1, Lcom/google/api/services/plusi/model/EventsHomeResponse;->past:Ljava/util/List;

    iget-object v5, p1, Lcom/google/api/services/plusi/model/EventsHomeResponse;->resolvedPerson:Ljava/util/List;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventHomeList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    return-void
.end method
