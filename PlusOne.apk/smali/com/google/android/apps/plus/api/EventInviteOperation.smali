.class public final Lcom/google/android/apps/plus/api/EventInviteOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "EventInviteOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/InviteEventRequest;",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private final mAuthKey:Ljava/lang/String;

.field private final mEventId:Ljava/lang/String;

.field private final mOwnerId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/apps/plus/content/AudienceData;
    .param p7    # Landroid/content/Intent;
    .param p8    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "inviteevent"

    invoke-static {}, Lcom/google/api/services/plusi/model/InviteEventRequestJson;->getInstance()Lcom/google/api/services/plusi/model/InviteEventRequestJson;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/EventInviteOperation;->mEventId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/EventInviteOperation;->mOwnerId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/EventInviteOperation;->mAuthKey:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/EventInviteOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    return-void
.end method


# virtual methods
.method protected final handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/InviteEventRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventInviteOperation;->mEventId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/InviteEventRequest;->eventId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventInviteOperation;->mOwnerId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/InviteEventRequest;->organizerId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventInviteOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->convertAudienceToSharingRoster(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/api/services/plusi/model/SharingRoster;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/InviteEventRequest;->inviteRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    new-instance v0, Lcom/google/api/services/plusi/model/EventSelector;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EventSelector;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EventInviteOperation;->mEventId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventSelector;->eventId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EventInviteOperation;->mAuthKey:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventSelector;->authKey:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/InviteEventRequest;->eventSelector:Lcom/google/api/services/plusi/model/EventSelector;

    return-void
.end method
