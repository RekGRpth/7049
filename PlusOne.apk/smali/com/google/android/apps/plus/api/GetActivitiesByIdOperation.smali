.class public final Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetActivitiesByIdOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;",
        "Lcom/google/api/services/plusi/model/GetActivitiesByIdResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivityIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mResponseUpdateId:Ljava/lang/String;

.field private final mSquareId:Ljava/lang/String;

.field private final mViewerIsModerator:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Landroid/content/Intent;
    .param p7    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "getactivitiesbyid"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetActivitiesByIdRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetActivitiesByIdResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetActivitiesByIdResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->mActivityIds:Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->mSquareId:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->mViewerIsModerator:Z

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdResponse;->update:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdResponse;->update:Ljava/util/List;

    const-string v4, "DEFAULT"

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivitiesAndOverwrite(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Z)V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdResponse;->update:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/Update;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->mResponseUpdateId:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->mActivityIds:Ljava/util/List;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;->activityId:Ljava/util/List;

    new-instance v0, Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientEmbedOptions;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;->embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;->embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    invoke-static {}, Lcom/google/android/apps/plus/content/EsPostsData;->getEmbedsWhitelist()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ClientEmbedOptions;->includeType:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->mSquareId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->mSquareId:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->mViewerIsModerator:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsPostsData;->createSquareStreamRenderContext(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/api/services/plusi/model/RenderContext;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;->renderContext:Lcom/google/api/services/plusi/model/RenderContext;

    :cond_0
    return-void
.end method
