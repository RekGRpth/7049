.class public final Lcom/google/android/apps/plus/api/GetActivitiesOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetActivitiesOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetActivitiesRequest;",
        "Lcom/google/api/services/plusi/model/GetActivitiesResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCircleId:Ljava/lang/String;

.field private final mContinuationToken:Ljava/lang/String;

.field private final mFromWidget:Z

.field private final mGaiaId:Ljava/lang/String;

.field private final mMaxCount:I

.field private mRequestTime:J

.field private final mSquareStreamId:Ljava/lang/String;

.field private final mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

.field private final mView:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Z
    .param p8    # Ljava/lang/String;
    .param p9    # I
    .param p10    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p11    # Landroid/content/Intent;
    .param p12    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v4, "getactivities"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetActivitiesRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetActivitiesRequestJson;

    move-result-object v5

    invoke-static {}, Lcom/google/api/services/plusi/model/GetActivitiesResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetActivitiesResponseJson;

    move-result-object v6

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v8, p12

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput p3, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mView:I

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "f."

    invoke-virtual {p4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {p4, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p4

    :cond_0
    iput-object p4, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mCircleId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mGaiaId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mSquareStreamId:Ljava/lang/String;

    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mFromWidget:Z

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    if-lez p9, :cond_1

    :goto_0
    move/from16 v0, p9

    iput v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mMaxCount:I

    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    return-void

    :cond_1
    const/16 p9, 0xa

    goto :goto_0
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x4

    check-cast p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;

    iget v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mView:I

    if-ne v0, v5, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mGaiaId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mSquareStreamId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->buildSquareStreamKey(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    :goto_0
    const-string v0, "GetActivitiesOp"

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/Stream;->continuationToken:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "GetActivitiesOp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v0, :cond_2

    const-string v0, "!!!!!"

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "Sent token "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " at time "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mRequestTime:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " and received token "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Stream;->continuationToken:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " with "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Stream;->item:Ljava/util/List;

    if-nez v0, :cond_3

    const-string v0, "0"

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " activities for "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v0, :cond_4

    const-string v0, "?"

    :goto_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Stream;->item:Ljava/util/List;

    const-string v4, "MOBILE"

    iget-object v5, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    iget-object v6, p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/Stream;->continuationToken:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/content/EsPostsData;->updateStreamItems(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mGaiaId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mCircleId:Ljava/lang/String;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mFromWidget:Z

    iget v4, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mView:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_1

    :cond_3
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Stream;->item:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const/4 v4, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    check-cast p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;

    new-instance v0, Lcom/google/api/services/plusi/model/StreamParams;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/StreamParams;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mView:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v0, "ALL"

    :goto_0
    iput-object v0, v3, Lcom/google/api/services/plusi/model/StreamParams;->viewType:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mView:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mView:I

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mCircleId:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mGaiaId:Ljava/lang/String;

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    const-string v3, "LATEST"

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->sort:Ljava/lang/String;

    :goto_1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mCircleId:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->focusGroupId:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mGaiaId:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->productionStreamOid:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mSquareStreamId:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->squareStreamId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->continuesToken:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget v3, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mMaxCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->maxNumUpdates:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    const-string v3, "MOBILE"

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->collapserType:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->skipCommentCollapsing:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->maxComments:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->maxNumImages:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->maxResharers:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    new-instance v3, Lcom/google/api/services/plusi/model/FieldRequestOptions;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/FieldRequestOptions;-><init>()V

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v3, v0, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeLegacyMediaData:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v3, v0, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeEmbedsData:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v3, v0, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeSharingData:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v3, v0, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeUrls:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    new-instance v3, Lcom/google/api/services/plusi/model/UpdateFilter;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/UpdateFilter;-><init>()V

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mFromWidget:Z

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPostsData;->getStreamNamespaces(Z)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/UpdateFilter;->includeNamespace:Ljava/util/List;

    iget v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mView:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mCircleId:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mGaiaId:Ljava/lang/String;

    if-nez v0, :cond_3

    move v3, v1

    :goto_2
    if-nez v3, :cond_4

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->skipPopularMixin:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    new-instance v4, Lcom/google/api/services/plusi/model/UpdateMixinFilter;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/UpdateMixinFilter;-><init>()V

    iput-object v4, v0, Lcom/google/api/services/plusi/model/StreamParams;->updateMixinFilter:Lcom/google/api/services/plusi/model/UpdateMixinFilter;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->updateMixinFilter:Lcom/google/api/services/plusi/model/UpdateMixinFilter;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPostsData;->getMixinsWhitelist(Z)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/UpdateMixinFilter;->mixinType:Ljava/util/List;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    new-instance v3, Lcom/google/api/services/plusi/model/StreamItemFilter;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/StreamItemFilter;-><init>()V

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->streamItemFilter:Lcom/google/api/services/plusi/model/StreamItemFilter;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->streamItemFilter:Lcom/google/api/services/plusi/model/StreamItemFilter;

    invoke-static {}, Lcom/google/android/apps/plus/content/EsPostsData;->getStreamItemsWhiteList()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamItemFilter;->promoTypes:Ljava/util/List;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->streamItemFilter:Lcom/google/api/services/plusi/model/StreamItemFilter;

    invoke-static {}, Lcom/google/android/apps/plus/content/EsPostsData;->getStreamItemTypes()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamItemFilter;->itemTypes:Ljava/util/List;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->streamItemFilter:Lcom/google/api/services/plusi/model/StreamItemFilter;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamItemFilter;->includeStaticMessages:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/StreamParams;->streamItemFilter:Lcom/google/api/services/plusi/model/StreamItemFilter;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamItemFilter;->useCompactDataProto:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->includeStaticSegments:Ljava/lang/Boolean;

    new-instance v0, Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientEmbedOptions;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    invoke-static {}, Lcom/google/android/apps/plus/content/EsPostsData;->getEmbedsWhitelist()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, v0, Lcom/google/api/services/plusi/model/ClientEmbedOptions;->includeType:Ljava/util/List;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mFromWidget:Z

    if-nez v0, :cond_5

    :goto_4
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->isUserInitiated:Ljava/lang/Boolean;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->mRequestTime:J

    return-void

    :pswitch_1
    const-string v0, "CIRCLES"

    goto/16 :goto_0

    :pswitch_2
    const-string v0, "WHATS_HOT"

    goto/16 :goto_0

    :pswitch_3
    const-string v0, "SQUARES"

    goto/16 :goto_0

    :cond_2
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivitiesRequest;->streamParams:Lcom/google/api/services/plusi/model/StreamParams;

    const-string v3, "BEST"

    iput-object v3, v0, Lcom/google/api/services/plusi/model/StreamParams;->sort:Ljava/lang/String;

    goto/16 :goto_1

    :cond_3
    move v3, v2

    goto/16 :goto_2

    :cond_4
    move v0, v2

    goto/16 :goto_3

    :cond_5
    move v1, v2

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
