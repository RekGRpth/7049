.class public final Lcom/google/android/apps/plus/api/GetActivityOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetActivityOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetActivityRequest;",
        "Lcom/google/api/services/plusi/model/GetActivityResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivityId:Ljava/lang/String;

.field private final mOwnerGaiaId:Ljava/lang/String;

.field private mResponseUpdateId:Ljava/lang/String;

.field private final mSquareId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Landroid/content/Intent;
    .param p7    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const/4 v6, 0x0

    const-string v3, "getactivity"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetActivityRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetActivityRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetActivityResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetActivityResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mActivityId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mOwnerGaiaId:Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mSquareId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getResponseUpdateId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mResponseUpdateId:Ljava/lang/String;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x1

    check-cast p1, Lcom/google/api/services/plusi/model/GetActivityResponse;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivityResponse;->update:Lcom/google/api/services/plusi/model/Update;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    const-string v4, "DEFAULT"

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivitiesAndOverwrite(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Z)V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/Update;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mResponseUpdateId:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/GetActivityRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mActivityId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivityRequest;->activityId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mOwnerGaiaId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mOwnerGaiaId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivityRequest;->ownerId:Ljava/lang/String;

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivityRequest;->fetchReadState:Ljava/lang/Boolean;

    new-instance v0, Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientEmbedOptions;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivityRequest;->embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetActivityRequest;->embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    invoke-static {}, Lcom/google/android/apps/plus/content/EsPostsData;->getEmbedsWhitelist()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ClientEmbedOptions;->includeType:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mSquareId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mSquareId:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsPostsData;->createSquareStreamRenderContext(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/api/services/plusi/model/RenderContext;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivityRequest;->renderContext:Lcom/google/api/services/plusi/model/RenderContext;

    :cond_1
    return-void
.end method
