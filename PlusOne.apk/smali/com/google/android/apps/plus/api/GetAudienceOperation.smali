.class public final Lcom/google/android/apps/plus/api/GetAudienceOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetAudienceOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetAudienceRequest;",
        "Lcom/google/api/services/plusi/model/GetAudienceResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivityId:Ljava/lang/String;

.field private mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/content/Intent;
    .param p5    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "getaudience"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetAudienceRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetAudienceRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetAudienceResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetAudienceResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/GetAudienceOperation;->mActivityId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getAudience()Lcom/google/android/apps/plus/content/AudienceData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetAudienceOperation;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 12
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v11, 0x0

    const/4 v0, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/GetAudienceResponse;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetAudienceResponse;->gaiaAudienceCount:Ljava/lang/Integer;

    if-nez v1, :cond_0

    move v1, v0

    :goto_0
    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetAudienceResponse;->nonGaiaAudienceCount:Ljava/lang/Integer;

    if-nez v2, :cond_1

    move v2, v0

    :goto_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetAudienceResponse;->person:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetAudienceOperation;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/GetAudienceOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetAudienceResponse;->person:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/Person;

    new-instance v6, Lcom/google/android/apps/plus/content/PersonData;

    iget-object v7, v0, Lcom/google/api/services/plusi/model/Person;->obfuscatedId:Ljava/lang/String;

    iget-object v8, v0, Lcom/google/api/services/plusi/model/Person;->userName:Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, v0, Lcom/google/api/services/plusi/model/Person;->photoUrl:Ljava/lang/String;

    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsAvatarData;->compressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v7, v8, v9, v10}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v6, v0, Lcom/google/api/services/plusi/model/Person;->obfuscatedId:Ljava/lang/String;

    iget-object v7, v0, Lcom/google/api/services/plusi/model/Person;->userName:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Person;->photoUrl:Ljava/lang/String;

    invoke-static {v4, v6, v7, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    :cond_0
    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetAudienceResponse;->gaiaAudienceCount:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetAudienceResponse;->nonGaiaAudienceCount:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v2, v0

    goto :goto_1

    :cond_2
    :try_start_1
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_3
    new-instance v0, Lcom/google/android/apps/plus/content/AudienceData;

    add-int/2addr v1, v2

    invoke-direct {v0, v3, v11, v1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetAudienceOperation;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/GetAudienceRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetAudienceOperation;->mActivityId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetAudienceRequest;->updateId:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetAudienceRequest;->returnFullProfile:Ljava/lang/Boolean;

    return-void
.end method
