.class public final Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetCelebritySuggestionsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetCelebritySuggestionsRequest;",
        "Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mPreviewOnly:Z

.field private mResponse:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Z

    const-string v3, "getcelebritysuggestions"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetCelebritySuggestionsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponseJson;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-boolean p5, p0, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;->mPreviewOnly:Z

    return-void
.end method


# virtual methods
.method public final getResponse()Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;->mResponse:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v4, 0xa

    check-cast p1, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    iput-object p1, p0, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;->mResponse:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;->previewCeleb:Ljava/util/List;

    if-nez v0, :cond_2

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    const/16 v0, 0x200

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;->category:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;->celebrity:Ljava/util/List;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v4, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iput-object v3, p1, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;->previewCeleb:Ljava/util/List;

    :cond_2
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsRequest;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;->mPreviewOnly:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsRequest;->maxPerCategory:Ljava/lang/Integer;

    return-void

    :cond_0
    const/16 v0, 0x50

    goto :goto_0
.end method
