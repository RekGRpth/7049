.class public final Lcom/google/android/apps/plus/api/GetDoritosCookieOperation;
.super Lcom/google/android/apps/plus/network/HttpOperation;
.source "GetDoritosCookieOperation.java"


# instance fields
.field private mDoritosCookie:Lorg/apache/http/cookie/Cookie;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x0

    const-string v2, "POST"

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->SOCIAL_ADS_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/HttpOperation;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;Ljava/io/OutputStream;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    return-void
.end method


# virtual methods
.method public final getDoritosCookie()Lorg/apache/http/cookie/Cookie;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetDoritosCookieOperation;->mDoritosCookie:Lorg/apache/http/cookie/Cookie;

    return-object v0
.end method

.method public final onHttpCookie(Lorg/apache/http/cookie/Cookie;)V
    .locals 2
    .param p1    # Lorg/apache/http/cookie/Cookie;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/network/HttpOperation;->onHttpCookie(Lorg/apache/http/cookie/Cookie;)V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "_drt_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/plus/api/GetDoritosCookieOperation;->mDoritosCookie:Lorg/apache/http/cookie/Cookie;

    :cond_0
    return-void
.end method
