.class public interface abstract Lcom/google/android/apps/plus/api/GetEventOperation$EventQuery;
.super Ljava/lang/Object;
.source "GetEventOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/api/GetEventOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EventQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "polling_token"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "resume_token"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "display_time"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/api/GetEventOperation$EventQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
