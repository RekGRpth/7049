.class public final Lcom/google/android/apps/plus/api/GetNotificationsOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetNotificationsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetNotificationsRequest;",
        "Lcom/google/api/services/plusi/model/GetNotificationsResponse;",
        ">;"
    }
.end annotation


# static fields
.field public static final TYPE_GROUP_TO_FETCH:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataActor;",
            ">;"
        }
    .end annotation
.end field

.field private mContinuationToken:Ljava/lang/String;

.field private mLastNotificationTime:D

.field private mLastReadTime:Ljava/lang/Double;

.field private mNotifications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCoalescedItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "BASELINE_STREAM"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "BASELINE_CIRCLE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "BASELINE_PHOTOS"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "BASELINE_EVENTS"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "BASELINE_SQUARE"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "CIRCLE_SUGGESTIONS_GROUP"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->TYPE_GROUP_TO_FETCH:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "getnotifications"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetNotificationsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetNotificationsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetNotificationsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetNotificationsResponseJson;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    return-void
.end method


# virtual methods
.method public final getContinuationToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mContinuationToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getDataActors()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataActor;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mActors:Ljava/util/List;

    return-object v0
.end method

.method public final getLastReadTime()Ljava/lang/Double;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mLastReadTime:Ljava/lang/Double;

    return-object v0
.end method

.method public final getNotifications()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCoalescedItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mNotifications:Ljava/util/List;

    return-object v0
.end method

.method public final getNotifications(DLjava/lang/String;)V
    .locals 0
    .param p1    # D
    .param p3    # Ljava/lang/String;

    iput-wide p1, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mLastNotificationTime:D

    iput-object p3, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mContinuationToken:Ljava/lang/String;

    return-void
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/GetNotificationsResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->onStartResultProcessing()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsResponse;->notificationsData:Lcom/google/api/services/plusi/model/DataNotificationsData;

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationsData;->hasMoreNotifications:Ljava/lang/Boolean;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationsData;->continuationToken:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mContinuationToken:Ljava/lang/String;

    :cond_0
    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationsData;->lastReadTime:Ljava/lang/Double;

    iput-object v1, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mLastReadTime:Ljava/lang/Double;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationsData;->coalescedItem:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mNotifications:Ljava/util/List;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationsData;->actor:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mActors:Ljava/util/List;

    :cond_1
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const/4 v4, 0x1

    check-cast p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;

    const-wide/16 v0, 0xf

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->maxResults:Ljava/lang/Long;

    const-string v0, "NOTIFICATIONS_MOBILE"

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->renderContextLocation:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mContinuationToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mContinuationToken:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->continuationToken:Ljava/lang/String;

    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mLastNotificationTime:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mLastNotificationTime:D

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->oldestNotificationTimeUsec:Ljava/math/BigInteger;

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "PLAIN"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->summarySnippets:Ljava/util/List;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->setPushEnabled:Ljava/lang/Boolean;

    sget-object v0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->TYPE_GROUP_TO_FETCH:Ljava/util/List;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->typeGroupToFetch:Ljava/util/List;

    new-instance v0, Lcom/google/api/services/plusi/model/NotificationsResponseOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/NotificationsResponseOptions;-><init>()V

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/NotificationsResponseOptions;->includeFullActorDetails:Ljava/lang/Boolean;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/NotificationsResponseOptions;->includeFullEntityDetails:Ljava/lang/Boolean;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/NotificationsResponseOptions;->includeFullRootDetails:Ljava/lang/Boolean;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/NotificationsResponseOptions;->numPhotoEntities:Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/NotificationsResponseOptions;->includeActorMap:Ljava/lang/Boolean;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->notificationsResponseOptions:Lcom/google/api/services/plusi/model/NotificationsResponseOptions;

    return-void
.end method
