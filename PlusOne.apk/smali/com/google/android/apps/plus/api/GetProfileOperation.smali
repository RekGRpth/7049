.class public final Lcom/google/android/apps/plus/api/GetProfileOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetProfileOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetSimpleProfileRequest;",
        "Lcom/google/api/services/plusi/model/GetSimpleProfileResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mInsertInDatabase:Z

.field private final mOwnerId:Ljava/lang/String;

.field private mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Landroid/content/Intent;
    .param p6    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const/4 v6, 0x0

    const-string v3, "getsimpleprofile"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetSimpleProfileRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetSimpleProfileRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetSimpleProfileResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetSimpleProfileResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mOwnerId:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mInsertInDatabase:Z

    return-void
.end method


# virtual methods
.method public final getProfile()Lcom/google/api/services/plusi/model/SimpleProfile;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/GetSimpleProfileResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetSimpleProfileResponse;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mOwnerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {}, Lcom/google/android/apps/plus/content/DeviceLocationSettings;->getRecentlySavedSettings()Lcom/google/android/apps/plus/content/DeviceLocationSettings;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/api/services/plusi/model/User;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/User;-><init>()V

    iput-object v2, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    :cond_0
    iget-object v2, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/User;->deviceLocations:Lcom/google/api/services/plusi/model/DeviceLocations;

    if-nez v2, :cond_1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    new-instance v3, Lcom/google/api/services/plusi/model/DeviceLocations;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/DeviceLocations;-><init>()V

    iput-object v3, v2, Lcom/google/api/services/plusi/model/User;->deviceLocations:Lcom/google/api/services/plusi/model/DeviceLocations;

    :cond_1
    iget-object v0, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/User;->deviceLocations:Lcom/google/api/services/plusi/model/DeviceLocations;

    iget-object v1, v1, Lcom/google/android/apps/plus/content/DeviceLocationSettings;->isLocationSharingEnabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DeviceLocations;->sharingEnabled:Ljava/lang/Boolean;

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mInsertInDatabase:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mOwnerId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->insertProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/SimpleProfile;)V

    :cond_3
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/GetSimpleProfileRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mOwnerId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetSimpleProfileRequest;->ownerId:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetSimpleProfileRequest;->useCachedDataForCircles:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mOwnerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetSimpleProfileRequest;->includeAclData:Ljava/lang/Boolean;

    return-void
.end method
