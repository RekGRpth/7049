.class public final Lcom/google/android/apps/plus/api/GetSettingsOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetSettingsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetMobileSettingsRequest;",
        "Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mAllowNonGooglePlusUsers:Ljava/lang/Boolean;

.field private mSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

.field private mSetupAccount:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Z
    .param p4    # Landroid/content/Intent;
    .param p5    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "getmobilesettings"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetMobileSettingsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetMobileSettingsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetMobileSettingsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetMobileSettingsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-boolean p3, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mSetupAccount:Z

    return-void
.end method


# virtual methods
.method public final getAccountSettings()Lcom/google/android/apps/plus/content/AccountSettingsData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 13
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v2, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->user:Lcom/google/api/services/plusi/model/MobileSettingsUser;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->user:Lcom/google/api/services/plusi/model/MobileSettingsUser;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MobileSettingsUser;->info:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->user:Lcom/google/api/services/plusi/model/MobileSettingsUser;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MobileSettingsUser;->info:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const-string v0, "HttpTransaction"

    const-string v1, "Settings response missing gaid ID"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Settings response missing gaid ID"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v5, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->user:Lcom/google/api/services/plusi/model/MobileSettingsUser;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/MobileSettingsUser;->info:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    iget-object v1, v7, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->obfuscatedGaiaId:Ljava/lang/String;

    iget-object v3, v7, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->displayName:Ljava/lang/String;

    iget-object v4, v7, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->photoIsSilhouette:Ljava/lang/Boolean;

    if-nez v4, :cond_6

    iget-object v4, v7, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->photoUrl:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    move v6, v0

    :goto_0
    iget-object v4, v5, Lcom/google/api/services/plusi/model/MobileSettingsUser;->isChild:Ljava/lang/Boolean;

    if-eqz v4, :cond_8

    iget-object v4, v5, Lcom/google/api/services/plusi/model/MobileSettingsUser;->isChild:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_8

    move v4, v0

    :goto_1
    new-instance v8, Lcom/google/android/apps/plus/content/AccountSettingsData;

    iget-object v9, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-boolean v11, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mSetupAccount:Z

    invoke-direct {v8, v9, v10, p1, v11}, Lcom/google/android/apps/plus/content/AccountSettingsData;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;Z)V

    iput-object v8, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    iget-boolean v8, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mSetupAccount:Z

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v8

    if-nez v8, :cond_2

    iget-object v8, v5, Lcom/google/api/services/plusi/model/MobileSettingsUser;->plusPageInfo:Ljava/util/List;

    if-eqz v8, :cond_2

    iget-object v8, v5, Lcom/google/api/services/plusi/model/MobileSettingsUser;->plusPageInfo:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-gtz v8, :cond_4

    :cond_2
    iget-object v8, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v5, v5, Lcom/google/api/services/plusi/model/MobileSettingsUser;->isPlusPage:Ljava/lang/Boolean;

    invoke-static {v5}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v5

    if-eqz v5, :cond_9

    :cond_3
    move v5, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->insertAccount(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mContext:Landroid/content/Context;

    iget-object v2, v7, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->photoUrl:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->activateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveServerSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)V

    :cond_4
    return-void

    :cond_5
    move v6, v2

    goto :goto_0

    :cond_6
    iget-object v4, v7, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->photoIsSilhouette:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_7

    move v6, v0

    goto :goto_0

    :cond_7
    move v6, v2

    goto :goto_0

    :cond_8
    move v4, v2

    goto :goto_1

    :cond_9
    move v5, v2

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v7, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object v9, v1

    move-object v10, v3

    move v11, v4

    move v12, v6

    invoke-static/range {v7 .. v12}, Lcom/google/android/apps/plus/content/EsAccountsData;->updateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_3
.end method

.method public final hasPlusPages()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getNumPlusPages()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasSkinnyPlusPages()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->hasPlusPages()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AccountSettingsData;->isGooglePlus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/GetMobileSettingsRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mAllowNonGooglePlusUsers:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mAllowNonGooglePlusUsers:Ljava/lang/Boolean;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsRequest;->allowNonGooglePlusUsers:Ljava/lang/Boolean;

    :cond_0
    return-void
.end method

.method public final setAllowNonGooglePlusUsers(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mAllowNonGooglePlusUsers:Ljava/lang/Boolean;

    return-void
.end method
