.class public final Lcom/google/android/apps/plus/api/GetSquaresOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetSquaresOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetSquaresOzRequest;",
        "Lcom/google/api/services/plusi/model/GetSquaresOzResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const-string v3, "getsquares"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetSquaresOzRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetSquaresOzRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetSquaresOzResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetSquaresOzResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetSquaresOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/GetSquaresOzResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetSquaresOzResponse;->invitedSquare:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetSquaresOzResponse;->invitedSquare:Ljava/util/List;

    :goto_0
    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetSquaresOzResponse;->joinedSquare:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetSquaresOzResponse;->joinedSquare:Ljava/util/List;

    :goto_1
    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetSquaresOzResponse;->suggestedSquare:Ljava/util/List;

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetSquaresOzResponse;->suggestedSquare:Ljava/util/List;

    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/plus/api/GetSquaresOperation;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/GetSquaresOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v4, v0, v1, v2}, Lcom/google/android/apps/plus/content/EsSquaresData;->insertSquares(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;Ljava/util/List;Ljava/util/List;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetSquaresOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetSquaresOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount(I)V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_1

    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    goto :goto_2
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/GetSquaresOzRequest;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetSquaresOzRequest;->includePeopleInCommon:Ljava/lang/Boolean;

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    const-string v1, "INVITED"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "JOINED"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "SUGGESTED"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetSquaresOzRequest;->squareType:Ljava/util/List;

    return-void
.end method
