.class public final Lcom/google/android/apps/plus/api/GetViewerSquareOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetViewerSquareOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetViewerSquareOzRequest;",
        "Lcom/google/api/services/plusi/model/GetViewerSquareOzResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mSquareId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/content/Intent;
    .param p5    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const/4 v6, 0x0

    const-string v3, "getviewersquare"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetViewerSquareOzRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetViewerSquareOzRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetViewerSquareOzResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetViewerSquareOzResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/GetViewerSquareOperation;->mSquareId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/GetViewerSquareOzResponse;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetViewerSquareOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetViewerSquareOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetViewerSquareOzResponse;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsSquaresData;->insertSquare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/ViewerSquare;)Z

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/GetViewerSquareOzRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetViewerSquareOperation;->mSquareId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetViewerSquareOzRequest;->obfuscatedSquareId:Ljava/lang/String;

    return-void
.end method
