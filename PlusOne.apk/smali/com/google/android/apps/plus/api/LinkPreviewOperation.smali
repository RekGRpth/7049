.class public final Lcom/google/android/apps/plus/api/LinkPreviewOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "LinkPreviewOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/LinkPreviewRequest;",
        "Lcom/google/api/services/plusi/model/LinkPreviewResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mActivity:Lcom/google/android/apps/plus/api/ApiaryActivity;

.field private final mCallToAction:Lcom/google/android/apps/plus/api/CallToActionData;

.field private final mSourceUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Lcom/google/android/apps/plus/api/CallToActionData;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/apps/plus/api/CallToActionData;
    .param p7    # Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    const-string v10, "linkpreview"

    invoke-static {}, Lcom/google/api/services/plusi/model/LinkPreviewRequestJson;->getInstance()Lcom/google/api/services/plusi/model/LinkPreviewRequestJson;

    move-result-object v11

    invoke-static {}, Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;->getInstance()Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v1, Lcom/google/android/apps/plus/network/PlatformHttpRequestConfiguration;

    const-string v4, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite https://www.googleapis.com/auth/plus.native"

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->PLUS_BACKEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v5

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v6, p7

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/network/PlatformHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v10

    move-object v6, v11

    move-object v10, v1

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;)V

    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->mSourceUrl:Ljava/lang/String;

    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->mCallToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    return-void
.end method


# virtual methods
.method public final getActivity()Lcom/google/android/apps/plus/api/ApiaryActivity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->mActivity:Lcom/google/android/apps/plus/api/ApiaryActivity;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/api/ApiaryActivityFactory;->getApiaryActivity(Lcom/google/api/services/plusi/model/LinkPreviewResponse;)Lcom/google/android/apps/plus/api/ApiaryActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->mActivity:Lcom/google/android/apps/plus/api/ApiaryActivity;

    :cond_0
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const/4 v1, 0x1

    check-cast p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->mSourceUrl:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->content:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->mCallToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    if-eqz v0, :cond_0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->isInteractivePost:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->mCallToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    iget-object v0, v0, Lcom/google/android/apps/plus/api/CallToActionData;->mLabel:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->callToActionLabel:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->mCallToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    iget-object v0, v0, Lcom/google/android/apps/plus/api/CallToActionData;->mUrl:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->callToActionUrl:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->mCallToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    iget-object v0, v0, Lcom/google/android/apps/plus/api/CallToActionData;->mDeepLinkId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->callToActionDeepLinkId:Ljava/lang/String;

    :cond_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->useBlackboxPreviewData:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->fallbackToUrl:Ljava/lang/Boolean;

    new-instance v0, Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientEmbedOptions;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    invoke-static {}, Lcom/google/android/apps/plus/content/EsPostsData;->getShareboxEmbedsWhitelist()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ClientEmbedOptions;->includeType:Ljava/util/List;

    return-void
.end method
