.class public final Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "LoadSocialNetworkOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;",
        "Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mCircleList:Lcom/google/api/services/plusi/model/DataViewerCircles;

.field private final mLoadCircles:Z

.field private final mLoadPeople:Z

.field private final mMaxPeople:I

.field private mPersonList:Lcom/google/api/services/plusi/model/DataPersonList;

.field private final mSyncStateToken:Ljava/lang/String;

.field private mSystemGroups:Lcom/google/api/services/plusi/model/DataSystemGroups;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZZILjava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Z
    .param p4    # Z
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .param p7    # Landroid/content/Intent;
    .param p8    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "loadsocialnetwork"

    invoke-static {}, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestJson;->getInstance()Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponseJson;->getInstance()Lcom/google/api/services/plusi/model/LoadSocialNetworkResponseJson;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p6, p0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->mSyncStateToken:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->mLoadCircles:Z

    iput-boolean p4, p0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->mLoadPeople:Z

    iput p5, p0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->mMaxPeople:I

    return-void
.end method


# virtual methods
.method public final getCircleList()Lcom/google/api/services/plusi/model/DataViewerCircles;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->mCircleList:Lcom/google/api/services/plusi/model/DataViewerCircles;

    return-object v0
.end method

.method public final getPersonList()Lcom/google/api/services/plusi/model/DataPersonList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->mPersonList:Lcom/google/api/services/plusi/model/DataPersonList;

    return-object v0
.end method

.method public final getSystemGroups()Lcom/google/api/services/plusi/model/DataSystemGroups;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->mSystemGroups:Lcom/google/api/services/plusi/model/DataSystemGroups;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;->viewerCircles:Lcom/google/api/services/plusi/model/DataViewerCircles;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->mCircleList:Lcom/google/api/services/plusi/model/DataViewerCircles;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;->systemGroups:Lcom/google/api/services/plusi/model/DataSystemGroups;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->mSystemGroups:Lcom/google/api/services/plusi/model/DataSystemGroups;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;->personList:Lcom/google/api/services/plusi/model/DataPersonList;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->mPersonList:Lcom/google/api/services/plusi/model/DataPersonList;

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const/4 v2, 0x1

    check-cast p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->mLoadCircles:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestViewerCirclesOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestViewerCirclesOptions;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->circlesOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestViewerCirclesOptions;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->circlesOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestViewerCirclesOptions;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestViewerCirclesOptions;->includeCircles:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->circlesOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestViewerCirclesOptions;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestViewerCirclesOptions;->includeMemberCounts:Ljava/lang/Boolean;

    new-instance v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestSystemGroupsOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestSystemGroupsOptions;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->systemGroupsOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestSystemGroupsOptions;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->systemGroupsOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestSystemGroupsOptions;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestSystemGroupsOptions;->includeSystemGroups:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->systemGroupsOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestSystemGroupsOptions;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestSystemGroupsOptions;->includeMemberCounts:Ljava/lang/Boolean;

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->mLoadPeople:Z

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestPersonListOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestPersonListOptions;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->personListOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestPersonListOptions;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->personListOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestPersonListOptions;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestPersonListOptions;->includePeople:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->personListOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestPersonListOptions;

    iget v1, p0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->mMaxPeople:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestPersonListOptions;->maxPeople:Ljava/lang/Integer;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->personListOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestPersonListOptions;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestPersonListOptions;->includeExtendedProfileInfo:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->mSyncStateToken:Ljava/lang/String;

    if-eqz v0, :cond_1

    :try_start_0
    invoke-static {}, Lcom/google/api/services/plusi/model/DataSyncStateTokenJson;->getInstance()Lcom/google/api/services/plusi/model/DataSyncStateTokenJson;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->mSyncStateToken:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/services/plusi/model/DataSyncStateTokenJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataSyncStateToken;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequest;->personListOptions:Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestPersonListOptions;

    iput-object v0, v1, Lcom/google/api/services/plusi/model/LoadSocialNetworkRequestPersonListOptions;->syncStateToken:Lcom/google/api/services/plusi/model/DataSyncStateToken;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
