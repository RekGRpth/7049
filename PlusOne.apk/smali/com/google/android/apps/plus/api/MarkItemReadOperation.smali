.class public final Lcom/google/android/apps/plus/api/MarkItemReadOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "MarkItemReadOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/MarkItemReadRequest;",
        "Lcom/google/api/services/plusi/model/MarkItemReadResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCreationSourceIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mIsNotificationType:Z

.field private final mItemIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/util/List;Ljava/util/List;Z)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p7    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    const-string v3, "markitemread"

    invoke-static {}, Lcom/google/api/services/plusi/model/MarkItemReadRequestJson;->getInstance()Lcom/google/api/services/plusi/model/MarkItemReadRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/MarkItemReadResponseJson;->getInstance()Lcom/google/api/services/plusi/model/MarkItemReadResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mItemIds:Ljava/util/List;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mCreationSourceIds:Ljava/util/List;

    iput-boolean p7, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mIsNotificationType:Z

    return-void
.end method


# virtual methods
.method public final getCreationSourceIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mCreationSourceIds:Ljava/util/List;

    return-object v0
.end method

.method public final getItemIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mItemIds:Ljava/util/List;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mIsNotificationType:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mItemIds:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->markActivitiesAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public final isNotificationType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mIsNotificationType:Z

    return v0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/MarkItemReadRequest;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mIsNotificationType:Z

    if-eqz v0, :cond_0

    const-string v0, "4"

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MarkItemReadRequest;->networkType:Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mItemIds:Ljava/util/List;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MarkItemReadRequest;->itemIds:Ljava/util/List;

    return-void

    :cond_0
    const-string v0, "3"

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MarkItemReadRequest;->networkType:Ljava/lang/String;

    goto :goto_0
.end method
