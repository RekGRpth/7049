.class public final Lcom/google/android/apps/plus/api/MutateProfileOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "MutateProfileOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/api/MutateProfileOperation$MutateProfileException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/MutateProfileRequest;",
        "Lcom/google/api/services/plusi/model/MutateProfileResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static sErrorCodeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mProfileUpdates:Lcom/google/api/services/plusi/model/SimpleProfile;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/api/services/plusi/model/SimpleProfile;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Lcom/google/api/services/plusi/model/SimpleProfile;

    const-string v3, "mutateprofile"

    invoke-static {}, Lcom/google/api/services/plusi/model/MutateProfileRequestJson;->getInstance()Lcom/google/api/services/plusi/model/MutateProfileRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/MutateProfileResponseJson;->getInstance()Lcom/google/api/services/plusi/model/MutateProfileResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->mProfileUpdates:Lcom/google/api/services/plusi/model/SimpleProfile;

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)I
    .locals 3
    .param p0    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "DATA_TOO_LARGE"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "INVALID_DATE"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "NAME_VIOLATION"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "INVALID_PUBLIC_USERNAME"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "NAME_CHANGE_THROTTLED"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "INVALID_CHAR"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "INCLUDES_NICKNAME"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "HARD_NAME_VIOLATION"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "HARD_INVALID_CHAR"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "HARD_INCLUDES_NICKNAME"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "HARD_INVALID_NICKNAME"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "TAGLINE_HARD_INVALID_CHAR"

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "INVALID_NICKNAME"

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "INVALID_WEBSITE"

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    const-string v1, "INVALID_BIRTHDAY"

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->sErrorCodeMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/MutateProfileResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/MutateProfileResponse;->updatedProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/api/MutateProfileOperation$MutateProfileException;

    invoke-direct {v0, p1}, Lcom/google/android/apps/plus/api/MutateProfileOperation$MutateProfileException;-><init>(Lcom/google/api/services/plusi/model/MutateProfileResponse;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->insertProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/SimpleProfile;)V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/MutateProfileResponse;->fbsVersionInfo:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/api/FbsVersionInfoTracker;->setVersionInfo(Ljava/lang/String;)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/MutateProfileRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MutateProfileOperation;->mProfileUpdates:Lcom/google/api/services/plusi/model/SimpleProfile;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MutateProfileRequest;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MutateProfileRequest;->includeAclData:Ljava/lang/Boolean;

    return-void
.end method
