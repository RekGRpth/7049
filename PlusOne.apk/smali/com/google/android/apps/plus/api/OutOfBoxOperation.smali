.class public final Lcom/google/android/apps/plus/api/OutOfBoxOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "OutOfBoxOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;",
        "Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

.field private mResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;
    .param p4    # Landroid/content/Intent;
    .param p5    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const/4 v6, 0x0

    const-string v3, "mobileoutofboxflow"

    invoke-static {}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;->getInstance()Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;->getInstance()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    return-void
.end method


# virtual methods
.method public final getResponse()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->mResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    iput-object p1, p0, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->mResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    const-string v0, "NATIVE_ANDROID"

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->clientType:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->upgradeOrigin:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->upgradeOrigin:Ljava/lang/String;

    :goto_0
    iput-object v0, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->upgradeOrigin:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->input:Ljava/util/List;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->input:Ljava/util/List;

    return-void

    :cond_0
    const-string v0, "DEFAULT"

    goto :goto_0
.end method
