.class public final Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "PhotosNameTagApprovalOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PhotosNameTagApprovalRequest;",
        "Lcom/google/api/services/plusi/model/PhotosNameTagApprovalResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mApprove:Z

.field private final mOwnerId:Ljava/lang/String;

.field private final mPhotoId:J

.field private final mShapeId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;JZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # J
    .param p8    # Z
    .param p9    # Landroid/content/Intent;
    .param p10    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v4, "photosnametagapproval"

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosNameTagApprovalRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosNameTagApprovalRequestJson;

    move-result-object v5

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosNameTagApprovalResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosNameTagApprovalResponseJson;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;->mOwnerId:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;->mPhotoId:J

    iput-wide p6, p0, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;->mShapeId:J

    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;->mApprove:Z

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/PhotosNameTagApprovalResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;->onStartResultProcessing()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PhotosNameTagApprovalResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;->mPhotoId:J

    iget-wide v4, p0, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;->mShapeId:J

    iget-boolean v6, p0, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;->mApprove:Z

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePhotoShapeApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JJZ)V

    :cond_0
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/PhotosNameTagApprovalRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;->mOwnerId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosNameTagApprovalRequest;->obfuscatedOwnerId:Ljava/lang/String;

    iget-wide v0, p0, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;->mPhotoId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosNameTagApprovalRequest;->photoId:Ljava/lang/Long;

    iget-wide v0, p0, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;->mShapeId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosNameTagApprovalRequest;->shapeId:Ljava/lang/Long;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;->mApprove:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosNameTagApprovalRequest;->approve:Ljava/lang/Boolean;

    return-void
.end method
