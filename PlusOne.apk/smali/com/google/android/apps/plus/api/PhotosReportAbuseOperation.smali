.class public final Lcom/google/android/apps/plus/api/PhotosReportAbuseOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "PhotosReportAbuseOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;",
        "Lcom/google/api/services/plusi/model/ReportAbusePhotoResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mOwnerId:Ljava/lang/String;

.field private final mPhotoId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Landroid/content/Intent;
    .param p7    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "reportabusephoto"

    invoke-static {}, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequestJson;->getInstance()Lcom/google/api/services/plusi/model/ReportAbusePhotoRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/ReportAbusePhotoResponseJson;->getInstance()Lcom/google/api/services/plusi/model/ReportAbusePhotoResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-wide p3, p0, Lcom/google/android/apps/plus/api/PhotosReportAbuseOperation;->mPhotoId:J

    iput-object p5, p0, Lcom/google/android/apps/plus/api/PhotosReportAbuseOperation;->mOwnerId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;

    iget-wide v0, p0, Lcom/google/android/apps/plus/api/PhotosReportAbuseOperation;->mPhotoId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;->photoId:Ljava/lang/Long;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosReportAbuseOperation;->mOwnerId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;->ownerId:Ljava/lang/String;

    new-instance v0, Lcom/google/api/services/plusi/model/DataAbuseReport;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataAbuseReport;-><init>()V

    const-string v1, "SPAM"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataAbuseReport;->abuseType:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;->abuseReport:Lcom/google/api/services/plusi/model/DataAbuseReport;

    return-void
.end method
