.class public final Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "PhotosTagSuggestionApprovalOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;",
        "Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mApprove:Z

.field private final mOwnerId:Ljava/lang/String;

.field private final mPhotoId:Ljava/lang/String;

.field private final mShapeId:Ljava/lang/String;

.field private final mTaggeeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Landroid/content/Intent;
    .param p9    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "photosnametagsuggestionapproval"

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->mOwnerId:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->mApprove:Z

    iput-object p5, p0, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->mPhotoId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->mShapeId:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->mTaggeeId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->onStartResultProcessing()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->mPhotoId:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->mShapeId:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-boolean v6, p0, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->mApprove:Z

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePhotoShapeApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JJZ)V

    :cond_0
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->mOwnerId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;->ownerId:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->mApprove:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;->approve:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->mPhotoId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;->photoId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->mShapeId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;->shapeId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;->mTaggeeId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;->taggeeId:Ljava/lang/String;

    return-void
.end method
