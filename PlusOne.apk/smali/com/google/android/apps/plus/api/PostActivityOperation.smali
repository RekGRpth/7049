.class public final Lcom/google/android/apps/plus/api/PostActivityOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "PostActivityOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PostActivityRequest;",
        "Lcom/google/api/services/plusi/model/PostActivityResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivity:Lcom/google/android/apps/plus/api/ApiaryActivity;

.field private final mAlbumOwnerId:Ljava/lang/String;

.field private final mAlbumTitle:Ljava/lang/String;

.field private final mApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

.field private final mAttachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private final mAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private final mBirthdayData:Lcom/google/android/apps/plus/api/BirthdayData;

.field private final mContent:Ljava/lang/String;

.field private final mDeepLinkId:Ljava/lang/String;

.field private final mEmotiShare:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

.field private final mExternalId:Ljava/lang/String;

.field private final mLocation:Lcom/google/android/apps/plus/content/DbLocation;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private mPostedAttachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private final mSaveAcl:Z

.field private final mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

.field private final mTargetAlbumId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/api/ApiaryActivity;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/api/ApiaryApiInfo;Ljava/lang/String;ZLcom/google/android/apps/plus/api/BirthdayData;Lcom/google/android/apps/plus/content/DbEmbedEmotishare;Lcom/google/android/apps/plus/content/DbEmbedSquare;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Lcom/google/android/apps/plus/api/ApiaryActivity;
    .param p6    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Lcom/google/android/apps/plus/content/DbLocation;
    .param p10    # Lcom/google/android/apps/plus/content/AudienceData;
    .param p11    # Lcom/google/android/apps/plus/api/ApiaryApiInfo;
    .param p12    # Ljava/lang/String;
    .param p13    # Z
    .param p14    # Lcom/google/android/apps/plus/api/BirthdayData;
    .param p15    # Lcom/google/android/apps/plus/content/DbEmbedEmotishare;
    .param p16    # Lcom/google/android/apps/plus/content/DbEmbedSquare;
    .param p17    # Ljava/lang/String;
    .param p18    # Ljava/lang/String;
    .param p19    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            "Lcom/google/android/apps/plus/api/ApiaryActivity;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/DbLocation;",
            "Lcom/google/android/apps/plus/content/AudienceData;",
            "Lcom/google/android/apps/plus/api/ApiaryApiInfo;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/google/android/apps/plus/api/BirthdayData;",
            "Lcom/google/android/apps/plus/content/DbEmbedEmotishare;",
            "Lcom/google/android/apps/plus/content/DbEmbedSquare;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v8, "postactivity"

    invoke-static {}, Lcom/google/api/services/plusi/model/PostActivityRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PostActivityRequestJson;

    move-result-object v9

    invoke-static {}, Lcom/google/api/services/plusi/model/PostActivityResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PostActivityResponseJson;

    move-result-object v7

    new-instance v1, Lcom/google/android/apps/plus/network/PlatformHttpRequestConfiguration;

    const-string v4, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite https://www.googleapis.com/auth/plus.native"

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->PLUS_BACKEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v5

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v6, p11

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/network/PlatformHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v8

    move-object v6, v9

    move-object v8, p3

    move-object v9, p4

    move-object v10, v1

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;)V

    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mActivity:Lcom/google/android/apps/plus/api/ApiaryActivity;

    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContent:Ljava/lang/String;

    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAttachments:Ljava/util/List;

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mExternalId:Ljava/lang/String;

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mPackageManager:Landroid/content/pm/PackageManager;

    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mDeepLinkId:Ljava/lang/String;

    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mSaveAcl:Z

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mBirthdayData:Lcom/google/android/apps/plus/api/BirthdayData;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAlbumTitle:Ljava/lang/String;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mTargetAlbumId:Ljava/lang/String;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAlbumOwnerId:Ljava/lang/String;

    return-void
.end method

.method private static getFingerprint(Landroid/content/Context;J)Lcom/android/gallery3d/common/Fingerprint;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # J

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "fingerprint"

    aput-object v0, v2, v4

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_0

    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v3, Lcom/android/gallery3d/common/Fingerprint;

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/android/gallery3d/common/Fingerprint;-><init>([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getFingerprint(Landroid/content/Context;Landroid/net/Uri;)Lcom/android/gallery3d/common/Fingerprint;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {p0}, Lcom/google/android/picasastore/PicasaStoreFacade;->get(Landroid/content/Context;)Lcom/google/android/picasastore/PicasaStoreFacade;

    move-result-object v0

    invoke-virtual {v0, v5, v4}, Lcom/google/android/picasastore/PicasaStoreFacade;->getFingerprintUri(ZZ)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_0

    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    new-instance v3, Lcom/android/gallery3d/common/Fingerprint;

    invoke-direct {v3, v6}, Lcom/android/gallery3d/common/Fingerprint;-><init>([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getPhotoIdFromStream(Landroid/content/Context;Ljava/lang/String;)J
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_STREAM_ID_AND_OWNER_ID_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v7

    invoke-virtual {v7, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-static {v9, v6}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "photo_id"

    aput-object v0, v2, v5

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_1

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v3

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_0
    return-wide v3

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    const-wide/16 v3, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private getPhotosShareData(Ljava/util/List;Ljava/lang/String;)Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;
    .locals 21
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;"
        }
    .end annotation

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v15, 0x0

    :cond_0
    :goto_0
    return-object v15

    :cond_1
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    new-instance v14, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAttachments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v14, v1}, Ljava/util/HashSet;-><init>(I)V

    new-instance v10, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAttachments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v10, v1}, Ljava/util/HashSet;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v16

    const/16 v17, 0x0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1a

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v1, 0x4

    move/from16 v0, v17

    if-ge v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mPostedAttachments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_3

    const/4 v8, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mPostedAttachments:Ljava/util/List;

    move-object/from16 v20, v0

    new-instance v12, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;

    invoke-direct {v12}, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;-><init>()V

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    if-ne v1, v2, :cond_4

    const-string v1, "2"

    :goto_3
    iput-object v1, v12, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;->mediaType:Ljava/lang/String;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->hasLocalUri()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v1

    move-object/from16 v0, v19

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/api/PostActivityOperation;->getFingerprint(Landroid/content/Context;Landroid/net/Uri;)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v18

    if-nez v18, :cond_5

    const-string v2, "HttpTransaction"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not determine fingerprint for media: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v12, 0x0

    :goto_4
    if-eqz v12, :cond_2

    invoke-interface {v13, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v12, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;->imageData:Ljava/lang/String;

    if-eqz v1, :cond_2

    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    :cond_3
    const/4 v8, 0x0

    goto :goto_2

    :cond_4
    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    sget-object v1, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->PANORAMA:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    const-string v1, "1"

    goto :goto_3

    :cond_5
    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContext:Landroid/content/Context;

    invoke-static {v3, v1, v2}, Lcom/google/android/apps/plus/api/PostActivityOperation;->getFingerprint(Landroid/content/Context;J)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v1

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/common/Fingerprint;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContext:Landroid/content/Context;

    invoke-virtual/range {v18 .. v18}, Lcom/android/gallery3d/common/Fingerprint;->toStreamId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/api/PostActivityOperation;->getPhotoIdFromStream(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v3

    const-string v2, "HttpTransaction"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Fingerprint mismatch; old: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/android/gallery3d/common/Fingerprint;->toStreamId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", new: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {v18 .. v18}, Lcom/android/gallery3d/common/Fingerprint;->toStreamId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v5, Lcom/google/android/apps/plus/api/PostActivityOperation$1;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v1, v2}, Lcom/google/android/apps/plus/api/PostActivityOperation$1;-><init>(Lcom/google/android/apps/plus/api/PostActivityOperation;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {v5}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    new-instance v1, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    :goto_5
    move-object v9, v1

    move-object/from16 v1, v18

    :goto_6
    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v14, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v1, "HttpTransaction"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Duplicate server reference found; "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const/4 v12, 0x0

    goto/16 :goto_4

    :cond_7
    move-object/from16 v1, v18

    goto :goto_6

    :cond_8
    const-string v1, "HttpTransaction"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Looking for remote photo w/ CAID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Lcom/android/gallery3d/common/Fingerprint;->toStreamId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContext:Landroid/content/Context;

    invoke-virtual/range {v18 .. v18}, Lcom/android/gallery3d/common/Fingerprint;->toStreamId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/api/PostActivityOperation;->getPhotoIdFromStream(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v1, 0x0

    cmp-long v1, v3, v1

    if-eqz v1, :cond_b

    const-string v1, "HttpTransaction"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Found remote photo; ID: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " matches CAID: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v18 .. v18}, Lcom/android/gallery3d/common/Fingerprint;->toStreamId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    move-object v9, v1

    move-object/from16 v1, v18

    goto/16 :goto_6

    :cond_b
    move-object v1, v9

    goto/16 :goto_5

    :cond_c
    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v1

    if-eqz v1, :cond_d

    const/4 v1, 0x0

    goto/16 :goto_6

    :cond_d
    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No photo ID or local Uri for attachment: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v12, 0x0

    goto/16 :goto_4

    :cond_e
    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/android/gallery3d/common/Fingerprint;->toStreamId()Ljava/lang/String;

    move-result-object v2

    :goto_7
    if-eqz v2, :cond_11

    invoke-interface {v10, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    const-string v1, "HttpTransaction"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Duplicate CAID found; "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    const/4 v12, 0x0

    goto/16 :goto_4

    :cond_10
    const/4 v2, 0x0

    goto :goto_7

    :cond_11
    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v3

    if-nez v3, :cond_19

    if-eqz v2, :cond_12

    iput-object v2, v12, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;->clientAssignedUniqueId:Ljava/lang/String;

    invoke-interface {v10, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_12
    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->getMimeType(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    if-eqz v8, :cond_15

    const-string v3, "image/jpeg"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    :goto_8
    if-eqz v2, :cond_17

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v1

    const/16 v3, 0x140

    const/16 v4, 0x140

    move-object/from16 v0, v19

    invoke-static {v0, v1, v3, v4}, Lcom/google/android/apps/plus/content/EsPhotosData;->loadLocalBitmap(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_16

    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bitmap decoding failed for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_9
    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_13
    const-string v3, "image/png"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    goto :goto_8

    :cond_14
    const/4 v2, 0x0

    goto :goto_8

    :cond_15
    const/4 v2, 0x0

    goto :goto_8

    :cond_16
    const/16 v3, 0x55

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/util/ImageUtils;->compressBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;IZ)[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v12, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;->imageData:Ljava/lang/String;

    const-string v1, "1"

    iput-object v1, v12, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;->imageStatus:Ljava/lang/String;

    goto :goto_9

    :cond_17
    if-nez v1, :cond_18

    const/4 v12, 0x0

    goto/16 :goto_4

    :cond_18
    const-string v1, "2"

    iput-object v1, v12, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;->imageStatus:Ljava/lang/String;

    goto :goto_9

    :cond_19
    new-instance v1, Lcom/google/api/services/plusi/model/PhotoServiceMediaReferencePhoto;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/PhotoServiceMediaReferencePhoto;-><init>()V

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PhotoServiceMediaReferencePhoto;->obfuscatedOwnerId:Ljava/lang/String;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PhotoServiceMediaReferencePhoto;->photoId:Ljava/lang/String;

    iput-object v1, v12, Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;->sourcePhoto:Lcom/google/api/services/plusi/model/PhotoServiceMediaReferencePhoto;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v14, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_1a
    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1b

    const/4 v15, 0x0

    goto/16 :goto_0

    :cond_1b
    new-instance v15, Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;

    invoke-direct {v15}, Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;-><init>()V

    iput-object v13, v15, Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;->mediaRef:Ljava/util/List;

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p2

    iput-object v0, v15, Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;->albumTitle:Ljava/lang/String;

    goto/16 :goto_0
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 12
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/PostActivityResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Stream;->update:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Landroid/content/ContentValues;

    const/4 v0, 0x4

    invoke-direct {v4, v0}, Landroid/content/ContentValues;-><init>(I)V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/Update;

    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    const-string v6, "upload_account"

    invoke-virtual {v4, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, v0, Lcom/google/api/services/plusi/model/Update;->albumId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mTargetAlbumId:Ljava/lang/String;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mTargetAlbumId:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumAudienceFromUpdate(Lcom/google/api/services/plusi/model/Update;)Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v7, v8, v6, v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updateAlbumAudience(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/InstantUpload;->isEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "0"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "album_id"

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mPostedAttachments:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mPostedAttachments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->hasLocalUri()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    const-string v7, "HttpTransaction"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "HttpTransaction"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "  -- on-demand upload; img: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->getMediaId(Landroid/content/ContentResolver;Landroid/net/Uri;)J

    move-result-wide v7

    const-wide/16 v9, 0x0

    cmp-long v9, v7, v9

    if-ltz v9, :cond_6

    const-string v9, "HttpTransaction"

    const/4 v10, 0x4

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_5

    const-string v9, "HttpTransaction"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "  -- on-demand upload; media ID "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    const-string v9, "media_id"

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v9, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_6
    const-string v7, "media_url"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->UPLOADS_URI:Landroid/net/Uri;

    invoke-virtual {v1, v0, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0

    :cond_7
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v0, v1, v2, v4, v5}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v0

    if-lez v0, :cond_a

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargets()[Lcom/google/android/apps/plus/content/SquareTargetData;

    move-result-object v2

    array-length v4, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_8

    aget-object v5, v2, v0

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareStreamId()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Lcom/google/android/apps/plus/content/EsPostsData;->buildSquareStreamKey(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareId()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/google/android/apps/plus/content/EsPostsData;->buildSquareStreamKey(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v4, "DEFAULT"

    invoke-static {v0, v2, v1, v3, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->insertMultiStreamActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/Collection;Ljava/util/List;Ljava/lang/String;)V

    :goto_2
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityResponse;->shareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostActivityResponse;->shareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->savePostingPreferences(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/ShareboxSettings;)V

    :cond_9
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->updateAudienceHistory(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;)V

    return-void

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v4, "DEFAULT"

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivitiesAndOverwrite(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Z)V

    goto :goto_2

    :cond_b
    invoke-static {}, Lcom/google/android/apps/plus/content/EsAccountsData;->hadSharingRoster()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/content/AudienceData;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/DbAudienceData;->serialize(Lcom/google/android/apps/plus/content/AudienceData;)[B

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveAudience(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    const-string v0, "HttpTransaction"

    const-string v1, "Error saving default audience"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const/4 v7, 0x1

    const/4 v1, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/PostActivityRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mContent:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->updateText:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mExternalId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->externalId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Mobile"

    const-string v3, "com.google.android.apps.social"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    new-instance v2, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestAttribution;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestAttribution;-><init>()V

    iput-object v0, v2, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestAttribution;->androidAppName:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->attribution:Lcom/google/api/services/plusi/model/RequestsPostActivityRequestAttribution;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mActivity:Lcom/google/android/apps/plus/api/ApiaryActivity;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mActivity:Lcom/google/android/apps/plus/api/ApiaryActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ApiaryActivity;->getMediaJson()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->mediaJson:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mActivity:Lcom/google/android/apps/plus/api/ApiaryActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mDeepLinkId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/api/ApiaryActivity;->getEmbed(Ljava/lang/String;)Lcom/google/api/services/plusi/model/EmbedClientItem;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAttachments:Ljava/util/List;

    if-eqz v0, :cond_6

    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAttachments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mPostedAttachments:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAttachments:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAlbumTitle:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/plus/api/PostActivityOperation;->getPhotosShareData(Ljava/util/List;Ljava/lang/String;)Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;

    move-result-object v0

    if-eqz v0, :cond_3

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->photosShareData:Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbLocation;->toProtocolObject()Lcom/google/api/services/plusi/model/Location;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->location:Lcom/google/api/services/plusi/model/Location;

    :cond_4
    invoke-static {}, Lcom/google/android/apps/plus/util/ResourceRedirector;->getInstance()Lcom/google/android/apps/plus/util/ResourceRedirector;

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_EMOTISHARE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->createEmbed()Lcom/google/api/services/plusi/model/EmbedClientItem;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->convertAudienceToSharingRoster(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/api/services/plusi/model/SharingRoster;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mSaveAcl:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->saveDefaultAcl:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v0

    if-lez v0, :cond_7

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->squareStreams:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargets()[Lcom/google/android/apps/plus/content/SquareTargetData;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_7

    aget-object v4, v2, v0

    new-instance v5, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestSquareStreamInfo;

    invoke-direct {v5}, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestSquareStreamInfo;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareId()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestSquareStreamInfo;->squareId:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareStreamId()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v5, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestSquareStreamInfo;->streamId:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->squareStreams:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mTargetAlbumId:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mTargetAlbumId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mAlbumOwnerId:Ljava/lang/String;

    new-instance v3, Lcom/google/api/services/plusi/model/PhotoServiceShareActionDataAlbum;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/PhotoServiceShareActionDataAlbum;-><init>()V

    iput-object v0, v3, Lcom/google/api/services/plusi/model/PhotoServiceShareActionDataAlbum;->albumId:Ljava/lang/String;

    iput-object v2, v3, Lcom/google/api/services/plusi/model/PhotoServiceShareActionDataAlbum;->obfuscatedOwnerId:Ljava/lang/String;

    new-instance v0, Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;-><init>()V

    iput-object v3, v0, Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;->targetAlbum:Lcom/google/api/services/plusi/model/PhotoServiceShareActionDataAlbum;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;->isFullAlbumShare:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->photosShareData:Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;

    goto/16 :goto_1

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mBirthdayData:Lcom/google/android/apps/plus/api/BirthdayData;

    if-eqz v0, :cond_8

    new-instance v0, Lcom/google/api/services/plusi/model/UpdateMetadata;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/UpdateMetadata;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->updateMetadata:Lcom/google/api/services/plusi/model/UpdateMetadata;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->updateMetadata:Lcom/google/api/services/plusi/model/UpdateMetadata;

    const-string v2, "BIRTHDAY"

    iput-object v2, v0, Lcom/google/api/services/plusi/model/UpdateMetadata;->namespace:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->updateMetadata:Lcom/google/api/services/plusi/model/UpdateMetadata;

    new-instance v2, Lcom/google/api/services/plusi/model/NamespaceSpecificData;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/NamespaceSpecificData;-><init>()V

    iput-object v2, v0, Lcom/google/api/services/plusi/model/UpdateMetadata;->namespaceSpecificData:Lcom/google/api/services/plusi/model/NamespaceSpecificData;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->updateMetadata:Lcom/google/api/services/plusi/model/UpdateMetadata;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/UpdateMetadata;->namespaceSpecificData:Lcom/google/api/services/plusi/model/NamespaceSpecificData;

    new-instance v2, Lcom/google/api/services/plusi/model/BirthdayData;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/BirthdayData;-><init>()V

    iput-object v2, v0, Lcom/google/api/services/plusi/model/NamespaceSpecificData;->birthdayData:Lcom/google/api/services/plusi/model/BirthdayData;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->updateMetadata:Lcom/google/api/services/plusi/model/UpdateMetadata;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/UpdateMetadata;->namespaceSpecificData:Lcom/google/api/services/plusi/model/NamespaceSpecificData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/NamespaceSpecificData;->birthdayData:Lcom/google/api/services/plusi/model/BirthdayData;

    new-instance v2, Lcom/google/api/services/plusi/model/CommonPerson;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/CommonPerson;-><init>()V

    iput-object v2, v0, Lcom/google/api/services/plusi/model/BirthdayData;->person:Lcom/google/api/services/plusi/model/CommonPerson;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->updateMetadata:Lcom/google/api/services/plusi/model/UpdateMetadata;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/UpdateMetadata;->namespaceSpecificData:Lcom/google/api/services/plusi/model/NamespaceSpecificData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/NamespaceSpecificData;->birthdayData:Lcom/google/api/services/plusi/model/BirthdayData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/BirthdayData;->person:Lcom/google/api/services/plusi/model/CommonPerson;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mBirthdayData:Lcom/google/android/apps/plus/api/BirthdayData;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/BirthdayData;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/api/services/plusi/model/CommonPerson;->obfuscatedId:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->updateMetadata:Lcom/google/api/services/plusi/model/UpdateMetadata;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/UpdateMetadata;->namespaceSpecificData:Lcom/google/api/services/plusi/model/NamespaceSpecificData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/NamespaceSpecificData;->birthdayData:Lcom/google/api/services/plusi/model/BirthdayData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/BirthdayData;->person:Lcom/google/api/services/plusi/model/CommonPerson;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mBirthdayData:Lcom/google/android/apps/plus/api/BirthdayData;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/BirthdayData;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/api/services/plusi/model/CommonPerson;->userName:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->updateMetadata:Lcom/google/api/services/plusi/model/UpdateMetadata;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/UpdateMetadata;->namespaceSpecificData:Lcom/google/api/services/plusi/model/NamespaceSpecificData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/NamespaceSpecificData;->birthdayData:Lcom/google/api/services/plusi/model/BirthdayData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/BirthdayData;->person:Lcom/google/api/services/plusi/model/CommonPerson;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, Lcom/google/api/services/plusi/model/CommonPerson;->isContactSafe:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->updateMetadata:Lcom/google/api/services/plusi/model/UpdateMetadata;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/UpdateMetadata;->namespaceSpecificData:Lcom/google/api/services/plusi/model/NamespaceSpecificData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/NamespaceSpecificData;->birthdayData:Lcom/google/api/services/plusi/model/BirthdayData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/BirthdayData;->person:Lcom/google/api/services/plusi/model/CommonPerson;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/CommonPerson;->isViewerFollowing:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->updateMetadata:Lcom/google/api/services/plusi/model/UpdateMetadata;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/UpdateMetadata;->namespaceSpecificData:Lcom/google/api/services/plusi/model/NamespaceSpecificData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/NamespaceSpecificData;->birthdayData:Lcom/google/api/services/plusi/model/BirthdayData;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mBirthdayData:Lcom/google/android/apps/plus/api/BirthdayData;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/BirthdayData;->getYear()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/BirthdayData;->year:Ljava/lang/Integer;

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    if-eqz v0, :cond_9

    new-instance v0, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestCreateEmbedRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestCreateEmbedRequest;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->createEmbedRequest:Lcom/google/api/services/plusi/model/RequestsPostActivityRequestCreateEmbedRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->isInvitation()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->createEmbedRequest:Lcom/google/api/services/plusi/model/RequestsPostActivityRequestCreateEmbedRequest;

    new-instance v1, Lcom/google/api/services/plusi/model/CreateSquareInviteEmbed;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/CreateSquareInviteEmbed;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestCreateEmbedRequest;->createSquareInviteEmbed:Lcom/google/api/services/plusi/model/CreateSquareInviteEmbed;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->createEmbedRequest:Lcom/google/api/services/plusi/model/RequestsPostActivityRequestCreateEmbedRequest;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestCreateEmbedRequest;->createSquareInviteEmbed:Lcom/google/api/services/plusi/model/CreateSquareInviteEmbed;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->getSquareId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/CreateSquareInviteEmbed;->communityId:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->disableReshares:Ljava/lang/Boolean;

    :cond_9
    :goto_3
    return-void

    :cond_a
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->createEmbedRequest:Lcom/google/api/services/plusi/model/RequestsPostActivityRequestCreateEmbedRequest;

    new-instance v1, Lcom/google/api/services/plusi/model/CreateSquareEmbed;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/CreateSquareEmbed;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestCreateEmbedRequest;->createSquareEmbed:Lcom/google/api/services/plusi/model/CreateSquareEmbed;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->createEmbedRequest:Lcom/google/api/services/plusi/model/RequestsPostActivityRequestCreateEmbedRequest;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestCreateEmbedRequest;->createSquareEmbed:Lcom/google/api/services/plusi/model/CreateSquareEmbed;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PostActivityOperation;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->getSquareId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/CreateSquareEmbed;->communityId:Ljava/lang/String;

    goto :goto_3

    :catch_0
    move-exception v2

    goto/16 :goto_0
.end method
