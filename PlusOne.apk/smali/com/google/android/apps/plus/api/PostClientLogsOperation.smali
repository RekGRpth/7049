.class public final Lcom/google/android/apps/plus/api/PostClientLogsOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "PostClientLogsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PostClientLogsRequest;",
        "Lcom/google/api/services/plusi/model/PostClientLogsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mClientOzExtension:Lcom/google/api/services/plusi/model/ClientOzExtension;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "postclientlogs"

    invoke-static {}, Lcom/google/api/services/plusi/model/PostClientLogsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PostClientLogsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/PostClientLogsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PostClientLogsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createClientOzExtension(Landroid/content/Context;)Lcom/google/api/services/plusi/model/ClientOzExtension;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->mClientOzExtension:Lcom/google/api/services/plusi/model/ClientOzExtension;

    return-void
.end method


# virtual methods
.method public final getClientOzEvents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientOzEvent;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->mClientOzExtension:Lcom/google/api/services/plusi/model/ClientOzExtension;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->mClientOzExtension:Lcom/google/api/services/plusi/model/ClientOzExtension;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ClientOzExtension;->clientEvent:Ljava/util/List;

    goto :goto_0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->onStartResultProcessing()V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/PostClientLogsRequest;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostClientLogsRequest;->enableTracing:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->mClientOzExtension:Lcom/google/api/services/plusi/model/ClientOzExtension;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostClientLogsRequest;->clientLog:Lcom/google/api/services/plusi/model/ClientOzExtension;

    return-void
.end method

.method public final setClientOzEvents(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientOzEvent;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->mClientOzExtension:Lcom/google/api/services/plusi/model/ClientOzExtension;

    iput-object p1, v0, Lcom/google/api/services/plusi/model/ClientOzExtension;->clientEvent:Ljava/util/List;

    return-void
.end method
