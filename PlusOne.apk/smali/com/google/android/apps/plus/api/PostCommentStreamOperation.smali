.class public final Lcom/google/android/apps/plus/api/PostCommentStreamOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "PostCommentStreamOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PostCommentRequest;",
        "Lcom/google/api/services/plusi/model/PostCommentResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final sRandom:Ljava/util/Random;


# instance fields
.field private final mActivityId:Ljava/lang/String;

.field private final mContent:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;->sRandom:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    const-string v3, "postcomment"

    invoke-static {}, Lcom/google/api/services/plusi/model/PostCommentRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PostCommentRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/PostCommentResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PostCommentResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;->mActivityId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;->mContent:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/PostCommentResponse;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostCommentResponse;->comment:Lcom/google/api/services/plusi/model/Comment;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;->mActivityId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Comment;->updateId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Comment;->updateId:Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/content/EsPostsData;->insertComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/Comment;)V

    :cond_0
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/PostCommentRequest;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;->sRandom:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->clientId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;->mActivityId:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->activityId:Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->creationTimeMs:Ljava/lang/Long;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;->mContent:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->commentText:Ljava/lang/String;

    return-void
.end method
