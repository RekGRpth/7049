.class public final Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "ReadSquareMembersOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;",
        "Lcom/google/api/services/plusi/model/ReadSquareMembersOzResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final ADMIN_MEMBER_TYPES:[Ljava/lang/String;

.field private static final NON_ADMIN_MEMBER_TYPES:[Ljava/lang/String;


# instance fields
.field private final mContinuationToken:Ljava/lang/String;

.field private final mMemberLimit:I

.field private final mMemberListType:I

.field private final mSquareId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "MODERATOR"

    aput-object v1, v0, v2

    const-string v1, "MEMBER"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->NON_ADMIN_MEMBER_TYPES:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "MODERATOR"

    aput-object v1, v0, v2

    const-string v1, "MEMBER"

    aput-object v1, v0, v3

    const-string v1, "PENDING"

    aput-object v1, v0, v4

    const/4 v1, 0x3

    const-string v2, "BANNED"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "INVITED"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->ADMIN_MEMBER_TYPES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ILjava/lang/String;ILandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p6    # I
    .param p7    # Landroid/content/Intent;
    .param p8    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "readsquaremembers"

    invoke-static {}, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequestJson;->getInstance()Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/ReadSquareMembersOzResponseJson;->getInstance()Lcom/google/api/services/plusi/model/ReadSquareMembersOzResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mSquareId:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mMemberListType:I

    iput-object p5, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mContinuationToken:Ljava/lang/String;

    const/16 v0, 0x64

    invoke-static {p6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mMemberLimit:I

    return-void
.end method

.method private createQuery(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plusi/model/MemberListQuery;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/api/services/plusi/model/MemberListQuery;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/MemberListQuery;-><init>()V

    iput-object p1, v0, Lcom/google/api/services/plusi/model/MemberListQuery;->membershipStatus:Ljava/lang/String;

    iput-object p2, v0, Lcom/google/api/services/plusi/model/MemberListQuery;->continuationToken:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mMemberLimit:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/MemberListQuery;->pageLimit:Ljava/lang/Integer;

    return-object v0
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzResponse;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzResponse;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsSquaresData;->insertSquare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/ViewerSquare;)Z

    :cond_0
    iget-object v0, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzResponse;->memberList:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mContinuationToken:Ljava/lang/String;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mSquareId:Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/content/EsSquaresData;->replaceSquareMembers(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)I

    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mSquareId:Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/content/EsSquaresData;->addSquareMembers(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)I

    goto :goto_0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const/4 v5, 0x0

    const/4 v0, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mSquareId:Ljava/lang/String;

    iput-object v1, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;->obfuscatedSquareId:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;->memberListQuery:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mMemberListType:I

    packed-switch v1, :pswitch_data_0

    sget-object v1, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->NON_ADMIN_MEMBER_TYPES:[Ljava/lang/String;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    iget-object v4, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;->memberListQuery:Ljava/util/List;

    invoke-direct {p0, v3, v5}, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->createQuery(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plusi/model/MemberListQuery;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_0
    iget-object v0, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;->memberListQuery:Ljava/util/List;

    const-string v1, "BANNED"

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mContinuationToken:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->createQuery(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plusi/model/MemberListQuery;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    return-void

    :pswitch_1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;->memberListQuery:Ljava/util/List;

    const-string v1, "INVITED"

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mContinuationToken:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->createQuery(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plusi/model/MemberListQuery;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    iget-object v0, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;->memberListQuery:Ljava/util/List;

    const-string v1, "PENDING"

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mContinuationToken:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->createQuery(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plusi/model/MemberListQuery;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_3
    iget-object v0, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;->memberListQuery:Ljava/util/List;

    const-string v1, "MODERATOR"

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mContinuationToken:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->createQuery(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plusi/model/MemberListQuery;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_4
    iget-object v0, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;->memberListQuery:Ljava/util/List;

    const-string v1, "MEMBER"

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->mContinuationToken:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->createQuery(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plusi/model/MemberListQuery;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_5
    sget-object v1, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->ADMIN_MEMBER_TYPES:[Ljava/lang/String;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    iget-object v4, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;->memberListQuery:Ljava/util/List;

    invoke-direct {p0, v3, v5}, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->createQuery(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plusi/model/MemberListQuery;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
