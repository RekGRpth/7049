.class public final Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "RecordSuggestionActionOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/RecordSuggestionRequest;",
        "Lcom/google/api/services/plusi/model/RecordSuggestionResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mEvent:Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;
    .param p4    # Landroid/content/Intent;
    .param p5    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "recordsuggestion"

    invoke-static {}, Lcom/google/api/services/plusi/model/RecordSuggestionRequestJson;->getInstance()Lcom/google/api/services/plusi/model/RecordSuggestionRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/RecordSuggestionResponseJson;->getInstance()Lcom/google/api/services/plusi/model/RecordSuggestionResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;->mEvent:Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;JLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # J
    .param p9    # Landroid/content/Intent;
    .param p10    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "J",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            ")V"
        }
    .end annotation

    const-string v3, "recordsuggestion"

    invoke-static {}, Lcom/google/api/services/plusi/model/RecordSuggestionRequestJson;->getInstance()Lcom/google/api/services/plusi/model/RecordSuggestionRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/RecordSuggestionResponseJson;->getInstance()Lcom/google/api/services/plusi/model/RecordSuggestionResponseJson;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    new-instance v0, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;

    move-object v1, p6

    move-object v2, p4

    move-object v3, p5

    move-object v4, p3

    move-wide v5, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;J)V

    iput-object v0, p0, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;->mEvent:Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 9
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const/4 v8, 0x2

    const/4 v0, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;->mEvent:Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;

    if-eqz v1, :cond_2

    new-instance v1, Lcom/google/api/services/plusi/model/DataSuggestionAction;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/DataSuggestionAction;-><init>()V

    iput-object v1, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/DataSuggestionAction;->accepted:Ljava/lang/Boolean;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;->mEvent:Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;->getActionType()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/DataSuggestionAction;->actionType:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;->mEvent:Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;->getSuggestionUi()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestionUi:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestedEntityId:Ljava/util/List;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestionId:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;->mEvent:Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;->getPersonIds()Ljava/util/ArrayList;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;->mEvent:Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;->getSuggestionIds()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v3, :cond_2

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v1, v2, :cond_2

    move v2, v0

    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v5, Lcom/google/api/services/plusi/model/DataSuggestedEntityId;

    invoke-direct {v5}, Lcom/google/api/services/plusi/model/DataSuggestedEntityId;-><init>()V

    new-instance v6, Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/DataCircleMemberId;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v1, v5, Lcom/google/api/services/plusi/model/DataSuggestedEntityId;->suggestionId:Ljava/lang/String;

    if-eqz v7, :cond_1

    iput-object v7, v5, Lcom/google/api/services/plusi/model/DataSuggestedEntityId;->obfuscatedGaiaId:Ljava/lang/String;

    iput-object v7, v6, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestedEntityId:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestionId:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    const-string v1, "e:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/google/api/services/plusi/model/DataSuggestedEntityId;->email:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/api/services/plusi/model/DataCircleMemberId;->email:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestedEntityId:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestionId:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-void
.end method
