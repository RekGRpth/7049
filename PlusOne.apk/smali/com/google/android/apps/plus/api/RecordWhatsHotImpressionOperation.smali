.class public final Lcom/google/android/apps/plus/api/RecordWhatsHotImpressionOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "RecordWhatsHotImpressionOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/RecordFeatureHintActionRequest;",
        "Lcom/google/api/services/plusi/model/RecordFeatureHintActionResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mActivityId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/content/Intent;
    .param p5    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "recordfeaturehintaction"

    invoke-static {}, Lcom/google/api/services/plusi/model/RecordFeatureHintActionRequestJson;->getInstance()Lcom/google/api/services/plusi/model/RecordFeatureHintActionRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/RecordFeatureHintActionResponseJson;->getInstance()Lcom/google/api/services/plusi/model/RecordFeatureHintActionResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/RecordWhatsHotImpressionOperation;->mActivityId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method public final onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    const/16 v0, 0xc8

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/RecordWhatsHotImpressionOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/RecordWhatsHotImpressionOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/RecordWhatsHotImpressionOperation;->mActivityId:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->markDbWhatsHotViewed(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/api/PlusiOperation;->onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/RecordFeatureHintActionRequest;

    new-instance v0, Lcom/google/api/services/plusi/model/FeatureHintAction;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FeatureHintAction;-><init>()V

    const-string v1, "WHATS_HOT"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/FeatureHintAction;->featureHintType:Ljava/lang/String;

    const-string v1, "IMPRESSION"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/FeatureHintAction;->type:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p1, Lcom/google/api/services/plusi/model/RecordFeatureHintActionRequest;->featureHintAction:Ljava/util/List;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/RecordFeatureHintActionRequest;->featureHintAction:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/api/RecordWhatsHotImpressionOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/RecordWhatsHotImpressionOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/RecordWhatsHotImpressionOperation;->mActivityId:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->markDbWhatsHotViewed(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V

    return-void
.end method
