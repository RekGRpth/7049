.class public final Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "ReportProfileAbuseOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/ReportProfileRequest;",
        "Lcom/google/api/services/plusi/model/ReportProfileResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAbuseType:Ljava/lang/String;

.field private final mGaiaId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/content/Intent;
    .param p6    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const/4 v6, 0x0

    const-string v3, "reportprofile"

    invoke-static {}, Lcom/google/api/services/plusi/model/ReportProfileRequestJson;->getInstance()Lcom/google/api/services/plusi/model/ReportProfileRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/ReportProfileResponseJson;->getInstance()Lcom/google/api/services/plusi/model/ReportProfileResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;->mGaiaId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;->mAbuseType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/ReportProfileRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;->mGaiaId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/ReportProfileRequest;->ownerId:Ljava/lang/String;

    new-instance v0, Lcom/google/api/services/plusi/model/DataAbuseReport;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataAbuseReport;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/ReportProfileRequest;->abuseReport:Lcom/google/api/services/plusi/model/DataAbuseReport;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/ReportProfileRequest;->abuseReport:Lcom/google/api/services/plusi/model/DataAbuseReport;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;->mAbuseType:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataAbuseReport;->abuseType:Ljava/lang/String;

    return-void
.end method
