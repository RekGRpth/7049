.class public final Lcom/google/android/apps/plus/api/SaveLocationSharingSettingsOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "SaveLocationSharingSettingsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/MobileDeviceLocationAclsMutateRequest;",
        "Lcom/google/api/services/plusi/model/MobileDeviceLocationAclsMutateResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final mLocationAcl:Lcom/google/android/apps/plus/content/AudienceData;

.field final mLocationSharingEnabled:Z

.field final mLocationType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZILcom/google/android/apps/plus/content/AudienceData;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Z
    .param p4    # I
    .param p5    # Lcom/google/android/apps/plus/content/AudienceData;
    .param p6    # Landroid/content/Intent;
    .param p7    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "mutatedevicelocationacls"

    invoke-static {}, Lcom/google/api/services/plusi/model/MobileDeviceLocationAclsMutateRequestJson;->getInstance()Lcom/google/api/services/plusi/model/MobileDeviceLocationAclsMutateRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/MobileDeviceLocationAclsMutateResponseJson;->getInstance()Lcom/google/api/services/plusi/model/MobileDeviceLocationAclsMutateResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-boolean p3, p0, Lcom/google/android/apps/plus/api/SaveLocationSharingSettingsOperation;->mLocationSharingEnabled:Z

    iput p4, p0, Lcom/google/android/apps/plus/api/SaveLocationSharingSettingsOperation;->mLocationType:I

    iput-object p5, p0, Lcom/google/android/apps/plus/api/SaveLocationSharingSettingsOperation;->mLocationAcl:Lcom/google/android/apps/plus/content/AudienceData;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/MobileDeviceLocationAclsMutateRequest;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/SaveLocationSharingSettingsOperation;->mLocationSharingEnabled:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MobileDeviceLocationAclsMutateRequest;->clearAcl:Ljava/lang/Boolean;

    iget v0, p0, Lcom/google/android/apps/plus/api/SaveLocationSharingSettingsOperation;->mLocationType:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p1, Lcom/google/api/services/plusi/model/MobileDeviceLocationAclsMutateRequest;->type:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SaveLocationSharingSettingsOperation;->mLocationAcl:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SaveLocationSharingSettingsOperation;->mLocationAcl:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->convertAudienceToSharingRoster(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/api/services/plusi/model/SharingRoster;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MobileDeviceLocationAclsMutateRequest;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    const-string v0, "BEST_AVAILABLE"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
