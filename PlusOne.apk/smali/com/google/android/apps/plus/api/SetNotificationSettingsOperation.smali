.class public final Lcom/google/android/apps/plus/api/SetNotificationSettingsOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "SetNotificationSettingsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/SettingsSetRequest;",
        "Lcom/google/api/services/plusi/model/SettingsSetResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/NotificationSettingsData;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/content/NotificationSettingsData;
    .param p4    # Landroid/content/Intent;
    .param p5    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "settingsset"

    invoke-static {}, Lcom/google/api/services/plusi/model/SettingsSetRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SettingsSetRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SettingsSetResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SettingsSetResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/SetNotificationSettingsOperation;->mSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/SettingsSetResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SettingsSetResponse;->success:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SettingsSetResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/api/ProtocolException;

    const-string v1, "SettingsSetRequest failed"

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/api/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const/4 v1, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/SettingsSetRequest;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetNotificationSettingsOperation;->mSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/NotificationSettingsData;->getCategoriesCount()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetNotificationSettingsOperation;->mSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/content/NotificationSettingsData;->getCategory(I)Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->getSettingsCount()I

    move-result v6

    move v0, v1

    :goto_1
    if-ge v0, v6, :cond_0

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->getSetting(I)Lcom/google/android/apps/plus/content/NotificationSetting;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/NotificationSetting;->getDeliveryOption()Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/api/services/plusi/model/DataNotificationSettings;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataNotificationSettings;-><init>()V

    iput-object v3, v0, Lcom/google/api/services/plusi/model/DataNotificationSettings;->deliveryOption:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SetNotificationSettingsOperation;->mSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/NotificationSettingsData;->getEmailAddress()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettings;->emailAddress:Ljava/lang/String;

    new-instance v1, Lcom/google/api/services/plusi/model/DataMobileSettings;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/DataMobileSettings;-><init>()V

    const-string v2, "PUSH"

    iput-object v2, v1, Lcom/google/api/services/plusi/model/DataMobileSettings;->mobileNotificationType:Ljava/lang/String;

    new-instance v2, Lcom/google/api/services/plusi/model/OzDataSettings;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/OzDataSettings;-><init>()V

    iput-object v0, v2, Lcom/google/api/services/plusi/model/OzDataSettings;->notificationSettings:Lcom/google/api/services/plusi/model/DataNotificationSettings;

    iput-object v1, v2, Lcom/google/api/services/plusi/model/OzDataSettings;->mobileSettings:Lcom/google/api/services/plusi/model/DataMobileSettings;

    iput-object v2, p1, Lcom/google/api/services/plusi/model/SettingsSetRequest;->settings:Lcom/google/api/services/plusi/model/OzDataSettings;

    return-void
.end method
