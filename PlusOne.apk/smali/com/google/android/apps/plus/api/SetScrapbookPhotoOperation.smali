.class public final Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "SetScrapbookPhotoOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation$SetScrapbookPhotoException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/SetScrapbookCoverPhotoRequest;",
        "Lcom/google/api/services/plusi/model/SetScrapbookCoverPhotoResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mIsGalleryPhoto:Z

.field private mPhotoId:Ljava/lang/String;

.field private mTopOffset:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;IZ)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # I
    .param p7    # Z

    const-string v3, "setscrapbookcoverphoto"

    invoke-static {}, Lcom/google/api/services/plusi/model/SetScrapbookCoverPhotoRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SetScrapbookCoverPhotoRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SetScrapbookCoverPhotoResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SetScrapbookCoverPhotoResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation;->mPhotoId:Ljava/lang/String;

    iput p6, p0, Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation;->mTopOffset:I

    iput-boolean p7, p0, Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation;->mIsGalleryPhoto:Z

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/SetScrapbookCoverPhotoResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SetScrapbookCoverPhotoResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation$SetScrapbookPhotoException;

    invoke-direct {v0}, Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation$SetScrapbookPhotoException;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/SetScrapbookCoverPhotoRequest;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SetScrapbookCoverPhotoRequest;->ownerId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation;->mPhotoId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SetScrapbookCoverPhotoRequest;->photoId:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation;->mTopOffset:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SetScrapbookCoverPhotoRequest;->offset:Ljava/lang/Integer;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation;->mIsGalleryPhoto:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SetScrapbookCoverPhotoRequest;->galleryPhoto:Ljava/lang/Boolean;

    return-void
.end method
