.class public final Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "SharePhotosToEventOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/SharePhotosToEventRequest;",
        "Lcom/google/api/services/plusi/model/SharePhotosToEventResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final mEventId:Ljava/lang/String;

.field final mPhotoIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Landroid/content/Intent;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            ")V"
        }
    .end annotation

    const-string v3, "sharephotostoevent"

    invoke-static {}, Lcom/google/api/services/plusi/model/SharePhotosToEventRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SharePhotosToEventRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SharePhotosToEventResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SharePhotosToEventResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;->mEventId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;->mPhotoIds:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/SharePhotosToEventRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;->mEventId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SharePhotosToEventRequest;->eventId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;->mPhotoIds:Ljava/util/List;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SharePhotosToEventRequest;->photoId:Ljava/util/List;

    return-void
.end method
