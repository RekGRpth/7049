.class public final Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "SquareMemberSearchOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;",
        "Lcom/google/api/services/plusi/model/ReadSquareMembersOzResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContinuationToken:Ljava/lang/String;

.field private final mMemberLimit:I

.field private mMemberList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/SquareMember;",
            ">;"
        }
    .end annotation
.end field

.field private mNextContinuationToken:Ljava/lang/String;

.field private final mQuery:Ljava/lang/String;

.field private final mSquareId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # I
    .param p7    # Landroid/content/Intent;
    .param p8    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "readsquaremembers"

    invoke-static {}, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequestJson;->getInstance()Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/ReadSquareMembersOzResponseJson;->getInstance()Lcom/google/api/services/plusi/model/ReadSquareMembersOzResponseJson;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->mSquareId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->mQuery:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->mContinuationToken:Ljava/lang/String;

    const/16 v0, 0x32

    const/16 v1, 0x64

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->mMemberLimit:I

    return-void
.end method


# virtual methods
.method public final getNextContinuationToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->mNextContinuationToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquareMemberList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/SquareMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->mMemberList:Ljava/util/List;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzResponse;->memberList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzResponse;->memberList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzResponse;->memberList:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/MemberList;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/api/services/plusi/model/MemberList;->continuationToken:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->mNextContinuationToken:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MemberList;->member:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->mMemberList:Ljava/util/List;

    :cond_0
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->mSquareId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;->obfuscatedSquareId:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;->memberListQuery:Ljava/util/List;

    new-instance v0, Lcom/google/api/services/plusi/model/MemberListQuery;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/MemberListQuery;-><init>()V

    const-string v1, "MEMBER"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/MemberListQuery;->membershipStatus:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->mContinuationToken:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/MemberListQuery;->continuationToken:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->mQuery:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/MemberListQuery;->searchTokenPrefix:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->mMemberLimit:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/MemberListQuery;->pageLimit:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/ReadSquareMembersOzRequest;->memberListQuery:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
