.class public final Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "SquareSearchQueryOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/SearchQueryRequest;",
        "Lcom/google/api/services/plusi/model/SearchQueryResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContinuationToken:Ljava/lang/String;

.field private mNewContinuationToken:Ljava/lang/String;

.field private final mQuery:Ljava/lang/String;

.field private mSquareResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/SquareResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/content/Intent;
    .param p6    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const/4 v6, 0x0

    const-string v3, "searchquery"

    invoke-static {}, Lcom/google/api/services/plusi/model/SearchQueryRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SearchQueryRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SearchQueryResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SearchQueryResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;->mQuery:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;->mContinuationToken:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getContinuationToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;->mNewContinuationToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquareSearchResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/SquareResult;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;->mSquareResults:Ljava/util/List;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SearchResults;->squareResults:Lcom/google/api/services/plusi/model/SquareResults;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SearchResults;->squareResults:Lcom/google/api/services/plusi/model/SquareResults;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SquareResults;->result:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;->mSquareResults:Ljava/util/List;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SearchResults;->peopleResults:Lcom/google/api/services/plusi/model/PeopleResults;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SearchResults;->peopleResults:Lcom/google/api/services/plusi/model/PeopleResults;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PeopleResults;->shownPeopleBlob:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;->mNewContinuationToken:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;

    new-instance v0, Lcom/google/api/services/plusi/model/SearchQuery;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SearchQuery;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;->mQuery:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/SearchQuery;->queryText:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

    const-string v1, "SQUARES"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/SearchQuery;->filter:Ljava/lang/String;

    new-instance v0, Lcom/google/api/services/plusi/model/PeopleRequestData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PeopleRequestData;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->peopleRequestData:Lcom/google/api/services/plusi/model/PeopleRequestData;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->peopleRequestData:Lcom/google/api/services/plusi/model/PeopleRequestData;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;->mContinuationToken:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/PeopleRequestData;->shownPeopleBlob:Ljava/lang/String;

    return-void
.end method
