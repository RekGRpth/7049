.class public final Lcom/google/android/apps/plus/api/UpdateEventOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "UpdateEventOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/UpdateEventRequest;",
        "Lcom/google/api/services/plusi/model/UpdateEventResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p4    # Landroid/content/Intent;
    .param p5    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "updateevent"

    invoke-static {}, Lcom/google/api/services/plusi/model/UpdateEventRequestJson;->getInstance()Lcom/google/api/services/plusi/model/UpdateEventRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/UpdateEventResponseJson;->getInstance()Lcom/google/api/services/plusi/model/UpdateEventResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/UpdateEventResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/UpdateEventResponse;->event:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->theme:Lcom/google/api/services/plusi/model/Theme;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->theme:Lcom/google/api/services/plusi/model/Theme;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startDate:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startDate:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endDate:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->endDate:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->displayContent:Lcom/google/api/services/plusi/model/PlusEventDisplayContent;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->displayContent:Lcom/google/api/services/plusi/model/PlusEventDisplayContent;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->hangoutInfo:Lcom/google/api/services/plusi/model/HangoutInfo;

    iput-object v0, v1, Lcom/google/api/services/plusi/model/PlusEvent;->hangoutInfo:Lcom/google/api/services/plusi/model/HangoutInfo;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v0, v1, v3, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->insertEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;)V

    :cond_0
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/UpdateEventRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UpdateEventOperation;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/UpdateEventRequest;->event:Lcom/google/api/services/plusi/model/PlusEvent;

    return-void
.end method
