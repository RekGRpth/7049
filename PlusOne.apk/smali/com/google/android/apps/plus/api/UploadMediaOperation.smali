.class public final Lcom/google/android/apps/plus/api/UploadMediaOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "UploadMediaOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/api/UploadMediaOperation$UploadMediaException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/UploadMediaRequest;",
        "Lcom/google/api/services/plusi/model/UploadMediaResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final QUERY_PARAMS:Landroid/os/Bundle;


# instance fields
.field private final mAlbumId:Ljava/lang/String;

.field private final mAlbumLabel:Ljava/lang/String;

.field private final mAlbumTitle:Ljava/lang/String;

.field private final mOwnerId:Ljava/lang/String;

.field private final mPayloadData:[B

.field private mResponse:Lcom/google/api/services/plusi/model/UploadMediaResponse;

.field private mTopOffset:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "uploadType"

    const-string v2, "multipart"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->QUERY_PARAMS:Landroid/os/Bundle;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # [B

    const-string v10, "uploadmedia"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/upload"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->PLUS_FRONTEND_PATH:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    sget-object v7, Lcom/google/android/apps/plus/api/UploadMediaOperation;->QUERY_PARAMS:Landroid/os/Bundle;

    invoke-static {}, Lcom/google/api/services/plusi/model/UploadMediaRequestJson;->getInstance()Lcom/google/api/services/plusi/model/UploadMediaRequestJson;

    move-result-object v8

    invoke-static {}, Lcom/google/api/services/plusi/model/UploadMediaResponseJson;->getInstance()Lcom/google/api/services/plusi/model/UploadMediaResponseJson;

    move-result-object v9

    new-instance v1, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;

    const-string v4, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite https://www.googleapis.com/auth/plus.native"

    const/4 v5, 0x0

    const-string v6, "multipart/related; boundary=onetwothreefourfivesixseven"

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v10

    move-object v6, v11

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move-object v12, v1

    invoke-direct/range {v2 .. v12}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;)V

    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mOwnerId:Ljava/lang/String;

    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mAlbumId:Ljava/lang/String;

    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mAlbumTitle:Ljava/lang/String;

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mAlbumLabel:Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mTopOffset:Ljava/lang/Integer;

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mPayloadData:[B

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # [B

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/api/UploadMediaOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/Integer;)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # [B
    .param p8    # Ljava/lang/Integer;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v10, p7

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/plus/api/UploadMediaOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mTopOffset:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic createHttpEntity(Lcom/google/android/apps/plus/json/GenericJson;)Lcom/google/android/apps/plus/network/JsonHttpEntity;
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;

    new-instance v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mRequestJson:Lcom/google/android/apps/plus/json/EsJson;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mPayloadData:[B

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/apps/plus/network/JsonHttpEntity;-><init>(Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/GenericJson;[B)V

    return-object v0
.end method

.method public final getUploadMediaResponse()Lcom/google/api/services/plusi/model/UploadMediaResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mResponse:Lcom/google/api/services/plusi/model/UploadMediaResponse;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/UploadMediaResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/UploadMediaOperation;->onStartResultProcessing()V

    iput-object p1, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mResponse:Lcom/google/api/services/plusi/model/UploadMediaResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/UploadMediaResponse;->setProfilePhotoSucceeded:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "HttpTransaction"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "profile"

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mAlbumId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "HttpTransaction"

    const-string v1, "Failed to upload and set profile photo"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    new-instance v0, Lcom/google/android/apps/plus/api/UploadMediaOperation$UploadMediaException;

    invoke-direct {v0}, Lcom/google/android/apps/plus/api/UploadMediaOperation$UploadMediaException;-><init>()V

    throw v0

    :cond_1
    const-string v0, "scrapbook"

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mAlbumId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HttpTransaction"

    const-string v1, "Failed to upload and set cover photo"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mOwnerId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->ownerId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mAlbumId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->albumId:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->autoResize:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mAlbumLabel:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mAlbumLabel:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->albumLabel:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mAlbumTitle:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mAlbumTitle:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->albumTitle:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mTopOffset:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mTopOffset:Ljava/lang/Integer;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->offset:Ljava/lang/Integer;

    :cond_2
    return-void
.end method
