.class public final Lcom/google/android/apps/plus/api/WritePlusOneOperation;
.super Lcom/google/android/apps/plus/api/ApiaryOperation;
.source "WritePlusOneOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/ApiaryOperation",
        "<",
        "Lcom/google/api/services/pos/model/Plusones;",
        "Lcom/google/api/services/pos/model/Plusones;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAbtk:Ljava/lang/String;

.field private final mAdd:Z

.field private mPlusones:Lcom/google/api/services/pos/model/Plusones;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/api/ApiaryApiInfo;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Lcom/google/android/apps/plus/api/ApiaryApiInfo;
    .param p6    # Ljava/lang/String;
    .param p7    # Z
    .param p8    # Ljava/lang/String;

    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "https"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->POS_FRONTEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->POS_FRONTEND_PATH:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "plusones"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "source"

    const-string v3, "native:android_app"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    sget-object v1, Lcom/google/android/apps/plus/util/Property;->POS_BACKEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    invoke-static {}, Lcom/google/api/services/pos/model/PlusonesJson;->getInstance()Lcom/google/api/services/pos/model/PlusonesJson;

    move-result-object v12

    invoke-static {}, Lcom/google/api/services/pos/model/PlusonesJson;->getInstance()Lcom/google/api/services/pos/model/PlusonesJson;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v1, Lcom/google/android/apps/plus/network/PlatformHttpRequestConfiguration;

    const-string v4, "Manage your +1\'s"

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->POS_BACKEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v5

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/network/PlatformHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V

    if-eqz p7, :cond_0

    const-string v11, "POST"

    :goto_0
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v10

    move-object v6, v12

    move-object v10, v1

    invoke-direct/range {v2 .. v11}, Lcom/google/android/apps/plus/api/ApiaryOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Ljava/lang/String;)V

    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->mAdd:Z

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->mAbtk:Ljava/lang/String;

    return-void

    :cond_0
    const-string v11, "DELETE"

    goto :goto_0
.end method


# virtual methods
.method public final createPostData()Lorg/apache/http/HttpEntity;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->mAdd:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/api/ApiaryOperation;->createPostData()Lorg/apache/http/HttpEntity;

    move-result-object v0

    goto :goto_0
.end method

.method public final getPlusones()Lcom/google/api/services/pos/model/Plusones;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->mPlusones:Lcom/google/api/services/pos/model/Plusones;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/api/services/pos/model/Plusones;

    invoke-direct {v0}, Lcom/google/api/services/pos/model/Plusones;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->mPlusones:Lcom/google/api/services/pos/model/Plusones;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->mPlusones:Lcom/google/api/services/pos/model/Plusones;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/pos/model/Plusones;

    iput-object p1, p0, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->mPlusones:Lcom/google/api/services/pos/model/Plusones;

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/pos/model/Plusones;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->mAbtk:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/pos/model/Plusones;->abtk:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->mAdd:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    return-void
.end method
