.class public Lcom/google/android/apps/plus/content/AudienceData;
.super Ljava/lang/Object;
.source "AudienceData.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/AudienceData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCircles:[Lcom/google/android/apps/plus/content/CircleData;

.field private mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

.field private mTotalPersonCount:I

.field private mUsers:[Lcom/google/android/apps/plus/content/PersonData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/content/AudienceData$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/AudienceData$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/AudienceData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v1, v0, [Lcom/google/android/apps/plus/content/PersonData;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    sget-object v2, Lcom/google/android/apps/plus/content/PersonData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v1, v0, [Lcom/google/android/apps/plus/content/CircleData;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    sget-object v2, Lcom/google/android/apps/plus/content/CircleData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v1, v0, [Lcom/google/android/apps/plus/content/SquareTargetData;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    sget-object v2, Lcom/google/android/apps/plus/content/SquareTargetData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mTotalPersonCount:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/content/CircleData;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [Lcom/google/android/apps/plus/content/PersonData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/plus/content/CircleData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    new-array v0, v1, [Lcom/google/android/apps/plus/content/SquareTargetData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    aput-object p1, v0, v1

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/content/PersonData;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/content/PersonData;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v2, [Lcom/google/android/apps/plus/content/PersonData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    new-array v0, v1, [Lcom/google/android/apps/plus/content/CircleData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    new-array v0, v1, [Lcom/google/android/apps/plus/content/SquareTargetData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    aput-object p1, v0, v1

    iput v2, p0, Lcom/google/android/apps/plus/content/AudienceData;->mTotalPersonCount:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/content/SquareTargetData;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/content/SquareTargetData;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [Lcom/google/android/apps/plus/content/PersonData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    new-array v0, v1, [Lcom/google/android/apps/plus/content/CircleData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/plus/content/SquareTargetData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    aput-object p1, v0, v1

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;I)V
    .locals 1
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;I)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0, p3}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    return-void
.end method

.method private constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/SquareTargetData;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    invoke-direct {p0, p1, p2, v1, v0}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V
    .locals 2
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/SquareTargetData;",
            ">;I)V"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/plus/content/PersonData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    :goto_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/plus/content/CircleData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    invoke-interface {p2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    :goto_1
    if-eqz p3, :cond_2

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/plus/content/SquareTargetData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    invoke-interface {p3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    :goto_2
    iput p4, p0, Lcom/google/android/apps/plus/content/AudienceData;->mTotalPersonCount:I

    return-void

    :cond_0
    new-array v0, v1, [Lcom/google/android/apps/plus/content/PersonData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    goto :goto_0

    :cond_1
    new-array v0, v1, [Lcom/google/android/apps/plus/content/CircleData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    goto :goto_1

    :cond_2
    new-array v0, v1, [Lcom/google/android/apps/plus/content/SquareTargetData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    goto :goto_2
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/plus/content/AudienceData;->mTotalPersonCount:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    instance-of v1, p1, Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    iget v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mTotalPersonCount:I

    iget v2, v0, Lcom/google/android/apps/plus/content/AudienceData;->mTotalPersonCount:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    iget-object v2, v0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    iget-object v2, v0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    iget-object v2, v0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getCircle(I)Lcom/google/android/apps/plus/content/CircleData;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getCircleCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    array-length v0, v0

    return v0
.end method

.method public final getCircles()[Lcom/google/android/apps/plus/content/CircleData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    return-object v0
.end method

.method public final getHiddenUserCount()I
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mTotalPersonCount:I

    iget-object v2, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    array-length v2, v2

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final getSquareTarget(I)Lcom/google/android/apps/plus/content/SquareTargetData;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final getSquareTargetCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    array-length v0, v0

    return v0
.end method

.method public final getSquareTargets()[Lcom/google/android/apps/plus/content/SquareTargetData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    return-object v0
.end method

.method public final getTotalPersonCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mTotalPersonCount:I

    return v0
.end method

.method public final getUser(I)Lcom/google/android/apps/plus/content/PersonData;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getUserCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    array-length v0, v0

    return v0
.end method

.method public final getUsers()[Lcom/google/android/apps/plus/content/PersonData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mTotalPersonCount:I

    add-int/lit16 v0, v1, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    array-length v0, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    array-length v0, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toNameList(Landroid/content/Context;)Ljava/lang/String;
    .locals 18
    .param p1    # Landroid/content/Context;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    sget v15, Lcom/google/android/apps/plus/R$string;->compose_acl_separator:I

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    const v15, 0x104000e

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v15, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v15, Lcom/google/android/apps/plus/R$string;->square_unknown:I

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    array-length v15, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    add-int v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    add-int v2, v15, v16

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v10, 0x0

    const/4 v3, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    array-length v15, v15

    if-ge v3, v15, :cond_2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    aget-object v15, v15, v3

    invoke-virtual {v15}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_1

    :goto_1
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v10, v10, 0x1

    if-ge v10, v2, :cond_0

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move-object v1, v4

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    array-length v15, v15

    if-ge v3, v15, :cond_6

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    aget-object v15, v15, v3

    invoke-virtual {v15}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    aget-object v15, v15, v3

    invoke-virtual {v15}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_4

    :goto_3
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v10, v10, 0x1

    if-ge v10, v2, :cond_3

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_5

    move-object v9, v8

    goto :goto_3

    :cond_5
    move-object v9, v6

    goto :goto_3

    :cond_6
    const/4 v3, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    array-length v15, v15

    if-ge v3, v15, :cond_a

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    aget-object v15, v15, v3

    invoke-virtual {v15}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareName()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    aget-object v15, v15, v3

    invoke-virtual {v15}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareStreamName()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_7

    move-object v13, v5

    :cond_7
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_9

    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    add-int/lit8 v10, v10, 0x1

    if-ge v10, v2, :cond_8

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_9
    sget v15, Lcom/google/android/apps/plus/R$string;->square_name_and_topic:I

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v13, v16, v17

    const/16 v17, 0x1

    aput-object v14, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v7, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    return-object v15
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Audience circles: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", users: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", squares: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hidden users: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getHiddenUserCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mSquareTargets:[Lcom/google/android/apps/plus/content/SquareTargetData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    iget v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mTotalPersonCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
