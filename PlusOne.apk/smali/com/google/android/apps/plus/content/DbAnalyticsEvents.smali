.class public final Lcom/google/android/apps/plus/content/DbAnalyticsEvents;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbAnalyticsEvents.java"


# direct methods
.method public static deserializeClientOzEventList([B)Ljava/util/List;
    .locals 9
    .param p0    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientOzEvent;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x0

    if-nez p0, :cond_1

    move-object v5, v6

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->getInstance()Lcom/google/api/services/plusi/model/ClientOzEventJson;

    move-result-object v2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    new-array v1, v7, [B

    const/4 v8, 0x0

    invoke-virtual {v0, v1, v8, v7}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_2

    array-length v7, v1

    if-nez v7, :cond_3

    :cond_2
    move-object v5, v6

    goto :goto_0

    :cond_3
    invoke-virtual {v2, v1}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static serializeClientOzEventList(Ljava/util/List;)[B
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientOzEvent;",
            ">;)[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p0, :cond_0

    const/4 v5, 0x0

    :goto_0
    return-object v5

    :cond_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-static {}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->getInstance()Lcom/google/api/services/plusi/model/ClientOzEventJson;

    move-result-object v3

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v1, v6}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/ClientOzEvent;

    invoke-virtual {v3, v2}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v6

    if-nez v6, :cond_1

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto :goto_1

    :cond_1
    array-length v7, v6

    invoke-virtual {v1, v7}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v1, v6}, Ljava/io/DataOutputStream;->write([B)V

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    goto :goto_0
.end method
