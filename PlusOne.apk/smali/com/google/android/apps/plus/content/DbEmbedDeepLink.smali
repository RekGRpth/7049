.class public final Lcom/google/android/apps/plus/content/DbEmbedDeepLink;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbEmbedDeepLink.java"


# instance fields
.field protected mAppId:J

.field protected mClientPackageNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mDeepLinkId:Ljava/lang/String;

.field protected mLabel:Ljava/lang/String;

.field protected mProductName:Ljava/lang/String;

.field protected mUrl:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/DeepLinkData;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/google/api/services/plusi/model/DeepLinkData;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mClientPackageNames:Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeepLinkData;->client:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeepLinkData;->client:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/PackagingServiceClient;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PackagingServiceClient;->androidPackageName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "ANDROID"

    iget-object v3, v0, Lcom/google/api/services/plusi/model/PackagingServiceClient;->type:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mClientPackageNames:Ljava/util/ArrayList;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/PackagingServiceClient;->androidPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeepLinkData;->deepLinkId:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mDeepLinkId:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeepLinkData;->url:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mUrl:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mLabel:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mProductName:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeepLinkData;->appId:Ljava/lang/Long;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeLong(Ljava/lang/Long;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mAppId:J

    return-void
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedDeepLink;
    .locals 4
    .param p0    # [B

    if-nez p0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-direct {v1}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getShortStringList(Ljava/nio/ByteBuffer;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mClientPackageNames:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mDeepLinkId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mLabel:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mProductName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mAppId:J

    goto :goto_0
.end method

.method public static serialize(Lcom/google/api/services/plusi/model/DeepLinkData;Ljava/lang/String;Ljava/lang/String;)[B
    .locals 5
    .param p0    # Lcom/google/api/services/plusi/model/DeepLinkData;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;-><init>(Lcom/google/api/services/plusi/model/DeepLinkData;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mClientPackageNames:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->putShortStringList(Ljava/io/DataOutputStream;Ljava/util/List;)V

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mDeepLinkId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mLabel:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mUrl:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mProductName:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-wide v3, v0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mAppId:J

    invoke-virtual {v2, v3, v4}, Ljava/io/DataOutputStream;->writeLong(J)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    return-object v0
.end method


# virtual methods
.method public final getClientPackageNames()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mClientPackageNames:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final getDeepLinkId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mDeepLinkId:Ljava/lang/String;

    return-object v0
.end method

.method public final getLabelOrDefault(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mLabel:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$string;->app_invite_default_action:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mLabel:Ljava/lang/String;

    goto :goto_0
.end method

.method public final getProductName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mProductName:Ljava/lang/String;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->mUrl:Ljava/lang/String;

    return-object v0
.end method
