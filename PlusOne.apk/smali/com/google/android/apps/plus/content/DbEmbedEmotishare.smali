.class public Lcom/google/android/apps/plus/content/DbEmbedEmotishare;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbEmbedEmotishare.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/DbEmbedEmotishare;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDescription:Ljava/lang/String;

.field private mImageRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mImageUrl:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mType:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mUrl:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageUrl:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mDescription:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mType:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->createMediaRef(Ljava/lang/String;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/Emotishare;)V
    .locals 1
    .param p1    # Lcom/google/api/services/plusi/model/Emotishare;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Emotishare;->url:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mUrl:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Emotishare;->emotion:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mType:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Emotishare;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mName:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Emotishare;->description:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mDescription:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Emotishare;->proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Emotishare;->proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Thumbnail;->imageUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageUrl:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iput-object p3, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mUrl:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mType:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mDescription:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageUrl:Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->createMediaRef(Ljava/lang/String;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-void
.end method

.method public static deserialize(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/plus/content/DbEmbedEmotishare;
    .locals 2
    .param p0    # Ljava/nio/ByteBuffer;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;-><init>()V

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mUrl:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageUrl:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mName:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mDescription:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mType:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageUrl:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->createMediaRef(Ljava/lang/String;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    goto :goto_0
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedEmotishare;
    .locals 2
    .param p0    # [B

    if-nez p0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->deserialize(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    move-result-object v1

    goto :goto_0
.end method

.method public static serialize(Lcom/google/android/apps/plus/content/DbEmbedEmotishare;)[B
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/content/DbEmbedEmotishare;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x40

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mUrl:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->getImageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mName:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mDescription:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mType:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    return-object v1
.end method


# virtual methods
.method public final createEmbed()Lcom/google/api/services/plusi/model/EmbedClientItem;
    .locals 4

    new-instance v0, Lcom/google/api/services/plusi/model/EmbedClientItem;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EmbedClientItem;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->type:Ljava/util/List;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->type:Ljava/util/List;

    const-string v2, "EMOTISHARE"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/api/services/plusi/model/Emotishare;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/Emotishare;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mUrl:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/Emotishare;->url:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mType:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/Emotishare;->emotion:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mName:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/Emotishare;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageUrl:Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v2, Lcom/google/api/services/plusi/model/Thumbnail;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/Thumbnail;-><init>()V

    iput-object v2, v1, Lcom/google/api/services/plusi/model/Emotishare;->proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/Emotishare;->proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageUrl:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/api/services/plusi/model/Thumbnail;->imageUrl:Ljava/lang/String;

    :cond_0
    iput-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->emotishare:Lcom/google/api/services/plusi/model/Emotishare;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->type:Ljava/util/List;

    const-string v2, "THING"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/api/services/plusi/model/Thing;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/Thing;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mUrl:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/Thing;->url:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mName:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/Thing;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageUrl:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/Thing;->imageUrl:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getImageRef()Lcom/google/android/apps/plus/api/MediaRef;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method public final getImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageUrl:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mImageUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->mType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
