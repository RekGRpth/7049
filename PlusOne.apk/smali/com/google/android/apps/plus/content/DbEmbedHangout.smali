.class public final Lcom/google/android/apps/plus/content/DbEmbedHangout;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbEmbedHangout.java"


# instance fields
.field protected mAttendeeAvatarUrls:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mAttendeeGaiaIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mAttendeeNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mHangoutId:Ljava/lang/String;

.field protected mStatus:Ljava/lang/String;

.field protected mYoutubeLiveId:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/google/api/services/plusi/model/HangoutConsumer;)V
    .locals 5
    .param p1    # Lcom/google/api/services/plusi/model/HangoutConsumer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iget-object v3, p1, Lcom/google/api/services/plusi/model/HangoutConsumer;->startContext:Lcom/google/api/services/plusi/model/HangoutStartContext;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/google/api/services/plusi/model/HangoutConsumer;->startContext:Lcom/google/api/services/plusi/model/HangoutStartContext;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/HangoutStartContext;->hangoutId:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mHangoutId:Ljava/lang/String;

    :goto_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeGaiaIds:Ljava/util/ArrayList;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeNames:Ljava/util/ArrayList;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeAvatarUrls:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/HangoutConsumer;->attendees:Ljava/util/List;

    if-eqz v3, :cond_1

    const/4 v0, 0x0

    iget-object v3, p1, Lcom/google/api/services/plusi/model/HangoutConsumer;->attendees:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    :goto_1
    if-ge v0, v2, :cond_1

    iget-object v3, p1, Lcom/google/api/services/plusi/model/HangoutConsumer;->attendees:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeGaiaIds:Ljava/util/ArrayList;

    iget-object v4, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeNames:Ljava/util/ArrayList;

    iget-object v4, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeAvatarUrls:Ljava/util/ArrayList;

    iget-object v4, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->imageUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mHangoutId:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v3, p1, Lcom/google/api/services/plusi/model/HangoutConsumer;->youtubeLiveId:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mYoutubeLiveId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/HangoutConsumer;->status:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mStatus:Ljava/lang/String;

    return-void
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedHangout;
    .locals 3
    .param p0    # [B

    if-nez p0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-direct {v1}, Lcom/google/android/apps/plus/content/DbEmbedHangout;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mHangoutId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getShortStringList(Ljava/nio/ByteBuffer;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeGaiaIds:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getShortStringList(Ljava/nio/ByteBuffer;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeNames:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getShortStringList(Ljava/nio/ByteBuffer;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeAvatarUrls:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mYoutubeLiveId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mStatus:Ljava/lang/String;

    goto :goto_0
.end method

.method public static serialize(Lcom/google/api/services/plusi/model/HangoutConsumer;)[B
    .locals 4
    .param p0    # Lcom/google/api/services/plusi/model/HangoutConsumer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/content/DbEmbedHangout;-><init>(Lcom/google/api/services/plusi/model/HangoutConsumer;)V

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x100

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mHangoutId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeGaiaIds:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->putShortStringList(Ljava/io/DataOutputStream;Ljava/util/List;)V

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeNames:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->putShortStringList(Ljava/io/DataOutputStream;Ljava/util/List;)V

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeAvatarUrls:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->putShortStringList(Ljava/io/DataOutputStream;Ljava/util/List;)V

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mYoutubeLiveId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mStatus:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    return-object v0
.end method


# virtual methods
.method public final getAttendeeAvatarUrls()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeAvatarUrls:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final getAttendeeGaiaIds()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeGaiaIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final getAttendeeNames()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeNames:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final getHangoutId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mHangoutId:Ljava/lang/String;

    return-object v0
.end method

.method public final getNumAttendees()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mAttendeeGaiaIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getYoutubeLiveId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mYoutubeLiveId:Ljava/lang/String;

    return-object v0
.end method

.method public final isInProgress()Z
    .locals 2

    const-string v0, "ACTIVE"

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mStatus:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public final isJoinable()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedHangout;->mYoutubeLiveId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
