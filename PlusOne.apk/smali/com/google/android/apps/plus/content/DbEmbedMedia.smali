.class public final Lcom/google/android/apps/plus/content/DbEmbedMedia;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbEmbedMedia.java"


# instance fields
.field protected mAlbumId:Ljava/lang/String;

.field protected mContentUrl:Ljava/lang/String;

.field protected mDescription:Ljava/lang/String;

.field protected mHeight:S

.field protected mImageUrl:Ljava/lang/String;

.field protected mIsAlbum:Z

.field protected mIsPanorama:Z

.field protected mIsVideo:Z

.field protected mOwnerId:Ljava/lang/String;

.field protected mPhotoId:J

.field protected mTitle:Ljava/lang/String;

.field protected mWidth:S


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/PlusPhoto;)V
    .locals 0
    .param p1    # Lcom/google/api/services/plusi/model/PlusPhoto;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->initPlusPhoto(Lcom/google/api/services/plusi/model/PlusPhoto;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/PlusPhotoAlbum;)V
    .locals 4
    .param p1    # Lcom/google/api/services/plusi/model/PlusPhotoAlbum;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->associatedMedia:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->associatedMedia:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    if-lez v0, :cond_0

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->associatedMedia:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/PlusPhoto;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->initPlusPhoto(Lcom/google/api/services/plusi/model/PlusPhoto;)V

    :cond_0
    if-le v0, v2, :cond_2

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsAlbum:Z

    return-void

    :cond_1
    move v0, v3

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/Thing;)V
    .locals 1
    .param p1    # Lcom/google/api/services/plusi/model/Thing;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Thing;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Thing;->description:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mTitle:Ljava/lang/String;

    :goto_0
    iget-object v0, p1, Lcom/google/api/services/plusi/model/Thing;->url:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mContentUrl:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Thing;->imageUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mImageUrl:Ljava/lang/String;

    return-void

    :cond_0
    iget-object v0, p1, Lcom/google/api/services/plusi/model/Thing;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mTitle:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Thing;->description:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mDescription:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/VideoObject;)V
    .locals 4
    .param p1    # Lcom/google/api/services/plusi/model/VideoObject;

    const/16 v3, 0x1e0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iget-object v2, p1, Lcom/google/api/services/plusi/model/VideoObject;->url:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mContentUrl:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/VideoObject;->thumbnailUrl:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mImageUrl:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/VideoObject;->widthPx:Ljava/lang/Integer;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/VideoObject;->heightPx:Ljava/lang/Integer;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v0

    if-lez v1, :cond_0

    if-lez v0, :cond_0

    iput-short v3, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mWidth:S

    iget-short v2, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mWidth:S

    mul-int/2addr v2, v0

    div-int/2addr v2, v1

    int-to-short v2, v2

    iput-short v2, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mHeight:S

    :goto_0
    iget-object v2, p1, Lcom/google/api/services/plusi/model/VideoObject;->name:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/VideoObject;->name:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mTitle:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/VideoObject;->description:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mDescription:Ljava/lang/String;

    :goto_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsVideo:Z

    return-void

    :cond_0
    iput-short v3, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mWidth:S

    const/16 v2, 0x168

    iput-short v2, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mHeight:S

    goto :goto_0

    :cond_1
    iget-object v2, p1, Lcom/google/api/services/plusi/model/VideoObject;->description:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mTitle:Ljava/lang/String;

    goto :goto_1
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/WebPage;)V
    .locals 1
    .param p1    # Lcom/google/api/services/plusi/model/WebPage;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/WebPage;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mTitle:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/WebPage;->description:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mDescription:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/WebPage;->url:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mContentUrl:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/WebPage;->imageUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mImageUrl:Ljava/lang/String;

    return-void
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedMedia;
    .locals 2
    .param p0    # [B

    if-nez p0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-direct {v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->deserializeFromStream(Ljava/nio/ByteBuffer;)V

    goto :goto_0
.end method

.method private initPlusPhoto(Lcom/google/api/services/plusi/model/PlusPhoto;)V
    .locals 5
    .param p1    # Lcom/google/api/services/plusi/model/PlusPhoto;

    const-wide/16 v1, 0x0

    const/4 v4, 0x0

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->thumbnail:Lcom/google/api/services/plusi/model/ImageObject;

    if-eqz v3, :cond_7

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->thumbnail:Lcom/google/api/services/plusi/model/ImageObject;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ImageObject;->proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

    if-eqz v3, :cond_4

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->thumbnail:Lcom/google/api/services/plusi/model/ImageObject;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ImageObject;->proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Thumbnail;->imageUrl:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->thumbnail:Lcom/google/api/services/plusi/model/ImageObject;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ImageObject;->proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Thumbnail;->imageUrl:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/api/ApiUtils;->prependProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/plus/util/ImageUrlUtils;->getCanonicalUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mImageUrl:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mOwnerId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->ownerObfuscatedId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->ownerObfuscatedId:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mOwnerId:Ljava/lang/String;

    :cond_0
    iput-object v4, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mAlbumId:Ljava/lang/String;

    :try_start_0
    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->albumId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->albumId:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    cmp-long v3, v1, v3

    if-eqz v3, :cond_1

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->albumId:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mAlbumId:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->photoId:Ljava/lang/String;

    if-nez v3, :cond_8

    :goto_2
    iput-wide v1, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mPhotoId:J

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->thumbnail:Lcom/google/api/services/plusi/model/ImageObject;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->thumbnail:Lcom/google/api/services/plusi/model/ImageObject;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/ImageObject;->widthPx:Ljava/lang/Integer;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mWidth:S

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->thumbnail:Lcom/google/api/services/plusi/model/ImageObject;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/ImageObject;->heightPx:Ljava/lang/Integer;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mHeight:S

    :cond_2
    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->isVideo:Ljava/lang/Boolean;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->originalContentUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    const/4 v1, 0x1

    :goto_3
    iput-boolean v1, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsVideo:Z

    const-string v1, "PHOTOSPHERE"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->mediaType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsPanorama:Z

    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsVideo:Z

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->originalContentUrl:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mContentUrl:Ljava/lang/String;

    :cond_3
    return-void

    :cond_4
    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->thumbnail:Lcom/google/api/services/plusi/model/ImageObject;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ImageObject;->contentUrl:Ljava/lang/String;

    if-eqz v3, :cond_5

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->thumbnail:Lcom/google/api/services/plusi/model/ImageObject;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ImageObject;->contentUrl:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/api/ApiUtils;->prependProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->thumbnail:Lcom/google/api/services/plusi/model/ImageObject;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ImageObject;->imageUrl:Ljava/lang/String;

    if-eqz v3, :cond_6

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->thumbnail:Lcom/google/api/services/plusi/model/ImageObject;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ImageObject;->imageUrl:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/api/ApiUtils;->prependProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->originalMediaPlayerUrl:Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusPhoto;->originalMediaPlayerUrl:Ljava/lang/String;

    goto/16 :goto_0

    :cond_8
    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeLong(Ljava/lang/Long;)J

    move-result-wide v1

    goto :goto_2

    :cond_9
    const/4 v1, 0x0

    goto :goto_3

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public static serialize(Lcom/google/android/apps/plus/content/DbEmbedMedia;)[B
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/content/DbEmbedMedia;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x40

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->serializeToStream(Ljava/io/DataOutputStream;)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    return-object v1
.end method


# virtual methods
.method public final deserializeFromStream(Ljava/nio/ByteBuffer;)V
    .locals 5
    .param p1    # Ljava/nio/ByteBuffer;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-static {p1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mTitle:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mDescription:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mContentUrl:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mImageUrl:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mOwnerId:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mAlbumId:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mPhotoId:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    iput-short v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mWidth:S

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    iput-short v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mHeight:S

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsPanorama:Z

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsVideo:Z

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsAlbum:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public final getAlbumId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mAlbumId:Ljava/lang/String;

    return-object v0
.end method

.method public final getContentUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mContentUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public final getHeight()S
    .locals 1

    iget-short v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mHeight:S

    return v0
.end method

.method public final getImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getMediaType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsVideo:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_0
    return-object v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsPanorama:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->PANORAMA:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_0
.end method

.method public final getOwnerId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mOwnerId:Ljava/lang/String;

    return-object v0
.end method

.method public final getPhotoId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mPhotoId:J

    return-wide v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final getVideoUrl()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsVideo:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mContentUrl:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getWidth()S
    .locals 1

    iget-short v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mWidth:S

    return v0
.end method

.method public final isAlbum()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsAlbum:Z

    return v0
.end method

.method public final isClickable()Z
    .locals 5

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsVideo:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v1, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mPhotoId:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mAlbumId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mOwnerId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isPanorama()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsPanorama:Z

    return v0
.end method

.method public final isVideo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsVideo:Z

    return v0
.end method

.method public final serializeToStream(Ljava/io/DataOutputStream;)V
    .locals 2
    .param p1    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mTitle:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mDescription:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mContentUrl:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mImageUrl:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mOwnerId:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mAlbumId:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mPhotoId:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    iget-short v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mWidth:S

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-short v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mHeight:S

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsPanorama:Z

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsVideo:Z

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbEmbedMedia;->mIsAlbum:Z

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    return-void
.end method
