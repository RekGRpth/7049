.class public Lcom/google/android/apps/plus/content/DbEmbedSquare;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbEmbedSquare.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/DbEmbedSquare;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected mImageUrl:Ljava/lang/String;

.field protected mIsInvitation:Z

.field protected mSquareDescription:Ljava/lang/String;

.field protected mSquareId:Ljava/lang/String;

.field protected mSquareName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/content/DbEmbedSquare$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/DbEmbedSquare$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareDescription:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mImageUrl:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mIsInvitation:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/DbEmbedSquare;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/api/services/plusi/model/EmbedsSquare;)V
    .locals 2
    .param p1    # Lcom/google/api/services/plusi/model/EmbedsSquare;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/EmbedsSquare;->communityId:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/EmbedsSquare;->url:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->resolveSquareId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareId:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/EmbedsSquare;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareName:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/EmbedsSquare;->description:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareDescription:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/EmbedsSquare;->imageUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/api/ApiUtils;->prependProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mImageUrl:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/api/services/plusi/model/SquareInvite;)V
    .locals 2
    .param p1    # Lcom/google/api/services/plusi/model/SquareInvite;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SquareInvite;->communityId:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/SquareInvite;->url:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->resolveSquareId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareId:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SquareInvite;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareName:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SquareInvite;->description:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareDescription:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SquareInvite;->imageUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/api/ApiUtils;->prependProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mImageUrl:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mIsInvitation:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareDescription:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mImageUrl:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mIsInvitation:Z

    return-void
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedSquare;
    .locals 4
    .param p0    # [B

    const/4 v2, 0x1

    if-nez p0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-direct {v1}, Lcom/google/android/apps/plus/content/DbEmbedSquare;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareDescription:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mImageUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    if-ne v3, v2, :cond_1

    :goto_1
    iput-boolean v2, v1, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mIsInvitation:Z

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static resolveSquareId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "communities/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method private static serialize(Lcom/google/android/apps/plus/content/DbEmbedSquare;)[B
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/content/DbEmbedSquare;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareId:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareName:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareDescription:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mImageUrl:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mIsInvitation:Z

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    return-object v1
.end method

.method public static serialize(Lcom/google/api/services/plusi/model/EmbedsSquare;)[B
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/EmbedsSquare;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;-><init>(Lcom/google/api/services/plusi/model/EmbedsSquare;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->serialize(Lcom/google/android/apps/plus/content/DbEmbedSquare;)[B

    move-result-object v0

    return-object v0
.end method

.method public static serialize(Lcom/google/api/services/plusi/model/SquareInvite;)[B
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/SquareInvite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;-><init>(Lcom/google/api/services/plusi/model/SquareInvite;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->serialize(Lcom/google/android/apps/plus/content/DbEmbedSquare;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquareId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareId:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquareName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareName:Ljava/lang/String;

    return-object v0
.end method

.method public final isInvitation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mIsInvitation:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mSquareDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mImageUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbEmbedSquare;->mIsInvitation:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
