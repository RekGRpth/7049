.class public Lcom/google/android/apps/plus/content/DbEmotishareMetadata;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbEmotishareMetadata.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/DbEmotishareMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCategory:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

.field private mGeneration:I

.field private mIconRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mIconUrl:Ljava/lang/String;

.field private mId:I

.field private mShareText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    return-void
.end method

.method public constructor <init>(ILjava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedEmotishare;I)V
    .locals 1
    .param p1    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/apps/plus/content/DbEmbedEmotishare;
    .param p6    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/DbEmbedEmotishare;",
            "I)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iput p1, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mId:I

    iput-object p2, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mCategory:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mShareText:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mIconUrl:Ljava/lang/String;

    iput p6, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mGeneration:I

    iput-object p5, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mIconUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->createMediaRef(Ljava/lang/String;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mIconRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mId:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mCategory:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mCategory:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mShareText:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mIconUrl:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mGeneration:I

    const-class v0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mIconUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->createMediaRef(Ljava/lang/String;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mIconRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static createMediaRef(Ljava/lang/String;)Lcom/google/android/apps/plus/api/MediaRef;
    .locals 7
    .param p0    # Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    const-wide/16 v2, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-object v4, p0

    move-object v5, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    return-object v0
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;
    .locals 4
    .param p0    # [B

    if-nez p0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    invoke-direct {v1}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;-><init>()V

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    iput v2, v1, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mId:I

    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->getShortStringList(Ljava/nio/ByteBuffer;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mCategory:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mShareText:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mIconUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    iput v2, v1, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mGeneration:I

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->deserialize(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    iget-object v2, v1, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mIconUrl:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->createMediaRef(Ljava/lang/String;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mIconRef:Lcom/google/android/apps/plus/api/MediaRef;

    goto :goto_0
.end method

.method public static serialize(Lcom/google/android/apps/plus/content/DbEmotishareMetadata;)[B
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/content/DbEmotishareMetadata;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x40

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget v3, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mId:I

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mCategory:Ljava/util/ArrayList;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->putShortStringList(Ljava/io/DataOutputStream;Ljava/util/List;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mShareText:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mIconUrl:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mGeneration:I

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->serialize(Lcom/google/android/apps/plus/content/DbEmbedEmotishare;)[B

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->write([B)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    return-object v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getEmbed()Lcom/google/android/apps/plus/content/DbEmbedEmotishare;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    return-object v0
.end method

.method public final getGeneration()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mGeneration:I

    return v0
.end method

.method public final getIconRef()Lcom/google/android/apps/plus/api/MediaRef;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mIconRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method public final getId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mId:I

    return v0
.end method

.method public final getImageRef()Lcom/google/android/apps/plus/api/MediaRef;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->getImageRef()Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    goto :goto_0
.end method

.method public final getImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getShareText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mShareText:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->getType()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "TypedImageEmbed name: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cat: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mCategory:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", share: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mShareText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", icon: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mIconUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gen: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mGeneration:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", embed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mCategory:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mShareText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mIconUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mGeneration:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->mEmbed:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
