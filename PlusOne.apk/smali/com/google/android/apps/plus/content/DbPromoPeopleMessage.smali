.class public final Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbPromoPeopleMessage.java"


# instance fields
.field private mBodyText:Ljava/lang/String;

.field private mButtonText:Ljava/lang/String;

.field private mPeople:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/DbSuggestedPerson;",
            ">;"
        }
    .end annotation
.end field

.field private mPromoType:I

.field private mTitleText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoMessage;Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoData;I)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoMessage;
    .param p3    # Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoData;
    .param p4    # I

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-eqz p3, :cond_1

    iget-object v0, p3, Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoData;->category:Ljava/util/List;

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    iget-object v5, p3, Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoData;->category:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result p4

    const/4 v7, 0x0

    :goto_0
    if-ge v7, p4, :cond_1

    iget-object v0, p3, Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoData;->category:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoDataCelebrityCategory;

    iget-object v0, v6, Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoDataCelebrityCategory;->people:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, v6, Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoDataCelebrityCategory;->people:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v8, Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    iget-object v0, v6, Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoDataCelebrityCategory;->people:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;

    invoke-direct {v8, p1, v0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;-><init>(Landroid/content/Context;Lcom/google/api/services/plusi/model/PeopleViewPerson;)V

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    iget-object v1, p2, Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoMessage;->titleText:Ljava/lang/String;

    iget-object v2, p2, Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoMessage;->bodyText:Ljava/lang/String;

    iget-object v3, p2, Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoMessage;->buttonText:Ljava/lang/String;

    :cond_2
    const/4 v5, 0x2

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/api/services/plusi/model/SuggestedPeoplePromoMessage;Lcom/google/api/services/plusi/model/SuggestedPeoplePromoData;I)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/api/services/plusi/model/SuggestedPeoplePromoMessage;
    .param p3    # Lcom/google/api/services/plusi/model/SuggestedPeoplePromoData;
    .param p4    # I

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-eqz p3, :cond_0

    iget-object v0, p3, Lcom/google/api/services/plusi/model/SuggestedPeoplePromoData;->people:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget-object v5, p3, Lcom/google/api/services/plusi/model/SuggestedPeoplePromoData;->people:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result p4

    const/4 v6, 0x0

    :goto_0
    if-ge v6, p4, :cond_0

    iget-object v0, p3, Lcom/google/api/services/plusi/model/SuggestedPeoplePromoData;->people:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/api/services/plusi/model/PeopleViewPerson;

    new-instance v7, Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-direct {v7, p1, v8}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;-><init>(Landroid/content/Context;Lcom/google/api/services/plusi/model/PeopleViewPerson;)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    iget-object v1, p2, Lcom/google/api/services/plusi/model/SuggestedPeoplePromoMessage;->titleText:Ljava/lang/String;

    iget-object v2, p2, Lcom/google/api/services/plusi/model/SuggestedPeoplePromoMessage;->bodyText:Ljava/lang/String;

    iget-object v3, p2, Lcom/google/api/services/plusi/model/SuggestedPeoplePromoMessage;->buttonText:Ljava/lang/String;

    :cond_1
    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V

    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/FindFriendsPromoMessage;)V
    .locals 6
    .param p1    # Lcom/google/api/services/plusi/model/FindFriendsPromoMessage;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/google/api/services/plusi/model/FindFriendsPromoMessage;->titleText:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FindFriendsPromoMessage;->bodyText:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/FindFriendsPromoMessage;->buttonText:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V

    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/DbSuggestedPerson;",
            ">;I)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V

    return-void
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;
    .locals 9
    .param p0    # [B

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    invoke-static {v6}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v6}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v6}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v7}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v7, :cond_1

    invoke-static {v6}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getSuggestedPerson(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V

    goto :goto_0
.end method

.method private init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/DbSuggestedPerson;",
            ">;I)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->mTitleText:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->mBodyText:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->mButtonText:Ljava/lang/String;

    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->mPeople:Ljava/util/ArrayList;

    iput p5, p0, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->mPromoType:I

    return-void

    :cond_0
    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method private static putSuggestedPeople(Ljava/io/DataOutputStream;Ljava/util/List;)V
    .locals 3
    .param p0    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataOutputStream;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/DbSuggestedPerson;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    :goto_0
    invoke-virtual {p0, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->putSuggestedPerson(Ljava/io/DataOutputStream;Lcom/google/android/apps/plus/content/DbSuggestedPerson;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static serialize(Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;)[B
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x100

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    iget v3, p0, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->mPromoType:I

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->mTitleText:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->mBodyText:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->mButtonText:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->mPeople:Ljava/util/ArrayList;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->putSuggestedPeople(Ljava/io/DataOutputStream;Ljava/util/List;)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    return-object v1

    :catchall_0
    move-exception v3

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    throw v3
.end method


# virtual methods
.method public final getBodyText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->mBodyText:Ljava/lang/String;

    return-object v0
.end method

.method public final getButtonText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->mButtonText:Ljava/lang/String;

    return-object v0
.end method

.method public final getSuggestedPeople()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/DbSuggestedPerson;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->mPeople:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final getTitleText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->mTitleText:Ljava/lang/String;

    return-object v0
.end method
