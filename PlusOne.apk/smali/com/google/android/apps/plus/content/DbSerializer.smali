.class public Lcom/google/android/apps/plus/content/DbSerializer;
.super Ljava/lang/Object;
.source "DbSerializer.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static decodeUtf8([B)Ljava/lang/String;
    .locals 2
    .param p0    # [B

    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static deserializeShortStringList([B)Ljava/util/List;
    .locals 5
    .param p0    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbSerializer;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method protected static getCircle(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/plus/content/CircleData;
    .locals 5
    .param p0    # Ljava/nio/ByteBuffer;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbSerializer;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbSerializer;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    new-instance v4, Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {v4, v0, v3, v1, v2}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    return-object v4
.end method

.method protected static getCircles(Ljava/nio/ByteBuffer;)Ljava/util/List;
    .locals 4
    .param p0    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbSerializer;->getCircle(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method protected static getPeople(Ljava/nio/ByteBuffer;)Ljava/util/List;
    .locals 4
    .param p0    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbSerializer;->getPerson(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method protected static getPerson(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/plus/content/PersonData;
    .locals 5
    .param p0    # Ljava/nio/ByteBuffer;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbSerializer;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbSerializer;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbSerializer;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbSerializer;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v4, v3, v2, v1, v0}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v4
.end method

.method protected static getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    if-lez v1, :cond_0

    new-array v0, v1, [B

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbSerializer;->decodeUtf8([B)Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method static getShortStringList(Ljava/nio/ByteBuffer;)Ljava/util/List;
    .locals 4
    .param p0    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbSerializer;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method protected static putCircle(Ljava/io/DataOutputStream;Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 1
    .param p0    # Ljava/io/DataOutputStream;
    .param p1    # Lcom/google/android/apps/plus/content/CircleData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbSerializer;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbSerializer;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/CircleData;->getSize()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    return-void
.end method

.method protected static putCircles(Ljava/io/DataOutputStream;Ljava/util/List;)V
    .locals 3
    .param p0    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataOutputStream;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/CircleData;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbSerializer;->putCircle(Ljava/io/DataOutputStream;Lcom/google/android/apps/plus/content/CircleData;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected static putPeople(Ljava/io/DataOutputStream;Ljava/util/List;)V
    .locals 3
    .param p0    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataOutputStream;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/PersonData;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/content/DbSerializer;->putPerson(Ljava/io/DataOutputStream;Lcom/google/android/apps/plus/content/PersonData;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected static putPerson(Ljava/io/DataOutputStream;Lcom/google/android/apps/plus/content/PersonData;)V
    .locals 1
    .param p0    # Ljava/io/DataOutputStream;
    .param p1    # Lcom/google/android/apps/plus/content/PersonData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbSerializer;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbSerializer;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbSerializer;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/PersonData;->getCompressedPhotoUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbSerializer;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    return-void
.end method

.method protected static putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/io/DataOutputStream;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    const-string v1, "UTF-8"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->write([B)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    goto :goto_0
.end method

.method protected static putShortStringList(Ljava/io/DataOutputStream;Ljava/util/List;)V
    .locals 3
    .param p0    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataOutputStream;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/content/DbSerializer;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static serializeShortStringList(Ljava/util/List;)[B
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    if-nez p0, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    :try_start_0
    invoke-static {v1, p0}, Lcom/google/android/apps/plus/content/DbSerializer;->putShortStringList(Ljava/io/DataOutputStream;Ljava/util/List;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    throw v3
.end method
