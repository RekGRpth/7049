.class public final Lcom/google/android/apps/plus/content/DbSquareUpdate;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbSquareUpdate.java"


# instance fields
.field protected mModerationState:Ljava/lang/String;

.field protected mSquareId:Ljava/lang/String;

.field protected mSquareName:Ljava/lang/String;

.field protected mSquareStreamId:Ljava/lang/String;

.field protected mSquareStreamName:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/google/api/services/plusi/model/SquareUpdate;)V
    .locals 1
    .param p1    # Lcom/google/api/services/plusi/model/SquareUpdate;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SquareUpdate;->obfuscatedSquareId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareId:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SquareUpdate;->squareName:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareName:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SquareUpdate;->squareStreamId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareStreamId:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SquareUpdate;->squareStreamName:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareStreamName:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SquareUpdate;->moderationState:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mModerationState:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbSquareUpdate;
    .locals 3
    .param p0    # [B

    if-nez p0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/content/DbSquareUpdate;

    invoke-direct {v1}, Lcom/google/android/apps/plus/content/DbSquareUpdate;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareStreamId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareStreamName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mModerationState:Ljava/lang/String;

    goto :goto_0
.end method

.method public static serialize(Lcom/google/api/services/plusi/model/SquareUpdate;)[B
    .locals 4
    .param p0    # Lcom/google/api/services/plusi/model/SquareUpdate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/content/DbSquareUpdate;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/content/DbSquareUpdate;-><init>(Lcom/google/api/services/plusi/model/SquareUpdate;)V

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareName:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareStreamId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareStreamName:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mModerationState:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    return-object v0
.end method


# virtual methods
.method public final getSquareId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareId:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquareModerationState()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mModerationState:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquareName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareName:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquareStreamId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareStreamId:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquareStreamName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareUpdate;->mSquareStreamName:Ljava/lang/String;

    return-object v0
.end method
