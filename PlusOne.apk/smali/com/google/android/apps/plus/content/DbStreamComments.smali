.class public final Lcom/google/android/apps/plus/content/DbStreamComments;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbStreamComments.java"


# instance fields
.field protected mAuthorIds:[Ljava/lang/String;

.field protected mAuthorNames:[Ljava/lang/String;

.field protected mAuthorPhotoUrls:[Ljava/lang/String;

.field protected mContent:[Ljava/lang/String;

.field protected mNumComments:I


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;I)V
    .locals 4
    .param p1    # Landroid/database/Cursor;
    .param p2    # I

    const/4 v3, 0x3

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    iget v1, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorIds:[Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorNames:[Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorPhotoUrls:[Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mContent:[Ljava/lang/String;

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorIds:[Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorNames:[Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorPhotoUrls:[Ljava/lang/String;

    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mContent:[Ljava/lang/String;

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/util/List;I)V
    .locals 4
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Comment;",
            ">;I)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    const/4 v2, 0x3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    iget v2, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorIds:[Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorNames:[Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorPhotoUrls:[Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mContent:[Ljava/lang/String;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    sub-int v0, v2, v3

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorIds:[Ljava/lang/String;

    add-int v2, v0, v1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/Comment;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    aput-object v2, v3, v1

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorNames:[Ljava/lang/String;

    add-int v2, v0, v1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/Comment;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/Comment;->authorName:Ljava/lang/String;

    aput-object v2, v3, v1

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorPhotoUrls:[Ljava/lang/String;

    add-int v2, v0, v1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/Comment;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/Comment;->authorPhotoUrl:Ljava/lang/String;

    aput-object v2, v3, v1

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mContent:[Ljava/lang/String;

    add-int v2, v0, v1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/Comment;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/Comment;->text:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbStreamComments;
    .locals 5
    .param p0    # [B

    if-nez p0, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-direct {v1}, Lcom/google/android/apps/plus/content/DbStreamComments;-><init>()V

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, v1, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    iget v3, v1, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, v1, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorIds:[Ljava/lang/String;

    iget v3, v1, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, v1, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorNames:[Ljava/lang/String;

    iget v3, v1, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, v1, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorPhotoUrls:[Ljava/lang/String;

    iget v3, v1, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, v1, Lcom/google/android/apps/plus/content/DbStreamComments;->mContent:[Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    iget v3, v1, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    if-ge v2, v3, :cond_0

    iget-object v3, v1, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorIds:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbStreamComments;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    iget-object v3, v1, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorNames:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbStreamComments;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    iget-object v3, v1, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorPhotoUrls:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbStreamComments;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    iget-object v3, v1, Lcom/google/android/apps/plus/content/DbStreamComments;->mContent:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbStreamComments;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static serialize(Lcom/google/android/apps/plus/content/DbStreamComments;)[B
    .locals 7
    .param p0    # Lcom/google/android/apps/plus/content/DbStreamComments;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v6, 0x400

    new-instance v4, Ljava/io/ByteArrayOutputStream;

    const/16 v5, 0x100

    invoke-direct {v4, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget v5, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    const/4 v1, 0x0

    :goto_0
    iget v5, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    if-ge v1, v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorIds:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/content/DbStreamComments;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorNames:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/content/DbStreamComments;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorPhotoUrls:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/content/DbStreamComments;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mContent:[Ljava/lang/String;

    aget-object v0, v5, v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-gt v5, v6, :cond_0

    :goto_1
    invoke-static {v2, v0}, Lcom/google/android/apps/plus/content/DbStreamComments;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    return-object v3
.end method


# virtual methods
.method public final getAuthorIds(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorIds:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getAuthorNames(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getAuthorPhotoUrls(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mAuthorPhotoUrls:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getContent(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mContent:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getNumComments()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/content/DbStreamComments;->mNumComments:I

    return v0
.end method
