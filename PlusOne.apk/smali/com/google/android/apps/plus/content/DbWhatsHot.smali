.class public final Lcom/google/android/apps/plus/content/DbWhatsHot;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbWhatsHot.java"


# instance fields
.field private mMessage:Ljava/lang/String;

.field private mViewed:Z


# direct methods
.method public constructor <init>(ZLjava/lang/String;)V
    .locals 0
    .param p1    # Z
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iput-boolean p1, p0, Lcom/google/android/apps/plus/content/DbWhatsHot;->mViewed:Z

    iput-object p2, p0, Lcom/google/android/apps/plus/content/DbWhatsHot;->mMessage:Ljava/lang/String;

    return-void
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbWhatsHot;
    .locals 4
    .param p0    # [B

    const/4 v2, 0x1

    if-nez p0, :cond_0

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    if-ne v3, v2, :cond_1

    :goto_1
    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbWhatsHot;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/plus/content/DbWhatsHot;

    invoke-direct {v3, v2, v1}, Lcom/google/android/apps/plus/content/DbWhatsHot;-><init>(ZLjava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static serialize(Lcom/google/android/apps/plus/content/DbWhatsHot;)[B
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/content/DbWhatsHot;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x100

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/apps/plus/content/DbWhatsHot;->mViewed:Z

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbWhatsHot;->mMessage:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbWhatsHot;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    return-object v1

    :catchall_0
    move-exception v3

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    throw v3
.end method


# virtual methods
.method public final getMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbWhatsHot;->mMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final getViewed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbWhatsHot;->mViewed:Z

    return v0
.end method

.method public final setViewed(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/content/DbWhatsHot;->mViewed:Z

    return-void
.end method
