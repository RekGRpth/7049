.class final Lcom/google/android/apps/plus/content/EsAccountsData$3;
.super Landroid/os/AsyncTask;
.source "EsAccountsData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/content/EsAccountsData;->restoreAccountSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$account:Lcom/google/android/apps/plus/content/EsAccount;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$enabled:Z

.field final synthetic val$photosWifiOnly:Z

.field final synthetic val$videosWifiOnly:Z


# direct methods
.method constructor <init>(Landroid/content/Context;ZZLcom/google/android/apps/plus/content/EsAccount;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/content/EsAccountsData$3;->val$context:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/google/android/apps/plus/content/EsAccountsData$3;->val$photosWifiOnly:Z

    iput-boolean p3, p0, Lcom/google/android/apps/plus/content/EsAccountsData$3;->val$videosWifiOnly:Z

    iput-object p4, p0, Lcom/google/android/apps/plus/content/EsAccountsData$3;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iput-boolean p5, p0, Lcom/google/android/apps/plus/content/EsAccountsData$3;->val$enabled:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsAccountsData$3;->val$context:Landroid/content/Context;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/EsAccountsData$3;->val$photosWifiOnly:Z

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/InstantUpload;->setPhotoWiFiOnlySetting(Landroid/content/Context;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsAccountsData$3;->val$context:Landroid/content/Context;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/EsAccountsData$3;->val$videosWifiOnly:Z

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/InstantUpload;->setVideoWiFiOnlySetting(Landroid/content/Context;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsAccountsData$3;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/EsAccountsData$3;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/content/EsAccountsData$3;->val$enabled:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/phone/InstantUpload;->enableInstantUpload(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    const/4 v0, 0x0

    return-object v0
.end method
