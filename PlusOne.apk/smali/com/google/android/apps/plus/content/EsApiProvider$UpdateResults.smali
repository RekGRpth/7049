.class final Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;
.super Ljava/lang/Object;
.source "EsApiProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/content/EsApiProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "UpdateResults"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final mResults:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field public final mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;


# direct methods
.method public constructor <init>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;-><init>(Lcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;-><init>(Lcom/google/android/apps/plus/service/ServiceResult;Ljava/util/Map;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/service/ServiceResult;Ljava/util/Map;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            "Ljava/util/Map",
            "<TK;TV;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    iput-object p2, p0, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mResults:Ljava/util/Map;

    return-void
.end method
