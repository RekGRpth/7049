.class public Lcom/google/android/apps/plus/content/EsApiProvider;
.super Landroid/content/ContentProvider;
.source "EsApiProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;
    }
.end annotation


# static fields
.field private static sMatcher:Landroid/content/UriMatcher;

.field private static final sPlusoneCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/api/services/pos/model/Plusones;",
            ">;"
        }
    .end annotation
.end field

.field private static final sPreviewCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Lcom/google/android/apps/plus/content/PreviewRequestData;",
            "Lcom/google/android/apps/plus/api/ApiaryActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v4, 0x14

    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.ApiProvider"

    const-string v2, "plusone"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.ApiProvider"

    const-string v2, "account"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.ApiProvider"

    const-string v2, "preview"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    new-instance v0, Landroid/support/v4/util/LruCache;

    invoke-direct {v0, v4}, Landroid/support/v4/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    new-instance v0, Landroid/support/v4/util/LruCache;

    invoke-direct {v0, v4}, Landroid/support/v4/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/support/v4/util/LruCache;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/content/EsApiProvider;ZLandroid/support/v4/util/LruCache;[Lcom/google/android/apps/plus/content/PreviewRequestData;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/content/EsApiProvider;
    .param p1    # Z
    .param p2    # Landroid/support/v4/util/LruCache;
    .param p3    # [Lcom/google/android/apps/plus/content/PreviewRequestData;

    invoke-static {p1, p2, p3}, Lcom/google/android/apps/plus/content/EsApiProvider;->getUncachedKeys(ZLandroid/support/v4/util/LruCache;[Lcom/google/android/apps/plus/content/PreviewRequestData;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/content/EsApiProvider;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/ApiaryApiInfo;Ljava/util/List;)Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/content/EsApiProvider;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/api/ApiaryApiInfo;
    .param p3    # Ljava/util/List;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/content/EsApiProvider;->updatePreviewEntries(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/ApiaryApiInfo;Ljava/util/List;)Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    move-result-object v0

    return-object v0
.end method

.method private buildPlusOneCursorFromCache([Ljava/lang/String;Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;)Landroid/database/Cursor;
    .locals 11
    .param p1    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/api/services/pos/model/Plusones;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v6, Lcom/google/android/apps/plus/external/PlatformContract$PlusOneContent;->COLUMNS:[Ljava/lang/String;

    invoke-direct {v0, v6}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "com.google.circles.platform.result.extra.ERROR_CODE"

    iget-object v8, p2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/service/ServiceResult;->getErrorCode()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "com.google.circles.platform.result.extra.ERROR_MESSAGE"

    iget-object v8, p2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/service/ServiceResult;->getReasonPhrase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/ServiceResult;->getErrorCode()I

    move-result v6

    const/16 v7, 0xc8

    if-ne v6, v7, :cond_3

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sget-object v7, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v7

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    array-length v6, p1

    if-ge v1, v6, :cond_2

    iget-object v6, p2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mResults:Ljava/util/Map;

    new-instance v8, Lcom/google/android/apps/plus/content/PreviewRequestData;

    aget-object v9, p1, v1

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Lcom/google/android/apps/plus/content/PreviewRequestData;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/CallToActionData;)V

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/pos/model/Plusones;

    if-nez v3, :cond_0

    sget-object v6, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    aget-object v8, p1, v1

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/pos/model/Plusones;

    :cond_0
    if-nez v3, :cond_1

    new-instance v3, Lcom/google/api/services/pos/model/Plusones;

    invoke-direct {v3}, Lcom/google/api/services/pos/model/Plusones;-><init>()V

    aget-object v6, p1, v1

    iput-object v6, v3, Lcom/google/api/services/pos/model/Plusones;->id:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/google/android/apps/plus/content/EsApiProvider;->expandPlusOneToCursor(Lcom/google/api/services/pos/model/Plusones;Lcom/google/android/apps/plus/phone/EsMatrixCursor;)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    sget-object v6, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    aget-object v8, p1, v1

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/pos/model/Plusones;

    invoke-static {v6, v0}, Lcom/google/android/apps/plus/content/EsApiProvider;->expandPlusOneToCursor(Lcom/google/api/services/pos/model/Plusones;Lcom/google/android/apps/plus/phone/EsMatrixCursor;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    :cond_2
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    iget v7, v7, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v6, v7, :cond_4

    const/4 v5, 0x1

    :goto_2
    if-eqz v5, :cond_3

    new-instance v4, Lcom/google/api/services/pos/model/List;

    invoke-direct {v4}, Lcom/google/api/services/pos/model/List;-><init>()V

    iput-object v2, v4, Lcom/google/api/services/pos/model/List;->items:Ljava/util/List;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "com.google.android.apps.content.EXTRA_PLUSONES"

    invoke-static {}, Lcom/google/api/services/pos/model/ListJson;->getInstance()Lcom/google/api/services/pos/model/ListJson;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/google/api/services/pos/model/ListJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-object v0

    :cond_4
    const/4 v5, 0x0

    goto :goto_2
.end method

.method private static buildPreviewCursorFromCache([Lcom/google/android/apps/plus/content/PreviewRequestData;Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;)Landroid/database/Cursor;
    .locals 8
    .param p0    # [Lcom/google/android/apps/plus/content/PreviewRequestData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/apps/plus/content/PreviewRequestData;",
            "Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults",
            "<",
            "Lcom/google/android/apps/plus/content/PreviewRequestData;",
            "Lcom/google/android/apps/plus/api/ApiaryActivity;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v1, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "uri"

    aput-object v4, v3, v6

    invoke-direct {v1, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "com.google.circles.platform.result.extra.ERROR_CODE"

    iget-object v5, p1, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/ServiceResult;->getErrorCode()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "com.google.circles.platform.result.extra.ERROR_MESSAGE"

    iget-object v5, p1, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/ServiceResult;->getReasonPhrase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    array-length v3, p0

    new-array v0, v3, [Lcom/google/android/apps/plus/api/ApiaryActivity;

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_1

    new-array v3, v7, [Ljava/lang/Object;

    aget-object v4, p0, v2

    iget-object v4, v4, Lcom/google/android/apps/plus/content/PreviewRequestData;->uri:Landroid/net/Uri;

    aput-object v4, v3, v6

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    iget-object v3, p1, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mResults:Ljava/util/Map;

    aget-object v4, p0, v2

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/api/ApiaryActivity;

    aput-object v3, v0, v2

    aget-object v3, v0, v2

    if-nez v3, :cond_0

    sget-object v3, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    aget-object v4, p0, v2

    invoke-virtual {v3, v4}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/api/ApiaryActivity;

    aput-object v3, v0, v2

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    array-length v3, v0

    if-lez v3, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "com.google.android.apps.content.EXTRA_ACTIVITY"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    :cond_2
    return-object v1
.end method

.method private static deserializeSelectionArgs([Ljava/lang/String;)[Lcom/google/android/apps/plus/content/PreviewRequestData;
    .locals 5
    .param p0    # [Ljava/lang/String;

    array-length v2, p0

    new-array v1, v2, [Lcom/google/android/apps/plus/content/PreviewRequestData;

    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    aget-object v2, p0, v0

    invoke-static {v2}, Lcom/google/android/apps/plus/content/PreviewRequestData;->fromSelectionArg(Ljava/lang/String;)Lcom/google/android/apps/plus/content/PreviewRequestData;

    move-result-object v2

    aput-object v2, v1, v0

    aget-object v2, v1, v0

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to deserialized selectionArg "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".  Value was "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p0, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private static expandPlusOneToCursor(Lcom/google/api/services/pos/model/Plusones;Lcom/google/android/apps/plus/phone/EsMatrixCursor;)V
    .locals 9
    .param p0    # Lcom/google/api/services/pos/model/Plusones;
    .param p1    # Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    iget-object v5, p0, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_1

    sget-object v6, Lcom/google/android/apps/plus/external/PlatformContract$PlusOneContent;->STATE_PLUSONED:Ljava/lang/Integer;

    :goto_0
    const-wide/16 v0, 0x0

    iget-object v3, p0, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    if-eqz v3, :cond_0

    iget-object v2, v3, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    if-eqz v2, :cond_0

    iget-object v7, v2, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->longValue()J

    move-result-wide v0

    :cond_0
    const/4 v7, 0x4

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/api/services/pos/model/Plusones;->id:Ljava/lang/String;

    aput-object v8, v4, v7

    const/4 v7, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v4, v7

    const/4 v7, 0x2

    aput-object v6, v4, v7

    const/4 v7, 0x3

    iget-object v8, p0, Lcom/google/api/services/pos/model/Plusones;->abtk:Ljava/lang/String;

    aput-object v8, v4, v7

    invoke-virtual {p1, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    return-void

    :cond_1
    sget-object v6, Lcom/google/android/apps/plus/external/PlatformContract$PlusOneContent;->STATE_NOTPLUSONED:Ljava/lang/Integer;

    goto :goto_0

    :cond_2
    sget-object v6, Lcom/google/android/apps/plus/external/PlatformContract$PlusOneContent;->STATE_ANONYMOUS:Ljava/lang/Integer;

    goto :goto_0
.end method

.method private getApiaryApiInfo(Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/plus/api/ApiaryApiInfo;
    .locals 18
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v14

    const-string v1, "pkg"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v14}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v1, v2, :cond_0

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v4, v16

    :goto_0
    sget-object v1, Lcom/google/android/apps/plus/util/Property;->PLUS_CLIENTID:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v14}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v14}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v10, v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v11

    const-string v1, "apiKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "clientId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v1, "apiVersion"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v1, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    move-object/from16 v8, p2

    move-object v12, v6

    move-object v13, v1

    invoke-direct/range {v7 .. v13}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V

    return-object v7

    :cond_0
    invoke-virtual {v14}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v15

    const/4 v1, 0x0

    aget-object v4, v15, v1

    goto :goto_0
.end method

.method private static getUncachedKeys(ZLandroid/support/v4/util/LruCache;[Lcom/google/android/apps/plus/content/PreviewRequestData;)Ljava/util/List;
    .locals 6
    .param p0    # Z
    .param p2    # [Lcom/google/android/apps/plus/content/PreviewRequestData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(Z",
            "Landroid/support/v4/util/LruCache",
            "<",
            "Lcom/google/android/apps/plus/content/PreviewRequestData;",
            "TT;>;[",
            "Lcom/google/android/apps/plus/content/PreviewRequestData;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PreviewRequestData;",
            ">;"
        }
    .end annotation

    new-instance v4, Ljava/util/ArrayList;

    array-length v5, p2

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    if-nez p0, :cond_2

    monitor-enter p1

    move-object v0, p2

    :try_start_0
    array-length v3, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    invoke-virtual {p1, v2}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_0

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    return-object v4

    :catchall_0
    move-exception v5

    monitor-exit p1

    throw v5

    :cond_2
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method private static getUncachedUrls(ZLandroid/support/v4/util/LruCache;[Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p0    # Z
    .param p2    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(Z",
            "Landroid/support/v4/util/LruCache",
            "<",
            "Landroid/net/Uri;",
            "TT;>;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v4, Ljava/util/ArrayList;

    array-length v5, p2

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    if-nez p0, :cond_2

    monitor-enter p1

    move-object v0, p2

    :try_start_0
    array-length v2, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_0

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    return-object v4

    :catchall_0
    move-exception v5

    monitor-exit p1

    throw v5

    :cond_2
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public static makePreviewUri(Lcom/google/android/apps/plus/api/ApiaryApiInfo;)Landroid/net/Uri;
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    if-nez p0, :cond_0

    const-string v1, ""

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    move-result-object v0

    const-string v1, "content://com.google.android.apps.plus.content.ApiProvider/preview"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "apiKey"

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getApiKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "clientId"

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getClientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "apiVersion"

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getSdkVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "pkg"

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "hostKey"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getApiKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method private updatePlusoneEntries(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/ApiaryApiInfo;Ljava/util/List;)Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;
    .locals 11
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/api/ApiaryApiInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/api/ApiaryApiInfo;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/api/services/pos/model/Plusones;",
            ">;"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/plus/api/PlusOnesOperation;

    move-object v2, p1

    move-object v4, v3

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/PlusOnesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/api/ApiaryApiInfo;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/PlusOnesOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/PlusOnesOperation;->getErrorCode()I

    move-result v2

    const/16 v4, 0xc8

    if-ne v2, v4, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/PlusOnesOperation;->getPlusones()Lcom/google/api/services/pos/model/Plusones;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v8, v2, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v4

    :try_start_0
    sget-object v2, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v2, v5, v9}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v4

    throw v2

    :cond_1
    new-instance v2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/PlusOnesOperation;->getErrorCode()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/PlusOnesOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/PlusOnesOperation;->getException()Ljava/lang/Exception;

    move-result-object v10

    invoke-direct {v3, v4, v5, v10}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    invoke-direct {v2, v3}, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;-><init>(Lcom/google/android/apps/plus/service/ServiceResult;)V

    :goto_1
    return-object v2

    :cond_2
    new-instance v2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    invoke-direct {v2, v3, v8}, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;-><init>(Lcom/google/android/apps/plus/service/ServiceResult;Ljava/util/Map;)V

    goto :goto_1
.end method

.method private updatePreviewEntries(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/ApiaryApiInfo;Ljava/util/List;)Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;
    .locals 12
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/api/ApiaryApiInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/api/ApiaryApiInfo;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PreviewRequestData;",
            ">;)",
            "Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults",
            "<",
            "Lcom/google/android/apps/plus/content/PreviewRequestData;",
            "Lcom/google/android/apps/plus/api/ApiaryActivity;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/PreviewRequestData;

    new-instance v0, Lcom/google/android/apps/plus/api/LinkPreviewOperation;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v2, v10, Lcom/google/android/apps/plus/content/PreviewRequestData;->uri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v10, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    move-object v2, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/LinkPreviewOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Lcom/google/android/apps/plus/api/CallToActionData;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->getActivity()Lcom/google/android/apps/plus/api/ApiaryActivity;

    move-result-object v8

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->getErrorCode()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_0

    if-nez v8, :cond_0

    const/4 v2, 0x0

    const-string v3, "null activity"

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->setErrorInfo(ILjava/lang/String;Ljava/lang/Exception;)V

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->getErrorCode()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_1

    invoke-interface {v11, v10, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v2, v10, v8}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_1
    new-instance v2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->getErrorCode()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LinkPreviewOperation;->getException()Ljava/lang/Exception;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    invoke-direct {v2, v3}, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;-><init>(Lcom/google/android/apps/plus/service/ServiceResult;)V

    :goto_1
    return-object v2

    :cond_2
    new-instance v2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    invoke-direct {v2, v3, v11}, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;-><init>(Lcom/google/android/apps/plus/service/ServiceResult;Ljava/util/Map;)V

    goto :goto_1
.end method

.method private writePlusOne(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;)Z
    .locals 22
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/plus/content/EsApiProvider;->getApiaryApiInfo(Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-static/range {p3 .. p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    const-string v5, "state"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/external/PlatformContract$PlusOneContent;->STATE_PLUSONED:Ljava/lang/Integer;

    if-ne v5, v6, :cond_6

    const/4 v9, 0x1

    :goto_0
    const-string v5, "token"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v13, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    sget-object v6, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v6

    :try_start_0
    sget-object v5, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/api/services/pos/model/Plusones;

    if-eqz v11, :cond_1

    iget-object v13, v11, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    if-eqz v5, :cond_0

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    if-eqz v5, :cond_0

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    if-eqz v5, :cond_0

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    iget-object v12, v5, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v8, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    invoke-virtual {v12}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v18

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_7

    const/4 v5, 0x1

    :goto_1
    int-to-double v0, v5

    move-wide/from16 v20, v0

    add-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    iput-object v5, v8, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    :cond_0
    invoke-virtual {v11}, Ljava/lang/Object;->hashCode()I

    move-result v14

    const/4 v15, 0x1

    :cond_1
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v15, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "content://com.google.android.apps.plus.content.ApiProvider/plusone"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    const/4 v15, 0x0

    :cond_2
    new-instance v2, Lcom/google/android/apps/plus/api/WritePlusOneOperation;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v8, p3

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/plus/api/WritePlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/api/ApiaryApiInfo;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->start()V

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->getErrorCode()I

    move-result v5

    const/16 v6, 0xc8

    if-ne v5, v6, :cond_8

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->getPlusones()Lcom/google/api/services/pos/model/Plusones;

    move-result-object v16

    sget-object v6, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v6

    :try_start_1
    sget-object v5, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/api/services/pos/model/Plusones;

    if-eqz v11, :cond_4

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    move-object/from16 v0, v16

    iget-object v8, v0, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    if-eq v5, v8, :cond_4

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    iput-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    if-eqz v12, :cond_3

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    if-eqz v5, :cond_3

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    if-eqz v5, :cond_3

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    if-eqz v5, :cond_3

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    iput-object v12, v5, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    :cond_3
    const/4 v15, 0x1

    :cond_4
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_2
    if-eqz v15, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "content://com.google.android.apps.plus.content.ApiProvider/plusone"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_5
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->getErrorCode()I

    move-result v5

    const/16 v6, 0xc8

    if-ne v5, v6, :cond_b

    const/4 v5, 0x1

    :goto_3
    return v5

    :cond_6
    const/4 v9, 0x0

    goto/16 :goto_0

    :cond_7
    const/4 v5, -0x1

    goto/16 :goto_1

    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5

    :catchall_1
    move-exception v5

    monitor-exit v6

    throw v5

    :cond_8
    sget-object v6, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v6

    :try_start_2
    sget-object v5, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/api/services/pos/model/Plusones;

    if-eqz v11, :cond_a

    invoke-virtual {v11}, Ljava/lang/Object;->hashCode()I

    move-result v5

    if-ne v14, v5, :cond_a

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    if-eq v5, v13, :cond_a

    iput-object v13, v11, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    iput-object v12, v5, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    if-eqz v12, :cond_9

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    if-eqz v5, :cond_9

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    if-eqz v5, :cond_9

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    if-eqz v5, :cond_9

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    iput-object v12, v5, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    :cond_9
    const/4 v15, 0x1

    :cond_a
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_2

    :catchall_2
    move-exception v5

    monitor-exit v6

    throw v5

    :cond_b
    const/4 v5, 0x0

    goto :goto_3
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/plus/content/EsApiProvider;->sMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :pswitch_0
    if-eqz p3, :cond_0

    array-length v2, p3

    if-ne v2, v0, :cond_0

    aget-object v1, p3, v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v2, v1}, Landroid/support/v4/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://com.google.android.apps.plus.content.ApiProvider/plusone"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/net/Uri;

    sget-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.plusones"

    goto :goto_0

    :pswitch_1
    const-string v0, "vnd.android.cursor.item/vnd.google.android.apps.plus.account"

    goto :goto_0

    :pswitch_2
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.activitypreview"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    const-wide/16 v0, 0x0

    invoke-static {p1, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    if-eqz p4, :cond_0

    array-length v0, p4

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v9

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getApiaryApiInfo(Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    move-result-object v6

    const/4 v0, 0x0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    if-eq v1, v2, :cond_a

    new-instance v0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v6}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/api/ApiaryApiInfo;)Ljava/util/Map;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JLjava/util/Map;)V

    move-object v7, v0

    :goto_1
    const-string v0, "skipCache"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    sget-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    invoke-static {v3, v0, p4}, Lcom/google/android/apps/plus/content/EsApiProvider;->getUncachedUrls(ZLandroid/support/v4/util/LruCache;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    invoke-direct {p0, v9, v6, v4}, Lcom/google/android/apps/plus/content/EsApiProvider;->updatePlusoneEntries(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/ApiaryApiInfo;Ljava/util/List;)Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    move-result-object v0

    move-object v8, v0

    :goto_2
    if-eqz v9, :cond_1

    const-string v0, "no_preview"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v10, Ljava/lang/Thread;

    new-instance v0, Lcom/google/android/apps/plus/content/EsApiProvider$1;

    move-object v1, p0

    move-object v2, p4

    move-object v5, v9

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsApiProvider$1;-><init>(Lcom/google/android/apps/plus/content/EsApiProvider;[Ljava/lang/String;ZLjava/util/List;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V

    invoke-direct {v10, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    :cond_1
    invoke-direct {p0, p4, v8}, Lcom/google/android/apps/plus/content/EsApiProvider;->buildPlusOneCursorFromCache([Ljava/lang/String;Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v7, :cond_2

    iget-object v0, v8, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_READ_PLUSONES:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v9, v7, v0}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V

    :cond_2
    move-object v0, v1

    goto/16 :goto_0

    :cond_3
    new-instance v0, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;-><init>()V

    move-object v8, v0

    goto :goto_2

    :cond_4
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_READ_PLUSONES_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_3

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v9

    new-instance v8, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v0, Lcom/google/android/apps/plus/external/PlatformContract$AccountContent;->COLUMNS:[Ljava/lang/String;

    invoke-direct {v8, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    if-eqz v9, :cond_6

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_5

    array-length v0, v6

    if-lez v0, :cond_5

    new-instance v0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    aget-object v3, v6, v3

    const/4 v5, 0x0

    aget-object v5, v6, v5

    invoke-static {v5, v4}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v7, v0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V

    invoke-static {v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/api/ApiaryApiInfo;)Ljava/util/Map;

    move-result-object v5

    :cond_5
    new-instance v0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JLjava/util/Map;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_GET_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v9, v0, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_6
    move-object v0, v8

    goto/16 :goto_0

    :pswitch_2
    if-eqz p4, :cond_0

    array-length v0, p4

    if-lez v0, :cond_0

    invoke-static {p4}, Lcom/google/android/apps/plus/content/EsApiProvider;->deserializeSelectionArgs([Ljava/lang/String;)[Lcom/google/android/apps/plus/content/PreviewRequestData;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v0, v2, :cond_7

    const/4 v0, 0x1

    :goto_4
    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_4

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    const-string v2, "hostKey"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/plus/content/EsApiProvider;->getApiaryApiInfo(Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    move-result-object v2

    const-string v3, "skipCache"

    invoke-virtual {p1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    invoke-static {v3, v4, v1}, Lcom/google/android/apps/plus/content/EsApiProvider;->getUncachedKeys(ZLandroid/support/v4/util/LruCache;[Lcom/google/android/apps/plus/content/PreviewRequestData;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_9

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/plus/content/EsApiProvider;->updatePreviewEntries(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/ApiaryApiInfo;Ljava/util/List;)Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    move-result-object v0

    :goto_5
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/EsApiProvider;->buildPreviewCursorFromCache([Lcom/google/android/apps/plus/content/PreviewRequestData;Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;)Landroid/database/Cursor;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    new-instance v0, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;-><init>()V

    goto :goto_5

    :cond_a
    move-object v7, v0

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v3, v4, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1}, Ljava/lang/SecurityException;-><init>()V

    throw v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/google/android/apps/plus/content/EsApiProvider;->sMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_2
    :goto_1
    move v1, v2

    :cond_3
    :goto_2
    return v1

    :pswitch_0
    if-eqz p4, :cond_2

    array-length v3, p4

    if-ne v3, v1, :cond_2

    aget-object v3, p4, v2

    invoke-direct {p0, p1, p2, v3}, Lcom/google/android/apps/plus/content/EsApiProvider;->writePlusOne(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    move v1, v2

    goto :goto_2

    :pswitch_1
    sget-object v3, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v3

    :try_start_0
    sget-object v1, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1}, Landroid/support/v4/util/LruCache;->evictAll()V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v3, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    monitor-enter v3

    :try_start_1
    sget-object v1, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1}, Landroid/support/v4/util/LruCache;->evictAll()V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "content://com.google.android.apps.plus.content.ApiProvider/account"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "content://com.google.android.apps.plus.content.ApiProvider/plusone"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v3

    throw v1

    :catchall_1
    move-exception v1

    monitor-exit v3

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
