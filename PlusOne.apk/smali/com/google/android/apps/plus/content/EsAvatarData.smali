.class public final Lcom/google/android/apps/plus/content/EsAvatarData;
.super Ljava/lang/Object;
.source "EsAvatarData.java"


# static fields
.field private static final AVATAR_URL_PROJECTION:[Ljava/lang/String;

.field private static sDefaultAvatarMedium:Landroid/graphics/Bitmap;

.field private static sDefaultAvatarMediumRound:Landroid/graphics/Bitmap;

.field private static sDefaultAvatarSmall:Landroid/graphics/Bitmap;

.field private static sDefaultAvatarSmallRound:Landroid/graphics/Bitmap;

.field private static sDefaultAvatarTiny:Landroid/graphics/Bitmap;

.field private static sDefaultAvatarTinyRound:Landroid/graphics/Bitmap;

.field private static final sFifeAbbrs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sFifeHosts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sMediumAvatarSize:I

.field public static sRoundAvatarsEnabled:Z

.field private static sSmallAvatarSize:I

.field private static sTinyAvatarSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    sput-boolean v3, Lcom/google/android/apps/plus/content/EsAvatarData;->sRoundAvatarsEnabled:Z

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    const-string v1, "avatar"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->AVATAR_URL_PROJECTION:[Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeHosts:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeAbbrs:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeHosts:Ljava/util/HashMap;

    const-string v1, "lh3.googleusercontent.com"

    const-string v2, "~3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeAbbrs:Ljava/util/HashMap;

    const-string v1, "~3"

    const-string v2, "lh3.googleusercontent.com"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeHosts:Ljava/util/HashMap;

    const-string v1, "lh4.googleusercontent.com"

    const-string v2, "~4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeAbbrs:Ljava/util/HashMap;

    const-string v1, "~4"

    const-string v2, "lh4.googleusercontent.com"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeHosts:Ljava/util/HashMap;

    const-string v1, "lh5.googleusercontent.com"

    const-string v2, "~5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeAbbrs:Ljava/util/HashMap;

    const-string v1, "~5"

    const-string v2, "lh5.googleusercontent.com"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeHosts:Ljava/util/HashMap;

    const-string v1, "lh6.googleusercontent.com"

    const-string v2, "~6"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeAbbrs:Ljava/util/HashMap;

    const-string v1, "~6"

    const-string v2, "lh6.googleusercontent.com"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static compressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_0
    const/4 v3, 0x0

    const-string v4, "https://"

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v3, 0x8

    :cond_1
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const-string v4, "/photo.jpg"

    invoke-virtual {p0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    add-int/lit8 v0, v0, -0x9

    :cond_2
    const/16 v4, 0x2f

    invoke-virtual {p0, v4, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    const/4 v4, -0x1

    if-ne v2, v4, :cond_5

    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_3
    const-string v4, "http://"

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v3, 0x7

    goto :goto_1

    :cond_4
    const-string v4, "//"

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v3, 0x2

    goto :goto_1

    :cond_5
    sget-object v4, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeHosts:Ljava/util/HashMap;

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_2

    :cond_6
    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static getAvatarSizeInPx(Landroid/content/Context;I)I
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyAvatarSize(Landroid/content/Context;)I

    move-result v0

    goto :goto_0

    :pswitch_1
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallAvatarSize(Landroid/content/Context;)I

    move-result v0

    goto :goto_0

    :pswitch_2
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumAvatarSize(Landroid/content/Context;)I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getAvatarUrlSignature(Ljava/lang/String;)I
    .locals 2
    .param p0    # Ljava/lang/String;

    const/4 v1, 0x1

    if-nez p0, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    if-eqz v0, :cond_2

    if-ne v0, v1, :cond_0

    :cond_2
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static getMediumAvatarSize(Landroid/content/Context;)I
    .locals 2
    .param p0    # Landroid/content/Context;

    sget v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sMediumAvatarSize:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->medium_avatar_dimension:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sMediumAvatarSize:I

    :cond_0
    sget v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sMediumAvatarSize:I

    return v0
.end method

.method public static getMediumDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v2, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarMedium:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_avatar:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarMedium:Landroid/graphics/Bitmap;

    :cond_0
    sget-object v2, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarMedium:Landroid/graphics/Bitmap;

    return-object v2
.end method

.method public static getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Z

    if-eqz p1, :cond_1

    sget-boolean v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sRoundAvatarsEnabled:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarMediumRound:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/util/ImageUtils;->getRoundedBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarMediumRound:Landroid/graphics/Bitmap;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarMediumRound:Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static getSmallAvatarSize(Landroid/content/Context;)I
    .locals 2
    .param p0    # Landroid/content/Context;

    sget v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sSmallAvatarSize:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->avatar_dimension:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sSmallAvatarSize:I

    :cond_0
    sget v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sSmallAvatarSize:I

    return v0
.end method

.method public static getSmallDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarSmall:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallAvatarSize(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarSmall:Landroid/graphics/Bitmap;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarSmall:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static getSmallDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Z

    if-eqz p1, :cond_1

    sget-boolean v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sRoundAvatarsEnabled:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarSmallRound:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/util/ImageUtils;->getRoundedBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarSmallRound:Landroid/graphics/Bitmap;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarSmallRound:Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static getTinyAvatarSize(Landroid/content/Context;)I
    .locals 2
    .param p0    # Landroid/content/Context;

    sget v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sTinyAvatarSize:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tiny_avatar_dimension:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sTinyAvatarSize:I

    :cond_0
    sget v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sTinyAvatarSize:I

    return v0
.end method

.method private static getTinyDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarTiny:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyAvatarSize(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarTiny:Landroid/graphics/Bitmap;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarTiny:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static getTinyDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Z

    if-eqz p1, :cond_1

    sget-boolean v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sRoundAvatarsEnabled:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarTinyRound:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/util/ImageUtils;->getRoundedBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarTinyRound:Landroid/graphics/Bitmap;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarTinyRound:Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static loadAvatarUrl(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "contacts"

    sget-object v2, Lcom/google/android/apps/plus/content/EsAvatarData;->AVATAR_URL_PROJECTION:[Ljava/lang/String;

    const-string v3, "gaia_id=?"

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v8}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v5

    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x7e

    if-ne v2, v3, :cond_2

    const/16 v2, 0x2f

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeAbbrs:Ljava/util/HashMap;

    invoke-virtual {p0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const-string v2, "/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "photo.jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
