.class public final Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;
.super Ljava/lang/Object;
.source "EsDeepLinkInstallsData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeepLinkInstall"
.end annotation


# instance fields
.field public final authorName:Ljava/lang/String;

.field public final creationSource:Ljava/lang/String;

.field public final data:Ljava/lang/String;

.field public final launchSource:Ljava/lang/String;

.field public final packageName:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->authorName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->creationSource:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->packageName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->launchSource:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->data:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Landroid/database/Cursor;)Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;
    .locals 6
    .param p0    # Landroid/database/Cursor;

    const-string v5, ""

    const-string v0, "launch_source"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x0

    const-string v1, "stream_install"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "embed_deep_link"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getDeepLinkId()Ljava/lang/String;

    move-result-object v5

    :cond_1
    new-instance v0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;

    const-string v1, "name"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "source_name"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "package_name"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_2
    const-string v1, "stream_install_interactive_post"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "embed_appinvite"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "DeepLinkInstall: authorName=%s, appName=%s, packageName=%s, launchSource=%s, data=%s"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->authorName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->creationSource:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->packageName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->launchSource:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->data:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
