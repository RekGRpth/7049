.class public final Lcom/google/android/apps/plus/content/EsEmotiShareData;
.super Ljava/lang/Object;
.source "EsEmotiShareData.java"


# static fields
.field public static final EMOTISHARE_PROJECTION:[Ljava/lang/String;

.field private static final sEmotiShareSyncLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsEmotiShareData;->sEmotiShareSyncLock:Ljava/lang/Object;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "data"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "generation"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/content/EsEmotiShareData;->EMOTISHARE_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public static cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const/4 v2, 0x0

    const-string v1, "emotishare_data"

    invoke-virtual {p0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x3

    const-string v2, "EsEmotiShareData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cleanupData deleted EmotiShares: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static doSync$1ef5a3b5(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z
    .locals 25
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    const-string v2, "EmotiShare"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsEmotiShareData;->cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$array;->emotishare_id:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v13

    sget v3, Lcom/google/android/apps/plus/R$array;->emotishare_release_generation:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v14

    sget v3, Lcom/google/android/apps/plus/R$array;->emotishare_name:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v15

    sget v3, Lcom/google/android/apps/plus/R$array;->emotishare_type:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v16

    sget v3, Lcom/google/android/apps/plus/R$array;->emotishare_category:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v17

    sget v3, Lcom/google/android/apps/plus/R$array;->emotishare_share_text:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v18

    sget v3, Lcom/google/android/apps/plus/R$array;->emotishare_description:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v19

    sget v3, Lcom/google/android/apps/plus/R$array;->emotishare_icon_uri:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v20

    sget v3, Lcom/google/android/apps/plus/R$array;->emotishare_image_uri:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v21

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    const/4 v2, 0x3

    new-array v0, v2, [J

    move-object/from16 v24, v0

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/plus/util/Property;->EMOTISHARE_GEN1_DATE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    aput-wide v3, v24, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/apps/plus/util/Property;->EMOTISHARE_GEN2_DATE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    aput-wide v3, v24, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/google/android/apps/plus/util/Property;->EMOTISHARE_GEN3_DATE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    aput-wide v3, v24, v2

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    move v12, v2

    :goto_1
    array-length v2, v13

    if-ge v12, v2, :cond_2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    aget-object v2, v17, v12

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    aget-object v2, v16, v12

    aget-object v3, v15, v12

    aget-object v5, v21, v12

    aget-object v6, v19, v12

    invoke-direct {v7, v2, v3, v5, v6}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    aget v3, v13, v12

    aget-object v5, v18, v12

    aget-object v6, v20, v12

    aget v8, v14, v12

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;-><init>(ILjava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedEmotishare;I)V

    aget v3, v14, v12

    add-int/lit8 v3, v3, -0x1

    if-ltz v3, :cond_1

    move-object/from16 v0, v24

    array-length v4, v0

    if-ge v3, v4, :cond_1

    aget-wide v3, v24, v3

    cmp-long v3, v22, v3

    if-ltz v3, :cond_1

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v2, v12, 0x1

    move v12, v2

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-virtual {v10}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v11}, Lcom/google/android/apps/plus/content/EsEmotiShareData;->insertEmotiShares(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)I

    move-result v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method public static ensureSynced(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    const-string v2, "Exp sync"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p0, p1, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsEmotiShareData;->syncAll(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)Z

    move-result v0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    return v0
.end method

.method private static insertEmotiShares(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)I
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/DbEmotishareMetadata;",
            ">;)I"
        }
    .end annotation

    const/4 v11, 0x0

    const/4 v0, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsEmotiShareData;->toContentValues(Lcom/google/android/apps/plus/content/DbEmotishareMetadata;)Landroid/content/ContentValues;

    move-result-object v6

    if-eqz v6, :cond_0

    const-string v7, "emotishare_data"

    const/4 v8, 0x0

    invoke-virtual {v1, v7, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    add-int/lit8 v0, v0, 0x1

    :cond_0
    const/4 v7, 0x3

    const-string v8, "EsEmotiShareData"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Insert: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v7

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v7

    :cond_1
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-static {p0, p1, v4, v5}, Lcom/google/android/apps/plus/content/EsEmotiShareData;->saveSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/plus/content/EsProvider;->EMOTISHARE_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_2
    return v0
.end method

.method private static querySyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    :try_start_0
    const-string v1, "SELECT last_emotishare_sync_time  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    :goto_0
    return-wide v1

    :catch_0
    move-exception v1

    const-wide/16 v1, -0x1

    goto :goto_0
.end method

.method private static saveSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J

    const/4 v4, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "last_emotishare_sync_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "account_status"

    invoke-virtual {v0, v2, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNT_STATUS_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void
.end method

.method public static syncAll(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)Z
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p3    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p4    # Z

    sget-object v6, Lcom/google/android/apps/plus/content/EsEmotiShareData;->sEmotiShareSyncLock:Ljava/lang/Object;

    monitor-enter v6

    if-nez p4, :cond_0

    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsEmotiShareData;->querySyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long v0, v7, v3

    const-wide/16 v7, 0x0

    cmp-long v5, v0, v7

    if-lez v5, :cond_0

    const-wide/32 v7, 0xea60

    cmp-long v5, v0, v7

    if-gez v5, :cond_0

    const/4 v2, 0x1

    monitor-exit v6

    :goto_0
    return v2

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/content/EsEmotiShareData;->doSync$1ef5a3b5(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static {p0, p1, v7, v8}, Lcom/google/android/apps/plus/content/EsEmotiShareData;->saveSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    :cond_1
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5
.end method

.method private static toContentValues(Lcom/google/android/apps/plus/content/DbEmotishareMetadata;)Landroid/content/ContentValues;
    .locals 6
    .param p0    # Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    const/4 v3, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->getEmbed()Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->serialize(Lcom/google/android/apps/plus/content/DbEmotishareMetadata;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "type"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "data"

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v4, "generation"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->getGeneration()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :catch_0
    move-exception v4

    goto :goto_0
.end method
