.class public final Lcom/google/android/apps/plus/content/EsEventData;
.super Ljava/lang/Object;
.source "EsEventData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;,
        Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;,
        Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;,
        Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;,
        Lcom/google/android/apps/plus/content/EsEventData$InviteeList;,
        Lcom/google/android/apps/plus/content/EsEventData$EventPerson;,
        Lcom/google/android/apps/plus/content/EsEventData$EventComment;,
        Lcom/google/android/apps/plus/content/EsEventData$EventActivity;,
        Lcom/google/android/apps/plus/content/EsEventData$ResolvedPerson;,
        Lcom/google/android/apps/plus/content/EsEventData$EventThemesQuery;
    }
.end annotation


# static fields
.field public static final EVENT_ACTIVITY_JSON:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventActivity;",
            ">;"
        }
    .end annotation
.end field

.field public static final EVENT_COALESCED_FRAME_JSON:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;",
            ">;"
        }
    .end annotation
.end field

.field public static final EVENT_COMMENT_JSON:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventComment;",
            ">;"
        }
    .end annotation
.end field

.field private static final EVENT_INSTANT_SHARE_END_TIME_QUERY_PROJECTION:[Ljava/lang/String;

.field public static final EVENT_PERSON_JSON:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventPerson;",
            ">;"
        }
    .end annotation
.end field

.field private static final EVENT_QUERY_PROJECTION:[Ljava/lang/String;

.field private static final EVENT_RESUME_TOKEN_QUERY_PROJECTION:[Ljava/lang/String;

.field public static final INVITEE_LIST_JSON:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$InviteeList;",
            ">;"
        }
    .end annotation
.end field

.field private static final SYNC_QUERY_PROJECTION:[Ljava/lang/String;

.field private static final mSyncLock:Ljava/lang/Object;

.field private static final sEventOperationSyncObject:Ljava/lang/Object;

.field private static sEventThemePlaceholderDir:Ljava/io/File;

.field private static sEventThemesLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->sEventOperationSyncObject:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->mSyncLock:Ljava/lang/Object;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "event_id"

    aput-object v1, v0, v3

    const-string v1, "polling_token"

    aput-object v1, v0, v4

    const-string v1, "resume_token"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "event_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->SYNC_QUERY_PROJECTION:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "event_data"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_QUERY_PROJECTION:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "instant_share_end_time"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_INSTANT_SHARE_END_TIME_QUERY_PROJECTION:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "resume_token"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_RESUME_TOKEN_QUERY_PROJECTION:[Ljava/lang/String;

    const-class v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/EsJson;->getSimpleJson(Ljava/lang/Class;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_ACTIVITY_JSON:Lcom/google/android/apps/plus/json/EsJson;

    const-class v0, Lcom/google/android/apps/plus/content/EsEventData$EventComment;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/EsJson;->getSimpleJson(Ljava/lang/Class;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_COMMENT_JSON:Lcom/google/android/apps/plus/json/EsJson;

    const-class v0, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/EsJson;->getSimpleJson(Ljava/lang/Class;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_PERSON_JSON:Lcom/google/android/apps/plus/json/EsJson;

    const-class v0, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;

    new-array v1, v5, [Ljava/lang/Object;

    const-class v2, Lcom/google/api/services/plusi/model/InviteeJson;

    aput-object v2, v1, v3

    const-string v2, "invitees"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/json/EsJson;->buildJson(Ljava/lang/Class;[Ljava/lang/Object;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->INVITEE_LIST_JSON:Lcom/google/android/apps/plus/json/EsJson;

    const-class v0, Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;

    new-array v1, v5, [Ljava/lang/Object;

    sget-object v2, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_PERSON_JSON:Lcom/google/android/apps/plus/json/EsJson;

    aput-object v2, v1, v3

    const-string v2, "people"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/json/EsJson;->buildJson(Ljava/lang/Class;[Ljava/lang/Object;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_COALESCED_FRAME_JSON:Lcom/google/android/apps/plus/json/EsJson;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsEventData;->sEventThemesLock:Ljava/lang/Object;

    return-void
.end method

.method public static canAddPhotos(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z
    .locals 2
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventOptions;->openPhotoAcl:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventOptions;->openPhotoAcl:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    goto :goto_0
.end method

.method public static canInviteOthers(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/EsAccount;)Z
    .locals 6
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v2, v3

    :cond_1
    :goto_0
    return v2

    :cond_2
    iget-object v4, p0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Invitee;->inviter:Lcom/google/api/services/plusi/model/EmbedsPerson;

    if-eqz v4, :cond_5

    move v0, v2

    :goto_1
    iget-object v4, p0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventOptions;->openEventAcl:Ljava/lang/Boolean;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventOptions;->openEventAcl:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_6

    :cond_3
    move v1, v2

    :goto_2
    if-eqz v0, :cond_4

    if-nez v1, :cond_1

    :cond_4
    move v2, v3

    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_1

    :cond_6
    move v1, v3

    goto :goto_2
.end method

.method public static canRsvp(Lcom/google/api/services/plusi/model/PlusEvent;)Z
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->isBroadcastView:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->isBroadcastView:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static canViewerAddPhotos(Lcom/google/api/services/plusi/model/PlusEvent;)Z
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Invitee;->canUploadPhotos:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Invitee;->canUploadPhotos:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static copyRsvpFromSummary(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 4
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/InviteeSummary;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    const-string v3, "INVITED"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    invoke-static {p0, p1, v2}, Lcom/google/android/apps/plus/content/EsEventData;->setViewerInfoRsvp(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public static deleteEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v3, "events"

    const-string v4, "event_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    invoke-virtual {v1, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->disableInstantShare(Landroid/content/Context;)V

    :cond_0
    if-lez v0, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_1
    const-string v3, "EsEventData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v4, "EsEventData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "[DELETE_EVENT], id: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v2, :cond_3

    const-string v3, "; disable IS"

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :cond_3
    const-string v3, ""

    goto :goto_0
.end method

.method public static deletePhotosFromEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    new-array v2, v4, [Ljava/lang/String;

    const-string v4, "photo_id IN("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    :goto_1
    if-ltz v1, :cond_2

    const-string v4, "?,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v1

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "event_activities"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public static disableInstantShare(Landroid/content/Context;)V
    .locals 9
    .param p0    # Landroid/content/Context;

    const-wide/16 v5, 0x0

    const/4 v2, 0x0

    const-string v0, "EsEventData"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsEventData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "#disableInstantShare; now: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-wide v7, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/content/EsEventData;->enableInstantShareInternal(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    return-void
.end method

.method public static enableInstantShare(Landroid/content/Context;ZLcom/google/api/services/plusi/model/PlusEvent;)V
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Z
    .param p2    # Lcom/google/api/services/plusi/model/PlusEvent;

    const/4 v3, 0x4

    const-string v0, "EsEventData"

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsEventData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#enableInstantShare; event: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p2, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/AlarmManager;

    iget-object v0, p2, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/phone/Intents;->getEventFinishedIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v10

    invoke-static {p2}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v9, v10}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    if-eqz p1, :cond_2

    const-wide/16 v0, 0x1388

    add-long/2addr v0, v5

    cmp-long v0, v0, v7

    if-gez v0, :cond_2

    const-string v0, "EsEventData"

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "EsEventData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#enableInstantShare; start IS; now: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", end: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", wake in: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sub-long v2, v7, v5

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p2, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    iget-object v3, p2, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/content/EsEventData;->enableInstantShareInternal(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    const/4 v0, 0x0

    invoke-virtual {v9, v0, v7, v8, v10}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    :goto_0
    return-void

    :cond_2
    const-string v0, "EsEventData"

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "EsEventData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#enableInstantShare; event over; now: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", end: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->disableInstantShare(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private static enableInstantShareInternal(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 18
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # J
    .param p7    # J

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    if-eqz p2, :cond_5

    const/4 v4, 0x1

    :goto_0
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareStartTime(Landroid/content/Context;)J

    move-result-wide v5

    const-wide/16 v13, -0x1

    cmp-long v13, v5, v13

    if-eqz v13, :cond_0

    move-wide/from16 p5, v5

    :cond_0
    if-eqz v4, :cond_1

    const-string v13, "auto_upload_account_name"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v13, "auto_upload_account_type"

    const-string v14, "com.google"

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v13, "instant_share_eventid"

    move-object/from16 v0, p2

    invoke-virtual {v12, v13, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v13, "instant_share_starttime"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-wide/32 v13, 0x1b77400

    add-long v13, v13, p5

    move-wide/from16 v0, p7

    invoke-static {v0, v1, v13, v14}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    if-eqz p1, :cond_2

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v14

    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    if-eqz p2, :cond_6

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object p2, v16, v17

    const-string v17, "instant_share_end_time"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v13, "events"

    const-string v17, "event_id=?"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v14, v13, v15, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_2
    :goto_1
    const-string v13, "instant_share_endtime"

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v13, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->SETTINGS_URI:Landroid/net/Uri;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v11, v13, v12, v14, v15}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    if-eqz v4, :cond_7

    if-nez v9, :cond_3

    invoke-static/range {p0 .. p3}, Lcom/google/android/apps/plus/phone/Intents;->getViewEventActivityNotificationIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v10

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-static {v0, v1, v10}, Lcom/google/android/apps/plus/util/NotificationUtils;->notifyInstantShareEnabled(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    :cond_3
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/phone/InstantUpload;->ensureSyncEnabled$1f9c1b47(Lcom/google/android/apps/plus/content/EsAccount;)V

    :goto_2
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->startMonitoring(Landroid/content/Context;)V

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_4

    new-instance v13, Lcom/google/android/apps/plus/content/EsEventData$2;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v13, v0, v3, v1, v2}, Lcom/google/android/apps/plus/content/EsEventData$2;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    invoke-static {v13}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    :cond_4
    return-void

    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_0

    :cond_6
    const-string v13, "instant_share_end_time"

    invoke-virtual {v15, v13}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v13, "events"

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v13, v15, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    :cond_7
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/util/NotificationUtils;->cancelInstantShareEnabled(Landroid/content/Context;)V

    goto :goto_2
.end method

.method private static ensureFreshEventThemes(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v7

    if-nez v7, :cond_2

    :cond_0
    sget-object v8, Lcom/google/android/apps/plus/content/EsEventData;->sEventThemesLock:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v7, "SELECT event_themes_sync_time  FROM account_status"

    const/4 v9, 0x0

    invoke-static {v2, v7, v9}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v3

    :goto_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long v0, v9, v3

    const-wide/32 v9, 0x5265c00

    cmp-long v7, v0, v9

    if-lez v7, :cond_1

    new-instance v6, Lcom/google/android/apps/plus/api/GetEventThemesOperation;

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-direct {v6, p0, p1, v7, v9}, Lcom/google/android/apps/plus/api/GetEventThemesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/GetEventThemesOperation;->start()V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/GetEventThemesOperation;->hasError()Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "EsEventData"

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/api/GetEventThemesOperation;->logError(Ljava/lang/String;)V

    :cond_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    return-void

    :catch_0
    move-exception v5

    const-wide/16 v3, -0x1

    goto :goto_0

    :catchall_0
    move-exception v7

    monitor-exit v8

    throw v7
.end method

.method public static getDisplayTime(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)J
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/api/services/plusi/model/PlusEvent;

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    if-nez p2, :cond_0

    const-wide/16 v9, 0x0

    :goto_0
    return-wide v9

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-array v4, v2, [Ljava/lang/String;

    iget-object v1, p2, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    aput-object v1, v4, v6

    const-wide/16 v9, 0x0

    const-string v1, "events"

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "display_time"

    aput-object v3, v2, v6

    const-string v3, "event_id=?"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v9

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "events"

    const-string v3, "event_id=?"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v4, v2

    move-object v2, p3

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method public static getEventActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-wide/16 v8, 0x0

    :try_start_0
    const-string v1, "SELECT display_time FROM events WHERE event_id = ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    :goto_0
    const-string v1, "event_activities"

    const-string v3, "event_id = ? AND timestamp >= ?"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    aput-object p2, v4, v6

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v7

    const-string v7, "timestamp DESC"

    move-object v2, p3

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J
    .locals 4
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;

    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/32 v2, 0x6ddd00

    add-long/2addr v0, v2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method public static getEventFromServer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    sget-object v7, Lcom/google/android/apps/plus/content/EsEventData;->sEventOperationSyncObject:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    new-instance v0, Lcom/google/android/apps/plus/api/GetEventOperation;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/GetEventOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetEventOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetEventOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "EsEventData"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/api/GetEventOperation;->logError(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetEventOperation;->hasError()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v7

    throw v1
.end method

.method private static getEventInstantShareEndTime(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)J
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    iget-object v1, p2, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p2, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_INSTANT_SHARE_END_TIME_QUERY_PROJECTION:[Ljava/lang/String;

    invoke-static {p0, p1, v1, v2}, Lcom/google/android/apps/plus/content/EsEventData;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :goto_0
    return-wide v1

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    invoke-static {p2}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v1

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static getEventName(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    :try_start_0
    const-string v1, "SELECT name FROM events WHERE event_id = ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v1

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getEventPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPlusOneData;
    .locals 9
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    const-string v1, "events"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "plus_one_data"

    aput-object v0, v2, v6

    const-string v3, "activity_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-nez v8, :cond_0

    :goto_0
    return-object v5

    :cond_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v5, Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-direct {v5}, Lcom/google/android/apps/plus/content/DbPlusOneData;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static getEventResolvedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "event_people_view"

    const-string v3, "event_id = ?"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v4, v2

    move-object v2, p3

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method public static getEventResumeToken(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    sget-object v2, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_RESUME_TOKEN_QUERY_PROJECTION:[Ljava/lang/String;

    invoke-static {p0, p1, p2, v2}, Lcom/google/android/apps/plus/content/EsEventData;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method public static getEventTheme(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I
    .param p3    # [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    const-string v3, "is_default!=0"

    const/4 v4, 0x0

    :goto_0
    const-string v1, "event_themes"

    const-string v7, "theme_id"

    move-object v2, p3

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    return-object v8

    :cond_0
    const-string v3, "theme_id=?"

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    goto :goto_0

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    invoke-static {p0, p1, v5}, Lcom/google/android/apps/plus/content/EsEventData;->ensureFreshEventThemes(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    const-string v1, "event_themes"

    const-string v7, "theme_id"

    move-object v2, p3

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    goto :goto_1
.end method

.method private static getEventThemePlaceholderDir(Landroid/content/Context;)Ljava/io/File;
    .locals 4
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->sEventThemePlaceholderDir:Ljava/io/File;

    if-nez v1, :cond_0

    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "event_themes"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/plus/content/EsEventData;->sEventThemePlaceholderDir:Ljava/io/File;

    :cond_0
    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->sEventThemePlaceholderDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    :try_start_0
    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->sEventThemePlaceholderDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    sget-object v1, Lcom/google/android/apps/plus/content/EsEventData;->sEventThemePlaceholderDir:Ljava/io/File;

    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "EsEventData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot create event theme placeholder directory: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/apps/plus/content/EsEventData;->sEventThemePlaceholderDir:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    sput-object v1, Lcom/google/android/apps/plus/content/EsEventData;->sEventThemePlaceholderDir:Ljava/io/File;

    goto :goto_0
.end method

.method public static getEventThemes(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I
    .param p3    # [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p0, p1, v4}, Lcom/google/android/apps/plus/content/EsEventData;->ensureFreshEventThemes(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "event_themes"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v2, "is_featured="

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v7, "sort_order ASC"

    move-object v2, p3

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getImageUrl(Lcom/google/api/services/plusi/model/Theme;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/google/api/services/plusi/model/Theme;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeImage(Lcom/google/api/services/plusi/model/Theme;)Lcom/google/api/services/plusi/model/ThemeImage;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/api/services/plusi/model/ThemeImage;->url:Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getInviteeSummary(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Lcom/google/api/services/plusi/model/InviteeSummary;
    .locals 5
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p1    # Ljava/lang/String;

    iget-object v3, p0, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    if-nez v3, :cond_1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/InviteeSummary;

    iget-object v3, v2, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, v2, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    iget-object v3, v2, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    if-eqz v3, :cond_3

    iget-object v3, v2, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_3
    iget-object v3, v2, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object v1, v2

    goto :goto_0
.end method

.method private static getMyEventIds(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Set;
    .locals 10
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v1, "events"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "event_id"

    aput-object v0, v2, v3

    const-string v3, "mine = 1"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return-object v9
.end method

.method public static getMyEvents$13db9565(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "events"

    const-string v3, "mine AND source = 1"

    const-string v7, "end_time ASC"

    move-object v2, p2

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    return-object v8
.end method

.method public static getPlusEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Lcom/google/api/services/plusi/model/PlusEvent;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    sget-object v2, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_QUERY_PROJECTION:[Ljava/lang/String;

    invoke-static {p0, p1, p2, v2}, Lcom/google/android/apps/plus/content/EsEventData;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/PlusEvent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method public static getRsvpStatus(Lcom/google/api/services/plusi/model/PlusEvent;)I
    .locals 2
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpType(Lcom/google/api/services/plusi/model/PlusEvent;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CHECKIN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ATTENDING"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const-string v1, "MAYBE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    goto :goto_0

    :cond_2
    const-string v1, "NOT_ATTENDING"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "NOT_ATTENDING_AND_REMOVE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    const/4 v1, 0x3

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getRsvpType(Lcom/google/api/services/plusi/model/PlusEvent;)Ljava/lang/String;
    .locals 4
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz p0, :cond_3

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Invitee;->rsvpType:Ljava/lang/String;

    const-string v1, "CHECKIN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ATTENDING"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "MAYBE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "NOT_ATTENDING"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "NOT_ATTENDING_AND_REMOVE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "EsEventData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "EsEventData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[FILTER_RSVP_TYPE]; "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " not recognized"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v0, "NOT_RESPONDED"

    goto :goto_0

    :cond_3
    const-string v0, "NOT_RESPONDED"

    goto :goto_0
.end method

.method public static getThemeImage(Lcom/google/api/services/plusi/model/Theme;)Lcom/google/api/services/plusi/model/ThemeImage;
    .locals 5
    .param p0    # Lcom/google/api/services/plusi/model/Theme;

    const/4 v1, 0x0

    if-eqz p0, :cond_2

    iget-object v3, p0, Lcom/google/api/services/plusi/model/Theme;->image:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/api/services/plusi/model/Theme;->image:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/ThemeImage;

    if-eqz v2, :cond_0

    const-string v3, "LARGE"

    iget-object v4, v2, Lcom/google/api/services/plusi/model/ThemeImage;->aspectRatio:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "JPG"

    iget-object v4, v2, Lcom/google/api/services/plusi/model/ThemeImage;->format:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v1, v2

    goto :goto_0

    :cond_1
    if-nez v1, :cond_0

    const-string v3, "MOV"

    iget-object v4, v2, Lcom/google/api/services/plusi/model/ThemeImage;->format:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object v1, v2

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method public static getThemeMediaHeight(I)I
    .locals 2
    .param p0    # I

    int-to-float v0, p0

    const v1, 0x40570a3d

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static getThemeMediaWidth(Landroid/content/Context;)I
    .locals 4
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v0

    iget v1, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->shortDimension:I

    iget v2, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    div-int/lit8 v1, v1, 0x2

    :cond_0
    const/16 v2, 0x3e8

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    return v2
.end method

.method private static hasExistingInstantShareWindowShortened(Landroid/content/Context;Lcom/google/api/services/plusi/model/PlusEvent;)Z
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/api/services/plusi/model/PlusEvent;

    const/4 v4, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v0

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEndTime(Landroid/content/Context;)J

    move-result-wide v2

    const-wide/16 v5, -0x1

    cmp-long v5, v2, v5

    if-eqz v5, :cond_0

    cmp-long v5, v2, v0

    if-lez v5, :cond_0

    const/4 v4, 0x1

    goto :goto_0
.end method

.method public static insertEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;)V
    .locals 14
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p4    # Lcom/google/api/services/plusi/model/Update;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v1, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object v11, p1

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventInTransaction(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/util/List;Ljava/lang/Long;Ljava/util/List;Lcom/google/android/apps/plus/content/EsAccount;)Z

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p3

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/content/EsEventData;->validateInstantShare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p3

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsEventData;->hasExistingInstantShareWindowShortened(Landroid/content/Context;Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-static {p0, v1, v0}, Lcom/google/android/apps/plus/content/EsEventData;->enableInstantShare(Landroid/content/Context;ZLcom/google/api/services/plusi/model/PlusEvent;)V

    :cond_0
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v13, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void
.end method

.method public static insertEventActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventActivity;",
            ">;Z)V"
        }
    .end annotation

    const/4 v8, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const/4 v0, 0x0

    :try_start_0
    invoke-static {v1, p2, v0}, Lcom/google/android/apps/plus/content/EsEventData;->insertResumeTokenInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p4, :cond_0

    const/4 v4, 0x1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventActivitiesInTransaction(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;ZLjava/util/List;)V

    :cond_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void
.end method

.method private static insertEventActivitiesInTransaction(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;ZLjava/util/List;)V
    .locals 29
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # Ljava/lang/String;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventActivity;",
            ">;Z",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    const/4 v5, 0x1

    new-array v9, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v9, v5

    new-instance v25, Ljava/util/HashMap;

    invoke-direct/range {v25 .. v25}, Ljava/util/HashMap;-><init>()V

    const/16 v26, 0x0

    const-string v6, "event_activities"

    const/4 v5, 0x5

    new-array v7, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "_id"

    aput-object v8, v7, v5

    const/4 v5, 0x1

    const-string v8, "type"

    aput-object v8, v7, v5

    const/4 v5, 0x2

    const-string v8, "owner_gaia_id"

    aput-object v8, v7, v5

    const/4 v5, 0x3

    const-string v8, "timestamp"

    aput-object v8, v7, v5

    const/4 v5, 0x4

    const-string v8, "fingerprint"

    aput-object v8, v7, v5

    const-string v8, "event_id=?"

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v5, p1

    invoke-virtual/range {v5 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    :goto_0
    :try_start_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v19, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;-><init>(B)V

    new-instance v24, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;

    const/4 v5, 0x0

    move-object/from16 v0, v24

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;-><init>(B)V

    const/4 v5, 0x0

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v24

    iput-object v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;->id:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    move-object/from16 v0, v19

    iput v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->type:I

    const/4 v5, 0x2

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    iput-object v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->ownerGaiaId:Ljava/lang/String;

    const/4 v5, 0x3

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    move-object/from16 v0, v19

    iput-wide v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->timestamp:J

    const/4 v5, 0x4

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    move-object/from16 v0, v24

    iput v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;->fingerprint:I

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    move-object/from16 v2, v24

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    throw v5

    :cond_0
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    new-instance v28, Landroid/content/ContentValues;

    invoke-direct/range {v28 .. v28}, Landroid/content/ContentValues;-><init>()V

    new-instance v19, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;-><init>(B)V

    new-instance v23, Ljava/util/HashMap;

    invoke-direct/range {v23 .. v23}, Ljava/util/HashMap;-><init>()V

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_1
    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;

    iget v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    move-object/from16 v0, v19

    iput v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->type:I

    iget-object v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->ownerGaiaId:Ljava/lang/String;

    iget-wide v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->timestamp:J

    move-object/from16 v0, v19

    iput-wide v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->timestamp:J

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;

    iget-object v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    if-nez v5, :cond_3

    const/16 v17, 0x0

    :goto_2
    const/16 v20, 0x0

    const/16 v27, 0x0

    const/4 v14, 0x0

    const/16 v21, 0x0

    iget v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    const/16 v6, 0x64

    if-ne v5, v6, :cond_4

    invoke-static {}, Lcom/google/api/services/plusi/model/DataPhotoJson;->getInstance()Lcom/google/api/services/plusi/model/DataPhotoJson;

    move-result-object v5

    iget-object v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/api/services/plusi/model/DataPhotoJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/api/services/plusi/model/DataPhoto;

    if-eqz v20, :cond_2

    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v21

    :cond_2
    :goto_3
    if-nez v24, :cond_5

    invoke-virtual/range {v28 .. v28}, Landroid/content/ContentValues;->clear()V

    const-string v5, "event_id"

    move-object/from16 v0, v28

    move-object/from16 v1, p2

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "type"

    iget v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "timestamp"

    iget-wide v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->timestamp:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "owner_gaia_id"

    iget-object v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    move-object/from16 v0, v28

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "owner_name"

    iget-object v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerName:Ljava/lang/String;

    move-object/from16 v0, v28

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "data"

    iget-object v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    move-object/from16 v0, v28

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "url"

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "comment"

    move-object/from16 v0, v28

    invoke-virtual {v0, v5, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "fingerprint"

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "photo_id"

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "event_activities"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :goto_4
    iget v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    const/16 v6, 0x64

    if-ne v5, v6, :cond_1

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move-object/from16 v2, p2

    move-object/from16 v3, v23

    move-object/from16 v4, p5

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertEventPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPhoto;Ljava/lang/String;Ljava/util/Map;Ljava/util/List;)V

    const/16 v26, 0x1

    goto/16 :goto_1

    :cond_3
    iget-object v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v17

    goto/16 :goto_2

    :cond_4
    iget v5, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    const/4 v6, 0x5

    if-ne v5, v6, :cond_2

    sget-object v5, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_COMMENT_JSON:Lcom/google/android/apps/plus/json/EsJson;

    iget-object v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/json/EsJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/android/apps/plus/content/EsEventData$EventComment;

    if-eqz v16, :cond_2

    move-object/from16 v0, v16

    iget-object v14, v0, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->text:Ljava/lang/String;

    goto/16 :goto_3

    :cond_5
    move-object/from16 v0, v24

    iget v5, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;->fingerprint:I

    move/from16 v0, v17

    if-eq v0, v5, :cond_6

    invoke-virtual/range {v28 .. v28}, Landroid/content/ContentValues;->clear()V

    const-string v5, "data"

    iget-object v6, v13, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    move-object/from16 v0, v28

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "url"

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "comment"

    move-object/from16 v0, v28

    invoke-virtual {v0, v5, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "fingerprint"

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "event_activities"

    const-string v6, "_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    move-object/from16 v0, v24

    iget-object v10, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;->id:Ljava/lang/String;

    aput-object v10, v7, v8

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v5, v1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_6
    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    :cond_7
    if-nez p4, :cond_8

    invoke-interface/range {v25 .. v25}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_5
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;

    const-string v5, "event_activities"

    const-string v6, "_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    move-object/from16 v0, v24

    iget-object v10, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityStatus;->id:Ljava/lang/String;

    aput-object v10, v7, v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_5

    :cond_8
    invoke-virtual/range {v28 .. v28}, Landroid/content/ContentValues;->clear()V

    const-string v5, "activity_refresh_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "events"

    const-string v6, "event_id=?"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v5, v1, v6, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    if-eqz v26, :cond_9

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_EVENT_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p2

    invoke-static {v5, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    if-eqz p5, :cond_a

    move-object/from16 v0, p5

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    :goto_6
    return-void

    :cond_a
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v5, v0, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_6
.end method

.method public static insertEventHomeList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 18
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;)V"
        }
    .end annotation

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsEventData;->getMyEventIds(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/Set;

    move-result-object v6

    const/4 v2, 0x5

    new-array v7, v2, [I

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    move-object/from16 v2, p0

    move-object/from16 v5, p2

    move-object/from16 v9, p5

    move-object/from16 v10, p1

    :try_start_0
    invoke-static/range {v2 .. v10}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventListInTransaction(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;Ljava/util/Set;[ILjava/util/List;Ljava/util/List;Lcom/google/android/apps/plus/content/EsAccount;)V

    move-object/from16 v2, p0

    move-object/from16 v5, p3

    move-object/from16 v9, p5

    move-object/from16 v10, p1

    invoke-static/range {v2 .. v10}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventListInTransaction(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;Ljava/util/Set;[ILjava/util/List;Ljava/util/List;Lcom/google/android/apps/plus/content/EsAccount;)V

    move-object/from16 v2, p0

    move-object/from16 v5, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p1

    invoke-static/range {v2 .. v10}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventListInTransaction(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;Ljava/util/Set;[ILjava/util/List;Ljava/util/List;Lcom/google/android/apps/plus/content/EsAccount;)V

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v6, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Ljava/lang/String;

    const-string v2, "event_id IN ("

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v12, 0x0

    :goto_0
    array-length v2, v11

    if-ge v12, v2, :cond_1

    if-eqz v12, :cond_0

    const/16 v2, 0x2c

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    const/16 v2, 0x3f

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :cond_1
    const/16 v2, 0x29

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, "events"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v2, v5, v11}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const/4 v2, 0x3

    array-length v5, v11

    aput v5, v7, v2

    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "event_list_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "account_status"

    const/4 v5, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v3, v2, v0, v5, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v2, "EsEventData"

    const/4 v5, 0x3

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "EsEventData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "[INSERT_EVENT_LIST]; "

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    aget v9, v7, v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " inserted, "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v9, 0x1

    aget v9, v7, v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " changed, "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v9, 0x2

    aget v9, v7, v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " not changed, "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v9, 0x3

    aget v9, v7, v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " removed, "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v9, 0x4

    aget v9, v7, v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " ignored"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsEventData;->getPlusEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Lcom/google/api/services/plusi/model/PlusEvent;

    move-result-object v15

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v15}, Lcom/google/android/apps/plus/content/EsEventData;->validateInstantShare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/google/android/apps/plus/content/EsEventData;->hasExistingInstantShareWindowShortened(Landroid/content/Context;Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v2, v15}, Lcom/google/android/apps/plus/content/EsEventData;->enableInstantShare(Landroid/content/Context;ZLcom/google/api/services/plusi/model/PlusEvent;)V

    :cond_3
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v2, v14, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    :cond_4
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNT_STATUS_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-virtual {v2, v5, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-virtual {v2, v5, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void
.end method

.method private static insertEventInTransaction(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/util/List;Ljava/lang/Long;Ljava/util/List;Lcom/google/android/apps/plus/content/EsAccount;)Z
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p6    # Lcom/google/api/services/plusi/model/Update;
    .param p8    # Ljava/lang/Long;
    .param p10    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            "Lcom/google/api/services/plusi/model/Update;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")Z"
        }
    .end annotation

    const/4 v9, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v10, p10

    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventInTransaction$32ff2bf1(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/lang/Long;Ljava/util/List;ILcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    return v0
.end method

.method static insertEventInTransaction$32ff2bf1(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/lang/Long;Ljava/util/List;ILcom/google/android/apps/plus/content/EsAccount;)Z
    .locals 27
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p6    # Lcom/google/api/services/plusi/model/Update;
    .param p7    # Ljava/lang/Long;
    .param p9    # I
    .param p10    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            "Lcom/google/api/services/plusi/model/Update;",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;I",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")Z"
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    const/4 v13, 0x0

    const/16 v18, 0x1

    const/16 v22, 0x0

    const/16 v23, 0x0

    const-string v5, "events"

    const/4 v4, 0x3

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "fingerprint"

    aput-object v7, v6, v4

    const/4 v4, 0x1

    const-string v7, "source"

    aput-object v7, v6, v4

    const/4 v4, 0x2

    const-string v7, "can_comment"

    aput-object v7, v6, v4

    const-string v7, "event_id=?"

    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p3, v8, v4

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x0

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    const/4 v4, 0x1

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    const/16 v18, 0x0

    const/4 v4, 0x2

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_0

    const/16 v23, 0x1

    :goto_0
    const/4 v4, 0x1

    move/from16 v0, v17

    if-ne v0, v4, :cond_1

    if-nez p9, :cond_1

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    const/4 v4, 0x0

    :goto_1
    return v4

    :cond_0
    const/16 v23, 0x0

    goto :goto_0

    :cond_1
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Lcom/google/api/services/plusi/model/PlusEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/util/Arrays;->hashCode([B)I

    move-result v19

    new-instance v26, Landroid/content/ContentValues;

    invoke-direct/range {v26 .. v26}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "source"

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    if-eqz p6, :cond_2

    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v4, :cond_2

    :try_start_1
    const-string v4, "plus_one_data"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Update;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/DbPlusOneData;->serialize(Lcom/google/api/services/plusi/model/DataPlusOne;)[B

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    if-eqz p9, :cond_2

    new-instance v8, Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-direct {v8, v4}, Lcom/google/android/apps/plus/content/DbPlusOneData;-><init>(Lcom/google/api/services/plusi/model/DataPlusOne;)V

    const/4 v9, 0x1

    move-object/from16 v4, p0

    move-object/from16 v5, p10

    move-object/from16 v6, p2

    move-object/from16 v7, p4

    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/plus/content/EsPostsData;->replacePostPlusOneData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    :goto_2
    if-nez v18, :cond_3

    move/from16 v0, v22

    move/from16 v1, v19

    if-eq v0, v1, :cond_11

    :cond_3
    const-string v4, "refresh_timestamp"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "name"

    move-object/from16 v0, p5

    iget-object v5, v0, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "event_data"

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v4, "mine"

    move-object/from16 v0, p5

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsEventData;->isMine(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v5, "can_invite_people"

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v4, :cond_c

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventOptions;->openEventAcl:Ljava/lang/Boolean;

    if-eqz v4, :cond_c

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventOptions;->openEventAcl:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_c

    :cond_4
    const/4 v4, 0x1

    :goto_3
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v5, "can_post_photos"

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v4, :cond_d

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventOptions;->openPhotoAcl:Ljava/lang/Boolean;

    if-eqz v4, :cond_d

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventOptions;->openPhotoAcl:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_d

    :cond_5
    const/4 v4, 0x1

    :goto_4
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v5, "can_comment"

    if-eqz p6, :cond_e

    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->canViewerComment:Ljava/lang/Boolean;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v4

    :goto_5
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v4, :cond_f

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    :goto_6
    const-string v4, "start_time"

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-static/range {p5 .. p5}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v14

    const-string v4, "end_time"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "fingerprint"

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    if-nez v18, :cond_6

    if-eqz p4, :cond_7

    :cond_6
    const-string v4, "activity_id"

    move-object/from16 v0, v26

    move-object/from16 v1, p4

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    if-eqz p7, :cond_8

    const-string v4, "display_time"

    move-object/from16 v0, v26

    move-object/from16 v1, p7

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_8
    if-eqz v18, :cond_10

    const-string v4, "event_id"

    move-object/from16 v0, v26

    move-object/from16 v1, p3

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "events"

    const/4 v5, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-virtual {v0, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :goto_7
    const/4 v13, 0x1

    :cond_9
    :goto_8
    const-string v4, "EsEventData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_b

    const-string v4, "EsEventData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[INSERT_EVENT], duration: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v6, v6, v20

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x3

    const-string v6, "EsEventData"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v4, 0x0

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, "EVENT [id: "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p5

    iget-object v9, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, ", owner: "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    if-nez v4, :cond_12

    const-string v4, "N/A"

    :goto_9
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "MMM dd, yyyy h:mmaa"

    new-instance v9, Ljava/util/Date;

    move-object/from16 v0, p5

    iget-object v10, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-static {v4, v9}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v4

    const-string v9, ", start: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v4, :cond_a

    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    if-eqz v4, :cond_a

    const-string v4, "MMM dd, yyyy h:mmaa"

    new-instance v9, Ljava/util/Date;

    move-object/from16 v0, p5

    iget-object v10, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-static {v4, v9}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v4

    const-string v9, ", end: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_a
    const-string v4, ", \n"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "      title: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p5

    iget-object v8, v0, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "]"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\n"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\n"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v6, v4}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, p8

    move-object/from16 v3, p2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->insertReferencedPeopleInTransaction(Landroid/content/Context;Lcom/google/api/services/plusi/model/PlusEvent;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V

    move v4, v13

    goto/16 :goto_1

    :catchall_0
    move-exception v4

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_e
    move/from16 v4, v23

    goto/16 :goto_5

    :cond_f
    move-wide/from16 v24, v20

    goto/16 :goto_6

    :cond_10
    const-string v4, "events"

    const-string v5, "event_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p3, v6, v7

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_7

    :cond_11
    if-eqz p4, :cond_9

    const-string v4, "activity_id"

    move-object/from16 v0, v26

    move-object/from16 v1, p4

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "events"

    const-string v5, "event_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p3, v6, v7

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_8

    :cond_12
    move-object/from16 v0, p5

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    goto/16 :goto_9

    :catch_0
    move-exception v4

    goto/16 :goto_2
.end method

.method public static insertEventInviteeList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)V
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Invitee;",
            ">;)V"
        }
    .end annotation

    const/4 v9, 0x0

    const-string v6, "EsEventData"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Invitee;

    const-string v6, "EsEventData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[INSERT_EVENT_INVITEE]; "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/google/api/services/plusi/model/InviteeJson;->getInstance()Lcom/google/api/services/plusi/model/InviteeJson;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/google/api/services/plusi/model/InviteeJson;->toPrettyString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v6, 0x1

    new-array v1, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v1, v6

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    new-instance v4, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;

    invoke-direct {v4}, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;-><init>()V

    iput-object p3, v4, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;->invitees:Ljava/util/List;

    const-string v6, "invitee_roster_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "invitee_roster"

    sget-object v7, Lcom/google/android/apps/plus/content/EsEventData;->INVITEE_LIST_JSON:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v7, v4}, Lcom/google/android/apps/plus/json/EsJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    invoke-static {p0, p2, v9, p3, v0}, Lcom/google/android/apps/plus/content/EsEventData;->insertPeopleInInviteeSummaries(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V

    const-string v6, "events"

    const-string v7, "event_id=?"

    invoke-virtual {v0, v6, v5, v7, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    invoke-virtual {v6, v7, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void
.end method

.method private static insertEventListInTransaction(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;Ljava/util/Set;[ILjava/util/List;Ljava/util/List;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 16
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # Ljava/lang/String;
    .param p5    # [I
    .param p8    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;[I",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")V"
        }
    .end annotation

    if-nez p3, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/plusi/model/PlusEvent;

    move-object/from16 v0, p2

    invoke-static {v6, v0}, Lcom/google/android/apps/plus/content/EsEventData;->isMine(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x4

    aget v2, p5, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, p5, v1

    goto :goto_0

    :cond_2
    iget-object v1, v6, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v13

    iget-object v4, v6, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    move-object/from16 v8, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventInTransaction(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/util/List;Ljava/lang/Long;Ljava/util/List;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v12

    if-eqz v12, :cond_4

    if-eqz v13, :cond_3

    const/4 v15, 0x1

    :goto_1
    aget v1, p5, v15

    add-int/lit8 v1, v1, 0x1

    aput v1, p5, v15

    goto :goto_0

    :cond_3
    const/4 v15, 0x0

    goto :goto_1

    :cond_4
    const/4 v15, 0x2

    goto :goto_1
.end method

.method public static insertEventThemes(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V
    .locals 23
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Theme;",
            ">;)V"
        }
    .end annotation

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    const-string v3, "event_themes"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "theme_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "is_default"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "is_featured"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "image_url"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "sort_order"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "placeholder_path"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v11

    :goto_0
    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v20, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;

    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;-><init>(B)V

    const/4 v3, 0x0

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    const/4 v3, 0x1

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_1
    move-object/from16 v0, v20

    iput-boolean v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->isDefault:Z

    const/4 v3, 0x2

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_2
    move-object/from16 v0, v20

    iput-boolean v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->isFeatured:Z

    const/4 v3, 0x3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->imageUrl:Ljava/lang/String;

    const/4 v3, 0x4

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    iput v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->sortOrder:I

    const/4 v3, 0x5

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->placeholderPath:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    :try_start_2
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    goto :goto_2

    :cond_2
    :try_start_3
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    new-instance v12, Ljava/util/HashMap;

    move-object/from16 v0, v19

    invoke-direct {v12, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v13, v3, -0x1

    :goto_3
    if-ltz v13, :cond_b

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/api/services/plusi/model/Theme;

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Theme;->themeId:Ljava/lang/Integer;

    if-eqz v3, :cond_7

    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/plus/content/EsEventData;->getImageUrl(Lcom/google/api/services/plusi/model/Theme;)Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_7

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Theme;->themeId:Ljava/lang/Integer;

    invoke-virtual {v12, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Theme;->category:Ljava/util/List;

    if-eqz v3, :cond_5

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Theme;->category:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_3
    :goto_4
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/api/services/plusi/model/EventCategory;

    const-string v3, "FEATURED"

    iget-object v4, v10, Lcom/google/api/services/plusi/model/EventCategory;->category:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/16 v17, 0x1

    goto :goto_4

    :cond_4
    const-string v3, "DEFAULT"

    iget-object v4, v10, Lcom/google/api/services/plusi/model/EventCategory;->category:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/16 v16, 0x1

    goto :goto_4

    :cond_5
    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Theme;->themeId:Ljava/lang/Integer;

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;

    if-eqz v18, :cond_6

    move-object/from16 v0, v18

    iget-boolean v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->isDefault:Z

    move/from16 v0, v16

    if-ne v3, v0, :cond_6

    move-object/from16 v0, v18

    iget-boolean v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->isFeatured:Z

    move/from16 v0, v17

    if-ne v3, v0, :cond_6

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->imageUrl:Ljava/lang/String;

    invoke-static {v3, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    move-object/from16 v0, v18

    iget v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->sortOrder:I

    if-eq v3, v13, :cond_7

    :cond_6
    invoke-virtual/range {v22 .. v22}, Landroid/content/ContentValues;->clear()V

    const-string v4, "is_featured"

    if-eqz v17, :cond_8

    const/4 v3, 0x1

    :goto_5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "is_default"

    if-eqz v16, :cond_9

    const/4 v3, 0x1

    :goto_6
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "image_url"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "sort_order"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    if-eqz v18, :cond_a

    const-string v3, "event_themes"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "theme_id = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Theme;->themeId:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_7
    :goto_7
    add-int/lit8 v13, v13, -0x1

    goto/16 :goto_3

    :cond_8
    const/4 v3, 0x0

    goto :goto_5

    :cond_9
    const/4 v3, 0x0

    goto :goto_6

    :cond_a
    const-string v3, "theme_id"

    move-object/from16 v0, v20

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Theme;->themeId:Ljava/lang/Integer;

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "event_themes"

    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_7

    :cond_b
    invoke-virtual {v12}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_c
    :goto_8
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;

    const-string v3, "event_themes"

    const-string v4, "theme_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->placeholderPath:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, v20

    iget-object v4, v0, Lcom/google/android/apps/plus/content/EsEventData$ThemeStatus;->placeholderPath:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_8

    :cond_d
    invoke-virtual/range {v22 .. v22}, Landroid/content/ContentValues;->clear()V

    const-string v3, "event_themes_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "account_status"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void
.end method

.method private static insertMentionedPersonInTransaction(Landroid/content/Context;Ljava/lang/String;Lcom/google/api/services/plusi/model/EmbedsPerson;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/api/services/plusi/model/EmbedsPerson;
    .param p4    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")V"
        }
    .end annotation

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    iget-object v3, p2, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    if-eqz v3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    if-nez v1, :cond_1

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v3, v2, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    iget-object v4, v2, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    iget-object v5, v2, Lcom/google/api/services/plusi/model/EmbedsPerson;->imageUrl:Ljava/lang/String;

    invoke-static {p1, v3, v4, v5, p4}, Lcom/google/android/apps/plus/content/EsEventData;->insertPersonInTransaction$5725cc2c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V

    const/4 v1, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    if-nez v1, :cond_2

    if-eqz p2, :cond_2

    iget-object v3, p2, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p2, Lcom/google/api/services/plusi/model/EmbedsPerson;->imageUrl:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p2, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    iget-object v5, p2, Lcom/google/api/services/plusi/model/EmbedsPerson;->imageUrl:Ljava/lang/String;

    invoke-static {p1, v3, v4, v5, p4}, Lcom/google/android/apps/plus/content/EsEventData;->insertPersonInTransaction$5725cc2c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_2
    return-void
.end method

.method private static insertPeopleInInviteeSummaries(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p4    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Invitee;",
            ">;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")V"
        }
    .end annotation

    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/Invitee;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-static {p0, p1, v2, p2, p4}, Lcom/google/android/apps/plus/content/EsEventData;->insertMentionedPersonInTransaction(Landroid/content/Context;Ljava/lang/String;Lcom/google/api/services/plusi/model/EmbedsPerson;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V

    iget-object v2, v1, Lcom/google/api/services/plusi/model/Invitee;->inviter:Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-static {p0, p1, v2, p2, p4}, Lcom/google/android/apps/plus/content/EsEventData;->insertMentionedPersonInTransaction(Landroid/content/Context;Ljava/lang/String;Lcom/google/api/services/plusi/model/EmbedsPerson;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static insertPersonInTransaction$5725cc2c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/database/sqlite/SQLiteDatabase;

    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p0, v0, v3

    const/4 v3, 0x1

    aput-object p1, v0, v3

    const/4 v1, 0x1

    :try_start_0
    const-string v3, "SELECT event_id FROM event_people WHERE event_id=? AND gaia_id=?"

    invoke-static {p4, v3, v0}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "event_id"

    invoke-virtual {v2, v3, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "gaia_id"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "event_people"

    const/4 v4, 0x0

    invoke-virtual {p4, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_2
    invoke-static {p4, p1, p2, p3}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method private static insertReferencedPeopleInTransaction(Landroid/content/Context;Lcom/google/api/services/plusi/model/PlusEvent;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p3    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")V"
        }
    .end annotation

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->creator:Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-static {p0, v0, v3, p2, p3}, Lcom/google/android/apps/plus/content/EsEventData;->insertMentionedPersonInTransaction(Landroid/content/Context;Ljava/lang/String;Lcom/google/api/services/plusi/model/EmbedsPerson;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/InviteeSummary;

    iget-object v3, v2, Lcom/google/api/services/plusi/model/InviteeSummary;->invitee:Ljava/util/List;

    invoke-static {p0, v0, p2, v3, p3}, Lcom/google/android/apps/plus/content/EsEventData;->insertPeopleInInviteeSummaries(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static insertResumeTokenInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    new-array v4, v0, [Ljava/lang/String;

    aput-object p1, v4, v3

    const/4 v9, 0x1

    const/4 v10, 0x0

    const-string v1, "events"

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "resume_token"

    aput-object v0, v2, v3

    const-string v3, "event_id=?"

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    const/4 v9, 0x0

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    invoke-static {v10, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "resume_token"

    invoke-virtual {v11, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v9, :cond_2

    const-string v0, "events"

    invoke-virtual {p0, v0, v5, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_1
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    const-string v0, "events"

    const-string v1, "event_id=?"

    invoke-virtual {p0, v0, v11, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isEventHangout(Lcom/google/api/services/plusi/model/PlusEvent;)Z
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventOptions;->broadcast:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventOptions;->broadcast:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isEventOver(Lcom/google/api/services/plusi/model/PlusEvent;J)Z
    .locals 3
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p1    # J

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v0

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isInstantShareAllowed(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;J)Z
    .locals 12
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsEventData;->canAddPhotos(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z

    move-result v0

    iget-object v8, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Invitee;->rsvpType:Ljava/lang/String;

    if-eqz v8, :cond_0

    move v5, v6

    :goto_0
    iget-object v8, p0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/32 v10, 0xa4cb80

    sub-long v3, v8, v10

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v8

    const-wide/16 v10, 0x1388

    sub-long v1, v8, v10

    if-eqz v0, :cond_1

    if-eqz v5, :cond_1

    cmp-long v8, p2, v3

    if-lez v8, :cond_1

    cmp-long v8, p2, v1

    if-gez v8, :cond_1

    :goto_1
    return v6

    :cond_0
    move v5, v7

    goto :goto_0

    :cond_1
    move v6, v7

    goto :goto_1
.end method

.method private static isMine(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z
    .locals 4
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Invitee;->rsvpType:Ljava/lang/String;

    if-nez v3, :cond_0

    :cond_2
    iget-object v3, p0, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/InviteeSummary;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    if-eqz v3, :cond_3

    iget-object v3, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isViewerCheckedIn(Lcom/google/api/services/plusi/model/PlusEvent;)Z
    .locals 2
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;

    const-string v0, "CHECKIN"

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpType(Lcom/google/api/services/plusi/model/PlusEvent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static populateEventThemePlaceholders(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 40
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const/4 v12, 0x0

    const/16 v16, 0x0

    :try_start_0
    const-string v5, "event_themes"

    sget-object v6, Lcom/google/android/apps/plus/content/EsEventData$EventThemesQuery;->PROJECTION:[Ljava/lang/String;

    const-string v7, "placeholder_path IS NOT NULL"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-string v11, "sort_order ASC"

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    move-result v15

    const-wide/16 v32, 0x0

    const-wide/16 v26, 0x0

    new-instance v30, Landroid/util/SparseIntArray;

    invoke-direct/range {v30 .. v30}, Landroid/util/SparseIntArray;-><init>()V

    new-instance v38, Landroid/content/ContentValues;

    invoke-direct/range {v38 .. v38}, Landroid/content/ContentValues;-><init>()V

    :cond_2
    :goto_1
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_6

    new-instance v31, Ljava/io/File;

    const/4 v5, 0x2

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v31 .. v31}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_3

    const-wide/16 v5, 0x0

    cmp-long v5, v26, v5

    if-nez v5, :cond_3

    invoke-virtual/range {v31 .. v31}, Ljava/io/File;->length()J

    move-result-wide v32

    const-wide/16 v5, 0x0

    cmp-long v5, v32, v5

    if-lez v5, :cond_2

    const-wide/32 v5, 0x100000

    div-long v26, v5, v32

    goto :goto_1

    :cond_3
    if-nez v17, :cond_2

    const/4 v5, 0x1

    move-object/from16 v0, v16

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/4 v6, 0x1

    move-object/from16 v0, v30

    invoke-virtual {v0, v5, v6}, Landroid/util/SparseIntArray;->append(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v5

    if-eqz v16, :cond_4

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    :cond_4
    if-eqz v12, :cond_5

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v5

    :cond_6
    :try_start_1
    invoke-virtual/range {v30 .. v30}, Landroid/util/SparseIntArray;->size()I

    move-result v5

    if-lez v5, :cond_9

    invoke-virtual/range {v38 .. v38}, Landroid/content/ContentValues;->clear()V

    const-string v5, "placeholder_path"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    new-instance v35, Ljava/lang/StringBuffer;

    const/16 v5, 0x100

    move-object/from16 v0, v35

    invoke-direct {v0, v5}, Ljava/lang/StringBuffer;-><init>(I)V

    const-string v5, "theme_id IN("

    move-object/from16 v0, v35

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v20, 0x1

    invoke-virtual/range {v30 .. v30}, Landroid/util/SparseIntArray;->size()I

    move-result v5

    add-int/lit8 v22, v5, -0x1

    :goto_2
    if-ltz v22, :cond_8

    if-eqz v20, :cond_7

    const/16 v20, 0x0

    :goto_3
    move-object/from16 v0, v30

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v5

    move-object/from16 v0, v35

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    add-int/lit8 v22, v22, -0x1

    goto :goto_2

    :cond_7
    const/16 v5, 0x2c

    move-object/from16 v0, v35

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_3

    :cond_8
    const/16 v5, 0x29

    move-object/from16 v0, v35

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const-string v5, "event_themes"

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v38

    invoke-virtual {v4, v5, v0, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_9
    const-string v5, "event_themes"

    sget-object v6, Lcom/google/android/apps/plus/content/EsEventData$EventThemesQuery;->PROJECTION:[Ljava/lang/String;

    const-string v7, "placeholder_path IS NULL"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-string v11, "sort_order ASC"

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_d

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsEventData;->getEventThemePlaceholderDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v14

    invoke-virtual/range {v38 .. v38}, Landroid/content/ContentValues;->clear()V

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v29

    const/high16 v5, 0x3e800000

    move-object/from16 v0, v29

    iget v6, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->shortDimension:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v39

    invoke-static/range {v39 .. v39}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeMediaHeight(I)I

    move-result v21

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v25

    :cond_a
    :goto_4
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v5

    if-nez v5, :cond_d

    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_d

    const-wide/16 v5, 0x0

    cmp-long v5, v32, v5

    if-eqz v5, :cond_b

    int-to-long v5, v15

    cmp-long v5, v5, v26

    if-gez v5, :cond_d

    :cond_b
    const/4 v5, 0x1

    invoke-interface {v12, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v37

    const/4 v5, 0x3

    invoke-interface {v12, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    new-instance v28, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v5, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-object/from16 v0, v28

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/16 v23, 0x0

    const/16 v5, 0x8

    :try_start_2
    move-object/from16 v0, v25

    move-object/from16 v1, v28

    move/from16 v2, v39

    move/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getBlockingMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;III)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, [B

    move-object/from16 v23, v0
    :try_end_2
    .catch Lcom/google/android/apps/plus/service/ResourceUnavailableException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_5
    if-eqz v23, :cond_a

    :try_start_3
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    move-object/from16 v1, v37

    invoke-direct {v0, v14, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-wide/16 v5, 0x0

    cmp-long v5, v32, v5

    if-nez v5, :cond_c

    move-object/from16 v0, v23

    array-length v5, v0

    int-to-long v0, v5

    move-wide/from16 v32, v0

    const-wide/32 v5, 0x100000

    div-long v26, v5, v32

    :cond_c
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v19

    :try_start_4
    new-instance v36, Ljava/io/FileOutputStream;

    move-object/from16 v0, v36

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v36

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual/range {v36 .. v36}, Ljava/io/FileOutputStream;->close()V

    add-int/lit8 v15, v15, 0x1

    invoke-virtual/range {v38 .. v38}, Landroid/content/ContentValues;->clear()V

    const-string v5, "placeholder_path"

    move-object/from16 v0, v38

    move-object/from16 v1, v19

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "event_themes"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "theme_id = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v37

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v38

    invoke-virtual {v4, v5, v0, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v34

    if-nez v34, :cond_a

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_4

    :catch_0
    move-exception v13

    :try_start_5
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    const-string v5, "EsEventData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Cannot write event placeholder file: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_4

    :catch_1
    move-exception v13

    const-string v5, "EsEventData"

    const-string v6, "Cannot download event theme"

    invoke-static {v5, v6, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_5

    :cond_d
    if-eqz v16, :cond_e

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    :cond_e
    if-eqz v12, :cond_0

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method public static readEventFromServer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)Lcom/google/android/apps/plus/network/HttpOperation;
    .locals 14
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Z
    .param p8    # Z
    .param p9    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p10    # Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    sget-object v13, Lcom/google/android/apps/plus/content/EsEventData;->sEventOperationSyncObject:Ljava/lang/Object;

    monitor-enter v13

    if-eqz p8, :cond_1

    :try_start_0
    new-instance v2, Lcom/google/android/apps/plus/api/EventReadOperation;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p5

    move/from16 v7, p7

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/plus/api/EventReadOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    :goto_0
    if-eqz p9, :cond_2

    if-eqz p10, :cond_2

    move-object/from16 v0, p9

    move-object/from16 v1, p10

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/plus/api/EventReadOperation;->start(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    :goto_1
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/EventReadOperation;->hasError()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "EsEventData"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/api/EventReadOperation;->logError(Ljava/lang/String;)V

    :cond_0
    monitor-exit v13

    return-object v2

    :cond_1
    new-instance v2, Lcom/google/android/apps/plus/api/EventReadOperation;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move/from16 v10, p7

    invoke-direct/range {v2 .. v12}, Lcom/google/android/apps/plus/api/EventReadOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/EventReadOperation;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v13

    throw v3
.end method

.method public static refreshEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/plus/content/EsEventData$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/plus/content/EsEventData$1;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->postOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static replaceEventPlusOneData(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;Z)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/content/DbPlusOneData;
    .param p4    # Z

    const/4 v5, 0x1

    :try_start_0
    invoke-static {p3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->serialize(Lcom/google/android/apps/plus/content/DbPlusOneData;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2, v5}, Landroid/content/ContentValues;-><init>(I)V

    const-string v3, "plus_one_data"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v3, "events"

    const-string v4, "activity_id=?"

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    invoke-virtual {p1, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v3, "EsEventData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not serialize DbPlusOneData "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static retrieveEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    invoke-static {p0, p1, p2, p4}, Lcom/google/android/apps/plus/content/EsEventData;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/plus/content/EsEventData;->getEventFromServer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0, p1, p2, p4}, Lcom/google/android/apps/plus/content/EsEventData;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static rsvpForEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    sget-object v10, Lcom/google/android/apps/plus/content/EsEventData;->sEventOperationSyncObject:Ljava/lang/Object;

    monitor-enter v10

    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/content/EsEventData;->getPlusEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Lcom/google/api/services/plusi/model/PlusEvent;

    move-result-object v9

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/plus/content/EsEventData;->setRsvpType(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v9, :cond_0

    if-nez v6, :cond_0

    const/4 v1, 0x0

    monitor-exit v10

    :goto_0
    return v1

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->hasError()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v10

    throw v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static setRsvpType(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 20
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const-string v3, "CHECKIN"

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const-string v14, "UNDO_CHECKIN"

    :goto_0
    const/4 v12, 0x0

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "events"

    sget-object v4, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_QUERY_PROJECTION:[Ljava/lang/String;

    const-string v5, "event_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/api/services/plusi/model/PlusEvent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    if-eqz v12, :cond_c

    iget-object v3, v12, Lcom/google/api/services/plusi/model/PlusEvent;->isPublic:Ljava/lang/Boolean;

    if-eqz v3, :cond_1

    iget-object v3, v12, Lcom/google/api/services/plusi/model/PlusEvent;->isPublic:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v12, v3}, Lcom/google/android/apps/plus/content/EsEventData;->isMine(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, v12, Lcom/google/api/services/plusi/model/PlusEvent;->authKey:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    :goto_1
    return-object v3

    :cond_2
    const-string v14, "NOT_RESPONDED"

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_3
    iget-object v3, v12, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    if-eqz v3, :cond_b

    const/16 v18, 0x0

    iget-object v3, v12, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_4
    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/api/services/plusi/model/InviteeSummary;

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    if-eqz v3, :cond_4

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    if-eqz v3, :cond_6

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v16, 0x1

    :goto_3
    if-eqz v16, :cond_5

    if-nez v10, :cond_5

    move-object/from16 v0, v17

    iget-object v14, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    :cond_5
    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v18, 0x1

    if-nez v16, :cond_4

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    if-nez v3, :cond_7

    const/4 v3, 0x1

    :goto_4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    goto :goto_2

    :cond_6
    const/16 v16, 0x0

    goto :goto_3

    :cond_7
    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_8
    if-eqz v16, :cond_4

    const/4 v15, 0x1

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    const-string v4, "CHECKIN"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "UNDO_CHECKIN"

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    :cond_9
    if-eqz v15, :cond_4

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    if-eqz v3, :cond_4

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_4

    invoke-interface {v13}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_2

    :cond_a
    if-nez v18, :cond_b

    new-instance v17, Lcom/google/api/services/plusi/model/InviteeSummary;

    invoke-direct/range {v17 .. v17}, Lcom/google/api/services/plusi/model/InviteeSummary;-><init>()V

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/google/api/services/plusi/model/InviteeSummary;->setByViewer:Ljava/lang/Boolean;

    iget-object v3, v12, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-interface {v3, v4, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_b
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v12, v0, v1}, Lcom/google/android/apps/plus/content/EsEventData;->setViewerInfoRsvp(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "event_data"

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v4

    invoke-virtual {v4, v12}, Lcom/google/api/services/plusi/model/PlusEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v3, "refresh_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "events"

    const-string v4, "event_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    move-object/from16 v0, v19

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_c
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    move-object v3, v14

    goto/16 :goto_1
.end method

.method private static setViewerInfoRsvp(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 2
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    if-eqz p0, :cond_1

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/api/services/plusi/model/Invitee;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/Invitee;-><init>()V

    iput-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    new-instance v1, Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/EmbedsPerson;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/api/services/plusi/model/PlusEvent;->viewerInfo:Lcom/google/api/services/plusi/model/Invitee;

    iput-object p2, v0, Lcom/google/api/services/plusi/model/Invitee;->rsvpType:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public static syncCurrentEvents$1ef5a3b9(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 24
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    sget-object v23, Lcom/google/android/apps/plus/content/EsEventData;->mSyncLock:Ljava/lang/Object;

    monitor-enter v23

    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_0

    monitor-exit v23

    :goto_0
    return-void

    :cond_0
    const-string v2, "Current events"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    const-wide/32 v2, 0x112a880

    sub-long v16, v20, v2

    const-wide/32 v2, 0xa4cb80

    add-long v14, v20, v2

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "events"

    sget-object v3, Lcom/google/android/apps/plus/content/EsEventData;->SYNC_QUERY_PROJECTION:[Ljava/lang/String;

    const-string v4, "end_time > ? AND start_time < ?"

    const/4 v7, 0x2

    new-array v5, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x1

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v18

    const/4 v13, 0x0

    :cond_1
    :goto_1
    :try_start_1
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v2, 0x2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v2

    const/4 v3, 0x3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/api/services/plusi/model/PlusEvent;

    move-object/from16 v0, v19

    iget-object v7, v0, Lcom/google/api/services/plusi/model/PlusEvent;->authKey:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    new-instance v12, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct {v12}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v11, p2

    invoke-static/range {v2 .. v12}, Lcom/google/android/apps/plus/content/EsEventData;->readEventFromServer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)Lcom/google/android/apps/plus/network/HttpOperation;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-nez v2, :cond_1

    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    :cond_2
    :try_start_2
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    monitor-exit v23
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v23

    throw v2

    :catchall_1
    move-exception v2

    :try_start_3
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public static syncEventThemes$1ef5a3b9(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const/4 v0, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v4, "Event Themes"

    invoke-virtual {p2, v4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    const-string v4, "connectivity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    const-string v4, "EsEventData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "EsEventData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "active network: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    move v4, v0

    :goto_1
    if-eqz v4, :cond_3

    :goto_2
    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/content/EsEventData;->ensureFreshEventThemes(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/content/EsEventData;->populateEventThemePlaceholders(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    :cond_2
    invoke-virtual {p2, v3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    goto :goto_0

    :pswitch_1
    move v4, v3

    goto :goto_1

    :cond_3
    move v0, v3

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static timeUntilInstantShareAllowed(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;J)J
    .locals 6
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/plus/content/EsEventData;->isInstantShareAllowed(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;J)Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    :goto_0
    return-wide v2

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsEventData;->canAddPhotos(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    cmp-long v2, p2, v0

    if-lez v2, :cond_2

    :cond_1
    const-wide/16 v2, -0x1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/32 v4, 0xa4cb80

    sub-long/2addr v2, v4

    sub-long/2addr v2, p2

    goto :goto_0
.end method

.method public static updateDataPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Z
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/plus/api/GetPhotoOperation;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-wide v5, p4

    move-object/from16 v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/GetPhotoOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;JLjava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetPhotoOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetPhotoOperation;->hasError()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetPhotoOperation;->getDataPhoto()Lcom/google/api/services/plusi/model/DataPhoto;

    move-result-object v9

    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "data"

    invoke-static {}, Lcom/google/api/services/plusi/model/DataPhotoJson;->getInstance()Lcom/google/api/services/plusi/model/DataPhotoJson;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/api/services/plusi/model/DataPhotoJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const-string v1, "event_activities"

    const-string v2, "event_id=? AND fingerprint=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    aput-object p3, v3, v4

    invoke-virtual {v8, v1, v10, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method public static updateEventActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;ZJLjava/util/List;)V
    .locals 22
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p4    # Lcom/google/api/services/plusi/model/Update;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p8    # Z
    .param p9    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            "Lcom/google/api/services/plusi/model/Update;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventActivity;",
            ">;ZJ",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;)V"
        }
    .end annotation

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p3

    iget-object v6, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p9 .. p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    const/4 v12, 0x0

    move-object/from16 v3, p0

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v13, p1

    invoke-static/range {v3 .. v13}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventInTransaction(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/util/List;Ljava/lang/Long;Ljava/util/List;Lcom/google/android/apps/plus/content/EsAccount;)Z

    if-eqz p11, :cond_0

    invoke-interface/range {p11 .. p11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_0
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/api/services/plusi/model/EmbedsPerson;

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    move-object/from16 v0, v21

    iget-object v7, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->imageUrl:Ljava/lang/String;

    invoke-static {v6, v3, v4, v7, v5}, Lcom/google/android/apps/plus/content/EsEventData;->insertPersonInTransaction$5725cc2c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    :cond_0
    const/4 v3, 0x1

    :try_start_1
    new-array v15, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v6, v15, v3

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v12, "events"

    const/4 v7, 0x1

    new-array v13, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "polling_token"

    aput-object v8, v13, v7

    const-string v14, "event_id=?"

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object v11, v5

    invoke-virtual/range {v11 .. v18}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v3, 0x0

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v3

    const/4 v4, 0x0

    :cond_1
    :try_start_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p5

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "polling_token"

    move-object/from16 v0, p5

    invoke-virtual {v3, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v4, :cond_7

    const-string v4, "events"

    const/4 v7, 0x0

    invoke-virtual {v5, v4, v7, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_2
    :goto_1
    move-object/from16 v0, p6

    invoke-static {v5, v6, v0}, Lcom/google/android/apps/plus/content/EsEventData;->insertResumeTokenInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v15, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v6, v15, v3

    const/4 v7, 0x1

    const-wide/16 v3, 0x0

    const-string v12, "events"

    const/4 v8, 0x1

    new-array v13, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "display_time"

    aput-object v9, v13, v8

    const-string v14, "event_id=?"

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object v11, v5

    invoke-virtual/range {v11 .. v18}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v8

    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v9

    if-eqz v9, :cond_3

    const/4 v3, 0x0

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-wide v3

    const/4 v7, 0x0

    :cond_3
    :try_start_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    cmp-long v3, p9, v3

    if-eqz v3, :cond_4

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "display_time"

    invoke-static/range {p9 .. p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v3, v4, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    if-eqz v7, :cond_8

    const-string v4, "events"

    const/4 v7, 0x0

    invoke-virtual {v5, v4, v7, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_4
    :goto_2
    if-eqz p7, :cond_5

    move-object/from16 v4, p0

    move-object/from16 v7, p7

    move/from16 v8, p8

    move-object v9, v10

    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventActivitiesInTransaction(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;ZLjava/util/List;)V

    :cond_5
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsEventData;->validateInstantShare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v3

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsEventData;->hasExistingInstantShareWindowShortened(Landroid/content/Context;Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v3, v1}, Lcom/google/android/apps/plus/content/EsEventData;->enableInstantShare(Landroid/content/Context;ZLcom/google/api/services/plusi/model/PlusEvent;)V

    :cond_6
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_3

    :catchall_1
    move-exception v3

    :try_start_6
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_7
    const-string v4, "events"

    const-string v7, "event_id=?"

    invoke-virtual {v5, v4, v3, v7, v15}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1

    :catchall_2
    move-exception v3

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_8
    const-string v4, "events"

    const-string v7, "event_id=?"

    invoke-virtual {v5, v4, v3, v7, v15}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    :cond_9
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void
.end method

.method public static updateEventInviteeList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 20
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const/4 v15, 0x0

    const/16 v19, 0x0

    const-string v5, "events"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "event_data"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "invitee_roster"

    aput-object v8, v6, v7

    const-string v7, "event_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p2, v8, v9

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x0

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v16

    const/4 v5, 0x1

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v13

    if-eqz v13, :cond_0

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/google/api/services/plusi/model/PlusEvent;

    move-object v15, v0

    sget-object v5, Lcom/google/android/apps/plus/content/EsEventData;->INVITEE_LIST_JSON:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v5, v13}, Lcom/google/android/apps/plus/json/EsJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;

    iget-object v0, v5, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;->invitees:Ljava/util/List;

    move-object/from16 v19, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    if-eqz v19, :cond_1

    if-nez v15, :cond_2

    :cond_1
    :goto_0
    return-void

    :catchall_0
    move-exception v5

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v5

    :cond_2
    const/16 v18, 0x0

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/api/services/plusi/model/Invitee;

    iget-object v5, v12, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    if-eqz v5, :cond_3

    iget-object v5, v12, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-static {v0, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v12, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EmbedsPerson;->email:Ljava/lang/String;

    move-object/from16 v0, p5

    invoke-static {v0, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    move-object/from16 v18, v12

    :cond_4
    if-eqz v18, :cond_1

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Invitee;->isAdminBlacklisted:Ljava/lang/Boolean;

    if-eqz v5, :cond_5

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Invitee;->isAdminBlacklisted:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move/from16 v0, p3

    if-eq v5, v0, :cond_1

    :cond_5
    invoke-static/range {p3 .. p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/google/api/services/plusi/model/Invitee;->isAdminBlacklisted:Ljava/lang/Boolean;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v19

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventInviteeList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)V

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/google/api/services/plusi/model/Invitee;->rsvpType:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Invitee;->numAdditionalGuests:Ljava/lang/Integer;

    invoke-static {v5}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v5

    add-int/lit8 v8, v5, 0x1

    if-eqz v15, :cond_1

    iget-object v5, v15, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    if-eqz v5, :cond_1

    iget-object v5, v15, Lcom/google/api/services/plusi/model/PlusEvent;->inviteeSummary:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_6
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/InviteeSummary;

    if-eqz p3, :cond_7

    const/4 v6, -0x1

    :goto_2
    iget-object v10, v5, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    if-eqz v10, :cond_6

    iget-object v10, v5, Lcom/google/api/services/plusi/model/InviteeSummary;->rsvpType:Ljava/lang/String;

    invoke-static {v10, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_6

    iget-object v10, v5, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    mul-int/2addr v6, v8

    add-int/2addr v6, v10

    const/4 v10, 0x0

    invoke-static {v6, v10}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    goto :goto_1

    :cond_7
    const/4 v6, 0x1

    goto :goto_2

    :cond_8
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, v15, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v7

    invoke-virtual {v7, v15}, Lcom/google/api/services/plusi/model/PlusEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v7

    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    const-string v9, "event_data"

    invoke-virtual {v8, v9, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v7, "events"

    const-string v9, "event_id=?"

    invoke-virtual {v5, v7, v8, v9, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_0
.end method

.method public static validateInstantShare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/plus/content/EsEventData;->getPlusEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Lcom/google/api/services/plusi/model/PlusEvent;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/content/EsEventData;->validateInstantShare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v1

    return v1
.end method

.method private static validateInstantShare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)Z
    .locals 21
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/api/services/plusi/model/PlusEvent;

    const/4 v6, 0x0

    const-string v17, "EsEventData"

    const/16 v18, 0x4

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_0

    const-string v17, "EsEventData"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "#validateInstantShare; now: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static/range {p0 .. p2}, Lcom/google/android/apps/plus/content/EsEventData;->getEventInstantShareEndTime(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v10

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v13

    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    const-string v17, "alarm"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/AlarmManager;

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getEventFinishedIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    const-string v17, "EsEventData"

    const/16 v18, 0x4

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_1

    const-string v17, "EsEventData"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "#validateInstantShare; cur event: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-nez v13, :cond_3

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-static {v0, v1, v14, v15}, Lcom/google/android/apps/plus/content/EsEventData;->isInstantShareAllowed(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;J)Z

    move-result v17

    if-eqz v17, :cond_3

    const-wide/16 v17, 0x1388

    sub-long v17, v10, v17

    cmp-long v17, v14, v17

    if-gez v17, :cond_3

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-static {v0, v1, v8, v2}, Lcom/google/android/apps/plus/phone/Intents;->getViewEventActivityNotificationIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v12

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v0, v1, v12, v2}, Lcom/google/android/apps/plus/util/NotificationUtils;->notifyInstantShareEnabled(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/app/PendingIntent;Z)V

    move-object/from16 v0, p0

    invoke-static {v0, v8}, Lcom/google/android/apps/plus/phone/Intents;->getEventFinishedIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v7

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v5, v0, v10, v11, v7}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    const/4 v6, 0x1

    const-string v17, "EsEventData"

    const/16 v18, 0x4

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_2

    const-string v17, "EsEventData"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "#validateInstantShare; keep IS; now: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", end: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", wake in: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sub-long v19, v10, v14

    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v17, "EsEventData"

    const/16 v18, 0x4

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_3

    const-string v9, "MMM dd, yyyy h:mmaa"

    const-string v17, "EsEventData"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Enable Instant Share; now: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v19, Ljava/util/Date;

    move-object/from16 v0, v19

    invoke-direct {v0, v14, v15}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v19

    invoke-static {v9, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", alarm: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    new-instance v19, Ljava/util/Date;

    move-object/from16 v0, v19

    invoke-direct {v0, v10, v11}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v19

    invoke-static {v9, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    if-nez v6, :cond_5

    if-eqz p1, :cond_4

    if-eqz p2, :cond_4

    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v17

    cmp-long v17, v10, v17

    if-gez v17, :cond_4

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    move-object/from16 v3, v18

    move-object/from16 v4, v19

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/AndroidNotification;->showPartyModeExpiredNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsEventData;->disableInstantShare(Landroid/content/Context;)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/util/NotificationUtils;->cancelInstantShareEnabled(Landroid/content/Context;)V

    const-string v17, "EsEventData"

    const/16 v18, 0x4

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_5

    const-string v17, "EsEventData"

    const-string v18, "Disable Instant Share"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    return v6

    :catchall_0
    move-exception v17

    if-nez v6, :cond_7

    if-eqz p1, :cond_6

    if-eqz p2, :cond_6

    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v18

    cmp-long v18, v10, v18

    if-gez v18, :cond_6

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    move-object/from16 v4, v20

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/AndroidNotification;->showPartyModeExpiredNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsEventData;->disableInstantShare(Landroid/content/Context;)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/util/NotificationUtils;->cancelInstantShareEnabled(Landroid/content/Context;)V

    const-string v18, "EsEventData"

    const/16 v19, 0x4

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_7

    const-string v18, "EsEventData"

    const-string v19, "Disable Instant Share"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    throw v17
.end method
