.class public abstract Lcom/google/android/apps/plus/content/EsLocalPageData;
.super Ljava/lang/Object;
.source "EsLocalPageData.java"


# direct methods
.method private static buildOpeningHoursStringForADay(Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;)Ljava/lang/String;
    .locals 7
    .param p0    # Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;->interval:Ljava/util/List;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;->interval:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x1

    iget-object v5, p0, Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;->interval:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/OpeningHoursProtoDayInterval;

    iget-object v5, v2, Lcom/google/api/services/plusi/model/OpeningHoursProtoDayInterval;->value:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    if-nez v0, :cond_3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v0, 0x0

    iget-object v5, v2, Lcom/google/api/services/plusi/model/OpeningHoursProtoDayInterval;->value:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-eqz v5, :cond_0

    iget-object v4, p0, Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;->dayName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;->dayName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private static getCircleActivityStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/FeaturedActivityProto;
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FrontendPaperProto;->circleActivity:Lcom/google/api/services/plusi/model/FeaturedActivityProto;

    return-object v0
.end method

.method public static getCircleReviews(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/util/List;
    .locals 5
    .param p0    # Lcom/google/api/services/plusi/model/SimpleProfile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/plusi/model/SimpleProfile;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/GoogleReviewProto;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getCircleActivityStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/FeaturedActivityProto;

    move-result-object v3

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->hasCircleActivity(Lcom/google/api/services/plusi/model/SimpleProfile;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v3, Lcom/google/api/services/plusi/model/FeaturedActivityProto;->activity:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;

    iget-object v4, v1, Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;->review:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    if-eqz v4, :cond_0

    iget-object v4, v1, Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;->review:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static getOpeningHoursFull(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;
    .locals 8
    .param p0    # Lcom/google/api/services/plusi/model/SimpleProfile;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v5, v7, Lcom/google/api/services/plusi/model/FrontendPaperProto;->openingHours:Lcom/google/api/services/plusi/model/OpeningHoursProto;

    if-eqz v5, :cond_0

    iget-object v7, v5, Lcom/google/api/services/plusi/model/OpeningHoursProto;->day:Ljava/util/List;

    if-eqz v7, :cond_0

    iget-object v7, v5, Lcom/google/api/services/plusi/model/OpeningHoursProto;->day:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    :goto_0
    return-object v6

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x1

    iget-object v7, v5, Lcom/google/api/services/plusi/model/OpeningHoursProto;->day:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsLocalPageData;->buildOpeningHoursStringForADay(Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    if-nez v3, :cond_3

    const-string v7, "\n"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v3, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public static getOpeningHoursSummary(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v0, v1, Lcom/google/api/services/plusi/model/FrontendPaperProto;->openingHours:Lcom/google/api/services/plusi/model/OpeningHoursProto;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/api/services/plusi/model/OpeningHoursProto;->today:Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, v0, Lcom/google/api/services/plusi/model/OpeningHoursProto;->today:Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsLocalPageData;->buildOpeningHoursStringForADay(Lcom/google/api/services/plusi/model/OpeningHoursProtoDay;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getPriceLabel(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getPriceStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/AttributeProto;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/api/services/plusi/model/AttributeProto;->labelDisplay:Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getPriceStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/AttributeProto;
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FrontendPaperProto;->priceContinuous:Lcom/google/api/services/plusi/model/AttributeProto;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FrontendPaperProto;->priceContinuous:Lcom/google/api/services/plusi/model/AttributeProto;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FrontendPaperProto;->price:Lcom/google/api/services/plusi/model/AttributeProto;

    goto :goto_0
.end method

.method public static getPriceValue(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getPriceStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/AttributeProto;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/api/services/plusi/model/AttributeProto;->value:Lcom/google/api/services/plusi/model/AttributeProtoCanonicalValue;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/AttributeProtoCanonicalValue;->priceLevel:Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getReviews(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/util/List;
    .locals 6
    .param p0    # Lcom/google/api/services/plusi/model/SimpleProfile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/plusi/model/SimpleProfile;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/GoogleReviewProto;",
            ">;"
        }
    .end annotation

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/FrontendPaperProto;->googleReviews:Lcom/google/api/services/plusi/model/GoogleReviewsProto;

    if-eqz v0, :cond_0

    iget-object v5, v0, Lcom/google/api/services/plusi/model/GoogleReviewsProto;->review:Ljava/util/List;

    if-eqz v5, :cond_0

    iget-object v5, v0, Lcom/google/api/services/plusi/model/GoogleReviewsProto;->review:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    iget-object v5, v0, Lcom/google/api/services/plusi/model/GoogleReviewsProto;->review:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/GoogleReviewProto;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    return-object v4
.end method

.method private static getUserActivityStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/FeaturedActivityProto;
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, p0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FrontendPaperProto;->userActivity:Lcom/google/api/services/plusi/model/FeaturedActivityProto;

    return-object v0
.end method

.method public static getYourReview(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/GoogleReviewProto;
    .locals 4
    .param p0    # Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getUserActivityStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/FeaturedActivityProto;

    move-result-object v2

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->hasYourActivity(Lcom/google/api/services/plusi/model/SimpleProfile;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcom/google/api/services/plusi/model/FeaturedActivityProto;->activity:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;->review:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;->review:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    :goto_0
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static hasActivity(Lcom/google/api/services/plusi/model/FeaturedActivityProto;)Z
    .locals 2
    .param p0    # Lcom/google/api/services/plusi/model/FeaturedActivityProto;

    if-eqz p0, :cond_0

    iget-object v1, p0, Lcom/google/api/services/plusi/model/FeaturedActivityProto;->totalReviews:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/api/services/plusi/model/FeaturedActivityProto;->totalReviews:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/api/services/plusi/model/FeaturedActivityProto;->activity:Ljava/util/List;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasCircleActivity(Lcom/google/api/services/plusi/model/SimpleProfile;)Z
    .locals 2
    .param p0    # Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getCircleActivityStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/FeaturedActivityProto;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->hasActivity(Lcom/google/api/services/plusi/model/FeaturedActivityProto;)Z

    move-result v1

    return v1
.end method

.method public static hasYourActivity(Lcom/google/api/services/plusi/model/SimpleProfile;)Z
    .locals 2
    .param p0    # Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getUserActivityStory(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/FeaturedActivityProto;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->hasActivity(Lcom/google/api/services/plusi/model/FeaturedActivityProto;)Z

    move-result v1

    return v1
.end method
