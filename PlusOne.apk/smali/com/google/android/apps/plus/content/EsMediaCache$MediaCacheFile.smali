.class final Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;
.super Ljava/lang/Object;
.source "EsMediaCache.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/content/EsMediaCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MediaCacheFile"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;",
        ">;"
    }
.end annotation


# instance fields
.field file:Ljava/io/File;

.field recent:Z

.field size:J

.field timestamp:J


# direct methods
.method public constructor <init>(Ljava/io/File;J)V
    .locals 4
    .param p1    # Ljava/io/File;
    .param p2    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->file:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->timestamp:J

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->size:J

    iget-wide v0, p0, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->timestamp:J

    sub-long v0, p2, v0

    const-wide/32 v2, 0x1b7740

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->recent:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 4
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->recent:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p1, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->recent:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->timestamp:J

    iget-wide v2, p1, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->timestamp:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0

    :cond_1
    iget-boolean v0, p1, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->recent:Z

    if-eqz v0, :cond_2

    const/4 v0, -0x1

    goto :goto_0

    :cond_2
    iget-wide v0, p1, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->size:J

    iget-wide v2, p0, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->size:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method
