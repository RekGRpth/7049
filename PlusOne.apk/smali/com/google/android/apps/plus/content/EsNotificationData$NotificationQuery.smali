.class public interface abstract Lcom/google/android/apps/plus/content/EsNotificationData$NotificationQuery;
.super Ljava/lang/Object;
.source "EsNotificationData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/content/EsNotificationData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NotificationQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x1a

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "notif_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "coalescing_code"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "category"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "message"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "circle_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "pd_gaia_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "pd_album_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "pd_album_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "pd_photo_id"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "activity_id"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "read"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "ed_event"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "ed_event_id"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "ed_creator_id"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "notification_type"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "entity_type"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "entity_snippet"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "entity_photos_data"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "entity_squares_data"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "square_id"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "square_name"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "square_photo_url"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "taggee_photo_ids"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "taggee_data_actors"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/content/EsNotificationData$NotificationQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
