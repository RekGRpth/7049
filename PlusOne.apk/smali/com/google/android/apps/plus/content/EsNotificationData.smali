.class public final Lcom/google/android/apps/plus/content/EsNotificationData;
.super Ljava/lang/Object;
.source "EsNotificationData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/content/EsNotificationData$NotificationQuery;,
        Lcom/google/android/apps/plus/content/EsNotificationData$NotificationTimestampsQuery;,
        Lcom/google/android/apps/plus/content/EsNotificationData$NotificationIdsQuery;,
        Lcom/google/android/apps/plus/content/EsNotificationData$IdAndTimestampQuery;
    }
.end annotation


# static fields
.field private static final MAP_CATEGORY:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAP_ENTITY_TYPE:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAP_NOTIFICATION_TYPE:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final SQL_COUNT_UNREAD_NOTIFICATIONS:Ljava/lang/String;

.field private static final mSyncLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "CIRCLE"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "ENTITYPROFILE"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "EVENTS"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "GAMES"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "GENERIC_CATEGORY"

    const v2, 0xffff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "HANGOUT"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "MOBILE"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "PHOTOS"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "QUESTIONS"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "SQUARE"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "STREAM"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "SYSTEM"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "TARGET"

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "ACTIVITY"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "ALBUM"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_SHARE"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "DEPRECATED_SYSTEM_TACO"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "EVENT"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "MATERIALIZED_TORTILLA"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "PHOTO"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "QUESTION"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "RESHARED"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "UNKNOWN_ENTITY_TYPE"

    const v2, 0xffff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "ASPEN_INVITE"

    const/16 v2, 0x4a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "BIRTHDAY_WISH"

    const/16 v2, 0x3f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_CONTACT_JOINED"

    const/16 v2, 0x45

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_DIGESTED_ADD"

    const/16 v2, 0x28

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_EXPLICIT_INVITE"

    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_INVITE_REQUEST"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_INVITEE_JOINED_ES"

    const/16 v2, 0x26

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_MEMBER_JOINED_ES"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_PERSONAL_ADD"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_RECIPROCATING_ADD"

    const/16 v2, 0x27

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_RECOMMEND_PEOPLE"

    const/16 v2, 0x42

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_STATUS_CHANGE"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "DIGEST_SWEEP"

    const/16 v2, 0x46

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "ENTITYPROFILE_ADD_ADMIN"

    const/16 v2, 0x22

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "ENTITYPROFILE_REMOVE_ADMIN"

    const/16 v2, 0x23

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "ENTITYPROFILE_TRANSFER_OWNERSHIP"

    const/16 v2, 0x24

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_BEFORE_REMINDER"

    const/16 v2, 0x3b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_CHANGE"

    const/16 v2, 0x35

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_CHECKIN"

    const/16 v2, 0x3a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_INVITE"

    const/16 v2, 0x2f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_INVITEE_CHANGE"

    const/16 v2, 0x39

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_PHOTOS_ADDED"

    const/16 v2, 0x3e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_PHOTOS_COLLECTION"

    const/16 v2, 0x38

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_PHOTOS_REMINDER"

    const/16 v2, 0x37

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_RSVP_CONFIRMATION"

    const/16 v2, 0x43

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_STARTING"

    const/16 v2, 0x36

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "GAMES_APPLICATION_MESSAGE"

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "GAMES_INVITE_REQUEST"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "GAMES_ONEUP_NOTIFICATION"

    const/16 v2, 0x49

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "GAMES_PERSONAL_MESSAGE"

    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "HANGOUT_INVITE"

    const/16 v2, 0x21

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "MOBILE_NEW_CONVERSATION"

    const/16 v2, 0x1d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "PHOTOS_CAMERASYNC_UPLOADED"

    const/16 v2, 0x12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "PHOTOS_FACE_SUGGESTED"

    const/16 v2, 0x29

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "PHOTOS_PROFILE_PHOTO_SUGGESTED"

    const/16 v2, 0x44

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "PHOTOS_PROFILE_PHOTO_SUGGESTION_ACCEPTED"

    const/16 v2, 0x47

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "PHOTOS_TAG_ADDED_ON_PHOTO"

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "PHOTOS_TAGGED_IN_PHOTO"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "QUESTIONS_ANSWERER_FOLLOWUP"

    const/16 v2, 0x1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "QUESTIONS_ASKER_FOLLOWUP"

    const/16 v2, 0x1f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "QUESTIONS_DASHER_WELCOME"

    const/16 v2, 0x1b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "QUESTIONS_REFERRAL"

    const/16 v2, 0x13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "QUESTIONS_REPLY"

    const/16 v2, 0x16

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "QUESTIONS_UNANSWERED_QUESTION"

    const/16 v2, 0x1c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SQUARE_ABUSE"

    const/16 v2, 0x4f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SQUARE_INVITE"

    const/16 v2, 0x30

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SQUARE_MEMBERSHIP_APPROVED"

    const/16 v2, 0x33

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SQUARE_MEMBERSHIP_REQUEST"

    const/16 v2, 0x34

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SQUARE_NAME_CHANGE"

    const/16 v2, 0x48

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SQUARE_NEW_MODERATOR"

    const/16 v2, 0x41

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SQUARE_SUBSCRIPTION"

    const/16 v2, 0x31

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_AT_REPLY"

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_FOLLOWUP"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_FOR_PHOTO_TAGGED"

    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_FOR_PHOTO_TAGGER"

    const/16 v2, 0x1a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_NEW"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_ON_MENTION"

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_LIKE"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_PLUSONE_COMMENT"

    const/16 v2, 0x15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_PLUSONE_POST"

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_POST_AT_REPLY"

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_POST_FROM_UNCIRCLED"

    const/16 v2, 0x3d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_POST_SHARED"

    const/16 v2, 0x18

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_POST"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_POST_SUBSCRIBED"

    const/16 v2, 0x40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_RESHARE"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SYSTEM_CELEBRITY_SUGGESTIONS"

    const/16 v2, 0x2d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SYSTEM_CONNECTED_SITES"

    const/16 v2, 0x2e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SYSTEM_DO_NOT_USE"

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SYSTEM_FRIEND_SUGGESTIONS"

    const/16 v2, 0x2c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SYSTEM_INVITE"

    const/16 v2, 0x25

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SYSTEM_TOOLTIP"

    const/16 v2, 0x2b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SYSTEM_WELCOME"

    const/16 v2, 0x2a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "TARGET_SHARED"

    const/16 v2, 0x3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "UNKNOWN_NOTIFICATION_TYPE"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->mSyncLock:Ljava/lang/Object;

    const-string v0, "SELECT COUNT(*) FROM %s WHERE %s"

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "notifications"

    aput-object v3, v1, v2

    const-string v2, "read=0 AND seen=0 AND enabled=1"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->SQL_COUNT_UNREAD_NOTIFICATIONS:Ljava/lang/String;

    return-void
.end method

.method static cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 12
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const/4 v9, 0x0

    const-wide/16 v10, 0xc8

    const/4 v3, 0x0

    const-string v0, "notifications"

    invoke-static {p0, v0, v3, v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getRowsCount(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    const-string v0, "EsNotificationData"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsNotificationData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteOldNotifications, keep count: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", have: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sub-long v0, v4, v10

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-lez v0, :cond_3

    const-string v1, "notifications"

    sget-object v2, Lcom/google/android/apps/plus/content/EsNotificationData$NotificationIdsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v7, "timestamp ASC"

    sub-long/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_3

    new-instance v2, Ljava/lang/StringBuffer;

    const/16 v0, 0x100

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    :try_start_0
    const-string v0, "notif_id IN("

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v0, :cond_1

    move v0, v9

    :goto_1
    const/16 v4, 0x27

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v4, 0x27

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    const/16 v4, 0x2c

    :try_start_1
    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_2
    const/16 v0, 0x29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v0, "notifications"

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_3
    return-void
.end method

.method public static deactivateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 0
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->cancelAll(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->cancelQuotaNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->cancelFirstTimeFullSizeNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatNotifications;->cancel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method

.method public static getLatestNotificationTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)D
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const-wide/high16 v10, -0x4010000000000000L

    const/4 v3, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "notifications"

    sget-object v2, Lcom/google/android/apps/plus/content/EsNotificationData$NotificationTimestampsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v7, "timestamp DESC"

    const-string v8, "1"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    if-nez v9, :cond_0

    move-wide v1, v10

    :goto_0
    return-wide v1

    :cond_0
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getDouble(I)D
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move-wide v1, v10

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static getNotificationType(Ljava/lang/String;)I
    .locals 1
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getNotificationsToDisplay(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/database/Cursor;
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v9, 0x4

    const/4 v4, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "notifications"

    sget-object v2, Lcom/google/android/apps/plus/content/EsNotificationData$NotificationQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "read=0 AND seen=0 AND enabled=1"

    const-string v7, "timestamp DESC"

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_1

    const-string v1, "EsNotificationData"

    invoke-static {v1, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "EsNotificationData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getNotificationsToDisplay: unread notification id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", coalescingCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", timestamp: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x5

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const/4 v1, -0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_1
    return-object v8
.end method

.method public static getNumSquarePosts(Lcom/google/api/services/plusi/model/EntitySquaresData;)I
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/EntitySquaresData;

    iget-object v0, p0, Lcom/google/api/services/plusi/model/EntitySquaresData;->subscription:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/EntitySquaresData;->subscription:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getOldestUnreadNotificationTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)D
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const-wide/high16 v10, -0x4010000000000000L

    const/4 v4, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "notifications"

    sget-object v2, Lcom/google/android/apps/plus/content/EsNotificationData$NotificationTimestampsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "read=0"

    const-string v7, "timestamp ASC"

    const-string v8, "1"

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    if-nez v9, :cond_0

    move-wide v1, v10

    :goto_0
    return-wide v1

    :cond_0
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getDouble(I)D
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move-wide v1, v10

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static getSquarePostActivityId(Lcom/google/api/services/plusi/model/EntitySquaresData;Z)Ljava/lang/String;
    .locals 4
    .param p0    # Lcom/google/api/services/plusi/model/EntitySquaresData;
    .param p1    # Z

    iget-object v3, p0, Lcom/google/api/services/plusi/model/EntitySquaresData;->subscription:Ljava/util/List;

    if-eqz v3, :cond_3

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/api/services/plusi/model/EntitySquaresData;->subscription:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_3

    iget-object v3, p0, Lcom/google/api/services/plusi/model/EntitySquaresData;->subscription:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareSubscription;

    if-eqz v2, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    iget-object v3, v2, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareSubscription;->isRead:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    iget-object v3, v2, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareSubscription;->activityId:Ljava/lang/String;

    :goto_1
    return-object v3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static getUnreadCount(Landroid/database/sqlite/SQLiteDatabase;)J
    .locals 2
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->SQL_COUNT_UNREAD_NOTIFICATIONS:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getUnreadSquarePosts(Lcom/google/api/services/plusi/model/EntitySquaresData;)I
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/EntitySquaresData;

    iget-object v0, p0, Lcom/google/api/services/plusi/model/EntitySquaresData;->renderSquaresData:Lcom/google/api/services/plusi/model/EntitySquaresDataRenderSquaresData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/EntitySquaresData;->renderSquaresData:Lcom/google/api/services/plusi/model/EntitySquaresDataRenderSquaresData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EntitySquaresDataRenderSquaresData;->renderSubscriptionData:Lcom/google/api/services/plusi/model/EntitySquaresDataRenderSquaresDataRenderSquareSubscriptionData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/EntitySquaresData;->renderSquaresData:Lcom/google/api/services/plusi/model/EntitySquaresDataRenderSquaresData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EntitySquaresDataRenderSquaresData;->renderSubscriptionData:Lcom/google/api/services/plusi/model/EntitySquaresDataRenderSquaresDataRenderSquareSubscriptionData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EntitySquaresDataRenderSquaresDataRenderSquareSubscriptionData;->numUnread:Ljava/lang/Integer;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static insertNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;DDZLjava/util/Map;)V
    .locals 68
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # D
    .param p5    # D
    .param p7    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCoalescedItem;",
            ">;DDZ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/api/services/plusi/model/DataActor;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p7, :cond_1

    sget-object v17, Lcom/google/android/apps/plus/analytics/OzActions;->NOTIFICATION_FETCHED_FROM_TICKLE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_0
    if-eqz p7, :cond_2

    sget-object v59, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_SYSTEM:Lcom/google/android/apps/plus/analytics/OzViews;

    :goto_1
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    :try_start_0
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsNotificationData;->getUnreadCount(Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v54

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    const-wide/16 v5, 0x0

    cmpg-double v5, p3, v5

    if-gtz v5, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v6, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_0
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    new-instance v33, Landroid/os/Bundle;

    invoke-direct/range {v33 .. v33}, Landroid/os/Bundle;-><init>()V

    const-string v5, "extra_num_unread_notifi"

    move-wide/from16 v0, v54

    long-to-int v6, v0

    move-object/from16 v0, v33

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v5, "extra_prev_num_unread_noti"

    move-wide/from16 v0, v54

    long-to-int v6, v0

    move-object/from16 v0, v33

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v5, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-object/from16 v0, v59

    invoke-direct {v5, v0}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    move-object/from16 v3, v33

    invoke-static {v0, v1, v5, v2, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :goto_2
    return-void

    :cond_1
    sget-object v17, Lcom/google/android/apps/plus/analytics/OzActions;->NOTIFICATION_FETCHED_FROM_USER_REFRESH:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_0

    :cond_2
    sget-object v59, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_WIDGET:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_1

    :cond_3
    :try_start_1
    new-instance v64, Ljava/util/HashMap;

    invoke-direct/range {v64 .. v64}, Ljava/util/HashMap;-><init>()V

    const-string v5, "notifications"

    sget-object v6, Lcom/google/android/apps/plus/content/EsNotificationData$IdAndTimestampQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v25

    :goto_3
    :try_start_2
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    move-object/from16 v0, v64

    invoke-virtual {v0, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v5

    :try_start_3
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v5

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v5

    :cond_4
    :try_start_4
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    const-string v5, "SELECT MAX(%s) FROM %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v9, "timestamp"

    aput-object v9, v6, v7

    const/4 v7, 0x1

    const-string v9, "notifications"

    aput-object v9, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v60

    invoke-virtual/range {v60 .. v60}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J

    move-result-wide v41

    new-instance v46, Ljava/util/ArrayList;

    invoke-direct/range {v46 .. v46}, Ljava/util/ArrayList;-><init>()V

    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    new-instance v67, Landroid/content/ContentValues;

    invoke-direct/range {v67 .. v67}, Landroid/content/ContentValues;-><init>()V

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v34

    :cond_5
    :goto_4
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3f

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lcom/google/api/services/plusi/model/DataCoalescedItem;

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->id:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->coalescingCode:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    move-object/from16 v0, v44

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->id:Ljava/lang/String;

    move-object/from16 v37, v0

    move-object/from16 v0, v44

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->coalescingCode:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    invoke-static {v5}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeDouble(Ljava/lang/Double;)D

    move-result-wide v62

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->isEntityDeleted:Ljava/lang/Boolean;

    invoke-static {v5}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v38

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->category:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_9

    sget-object v6, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    sget-object v6, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v22

    :goto_5
    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityReferenceType:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a

    sget-object v6, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    sget-object v6, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v30

    :goto_6
    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->isRead:Ljava/lang/Boolean;

    invoke-static {v5}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v5

    if-nez v5, :cond_6

    cmpg-double v5, v62, p5

    if-gtz v5, :cond_b

    :cond_6
    const/16 v39, 0x1

    :goto_7
    const-string v5, "EsNotificationData"

    const/4 v6, 0x4

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_8

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Notification id: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v37

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", coalescingCode: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", category: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v44

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->category:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", filterType: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v44

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->filterType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", timestamp: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v62

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", read: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v39

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", entityDeleted: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v38

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", pushEnabled: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v44

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->pushEnabled:Ljava/lang/Boolean;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    if-eqz v5, :cond_7

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->summarySnippet:Lcom/google/api/services/plusi/model/EntitySummaryData;

    if-eqz v5, :cond_7

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v43

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", snippet: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v44

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/EntityEntityData;->summarySnippet:Lcom/google/api/services/plusi/model/EntitySummaryData;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/EntitySummaryData;->summaryPlaintext:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v43

    :cond_7
    const-string v5, "EsNotificationData"

    move-object/from16 v0, v43

    invoke-static {v5, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    move-object/from16 v0, v64

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v48

    check-cast v48, Ljava/lang/Double;

    if-eqz v48, :cond_c

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    cmpl-double v5, v5, v62

    if-nez v5, :cond_c

    if-nez v39, :cond_c

    const-string v5, "EsNotificationData"

    const/4 v6, 0x4

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v5, "EsNotificationData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Ignore notification with same timestamp and not read. Id: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v37

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_9
    const v22, 0xffff

    goto/16 :goto_5

    :cond_a
    const v30, 0xffff

    goto/16 :goto_6

    :cond_b
    const/16 v39, 0x0

    goto/16 :goto_7

    :cond_c
    invoke-virtual/range {v67 .. v67}, Landroid/content/ContentValues;->clear()V

    const-string v5, "notif_id"

    move-object/from16 v0, v67

    move-object/from16 v1, v37

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "coalescing_code"

    move-object/from16 v0, v67

    move-object/from16 v1, v23

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "timestamp"

    invoke-static/range {v62 .. v63}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    const-string v5, "entity_type"

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v5, 0x1

    move/from16 v0, v22

    if-ne v0, v5, :cond_d

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityReference:Ljava/lang/String;

    if-eqz v5, :cond_d

    const-string v5, "activity_id"

    move-object/from16 v0, v44

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityReference:Ljava/lang/String;

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    const/16 v45, 0x0

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->action:Ljava/util/List;

    if-eqz v5, :cond_13

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->action:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_13

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->action:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v35

    :cond_e
    :goto_8
    invoke-interface/range {v35 .. v35}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_12

    invoke-interface/range {v35 .. v35}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/api/services/plusi/model/DataAction;

    if-eqz v16, :cond_e

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    if-eqz v5, :cond_e

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_e

    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v36

    :cond_f
    :goto_9
    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_11

    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/google/api/services/plusi/model/DataItem;

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataItem;->actor:Lcom/google/api/services/plusi/model/DataActor;

    move-object/from16 v18, v0

    if-eqz v18, :cond_f

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    if-eqz v5, :cond_10

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsAvatarData;->compressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    :cond_10
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v5, "EsNotificationData"

    const/4 v6, 0x4

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_f

    const-string v5, "EsNotificationData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "- Actor name: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " gaiaId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " photoUrl: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    :cond_11
    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/DataItem;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/DataItem;->notificationType:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsNotificationData;->getNotificationType(Ljava/lang/String;)I

    move-result v45

    goto/16 :goto_8

    :cond_12
    const-string v5, "circle_data"

    move-object/from16 v0, v44

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->action:Ljava/util/List;

    invoke-static {v6}, Lcom/google/android/apps/plus/content/DbDataAction;->serializeDataActionList(Ljava/util/List;)[B

    move-result-object v6

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :cond_13
    const-string v5, "notification_type"

    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "read"

    invoke-static/range {v39 .. v39}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v5, "seen"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->pushEnabled:Ljava/lang/Boolean;

    if-eqz v5, :cond_16

    const-string v5, "enabled"

    move-object/from16 v0, v44

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->pushEnabled:Ljava/lang/Boolean;

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :goto_a
    invoke-static/range {v45 .. v45}, Lcom/google/android/apps/plus/content/EsNotificationData;->isEventNotificationType(I)Z

    move-result v5

    if-eqz v5, :cond_14

    const/16 v22, 0xa

    :cond_14
    const/16 v5, 0xa

    move/from16 v0, v22

    if-ne v0, v5, :cond_17

    const/16 v32, 0x1

    :goto_b
    const/16 v5, 0x12

    move/from16 v0, v45

    if-ne v0, v5, :cond_19

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->opaqueClientFields:Ljava/util/List;

    if-eqz v5, :cond_19

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->opaqueClientFields:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v35

    :cond_15
    :goto_c
    invoke-interface/range {v35 .. v35}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_19

    invoke-interface/range {v35 .. v35}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Lcom/google/api/services/plusi/model/DataKvPair;

    const-string v5, "TAGGEE_OGIDS"

    move-object/from16 v0, v49

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataKvPair;->key:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_18

    move-object/from16 v0, v49

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataKvPair;->value:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_18

    move-object/from16 v0, v49

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataKvPair;->value:Ljava/lang/String;

    move-object/from16 v0, p8

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/content/PhotoTaggeeData;->createDataActorList(Ljava/util/Map;Ljava/lang/String;)Ljava/util/List;

    move-result-object v61

    invoke-interface/range {v61 .. v61}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_15

    const-string v5, "taggee_data_actors"

    invoke-static/range {v61 .. v61}, Lcom/google/android/apps/plus/content/DbDataAction;->serializeDataActorList(Ljava/util/List;)[B

    move-result-object v6

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto :goto_c

    :cond_16
    const-string v5, "enabled"

    invoke-static/range {v39 .. v39}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_a

    :cond_17
    const/16 v32, 0x0

    goto :goto_b

    :cond_18
    const-string v5, "TAGGEE_PHOTO_IDS"

    move-object/from16 v0, v49

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataKvPair;->key:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_15

    move-object/from16 v0, v49

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataKvPair;->value:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_15

    const-string v5, "taggee_photo_ids"

    move-object/from16 v0, v49

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataKvPair;->value:Ljava/lang/String;

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    :cond_19
    const/16 v26, 0x0

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    if-eqz v5, :cond_2c

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->summarySnippet:Lcom/google/api/services/plusi/model/EntitySummaryData;

    if-eqz v5, :cond_1a

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->summarySnippet:Lcom/google/api/services/plusi/model/EntitySummaryData;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/EntitySummaryData;->summaryPlaintext:Ljava/lang/String;

    move-object/from16 v26, v0

    :cond_1a
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/4 v13, 0x0

    const/16 v51, 0x0

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    if-eqz v5, :cond_1d

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    if-eqz v5, :cond_1b

    new-instance v8, Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-direct {v8, v5}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v31

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v7, 0x0

    const-string v9, "DEFAULT"

    const/4 v10, 0x0

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-static/range {v5 .. v10}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivitiesAndOverwrite(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Z)V

    :cond_1b
    move-object/from16 v0, v31

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntityUpdateData;->safeAnnotationHtml:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2f

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EntityUpdateData;->safeAnnotationHtml:Ljava/lang/String;

    move-object/from16 v29, v0

    :goto_d
    invoke-static/range {v29 .. v29}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1c

    const-string v5, "entity_snippet"

    move-object/from16 v0, v67

    move-object/from16 v1, v29

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1c
    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    if-eqz v5, :cond_1d

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    if-eqz v5, :cond_1d

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusPhoto:Lcom/google/api/services/plusi/model/PlusPhoto;

    if-eqz v5, :cond_1d

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusPhoto:Lcom/google/api/services/plusi/model/PlusPhoto;

    move-object/from16 v53, v0

    move-object/from16 v0, v53

    iget-object v13, v0, Lcom/google/api/services/plusi/model/PlusPhoto;->ownerObfuscatedId:Ljava/lang/String;

    move-object/from16 v0, v53

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusPhoto;->albumId:Ljava/lang/String;

    move-object/from16 v20, v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    move-object/from16 v0, v53

    iget-object v5, v0, Lcom/google/api/services/plusi/model/PlusPhoto;->photoId:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1d

    move-object/from16 v0, v53

    iget-object v5, v0, Lcom/google/api/services/plusi/model/PlusPhoto;->photoId:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v51

    :cond_1d
    :goto_e
    :try_start_6
    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->photos:Lcom/google/api/services/plusi/model/EntityPhotosData;

    move-object/from16 v28, v0

    const/4 v5, 0x3

    move/from16 v0, v22

    if-ne v0, v5, :cond_22

    if-eqz v28, :cond_22

    const-string v5, "entity_photos_data"

    invoke-static {}, Lcom/google/api/services/plusi/model/EntityPhotosDataJson;->getInstance()Lcom/google/api/services/plusi/model/EntityPhotosDataJson;

    move-result-object v6

    move-object/from16 v0, v28

    invoke-virtual {v6, v0}, Lcom/google/api/services/plusi/model/EntityPhotosDataJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v6

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    move-object/from16 v0, v28

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->photo:Ljava/util/List;

    if-eqz v5, :cond_22

    move-object/from16 v0, v28

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->photo:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_22

    move-object/from16 v0, v28

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->photo:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v50

    check-cast v50, Lcom/google/api/services/plusi/model/DataPhoto;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v50, :cond_20

    :try_start_7
    move-object/from16 v0, v50

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1e

    move-object/from16 v0, v50

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v51

    :cond_1e
    :goto_f
    :try_start_8
    move-object/from16 v0, v50

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    if-eqz v5, :cond_1f

    move-object/from16 v0, v50

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v50

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/DataAlbum;->title:Ljava/lang/String;

    move-object/from16 v21, v0

    :cond_1f
    move-object/from16 v0, v50

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    if-eqz v5, :cond_20

    move-object/from16 v0, v50

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v13, v5, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    :cond_20
    const/16 v5, 0x12

    move/from16 v0, v45

    if-ne v0, v5, :cond_22

    if-nez v39, :cond_22

    const-string v5, "EsNotificationData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_21

    move-object/from16 v0, v28

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->numPhotos:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move-object/from16 v0, v28

    iget-object v6, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->numVideos:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    add-int v47, v5, v6

    const-string v5, "EsNotificationData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Insert "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v47

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " IU photos into the photo table!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_21
    const/4 v11, 0x0

    const-string v12, "camerasync"

    move-object/from16 v0, v28

    iget-object v14, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->photo:Ljava/util/List;

    const/4 v15, 0x1

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    invoke-static/range {v9 .. v15}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertStreamPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V

    :cond_22
    const-string v5, "EsNotificationData"

    const/4 v6, 0x4

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_24

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_23

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_23

    if-eqz v51, :cond_24

    :cond_23
    const-string v5, "EsNotificationData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "- Photo ownerId: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " albumId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " photoId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v51

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_24
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_25

    const-string v5, "pd_gaia_id"

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_25
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_26

    const-string v5, "pd_album_id"

    move-object/from16 v0, v67

    move-object/from16 v1, v20

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_26
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_27

    const-string v5, "pd_album_name"

    move-object/from16 v0, v67

    move-object/from16 v1, v21

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_27
    if-eqz v51, :cond_28

    const-string v5, "pd_photo_id"

    move-object/from16 v0, v67

    move-object/from16 v1, v51

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_28
    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->squares:Lcom/google/api/services/plusi/model/EntitySquaresData;

    if-eqz v5, :cond_2a

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->squares:Lcom/google/api/services/plusi/model/EntitySquaresData;

    move-object/from16 v58, v0

    const-string v5, "entity_squares_data"

    invoke-static {}, Lcom/google/api/services/plusi/model/EntitySquaresDataJson;->getInstance()Lcom/google/api/services/plusi/model/EntitySquaresDataJson;

    move-result-object v6

    move-object/from16 v0, v58

    invoke-virtual {v6, v0}, Lcom/google/api/services/plusi/model/EntitySquaresDataJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v6

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->invite:Ljava/util/List;

    if-eqz v5, :cond_35

    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->invite:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_35

    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->invite:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareInvite;

    if-eqz v5, :cond_35

    iget-object v6, v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareInvite;->square:Lcom/google/api/services/plusi/model/EntitySquaresDataSquare;

    if-eqz v6, :cond_35

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareInvite;->square:Lcom/google/api/services/plusi/model/EntitySquaresDataSquare;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquare;->oid:Ljava/lang/String;

    move-object/from16 v57, v0

    :goto_10
    const-string v5, "EsNotificationData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_29

    const-string v5, "EsNotificationData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "- squareId: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v57

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_29
    invoke-static/range {v57 .. v57}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2a

    const-string v5, "square_id"

    move-object/from16 v0, v67

    move-object/from16 v1, v57

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p8

    move-object/from16 v1, v57

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v56

    check-cast v56, Lcom/google/api/services/plusi/model/DataActor;

    if-eqz v56, :cond_2a

    const-string v5, "square_name"

    move-object/from16 v0, v56

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "square_photo_url"

    move-object/from16 v0, v56

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/apps/plus/content/EsAvatarData;->compressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2a
    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    if-eqz v5, :cond_2c

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    if-eqz v5, :cond_2c

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    if-eqz v5, :cond_2c

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v5, :cond_2c

    move-object/from16 v0, v44

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    move-object/from16 v52, v0

    const-string v5, "ed_event_id"

    move-object/from16 v0, v52

    iget-object v6, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "ed_creator_id"

    move-object/from16 v0, v52

    iget-object v6, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v5, 0x3a

    move/from16 v0, v45

    if-ne v5, v0, :cond_2b

    move-object/from16 v0, v52

    iget-object v5, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2b

    const-string v5, "read"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :cond_2b
    const/16 v22, 0xa

    const/16 v32, 0x1

    :cond_2c
    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2d

    if-eqz v38, :cond_2e

    :cond_2d
    if-eqz v32, :cond_3c

    sget v5, Lcom/google/android/apps/plus/R$string;->notification_event_deleted:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v26

    :cond_2e
    :goto_11
    const-string v5, "message"

    move-object/from16 v0, v67

    move-object/from16 v1, v26

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "ed_event"

    if-eqz v32, :cond_3e

    const/4 v5, 0x1

    :goto_12
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v67

    invoke-virtual {v0, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "category"

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v67

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "notifications"

    const-string v6, "coalescing_code"

    const/4 v7, 0x5

    move-object/from16 v0, v67

    invoke-virtual {v4, v5, v6, v0, v7}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-wide/from16 v0, v41

    long-to-double v5, v0

    cmpl-double v5, v62, v5

    if-lez v5, :cond_5

    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v46

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_2f
    move-object/from16 v0, v31

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntityUpdateData;->safeTitleHtml:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_30

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EntityUpdateData;->safeTitleHtml:Ljava/lang/String;

    move-object/from16 v29, v0

    goto/16 :goto_d

    :cond_30
    move-object/from16 v0, v31

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntityUpdateData;->summary:Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;

    if-eqz v5, :cond_34

    move-object/from16 v0, v31

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntityUpdateData;->summary:Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;

    iget-object v6, v5, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;->bodySanitizedHtml:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_31

    iget-object v0, v5, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;->bodySanitizedHtml:Ljava/lang/String;

    move-object/from16 v29, v0

    goto/16 :goto_d

    :cond_31
    iget-object v6, v5, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;->activityContentSanitizedHtml:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_32

    iget-object v0, v5, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;->activityContentSanitizedHtml:Ljava/lang/String;

    move-object/from16 v29, v0

    goto/16 :goto_d

    :cond_32
    iget-object v6, v5, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;->headerSanitizedHtml:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_33

    iget-object v0, v5, Lcom/google/api/services/plusi/model/EntityUpdateDataSummarySnippet;->activityContentSanitizedHtml:Ljava/lang/String;

    move-object/from16 v29, v0

    goto/16 :goto_d

    :cond_33
    const/16 v29, 0x0

    goto/16 :goto_d

    :cond_34
    const/16 v29, 0x0

    goto/16 :goto_d

    :catch_0
    move-exception v27

    const-string v5, "EsNotificationData"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1d

    const-string v5, "EsNotificationData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Invalid photoId "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_e

    :catch_1
    move-exception v27

    const-string v5, "EsNotificationData"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1e

    const-string v5, "EsNotificationData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Invalid photoId "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_f

    :cond_35
    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->membershipApproved:Ljava/util/List;

    if-eqz v5, :cond_36

    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->membershipApproved:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_36

    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->membershipApproved:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareMembershipApproved;

    if-eqz v5, :cond_36

    iget-object v6, v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareMembershipApproved;->square:Lcom/google/api/services/plusi/model/EntitySquaresDataSquare;

    if-eqz v6, :cond_36

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareMembershipApproved;->square:Lcom/google/api/services/plusi/model/EntitySquaresDataSquare;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquare;->oid:Ljava/lang/String;

    move-object/from16 v57, v0

    goto/16 :goto_10

    :cond_36
    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->membershipRequest:Ljava/util/List;

    if-eqz v5, :cond_37

    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->membershipRequest:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_37

    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->membershipRequest:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareMembershipRequest;

    if-eqz v5, :cond_37

    iget-object v6, v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareMembershipRequest;->square:Lcom/google/api/services/plusi/model/EntitySquaresDataSquare;

    if-eqz v6, :cond_37

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareMembershipRequest;->square:Lcom/google/api/services/plusi/model/EntitySquaresDataSquare;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquare;->oid:Ljava/lang/String;

    move-object/from16 v57, v0

    goto/16 :goto_10

    :cond_37
    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->newModerator:Ljava/util/List;

    if-eqz v5, :cond_38

    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->newModerator:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_38

    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->newModerator:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/EntitySquaresDataNewModerator;

    if-eqz v5, :cond_38

    iget-object v0, v5, Lcom/google/api/services/plusi/model/EntitySquaresDataNewModerator;->squareOid:Ljava/lang/String;

    move-object/from16 v57, v0

    goto/16 :goto_10

    :cond_38
    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->squareNameChange:Ljava/util/List;

    if-eqz v5, :cond_39

    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->squareNameChange:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_39

    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->squareNameChange:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareNameChange;

    if-eqz v5, :cond_39

    iget-object v0, v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareNameChange;->squareOid:Ljava/lang/String;

    move-object/from16 v57, v0

    goto/16 :goto_10

    :cond_39
    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->subscription:Ljava/util/List;

    if-eqz v5, :cond_3a

    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->subscription:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_3a

    move-object/from16 v0, v58

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->subscription:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareSubscription;

    if-eqz v5, :cond_3a

    iget-object v6, v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareSubscription;->square:Lcom/google/api/services/plusi/model/EntitySquaresDataSquare;

    if-eqz v6, :cond_3a

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareSubscription;->square:Lcom/google/api/services/plusi/model/EntitySquaresDataSquare;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/EntitySquaresDataSquare;->oid:Ljava/lang/String;

    move-object/from16 v57, v0

    goto/16 :goto_10

    :cond_3a
    const-string v5, "EsNotificationData"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3b

    const-string v5, "EsNotificationData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "No Square ID in notification:\n"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/api/services/plusi/model/EntitySquaresDataJson;->getInstance()Lcom/google/api/services/plusi/model/EntitySquaresDataJson;

    move-result-object v7

    move-object/from16 v0, v58

    invoke-virtual {v7, v0}, Lcom/google/api/services/plusi/model/EntitySquaresDataJson;->toPrettyString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3b
    const/16 v57, 0x0

    goto/16 :goto_10

    :cond_3c
    const/4 v5, 0x3

    move/from16 v0, v22

    if-ne v0, v5, :cond_3d

    sget v5, Lcom/google/android/apps/plus/R$string;->notification_photo_deleted:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_11

    :cond_3d
    sget v5, Lcom/google/android/apps/plus/R$string;->notification_post_deleted:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_11

    :cond_3e
    const/4 v5, 0x0

    goto/16 :goto_12

    :cond_3f
    new-instance v33, Landroid/os/Bundle;

    invoke-direct/range {v33 .. v33}, Landroid/os/Bundle;-><init>()V

    invoke-virtual/range {v46 .. v46}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_40

    invoke-virtual/range {v46 .. v46}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ne v5, v6, :cond_40

    const-string v5, "extra_notification_types"

    move-object/from16 v0, v33

    move-object/from16 v1, v46

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v5, "extra_coalescing_codes"

    move-object/from16 v0, v33

    move-object/from16 v1, v24

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_40
    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsNotificationData;->getUnreadCount(Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v65

    const-string v5, "extra_num_unread_notifi"

    move-wide/from16 v0, v65

    long-to-int v6, v0

    move-object/from16 v0, v33

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v5, "extra_prev_num_unread_noti"

    move-wide/from16 v0, v54

    long-to-int v6, v0

    move-object/from16 v0, v33

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v5, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-object/from16 v0, v59

    invoke-direct {v5, v0}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    move-object/from16 v3, v33

    invoke-static {v0, v1, v5, v2, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v6, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_2
.end method

.method public static isCommentNotificationType(I)Z
    .locals 1
    .param p0    # I

    sparse-switch p0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0xe -> :sswitch_0
        0xf -> :sswitch_0
        0x19 -> :sswitch_0
        0x1a -> :sswitch_0
    .end sparse-switch
.end method

.method public static isEventNotificationType(I)Z
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2f
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static markAllNotificationsAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-string v2, "EsNotificationData"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "EsNotificationData"

    const-string v3, "markAllNotificationsAsRead"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-nez p1, :cond_1

    const-string v2, "EsNotificationData"

    const-string v3, "markAllNotificationsAsRead: The account cannot be null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v4}, Landroid/content/ContentValues;-><init>(I)V

    const-string v2, "read"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "notifications"

    invoke-virtual {v0, v2, v1, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_URI:Landroid/net/Uri;

    invoke-static {v3, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    invoke-static {p0, p1, v4}, Lcom/google/android/apps/plus/service/AndroidNotification;->cancel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V

    goto :goto_0
.end method

.method public static markAllNotificationsAsSeen(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-string v2, "EsNotificationData"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "EsNotificationData"

    const-string v3, "markAllNotificationsAsSeen"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v4}, Landroid/content/ContentValues;-><init>(I)V

    const-string v2, "seen"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "notifications"

    invoke-virtual {v0, v2, v1, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public static markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x1

    const-string v2, "EsNotificationData"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "EsNotificationData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "markNotificationAsRead: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-nez p1, :cond_1

    const-string v2, "EsNotificationData"

    const-string v3, "markNotificationAsRead: The account cannot be null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    const-string v2, "read"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "notifications"

    const-string v3, "notif_id=?"

    new-array v4, v6, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsNotificationData;->getUnreadCount(Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    invoke-static {p0, p1, v6}, Lcom/google/android/apps/plus/service/AndroidNotification;->cancel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V

    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_URI:Landroid/net/Uri;

    invoke-static {v3, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public static syncNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V
    .locals 28
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p3    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v25, Lcom/google/android/apps/plus/content/EsNotificationData;->mSyncLock:Ljava/lang/Object;

    monitor-enter v25

    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v4

    if-eqz v4, :cond_0

    monitor-exit v25

    :goto_0
    return-void

    :cond_0
    const-string v4, "EsNotificationData"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "EsNotificationData"

    const-string v5, "syncNotifications starting sync stream"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v4, "Notifications"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsNotificationData;->getOldestUnreadNotificationTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)D

    move-result-wide v7

    const-wide/16 v4, 0x0

    cmpg-double v4, v7, v4

    if-gez v4, :cond_8

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsNotificationData;->getLatestNotificationTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)D

    move-result-wide v7

    const-string v4, "EsNotificationData"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "EsNotificationData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v11, "syncNotifications latest notification: "

    invoke-direct {v5, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-wide/16 v4, 0x0

    cmpl-double v4, v7, v4

    if-lez v4, :cond_3

    const-wide/high16 v4, 0x3ff0000000000000L

    add-double/2addr v7, v4

    :cond_3
    :goto_1
    const/16 v16, 0x0

    new-instance v24, Ljava/util/HashMap;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashMap;-><init>()V

    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    const/16 v17, 0x0

    const-wide/16 v9, 0x0

    const/16 v20, 0x0

    :goto_2
    const/4 v4, 0x3

    move/from16 v0, v20

    if-ge v0, v4, :cond_6

    const-string v4, "EsNotificationData"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "EsNotificationData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v11, "syncNotifications continuation token: "

    invoke-direct {v5, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v11, ", chunk: "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    new-instance v19, Lcom/google/android/apps/plus/api/GetNotificationsOperation;

    const/4 v4, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    if-nez v20, :cond_9

    move-wide v4, v7

    :goto_3
    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v5, v1}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->getNotifications(DLjava/lang/String;)V

    new-instance v4, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct {v4}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->start(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->hasError()Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v4, "EsNotificationData"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "EsNotificationData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v11, "syncNotifications error in chunk: "

    invoke-direct {v5, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v11, " with error code "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->getErrorCode()I

    move-result v11

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    const/16 v17, 0x1

    :cond_6
    if-nez v17, :cond_7

    new-instance v6, Ljava/util/ArrayList;

    invoke-interface/range {v24 .. v24}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-direct {v6, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v4, Lcom/google/android/apps/plus/content/EsNotificationData$1;

    invoke-direct {v4}, Lcom/google/android/apps/plus/content/EsNotificationData$1;-><init>()V

    invoke-static {v6, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move/from16 v11, p4

    invoke-static/range {v4 .. v12}, Lcom/google/android/apps/plus/content/EsNotificationData;->insertNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;DDZLjava/util/Map;)V

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->update(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_7
    invoke-interface/range {v24 .. v24}, Ljava/util/Map;->size()I

    move-result v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    monitor-exit v25
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v25

    throw v4

    :cond_8
    :try_start_1
    const-string v4, "EsNotificationData"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "EsNotificationData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v11, "syncNotifications oldest unread time: "

    invoke-direct {v5, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_9
    const-wide/16 v4, 0x0

    goto/16 :goto_3

    :cond_a
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->getLastReadTime()Ljava/lang/Double;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeDouble(Ljava/lang/Double;)D

    move-result-wide v9

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->getDataActors()Ljava/util/List;

    move-result-object v14

    if-eqz v14, :cond_c

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_b
    :goto_4
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/api/services/plusi/model/DataActor;

    if-eqz v13, :cond_b

    iget-object v4, v13, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    if-eqz v4, :cond_b

    iget-object v4, v13, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-interface {v12, v4, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_c
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->getNotifications()Ljava/util/List;

    move-result-object v15

    if-eqz v15, :cond_6

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "EsNotificationData"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_d

    const-string v4, "EsNotificationData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v11, "syncNotifications retrieved: "

    invoke-direct {v5, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_e
    :goto_5
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_11

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/google/api/services/plusi/model/DataCoalescedItem;

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->id:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/api/services/plusi/model/DataCoalescedItem;

    if-eqz v18, :cond_10

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    if-eqz v4, :cond_10

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    if-eqz v4, :cond_10

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    move-object/from16 v0, v18

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    invoke-virtual {v11}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v26

    cmpl-double v4, v4, v26

    if-lez v4, :cond_f

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "EsNotificationData"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_e

    const-string v4, "EsNotificationData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v11, "syncNotifications replacing notification: "

    invoke-direct {v5, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v11, " is newer: "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v23

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :cond_f
    const-string v4, "EsNotificationData"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_e

    const-string v4, "EsNotificationData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v11, "syncNotifications ignoring notification: "

    invoke-direct {v5, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v11, " is older: "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v23

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :cond_10
    move-object/from16 v0, v24

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    :cond_11
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->getContinuationToken()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v16

    if-eqz v16, :cond_6

    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_2
.end method
