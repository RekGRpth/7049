.class final Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;
.super Ljava/lang/Object;
.source "MediaTypeDetector.java"

# interfaces
.implements Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/content/MediaTypeDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DetectionRequest"
.end annotation


# instance fields
.field private mListener:Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;

.field private mUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/google/android/apps/plus/content/MediaTypeDetector;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/content/MediaTypeDetector;Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;Landroid/net/Uri;)V
    .locals 0
    .param p2    # Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;
    .param p3    # Landroid/net/Uri;

    iput-object p1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;->this$0:Lcom/google/android/apps/plus/content/MediaTypeDetector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;->mListener:Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;

    iput-object p3, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;->mUri:Landroid/net/Uri;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;->mUri:Landroid/net/Uri;

    return-object v0
.end method


# virtual methods
.method public final onPanoramaInfoLoaded$680664b4(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;->this$0:Lcom/google/android/apps/plus/content/MediaTypeDetector;

    iget-object v2, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;->mListener:Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;

    iget-object v3, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;->mUri:Landroid/net/Uri;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/content/MediaTypeDetector;->access$000(Lcom/google/android/apps/plus/content/MediaTypeDetector;Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;Landroid/net/Uri;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
