.class public final Lcom/google/android/apps/plus/content/MediaTypeDetector;
.super Landroid/os/HandlerThread;
.source "MediaTypeDetector.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;
    }
.end annotation


# static fields
.field private static sDetector:Lcom/google/android/apps/plus/content/MediaTypeDetector;

.field private static final sMediaTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mClient:Lcom/google/android/gms/panorama/PanoramaClient;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mWaitingForConnection:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->sMediaTypeMap:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const-string v0, "PanoramaDetector"

    const/16 v1, 0xa

    invoke-direct {p0, v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mQueue:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/content/MediaTypeDetector;Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;Landroid/net/Uri;Z)V
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/content/MediaTypeDetector;
    .param p1    # Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;
    .param p2    # Landroid/net/Uri;
    .param p3    # Z

    const/4 v1, 0x1

    if-eqz p3, :cond_0

    const/4 v0, 0x2

    :goto_0
    sget-object v2, Lcom/google/android/apps/plus/content/MediaTypeDetector;->sMediaTypeMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;->onMediaTypeDetected(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/apps/plus/util/ImageUtils;->getMimeType(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "image/gif"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    const-string v2, "image/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/content/MediaTypeDetector;Landroid/os/Message;)V
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/content/MediaTypeDetector;
    .param p1    # Landroid/os/Message;

    const/4 v2, 0x3

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mClient:Lcom/google/android/gms/panorama/PanoramaClient;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mClient:Lcom/google/android/gms/panorama/PanoramaClient;

    invoke-virtual {v1}, Lcom/google/android/gms/panorama/PanoramaClient;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "PanoramaDetector"

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "PanoramaDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Detecting if the image is a panorama: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;->mUri:Landroid/net/Uri;
    invoke-static {v0}, Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;->access$200(Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mClient:Lcom/google/android/gms/panorama/PanoramaClient;

    # getter for: Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;->mUri:Landroid/net/Uri;
    invoke-static {v0}, Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;->access$200(Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/panorama/PanoramaClient;->loadPanoramaInfo(Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;Landroid/net/Uri;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mQueue:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/MediaTypeDetector;->connect()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mClient:Lcom/google/android/gms/panorama/PanoramaClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mClient:Lcom/google/android/gms/panorama/PanoramaClient;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/PanoramaClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PanoramaDetector"

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "PanoramaDetector"

    const-string v1, "Disconnecting from GooglePlayServices"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mClient:Lcom/google/android/gms/panorama/PanoramaClient;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/PanoramaClient;->disconnect()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/content/MediaTypeDetector;)Lcom/google/android/gms/panorama/PanoramaClient;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/content/MediaTypeDetector;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mClient:Lcom/google/android/gms/panorama/PanoramaClient;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/content/MediaTypeDetector;Lcom/google/android/gms/panorama/PanoramaClient;)Lcom/google/android/gms/panorama/PanoramaClient;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/content/MediaTypeDetector;
    .param p1    # Lcom/google/android/gms/panorama/PanoramaClient;

    iput-object p1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mClient:Lcom/google/android/gms/panorama/PanoramaClient;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/content/MediaTypeDetector;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/content/MediaTypeDetector;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static clearCache()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->sMediaTypeMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method private declared-synchronized connect()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mClient:Lcom/google/android/gms/panorama/PanoramaClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mClient:Lcom/google/android/gms/panorama/PanoramaClient;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/PanoramaClient;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mWaitingForConnection:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mWaitingForConnection:Z

    new-instance v0, Lcom/google/android/apps/plus/content/MediaTypeDetector$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/content/MediaTypeDetector$2;-><init>(Lcom/google/android/apps/plus/content/MediaTypeDetector;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized detect(Landroid/content/Context;Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;Landroid/net/Uri;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;
    .param p2    # Landroid/net/Uri;

    const-class v2, Lcom/google/android/apps/plus/content/MediaTypeDetector;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/google/android/apps/plus/content/MediaTypeDetector;->sMediaTypeMap:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {p1, v1}, Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;->onMediaTypeDetected(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    :cond_1
    :try_start_1
    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/content/MediaTypeDetector;->sDetector:Lcom/google/android/apps/plus/content/MediaTypeDetector;

    if-nez v1, :cond_2

    new-instance v1, Lcom/google/android/apps/plus/content/MediaTypeDetector;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/content/MediaTypeDetector;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/google/android/apps/plus/content/MediaTypeDetector;->sDetector:Lcom/google/android/apps/plus/content/MediaTypeDetector;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/MediaTypeDetector;->start()V

    :cond_2
    sget-object v1, Lcom/google/android/apps/plus/content/MediaTypeDetector;->sDetector:Lcom/google/android/apps/plus/content/MediaTypeDetector;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/plus/content/MediaTypeDetector;->detectPanorama(Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;Landroid/net/Uri;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private declared-synchronized detectPanorama(Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;Landroid/net/Uri;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;
    .param p2    # Landroid/net/Uri;

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/MediaTypeDetector;->connect()V

    new-instance v0, Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/plus/content/MediaTypeDetector$DetectionRequest;-><init>(Lcom/google/android/apps/plus/content/MediaTypeDetector;Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;Landroid/net/Uri;)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mWaitingForConnection:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mQueue:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public final declared-synchronized onConnected()V
    .locals 5

    monitor-enter p0

    :try_start_0
    const-string v1, "PanoramaDetector"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PanoramaDetector"

    const-string v2, "Connected to GooglePlayServices"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mQueue:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mQueue:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mQueue:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mWaitingForConnection:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized onConnectionFailed$5d4cef71()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mClient:Lcom/google/android/gms/panorama/PanoramaClient;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mWaitingForConnection:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mQueue:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onDisconnected()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mClient:Lcom/google/android/gms/panorama/PanoramaClient;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mWaitingForConnection:Z

    return-void
.end method

.method public final declared-synchronized start()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/google/android/apps/plus/content/MediaTypeDetector$1;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/MediaTypeDetector;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/content/MediaTypeDetector$1;-><init>(Lcom/google/android/apps/plus/content/MediaTypeDetector;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/content/MediaTypeDetector;->mHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/MediaTypeDetector;->connect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
