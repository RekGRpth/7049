.class public final Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "AndroidContactSearchLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader$PhoneNumber;
    }
.end annotation


# static fields
.field private static final EMAIL_PROJECTION:[Ljava/lang/String;

.field private static final PHONE_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mIncludePhoneNumbers:Z

.field private final mMinQueryLength:I

.field private final mProjection:[Ljava/lang/String;

.field private final mQuery:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "lookup"

    aput-object v1, v0, v2

    const-string v1, "display_name"

    aput-object v1, v0, v3

    const-string v1, "data1"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->EMAIL_PROJECTION:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "lookup"

    aput-object v1, v0, v2

    const-string v1, "display_name"

    aput-object v1, v0, v3

    const-string v1, "data1"

    aput-object v1, v0, v4

    const-string v1, "data2"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const-string v2, "data3"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->PHONE_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mProjection:[Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mQuery:Ljava/lang/String;

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mMinQueryLength:I

    iput-boolean p5, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mIncludePhoneNumbers:Z

    return-void
.end method

.method private addPhoneNumberRows(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/phone/EsMatrixCursor;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader$PhoneNumber;",
            ">;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-virtual {p2, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_7

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader$PhoneNumber;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mProjection:[Ljava/lang/String;

    array-length v3, v3

    new-array v4, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    :goto_2
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mProjection:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_6

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mProjection:[Ljava/lang/String;

    aget-object v5, v5, v3

    const-string v6, "person_id"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "p:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v2, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader$PhoneNumber;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    :cond_1
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    const-string v6, "lookup_key"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v5, v2, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader$PhoneNumber;->lookupKey:Ljava/lang/String;

    aput-object v5, v4, v3

    goto :goto_3

    :cond_3
    const-string v6, "name"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v5, v2, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader$PhoneNumber;->name:Ljava/lang/String;

    aput-object v5, v4, v3

    goto :goto_3

    :cond_4
    const-string v6, "phone"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v5, v2, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader$PhoneNumber;->phoneNumber:Ljava/lang/String;

    aput-object v5, v4, v3

    goto :goto_3

    :cond_5
    const-string v6, "phone_type"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, v2, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader$PhoneNumber;->phoneType:Ljava/lang/String;

    aput-object v5, v4, v3

    goto :goto_3

    :cond_6
    invoke-virtual {p1, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    invoke-virtual {p2, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private buildEmailRow(Landroid/database/Cursor;)[Ljava/lang/Object;
    .locals 6
    .param p1    # Landroid/database/Cursor;

    const/4 v4, 0x2

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mProjection:[Ljava/lang/String;

    array-length v4, v4

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mProjection:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mProjection:[Ljava/lang/String;

    aget-object v1, v4, v2

    const-string v4, "person_id"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "e:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const-string v4, "lookup_key"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    goto :goto_1

    :cond_2
    const-string v4, "name"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    goto :goto_1

    :cond_3
    const-string v4, "email"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    aput-object v0, v3, v2

    goto :goto_1

    :cond_4
    return-object v3
.end method

.method private findEmailAddresses()Landroid/database/Cursor;
    .locals 8

    const/4 v3, 0x0

    new-instance v7, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mProjection:[Ljava/lang/String;

    invoke-direct {v7, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    sget-object v0, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_FILTER_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mQuery:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->EMAIL_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->buildEmailRow(Landroid/database/Cursor;)[Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method private findEmailAddressesAndPhoneNumbers()Landroid/database/Cursor;
    .locals 20

    new-instance v19, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mProjection:[Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v0, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    sget-object v4, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_FILTER_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mQuery:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->PHONE_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x2

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x0

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    new-instance v13, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader$PhoneNumber;

    const/4 v4, 0x0

    invoke-direct {v13, v4}, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader$PhoneNumber;-><init>(B)V

    iput-object v11, v13, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader$PhoneNumber;->lookupKey:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v13, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader$PhoneNumber;->name:Ljava/lang/String;

    move-object/from16 v0, v16

    iput-object v0, v13, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader$PhoneNumber;->phoneNumber:Ljava/lang/String;

    const/4 v4, 0x3

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x4

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-static {v0, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v17

    if-eqz v17, :cond_1

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v13, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader$PhoneNumber;->phoneType:Ljava/lang/String;

    :cond_1
    invoke-virtual {v14, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/ArrayList;

    if-nez v15, :cond_2

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v14, v11, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v15, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    sget-object v4, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_FILTER_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mQuery:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->EMAIL_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    const/4 v8, 0x0

    :cond_4
    :goto_1
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x0

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v14, v8}, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->addPhoneNumberRows(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;Ljava/lang/String;)V

    move-object v8, v11

    :cond_5
    const/4 v4, 0x2

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->buildEmailRow(Landroid/database/Cursor;)[Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v4

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_6
    if-eqz v8, :cond_7

    :try_start_2
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v14, v8}, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->addPhoneNumberRows(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_7
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    const/4 v10, 0x0

    :goto_2
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v10, v4, :cond_8

    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v14, v4}, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->addPhoneNumberRows(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;Ljava/lang/String;)V

    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    :cond_8
    return-object v19
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mQuery:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mMinQueryLength:I

    if-ge v0, v1, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mProjection:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->mIncludePhoneNumbers:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->findEmailAddressesAndPhoneNumbers()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;->findEmailAddresses()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method
