.class final Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "AudienceFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/AudienceFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SuggestedPeopleAdpater"
.end annotation


# instance fields
.field final mLayoutInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/AudienceFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/AudienceFragment;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;->this$0:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;->mLayoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const/4 v2, 0x1

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->setPersonId(Ljava/lang/String;)V

    const/4 v3, 0x2

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, " .*"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->setParticipantName(Ljava/lang/String;)V

    const/4 v3, 0x3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_0

    :goto_0
    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SuggestedPeopleListItemView;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;->mLayoutInflater:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/apps/plus/R$layout;->suggested_people_list_item_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onItemClick(I)V
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    const/4 v6, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v5, "g:"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    new-instance v1, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v1, v3, v4, v2}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;->this$0:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/ParticipantUtils;->getParticipantIdFromPerson(Lcom/google/android/apps/plus/content/PersonData;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/google/android/apps/plus/fragments/AudienceFragment;->isInAudience(Ljava/lang/String;)Z
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->access$400(Lcom/google/android/apps/plus/fragments/AudienceFragment;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;->this$0:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/views/AudienceView;->removePerson(Lcom/google/android/apps/plus/content/PersonData;)V

    :goto_1
    return-void

    :cond_0
    const-string v5, "e:"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    move-object v7, v3

    move-object v3, v2

    move-object v2, v7

    goto :goto_0

    :cond_1
    const-string v5, "p:"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    move-object v7, v3

    move-object v3, v2

    move-object v2, v7

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;->this$0:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/views/AudienceView;->addPerson(Lcom/google/android/apps/plus/content/PersonData;)V

    goto :goto_1

    :cond_3
    move-object v3, v2

    goto :goto_0
.end method
