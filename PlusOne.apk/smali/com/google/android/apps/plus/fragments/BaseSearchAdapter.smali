.class public abstract Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;
.super Lcom/android/common/widget/EsCompositeCursorAdapter;
.source "BaseSearchAdapter.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/BaseSearchAdapter$SearchStatusHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/common/widget/EsCompositeCursorAdapter;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/google/android/apps/plus/fragments/SearchLoaderResults;",
        ">;"
    }
.end annotation


# instance fields
.field protected final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mError:Z

.field private final mHandler:Landroid/os/Handler;

.field private final mLoaderManager:Landroid/support/v4/app/LoaderManager;

.field private mLoading:Z

.field private mNotFound:Z

.field protected mQuery:Ljava/lang/String;

.field private mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

.field private mResultsPreserved:Z

.field private final mSearchLoaderId:I

.field private mShowProgressWhenEmpty:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/support/v4/app/FragmentManager;
    .param p3    # Landroid/support/v4/app/LoaderManager;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p5    # I

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1, v5}, Lcom/android/common/widget/EsCompositeCursorAdapter;-><init>(Landroid/content/Context;B)V

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mShowProgressWhenEmpty:Z

    new-instance v3, Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->getQueryProjection()[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/apps/plus/fragments/SearchResults;-><init>([Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    new-instance v3, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter$SearchStatusHandler;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter$SearchStatusHandler;-><init>(Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x2

    if-ge v0, v3, :cond_0

    invoke-virtual {p0, v5, v5}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->addPartition(ZZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    mul-int/lit8 v3, p5, 0xa

    add-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mSearchLoaderId:I

    const-string v3, "search_results_fragment"

    invoke-virtual {p2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/fragments/SearchResults$SearchResultsFragment;

    if-nez v2, :cond_2

    new-instance v2, Lcom/google/android/apps/plus/fragments/SearchResults$SearchResultsFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/SearchResults$SearchResultsFragment;-><init>()V

    invoke-virtual {p2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    const-string v4, "search_results_fragment"

    invoke-virtual {v3, v2, v4}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/SearchResults$SearchResultsFragment;->setSearchResults(Lcom/google/android/apps/plus/fragments/SearchResults;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-void

    :cond_2
    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/SearchResults$SearchResultsFragment;->getSearchResults()Lcom/google/android/apps/plus/fragments/SearchResults;

    move-result-object v1

    if-eqz v1, :cond_1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/SearchResults;->getQuery()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mQuery:Ljava/lang/String;

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResultsPreserved:Z

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mLoading:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->updateSearchStatus()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;)V
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mSearchLoaderId:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->getContinuationToken()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/SearchResults;->getContinuationToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mLoading:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mSearchLoaderId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->updateSearchStatus()V

    :cond_0
    return-void
.end method

.method private updateSearchStatus()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->getMinQueryLength()I

    move-result v2

    if-lt v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mError:Z

    if-eqz v1, :cond_2

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->showEmptySearchResults()V

    :cond_1
    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->changeCursor(ILandroid/database/Cursor;)V

    return-void

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mNotFound:Z

    if-eqz v1, :cond_3

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mLoading:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mShowProgressWhenEmpty:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/SearchResults;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    :cond_4
    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract bindSearchItemView$2e05ad49(Landroid/view/View;Landroid/database/Cursor;)V
.end method

.method protected abstract bindSearchMessageView(Landroid/view/View;I)V
.end method

.method protected final bindView(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, p1, p3}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->bindSearchItemView$2e05ad49(Landroid/view/View;Landroid/database/Cursor;)V

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x4

    if-lt p4, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SearchResults;->hasMoreResults()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter$1;-><init>(Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->bindSearchMessageView(Landroid/view/View;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected abstract createSearchLoader(Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/BaseSearchLoader;
.end method

.method protected final getItemViewType(II)I
    .locals 0
    .param p1    # I
    .param p2    # I

    return p1
.end method

.method public final getItemViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method protected getMinQueryLength()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method protected abstract getQueryProjection()[Ljava/lang/String;
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method protected abstract newSearchItemView$3bea8f80(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method protected abstract newSearchMessageView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method protected final newView(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    packed-switch p2, :pswitch_data_0

    invoke-virtual {p0, p1, p5}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->newSearchItemView$3bea8f80(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-virtual {p0, p1, p5}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->newSearchMessageView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v0, "search_list_adapter.query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mQuery:Ljava/lang/String;

    const-string v0, "search_list_adapter.results"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResultsPreserved:Z

    if-nez v0, :cond_0

    const-string v0, "search_list_adapter.results"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/SearchResults;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    :cond_0
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/fragments/SearchLoaderResults;",
            ">;"
        }
    .end annotation

    iget v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mSearchLoaderId:I

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SearchResults;->getContinuationToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->createSearchLoader(Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/BaseSearchLoader;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 5
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    check-cast p2, Lcom/google/android/apps/plus/fragments/SearchLoaderResults;

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    iget v3, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mSearchLoaderId:I

    if-ne v0, v3, :cond_2

    sget-object v0, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->ABORTED:Lcom/google/android/apps/plus/fragments/SearchLoaderResults;

    if-eq p2, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    if-nez p2, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mError:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mError:Z

    if-eqz v0, :cond_4

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mLoading:Z

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SearchResults;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mHandler:Landroid/os/Handler;

    const-wide/16 v3, 0x1f4

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->updateSearchStatus()V

    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Lcom/google/android/apps/plus/fragments/SearchLoaderResults;->getContinuationToken()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/SearchResults;->getContinuationToken()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SearchResults;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SearchResults;->hasMoreResults()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_5
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mLoading:Z

    invoke-virtual {p2}, Lcom/google/android/apps/plus/fragments/SearchLoaderResults;->getNextContinuationToken()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/fragments/SearchResults;->setContinuationToken(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/fragments/SearchLoaderResults;->getCursor()Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/SearchResults;->addResults(Lcom/google/android/apps/plus/phone/EsMatrixCursor;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SearchResults;->getCount()I

    move-result v0

    if-nez v0, :cond_6

    :goto_3
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mNotFound:Z

    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_3

    :cond_7
    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->changeCursor(ILandroid/database/Cursor;)V

    goto :goto_2
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/fragments/SearchLoaderResults;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "search_list_adapter.query"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SearchResults;->isParcelable()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "search_list_adapter.results"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "query"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mSearchLoaderId:I

    invoke-virtual {v1, v2, v0, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->updateSearchStatus()V

    return-void
.end method

.method public final onStop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public final setQueryString(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/fragments/SearchResults;->setQueryString(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mSearchLoaderId:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->clearPartitions()V

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "query"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mError:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mNotFound:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mLoading:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mSearchLoaderId:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mSearchLoaderId:I

    invoke-virtual {v1, v2, v0, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->updateSearchStatus()V

    goto :goto_0
.end method

.method protected final showEmptySearchResults()V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/SearchResults;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/plus/fragments/BaseSearchAdapter;->changeCursor(ILandroid/database/Cursor;)V

    :cond_0
    return-void
.end method
