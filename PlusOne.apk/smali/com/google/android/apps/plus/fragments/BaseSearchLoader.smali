.class public abstract Lcom/google/android/apps/plus/fragments/BaseSearchLoader;
.super Landroid/support/v4/content/AsyncTaskLoader;
.source "BaseSearchLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/content/AsyncTaskLoader",
        "<",
        "Lcom/google/android/apps/plus/fragments/SearchLoaderResults;",
        ">;"
    }
.end annotation


# static fields
.field public static final ABORTED:Lcom/google/android/apps/plus/fragments/SearchLoaderResults;


# instance fields
.field protected final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field protected final mContinuationToken:Ljava/lang/String;

.field protected mData:Lcom/google/android/apps/plus/fragments/SearchLoaderResults;

.field protected volatile mOperation:Lcom/google/android/apps/plus/network/HttpOperation;

.field protected final mQuery:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/SearchLoaderResults;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/SearchLoaderResults;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->ABORTED:Lcom/google/android/apps/plus/fragments/SearchLoaderResults;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/support/v4/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->mQuery:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->mContinuationToken:Ljava/lang/String;

    return-void
.end method

.method private abort()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->mOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->abort()V

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->mOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    return-void
.end method


# virtual methods
.method public final cancelLoad()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->abort()V

    invoke-super {p0}, Landroid/support/v4/content/AsyncTaskLoader;->cancelLoad()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/plus/fragments/SearchLoaderResults;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->isReset()Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->mData:Lcom/google/android/apps/plus/fragments/SearchLoaderResults;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/support/v4/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final getContinuationToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->mContinuationToken:Ljava/lang/String;

    return-object v0
.end method

.method public final onAbandon()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->abort()V

    return-void
.end method

.method protected final onStartLoading()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->mData:Lcom/google/android/apps/plus/fragments/SearchLoaderResults;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;->forceLoad()V

    :cond_0
    return-void
.end method
