.class public final Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;
.super Ljava/lang/Object;
.source "BaseStreamSettingsFragment.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Settings"
.end annotation


# instance fields
.field public canSubscribe:Z

.field public isSubscribed:Z

.field public shouldShowPosts:Z

.field public volume:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->shouldShowPosts:Z

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->volume:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->canSubscribe:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->isSubscribed:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    iget-boolean v0, p1, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->shouldShowPosts:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->shouldShowPosts:Z

    iget v0, p1, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->volume:I

    iput v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->volume:I

    iget-boolean v0, p1, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->canSubscribe:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->canSubscribe:Z

    iget-boolean v0, p1, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->isSubscribed:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->isSubscribed:Z

    :cond_0
    return-void
.end method

.method public constructor <init>(ZIZ)V
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # Z

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;-><init>(ZIZZ)V

    return-void
.end method

.method public constructor <init>(ZIZZ)V
    .locals 0
    .param p1    # Z
    .param p2    # I
    .param p3    # Z
    .param p4    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->shouldShowPosts:Z

    iput p2, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->volume:I

    iput-boolean p3, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->canSubscribe:Z

    iput-boolean p4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->isSubscribed:Z

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    instance-of v2, p1, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    if-eqz v2, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->shouldShowPosts:Z

    iget-boolean v3, v0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->shouldShowPosts:Z

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->volume:I

    iget v3, v0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->volume:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->isSubscribed:Z

    iget-boolean v3, v0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->isSubscribed:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public final hashCode()I
    .locals 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->shouldShowPosts:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->volume:I

    add-int v0, v1, v4

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->isSubscribed:Z

    if-eqz v4, :cond_1

    :goto_1
    add-int v0, v1, v2

    return v0

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method
