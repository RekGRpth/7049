.class public abstract Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;
.super Landroid/support/v4/app/Fragment;
.source "BaseStreamSettingsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;
    }
.end annotation


# instance fields
.field protected mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mOriginalSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

.field private final mProfileEditServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mRequestId:Ljava/lang/Integer;

.field private mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

.field protected mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

.field private mShowPostsCheckBox:Landroid/widget/CheckBox;

.field private mShowPostsSection:Landroid/view/View;

.field private mSubscribedCheckBox:Landroid/widget/CheckBox;

.field private mSubscribedIcon:Landroid/widget/ImageView;

.field private mSubscriptionSection:Landroid/view/View;

.field private mVelocityDivider:Landroid/view/View;

.field private mVelocityItem:Landroid/view/View;

.field protected mVelocityOptions:[Ljava/lang/String;

.field private mVelocityTextView:Landroid/widget/TextView;

.field protected mVelocityValues:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mProfileEditServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;
    .param p1    # Ljava/lang/Integer;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;Lcom/google/android/apps/plus/service/ServiceResult;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->showErrorToast(Lcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mShowPostsCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->updateSubscribedItem()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSubscribedCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private onCancel()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageTextButton;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$string;->app_name:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->profile_edit_items_exit_unsaved:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->yes:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->no:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {v0, p0, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "quit"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->finishActivity(I)V

    goto :goto_0
.end method

.method private showErrorToast(Lcom/google/android/apps/plus/service/ServiceResult;)Z
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v4

    if-eqz v4, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    sget v4, Lcom/google/android/apps/plus/R$string;->profile_edit_update_error:I

    new-array v5, v3, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    move v2, v3

    :cond_1
    return v2

    :cond_2
    sget v2, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private updateSaveButton()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mOriginalSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateSubscribedItem()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSubscribedIcon:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->isSubscribed:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$drawable;->icn_notification_enabled:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSubscribedIcon:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->isSubscribed:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$string;->circle_settings_notifications_enabled:I

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->updateSaveButton()V

    return-void

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$drawable;->icn_notification_disabled:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$string;->circle_settings_notifications_disabled:I

    goto :goto_1
.end method


# virtual methods
.method protected abstract createBaseExtrasForLogging()Landroid/os/Bundle;
.end method

.method protected final dismissProgressDialog()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method protected final finishActivity(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    return-void
.end method

.method protected abstract getVolumeControTypeForLogging()I
.end method

.method protected abstract getVolumeSettingId()Ljava/lang/String;
.end method

.method protected abstract getVolumeSettingType()Ljava/lang/String;
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    sget v0, Lcom/google/android/apps/plus/R$id;->cancel:I

    if-ne v6, v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->onCancel()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$id;->save:I

    if-ne v6, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->shouldShowPosts:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget v4, v0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->volume:I

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getVolumeSettingType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getVolumeSettingId()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-boolean v5, v5, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->isSubscribed:Z

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->setVolumeControl(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;IZ)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->createBaseExtrasForLogging()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getVolumeControTypeForLogging()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mOriginalSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget v2, v2, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->volume:I

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mOriginalSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-boolean v3, v3, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->isSubscribed:Z

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget v4, v4, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->volume:I

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-boolean v5, v5, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->isSubscribed:Z

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createVolumeChange(IIZIZ)Lcom/google/api/services/plusi/model/VolumeChange;

    move-result-object v1

    const-string v2, "extra_notification_volume_change"

    invoke-static {}, Lcom/google/api/services/plusi/model/VolumeChangeJson;->getInstance()Lcom/google/api/services/plusi/model/VolumeChangeJson;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/api/services/plusi/model/VolumeChangeJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->VOLUME_CHANGED_VIA_COMMON_CONTROL:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzViews;->HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    sget v0, Lcom/google/android/apps/plus/R$string;->circle_settings_saving:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->showProgressDialog(I)V

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v1, "settings"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mRequestId:Ljava/lang/Integer;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v1, "settings"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "settings"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mOriginalSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    if-nez v1, :cond_1

    new-instance v1, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mOriginalSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;-><init>(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    :cond_1
    return-void

    :cond_2
    new-instance v1, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mOriginalSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v6, 0x0

    sget v4, Lcom/google/android/apps/plus/R$layout;->circle_settings:I

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    sget v4, Lcom/google/android/apps/plus/R$id;->cancel:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ImageTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v4, Lcom/google/android/apps/plus/R$id;->save:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/ImageTextButton;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v4, p0}, Lcom/google/android/apps/plus/views/ImageTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v4, v6}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    sget v4, Lcom/google/android/apps/plus/R$id;->show_posts:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mShowPostsCheckBox:Landroid/widget/CheckBox;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mShowPostsCheckBox:Landroid/widget/CheckBox;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-boolean v5, v5, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->shouldShowPosts:Z

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mShowPostsCheckBox:Landroid/widget/CheckBox;

    new-instance v5, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$2;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)V

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    sget v4, Lcom/google/android/apps/plus/R$id;->show_posts_section:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mShowPostsSection:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mShowPostsSection:Landroid/view/View;

    new-instance v5, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$3;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v4, Lcom/google/android/apps/plus/R$id;->amount_item:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mVelocityItem:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mVelocityItem:Landroid/view/View;

    new-instance v5, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$4;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v4, Lcom/google/android/apps/plus/R$id;->amount_value:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mVelocityTextView:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/plus/R$id;->amount_divider:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mVelocityDivider:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->updateVelocityItem()V

    sget v4, Lcom/google/android/apps/plus/R$id;->subscribed:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSubscribedCheckBox:Landroid/widget/CheckBox;

    sget v4, Lcom/google/android/apps/plus/R$id;->subscribed_icon:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSubscribedIcon:Landroid/widget/ImageView;

    sget v4, Lcom/google/android/apps/plus/R$id;->subscription_section:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSubscriptionSection:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-boolean v4, v4, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->canSubscribe:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSubscribedCheckBox:Landroid/widget/CheckBox;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-boolean v5, v5, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->isSubscribed:Z

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSubscribedCheckBox:Landroid/widget/CheckBox;

    new-instance v5, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$5;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)V

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSubscriptionSection:Landroid/view/View;

    new-instance v5, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$6;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$6;-><init>(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->updateSubscribedItem()V

    :goto_0
    return-object v1

    :cond_0
    sget v4, Lcom/google/android/apps/plus/R$id;->subscribe_label:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$color;->stream_circle_disabled_text:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSubscribedCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v4, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSubscribedCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v4, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSubscribedIcon:Landroid/widget/ImageView;

    sget v5, Lcom/google/android/apps/plus/R$drawable;->icn_notification_disabled:I

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSubscribedIcon:Landroid/widget/ImageView;

    sget v5, Lcom/google/android/apps/plus/R$string;->circle_settings_notifications_disabled:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v4, Lcom/google/android/apps/plus/R$id;->subscription_disabled:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    const-string v0, "velocity"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mVelocityValues:[I

    aget v1, v1, p1

    iput v1, v0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->volume:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->updateVelocityItem()V

    :cond_0
    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const-string v0, "quit"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->finishActivity(I)V

    :cond_0
    return-void
.end method

.method public final onDiscard()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->onCancel()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mProfileEditServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mProfileEditServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->dismissProgressDialog()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->showErrorToast(Lcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->finishActivity(I)V

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "settings"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected final showProgressDialog(I)V
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method protected final updateVelocityItem()V
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mVelocityItem:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->shouldShowPosts:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mVelocityDivider:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-boolean v3, v3, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->shouldShowPosts:Z

    if-eqz v3, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->shouldShowPosts:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mVelocityTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mSettings:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    iget v0, v0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;->volume:I

    packed-switch v0, :pswitch_data_0

    sget v0, Lcom/google/android/apps/plus/R$string;->circle_settings_amount_none:I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->updateSaveButton()V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :pswitch_0
    sget v0, Lcom/google/android/apps/plus/R$string;->circle_settings_amount_fewer:I

    goto :goto_2

    :pswitch_1
    sget v0, Lcom/google/android/apps/plus/R$string;->circle_settings_amount_standard:I

    goto :goto_2

    :pswitch_2
    sget v0, Lcom/google/android/apps/plus/R$string;->circle_settings_amount_more:I

    goto :goto_2

    :pswitch_3
    sget v0, Lcom/google/android/apps/plus/R$string;->circle_settings_amount_all:I

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
