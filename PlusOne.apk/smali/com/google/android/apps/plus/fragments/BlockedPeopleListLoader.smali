.class public final Lcom/google/android/apps/plus/fragments/BlockedPeopleListLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "BlockedPeopleListLoader.java"


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mIncludedPersonIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private final mProjection:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "[",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/BlockedPeopleListLoader;->setUri(Landroid/net/Uri;)V

    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/BlockedPeopleListLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/BlockedPeopleListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/BlockedPeopleListLoader;->mProjection:[Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/BlockedPeopleListLoader;->mIncludedPersonIds:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BlockedPeopleListLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/BlockedPeopleListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BlockedPeopleListLoader;->mProjection:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BlockedPeopleListLoader;->mIncludedPersonIds:Ljava/util/ArrayList;

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->getBlockedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/util/ArrayList;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BlockedPeopleListLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    return-object v0
.end method
