.class public Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "ChoosePhotoDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;
    }
.end annotation


# instance fields
.field private mCoverPhotoId:Ljava/lang/Long;

.field private mHasScrapbook:Z

.field private mIndexToAction:[I

.field private mIsCameraSupported:Z

.field private mIsForCoverPhoto:Z

.field private mIsOnlyFromInstantUpload:Z

.field private mTitle:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIndexToAction:[I

    sget v0, Lcom/google/android/apps/plus/R$string;->menu_choose_photo_from_gallery:I

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mTitle:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIndexToAction:[I

    sget v0, Lcom/google/android/apps/plus/R$string;->menu_choose_photo_from_gallery:I

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mTitle:I

    iput p1, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mTitle:I

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->dismiss()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v1, v2

    check-cast v1, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIndexToAction:[I

    aget v0, v3, p2

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-interface {v1}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;->doRepositionCoverPhoto()V

    goto :goto_0

    :pswitch_1
    invoke-interface {v1}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;->doTakePhoto()V

    goto :goto_0

    :pswitch_2
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;->doPickPhotoFromAlbums(I)V

    goto :goto_0

    :pswitch_3
    const/4 v3, 0x1

    invoke-interface {v1, v3}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;->doPickPhotoFromAlbums(I)V

    goto :goto_0

    :pswitch_4
    const/4 v3, 0x2

    invoke-interface {v1, v3}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;->doPickPhotoFromAlbums(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    if-eqz p1, :cond_0

    const-string v6, "is_camera_supported"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsCameraSupported:Z

    const-string v6, "title"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mTitle:I

    const-string v6, "only_instant_upload"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsOnlyFromInstantUpload:Z

    const-string v6, "has_scrapbook"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsForCoverPhoto:Z

    const-string v6, "has_scrapbook"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mHasScrapbook:Z

    const-string v6, "cover_photo_id"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "cover_photo_id"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mCoverPhotoId:Ljava/lang/Long;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsCameraSupported:Z

    if-eqz v6, :cond_7

    move v6, v7

    :goto_0
    add-int/lit8 v5, v6, 0x1

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsForCoverPhoto:Z

    if-eqz v6, :cond_2

    add-int/lit8 v5, v5, 0x1

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mHasScrapbook:Z

    if-eqz v6, :cond_1

    add-int/lit8 v5, v5, 0x1

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mCoverPhotoId:Ljava/lang/Long;

    if-eqz v6, :cond_2

    add-int/lit8 v5, v5, 0x1

    :cond_2
    new-array v4, v5, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mCoverPhotoId:Ljava/lang/Long;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIndexToAction:[I

    aput v7, v6, v8

    add-int/lit8 v2, v2, 0x1

    sget v6, Lcom/google/android/apps/plus/R$string;->change_photo_option_reposition:I

    invoke-virtual {v0, v6}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v8

    :cond_3
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsForCoverPhoto:Z

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIndexToAction:[I

    const/4 v8, 0x4

    aput v8, v6, v2

    add-int/lit8 v3, v2, 0x1

    sget v6, Lcom/google/android/apps/plus/R$string;->change_photo_option_select_gallery:I

    invoke-virtual {v0, v6}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v2

    move v2, v3

    :cond_4
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsCameraSupported:Z

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIndexToAction:[I

    const/4 v8, 0x2

    aput v8, v6, v2

    add-int/lit8 v3, v2, 0x1

    sget v6, Lcom/google/android/apps/plus/R$string;->change_photo_option_take_photo:I

    invoke-virtual {v0, v6}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v2

    move v2, v3

    :cond_5
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIndexToAction:[I

    const/4 v8, 0x3

    aput v8, v6, v2

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsOnlyFromInstantUpload:Z

    if-eqz v6, :cond_8

    add-int/lit8 v3, v2, 0x1

    sget v6, Lcom/google/android/apps/plus/R$string;->change_photo_option_instant_upload:I

    invoke-virtual {v0, v6}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v2

    move v2, v3

    :goto_1
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsForCoverPhoto:Z

    if-eqz v6, :cond_6

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mHasScrapbook:Z

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIndexToAction:[I

    const/4 v8, 0x5

    aput v8, v6, v2

    sget v6, Lcom/google/android/apps/plus/R$string;->change_photo_option_select_cover_photo:I

    invoke-virtual {v0, v6}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v2

    :cond_6
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget v6, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mTitle:I

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    new-instance v6, Landroid/widget/ArrayAdapter;

    const v8, 0x1090011

    invoke-direct {v6, v0, v8, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v1, v6, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    return-object v6

    :cond_7
    move v6, v8

    goto/16 :goto_0

    :cond_8
    add-int/lit8 v3, v2, 0x1

    sget v6, Lcom/google/android/apps/plus/R$string;->change_photo_option_your_photos:I

    invoke-virtual {v0, v6}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v2

    move v2, v3

    goto :goto_1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "is_camera_supported"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsCameraSupported:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "title"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mTitle:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "only_instant_upload"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsOnlyFromInstantUpload:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "has_scrapbook"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsForCoverPhoto:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "has_scrapbook"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mHasScrapbook:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mCoverPhotoId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    const-string v0, "cover_photo_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mCoverPhotoId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_0
    return-void
.end method

.method public final setIsCameraSupported(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsCameraSupported:Z

    return-void
.end method

.method public final setIsForCoverPhoto(ZZLjava/lang/Long;)V
    .locals 1
    .param p1    # Z
    .param p2    # Z
    .param p3    # Ljava/lang/Long;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsForCoverPhoto:Z

    iput-boolean p2, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mHasScrapbook:Z

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mCoverPhotoId:Ljava/lang/Long;

    return-void
.end method

.method public final show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/support/v4/app/FragmentManager;
    .param p2    # Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsCameraSupported:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->mIsForCoverPhoto:Z

    if-eqz v0, :cond_2

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;->doPickPhotoFromAlbums(I)V

    goto :goto_0
.end method
