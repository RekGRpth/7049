.class public final Lcom/google/android/apps/plus/fragments/CircleListLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "CircleListLoader.java"


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mMaxResults:I

.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private final mProjection:[Ljava/lang/String;

.field private final mQuery:Ljava/lang/String;

.field private final mUsageType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # I
    .param p4    # [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # I
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CircleListLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/CircleListLoader;->setUri(Landroid/net/Uri;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/CircleListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/CircleListLoader;->mProjection:[Ljava/lang/String;

    iput p3, p0, Lcom/google/android/apps/plus/fragments/CircleListLoader;->mUsageType:I

    iput-object p5, p0, Lcom/google/android/apps/plus/fragments/CircleListLoader;->mQuery:Ljava/lang/String;

    iput p6, p0, Lcom/google/android/apps/plus/fragments/CircleListLoader;->mMaxResults:I

    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CircleListLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CircleListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/CircleListLoader;->mUsageType:I

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CircleListLoader;->mProjection:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CircleListLoader;->mQuery:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/apps/plus/fragments/CircleListLoader;->mMaxResults:I

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CircleListLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-interface {v6, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    return-object v6
.end method
