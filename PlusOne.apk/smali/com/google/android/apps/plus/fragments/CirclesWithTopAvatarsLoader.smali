.class public final Lcom/google/android/apps/plus/fragments/CirclesWithTopAvatarsLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "CirclesWithTopAvatarsLoader.java"


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mMaxTopCount:I

.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesWithTopAvatarsLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/CirclesWithTopAvatarsLoader;->setUri(Landroid/net/Uri;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/CirclesWithTopAvatarsLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/apps/plus/fragments/CirclesWithTopAvatarsLoader;->mMaxTopCount:I

    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesWithTopAvatarsLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesWithTopAvatarsLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/CirclesWithTopAvatarsLoader;->mMaxTopCount:I

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCirclesWithTopPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesWithTopAvatarsLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    return-object v0
.end method
