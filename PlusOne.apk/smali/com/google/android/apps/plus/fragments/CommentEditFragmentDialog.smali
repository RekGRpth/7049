.class public Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;
.super Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
.source "CommentEditFragmentDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog$CommentEditDialogListener;
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mActivityId:Ljava/lang/String;

.field private mInputTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->mInputTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method private checkPositiveButtonEnabled()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->mInputTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static newInstance(ILjava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;
    .locals 3
    .param p0    # I
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "comment_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "comment_text"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "title_id"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "activity_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "account"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v1, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v2, 0x0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    new-instance v1, Landroid/text/SpannableStringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->mInputTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    :cond_0
    :goto_1
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog$CommentEditDialogListener;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "comment_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog$CommentEditDialogListener;->onCommentEditComplete$42c1be52(Landroid/text/Spannable;)V

    :cond_2
    :goto_2
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->onClick(Landroid/content/DialogInterface;I)V

    return-void

    :cond_3
    const/4 v0, -0x2

    if-ne p2, v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog$CommentEditDialogListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog$CommentEditDialogListener;->onCommentEditCancelled()V

    goto :goto_2
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    sget v4, Lcom/google/android/apps/plus/R$layout;->comment_edit_dialog:I

    invoke-virtual {v2, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$id;->text:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->mInputTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    const-string v4, "activity_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->mActivityId:Ljava/lang/String;

    const-string v4, "account"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->mInputTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->mActivityId:Ljava/lang/String;

    invoke-virtual {v4, p0, v5, v6, v7}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->init(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/views/AudienceView;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->mInputTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v4, p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    if-eqz p1, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->mInputTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    const-string v5, "comment_text"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const-string v4, "title_id"

    sget v5, Lcom/google/android/apps/plus/R$string;->menu_edit_comment:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    sget v4, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {v1, v4, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v4, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {v1, v4, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->mInputTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    const-string v5, "comment_text"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHtml(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->mInputTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->mInputTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    new-instance v1, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog$1;-><init>(Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "comment_text"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->mInputTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final onStart()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->onStart()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->checkPositiveButtonEnabled()V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->checkPositiveButtonEnabled()V

    return-void
.end method
