.class public Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;
.super Landroid/support/v4/app/Fragment;
.source "ComposeMessageFragment.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;
    }
.end annotation


# instance fields
.field private mAllowSendImages:Z

.field private mAllowSendMessage:Z

.field private mCurrentTypingStatus:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

.field private mHandler:Landroid/os/Handler;

.field private mInsertCameraPhotoRequestId:Ljava/lang/Integer;

.field private mListener:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;

.field private mMessageText:Landroid/widget/EditText;

.field private mSendButton:Landroid/view/View;

.field private mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mTimeSinceLastTypingEvent:J

.field private mTypingTimeoutRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    new-instance v0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mTypingTimeoutRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;
    .param p1    # Ljava/lang/Integer;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->insertCameraPhoto(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;)V
    .locals 7
    .param p0    # Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    const-wide/16 v5, 0x1388

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mMessageText:Landroid/widget/EditText;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mCurrentTypingStatus:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->getNumber()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mCurrentTypingStatus:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    sget-object v3, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->START:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mTypingTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mTypingTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v3, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iput-wide v1, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mTimeSinceLastTypingEvent:J

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mMessageText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    :pswitch_0
    if-lez v0, :cond_0

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->START:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mCurrentTypingStatus:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mCurrentTypingStatus:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->dispatchTypingStatusChangedEvent(Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;)V

    goto :goto_1

    :pswitch_1
    if-nez v0, :cond_3

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->CLEAR:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mCurrentTypingStatus:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->START:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mCurrentTypingStatus:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mCurrentTypingStatus:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->dispatchTypingStatusChangedEvent(Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;)V

    goto :goto_1

    :pswitch_2
    if-nez v0, :cond_4

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->CLEAR:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mCurrentTypingStatus:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mCurrentTypingStatus:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->dispatchTypingStatusChangedEvent(Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;)V

    goto :goto_1

    :cond_4
    iget-wide v3, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mTimeSinceLastTypingEvent:J

    sub-long v3, v1, v3

    cmp-long v0, v3, v5

    if-lez v0, :cond_0

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->PAUSE:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mCurrentTypingStatus:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mCurrentTypingStatus:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->dispatchTypingStatusChangedEvent(Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->updateSendButtonState()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->dispatchSendMessageEvent()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;)V
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    new-instance v0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;

    sget v1, Lcom/google/android/apps/plus/R$string;->menu_photo_chooser:I

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;-><init>(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->isCameraIntentRegistered(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->setIsCameraSupported(Z)V

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "share_photo"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private dispatchSendMessageEvent()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mListener:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mMessageText:Landroid/widget/EditText;

    if-nez v1, :cond_2

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mListener:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;

    invoke-interface {v1, v0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;->onSendTextMessage(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mMessageText:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mMessageText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private dispatchSendPhotoEvent(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mListener:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mListener:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;->onSendPhoto(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method private dispatchTypingStatusChangedEvent(Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;)V
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mListener:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mListener:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;->onTypingStatusChanged(Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;)V

    :cond_0
    return-void
.end method

.method private getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method private insertCameraPhoto(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz p1, :cond_1

    const/4 v1, 0x2

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->dispatchSendPhotoEvent(Ljava/lang/String;I)V

    :goto_0
    instance-of v1, v0, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;->hideInsertCameraPhotoDialog()V

    :cond_0
    return-void

    :cond_1
    sget v1, Lcom/google/android/apps/plus/R$string;->camera_photo_error:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private updateSendButtonState()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mMessageText:Landroid/widget/EditText;

    if-nez v2, :cond_1

    const/4 v1, 0x0

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mAllowSendMessage:Z

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mSendButton:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mSendButton:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mSendButton:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mMessageText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final allowSendingImages(Z)V
    .locals 3
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mAllowSendImages:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$id;->photo_button:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final doPickPhotoFromAlbums(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->newAlbumsActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPersonId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotosHome(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setShowCameraAlbum(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->photo_picker_album_label_messenger:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerTitleResourceId(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v3}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final doRepositionCoverPhoto()V
    .locals 0

    return-void
.end method

.method public final doTakePhoto()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    const-string v1, "camera-p.jpg"

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->getCameraIntentPhoto$3a35108a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v2, -0x1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-ne p2, v2, :cond_0

    if-eqz p3, :cond_0

    const-string v2, "photo_url"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->dispatchSendPhotoEvent(Ljava/lang/String;I)V

    goto :goto_0

    :pswitch_1
    if-ne p2, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    if-eqz v2, :cond_1

    move-object v2, v0

    check-cast v2, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    invoke-interface {v2}, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;->showInsertCameraPhotoDialog()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    const-string v3, "camera-p.jpg"

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->insertCameraPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v5, 0x0

    if-eqz p3, :cond_0

    const-string v3, "insert_camera_photo_req_id"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "insert_camera_photo_req_id"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    :cond_0
    sget v3, Lcom/google/android/apps/plus/R$layout;->compose_message:I

    invoke-virtual {p1, v3, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$id;->send_button:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mSendButton:Landroid/view/View;

    sget-object v3, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->CLEAR:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mCurrentTypingStatus:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    sget v3, Lcom/google/android/apps/plus/R$id;->message_text:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mMessageText:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mMessageText:Landroid/widget/EditText;

    new-instance v4, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$3;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mMessageText:Landroid/widget/EditText;

    invoke-virtual {v3, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->send_button:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$4;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->photo_button:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mAllowSendImages:Z

    if-eqz v3, :cond_1

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    new-instance v3, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$5;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mAllowSendMessage:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->updateSendButtonState()V

    return-object v2

    :cond_1
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/widget/TextView;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mMessageText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->dispatchSendMessageEvent()V

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public final onResume()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->getLastCameraMediaLocation()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->insertCameraPhoto(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "insert_camera_photo_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public final requestFocus()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mMessageText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void
.end method

.method public final setAllowSendMessage(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mAllowSendMessage:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->updateSendButtonState()V

    return-void
.end method

.method public final setListener(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->mListener:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;

    return-void
.end method
