.class public final Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "DesktopActivityIdLoader.java"


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mConnectionError:Z

.field private final mDesktopActivityId:Ljava/lang/String;

.field private final mOwnerGaiaId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mDesktopActivityId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mOwnerGaiaId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 13

    const/4 v12, 0x6

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v5, 0x0

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mDesktopActivityId:Ljava/lang/String;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-boolean v10, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mConnectionError:Z

    new-instance v0, Lcom/google/android/apps/plus/api/GetActivityOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mDesktopActivityId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mOwnerGaiaId:Ljava/lang/String;

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/GetActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetActivityOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetActivityOperation;->getException()Ljava/lang/Exception;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetActivityOperation;->getException()Ljava/lang/Exception;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->isConnectionError(Ljava/lang/Throwable;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mConnectionError:Z

    const-string v1, "DesktopActivityIdLoader"

    invoke-static {v1, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "DesktopActivityIdLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot resolve desktop activity ID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mDesktopActivityId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetActivityOperation;->getException()Ljava/lang/Exception;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    move-object v9, v5

    :goto_1
    return-object v9

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetActivityOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "DesktopActivityIdLoader"

    invoke-static {v1, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "DesktopActivityIdLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot resolve  desktop activity ID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mDesktopActivityId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetActivityOperation;->getErrorCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    new-instance v9, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    new-array v1, v11, [Ljava/lang/String;

    const-string v2, "activity_id"

    aput-object v2, v1, v10

    invoke-direct {v9, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    new-array v1, v11, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetActivityOperation;->getResponseUpdateId()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-virtual {v9, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final isConnectionError()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mConnectionError:Z

    return v0
.end method
