.class public Lcom/google/android/apps/plus/fragments/EditAudienceFragment;
.super Lcom/google/android/apps/plus/fragments/EsFragment;
.source "EditAudienceFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/plus/views/CheckableListItemView$OnItemCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;,
        Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/google/android/apps/plus/views/CheckableListItemView$OnItemCheckedChangeListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

.field private mAllowEmptyAudience:Z

.field private mAudienceChangedCallback:Ljava/lang/Runnable;

.field private mAudienceSet:Z

.field private mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

.field private final mCircleContentObserver:Landroid/database/DataSetObserver;

.field private mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private mCircleSelectionEnabled:Z

.field private mCircleUsageType:I

.field private mFilterNullGaiaIds:Z

.field private mIncludePlusPages:Z

.field private mIncomingAudienceIsReadOnly:Z

.field private mListView:Landroid/widget/ListView;

.field private mListener:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;

.field private mLoaderError:Z

.field private mLoadersInitialized:Z

.field private final mSelectedCircles:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;"
        }
    .end annotation
.end field

.field private final mSelectedPeople:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    new-instance v0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleContentObserver:Landroid/database/DataSetObserver;

    new-instance v0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceChangedCallback:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/fragments/CircleNameResolver;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mIncomingAudienceIsReadOnly:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/content/AudienceData;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAudienceFromList()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;Lcom/google/android/apps/plus/views/CircleListItemView;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/EditAudienceFragment;
    .param p1    # Lcom/google/android/apps/plus/views/CircleListItemView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->addToSelectedCircles(Lcom/google/android/apps/plus/views/CircleListItemView;)V

    return-void
.end method

.method private addToSelectedCircles(Lcom/google/android/apps/plus/views/CircleListItemView;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/views/CircleListItemView;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/CircleListItemView;->getCircleId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/CircleListItemView;->getCircleType()I

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/CircleListItemView;->getCircleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/CircleListItemView;->getMemberCount()I

    move-result v3

    new-instance v0, Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v5, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method private getAudienceFromList()Lcom/google/android/apps/plus/content/AudienceData;
    .locals 6

    new-instance v1, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/CircleData;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/PersonData;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    new-instance v5, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v5, v4, v1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v5
.end method

.method private isLoading()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleSelectionEnabled:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->isLoaded()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    return v0
.end method

.method private updateSelectionCount()V
    .locals 9

    const/4 v1, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;

    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ""

    :goto_0
    invoke-interface {v5, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;->onAudienceChanged(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAudienceFromList()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    :cond_1
    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleSelectionEnabled:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_8

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    move v2, v1

    move v3, v1

    :cond_3
    const/4 v7, 0x2

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v8, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v7, 0x3

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    packed-switch v7, :pswitch_data_0

    :pswitch_0
    add-int/lit8 v3, v3, 0x1

    :cond_4
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-nez v7, :cond_3

    :goto_2
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    add-int/2addr v3, v6

    if-nez v3, :cond_7

    if-eqz v2, :cond_5

    sget v0, Lcom/google/android/apps/plus/R$string;->audience_display_extended:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    move v2, v4

    goto :goto_1

    :pswitch_3
    move v0, v4

    goto :goto_1

    :cond_5
    if-eqz v0, :cond_6

    sget v0, Lcom/google/android/apps/plus/R$string;->audience_display_circles:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    sget v0, Lcom/google/android/apps/plus/R$string;->audience_selection_count_zero:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$plurals;->audience_display_selection_count:I

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v1

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final addSelectedCircle(Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/content/CircleData;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateSelectionCount()V

    return-void
.end method

.method public final addSelectedPerson(Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/content/PersonData;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mLoadersInitialized:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateSelectionCount()V

    return-void
.end method

.method public final getAudience()Lcom/google/android/apps/plus/content/AudienceData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    return-object v0
.end method

.method public final hasAudience()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceSet:Z

    return v0
.end method

.method protected final isEmpty()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->isLoading()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->isPartitionEmpty(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->isPartitionEmpty(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->isPartitionEmpty(I)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0
.end method

.method public final isSelectionValid()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAllowEmptyAudience:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1    # Landroid/app/Activity;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onAttach(Landroid/app/Activity;)V

    new-instance v0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;-><init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->addPartition(ZZ)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->addPartition(ZZ)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->addPartition(ZZ)V

    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleContentObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->registerObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleSelectionEnabled:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mLoadersInitialized:Z

    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 11
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleUsageType:I

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v4, v6

    const-string v5, "circle_name"

    aput-object v5, v4, v7

    const-string v5, "circle_id"

    aput-object v5, v4, v8

    const-string v5, "type"

    aput-object v5, v4, v9

    const-string v5, "contact_count"

    aput-object v5, v4, v10

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v3, v6

    const-string v4, "name"

    aput-object v4, v3, v7

    const-string v4, "person_id"

    aput-object v4, v3, v8

    const-string v4, "gaia_id"

    aput-object v4, v3, v9

    const-string v4, "avatar"

    aput-object v4, v3, v10

    const/4 v4, 0x5

    const-string v5, "packed_circle_ids"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mIncludePlusPages:Z

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mFilterNullGaiaIds:Z

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/PeopleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleNotInCirclesLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v3, v6

    const-string v4, "name"

    aput-object v4, v3, v7

    const-string v4, "person_id"

    aput-object v4, v3, v8

    const-string v4, "gaia_id"

    aput-object v4, v3, v9

    const-string v4, "avatar"

    aput-object v4, v3, v10

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mFilterNullGaiaIds:Z

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/PeopleNotInCirclesLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/util/HashMap;Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/16 v6, 0x8

    const/4 v5, 0x0

    sget v1, Lcom/google/android/apps/plus/R$layout;->edit_audience_fragment:I

    invoke-virtual {p1, v1, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v5, v4}, Lcom/google/android/apps/plus/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AudienceView;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceChangedCallback:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AudienceView;->setAudienceChangedCallback(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    sget v2, Lcom/google/android/apps/plus/R$id;->audience_to_text:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    sget v2, Lcom/google/android/apps/plus/R$id;->edit_audience:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-object v0
.end method

.method public final onItemCheckedChanged(Lcom/google/android/apps/plus/views/CheckableListItemView;Z)V
    .locals 11
    .param p1    # Lcom/google/android/apps/plus/views/CheckableListItemView;
    .param p2    # Z

    instance-of v9, p1, Lcom/google/android/apps/plus/views/CircleListItemView;

    if-eqz v9, :cond_3

    move-object v6, p1

    check-cast v6, Lcom/google/android/apps/plus/views/CircleListItemView;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/CircleListItemView;->getCircleId()Ljava/lang/String;

    move-result-object v3

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/CircleListItemView;->getCircleType()I

    move-result v9

    invoke-static {v0, v9}, Lcom/google/android/apps/plus/util/AccountsUtil;->isRestrictedCircleForAccount(Lcom/google/android/apps/plus/content/EsAccount;I)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasSeenMinorPublicExtendedDialog(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v9

    if-nez v9, :cond_1

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-direct {v2, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/CircleListItemView;->getCircleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    sget v9, Lcom/google/android/apps/plus/R$string;->dialog_public_or_extended_circle_for_minor:I

    invoke-virtual {v2, v9}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    sget v9, Lcom/google/android/apps/plus/R$string;->ok:I

    new-instance v10, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$3;

    invoke-direct {v10, p0, v6, v1, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;Lcom/google/android/apps/plus/views/CircleListItemView;Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v2, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v9, Lcom/google/android/apps/plus/R$string;->cancel:I

    new-instance v10, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$4;

    invoke-direct {v10, p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;Lcom/google/android/apps/plus/views/CheckableListItemView;)V

    invoke-virtual {v2, v9, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateSelectionCount()V

    return-void

    :cond_1
    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->addToSelectedCircles(Lcom/google/android/apps/plus/views/CircleListItemView;)V

    goto :goto_0

    :cond_2
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v9, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    instance-of v9, p1, Lcom/google/android/apps/plus/views/PeopleListItemView;

    if-eqz v9, :cond_0

    move-object v6, p1

    check-cast v6, Lcom/google/android/apps/plus/views/PeopleListItemView;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getPersonId()Ljava/lang/String;

    move-result-object v8

    if-eqz p2, :cond_6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getGaiaId()Ljava/lang/String;

    move-result-object v5

    const/4 v4, 0x0

    const-string v9, "e:"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    :cond_4
    :goto_1
    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContactName()Ljava/lang/String;

    move-result-object v7

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    new-instance v10, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v10, v5, v7, v4}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v8, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_5
    const-string v9, "p:"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    move-object v4, v8

    goto :goto_1

    :cond_6
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v9, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    instance-of v0, p2, Landroid/widget/Checkable;

    if-eqz v0, :cond_0

    check-cast p2, Landroid/widget/Checkable;

    invoke-interface {p2}, Landroid/widget/Checkable;->toggle()V

    :cond_0
    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 3
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    check-cast p2, Landroid/database/Cursor;

    if-nez p2, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mLoaderError:Z

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->changeCursor(ILandroid/database/Cursor;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateView(Landroid/view/View;)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->changeCursor(ILandroid/database/Cursor;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateView(Landroid/view/View;)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v0, v2, p2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->changeCursor(ILandroid/database/Cursor;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateView(Landroid/view/View;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "audience"

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAudienceFromList()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public final setAllowEmptyAudience(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAllowEmptyAudience:Z

    return-void
.end method

.method public final setAudience(Lcom/google/android/apps/plus/content/AudienceData;)V
    .locals 10
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceSet:Z

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->clear()V

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->clear()V

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v1, v0, v4

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_5

    aget-object v6, v0, v4

    const/4 v7, 0x0

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "g:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    :cond_1
    :goto_2
    if-eqz v7, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v8, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v2

    const-string v8, "p:"

    invoke-virtual {v2, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    move-object v7, v2

    goto :goto_2

    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "e:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_2

    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateSelectionCount()V

    return-void
.end method

.method public final setCircleSelectionEnabled(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleSelectionEnabled:Z

    return-void
.end method

.method public final setCircleUsageType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleUsageType:I

    return-void
.end method

.method public final setFilterNullGaiaIds(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mFilterNullGaiaIds:Z

    return-void
.end method

.method public final setIncludePlusPages(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mIncludePlusPages:Z

    return-void
.end method

.method public final setIncomingAudienceIsReadOnly(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mIncomingAudienceIsReadOnly:Z

    return-void
.end method

.method public final setOnSelectionChangeListener(Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;

    return-void
.end method

.method protected final updateView(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x0

    const/16 v3, 0x8

    const v2, 0x102000a

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$id;->server_error:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mLoaderError:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->showContent(Landroid/view/View;)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateSelectionCount()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->isLoading()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    sget v2, Lcom/google/android/apps/plus/R$string;->no_people_in_circles:I

    invoke-static {p1, v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setupEmptyView(Landroid/view/View;I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->showEmptyView(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->showContent(Landroid/view/View;)V

    goto :goto_0
.end method
