.class final Lcom/google/android/apps/plus/fragments/EditCommentFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "EditCommentFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/EditCommentFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/EditCommentFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/EditCommentFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEditComment$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/EditCommentFragment;->handleEditComment(Lcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->access$000(Lcom/google/android/apps/plus/fragments/EditCommentFragment;Lcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_0
    return-void
.end method

.method public final onEditPhotoCommentComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/EditCommentFragment;->handleEditComment(Lcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->access$000(Lcom/google/android/apps/plus/fragments/EditCommentFragment;Lcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_0
    return-void
.end method
