.class final Lcom/google/android/apps/plus/fragments/EditEventFragment$7;
.super Ljava/lang/Object;
.source "EditEventFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/EditEventFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$7;->this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$7;->this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditEventFragment;->mDescriptionView:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->access$1400(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$7;->this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->access$1100(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lcom/google/api/services/plusi/model/PlusEvent;

    move-result-object v1

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$7;->this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->access$1100(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lcom/google/api/services/plusi/model/PlusEvent;

    move-result-object v1

    iput-object v0, v1, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$7;->this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->access$1202(Lcom/google/android/apps/plus/fragments/EditEventFragment;Z)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$7;->this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->access$1300(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment$7;->this$0:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->access$1300(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    :cond_0
    return-void
.end method
