.class public Lcom/google/android/apps/plus/fragments/EditEventFragment;
.super Lcom/google/android/apps/plus/fragments/EsFragment;
.source "EditEventFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;
.implements Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;,
        Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;,
        Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;",
        "Landroid/widget/CompoundButton$OnCheckedChangeListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;",
        "Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;"
    }
.end annotation


# static fields
.field private static final EVENT_COLUMNS:[Ljava/lang/String;

.field private static final THEME_COLUMNS:[Ljava/lang/String;


# instance fields
.field private mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

.field private mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

.field private mAuthKey:Ljava/lang/String;

.field private mChanged:Z

.field private mCurrentSpinnerPosition:I

.field private mDescriptionView:Landroid/widget/EditText;

.field private mEndDateView:Landroid/widget/Button;

.field private mEndTimeView:Landroid/widget/Button;

.field private mError:Z

.field private mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

.field private mEventDescriptionTextWatcher:Landroid/text/TextWatcher;

.field private mEventId:Ljava/lang/String;

.field private mEventLoaded:Z

.field private mEventNameTextWatcher:Landroid/text/TextWatcher;

.field private mEventNameView:Landroid/widget/EditText;

.field private mEventThemeId:I

.field private mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

.field private mExternalId:Ljava/lang/String;

.field private mHangoutView:Landroid/widget/CheckBox;

.field private mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

.field private mLocationContainer:Landroid/view/View;

.field private mLocationView:Landroid/widget/TextView;

.field private mNewEvent:Z

.field private mOwnerId:Ljava/lang/String;

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mStartDateView:Landroid/widget/Button;

.field private mStartTimeView:Landroid/widget/Button;

.field private mThemeProgressBar:Landroid/widget/ProgressBar;

.field private mThemeSelectionButton:Landroid/view/View;

.field private mThemeSelectionTextView:Landroid/widget/TextView;

.field private mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

.field private mTimeZoneSpinner:Landroid/widget/Spinner;

.field private mTimeZoneSpinnerAdapter:Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "theme_id"

    aput-object v1, v0, v3

    const-string v1, "image_url"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "placeholder_path"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->THEME_COLUMNS:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "event_data"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->EVENT_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    new-instance v0, Lcom/google/android/apps/plus/fragments/EditEventFragment$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    new-instance v0, Lcom/google/android/apps/plus/fragments/EditEventFragment$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$6;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventNameTextWatcher:Landroid/text/TextWatcher;

    new-instance v0, Lcom/google/android/apps/plus/fragments/EditEventFragment$7;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$7;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventDescriptionTextWatcher:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->onAudienceChanged()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/EditEventFragment;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeId:I

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventNameView:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lcom/google/api/services/plusi/model/PlusEvent;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/android/apps/plus/fragments/EditEventFragment;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditEventFragment;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mChanged:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mDescriptionView:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$200()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->THEME_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAuthKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->EVENT_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lcom/google/android/apps/plus/views/EventThemeView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

    return-object v0
.end method

.method private bindEndDate()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndDateView:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getTimeZone(Lcom/google/api/services/plusi/model/EventTime;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/util/EventDateUtils;->getSingleDateDisplayLine(Landroid/content/Context;JLjava/util/TimeZone;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndDateView:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private bindEndTime()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndTimeView:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getTimeZone(Lcom/google/api/services/plusi/model/EventTime;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/util/EventDateUtils;->getDisplayTime(Landroid/content/Context;JLjava/util/TimeZone;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndTimeView:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private bindEvent()V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventNameView:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mDescriptionView:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mHangoutView:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventOptions;->hangout:Ljava/lang/Boolean;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindStartDate()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndDate()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindTimeZoneSpinner()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindStartTime()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndTime()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindLocation()V

    goto :goto_0

    :cond_2
    const/16 v0, 0x8

    goto :goto_1
.end method

.method private bindLocation()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v1, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mLocationView:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mLocationView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private bindStartDate()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mStartDateView:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getTimeZone(Lcom/google/api/services/plusi/model/EventTime;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/util/EventDateUtils;->getSingleDateDisplayLine(Landroid/content/Context;JLjava/util/TimeZone;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private bindStartTime()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mStartTimeView:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getTimeZone(Lcom/google/api/services/plusi/model/EventTime;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/util/EventDateUtils;->getDisplayTime(Landroid/content/Context;JLjava/util/TimeZone;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private bindTimeZoneSpinner()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->configure$622086bc(Ljava/util/Calendar;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneSpinnerAdapter:Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->setTimeZoneHelper(Lcom/google/android/apps/plus/util/TimeZoneHelper;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getTimeZonePos(Ljava/lang/String;Ljava/lang/Long;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mCurrentSpinnerPosition:I

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneSpinner:Landroid/widget/Spinner;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_0
    return-void
.end method

.method private clearEndTime()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    return-void
.end method

.method private enableEventPicker()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/EditEventFragment$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method private static getDefaultEventTime()J
    .locals 4

    const/16 v3, 0xc

    const/4 v2, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0x5a

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v0, v3, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    return-wide v1
.end method

.method private getTimeZone(Lcom/google/api/services/plusi/model/EventTime;)Ljava/util/TimeZone;
    .locals 4
    .param p1    # Lcom/google/api/services/plusi/model/EventTime;

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getTimeZone(Ljava/lang/String;Ljava/lang/Long;)Ljava/util/TimeZone;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getCurrentTimeZoneInfo()Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    goto :goto_0
.end method

.method private isEmptyAudience()Z
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v2

    add-int/2addr v1, v2

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onAudienceChanged()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    :cond_0
    return-void
.end method

.method private recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v1}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-static {v1, v0, p1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    :cond_0
    return-void
.end method

.method private setEndTime(Ljava/util/Calendar;)V
    .locals 6
    .param p1    # Ljava/util/Calendar;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    new-instance v4, Lcom/google/api/services/plusi/model/EventTime;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/EventTime;-><init>()V

    iput-object v4, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-static {}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getDefaultEventTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v3, v0

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-virtual {v2}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mChanged:Z

    :cond_1
    return-void
.end method

.method private setEventTheme(ILjava/lang/String;Landroid/net/Uri;Z)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/net/Uri;
    .param p4    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->themeSpecification:Lcom/google/api/services/plusi/model/ThemeSpecification;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    new-instance v1, Lcom/google/api/services/plusi/model/ThemeSpecification;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/ThemeSpecification;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/PlusEvent;->themeSpecification:Lcom/google/api/services/plusi/model/ThemeSpecification;

    :cond_2
    if-nez p4, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->themeSpecification:Lcom/google/api/services/plusi/model/ThemeSpecification;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ThemeSpecification;->themeId:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->themeSpecification:Lcom/google/api/services/plusi/model/ThemeSpecification;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ThemeSpecification;->themeId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_0

    :cond_3
    iput p1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeId:I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->themeSpecification:Lcom/google/api/services/plusi/model/ThemeSpecification;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ThemeSpecification;->themeId:Ljava/lang/Integer;

    if-eqz p3, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {p3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventThemeView;->setResourceDownloadingDrawablePath(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->enableEventPicker()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/EventThemeView;->setImageUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setStartTime(Ljava/util/Calendar;)V
    .locals 7
    .param p1    # Ljava/util/Calendar;

    const/4 v4, 0x1

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    if-eqz v5, :cond_2

    move v3, v4

    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v5, v5, v0

    if-nez v5, :cond_0

    if-nez v3, :cond_1

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v5, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-virtual {v2}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindTimeZoneSpinner()V

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mChanged:Z

    :cond_1
    return-void

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private updateView(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x0

    const/16 v3, 0x8

    if-eqz p1, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v2, Lcom/google/android/apps/plus/R$id;->server_error:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v2, :cond_2

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->showContent(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventLoaded:Z

    if-nez v2, :cond_3

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mError:Z

    if-eqz v2, :cond_4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    sget v2, Lcom/google/android/apps/plus/R$string;->event_details_error:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->showContent(Landroid/view/View;)V

    goto :goto_0

    :cond_4
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    sget v2, Lcom/google/android/apps/plus/R$string;->event_does_not_exist:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->showContent(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public final createEvent()V
    .locals 4

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/PlusEvent;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    new-instance v2, Lcom/google/api/services/plusi/model/EventOptions;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/EventOptions;-><init>()V

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/EventOptions;->openEventAcl:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/EventOptions;->openPhotoAcl:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    new-instance v2, Lcom/google/api/services/plusi/model/EventTime;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/EventTime;-><init>()V

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-static {}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getDefaultEventTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getCurrentTimeZoneInfo()Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x20

    invoke-static {v2}, Lcom/google/android/apps/plus/util/StringUtils;->randomString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mExternalId:Ljava/lang/String;

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeId:I

    :cond_0
    return-void
.end method

.method public final editEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mOwnerId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAuthKey:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeId:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    return-void
.end method

.method protected final handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq p1, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-eqz v1, :cond_3

    sget v1, Lcom/google/android/apps/plus/R$string;->create_event_server_error:I

    :goto_1
    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    sget v1, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-eqz v1, :cond_5

    sget v1, Lcom/google/android/apps/plus/R$string;->event_create_successful:I

    :goto_2
    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-interface {v1, v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;->onEventSaved(Lcom/google/api/services/plusi/model/PlusEvent;)V

    goto :goto_0

    :cond_5
    sget v1, Lcom/google/android/apps/plus/R$string;->event_save_successful:I

    goto :goto_2
.end method

.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v5, 0x0

    const/4 v4, -0x1

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragment;->onActivityResult(IILandroid/content/Intent;)V

    if-ne p2, v4, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v3, "location"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object v5, v3, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindLocation()V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {}, Lcom/google/api/services/plusi/model/PlaceJson;->getInstance()Lcom/google/api/services/plusi/model/PlaceJson;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/api/services/plusi/model/PlaceJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Place;

    iput-object v3, v4, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    goto :goto_1

    :pswitch_1
    const-string v3, "theme_id"

    invoke-virtual {p3, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "theme_url"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eq v2, v4, :cond_0

    if-eqz v1, :cond_0

    const/4 v3, 0x1

    invoke-direct {p0, v2, v1, v5, v3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setEventTheme(ILjava/lang/String;Landroid/net/Uri;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0

    :pswitch_2
    const-string v3, "audience"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final onAddPersonToCirclesAction$1165610b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onAttach(Landroid/app/Activity;)V

    new-instance v0, Lcom/google/android/apps/plus/util/TimeZoneHelper;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/util/TimeZoneHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->configure$622086bc(Ljava/util/Calendar;)V

    return-void
.end method

.method public final onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mHangoutView:Landroid/widget/CheckBox;

    if-ne p1, v1, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mLocationContainer:Landroid/view/View;

    if-nez p2, :cond_4

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-nez v1, :cond_1

    if-eqz p2, :cond_3

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    new-instance v2, Lcom/google/api/services/plusi/model/EventOptions;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/EventOptions;-><init>()V

    iput-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/EventOptions;->hangout:Ljava/lang/Boolean;

    :cond_3
    return-void

    :cond_4
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public final onCircleSelected(Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/content/CircleData;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->addCircle(Lcom/google/android/apps/plus/content/CircleData;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->clearText()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 13
    .param p1    # Landroid/view/View;

    const/4 v7, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v11

    sget v0, Lcom/google/android/apps/plus/R$id;->edit_audience:I

    if-ne v11, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHANGE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->event_invite_activity_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    const/16 v4, 0xb

    move v6, v5

    move v8, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$id;->start_date:I

    if-ne v11, v0, :cond_2

    new-instance v10, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;

    invoke-direct {v10, v7}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;-><init>(I)V

    invoke-virtual {v10, p0, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    const-string v0, "date_time"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v9, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "time_zone"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "date"

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/apps/plus/R$id;->end_date:I

    if-ne v11, v0, :cond_4

    new-instance v10, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;

    invoke-direct {v10, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;-><init>(I)V

    invoke-virtual {v10, p0, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v0, :cond_3

    const-string v0, "date_time"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v9, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :goto_1
    const-string v0, "time_zone"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "date"

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "date_time"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v9, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_1

    :cond_4
    sget v0, Lcom/google/android/apps/plus/R$id;->start_time:I

    if-ne v11, v0, :cond_5

    new-instance v10, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;

    invoke-direct {v10, v7}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;-><init>(I)V

    invoke-virtual {v10, p0, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    const-string v0, "date_time"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v9, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "time_zone"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "time"

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    sget v0, Lcom/google/android/apps/plus/R$id;->end_time:I

    if-ne v11, v0, :cond_7

    new-instance v10, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;

    invoke-direct {v10, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;-><init>(I)V

    invoke-virtual {v10, p0, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v0, :cond_6

    const-string v0, "date_time"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v9, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :goto_2
    const-string v0, "time_zone"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "time"

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    const-string v0, "date_time"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/32 v3, 0x6ddd00

    add-long/2addr v1, v3

    invoke-virtual {v9, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_2

    :cond_7
    sget v0, Lcom/google/android/apps/plus/R$id;->location_text:I

    if-ne v11, v0, :cond_8

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHANGE_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getEventLocationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/Place;)Landroid/content/Intent;

    move-result-object v12

    invoke-virtual {p0, v12, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_8
    sget v0, Lcom/google/android/apps/plus/R$id;->select_theme_button:I

    if-ne v11, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getEventThemePickerIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v12

    invoke-virtual {p0, v12, v7}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_2

    const-string v1, "new_event"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    const-string v1, "event_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventId:Ljava/lang/String;

    const-string v1, "owner_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mOwnerId:Ljava/lang/String;

    const-string v1, "event"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "event"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    :cond_0
    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_1
    const-string v1, "external_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mExternalId:Ljava/lang/String;

    const-string v1, "changed"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mChanged:Z

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_3
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/google/android/apps/plus/fragments/EditEventFragment$2;

    invoke-direct {v0, p0, v4, v4, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;Landroid/content/Context;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/google/android/apps/plus/fragments/EditEventFragment$3;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;Landroid/content/Context;Landroid/net/Uri;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v2, Lcom/google/android/apps/plus/R$layout;->edit_event_fragment:I

    invoke-virtual {p1, v2, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$id;->event_theme_image:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EventThemeView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/views/EventThemeView;->setOnImageListener(Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/EventThemeView;->setClickable(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/views/EventThemeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->select_theme_text:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionTextView:Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/plus/R$string;->event_change_theme:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->event_theme_progress_bar:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeProgressBar:Landroid/widget/ProgressBar;

    sget v2, Lcom/google/android/apps/plus/R$id;->event_name:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventNameView:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventNameView:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventNameTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->start_date:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mStartDateView:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mStartDateView:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->end_date:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndDateView:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndDateView:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->start_time:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mStartTimeView:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mStartTimeView:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->end_time:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndTimeView:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndTimeView:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->location_text:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mLocationView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mLocationView:Landroid/widget/TextView;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->location_container:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mLocationContainer:Landroid/view/View;

    sget v2, Lcom/google/android/apps/plus/R$id;->hangout_check:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mHangoutView:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mHangoutView:Landroid/widget/CheckBox;

    invoke-virtual {v2, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->audience_view:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    sget v3, Lcom/google/android/apps/plus/R$string;->event_invitees_hint:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setEmptyAudienceHint(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    new-instance v3, Lcom/google/android/apps/plus/fragments/EditEventFragment$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setAudienceChangedCallback(Ljava/lang/Runnable;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->select_theme_button:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionButton:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionButton:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->description:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mDescriptionView:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mDescriptionView:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventDescriptionTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$style;->CircleBrowserTheme:I

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    new-instance v2, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setCircleUsageType(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setShowPersonNameDialog(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setListener(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v2, p3}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onCreate(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setAutoCompleteAdapter(Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->edit_audience:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;

    invoke-direct {v2, v0}, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneSpinnerAdapter:Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneSpinnerAdapter:Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;->setTimeZoneHelper(Lcom/google/android/apps/plus/util/TimeZoneHelper;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->time_zone:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneSpinner:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneSpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneSpinnerAdapter:Lcom/google/android/apps/plus/phone/TimeZoneSpinnerAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getCurrentTimeZoneInfo()Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->getPosition()I

    move-result v2

    :goto_0
    iput v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mCurrentSpinnerPosition:I

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneSpinner:Landroid/widget/Spinner;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEvent()V

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->updateView(Landroid/view/View;)V

    return-object v1

    :cond_0
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const-string v0, "quit"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;->onEventClosed()V

    :cond_0
    return-void
.end method

.method public final onDiscard()V
    .locals 6

    const/4 v2, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->isEmptyAudience()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_3

    sget v1, Lcom/google/android/apps/plus/R$string;->new_event_quit_title:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$string;->new_event_quit_question:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->yes:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->no:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "quit"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    invoke-interface {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;->onEventClosed()V

    goto :goto_1

    :cond_4
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mChanged:Z

    if-eqz v1, :cond_5

    sget v1, Lcom/google/android/apps/plus/R$string;->edit_event_quit_title:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$string;->edit_event_quit_question:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->yes:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->no:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "quit"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    invoke-interface {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;->onEventClosed()V

    goto :goto_1
.end method

.method public final onEndDateCleared()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->clearEndTime()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndDate()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndTime()V

    return-void
.end method

.method public final onEndDateSet(III)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v3, p1, :cond_0

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v3, p2, :cond_0

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-eq v3, p3, :cond_1

    :cond_0
    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setEndTime(Ljava/util/Calendar;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndDate()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndTime()V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    :cond_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/32 v5, 0x6ddd00

    add-long/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_0
.end method

.method public final onEndTimeCleared()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->clearEndTime()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndTime()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndDate()V

    return-void
.end method

.method public final onEndTimeSet(II)V
    .locals 10
    .param p1    # I
    .param p2    # I

    const/16 v9, 0xc

    const/16 v8, 0xb

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v4, :cond_0

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v4, p1, :cond_0

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-eq v4, p2, :cond_3

    :cond_0
    invoke-virtual {v0, v8, p1}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0, v9, p2}, Ljava/util/Calendar;->set(II)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v1

    if-lez v4, :cond_2

    const/4 v4, 0x6

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    goto :goto_1

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/32 v6, 0x6ddd00

    add-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setEndTime(Ljava/util/Calendar;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndTime()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndDate()V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    :cond_3
    return-void
.end method

.method public final onImageLoadFinished$6ee69d29()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->enableEventPicker()V

    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 11
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget v8, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mCurrentSpinnerPosition:I

    if-eq p3, v8, :cond_1

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneSpinner:Landroid/widget/Spinner;

    invoke-virtual {v8}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->getOffset()J

    move-result-wide v5

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getCurrentTimeZoneInfo()Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->getOffset()J

    move-result-wide v0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getSystemTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneHelper:Lcom/google/android/apps/plus/util/TimeZoneHelper;

    invoke-virtual {v8, v2}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getOffset(Ljava/util/TimeZone;)J

    move-result-wide v0

    :cond_0
    sub-long v3, v0, v5

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v9, v8, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    add-long/2addr v9, v3

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    iput-object v9, v8, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v9, v8, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    add-long/2addr v9, v3

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    iput-object v9, v8, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    iput-object v9, v8, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 6
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    :goto_1
    invoke-direct {p0, v2, v3, v0, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setEventTheme(ILjava/lang/String;Landroid/net/Uri;Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->theme:Lcom/google/api/services/plusi/model/Theme;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->theme:Lcom/google/api/services/plusi/model/Theme;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeImage(Lcom/google/api/services/plusi/model/Theme;)Lcom/google/api/services/plusi/model/ThemeImage;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeId:I

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ThemeImage;->url:Ljava/lang/String;

    invoke-direct {p0, v2, v0, v1, v3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setEventTheme(ILjava/lang/String;Landroid/net/Uri;Z)V

    goto :goto_0

    :pswitch_1
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventLoaded:Z

    if-nez p2, :cond_3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mError:Z

    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->updateView(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mError:Z

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v0

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->authKey:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAuthKey:Ljava/lang/String;

    const/4 v0, -0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->theme:Lcom/google/api/services/plusi/model/Theme;

    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->theme:Lcom/google/api/services/plusi/model/Theme;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Theme;->themeId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_4
    iget v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeId:I

    if-eq v0, v2, :cond_5

    iput v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeId:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v5, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEvent()V

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public final onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/content/PersonData;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->addPerson(Lcom/google/android/apps/plus/content/PersonData;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->clearText()V

    return-void
.end method

.method public final onResume()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->onAudienceChanged()V

    :cond_1
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "new_event"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "event_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "owner_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mOwnerId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v0, :cond_0

    const-string v0, "event"

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-virtual {v1, v2}, Lcom/google/api/services/plusi/model/PlusEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    const-string v0, "external_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mExternalId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "changed"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mChanged:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final onSearchListAdapterStateChange(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    return-void
.end method

.method public final onStart()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onStart()V

    :cond_0
    return-void
.end method

.method public final onStartDateSet(III)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v3, p1, :cond_0

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v3, p2, :cond_0

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-eq v3, p3, :cond_2

    :cond_0
    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setStartTime(Ljava/util/Calendar;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindStartDate()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindStartTime()V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_1

    const/16 v3, 0xd

    const/16 v4, 0x1c20

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->add(II)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setEndTime(Ljava/util/Calendar;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndDate()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndTime()V

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    :cond_2
    return-void
.end method

.method public final onStartTimeSet(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    const/16 v7, 0xc

    const/16 v6, 0xb

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mTimeZoneSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v4, p1, :cond_0

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-eq v4, p2, :cond_2

    :cond_0
    invoke-virtual {v0, v6, p1}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0, v7, p2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setStartTime(Ljava/util/Calendar;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindStartTime()V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v1

    if-gez v4, :cond_1

    const/16 v4, 0xd

    const/16 v5, 0x1c20

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->add(II)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setEndTime(Ljava/util/Calendar;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndDate()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndTime()V

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    :cond_2
    return-void
.end method

.method public final onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onStop()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onStop()V

    :cond_0
    return-void
.end method

.method public final save()V
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$string;->event_update_operation_pending:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0, v1}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventOptions;->hangout:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object v6, v0, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mExternalId:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->createEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->event_no_title_hint:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->isEmptyAudience()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->event_no_audience_hint:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->event_no_time_hint:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->updateEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    goto/16 :goto_1
.end method

.method public final setOnEventChangedListener(Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    return-void
.end method
