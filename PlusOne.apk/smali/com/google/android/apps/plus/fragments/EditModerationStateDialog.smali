.class public Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "EditModerationStateDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/EditModerationStateDialog$ModerationListener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method private getActivityId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "activity_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSquareId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "square_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "square_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "activity_id"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    instance-of v3, v1, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog$ModerationListener;

    if-eqz v3, :cond_2

    move-object v2, v1

    check-cast v2, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog$ModerationListener;

    :cond_0
    :goto_0
    packed-switch p2, :pswitch_data_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v3, v0, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog$ModerationListener;

    if-eqz v3, :cond_0

    move-object v2, v0

    check-cast v2, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog$ModerationListener;

    goto :goto_0

    :pswitch_0
    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->getSquareId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->getActivityId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog$ModerationListener;->onRemoveActivity(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_1
    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->getSquareId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->getActivityId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog$ModerationListener;->onRestoreActivity(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_2
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1    # Landroid/os/Bundle;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/plus/R$string;->square_moderation_dialog_title:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->square_moderation_dialog_text:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->square_moderation_dialog_remove:I

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->square_moderation_dialog_restore:I

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
