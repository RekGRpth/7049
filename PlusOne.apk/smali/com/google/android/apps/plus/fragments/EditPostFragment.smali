.class public Lcom/google/android/apps/plus/fragments/EditPostFragment;
.super Landroid/support/v4/app/Fragment;
.source "EditPostFragment.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field private mChanged:Z

.field private mEditRequestId:Ljava/lang/Integer;

.field private mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

.field private mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/EditPostFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/EditPostFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/EditPostFragment;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditPostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/EditPostFragment;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/EditPostFragment;
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->handleEditPost(Lcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/fragments/EditPostFragment;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditPostFragment;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mChanged:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/EditPostFragment;)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditPostFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/EditPostActivity;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EditPostActivity;->invalidateMenu()V

    :cond_0
    return-void
.end method

.method private handleEditPost(Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v7, 0x0

    const/4 v6, 0x0

    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    const v3, 0x48ba7

    invoke-virtual {v0, v3}, Landroid/app/Activity;->dismissDialog(I)V

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v2

    instance-of v3, v2, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v3, :cond_1

    check-cast v2, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v3

    const/16 v4, 0xe

    if-ne v3, v4, :cond_1

    sget v3, Lcom/google/android/apps/plus/R$string;->post_not_sent_title:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->post_restricted_mention_error:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v3

    invoke-virtual {v1, v3, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "StreamPostRestrictionsNotSupported"

    invoke-virtual {v1, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v3, Lcom/google/android/apps/plus/R$string;->edit_post_error:I

    invoke-static {v0, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    const-string v0, "edit_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "edit_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    :cond_0
    const-string v0, "changed"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mChanged:Z

    :cond_1
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v5, Lcom/google/android/apps/plus/R$layout;->edit_comment_fragment:I

    const/4 v6, 0x0

    invoke-virtual {p1, v5, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$id;->text:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v5, "account"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    const-string v5, "activity_id"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    const/4 v6, 0x0

    invoke-virtual {v5, p0, v0, v1, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->init(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/views/AudienceView;)V

    const-string v5, "content"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    if-nez v2, :cond_0

    const-string v2, ""

    :cond_0
    invoke-virtual {v5, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHtml(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    new-instance v6, Lcom/google/android/apps/plus/fragments/EditPostFragment$2;

    invoke-direct {v6, p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/EditPostFragment;)V

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v5, p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-object v4
.end method

.method public final onDiscard()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/EditPostActivity;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mChanged:Z

    if-eqz v1, :cond_0

    const v1, 0xdc073

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EditPostActivity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EditPostActivity;->setResult(I)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EditPostActivity;->finish()V

    goto :goto_0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/widget/TextView;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    if-ne p1, v0, :cond_0

    packed-switch p2, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-static {p1}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public final onPost()V
    .locals 8

    const/4 v7, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v6}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/phone/EditPostActivity;

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mChanged:Z

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/phone/EditPostActivity;->setResult(I)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/EditPostActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/EditPostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v6, "account"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    const-string v6, "activity_id"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mPostTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/plus/api/ApiUtils;->buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v2

    const-string v6, "reshare"

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    const v6, 0x48ba7

    invoke-virtual {v3, v6}, Lcom/google/android/apps/plus/phone/EditPostActivity;->showDialog(I)V

    invoke-static {v3, v0, v1, v2, v5}, Lcom/google/android/apps/plus/service/EsService;->editActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public final onResume()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->handleEditPost(Lcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "edit_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mEditRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    const-string v0, "changed"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditPostFragment;->mChanged:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
