.class public Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "EditSquareAudienceFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field private static final SQUARES_PROJECTION:[Ljava/lang/String;

.field private static sDefaultSquareImage:Landroid/graphics/Bitmap;


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mLoaderError:Z

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mSquaresLoaderActive:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "square_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "square_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "photo_url"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->SQUARES_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mSquaresLoaderActive:Z

    new-instance v0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000()Landroid/graphics/Bitmap;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->sDefaultSquareImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mLoaderError:Z

    return v0
.end method

.method private isLoading()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateView(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x0

    const/16 v3, 0x8

    const v2, 0x102000a

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$id;->server_error:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mLoaderError:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->showContent(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->isLoading()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    sget v2, Lcom/google/android/apps/plus/R$string;->no_squares:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->showContent(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->isLoading()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isProgressIndicatorVisible()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->isProgressIndicatorVisible()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mSquaresLoaderActive:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez p1, :cond_0

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    invoke-virtual {v0, p2, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onAttach(Landroid/app/Activity;)V

    new-instance v0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;-><init>(Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    sget-object v1, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->sDefaultSquareImage:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_community_avatar:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->sDefaultSquareImage:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/google/android/apps/plus/fragments/SquareListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->SQUARES_PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/SquareListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v1, Lcom/google/android/apps/plus/R$layout;->edit_audience_fragment:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    instance-of v4, p2, Lcom/google/android/apps/plus/views/PeopleListItemView;

    if-eqz v4, :cond_0

    move-object v1, p2

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListItemView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContactName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v4, v5, v3, v2, v3}, Lcom/google/android/apps/plus/phone/Intents;->getSelectSquareCategoryActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v1, 0x0

    check-cast p2, Landroid/database/Cursor;

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mLoaderError:Z

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :pswitch_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mSquaresLoaderActive:Z

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/SquareListLoader;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/android/apps/plus/fragments/SquareListLoader;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/SquareListLoader;->isDataStale()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->refresh()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;->changeCursor(Landroid/database/Cursor;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->updateView(Landroid/view/View;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->refresh:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->refresh()V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "title"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    return-void
.end method

.method public final onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method public final refresh()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->refresh()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mNewerReqId:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsService;->getSquares(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mNewerReqId:Ljava/lang/Integer;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->updateSpinner()V

    return-void
.end method
