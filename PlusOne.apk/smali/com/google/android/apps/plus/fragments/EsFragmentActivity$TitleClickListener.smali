.class final Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;
.super Ljava/lang/Object;
.source "EsFragmentActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TitleClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;->this$0:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;-><init>(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->titlebar_icon_layout:I

    if-eq v0, v1, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$id;->titlebar_label:I

    if-eq v0, v1, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$id;->titlebar_label_2:I

    if-ne v0, v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;->this$0:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onTitlebarLabelClick()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget v1, Lcom/google/android/apps/plus/R$id;->title_button_1:I

    if-ne v0, v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;->this$0:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    # getter for: Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->access$100(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)[Landroid/view/MenuItem;

    move-result-object v1

    aget-object v1, v1, v3

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;->this$0:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;->this$0:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    # getter for: Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->access$100(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)[Landroid/view/MenuItem;

    move-result-object v2

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    goto :goto_0

    :cond_3
    sget v1, Lcom/google/android/apps/plus/R$id;->title_button_2:I

    if-ne v0, v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;->this$0:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    # getter for: Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->access$100(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)[Landroid/view/MenuItem;

    move-result-object v1

    aget-object v1, v1, v4

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;->this$0:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;->this$0:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    # getter for: Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->access$100(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)[Landroid/view/MenuItem;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    goto :goto_0

    :cond_4
    sget v1, Lcom/google/android/apps/plus/R$id;->title_button_3:I

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;->this$0:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    # getter for: Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->access$100(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)[Landroid/view/MenuItem;

    move-result-object v1

    aget-object v1, v1, v5

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;->this$0:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;->this$0:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    # getter for: Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->access$100(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)[Landroid/view/MenuItem;

    move-result-object v2

    aget-object v2, v2, v5

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    goto :goto_0
.end method
