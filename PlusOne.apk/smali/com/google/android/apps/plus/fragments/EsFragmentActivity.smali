.class public abstract Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.super Lcom/google/android/apps/plus/analytics/InstrumentedActivity;
.source "EsFragmentActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;,
        Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;,
        Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenu;
    }
.end annotation


# instance fields
.field private mHideTitleBar:Z

.field private final mMenuItems:[Landroid/view/MenuItem;

.field private final mTitleClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/view/MenuItem;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;

    new-instance v0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;-><init>(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mTitleClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)[Landroid/view/MenuItem;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;

    return-object v0
.end method

.method private static getVisibleItem(Landroid/view/Menu;I)Landroid/view/MenuItem;
    .locals 3
    .param p0    # Landroid/view/Menu;
    .param p1    # I

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Landroid/view/Menu;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-interface {p0, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/MenuItem;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    if-ne v1, p1, :cond_0

    invoke-interface {p0, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    :goto_1
    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private setupTitleButton1(Landroid/view/MenuItem;)V
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    const/4 v2, 0x0

    sget v1, Lcom/google/android/apps/plus/R$id;->title_button_1:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    invoke-interface {p1}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;

    aput-object p1, v1, v2

    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private setupTitleButton2(Landroid/view/MenuItem;)V
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    sget v1, Lcom/google/android/apps/plus/R$id;->title_button_2:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    invoke-interface {p1}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;

    const/4 v2, 0x1

    aput-object p1, v1, v2

    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private setupTitleButton3(Landroid/view/MenuItem;)V
    .locals 7
    .param p1    # Landroid/view/MenuItem;

    const/16 v6, 0xa

    const/4 v5, 0x0

    const/4 v4, 0x0

    sget v3, Lcom/google/android/apps/plus/R$id;->title_button_3:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3, v5, v5, v5}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    invoke-interface {p1}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getTitleButton3Text$9aa72f6()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    const/4 v3, -0x2

    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6, v4, v6, v4}, Landroid/widget/Button;->setPadding(IIII)V

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;

    const/4 v4, 0x2

    aput-object p1, v3, v4

    return-void

    :cond_1
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final createTitlebarButtons(I)V
    .locals 6
    .param p1    # I

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton1(Landroid/view/MenuItem;)V

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton2(Landroid/view/MenuItem;)V

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton3(Landroid/view/MenuItem;)V

    new-instance v1, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenu;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenu;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v3

    invoke-virtual {v3, p1, v1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPrepareTitlebarButtons(Landroid/view/Menu;)V

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Landroid/view/Menu;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-interface {v1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/MenuItem;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v2, v2, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    packed-switch v2, :pswitch_data_0

    const-string v3, "EsFragmentActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Maximum title buttons is 3. You have "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " visible menu items"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    :pswitch_0
    return-void

    :pswitch_1
    invoke-static {v1, v4}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getVisibleItem(Landroid/view/Menu;I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton3(Landroid/view/MenuItem;)V

    goto :goto_1

    :pswitch_2
    invoke-static {v1, v4}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getVisibleItem(Landroid/view/Menu;I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton2(Landroid/view/MenuItem;)V

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getVisibleItem(Landroid/view/Menu;I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton3(Landroid/view/MenuItem;)V

    goto :goto_1

    :pswitch_3
    invoke-static {v1, v4}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getVisibleItem(Landroid/view/Menu;I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton1(Landroid/view/MenuItem;)V

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getVisibleItem(Landroid/view/Menu;I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton2(Landroid/view/MenuItem;)V

    const/4 v3, 0x2

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getVisibleItem(Landroid/view/Menu;I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton3(Landroid/view/MenuItem;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected getTitleButton3Text$9aa72f6()Ljava/lang/CharSequence;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final goHome(Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v3, "notif_id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "notif_id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/phone/Intents;->getHostNavigationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x4000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected final isIntentAccountActive()Z
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "account"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "EsFragmentActivity"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "EsFragmentActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Activity finished because it is associated with a signed-out account: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .locals 0
    .param p1    # Landroid/view/Menu;

    return-void
.end method

.method protected onTitlebarLabelClick()V
    .locals 0

    return-void
.end method

.method protected final setTitlebarSubtitle(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$id;->titlebar_label_2:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez p1, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected final setTitlebarTitle(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$id;->titlebar_label:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method protected final showTitlebar(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->showTitlebar(ZZ)V

    return-void
.end method

.method protected showTitlebar(ZZ)V
    .locals 7
    .param p1    # Z
    .param p2    # Z

    const/4 v5, 0x0

    sget v4, Lcom/google/android/apps/plus/R$id;->title_layout:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mHideTitleBar:Z

    invoke-virtual {v2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    :cond_2
    if-eqz p1, :cond_3

    sget v4, Lcom/google/android/apps/plus/R$anim;->fade_in:I

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_3
    sget v4, Lcom/google/android/apps/plus/R$id;->titlebar_up:I

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-eqz p2, :cond_4

    move v4, v5

    :goto_1
    invoke-virtual {v6, v4}, Landroid/view/View;->setVisibility(I)V

    sget v4, Lcom/google/android/apps/plus/R$id;->titlebar_icon_layout:I

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz p2, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v4, Lcom/google/android/apps/plus/R$string;->nav_up_content_description:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_2
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    const/16 v4, 0x8

    goto :goto_1

    :cond_5
    invoke-virtual {v3, v5}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_2
.end method
