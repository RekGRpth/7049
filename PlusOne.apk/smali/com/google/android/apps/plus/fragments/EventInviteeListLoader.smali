.class public final Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "EventInviteeListLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$PersonProperties;,
        Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$CircleQuery;,
        Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$InviteeQuery;
    }
.end annotation


# static fields
.field private static final INVITEE_PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mEventId:Ljava/lang/String;

.field private final mOwnerId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "is_header"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "person_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "avatar"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "email"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "numaddguests"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "readstate"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "blacklisted"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "rsvp"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "is_past"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "invitee_count"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->INVITEE_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mEventId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mOwnerId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-void
.end method

.method private insertInviteeGroup(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;)V
    .locals 14
    .param p1    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p2    # Ljava/lang/String;
    .param p4    # Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Invitee;",
            ">;",
            "Lcom/google/android/apps/plus/phone/EsMatrixCursor;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$PersonProperties;",
            ">;)V"
        }
    .end annotation

    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsEventData;->getInviteeSummary(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Lcom/google/api/services/plusi/model/InviteeSummary;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v7, v6

    :goto_0
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v6

    if-ge v7, v6, :cond_1

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/plusi/model/Invitee;

    iget-object v8, v6, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-static {v8}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->isPersonVisible(Lcom/google/api/services/plusi/model/EmbedsPerson;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v6, v6, Lcom/google/api/services/plusi/model/Invitee;->numAdditionalGuests:Ljava/lang/Integer;

    invoke-static {v6}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    :cond_0
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_0

    :cond_1
    if-eqz v3, :cond_4

    iget-object v6, v3, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    :goto_1
    if-lez v4, :cond_c

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {p1, v1, v2}, Lcom/google/android/apps/plus/content/EsEventData;->isEventOver(Lcom/google/api/services/plusi/model/PlusEvent;J)Z

    move-result v6

    sget-object v7, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->INVITEE_PROJECTION:[Ljava/lang/String;

    array-length v7, v7

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/16 v8, 0xb

    aput-object p2, v7, v8

    const/16 v8, 0xd

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/16 v8, 0xc

    if-eqz v6, :cond_6

    const/4 v6, 0x1

    :goto_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v7, v8

    move-object/from16 v0, p4

    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    if-eqz p3, :cond_b

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/plusi/model/Invitee;

    iget-object v7, v6, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    if-eqz v7, :cond_2

    iget-object v7, v6, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-static {v7}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->isPersonVisible(Lcom/google/api/services/plusi/model/EmbedsPerson;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, v6, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v10, v7, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    iget-object v7, v6, Lcom/google/api/services/plusi/model/Invitee;->isAdminBlacklisted:Ljava/lang/Boolean;

    if-eqz v7, :cond_7

    iget-object v7, v6, Lcom/google/api/services/plusi/model/Invitee;->isAdminBlacklisted:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_7

    const/4 v7, 0x1

    :goto_4
    sget-object v8, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->INVITEE_PROJECTION:[Ljava/lang/String;

    array-length v8, v8

    new-array v11, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v8

    const/4 v8, 0x1

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v8

    const/4 v12, 0x2

    if-eqz v10, :cond_8

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v13, "g:"

    invoke-direct {v8, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    :goto_5
    aput-object v8, v11, v12

    const/4 v8, 0x3

    aput-object v10, v11, v8

    const/4 v8, 0x5

    iget-object v12, v6, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v12, v12, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    aput-object v12, v11, v8

    const/4 v8, 0x6

    iget-object v12, v6, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v12, v12, Lcom/google/api/services/plusi/model/EmbedsPerson;->email:Ljava/lang/String;

    aput-object v12, v11, v8

    const/16 v8, 0xb

    aput-object p2, v11, v8

    const/16 v12, 0x8

    iget-object v8, v6, Lcom/google/api/services/plusi/model/Invitee;->rsvpType:Ljava/lang/String;

    const-string v13, "ATTENDING"

    invoke-static {v8, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_9

    iget-object v8, v6, Lcom/google/api/services/plusi/model/Invitee;->numAdditionalGuests:Ljava/lang/Integer;

    invoke-static {v8}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v8

    :goto_6
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v11, v12

    const/16 v8, 0x9

    iget-object v6, v6, Lcom/google/api/services/plusi/model/Invitee;->readState:Ljava/lang/String;

    aput-object v6, v11, v8

    move-object/from16 v0, p5

    invoke-virtual {v0, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$PersonProperties;

    if-eqz v6, :cond_3

    const/4 v8, 0x4

    iget-object v10, v6, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$PersonProperties;->avatarUrl:Ljava/lang/String;

    aput-object v10, v11, v8

    const/4 v8, 0x7

    iget-object v6, v6, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$PersonProperties;->circleIds:Ljava/lang/String;

    aput-object v6, v11, v8

    :cond_3
    const/16 v8, 0xa

    if-eqz v7, :cond_a

    const/4 v6, 0x1

    :goto_7
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v11, v8

    move-object/from16 v0, p4

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_4
    const/4 v7, 0x0

    const/4 v6, 0x0

    move v8, v7

    move v7, v6

    :goto_8
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v6

    if-ge v7, v6, :cond_5

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/plusi/model/Invitee;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/Invitee;->numAdditionalGuests:Ljava/lang/Integer;

    invoke-static {v6}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    add-int/2addr v8, v6

    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_8

    :cond_5
    move v4, v8

    goto/16 :goto_1

    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_2

    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_4

    :cond_8
    const/4 v8, 0x0

    goto/16 :goto_5

    :cond_9
    const/4 v8, 0x0

    goto :goto_6

    :cond_a
    const/4 v6, 0x0

    goto :goto_7

    :cond_b
    sub-int v6, v4, v5

    if-lez v6, :cond_c

    sget-object v7, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->INVITEE_PROJECTION:[Ljava/lang/String;

    array-length v7, v7

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/16 v8, 0xd

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v7, v8

    move-object/from16 v0, p4

    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_c
    return-void
.end method

.method private static isPersonVisible(Lcom/google/api/services/plusi/model/EmbedsPerson;)Z
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/EmbedsPerson;

    if-eqz p0, :cond_1

    iget-object v0, p0, Lcom/google/api/services/plusi/model/EmbedsPerson;->email:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private queryCirclesAndAvatarsForPeople(Ljava/util/List;)Ljava/util/HashMap;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Invitee;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$PersonProperties;",
            ">;"
        }
    .end annotation

    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/api/services/plusi/model/Invitee;

    iget-object v0, v11, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    if-eqz v0, :cond_0

    iget-object v0, v11, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, v11, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v12, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "gaia_id IN("

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v9, 0x0

    :goto_1
    invoke-virtual {v12}, Ljava/util/HashMap;->size()I

    move-result v0

    if-ge v9, v0, :cond_3

    if-lez v9, :cond_2

    const/16 v0, 0x2c

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    const/16 v0, 0x3f

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_3
    const/16 v0, 0x29

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$CircleQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_5

    :goto_2
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v13, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$PersonProperties;

    const/4 v0, 0x0

    invoke-direct {v13, v0}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$PersonProperties;-><init>(B)V

    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v13, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$PersonProperties;->circleIds:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v13, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$PersonProperties;->avatarUrl:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_5
    return-object v12
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 32

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mEventId:Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mOwnerId:Ljava/lang/String;

    if-nez v2, :cond_1

    :cond_0
    const/4 v6, 0x0

    :goto_0
    return-object v6

    :cond_1
    const/16 v30, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->mEventId:Ljava/lang/String;

    sget-object v9, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader$InviteeQuery;->PROJECTION:[Ljava/lang/String;

    invoke-static {v2, v4, v8, v9}, Lcom/google/android/apps/plus/content/EsEventData;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v27

    const/4 v3, 0x0

    :try_start_0
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v2

    const/4 v4, 0x0

    move-object/from16 v0, v27

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/api/services/plusi/model/PlusEvent;

    move-object v3, v0

    const/4 v2, 0x1

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v26

    if-eqz v26, :cond_2

    sget-object v2, Lcom/google/android/apps/plus/content/EsEventData;->INVITEE_LIST_JSON:Lcom/google/android/apps/plus/json/EsJson;

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/json/EsJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    if-eqz v30, :cond_3

    move-object/from16 v0, v30

    iget-object v2, v0, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;->invitees:Ljava/util/List;

    if-nez v2, :cond_4

    :cond_3
    const/4 v6, 0x0

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_4
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v30

    iget-object v2, v0, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;->invitees:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    :goto_1
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/google/api/services/plusi/model/Invitee;

    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Invitee;->isAdminBlacklisted:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Invitee;->isAdminBlacklisted:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v0, v23

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Invitee;->rsvpType:Ljava/lang/String;

    move-object/from16 v31, v0

    const-string v2, "ATTENDING"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "CHECKIN"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_6
    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    const-string v2, "NOT_ATTENDING"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_8
    const-string v2, "MAYBE"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    move-object/from16 v0, v29

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_9
    move-object/from16 v0, v19

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_a
    move-object/from16 v0, v30

    iget-object v2, v0, Lcom/google/android/apps/plus/content/EsEventData$InviteeList;->invitees:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->queryCirclesAndAvatarsForPeople(Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v7

    new-instance v6, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v2, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->INVITEE_PROJECTION:[Ljava/lang/String;

    invoke-direct {v6, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    const-string v4, "ATTENDING"

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->insertInviteeGroup(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;)V

    const-string v10, "MAYBE"

    move-object/from16 v8, p0

    move-object v9, v3

    move-object v12, v6

    move-object v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->insertInviteeGroup(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;)V

    const-string v14, "NOT_ATTENDING"

    move-object/from16 v12, p0

    move-object v13, v3

    move-object/from16 v16, v6

    move-object/from16 v17, v7

    invoke-direct/range {v12 .. v17}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->insertInviteeGroup(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;)V

    const-string v18, "NOT_RESPONDED"

    move-object/from16 v16, p0

    move-object/from16 v17, v3

    move-object/from16 v20, v6

    move-object/from16 v21, v7

    invoke-direct/range {v16 .. v21}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->insertInviteeGroup(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;)V

    const-string v22, "REMOVED"

    move-object/from16 v20, p0

    move-object/from16 v21, v3

    move-object/from16 v24, v6

    move-object/from16 v25, v7

    invoke-direct/range {v20 .. v25}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;->insertInviteeGroup(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/util/HashMap;)V

    goto/16 :goto_0
.end method
