.class final Lcom/google/android/apps/plus/fragments/EventLocationFragment$1;
.super Ljava/lang/Object;
.source "EventLocationFragment.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/EventLocationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/EventLocationFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/EventLocationFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EventLocationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLocationChanged(Landroid/location/Location;)V
    .locals 5
    .param p1    # Landroid/location/Location;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EventLocationFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/EventLocationFragment;->removeLocationListener()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->access$000(Lcom/google/android/apps/plus/fragments/EventLocationFragment;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EventLocationFragment;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->access$100(Lcom/google/android/apps/plus/fragments/EventLocationFragment;DD)V

    return-void
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    return-void
.end method
