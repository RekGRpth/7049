.class public Lcom/google/android/apps/plus/fragments/EventLocationFragment;
.super Lcom/google/android/apps/plus/fragments/EsListFragment;
.source "EventLocationFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/EventLocationFragment$EventLocationAdapter;,
        Lcom/google/android/apps/plus/fragments/EventLocationFragment$OnLocationSelectedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsListFragment",
        "<",
        "Landroid/widget/ListView;",
        "Lcom/google/android/apps/plus/phone/EsCursorAdapter;",
        ">;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/text/TextWatcher;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field private static final LOCATION_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mCurrentLatitude:D

.field private mCurrentLongitude:D

.field private mInitialQuery:Ljava/lang/String;

.field private mListener:Lcom/google/android/apps/plus/fragments/EventLocationFragment$OnLocationSelectedListener;

.field private mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

.field private mLocationListener:Landroid/location/LocationListener;

.field private mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

.field private mLocationText:Landroid/widget/EditText;

.field private mQuery:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "description"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "location"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->LOCATION_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/EventLocationFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/EventLocationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationListener:Landroid/location/LocationListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/EventLocationFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/EventLocationFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->removeLocationListener()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/EventLocationFragment;DD)V
    .locals 9
    .param p0    # Lcom/google/android/apps/plus/fragments/EventLocationFragment;
    .param p1    # D
    .param p3    # D

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->isCurrentLocationKnown()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v8, v0, [F

    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLatitude:D

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLongitude:D

    move-wide v4, p1

    move-wide v6, p3

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    const/4 v0, 0x0

    aget v0, v8, v0

    const/high16 v1, 0x43480000

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    :cond_0
    iput-wide p1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLatitude:D

    iput-wide p3, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLongitude:D

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "event.current.latitude"

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLatitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "event.current.longitude"

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLongitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_2

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->buildLocationQuery()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->runQuery()V

    :cond_1
    return-void

    :cond_2
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private buildLocationQuery()V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mQuery:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->isCurrentLocationKnown()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLatitude:D

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    iget-wide v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLongitude:D

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V

    new-instance v1, Lcom/google/android/apps/plus/api/LocationQuery;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mQuery:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/plus/api/LocationQuery;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    :goto_0
    return-void

    :cond_0
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    goto :goto_0
.end method

.method private getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method private isCurrentLocationKnown()Z
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLatitude:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLongitude:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private removeLocationListener()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    :cond_0
    return-void
.end method

.method private runQuery()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getNearbyLocations(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/LocationQuery;)I

    :cond_0
    return-void
.end method

.method private updateAdapter(Landroid/database/Cursor;)V
    .locals 14
    .param p1    # Landroid/database/Cursor;

    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v9, 0x1

    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v5, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->LOCATION_PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    const/4 v1, 0x1

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mQuery:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    sget v6, Lcom/google/android/apps/plus/R$string;->event_location_none_title:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v11

    sget v6, Lcom/google/android/apps/plus/R$string;->event_location_none_description:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v12

    const/4 v6, 0x0

    aput-object v6, v5, v13

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void

    :cond_1
    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    sget v6, Lcom/google/android/apps/plus/R$string;->event_location_add:I

    new-array v7, v9, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mQuery:Ljava/lang/String;

    aput-object v8, v7, v10

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v11

    const/4 v6, 0x0

    aput-object v6, v5, v12

    const/4 v6, 0x0

    aput-object v6, v5, v13

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_2
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/content/DbLocation;->deserialize([B)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v3

    if-eqz v3, :cond_3

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    add-int/lit8 v2, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbLocation;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v11

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbLocation;->getBestAddress()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v12

    aput-object v4, v5, v13

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    move v1, v2

    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_2

    goto :goto_0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onAttach(Landroid/app/Activity;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->isCurrentLocationKnown()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "event.current.latitude"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "event.current.latitude"

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLatitude:D

    const-string v1, "event.current.longitude"

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLongitude:D

    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mQuery:Ljava/lang/String;

    const-string v0, "latitude"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLatitude:D

    const-string v0, "longitude"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLongitude:D

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->buildLocationQuery()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    if-nez v0, :cond_0

    const-string v7, "no_location_stream_key"

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-static {v0, v7}, Lcom/google/android/apps/plus/content/EsProvider;->buildLocationQueryUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "location"

    aput-object v6, v3, v5

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LocationQuery;->getKey()Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v1, Lcom/google/android/apps/plus/R$layout;->event_location_fragment:I

    invoke-super {p0, p1, p2, p3, v1}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/fragments/EventLocationFragment$EventLocationAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/fragments/EventLocationFragment$EventLocationAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v1, Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v1, Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->location_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationText:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mInitialQuery:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public final bridge synthetic onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic onDestroyView()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onDestroyView()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v5, p3}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/4 v5, 0x1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    const/4 v5, 0x4

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {v2}, Lcom/google/android/apps/plus/content/DbLocation;->deserialize([B)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbLocation;->toProtocolObject()Lcom/google/api/services/plusi/model/Location;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/util/LocationUtils;->convertLocationToPlace(Lcom/google/api/services/plusi/model/Location;)Lcom/google/api/services/plusi/model/Place;

    move-result-object v3

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getView()Landroid/view/View;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mListener:Lcom/google/android/apps/plus/fragments/EventLocationFragment$OnLocationSelectedListener;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mListener:Lcom/google/android/apps/plus/fragments/EventLocationFragment$OnLocationSelectedListener;

    invoke-interface {v5, v3}, Lcom/google/android/apps/plus/fragments/EventLocationFragment$OnLocationSelectedListener;->onLocationSelected(Lcom/google/api/services/plusi/model/Place;)V

    :cond_0
    return-void

    :pswitch_0
    const/4 v3, 0x0

    goto :goto_0

    :pswitch_1
    new-instance v3, Lcom/google/api/services/plusi/model/Place;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/Place;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mQuery:Ljava/lang/String;

    iput-object v5, v3, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->updateAdapter(Landroid/database/Cursor;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onPause()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onPause()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->removeLocationListener()V

    return-void
.end method

.method public final onResume()V
    .locals 8

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    const/4 v3, 0x1

    const-wide/16 v4, 0xbb8

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationListener:Landroid/location/LocationListener;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/LocationController;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZJLandroid/location/Location;Landroid/location/LocationListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->init()V

    :cond_1
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "query"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "latitude"

    iget-wide v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLatitude:D

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    const-string v0, "longitude"

    iget-wide v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mCurrentLongitude:D

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    return-void
.end method

.method public bridge synthetic onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onScroll(Landroid/widget/AbsListView;III)V

    return-void
.end method

.method public bridge synthetic onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mLocationText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mQuery:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mQuery:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->updateAdapter(Landroid/database/Cursor;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->buildLocationQuery()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->runQuery()V

    :cond_0
    return-void
.end method

.method public final setInitialQueryString(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mInitialQuery:Ljava/lang/String;

    return-void
.end method

.method public final setOnLocationSelectedListener(Lcom/google/android/apps/plus/fragments/EventLocationFragment$OnLocationSelectedListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/EventLocationFragment$OnLocationSelectedListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->mListener:Lcom/google/android/apps/plus/fragments/EventLocationFragment$OnLocationSelectedListener;

    return-void
.end method
