.class final Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment$ViewItemRecycler;
.super Ljava/lang/Object;
.source "HostedAllPhotosTileFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewItemRecycler"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment$ViewItemRecycler;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMovedToScrapHeap(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    instance-of v1, p1, Lcom/google/android/apps/plus/views/ImageResourceView;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->onRecycle()V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_0
    return-void
.end method
