.class public Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedAllPhotosTileFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment$ViewItemRecycler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;

.field private final mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field private mRefreshReqId:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$string;->refresh_photo_album_error:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->getView()Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->updateView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private updateView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-nez v0, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$string;->no_photos:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->updateSpinner()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->showContent(Landroid/view/View;)V

    goto :goto_1
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected final isProgressIndicatorVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->isProgressIndicatorVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoTileOneUpActivityIntentBuilder(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->setTileId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/google/android/apps/plus/content/EsTileData;->getViewId(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->setViewId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mRefreshReqId:Ljava/lang/Integer;

    :cond_0
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/google/android/apps/plus/content/EsTileData;->getViewId(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v6, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Lcom/google/android/apps/plus/R$layout;->hosted_all_photos_tile_fragment:I

    invoke-super {p0, p1, p2, p3, v3}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$dimen;->album_photo_grid_spacing:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    new-instance v3, Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;

    invoke-direct {v3, v0, v6}, Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;

    invoke-virtual {v3, p0}, Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->grid:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/ColumnGridView;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3, v1, v1, v1, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setPadding(IIII)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setItemMargin(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getPhotoColumns(Landroid/content/Context;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v4, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment$ViewItemRecycler;

    invoke-direct {v4, v5}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment$ViewItemRecycler;-><init>(B)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->setRecyclerListener(Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    sget v4, Lcom/google/android/apps/plus/R$drawable;->list_selected_holo:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelector(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    invoke-virtual {v3, v5, v6, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->updateView(Landroid/view/View;)V

    return-object v2
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->refresh()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->finishContextActionMode()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    return-void
.end method

.method public final onResume()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Lcom/google/android/apps/plus/views/ImageResourceView;

    if-eqz v3, :cond_0

    check-cast v2, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->onResume()V

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->onResume()V

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->showEmptyViewProgress(Landroid/view/View;)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {p0, v3, v1}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "refresh_request"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onStop()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v2, v1, Lcom/google/android/apps/plus/views/ImageResourceView;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->onStop()V

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final refresh()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->refresh()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getAllPhotoTiles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedAllPhotosTileFragment;->updateView(Landroid/view/View;)V

    goto :goto_0
.end method
