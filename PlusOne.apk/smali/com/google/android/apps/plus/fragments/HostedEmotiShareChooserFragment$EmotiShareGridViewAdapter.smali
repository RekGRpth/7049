.class final Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "HostedEmotiShareChooserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EmotiShareGridViewAdapter"
.end annotation


# instance fields
.field private final mClickListener:Landroid/view/View$OnClickListener;

.field private final mGrid:Lcom/google/android/apps/plus/views/ColumnGridView;

.field private final mLandscape:Z

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/apps/plus/views/ColumnGridView;Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;
    .param p4    # Lcom/google/android/apps/plus/views/ColumnGridView;
    .param p5    # Landroid/view/View$OnClickListener;

    const/4 v2, 0x2

    const/4 v1, 0x1

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->mLandscape:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->mLandscape:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p4, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOrientation(I)V

    iput-object p5, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->mGrid:Lcom/google/android/apps/plus/views/ColumnGridView;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private static createEmotiShareFromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;
    .locals 3
    .param p0    # Landroid/database/Cursor;

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    const/4 v2, 0x2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    move-result-object v1

    :cond_0
    return-object v1
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    sget v8, Lcom/google/android/apps/plus/R$string;->emotishare_in_list_count:I

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    add-int/lit8 v11, v0, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {p2, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v8, Lcom/google/android/apps/plus/R$id;->tag_position:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {p1, v8, v9}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-static {p3}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->createEmotiShareFromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    move-result-object v1

    sget v8, Lcom/google/android/apps/plus/R$id;->image_view:I

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/ImageResourceView;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->getIconRef()Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mSelectedObject:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;
    invoke-static {v8}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->access$000(Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->getId()I

    move-result v8

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mSelectedObject:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;
    invoke-static {v9}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->access$000(Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->getId()I

    move-result v9

    if-ne v8, v9, :cond_0

    sget v8, Lcom/google/android/apps/plus/R$id;->selector_view:I

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$drawable;->list_selected_holo:I

    invoke-virtual {v7, v8}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_0
    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setTag(Ljava/lang/Object;)V

    sget v8, Lcom/google/android/apps/plus/R$id;->image_label:I

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->mLandscape:Z

    if-eqz v8, :cond_1

    const/4 v6, 0x1

    :goto_0
    new-instance v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    const/4 v8, -0x3

    invoke-direct {v5, v6, v8}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_1
    const/4 v6, 0x2

    goto :goto_0
.end method

.method public final getEmotiShareForItem(I)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->createEmotiShareFromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->emotishare_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public final onResume()V
    .locals 5

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->onResume()V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->mGrid:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v4, :cond_1

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->mGrid:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->mGrid:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$id;->image_view:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->onResume()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->mGrid:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->onResume()V

    :cond_1
    return-void
.end method

.method public final onStop()V
    .locals 5

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->onStop()V

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->mGrid:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->mGrid:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$id;->image_view:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->onStop()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
