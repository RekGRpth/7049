.class public Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedEmotiShareChooserFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;

.field private mExtras:Landroid/os/Bundle;

.field private mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field private mMainView:Landroid/view/View;

.field private mSelectedObject:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mSelectedObject:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method private updateView(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->showContent(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->COMPOSE:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->isEmpty()Z

    move-result v0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    if-nez v2, :cond_2

    sget v6, Lcom/google/android/apps/plus/R$id;->tag_position:I

    invoke-virtual {p1, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    if-eqz v4, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->getEmotiShareForItem(I)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    move-result-object v2

    :cond_2
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v6, "android.intent.action.PICK"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v7, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_SELECTED:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v8, Lcom/google/android/apps/plus/analytics/OzViews;->COMPOSE:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v9

    invoke-static {v9, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->addExtrasForLogging(Landroid/os/Bundle;Lcom/google/android/apps/plus/content/DbEmotishareMetadata;)Landroid/os/Bundle;

    move-result-object v9

    invoke-static {v0, v6, v7, v8, v9}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-string v6, "typed_image_embed"

    invoke-virtual {v5, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v6, -0x1

    invoke-virtual {v0, v6, v5}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    :goto_1
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v6, v7, v2}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbEmotishareMetadata;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "audience"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/content/AudienceData;

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_4

    const-string v6, "audience"

    invoke-virtual {v3, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_4
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mExtras:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mExtras:Landroid/os/Bundle;

    const-string v1, "INTENT"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mExtras:Landroid/os/Bundle;

    const-string v1, "typed_image_embed"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mExtras:Landroid/os/Bundle;

    const-string v1, "typed_image_embed"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mSelectedObject:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mExtras:Landroid/os/Bundle;

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$1;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v2

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->EMOTISHARE_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsEmotiShareData;->EMOTISHARE_PROJECTION:[Ljava/lang/String;

    move-object v1, p0

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v0, Lcom/google/android/apps/plus/R$layout;->hosted_emotishare_chooser_view:I

    invoke-super {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mMainView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mMainView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->grid:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ColumnGridView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v0, Lcom/google/android/apps/plus/R$integer;->emotishare_icon_columns:I

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    sget v0, Lcom/google/android/apps/plus/R$dimen;->emotishare_item_margin:I

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/views/ColumnGridView;->setItemMargin(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v7, v7, v7, v7}, Lcom/google/android/apps/plus/views/ColumnGridView;->setPadding(IIII)V

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    move-object v1, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;-><init>(Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/apps/plus/views/ColumnGridView;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->list_selected_holo:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelector(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->isInSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->endSelectionMode()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mMainView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$string;->no_emotishares:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->setupEmptyView(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mMainView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->updateView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mMainView:Landroid/view/View;

    return-object v0
.end method

.method public final onDestroyView()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onDestroyView()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->unregisterSelectionListener()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOnScrollListener(Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;)V

    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->updateView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;

    invoke-static {}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->onPause()V

    return-void
.end method

.method public final onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mExtras:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    const-string v0, "INTENT"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mExtras:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onStop()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment$EmotiShareGridViewAdapter;->onStop()V

    return-void
.end method
