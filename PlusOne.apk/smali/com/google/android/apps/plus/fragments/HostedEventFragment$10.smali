.class final Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;
.super Landroid/os/AsyncTask;
.source "HostedEventFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedEventFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field currentAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field currentEventId:Ljava/lang/String;

.field skippedPhotoIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->skippedPhotoIds:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->currentAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$1100(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->currentEventId:Ljava/lang/String;

    return-void
.end method

.method private varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 13
    .param p1    # [Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v12, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->currentAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    aget-object v9, p1, v3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->val$context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->PHOTOS_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    iget-object v4, v4, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    new-instance v11, Ljava/util/StringTokenizer;

    invoke-direct {v11, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    new-instance v10, Ljava/util/LinkedHashSet;

    invoke-direct {v10}, Ljava/util/LinkedHashSet;-><init>()V

    :goto_0
    invoke-virtual {v11}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v11}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {v10}, Ljava/util/LinkedHashSet;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v10, v2}, Ljava/util/LinkedHashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->skippedPhotoIds:Ljava/util/List;

    const/4 v3, 0x1

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v2

    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->val$context:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->currentEventId:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/util/LinkedHashSet;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v10, v2}, Ljava/util/LinkedHashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-static {v3, v4, v6, v5, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$1800(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-object v12

    :cond_2
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :catchall_0
    move-exception v2

    if-eqz v7, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->skippedPhotoIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->currentAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->currentEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->skippedPhotoIds:Ljava/util/List;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->sharePhotosToEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)I

    :cond_0
    return-void
.end method
