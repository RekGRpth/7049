.class final Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;
.super Landroid/os/AsyncTask;
.source "HostedEventFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedEventFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "InstantShareToggle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mEnabled:Z

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # [Ljava/lang/Object;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;->mEnabled:Z

    const/4 v0, 0x1

    aget-object v0, p1, v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;->mActivity:Landroid/app/Activity;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;->mActivity:Landroid/app/Activity;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;->mEnabled:Z

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$2400(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Lcom/google/api/services/plusi/model/PlusEvent;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsEventData;->enableInstantShare(Landroid/content/Context;ZLcom/google/api/services/plusi/model/PlusEvent;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSettingsCallbacks:Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$2500(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_0
    return-void
.end method
