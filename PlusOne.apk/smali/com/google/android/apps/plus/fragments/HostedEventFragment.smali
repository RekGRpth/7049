.class public Lcom/google/android/apps/plus/fragments/HostedEventFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedEventFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog$CommentEditDialogListener;
.implements Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$ViewUseListener;
.implements Lcom/google/android/apps/plus/views/EventActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$12;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$DeleteEventConfirmationDialog;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$EventPeopleQuery;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$ActivityQuery;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$DetailsQuery;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$EventRefreshRunnable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog$CommentEditDialogListener;",
        "Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$ViewUseListener;",
        "Lcom/google/android/apps/plus/views/EventActionListener;"
    }
.end annotation


# static fields
.field private static mNextPagePreloadTriggerRows:I


# instance fields
.field private mActivityId:Ljava/lang/String;

.field private mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

.field private mAuthKey:Ljava/lang/String;

.field private mCanComment:Z

.field private mCommentReqId:Ljava/lang/Integer;

.field private mCreatePlusOneRequestId:Ljava/lang/Integer;

.field private mDeletePlusOneRequestId:Ljava/lang/Integer;

.field private mDeleteReqId:Ljava/lang/Integer;

.field private mDetailsObserver:Landroid/database/ContentObserver;

.field private mError:Z

.field private mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

.field private mEventId:Ljava/lang/String;

.field private mEventLoaded:Z

.field private mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

.field private mFetchReqId:Ljava/lang/Integer;

.field private mFirstActivityTimestamp:J

.field private mGhostEvent:Z

.field private mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field private mHasUserInteracted:Z

.field private mIncomingRsvpType:Ljava/lang/String;

.field private mInvitationToken:Ljava/lang/String;

.field private mInviteReqId:Ljava/lang/Integer;

.field private final mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mNeedsRefresh:Z

.field private mNewPhotoReqId:Ljava/lang/Integer;

.field private mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

.field private mPollingToken:Ljava/lang/String;

.field private mPreloadRequested:Z

.field private mRefreshRunnable:Ljava/lang/Runnable;

.field private mReportAbuseRequestId:Ljava/lang/Integer;

.field private mResumeToken:Ljava/lang/String;

.field private mSavedScrollPos:I

.field private mSendRsvpReqId:Ljava/lang/Integer;

.field private final mSettingsCallbacks:Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;

.field private mShowKeyboard:Z

.field private mSource:I

.field private mTemporalRsvpState:Ljava/lang/String;

.field private mTypeId:I

.field private mViewLogged:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSettingsCallbacks:Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;

    new-instance v0, Lcom/google/android/apps/plus/fragments/EventActiveState;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/EventActiveState;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSavedScrollPos:I

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$1;

    invoke-static {}, Lcom/google/android/apps/plus/util/ThreadUtil;->getUiThreadHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDetailsObserver:Landroid/database/ContentObserver;

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->isPaused()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleInviteMoreComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->isPaused()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1302(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mShowKeyboard:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showCommentDialog()V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->isPaused()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V
    .locals 8
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mPollingToken:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mResumeToken:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAuthKey:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/service/EsService;->readEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateProgressIndicator()V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Lcom/google/android/apps/plus/content/AudienceData;)V
    .locals 6
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$string;->event_inviting_more:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showProgressDialog(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAuthKey:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    :goto_1
    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->invitePeopleToEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 10
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v8, 0x0

    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "camera-sync"

    move-object v6, v0

    :goto_0
    move v7, v1

    :goto_1
    array-length v0, p4

    if-ge v7, v0, :cond_4

    aget-object v1, p4, v7

    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    const-string v0, "album_id"

    invoke-virtual {v9, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "upload_account"

    invoke-virtual {v9, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "media_url"

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "event_id"

    invoke-virtual {v9, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    :try_start_0
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/util/MediaStoreUtils;->MEDIA_ID_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "media_id"

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->UPLOADS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    :cond_2
    const-string v0, "events"

    move-object v6, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :cond_4
    return-void

    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method static synthetic access$1900(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Lcom/google/android/apps/plus/fragments/EventActiveState;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->isPaused()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    sget v0, Lcom/google/android/apps/plus/R$string;->event_deleting:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showProgressDialog(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAuthKey:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->deleteEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeleteReqId:Ljava/lang/Integer;

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ZZ)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;
    .param p1    # Z
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->turnOnInstantShare(ZZ)V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->isPaused()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Lcom/google/api/services/plusi/model/PlusEvent;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSettingsCallbacks:Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNeedsRefresh:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInvitationToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleGetEventUpdatesComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleCreateCommentComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeleteReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeleteReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->hideProgressDialog()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeleteReqId:Ljava/lang/Integer;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_DELETED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleSendEventRsvpComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleNewPhotoComplete(ILcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V

    return-void
.end method

.method private fetchData()V
    .locals 8

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mError:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mPollingToken:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mResumeToken:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInvitationToken:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAuthKey:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/service/EsService;->readEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateProgressIndicator()V

    return-void
.end method

.method private handleCreateCommentComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->hideProgressDialog()V

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_COMMENT_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->fetchData()V

    goto :goto_0
.end method

.method private handleGetEventUpdatesComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 5
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateProgressIndicator()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->hideProgressDialog()V

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getErrorCode()I

    move-result v0

    const/16 v1, 0x190

    if-lt v0, v1, :cond_3

    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_3

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGhostEvent:Z

    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v4, v3, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    const-string v2, "HEF"

    const-string v3, "HGEUC"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->checkPartitions(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mError:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->no_connection:I

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method private handleInviteMoreComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->hideProgressDialog()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->fetchData()V

    goto :goto_0
.end method

.method private handleNewPhotoComplete(ILcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V
    .locals 6
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;
    .param p3    # Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->hideProgressDialog()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_2

    sget v1, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    sget v1, Lcom/google/android/apps/plus/R$string;->event_post_photo:I

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedEventFragment$9;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$9;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Landroid/content/Context;)V

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    aput-object p3, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$9;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private handleSendEventRsvpComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "send_rsvp"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTemporalRsvpState:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateActiveEventState()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateRsvpSection()V

    goto :goto_0
.end method

.method private hideProgressDialog()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private inviteMore()V
    .locals 9

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->event_invite_activity_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/16 v4, 0xb

    const/4 v7, 0x1

    move v6, v5

    move v8, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private processPendingPhotoRequest()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->getLastCameraMediaLocation()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleNewPhotoComplete(ILcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    :cond_0
    return-void
.end method

.method private showCommentDialog()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "comment"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/DialogFragment;

    if-nez v1, :cond_0

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_COMMENT_BOX_OPENED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->event_comment_dialog_title:I

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->newInstance(ILjava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "comment"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private showPhotoDialog()V
    .locals 4

    const/4 v3, 0x1

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const-string v2, "camera_photos"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumType(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->photo_picker_album_label_share:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerTitleResourceId(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setHideAlbumSpinner(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setTakePhoto(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setTakeVideo(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private showProgressDialog(I)V
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private toggleInstantShare(Z)V
    .locals 5
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v1, v1, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareEnabled:Z

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private turnOnInstantShare(ZZ)V
    .locals 6
    .param p1    # Z
    .param p2    # Z

    const/4 v5, 0x1

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    const-string v0, "camera-event.jpg"

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/Intents;->getCameraIntentPhoto$3a35108a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsEventData;->isViewerCheckedIn(Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAuthKey:Ljava/lang/String;

    const-string v4, "CHECKIN"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->sendEventRsvp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->toggleInstantShare(Z)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->change_photo_no_camera:I

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private updateActiveEventState()V
    .locals 10

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mHasUserInteracted:Z

    iput-boolean v8, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->hasUserInteracted:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    invoke-static {v4, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    iput-boolean v8, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isOwner:Z

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean v7, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareAvailable:Z

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean v7, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareExpired:Z

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v5, v4, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->isInstantShareAllowed(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;J)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean v6, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareAvailable:Z

    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/content/EsEventData;->canInviteOthers(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v8

    iput-boolean v8, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->canInviteOthers:Z

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    if-nez v5, :cond_6

    move v5, v6

    :goto_1
    iput-boolean v5, v8, Lcom/google/android/apps/plus/fragments/EventActiveState;->isRsvpEnabled:Z

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTemporalRsvpState:Ljava/lang/String;

    iput-object v8, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->temporalRsvpValue:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget v8, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSource:I

    iput v8, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->eventSource:I

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    if-eqz v8, :cond_7

    :goto_2
    iput-boolean v6, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->plusable:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean v7, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareAvailable:Z

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean v7, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareExpired:Z

    :cond_1
    iget v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTypeId:I

    const/16 v6, 0x3a

    if-ne v5, v6, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v5, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareAvailable:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v5, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareEnabled:Z

    if-nez v5, :cond_2

    new-instance v5, Lcom/google/android/apps/plus/fragments/HostedEventFragment$11;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$11;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V

    invoke-static {v5}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    :cond_2
    iput v7, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTypeId:I

    return-void

    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v5, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->isEventOver(Lcom/google/api/services/plusi/model/PlusEvent;J)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean v6, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareExpired:Z

    goto :goto_0

    :cond_4
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mRefreshRunnable:Ljava/lang/Runnable;

    if-nez v5, :cond_5

    new-instance v5, Lcom/google/android/apps/plus/fragments/HostedEventFragment$EventRefreshRunnable;

    invoke-direct {v5, p0, v7}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$EventRefreshRunnable;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;B)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mRefreshRunnable:Ljava/lang/Runnable;

    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mRefreshRunnable:Ljava/lang/Runnable;

    invoke-static {v5}, Lcom/google/android/apps/plus/util/ThreadUtil;->removeCallbacksOnUiThread(Ljava/lang/Runnable;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v5, v4, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->timeUntilInstantShareAllowed(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v8, 0x0

    cmp-long v5, v0, v8

    if-lez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mRefreshRunnable:Ljava/lang/Runnable;

    invoke-static {v5, v0, v1}, Lcom/google/android/apps/plus/util/ThreadUtil;->postDelayedOnUiThread(Ljava/lang/Runnable;J)V

    goto/16 :goto_0

    :cond_6
    move v5, v7

    goto :goto_1

    :cond_7
    move v6, v7

    goto :goto_2
.end method

.method private updateProgressIndicator()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGhostEvent:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventLoaded:Z

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->hideProgressIndicator()V

    goto :goto_0
.end method

.method private updateRsvpSection()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getView()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v2, Lcom/google/android/apps/plus/R$id;->event_rsvp_section:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/EventRsvpLayout;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    invoke-virtual {v0, v2, v3, p0}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventActionListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->invalidate()V

    goto :goto_0
.end method

.method private updateView(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x0

    const/16 v3, 0x8

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget v2, Lcom/google/android/apps/plus/R$id;->server_error:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$id;->grid:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGhostEvent:Z

    if-eqz v2, :cond_2

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    sget v2, Lcom/google/android/apps/plus/R$string;->event_does_not_exist:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showContent(Landroid/view/View;)V

    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateProgressIndicator()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v2, :cond_3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showContent(Landroid/view/View;)V

    goto :goto_1

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventLoaded:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    :cond_4
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_1

    :cond_5
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mError:Z

    if-eqz v2, :cond_1

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    sget v2, Lcom/google/android/apps/plus/R$string;->event_details_error:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showContent(Landroid/view/View;)V

    goto :goto_1
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final handleReportEventCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 5
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "req_pending"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/DialogFragment;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateSpinner()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_3

    sget v2, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    invoke-static {v0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    sget v2, Lcom/google/android/apps/plus/R$string;->report_abuse_event_completed_toast:I

    invoke-static {v0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected final handleSharePhotosToEventCallBack$b5e9bbb(Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->event_photo_share_failed_toast:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isProgressIndicatorVisible()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->isProgressIndicatorVisible()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActionButtonClicked(I)V
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_PAGE_ADD_PHOTOS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showPhotoDialog()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showCommentDialog()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v3, -0x1

    if-eq p2, v3, :cond_1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->INVITE_WIDGET_CANCEL_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_0

    :cond_1
    packed-switch p1, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v3, v0, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    if-eqz v3, :cond_2

    move-object v3, v0

    check-cast v3, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    invoke-interface {v3}, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;->showInsertCameraPhotoDialog()V

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v4, "camera-event.jpg"

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->insertCameraPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    goto :goto_0

    :pswitch_3
    if-ne p2, v3, :cond_0

    if-eqz p3, :cond_0

    const-string v3, "media_taken"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v3, v3, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareEnabled:Z

    if-nez v3, :cond_0

    :cond_3
    const-string v3, "insert_photo_request_id"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v3, v0, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    if-eqz v3, :cond_4

    check-cast v0, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;->showInsertCameraPhotoDialog()V

    :cond_4
    const-string v3, "insert_photo_request_id"

    invoke-virtual {p3, v3, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->processPendingPhotoRequest()V

    goto :goto_0

    :cond_5
    const-string v3, "mediarefs"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget v5, Lcom/google/android/apps/plus/R$string;->event_post_photo:I

    invoke-static {v3, v5, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    new-instance v5, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;

    invoke-direct {v5, p0, v3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Landroid/content/Context;)V

    new-array v3, v8, [Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v5, v3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    :pswitch_4
    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->INVITE_WIDGET_ADD_PEOPLE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    const-string v3, "audience"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v3, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;

    invoke-direct {v3, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Lcom/google/android/apps/plus/content/AudienceData;)V

    invoke-static {v3}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public final onAddPhotosClicked()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showPhotoDialog()V

    return-void
.end method

.method public final onAvatarClicked(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onCommentEditCancelled()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_COMMENT_BOX_CLOSED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_0
.end method

.method public final onCommentEditComplete$42c1be52(Landroid/text/Spannable;)V
    .locals 6
    .param p1    # Landroid/text/Spannable;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_COMMENT_BOX_CLOSED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p1}, Lcom/google/android/apps/plus/api/ApiUtils;->buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v5

    sget v0, Lcom/google/android/apps/plus/R$string;->event_comment_sending:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showProgressDialog(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAuthKey:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->createEventComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean v2, v0, Lcom/google/android/apps/plus/fragments/EventActiveState;->expanded:Z

    if-eqz p1, :cond_e

    const-string v0, "id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    const-string v0, "typeid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTypeId:I

    const-string v0, "invitation_token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInvitationToken:Ljava/lang/String;

    const-string v0, "incoming_rsvp_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mIncomingRsvpType:Ljava/lang/String;

    const-string v0, "refresh"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNeedsRefresh:Z

    const-string v0, "scroll_pos"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSavedScrollPos:I

    const-string v0, "first_timestamp"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFirstActivityTimestamp:J

    const-string v0, "fetch_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "fetch_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    :cond_0
    const-string v0, "comment_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "comment_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    :cond_1
    const-string v0, "new_photo_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "new_photo_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    :cond_2
    const-string v0, "invite_more_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "invite_more_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    :cond_3
    const-string v0, "rsvp_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "rsvp_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    :cond_4
    const-string v0, "create_event_plus_one_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "create_event_plus_one_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCreatePlusOneRequestId:Ljava/lang/Integer;

    :cond_5
    const-string v0, "delete_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "delete_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeletePlusOneRequestId:Ljava/lang/Integer;

    :cond_6
    const-string v0, "temp_rsvp_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "temp_rsvp_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTemporalRsvpState:Ljava/lang/String;

    :cond_7
    const-string v0, "delete_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "delete_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeleteReqId:Ljava/lang/Integer;

    :cond_8
    const-string v0, "abuse_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "abuse_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    :cond_9
    const-string v0, "view_logged"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "view_logged"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mViewLogged:Z

    :cond_a
    const-string v0, "show_keyboard"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "show_keyboard"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mShowKeyboard:Z

    :cond_b
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mViewLogged:Z

    if-nez v0, :cond_c

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_PAGE_VIEWED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mViewLogged:Z

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    const-string v1, "expanded"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/apps/plus/fragments/EventActiveState;->expanded:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->invalidateActionBar()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mIncomingRsvpType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mIncomingRsvpType:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->onRsvpChanged(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mIncomingRsvpType:Ljava/lang/String;

    :cond_d
    return-void

    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    if-eqz v0, :cond_f

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNeedsRefresh:Z

    goto :goto_0

    :cond_f
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGhostEvent:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-object v1

    :pswitch_1
    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedEventFragment$3;

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    invoke-direct {v1, p0, v0, v2, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Landroid/content/Context;Landroid/net/Uri;Landroid/content/Context;)V

    goto :goto_0

    :pswitch_2
    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedEventFragment$4;

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    invoke-direct {v1, p0, v0, v2, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Landroid/content/Context;Landroid/net/Uri;Landroid/content/Context;)V

    goto :goto_0

    :pswitch_3
    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedEventFragment$5;

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    invoke-direct {v1, p0, v0, v2, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Landroid/content/Context;Landroid/net/Uri;Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v5, 0x0

    sget v2, Lcom/google/android/apps/plus/R$layout;->hosted_event_fragment:I

    invoke-virtual {p0, p1, p2, p3, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$id;->grid:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ColumnGridView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v2, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-direct {v2, v3, v4, p0, p0}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/ColumnGridView;Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/EventActionListener;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSettingsCallbacks:Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;

    invoke-virtual {v2, v3, v5, v4}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    sget v2, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNextPagePreloadTriggerRows:I

    if-nez v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v0

    iget v2, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v2, :cond_1

    const/16 v2, 0x8

    sput v2, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNextPagePreloadTriggerRows:I

    :cond_0
    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateView(Landroid/view/View;)V

    return-object v1

    :cond_1
    const/16 v2, 0x10

    sput v2, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNextPagePreloadTriggerRows:I

    goto :goto_0
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    const-string v0, "dialog_photo_sync"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->turnOnInstantShare(ZZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "report_event"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->reportActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    sget v0, Lcom/google/android/apps/plus/R$string;->report_abuse_operation_pending:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showProgressDialog(I)V

    goto :goto_0
.end method

.method public final onEventPlusOneClicked()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->isPostPlusOnePending(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->deletePostPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeletePlusOneRequestId:Ljava/lang/Integer;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->createPostPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCreatePlusOneRequestId:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public final onExpansionToggled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean p1, v0, Lcom/google/android/apps/plus/fragments/EventActiveState;->expanded:Z

    return-void
.end method

.method public final onHangoutClicked()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->hangoutInfo:Lcom/google/api/services/plusi/model/HangoutInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->RSVP_JOIN_HANGOUT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getEventHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public final onInstantShareToggle(Z)V
    .locals 9
    .param p1    # Z

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v5, "account"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/InstantUpload;->isSyncEnabled$1f9c1b43(Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v2, "dialog_check_in"

    invoke-virtual {v6, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsEventData;->isViewerCheckedIn(Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v5, :cond_2

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/EventOptions;->broadcast:Ljava/lang/Boolean;

    invoke-virtual {v5, v7}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    sget-object v5, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;->ON_AIR:Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    :goto_1
    new-instance v7, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;

    invoke-direct {v7, v2, v5}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;-><init>(ZLcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;)V

    const-string v2, "dialog_check_in"

    invoke-virtual {v7, v6, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    invoke-virtual {v7, p0, v4}, Landroid/support/v4/app/DialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    :cond_0
    :goto_2
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mHasUserInteracted:Z

    return-void

    :cond_1
    move v2, v4

    goto :goto_0

    :cond_2
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/PlusEvent;->isPublic:Ljava/lang/Boolean;

    invoke-virtual {v5, v7}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    sget-object v5, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;->PUBLIC:Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    goto :goto_1

    :cond_3
    sget-object v5, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;->PRIVATE:Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    goto :goto_1

    :cond_4
    if-nez v1, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v5, "dialog_master_sync"

    invoke-virtual {v2, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    if-nez v5, :cond_0

    sget v5, Lcom/google/android/apps/plus/R$string;->event_instant_share_dialog_title:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->event_master_sync_dialog_message:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v5, v6, v7, v8}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v5

    invoke-virtual {v5, p0, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    const-string v4, "dialog_master_sync"

    invoke-virtual {v5, v2, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v5, "dialog_photo_sync"

    invoke-virtual {v2, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    if-nez v5, :cond_0

    sget v5, Lcom/google/android/apps/plus/R$string;->es_google_iu_provider:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->event_enable_sync_dialog_message:I

    new-array v7, v3, [Ljava/lang/Object;

    aput-object v5, v7, v4

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->event_instant_share_dialog_title:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->yes:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$string;->no:I

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v5, v7, v8}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v5

    invoke-virtual {v5, p0, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    const-string v4, "dialog_photo_sync"

    invoke-virtual {v5, v2, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->toggleInstantShare(Z)V

    goto/16 :goto_2
.end method

.method public final onInviteMoreClicked()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->inviteMore()V

    return-void
.end method

.method public final onLinkClicked(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "https://plus.google.com/s/%23"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x1d

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/plus/phone/Intents;->getPostSearchActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/plus/phone/Intents;->isProfileUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/Intents;->getPersonIdFromProfileUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "extra_gaia_id"

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PERSON:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 8
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v5, -0x1

    const/4 v7, 0x4

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x1

    check-cast p2, Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    const-string v3, "HEF"

    const-string v4, "OLF"

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->checkPartitions(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mPreloadRequested:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventLoaded:Z

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x7

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSource:I

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v0

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->authKey:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAuthKey:Ljava/lang/String;

    const/4 v0, 0x6

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCanComment:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareEnabled:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareExpired:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->onInstantShareToggle(Z)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->changeInfoCursor(Landroid/database/Cursor;Lcom/google/android/apps/plus/fragments/EventActiveState;)V

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSavedScrollPos:I

    if-eq v0, v5, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->isWrapContentEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSavedScrollPos:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelection(I)V

    iput v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSavedScrollPos:I

    :cond_3
    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mPollingToken:Ljava/lang/String;

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mResumeToken:Ljava/lang/String;

    const/4 v0, 0x5

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->invalidateActionBar()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNeedsRefresh:Z

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->fetchData()V

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateActiveEventState()V

    :goto_2
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mShowKeyboard:Z

    if-eqz v0, :cond_5

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$6;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    invoke-virtual {v0, v6, v3}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->changeInfoCursor(Landroid/database/Cursor;Lcom/google/android/apps/plus/fragments/EventActiveState;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGhostEvent:Z

    if-eqz v0, :cond_8

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventLoaded:Z

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mError:Z

    goto :goto_2

    :cond_8
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->fetchData()V

    goto :goto_2

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->changeActivityCursor(Landroid/database/Cursor;)V

    if-nez p2, :cond_9

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFirstActivityTimestamp:J

    goto/16 :goto_0

    :cond_9
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFirstActivityTimestamp:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    iput-wide v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFirstActivityTimestamp:J

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelectionToTop()V

    goto/16 :goto_0

    :pswitch_3
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    if-eqz p2, :cond_a

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_a

    :goto_3
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/plus/content/EsEventData$ResolvedPerson;

    invoke-direct {v5, v3, v2, v4}, Lcom/google/android/apps/plus/content/EsEventData$ResolvedPerson;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_a
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->setResolvedPeople(Ljava/util/HashMap;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v1, v6, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onLocationClicked()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/MapUtils;->showDrivingDirections(Landroid/content/Context;Lcom/google/api/services/plusi/model/Place;)V

    :cond_0
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1    # Landroid/view/MenuItem;

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v4, Lcom/google/android/apps/plus/R$id;->edit_event:I

    if-ne v1, v4, :cond_0

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_PAGE_EDIT_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAuthKey:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/apps/plus/phone/Intents;->getEditEventActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return v2

    :cond_0
    sget v4, Lcom/google/android/apps/plus/R$id;->delete_event:I

    if-ne v1, v4, :cond_1

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_PAGE_DELETE_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    new-instance v4, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DeleteEventConfirmationDialog;

    invoke-direct {v4}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DeleteEventConfirmationDialog;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "delete_event_conf"

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DeleteEventConfirmationDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    invoke-virtual {v4, p0, v3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DeleteEventConfirmationDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    goto :goto_0

    :cond_1
    sget v4, Lcom/google/android/apps/plus/R$id;->invite_more:I

    if-ne v1, v4, :cond_2

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->INVITE_WIDGET_OPENED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->inviteMore()V

    goto :goto_0

    :cond_2
    sget v4, Lcom/google/android/apps/plus/R$id;->report_abuse:I

    if-ne v1, v4, :cond_3

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_REPORT_ABUSE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    sget v4, Lcom/google/android/apps/plus/R$string;->menu_report_abuse:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->event_report_question:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v4

    invoke-virtual {v4, p0, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "activity_id"

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v5, "report_event"

    invoke-virtual {v4, v3, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_0
.end method

.method public final onPause()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDetailsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$id;->event_header_view:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->pausePlayback()V

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    return-void
.end method

.method public final onPhotoClicked(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoOneUpActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoId(Ljava/lang/Long;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setEventId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_0
    sget v1, Lcom/google/android/apps/plus/R$string;->event_activity_title:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    :goto_2
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoUrl(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    goto :goto_1

    :cond_2
    sget v1, Lcom/google/android/apps/plus/R$string;->event_activity_title:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public final onPhotoUpdateNeeded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->updateEventPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/content/EsEventData;->canAddPhotos(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    if-eqz v3, :cond_0

    if-nez v0, :cond_0

    sget v3, Lcom/google/android/apps/plus/R$drawable;->icn_events_add_photo:I

    sget v4, Lcom/google/android/apps/plus/R$string;->event_button_add_photo_label:I

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCanComment:Z

    if-eqz v2, :cond_1

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_events_add_comment:I

    sget v3, Lcom/google/android/apps/plus/R$string;->event_button_add_comment_label:I

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateProgressIndicator()V

    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 5
    .param p1    # Landroid/view/Menu;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v0, v2

    :goto_0
    if-eqz v0, :cond_0

    sget v3, Lcom/google/android/apps/plus/R$id;->edit_event:I

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v3, Lcom/google/android/apps/plus/R$id;->delete_event:I

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    if-nez v0, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-nez v3, :cond_1

    move v1, v2

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v3, v3, Lcom/google/android/apps/plus/fragments/EventActiveState;->canInviteOthers:Z

    if-eqz v3, :cond_2

    sget v3, Lcom/google/android/apps/plus/R$id;->invite_more:I

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    if-eqz v1, :cond_3

    sget v3, Lcom/google/android/apps/plus/R$id;->report_abuse:I

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final onResume()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    const-string v3, "HEF"

    const-string v4, "OR"

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->checkPartitions(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleGetEventUpdatesComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleSendEventRsvpComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleCreateCommentComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->processPendingPhotoRequest()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleInviteMoreComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleReportEventCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCreatePlusOneRequestId:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCreatePlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCreatePlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCreatePlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCreatePlusOneRequestId:Ljava/lang/Integer;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCreatePlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v2, :cond_a

    :cond_5
    :goto_0
    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCreatePlusOneRequestId:Ljava/lang/Integer;

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeletePlusOneRequestId:Ljava/lang/Integer;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeletePlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeletePlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeletePlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeletePlusOneRequestId:Ljava/lang/Integer;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeletePlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v2, :cond_b

    :cond_7
    :goto_1
    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeletePlusOneRequestId:Ljava/lang/Integer;

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->SETTINGS_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDetailsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getView()Landroid/view/View;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$id;->event_header_view:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->resumePlayback()V

    :cond_9
    return-void

    :cond_a
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    invoke-virtual {v2, v6, v5, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0

    :cond_b
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    invoke-virtual {v2, v6, v5, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_1
.end method

.method public final onRsvpChanged(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpType(Lcom/google/api/services/plusi/model/PlusEvent;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAuthKey:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4, p1}, Lcom/google/android/apps/plus/service/EsService;->sendEventRsvp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTemporalRsvpState:Ljava/lang/String;

    const-string v1, "ATTENDING"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->RSVP_YES_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateActiveEventState()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateRsvpSection()V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mIncomingRsvpType:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    sget v1, Lcom/google/android/apps/plus/R$string;->event_send_rsvp:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v5, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "send_rsvp"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mIncomingRsvpType:Ljava/lang/String;

    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mHasUserInteracted:Z

    return-void

    :cond_4
    const-string v1, "MAYBE"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->RSVP_MAYBE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_0

    :cond_5
    const-string v1, "NOT_ATTENDING"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->RSVP_NO_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "typeid"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTypeId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "invitation_token"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInvitationToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "incoming_rsvp_type"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mIncomingRsvpType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "refresh"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNeedsRefresh:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "expanded"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v1, v1, Lcom/google/android/apps/plus/fragments/EventActiveState;->expanded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "view_logged"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mViewLogged:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "first_timestamp"

    iget-wide v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFirstActivityTimestamp:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "show_keyboard"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mShowKeyboard:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v0, :cond_0

    const-string v0, "scroll_pos"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    const-string v0, "fetch_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    const-string v0, "rsvp_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTemporalRsvpState:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v0, "temp_rsvp_state"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTemporalRsvpState:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    const-string v0, "comment_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    const-string v0, "new_photo_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    const-string v0, "invite_more_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeleteReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    const-string v0, "delete_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeleteReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    const-string v0, "abuse_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCreatePlusOneRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    const-string v0, "create_event_plus_one_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCreatePlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeletePlusOneRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    const-string v0, "delete_event_plus_one_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeletePlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    const-string v1, "HEF"

    const-string v2, "ON"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->checkPartitions(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSetArguments(Landroid/os/Bundle;)V

    const-string v0, "event_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    const-string v0, "invitation_token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInvitationToken:Ljava/lang/String;

    const-string v0, "auth_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAuthKey:Ljava/lang/String;

    const-string v0, "rsvp"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mIncomingRsvpType:Ljava/lang/String;

    const-string v0, "show_keyboard"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mShowKeyboard:Z

    const-string v0, "notif_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTypeId:I

    return-void
.end method

.method public final onStop()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$id;->event_header_view:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->stop()V

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onStop()V

    return-void
.end method

.method public final onUpdateCardClicked(Lcom/google/android/apps/plus/views/EventUpdate;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/EventUpdate;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "update_card"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->newInstance()Lcom/google/android/apps/plus/fragments/EventUpdateDialog;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->setUpdate(Lcom/google/android/apps/plus/views/EventUpdate;)V

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    const-string v2, "update_card"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onViewAllInviteesClicked()V
    .locals 5

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->LIST_INVITED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAuthKey:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    :goto_0
    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/apps/plus/phone/Intents;->getEventInviteeListActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onViewUsed(I)V
    .locals 2
    .param p1    # I

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mPreloadRequested:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mResumeToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mError:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->getCount()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNextPagePreloadTriggerRows:I

    sub-int/2addr v0, v1

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mPreloadRequested:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedEventFragment$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$7;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final refresh()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->refresh()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->fetchData()V

    return-void
.end method
