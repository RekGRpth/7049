.class final Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;
.super Ljava/lang/Object;
.source "HostedEventThemeListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ThemeSetInfo"
.end annotation


# instance fields
.field private mDisplayName:Ljava/lang/String;

.field private mFilter:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;->mDisplayName:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;->mFilter:I

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget v0, Lcom/google/android/apps/plus/R$string;->event_themes_featured_tab_text:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;->mDisplayName:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/apps/plus/R$string;->event_themes_patterns_tab_text:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;->mDisplayName:Ljava/lang/String;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final getFilter()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;->mFilter:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method
