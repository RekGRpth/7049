.class public Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;
.super Lcom/google/android/apps/plus/fragments/HostedListFragment;
.source "HostedEventThemeListFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$EventThemeListAdapter;,
        Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;,
        Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$OnThemeSelectedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedListFragment",
        "<",
        "Landroid/widget/ListView;",
        "Lcom/google/android/apps/plus/phone/EsCursorAdapter;",
        ">;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field private static final EVENT_THEME_COLUMNS:[Ljava/lang/String;


# instance fields
.field private mCurrentSpinnerPosition:I

.field private mDataLoaded:Z

.field private mFilter:I

.field private mListener:Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$OnThemeSelectedListener;

.field private mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mScrollToTop:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "theme_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "image_url"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "placeholder_path"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->EVENT_THEME_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedListFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mFilter:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedListFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mFilter:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mFilter:I

    return v0
.end method

.method static synthetic access$100()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->EVENT_THEME_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method private getPosition(I)I
    .locals 3
    .param p1    # I

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;->getFilter()I

    move-result v2

    if-ne v2, p1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT_THEMES:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedListFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "filter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mFilter:I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 2
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$2;

    invoke-direct {v1, p0, v0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;Landroid/content/Context;Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v1, Lcom/google/android/apps/plus/R$layout;->event_theme_list_fragment:I

    invoke-super {p0, p1, p2, p3, v1}, Lcom/google/android/apps/plus/fragments/HostedListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$EventThemeListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$EventThemeListAdapter;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v1, Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v1, Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v1, Landroid/widget/ListView;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    sget v1, Lcom/google/android/apps/plus/R$string;->event_theme_list_empty:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->setupEmptyView(Landroid/view/View;I)V

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v3, p3}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mListener:Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$OnThemeSelectedListener;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mListener:Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$OnThemeSelectedListener;

    invoke-interface {v3, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$OnThemeSelectedListener;->onThemeSelected(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mDataLoaded:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mScrollToTop:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->resetScrollPosition()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mScrollToTop:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mDataLoaded:Z

    if-eqz v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->showContent(Landroid/view/View;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget v1, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$layout;->simple_spinner_item:I

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mFilter:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->getPosition(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mCurrentSpinnerPosition:I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->updateSpinner()V

    return-void
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .locals 4
    .param p1    # I

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mCurrentSpinnerPosition:I

    if-eq v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;

    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$ThemeSetInfo;->getFilter()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mFilter:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mScrollToTop:Z

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "filter"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mFilter:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public final refresh()V
    .locals 0

    return-void
.end method

.method public final setOnThemeSelectedListener(Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$OnThemeSelectedListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$OnThemeSelectedListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->mListener:Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$OnThemeSelectedListener;

    return-void
.end method
