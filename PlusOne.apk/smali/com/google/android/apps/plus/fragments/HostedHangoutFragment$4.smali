.class final Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;
.super Ljava/lang/Object;
.source "HostedHangoutFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShowSuggestedPeople:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$700(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getSuggestedPeople()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$800(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$900(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Landroid/widget/TextView;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$900(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Landroid/widget/TextView;

    move-result-object v2

    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->updateSuggestedPeopleDisplay()V
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$1000(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    const/16 v5, 0xa

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->audienceSizeIsGreaterThan(I)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->isAudienceEmpty()Z

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$1100(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Landroid/widget/ImageButton;

    move-result-object v5

    if-nez v0, :cond_5

    move v2, v3

    :goto_0
    invoke-virtual {v5, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    if-nez v0, :cond_2

    if-eqz v1, :cond_6

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    invoke-static {v2, v4, v4}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$1200(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;ZZ)V

    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    # setter for: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyAudienceEmpty:Z
    invoke-static {v2, v0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$1302(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Z)Z

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    # setter for: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyOvercapacity:Z
    invoke-static {v2, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$1402(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Z)Z

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mStartHangoutButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$1600(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Landroid/widget/Button;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mStartHangoutButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$1600(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Landroid/widget/Button;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->isAudienceEmpty()Z

    move-result v5

    if-nez v5, :cond_8

    :goto_2
    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_4
    return-void

    :cond_5
    move v2, v4

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyAudienceEmpty:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$1300(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyOvercapacity:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$1400(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRingBeforeDisable:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$600(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->access$1500(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Z)V

    goto :goto_1

    :cond_8
    move v3, v4

    goto :goto_2
.end method
