.class public Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedHangoutFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$5;,
        Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;,
        Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;,
        Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$HangoutSuggestionsQuery;,
        Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleQuery;,
        Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;"
    }
.end annotation


# static fields
.field private static final ACTIVE_HANGOUT_MODE_DEFAULT:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;


# instance fields
.field private displayedSuggestedParticipants:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private mActiveViewMode:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

.field private mAudienceOverlay:Landroid/view/View;

.field protected mAudienceResult:Lcom/google/android/apps/plus/content/AudienceData;

.field protected mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

.field private mCacheSuggestionsResponse:Z

.field private mCircleUsageType:I

.field private mFilterNullGaiaIds:Z

.field private mGridView:Landroid/widget/GridView;

.field private mIncludePhoneOnlyContacts:Z

.field private mIncludePlusPages:Z

.field private mListHeader:Landroid/widget/TextView;

.field private mListParent:Landroid/view/View;

.field private mPreviouslyAudienceEmpty:Z

.field private mPreviouslyOvercapacity:Z

.field private mPublicProfileSearchEnabled:Z

.field private mRealTimeChatListener:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;

.field private mRequestId:Ljava/lang/Integer;

.field private mResumeHangoutButton:Landroid/widget/Button;

.field private mRingBeforeDisable:Z

.field protected mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

.field private mShowSuggestedPeople:Z

.field private mStartHangoutButton:Landroid/widget/Button;

.field private mSuggestedPeople:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

.field private mSuggestedPeopleScrollView:Landroid/widget/ScrollView;

.field private mSuggestedPeopleSize:I

.field private mToggleHangoutRingButton:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;->MODE_DISABLE:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    sput-object v0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->ACTIVE_HANGOUT_MODE_DEFAULT:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePhoneOnlyContacts:Z

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRealTimeChatListener:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    sget-object v0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->ACTIVE_HANGOUT_MODE_DEFAULT:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mActiveViewMode:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleSize:I

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyAudienceEmpty:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyOvercapacity:Z

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->updateSuggestedPeopleDisplay()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;ZZ)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;
    .param p1    # Z
    .param p2    # Z

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->disableHangoutRing(ZZ)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyAudienceEmpty:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyAudienceEmpty:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyOvercapacity:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyOvercapacity:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Z)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;
    .param p1    # Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->enableHangoutRing(Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mStartHangoutButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->loadSuggestedPeople(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->cacheSuggestedResponse(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->isInAudience(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRingBeforeDisable:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->disableHangoutRing(ZZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->audienceSizeIsGreaterThan(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->disableHangoutRing(ZZ)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->isAudienceEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->enableHangoutRing(Z)V

    goto :goto_0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRingBeforeDisable:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShowSuggestedPeople:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    return-object v0
.end method

.method private cacheSuggestedResponse(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCacheSuggestionsResponse:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/content/EsAudienceData;->processSuggestionsResponse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCacheSuggestionsResponse:Z

    :cond_0
    return-void
.end method

.method private disableHangoutRing(ZZ)V
    .locals 2
    .param p1    # Z
    .param p2    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRingBeforeDisable:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_ring_off:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_ring_off_content_description:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    if-eqz p1, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$string;->ring_off_overcapacity_hangout_toast:I

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->toast(I)V

    :cond_0
    return-void

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$string;->ring_off_hangout_toast:I

    goto :goto_0
.end method

.method private enableHangoutRing(Z)V
    .locals 2
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRingBeforeDisable:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_ring_on:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_ring_on_content_description:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$string;->ring_on_hangout_toast:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->toast(I)V

    :cond_0
    return-void
.end method

.method private isInAudience(Ljava/lang/String;)Z
    .locals 7
    .param p1    # Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v3, v0, v4

    invoke-static {v3}, Lcom/google/android/apps/plus/realtimechat/ParticipantUtils;->getParticipantIdFromPerson(Lcom/google/android/apps/plus/content/PersonData;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v6, 0x1

    :goto_1
    return v6

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private loadSuggestedPeople(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V
    .locals 6
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getSuggestionList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->getSuggestedUserList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->updateSuggestedPeopleDisplay()V

    return-void
.end method

.method private toast(I)V
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private updateSuggestedPeopleDisplay()V
    .locals 15

    const/4 v11, 0x1

    const/4 v12, 0x0

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    const/4 v2, 0x0

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->displayedSuggestedParticipants:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v2, 0x1

    :cond_2
    if-nez v2, :cond_0

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->displayedSuggestedParticipants:Ljava/util/List;

    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getVisibility()I

    move-result v10

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    const/4 v5, 0x0

    new-instance v7, Landroid/database/MatrixCursor;

    sget-object v10, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleQuery;->columnNames:[Ljava/lang/String;

    invoke-direct {v7, v10}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->displayedSuggestedParticipants:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    const/4 v10, 0x4

    new-array v13, v10, [Ljava/lang/Object;

    add-int/lit8 v6, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v13, v12

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v13, v11

    const/4 v10, 0x2

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v13, v10

    const/4 v14, 0x3

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->isInAudience(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    move v10, v11

    :goto_2
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v13, v14

    invoke-virtual {v7, v13}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move v5, v6

    goto :goto_1

    :cond_4
    move v10, v12

    goto :goto_2

    :cond_5
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {v10, v7}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleSize:I

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->getCount()I

    move-result v11

    if-eq v10, v11, :cond_6

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->getCount()I

    move-result v10

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v11}, Landroid/widget/GridView;->getChildCount()I

    move-result v11

    if-ne v10, v11, :cond_6

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v10, v12, v12}, Landroid/widget/ScrollView;->scrollTo(II)V

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->getCount()I

    move-result v10

    iput v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleSize:I

    :cond_6
    return-void
.end method


# virtual methods
.method public final audienceSizeIsGreaterThan(I)Z
    .locals 10
    .param p1    # I

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v8

    if-lez v8, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v8

    add-int/lit8 v3, v8, 0x0

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_6

    aget-object v2, v0, v4

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v8

    const/16 v9, 0x9

    if-eq v8, v9, :cond_3

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v8

    const/4 v9, 0x7

    if-eq v8, v9, :cond_3

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v8

    const/16 v9, 0x8

    if-ne v8, v9, :cond_4

    :cond_3
    move v6, v7

    goto :goto_0

    :cond_4
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getSize()I

    move-result v8

    if-lez v8, :cond_5

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getSize()I

    move-result v8

    add-int/2addr v3, v8

    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_6
    const/16 v8, 0xa

    if-le v3, v8, :cond_0

    move v6, v7

    goto :goto_0
.end method

.method public final getAudience()Lcom/google/android/apps/plus/content/AudienceData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    return-object v0
.end method

.method protected final getSuggestedPeople()V
    .locals 5

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->isAudienceEmpty()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCacheSuggestionsResponse:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;->HANGOUT:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    invoke-static {v2, v3, v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->requestSuggestedParticipants(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_0
    return-void
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final isAudienceEmpty()Z
    .locals 9

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v7

    if-lez v7, :cond_2

    move v5, v6

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v2, v0, v3

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getSize()I

    move-result v7

    if-gtz v7, :cond_3

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v7

    const/16 v8, 0x9

    if-eq v7, v8, :cond_3

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v7

    const/4 v8, 0x7

    if-ne v7, v8, :cond_4

    :cond_3
    move v5, v6

    goto :goto_0

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public final isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onActivityCreated(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "audience"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "audience"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceResult:Lcom/google/android/apps/plus/content/AudienceData;

    :cond_0
    return-void
.end method

.method public final onAddPersonToCirclesAction$1165610b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onCircleSelected(Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/content/CircleData;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->addCircle(Lcom/google/android/apps/plus/content/CircleData;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->clearText()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->edit_audience:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget v2, Lcom/google/android/apps/plus/R$string;->realtimechat_edit_audience_activity_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCircleUsageType:I

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePhoneOnlyContacts:Z

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePlusPages:Z

    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPublicProfileSearchEnabled:Z

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mFilterNullGaiaIds:Z

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeople:Ljava/util/List;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->displayedSuggestedParticipants:Ljava/util/List;

    if-eqz p1, :cond_0

    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    const-string v1, "cache_suggestions_response"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCacheSuggestionsResponse:Z

    :goto_0
    const-string v1, "show_suggested_people"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShowSuggestedPeople:Z

    const-string v1, "public_profile_search"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPublicProfileSearchEnabled:Z

    const-string v1, "phone_only_contacts"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePhoneOnlyContacts:Z

    const-string v1, "plus_pages"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePlusPages:Z

    :cond_0
    const/16 v1, 0xa

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCircleUsageType:I

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePhoneOnlyContacts:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePlusPages:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPublicProfileSearchEnabled:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShowSuggestedPeople:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mFilterNullGaiaIds:Z

    sget-object v1, Lcom/google/android/apps/plus/util/Property;->ACTIVE_HANGOUT_MODE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v0

    const-string v1, "disable"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;->MODE_DISABLE:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mActiveViewMode:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    :goto_1
    return-void

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCacheSuggestionsResponse:Z

    goto :goto_0

    :cond_2
    const-string v1, "hide"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;->MODE_HIDE:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mActiveViewMode:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    goto :goto_1

    :cond_3
    const-string v1, "none"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;->MODE_NONE:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mActiveViewMode:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    goto :goto_1

    :cond_4
    sget-object v1, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->ACTIVE_HANGOUT_MODE_DEFAULT:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mActiveViewMode:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    goto :goto_1
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    const-string v0, "HangoutFrag"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HangoutFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "onCreateLoader "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->HANGOUT_SUGGESTIONS_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$HangoutSuggestionsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v6, "sequence ASC"

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    :goto_0
    return-object v0

    :cond_1
    move-object v0, v4

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v4, 0x0

    const/4 v3, 0x0

    sget v1, Lcom/google/android/apps/plus/R$layout;->hosted_hangout_fragment:I

    invoke-virtual {p1, v1, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mGridView:Landroid/widget/GridView;

    sget v1, Lcom/google/android/apps/plus/R$id;->suggested_people_scroll_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleScrollView:Landroid/widget/ScrollView;

    sget v1, Lcom/google/android/apps/plus/R$id;->list_layout_parent:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListParent:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->list_header:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v1, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;-><init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mGridView:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->toggle_hangout_ring_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v4, v4}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->disableHangoutRing(ZZ)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->start_hangout_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mStartHangoutButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mStartHangoutButton:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mStartHangoutButton:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->resume_hangout_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mResumeHangoutButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mResumeHangoutButton:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->audience_overlay:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xc

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->onItemClick(I)V

    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 5
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v4, 0x3

    const/4 v3, 0x1

    check-cast p2, Landroid/database/Cursor;

    const-string v0, "HangoutFrag"

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HangoutFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onLoadFinished "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v3, :cond_2

    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->updateSuggestedPeopleDisplay()V

    :cond_2
    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/R$id;->help:I

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->url_param_help_hangouts:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->startExternalActivity(Landroid/content/Intent;)V

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_0
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShowSuggestedPeople:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRealTimeChatListener:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->unregisterListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    :cond_0
    return-void
.end method

.method public final onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/content/PersonData;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->addPerson(Lcom/google/android/apps/plus/content/PersonData;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->clearText()V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    sget v0, Lcom/google/android/apps/plus/R$string;->home_screen_hangout_label:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    return-void
.end method

.method public final onResume()V
    .locals 6

    const/4 v5, 0x4

    const/16 v4, 0x8

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mStartHangoutButton:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mResumeHangoutButton:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListParent:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$string;->realtimechat_users_you_may_know:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangout()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mStartHangoutButton:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mResumeHangoutButton:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    sget-object v1, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$5;->$SwitchMap$com$google$android$apps$plus$fragments$HostedHangoutFragment$ActiveHangoutMode:[I

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mActiveViewMode:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceResult:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceResult:Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceResult:Lcom/google/android/apps/plus/content/AudienceData;

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShowSuggestedPeople:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRealTimeChatListener:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->removeResult(I)Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getErrorCode()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasSuggestionsResponse()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getSuggestionsResponse()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->loadSuggestedPeople(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getSuggestionsResponse()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->cacheSuggestedResponse(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    :cond_3
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListParent:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onSaveInstanceState(Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "cache_suggestions_response"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCacheSuggestionsResponse:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1
    const-string v0, "show_suggested_people"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShowSuggestedPeople:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "public_profile_search"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPublicProfileSearchEnabled:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "phone_only_contacts"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePhoneOnlyContacts:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "plus_pages"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePlusPages:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final onSearchListAdapterStateChange(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListParent:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListParent:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListParent:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onStart()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onStart()V

    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onStop()V

    :cond_0
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->audience_view:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    sget v2, Lcom/google/android/apps/plus/R$string;->realtimechat_new_conversation_hint_text:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setEmptyAudienceHint(I)V

    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$style;->CircleBrowserTheme:I

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    new-instance v1, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePhoneOnlyContacts:Z

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setIncludePhoneNumberContacts(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePlusPages:Z

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setIncludePlusPages(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPublicProfileSearchEnabled:Z

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setPublicProfileSearchEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCircleUsageType:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setCircleUsageType(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mFilterNullGaiaIds:Z

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setFilterNullGaiaIds(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setListener(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onCreate(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setAutoCompleteAdapter(Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->initLoaders(Landroid/support/v4/app/LoaderManager;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->edit_audience:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setAudienceChangedCallback(Ljava/lang/Runnable;)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShowSuggestedPeople:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getSuggestedPeople()V

    :cond_1
    return-void
.end method
