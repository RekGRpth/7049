.class public final Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;
.super Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;
.source "HostedPeopleFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CompositeLoader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader",
        "<",
        "Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;",
        ">;"
    }
.end annotation


# instance fields
.field private mCelebrities:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

.field private mCelebritiesLoaded:Z

.field private mCirclesCursor:Landroid/database/Cursor;

.field private mCirclesCursorLoaded:Z

.field private mIsLoggedInAsPlusPage:Z

.field mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private mObserverRegistered:Z

.field private mPeopleData:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

.field private mSuggestedPeople:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

.field private mSuggestedPeopleLoaded:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    iput-boolean p2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mIsLoggedInAsPlusPage:Z

    return-void
.end method

.method private static addSuggestionItem(Lcom/google/android/apps/plus/phone/EsMatrixCursor;I[Ljava/lang/Object;II)[Ljava/lang/Object;
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .param p1    # I
    .param p2    # [Ljava/lang/Object;
    .param p3    # I
    .param p4    # I

    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    if-nez p2, :cond_0

    const/4 v1, 0x4

    new-array p2, v1, [Ljava/lang/Object;

    if-ne p1, v3, :cond_0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v3

    :cond_0
    aget-object v1, p2, v2

    if-nez v1, :cond_2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v4

    if-ne p1, v4, :cond_1

    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    move-object p2, v0

    :cond_1
    :goto_0
    return-object p2

    :cond_2
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v3

    const/4 v1, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p2, v1

    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    move-object p2, v0

    goto :goto_0
.end method

.method private deliverResult(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->isReset()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mPeopleData:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private esLoadInBackground()Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;
    .locals 24

    const/4 v4, 0x0

    const/16 v20, 0x0

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mSuggestedPeopleLoaded:Z

    move/from16 v21, v0

    if-eqz v21, :cond_1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mCirclesCursorLoaded:Z

    move/from16 v21, v0

    if-eqz v21, :cond_1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mCelebritiesLoaded:Z

    move/from16 v21, v0

    if-eqz v21, :cond_1

    const/4 v9, 0x1

    :goto_0
    if-eqz v9, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mCirclesCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mSuggestedPeople:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mCelebrities:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v9, :cond_2

    const/4 v7, 0x0

    :goto_1
    return-object v7

    :cond_1
    const/4 v9, 0x0

    goto :goto_0

    :catchall_0
    move-exception v21

    monitor-exit p0

    throw v21

    :cond_2
    new-instance v7, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    invoke-direct {v7}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;-><init>()V

    new-instance v10, Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->getContext()Landroid/content/Context;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v10, v0}, Lcom/google/android/apps/plus/util/StreamLayoutInfo;-><init>(Landroid/content/Context;)V

    const/16 v21, 0x2

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sSuggestionsMaxCardWidth:I
    invoke-static {}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$000()I

    move-result v22

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2, v10}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->getNumColumnsGivenMaxWidth(IILcom/google/android/apps/plus/util/StreamLayoutInfo;)I

    move-result v12

    const/16 v17, 0x0

    new-instance v6, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sSuggestionsCursorColumnNames:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$100()[Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v6, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    if-nez v20, :cond_9

    const/16 v21, 0x0

    const/16 v22, 0x7

    const/16 v23, -0x1

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v6, v12, v0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->addSuggestionItem(Lcom/google/android/apps/plus/phone/EsMatrixCursor;I[Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v17

    if-eqz v17, :cond_3

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    const/16 v17, 0x0

    :cond_3
    :goto_2
    iput-object v6, v7, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->suggestionsLayoutCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v0, v20

    iput-object v0, v7, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->suggestionsData:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    iput-object v3, v7, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->celebritiesData:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    const/16 v21, 0x3

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sYourCirclesMaxCardWidth:I
    invoke-static {}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$200()I

    move-result v22

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2, v10}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->getNumColumnsGivenMaxWidth(IILcom/google/android/apps/plus/util/StreamLayoutInfo;)I

    move-result v11

    invoke-static {v11, v10}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->getColumnWidth(ILcom/google/android/apps/plus/util/StreamLayoutInfo;)I

    move-result v21

    move/from16 v0, v21

    iput v0, v7, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->circlesColumnWidth:I

    new-instance v6, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sCircleCursorColumnNames:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$300()[Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v6, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    if-eqz v4, :cond_7

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v21

    if-eqz v21, :cond_7

    :cond_4
    invoke-interface {v4}, Landroid/database/Cursor;->getPosition()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    if-nez v17, :cond_5

    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    :cond_5
    const/16 v22, 0x0

    aget-object v22, v17, v22

    if-nez v22, :cond_11

    const/16 v22, 0x0

    aput-object v21, v17, v22

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v11, v0, :cond_6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    const/16 v17, 0x0

    :cond_6
    :goto_3
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v21

    if-nez v21, :cond_4

    :cond_7
    if-eqz v17, :cond_14

    const/4 v8, 0x1

    :goto_4
    if-ge v8, v11, :cond_13

    aget-object v21, v17, v8

    if-nez v21, :cond_8

    const/16 v21, -0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v17, v8

    :cond_8
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    :cond_9
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;->listResponse:Ljava/util/List;

    move-object/from16 v16, v0

    const/4 v13, 0x0

    const/16 v18, 0x0

    if-nez v16, :cond_a

    const/4 v5, 0x0

    :goto_5
    const/4 v8, 0x0

    move/from16 v19, v18

    move v14, v13

    :goto_6
    if-ge v8, v5, :cond_e

    move-object/from16 v0, v16

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/api/services/plusi/model/ListResponse;

    iget-object v0, v15, Lcom/google/api/services/plusi/model/ListResponse;->people:Ljava/util/List;

    move-object/from16 v21, v0

    if-eqz v21, :cond_15

    iget-object v0, v15, Lcom/google/api/services/plusi/model/ListResponse;->people:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v21

    if-eqz v21, :cond_15

    const-string v21, "FRIEND_ADDS"

    iget-object v0, v15, Lcom/google/api/services/plusi/model/ListResponse;->listType:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_b

    const/16 v21, 0x1

    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v6, v12, v0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->addSuggestionItem(Lcom/google/android/apps/plus/phone/EsMatrixCursor;I[Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v17

    move/from16 v18, v19

    move v13, v14

    :goto_7
    add-int/lit8 v8, v8, 0x1

    move/from16 v19, v18

    move v14, v13

    goto :goto_6

    :cond_a
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v5

    goto :goto_5

    :cond_b
    const-string v21, "YOU_MAY_KNOW"

    iget-object v0, v15, Lcom/google/api/services/plusi/model/ListResponse;->listType:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_c

    const/16 v21, 0x2

    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v6, v12, v0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->addSuggestionItem(Lcom/google/android/apps/plus/phone/EsMatrixCursor;I[Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v17

    move/from16 v18, v19

    move v13, v14

    goto :goto_7

    :cond_c
    const-string v21, "ORGANIZATION"

    iget-object v0, v15, Lcom/google/api/services/plusi/model/ListResponse;->listType:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_d

    const/16 v21, 0x4

    add-int/lit8 v13, v14, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-static {v6, v12, v0, v1, v14}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->addSuggestionItem(Lcom/google/android/apps/plus/phone/EsMatrixCursor;I[Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v17

    move/from16 v18, v19

    goto :goto_7

    :cond_d
    const-string v21, "SCHOOL"

    iget-object v0, v15, Lcom/google/api/services/plusi/model/ListResponse;->listType:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_15

    const/16 v21, 0x3

    add-int/lit8 v18, v19, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v21

    move/from16 v2, v19

    invoke-static {v6, v12, v0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->addSuggestionItem(Lcom/google/android/apps/plus/phone/EsMatrixCursor;I[Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v17

    move v13, v14

    goto :goto_7

    :cond_e
    if-eqz v3, :cond_f

    const/16 v21, 0x6

    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v6, v12, v0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->addSuggestionItem(Lcom/google/android/apps/plus/phone/EsMatrixCursor;I[Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v17

    :cond_f
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mIsLoggedInAsPlusPage:Z

    move/from16 v21, v0

    if-nez v21, :cond_10

    const/16 v21, 0x5

    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v6, v12, v0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->addSuggestionItem(Lcom/google/android/apps/plus/phone/EsMatrixCursor;I[Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v17

    :cond_10
    if-eqz v17, :cond_3

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    const/16 v17, 0x0

    goto/16 :goto_2

    :cond_11
    const/16 v22, 0x1

    aget-object v22, v17, v22

    if-nez v22, :cond_12

    const/16 v22, 0x1

    aput-object v21, v17, v22

    const/16 v21, 0x2

    move/from16 v0, v21

    if-ne v11, v0, :cond_6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    const/16 v17, 0x0

    goto/16 :goto_3

    :cond_12
    const/16 v22, 0x2

    aput-object v21, v17, v22

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    const/16 v17, 0x0

    goto/16 :goto_3

    :cond_13
    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_14
    iput-object v6, v7, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->circlesLayoutCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    iput-object v4, v7, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->circlesData:Landroid/database/Cursor;

    goto/16 :goto_1

    :cond_15
    move/from16 v18, v19

    move v13, v14

    goto/16 :goto_7
.end method

.method private static getColumnWidth(ILcom/google/android/apps/plus/util/StreamLayoutInfo;)I
    .locals 3
    .param p0    # I
    .param p1    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    add-int/lit8 v1, p0, -0x1

    iget v2, p1, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->separatorWidth:I

    mul-int v0, v1, v2

    iget v1, p1, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamWidth:I

    sub-int/2addr v1, v0

    div-int/2addr v1, p0

    return v1
.end method

.method private getNumColumnsGivenMaxWidth(IILcom/google/android/apps/plus/util/StreamLayoutInfo;)I
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1, p3}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->getColumnWidth(ILcom/google/android/apps/plus/util/StreamLayoutInfo;)I

    move-result v0

    if-eq v1, p1, :cond_0

    if-gt v0, p2, :cond_1

    :cond_0
    return v1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->deliverResult(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;)V

    return-void
.end method

.method public final bridge synthetic esLoadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->esLoadInBackground()Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    move-result-object v0

    return-object v0
.end method

.method protected final onReset()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;->onReset()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->cancelLoad()Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mPeopleData:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mObserverRegistered:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mObserverRegistered:Z

    :cond_0
    return-void
.end method

.method protected final onStartLoading()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mObserverRegistered:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mObserverRegistered:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mPeopleData:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mPeopleData:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->deliverResult(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->forceLoad()V

    goto :goto_0
.end method

.method protected final onStopLoading()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->cancelLoad()Z

    return-void
.end method

.method public final setCelebrities(Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;)V
    .locals 1
    .param p1    # Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mCelebrities:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mCelebritiesLoaded:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final setCircles(Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mCirclesCursor:Landroid/database/Cursor;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mCirclesCursorLoaded:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final setPeopleSuggestions(Lcom/google/api/services/plusi/model/PeopleViewDataResponse;)V
    .locals 2
    .param p1    # Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;->listResponse:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;->listResponse:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_0
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mSuggestedPeople:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->mSuggestedPeopleLoaded:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
