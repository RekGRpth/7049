.class final Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;
.super Ljava/lang/Object;
.source "HostedPeopleFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CompositeLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;-><init>(Landroid/content/Context;Z)V

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    # setter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPeopleData:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$402(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;)Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mListViewAdapter:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$500(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->setHostedPeopleData(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mListViewAdapter:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$500(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->setHostedPeopleData(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;)V

    return-void
.end method
