.class final Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleViewLoaderCallbacks;
.super Ljava/lang/Object;
.source "HostedPeopleFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PeopleViewLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/google/api/services/plusi/model/PeopleViewDataResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleViewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleViewLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/api/services/plusi/model/PeopleViewDataResponse;",
            ">;"
        }
    .end annotation

    const/16 v2, 0xa

    const/4 v1, 0x0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Lcom/google/api/services/plusi/model/DataRequestParameter;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/DataRequestParameter;-><init>()V

    const-string v0, "FRIEND_ADDS"

    iput-object v0, v6, Lcom/google/api/services/plusi/model/DataRequestParameter;->listType:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Lcom/google/api/services/plusi/model/DataRequestParameter;->maxResults:Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lcom/google/api/services/plusi/model/DataRequestParameter;->preferPhotos:Ljava/lang/Boolean;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v6, Lcom/google/api/services/plusi/model/DataRequestParameter;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/DataRequestParameter;-><init>()V

    const-string v0, "YOU_MAY_KNOW"

    iput-object v0, v6, Lcom/google/api/services/plusi/model/DataRequestParameter;->listType:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Lcom/google/api/services/plusi/model/DataRequestParameter;->maxResults:Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lcom/google/api/services/plusi/model/DataRequestParameter;->preferPhotos:Ljava/lang/Boolean;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v6, Lcom/google/api/services/plusi/model/DataRequestParameter;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/DataRequestParameter;-><init>()V

    const-string v0, "ORGANIZATION"

    iput-object v0, v6, Lcom/google/api/services/plusi/model/DataRequestParameter;->listType:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Lcom/google/api/services/plusi/model/DataRequestParameter;->maxResults:Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lcom/google/api/services/plusi/model/DataRequestParameter;->preferPhotos:Ljava/lang/Boolean;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v6, Lcom/google/api/services/plusi/model/DataRequestParameter;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/DataRequestParameter;-><init>()V

    const-string v0, "SCHOOL"

    iput-object v0, v6, Lcom/google/api/services/plusi/model/DataRequestParameter;->listType:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Lcom/google/api/services/plusi/model/DataRequestParameter;->maxResults:Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lcom/google/api/services/plusi/model/DataRequestParameter;->preferPhotos:Ljava/lang/Boolean;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleViewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleViewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    const-wide/32 v3, 0x7fffffff

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/util/List;)V

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleViewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->setPeopleSuggestions(Lcom/google/api/services/plusi/model/PeopleViewDataResponse;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->forceLoad()V

    :cond_0
    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/api/services/plusi/model/PeopleViewDataResponse;",
            ">;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleViewLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->setPeopleSuggestions(Lcom/google/api/services/plusi/model/PeopleViewDataResponse;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;->forceLoad()V

    :cond_0
    return-void
.end method
