.class public Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedPeopleFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog$CirclePropertiesListener;
.implements Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout$OnClickListener;
.implements Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;,
        Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;,
        Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoader;,
        Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;,
        Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CelebritiesLoaderCallbacks;,
        Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CirclesLoaderCallbacks;,
        Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleViewLoaderCallbacks;
    }
.end annotation


# static fields
.field private static sAddCircleHeight:I

.field private static sAddCircleTextSize:I

.field private static sAddCircleTopPadding:I

.field private static sCircleCursorColumnNames:[Ljava/lang/String;

.field protected static sListViewCacheColorHint:I

.field private static final sNewCircleCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

.field private static sNewCircleCursorColumnNames:[Ljava/lang/String;

.field private static sSuggestionsCursorColumnNames:[Ljava/lang/String;

.field private static sSuggestionsMaxCardWidth:I

.field private static sYourCirclesLeftPadding:I

.field private static sYourCirclesMaxCardWidth:I

.field private static sYourCirclesTextSize:I

.field private static sYourCirclesTopPadding:I


# instance fields
.field final mCelebritiesLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CelebritiesLoaderCallbacks;

.field private final mCircleContentObserver:Landroid/database/DataSetObserver;

.field private mCircleName:Ljava/lang/String;

.field private mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field final mCirclesLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CirclesLoaderCallbacks;

.field final mCompositeLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;

.field mFindCurrentNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field private mIsNew:Z

.field private mLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;

.field private mListView:Landroid/widget/ListView;

.field private mListViewAdapter:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;

.field protected mNewCircleRequestId:Ljava/lang/Integer;

.field protected mPendingRequestId:Ljava/lang/Integer;

.field private mPeopleData:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

.field private final mPeopleSuggestionLogHelper:Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field final mSuggestionsLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleViewLoaderCallbacks;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "type_1"

    aput-object v1, v0, v2

    const-string v1, "data_1"

    aput-object v1, v0, v3

    const-string v1, "type_2"

    aput-object v1, v0, v4

    const-string v1, "data_2"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sSuggestionsCursorColumnNames:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "index_1"

    aput-object v1, v0, v2

    const-string v1, "index_2"

    aput-object v1, v0, v3

    const-string v1, "index_3"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sCircleCursorColumnNames:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "dummy"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sNewCircleCursorColumnNames:[Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v1, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sNewCircleCursorColumnNames:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sNewCircleCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleContentObserver:Landroid/database/DataSetObserver;

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleViewLoaderCallbacks;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleViewLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionsLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleViewLoaderCallbacks;

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CirclesLoaderCallbacks;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CirclesLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CirclesLoaderCallbacks;

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CelebritiesLoaderCallbacks;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CelebritiesLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCelebritiesLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CelebritiesLoaderCallbacks;

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCompositeLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;

    invoke-direct {v0}, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPeopleSuggestionLogHelper:Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mIsNew:Z

    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sSuggestionsMaxCardWidth:I

    return v0
.end method

.method static synthetic access$100()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sSuggestionsCursorColumnNames:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sAddCircleHeight:I

    return v0
.end method

.method static synthetic access$1100()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sYourCirclesLeftPadding:I

    return v0
.end method

.method static synthetic access$1200()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sYourCirclesTopPadding:I

    return v0
.end method

.method static synthetic access$1300()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sYourCirclesTextSize:I

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/fragments/CircleNameResolver;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->handleNewCircleCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$200()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sYourCirclesMaxCardWidth:I

    return v0
.end method

.method static synthetic access$300()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sCircleCursorColumnNames:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;)Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;
    .param p1    # Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPeopleData:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mListViewAdapter:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;

    return-object v0
.end method

.method static synthetic access$600()Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sNewCircleCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    return-object v0
.end method

.method static synthetic access$700()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sAddCircleTextSize:I

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    return-object v0
.end method

.method static synthetic access$900()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sAddCircleTopPadding:I

    return v0
.end method

.method private static configureStartEndYear(Lcom/google/api/services/plusi/model/ListResponse;Lcom/google/api/services/plusi/model/DateInfo;)V
    .locals 3
    .param p0    # Lcom/google/api/services/plusi/model/ListResponse;
    .param p1    # Lcom/google/api/services/plusi/model/DateInfo;

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->startYear:Ljava/lang/Integer;

    :cond_0
    iget-object v0, p1, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->endYear:Ljava/lang/Integer;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p1, Lcom/google/api/services/plusi/model/DateInfo;->current:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->endYear:Ljava/lang/Integer;

    goto :goto_0
.end method

.method private createNamesList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 6
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPeopleData:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    iget-object v5, v5, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->suggestionsData:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    iget-object v4, v5, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;->listResponse:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/ListResponse;

    iget-object v5, v3, Lcom/google/api/services/plusi/model/ListResponse;->listType:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, v3, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->name:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private dismissProgressDialog()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private handleNewCircleCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 5
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq p1, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->dismissProgressDialog()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    sget v1, Lcom/google/android/apps/plus/R$string;->toast_new_circle_created:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private logViewAllAction()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getExtrasForLogging(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->MIXIN_PEOPLE_VIEW_ALL_SUGGESTIONS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v3, v4, v2, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    return-void
.end method

.method private showCircleMembershipDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/Intents;->getCircleMembershipActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private showProgressDialog(I)V
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_IN_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->dismissProgressDialog()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mListViewAdapter:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final onActionButtonClicked(I)V
    .locals 3
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getPeopleSearchIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 23
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super/range {p0 .. p3}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onActivityResult(IILandroid/content/Intent;)V

    return-void

    :pswitch_0
    const-string v2, "person_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v2, "display_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v2, "suggestion_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "original_circle_ids"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "selected_circle_ids"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mHandler:Landroid/os/Handler;

    move-object/from16 v22, v0

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$4;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :pswitch_1
    const-string v2, "profile_edit_mode"

    const/4 v3, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    const-string v2, "profile_edit_items_json"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const/16 v21, 0x0

    packed-switch v11, :pswitch_data_1

    :cond_1
    :goto_1
    if-eqz v21, :cond_2

    packed-switch v11, :pswitch_data_2

    :cond_2
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/SuggestionsFileCache;->clearAll(Landroid/content/Context;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v18

    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CirclesLoaderCallbacks;

    move-object/from16 v22, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v3, v1}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    const/4 v2, 0x3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCelebritiesLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CelebritiesLoaderCallbacks;

    move-object/from16 v22, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v3, v1}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    const/4 v2, 0x2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionsLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleViewLoaderCallbacks;

    move-object/from16 v22, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v3, v1}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    const/4 v2, 0x4

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCompositeLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;

    move-object/from16 v22, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v3, v1}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getView()Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->updateView(Landroid/view/View;)V

    goto/16 :goto_0

    :pswitch_2
    invoke-static {}, Lcom/google/api/services/plusi/model/EmploymentsJson;->getInstance()Lcom/google/api/services/plusi/model/EmploymentsJson;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/api/services/plusi/model/EmploymentsJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/api/services/plusi/model/Employments;

    const/16 v20, 0x0

    iget-object v2, v15, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, v15, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    const/16 v16, 0x0

    :goto_3
    move/from16 v0, v16

    if-ge v0, v9, :cond_3

    iget-object v2, v15, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    move/from16 v0, v16

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/api/services/plusi/model/Employment;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mFindCurrentNames:Ljava/util/ArrayList;

    iget-object v3, v14, Lcom/google/api/services/plusi/model/Employment;->employer:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    if-eqz v20, :cond_4

    const/16 v20, 0x0

    :cond_3
    if-eqz v20, :cond_1

    new-instance v21, Lcom/google/api/services/plusi/model/ListResponse;

    invoke-direct/range {v21 .. v21}, Lcom/google/api/services/plusi/model/ListResponse;-><init>()V

    const-string v2, "ORGANIZATION"

    move-object/from16 v0, v21

    iput-object v2, v0, Lcom/google/api/services/plusi/model/ListResponse;->listType:Ljava/lang/String;

    new-instance v2, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;-><init>()V

    move-object/from16 v0, v21

    iput-object v2, v0, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Employment;->employer:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->name:Ljava/lang/String;

    move-object/from16 v0, v20

    iget-object v10, v0, Lcom/google/api/services/plusi/model/Employment;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    move-object/from16 v0, v21

    invoke-static {v0, v10}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->configureStartEndYear(Lcom/google/api/services/plusi/model/ListResponse;Lcom/google/api/services/plusi/model/DateInfo;)V

    goto/16 :goto_1

    :cond_4
    move-object/from16 v20, v14

    :cond_5
    add-int/lit8 v16, v16, 0x1

    goto :goto_3

    :pswitch_3
    invoke-static {}, Lcom/google/api/services/plusi/model/EducationsJson;->getInstance()Lcom/google/api/services/plusi/model/EducationsJson;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/api/services/plusi/model/EducationsJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/api/services/plusi/model/Educations;

    const/16 v19, 0x0

    iget-object v2, v13, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, v13, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    const/16 v16, 0x0

    :goto_4
    move/from16 v0, v16

    if-ge v0, v9, :cond_6

    iget-object v2, v13, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    move/from16 v0, v16

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/api/services/plusi/model/Education;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mFindCurrentNames:Ljava/util/ArrayList;

    iget-object v3, v12, Lcom/google/api/services/plusi/model/Education;->school:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    if-eqz v19, :cond_7

    const/16 v19, 0x0

    :cond_6
    if-eqz v19, :cond_1

    new-instance v21, Lcom/google/api/services/plusi/model/ListResponse;

    invoke-direct/range {v21 .. v21}, Lcom/google/api/services/plusi/model/ListResponse;-><init>()V

    const-string v2, "ORGANIZATION"

    move-object/from16 v0, v21

    iput-object v2, v0, Lcom/google/api/services/plusi/model/ListResponse;->listType:Ljava/lang/String;

    new-instance v2, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;-><init>()V

    move-object/from16 v0, v21

    iput-object v2, v0, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Education;->school:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->name:Ljava/lang/String;

    move-object/from16 v0, v19

    iget-object v10, v0, Lcom/google/api/services/plusi/model/Education;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    move-object/from16 v0, v21

    invoke-static {v0, v10}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->configureStartEndYear(Lcom/google/api/services/plusi/model/ListResponse;Lcom/google/api/services/plusi/model/DateInfo;)V

    goto/16 :goto_1

    :cond_7
    move-object/from16 v19, v12

    :cond_8
    add-int/lit8 v16, v16, 0x1

    goto :goto_4

    :pswitch_4
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->onOrganizationViewAll(Lcom/google/api/services/plusi/model/ListResponse;)V

    goto/16 :goto_2

    :pswitch_5
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->onSchoolViewAll(Lcom/google/api/services/plusi/model/ListResponse;)V

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final onAddToCircleButtonClicked(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/FirstCircleAddDialogFactory;->needToShow(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v1, "person_id"

    invoke-virtual {v6, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "person_name"

    invoke-virtual {v6, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "for_sharing"

    invoke-virtual {v6, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "suggestion_id"

    invoke-virtual {v6, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "first_add"

    invoke-static {p0, v1, v6}, Lcom/google/android/apps/plus/fragments/FirstCircleAddDialogFactory;->show(Landroid/support/v4/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    if-nez p2, :cond_1

    const-string v8, "add_email_dialog"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    sget v1, Lcom/google/android/apps/plus/R$string;->add_email_dialog_title:I

    invoke-virtual {v4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/plus/R$string;->add_email_dialog_hint:I

    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x104000a

    invoke-virtual {v4, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/high16 v9, 0x1040000

    invoke-virtual {v4, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/EditFragmentDialog;->newInstance$405ed676(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/EditFragmentDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EditFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "person_id"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "for_sharing"

    invoke-virtual {v2, v3, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v3, "person_suggestion_id"

    invoke-virtual {v2, v3, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, p0, v5}, Lcom/google/android/apps/plus/fragments/EditFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v1, v2, v8}, Lcom/google/android/apps/plus/fragments/EditFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPeopleData:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    iget-object v1, v1, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->circlesData:Landroid/database/Cursor;

    invoke-static {v0, v1, p3}, Lcom/google/android/apps/plus/content/EsPeopleData;->getDefaultCircleId(Landroid/content/Context;Landroid/database/Cursor;Z)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, p1, p2, v7}, Lcom/google/android/apps/plus/service/EsService;->addPersonToCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v2, "ACCEPT"

    const-string v5, "ANDROID_PEOPLE_SUGGESTIONS_PAGE"

    move-object v3, p1

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->insertPeopleSuggestionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mListViewAdapter:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showCircleMembershipDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onAvatarClicked(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v2, "CLICK"

    const-string v5, "ANDROID_PEOPLE_SUGGESTIONS_PAGE"

    move-object v3, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->insertPeopleSuggestionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final onCircleClicked(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, p1, p2}, Lcom/google/android/apps/plus/phone/Intents;->getCirclePeopleActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onCirclePropertiesChange(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v5, 0x0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPeopleData:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    iget-object v3, v3, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->circlesData:Landroid/database/Cursor;

    if-eqz v3, :cond_4

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPeopleData:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    iget-object v3, v3, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->circlesData:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPeopleData:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    iget-object v3, v3, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->circlesData:Landroid/database/Cursor;

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPeopleData:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    iget-object v3, v3, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->circlesData:Landroid/database/Cursor;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v0, 0x1

    :cond_2
    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->toast_circle_already_exists:I

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPeopleData:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    iget-object v3, v3, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->circlesData:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_1

    :cond_4
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleName:Ljava/lang/String;

    sget v3, Lcom/google/android/apps/plus/R$string;->new_circle_operation_pending:I

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showProgressDialog(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v4, p2, p3}, Lcom/google/android/apps/plus/service/EsService;->createCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    sget v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sYourCirclesTextSize:I

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$dimen;->people_suggestions_max_card_width:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sSuggestionsMaxCardWidth:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->people_your_circles_text_size:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sYourCirclesTextSize:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->people_your_circles_top_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sYourCirclesTopPadding:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_border_left_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sYourCirclesLeftPadding:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->people_your_circles_max_card_width:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sYourCirclesMaxCardWidth:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->people_add_circle_text_size:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sAddCircleTextSize:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->people_add_circle_top_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sAddCircleTopPadding:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->people_add_circle_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sAddCircleHeight:I

    sget v2, Lcom/google/android/apps/plus/R$color;->profile_edit_bg:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sListViewCacheColorHint:I

    :cond_0
    if-eqz p1, :cond_3

    const-string v2, "request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_1
    const-string v2, "new_circle_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "new_circle_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    :cond_2
    const-string v2, "find_tracker"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "find_tracker"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mFindCurrentNames:Ljava/util/ArrayList;

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mIsNew:Z

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/SuggestionsFileCache;->clearAll(Landroid/content/Context;)V

    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mIsNew:Z

    :cond_4
    new-instance v2, Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/plus/util/StreamLayoutInfo;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, p0, v3, v5}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;Landroid/content/Context;B)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mListViewAdapter:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CirclesLoaderCallbacks;

    invoke-virtual {v0, v2, v4, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionsLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleViewLoaderCallbacks;

    invoke-virtual {v0, v2, v4, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCelebritiesLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CelebritiesLoaderCallbacks;

    invoke-virtual {v0, v2, v4, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCompositeLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CompositeLoaderCallbacks;

    invoke-virtual {v0, v2, v4, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    new-instance v2, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/16 v5, 0x10

    invoke-direct {v2, v3, v0, v4, v5}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleContentObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->registerObserver(Landroid/database/DataSetObserver;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v1, Lcom/google/android/apps/plus/R$layout;->hosted_people_fragment:I

    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$id;->people_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mListViewAdapter:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mListView:Landroid/widget/ListView;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mListView:Landroid/widget/ListView;

    sget v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sListViewCacheColorHint:I

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setCacheColorHint(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPeopleSuggestionLogHelper:Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->attachListener(Landroid/widget/AbsListView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->updateView(Landroid/view/View;)V

    return-object v0
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const-string v5, "add_email_dialog"

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "message"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "person_id"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v5, "for_sharing"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    const-string v5, "person_suggestion_id"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v1, v2, v0, v4}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->onAddToCircleButtonClicked(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v5, "first_add"

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "person_id"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v5, "person_name"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "for_sharing"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    const-string v5, "suggestion_id"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->onAddToCircleButtonClicked(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public final onFindClassmates()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const-string v1, "SCHOOL"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->createNamesList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mFindCurrentNames:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x2

    invoke-static {v1, v2, v3, v5, v5}, Lcom/google/android/apps/plus/phone/Intents;->getProfileEditActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "help_title"

    sget v2, Lcom/google/android/apps/plus/R$string;->find_people_classmates_help_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "help_desc"

    sget v2, Lcom/google/android/apps/plus/R$string;->find_people_classmates_help_details:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "profile_edit_return_json"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final onFindCoworkers()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v1, "ORGANIZATION"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->createNamesList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mFindCurrentNames:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, v3, v4, v4}, Lcom/google/android/apps/plus/phone/Intents;->getProfileEditActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "help_title"

    sget v2, Lcom/google/android/apps/plus/R$string;->find_people_coworkers_help_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "help_desc"

    sget v2, Lcom/google/android/apps/plus/R$string;->find_people_coworkers_help_details:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "profile_edit_return_json"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final onFollowInterestingPeople()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getCelebritySuggestionsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onFriendsAddViewAll()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getFriendsAddSuggestionsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startActivity(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->logViewAllAction()V

    return-void
.end method

.method public final onInACircleButtonClicked(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showCircleMembershipDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 9
    .param p1    # Landroid/view/MenuItem;

    const/4 v4, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v5, Lcom/google/android/apps/plus/R$id;->blocked_circle:I

    if-ne v3, v5, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v7, "15"

    sget v8, Lcom/google/android/apps/plus/R$string;->label_person_blocked:I

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v6, v7, v8}, Lcom/google/android/apps/plus/phone/Intents;->getCirclePeopleActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return v4

    :cond_0
    sget v5, Lcom/google/android/apps/plus/R$id;->help:I

    if-ne v3, v5, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->url_param_help_circles:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v5, v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startExternalActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final onOrganizationViewAll(Lcom/google/api/services/plusi/model/ListResponse;)V
    .locals 6
    .param p1    # Lcom/google/api/services/plusi/model/ListResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->name:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->startYear:Ljava/lang/Integer;

    iget-object v5, p1, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->endYear:Ljava/lang/Integer;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/phone/Intents;->getOrganizationSuggestionsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startActivity(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->logViewAllAction()V

    return-void
.end method

.method public final onPause()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->onPeopleListVisibilityChange(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPeopleSuggestionLogHelper:Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v3, "ANDROID_PEOPLE_SUGGESTIONS_PAGE"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->insertEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    sget v0, Lcom/google/android/apps/plus/R$string;->home_screen_people_label:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    const/4 v0, 0x0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_menu_search_holo_light:I

    sget v2, Lcom/google/android/apps/plus/R$string;->menu_search:I

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->blocked_circle:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void
.end method

.method public final onResume()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-static {v4}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->onPeopleListVisibilityChange(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->handleNewCircleCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesLoaderCallbacks:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$CirclesLoaderCallbacks;

    invoke-virtual {v0, v4, v3, v2}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    const-string v0, "new_circle_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mFindCurrentNames:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    const-string v0, "find_tracker"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mFindCurrentNames:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_2
    return-void
.end method

.method public final onSchoolViewAll(Lcom/google/api/services/plusi/model/ListResponse;)V
    .locals 6
    .param p1    # Lcom/google/api/services/plusi/model/ListResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->name:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->startYear:Ljava/lang/Integer;

    iget-object v5, p1, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->endYear:Ljava/lang/Integer;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/phone/Intents;->getSchoolSuggestionsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startActivity(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->logViewAllAction()V

    return-void
.end method

.method public final onYouMayKnowViewAll()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getYouMayKnowSuggestionsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startActivity(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->logViewAllAction()V

    return-void
.end method

.method protected final setCircleMembership(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {p4, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {p5, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-static {v7, v8}, Lcom/google/android/apps/plus/content/EsPeopleData;->getMembershipChangeMessageId(Ljava/util/List;Ljava/util/List;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showProgressDialog(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v7, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v8, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->setCircleMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v2, "ACCEPT"

    const-string v5, "ANDROID_PEOPLE_SUGGESTIONS_PAGE"

    move-object v3, p1

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->insertPeopleSuggestionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final shouldPersistStateToHost()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final showContent(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showContent(Landroid/view/View;)V

    return-void
.end method

.method protected final updateView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mListViewAdapter:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->isProgressIndicatorVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showEmptyViewProgress(Landroid/view/View;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->updateSpinner()V

    return-void

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$string;->loading_friend_suggestions:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showContent(Landroid/view/View;)V

    goto :goto_0
.end method
