.class final Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$4;
.super Ljava/lang/Object;
.source "HostedPhotosFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemDeselected(Landroid/view/View;I)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    :cond_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$200(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getMediaRefForItem(I)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$400(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->invalidateContextualActionBar()V
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$500(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V

    return-void
.end method

.method public final onItemSelected(Landroid/view/View;I)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    :cond_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$200(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getMediaRefForItem(I)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    if-eqz p1, :cond_1

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$400(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->invalidateContextualActionBar()V
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$500(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V

    return-void
.end method
