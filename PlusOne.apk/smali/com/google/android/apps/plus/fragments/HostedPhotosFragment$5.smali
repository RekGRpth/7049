.class final Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$5;
.super Ljava/lang/Object;
.source "HostedPhotosFragment.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$5;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/MenuItem;

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->reshare:I

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$5;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$400(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$5;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->shareSelectedPhotos()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$5;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$700(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ActionMode;->finish()V

    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_1
    sget v1, Lcom/google/android/apps/plus/R$id;->delete_photos:I

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$5;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->showDeleteConfirmationDialog()V
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$800(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$menu;->photos_cab_menu:I

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v1, 0x1

    return v1
.end method

.method public final onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2
    .param p1    # Landroid/view/ActionMode;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$5;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$702(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$5;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$900(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;I)V

    return-void
.end method

.method public final onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 9
    .param p1    # Landroid/view/ActionMode;
    .param p2    # Landroid/view/Menu;

    const/4 v8, 0x0

    const/4 v7, 0x1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$5;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$600(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)I

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$5;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionMode:Landroid/view/ActionMode;
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$700(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Landroid/view/ActionMode;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ActionMode;->finish()V

    :goto_0
    return v7

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$5;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$5;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$400(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v0

    sget v4, Lcom/google/android/apps/plus/R$plurals;->from_your_phone_selected_count:I

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v2, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    sget v4, Lcom/google/android/apps/plus/R$id;->reshare:I

    invoke-interface {p2, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$id;->delete_photos:I

    invoke-interface {p2, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-nez v0, :cond_1

    invoke-interface {v3, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v1, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    invoke-interface {v3, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v4, Lcom/google/android/apps/plus/R$plurals;->delete_photo:I

    invoke-virtual {v2, v4, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0
.end method
