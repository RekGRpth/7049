.class public Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedPhotosFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;
.implements Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;
.implements Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$AlbumDetailsQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/View$OnLongClickListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;",
        "Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;",
        "Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;"
    }
.end annotation


# instance fields
.field private mActionMode:Landroid/view/ActionMode;

.field private mActionModeCallback:Landroid/view/ActionMode$Callback;

.field private mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

.field private mAlbumCount:I

.field private mAlbumId:Ljava/lang/String;

.field private mAlbumName:Ljava/lang/String;

.field private mAlbumType:Ljava/lang/String;

.field private mAuthkey:Ljava/lang/String;

.field private mCount:I

.field private mDateFormat:Ljava/text/DateFormat;

.field private mDeleteReqId:Ljava/lang/Integer;

.field private final mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mEventId:Ljava/lang/String;

.field private mExcludedCount:I

.field private final mExcludedPhotoMediaRefs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private mExtras:Landroid/os/Bundle;

.field private mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field private mHereFromNotification:Z

.field private mHideParentActionBarItems:Z

.field private mLastNotificationTime:J

.field private mLoaderActive:Z

.field private mNotificationId:Ljava/lang/String;

.field private mOwnerId:Ljava/lang/String;

.field private mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

.field private mPhotoOfUserId:Ljava/lang/String;

.field private mPickerMode:I

.field private mPickerShareWithZeroSelected:Z

.field private mPickerTitleResourceId:I

.field private mRefreshReqId:Ljava/lang/Integer;

.field private mRefreshable:Z

.field private final mSelectedPhotoMediaRefs:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private mStreamId:Ljava/lang/String;

.field private mTakePhoto:Z

.field private mTakeVideo:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumCount:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExcludedPhotoMediaRefs:Ljava/util/ArrayList;

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDateFormat:Ljava/text/DateFormat;

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->handlePhotoDelete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Ljava/text/DateFormat;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDateFormat:Ljava/text/DateFormat;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Ljava/util/HashSet;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->invalidateContextualActionBar()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Landroid/view/ActionMode;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;
    .param p1    # Landroid/view/ActionMode;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->showDeleteConfirmationDialog()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;I)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;
    .param p1    # I

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updatePickerMode(I)V

    return-void
.end method

.method private handlePhotoDelete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 5
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$string;->remove_photo_error:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "progress_dialog"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/DialogFragment;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_3
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updatePickerMode(I)V

    goto :goto_0
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$string;->refresh_photo_album_error:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getView()Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private invalidateContextualActionBar()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->invalidateActionBar()V

    goto :goto_0
.end method

.method private isInstantUploadAlbum()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumType:Ljava/lang/String;

    const-string v1, "from_my_phone"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private isLocalCameraAlbum()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumType:Ljava/lang/String;

    const-string v1, "camera_photos"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private isShareableAlbum()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "hide_share_action"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "hide_share_action"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mStreamId:Ljava/lang/String;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumId:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private loadAlbumName()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumId:Ljava/lang/String;

    if-eqz v5, :cond_1

    move v0, v3

    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    iget v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumCount:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_2

    move v1, v3

    :goto_1
    if-eqz v0, :cond_3

    if-nez v2, :cond_0

    if-eqz v1, :cond_3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    invoke-virtual {v3, v7, v8, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->invalidateActionBar()V

    return-void

    :cond_1
    move v0, v4

    goto :goto_0

    :cond_2
    move v1, v4

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    invoke-virtual {v3, v7, v8, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_2
.end method

.method private showDeleteConfirmationDialog()V
    .locals 8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isLocalCameraAlbum()Z

    move-result v4

    if-eqz v4, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$plurals;->delete_local_photo_dialog_message:I

    :goto_0
    sget v4, Lcom/google/android/apps/plus/R$plurals;->delete_photo_dialog_title:I

    invoke-virtual {v3, v4, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$plurals;->delete_photo:I

    invoke-virtual {v3, v6, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, p0, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "delete_dialog"

    invoke-virtual {v1, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_0
    sget v2, Lcom/google/android/apps/plus/R$plurals;->delete_remote_photo_dialog_message:I

    goto :goto_0
.end method

.method private updatePickerMode(I)V
    .locals 2
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->invalidateContextualActionBar()V

    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->isInSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->endSelectionMode()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->isInSelectionMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->startSelectionMode()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->isInSelectionMode()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->startSelectionMode()V

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionMode:Landroid/view/ActionMode;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionMode:Landroid/view/ActionMode;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateView(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_2

    move v1, v4

    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-nez v6, :cond_1

    if-nez v0, :cond_3

    :cond_1
    move v3, v4

    :goto_2
    if-eqz v3, :cond_4

    if-nez v1, :cond_4

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->showEmptyViewProgress(Landroid/view/View;)V

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateSpinner()V

    goto :goto_0

    :cond_2
    move v1, v5

    goto :goto_1

    :cond_3
    move v3, v5

    goto :goto_2

    :cond_4
    if-eqz v1, :cond_5

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->showContent(Landroid/view/View;)V

    goto :goto_3

    :cond_5
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExcludedPhotoMediaRefs:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_6

    move v2, v4

    :goto_4
    if-eqz v2, :cond_7

    sget v4, Lcom/google/android/apps/plus/R$string;->no_photos_left:I

    :goto_5
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p1, v4}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    move v2, v5

    goto :goto_4

    :cond_7
    sget v4, Lcom/google/android/apps/plus/R$string;->no_photos:I

    goto :goto_5
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->isEmpty()Z

    move-result v0

    goto :goto_0
.end method

.method protected final isProgressIndicatorVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLoaderActive:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->isProgressIndicatorVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActionButtonClicked(I)V
    .locals 5
    .param p1    # I

    const/4 v1, 0x3

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mHereFromNotification:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->shareSelectedPhotos()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-string v3, "mediarefs"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    const-string v0, "camera-p.jpg"

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/Intents;->getCameraIntentPhoto$3a35108a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    invoke-static {}, Lcom/google/android/apps/plus/phone/Intents;->getCameraIntentVideo$7ec49240()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getMediaRefForItem(I)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    invoke-static {v1, v2, v0, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getAlbumPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/MediaRef;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 11
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v3, "camera-p.jpg"

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->insertCameraPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    const-string v1, "insert_photo_request_id"

    invoke-virtual {v10, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "media_taken"

    const/4 v2, 0x1

    invoke-virtual {v10, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    new-instance v8, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-string v1, "mediarefs"

    invoke-virtual {v10, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {v7, v1, v10}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v7}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    if-eqz p3, :cond_0

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    new-instance v8, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-string v1, "mediarefs"

    invoke-virtual {v10, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "media_taken"

    const/4 v2, 0x1

    invoke-virtual {v10, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "insert_photo_request_id"

    invoke-virtual {v10, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const/4 v1, -0x1

    invoke-virtual {v7, v1, v10}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v7}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 16
    .param p1    # Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "mediarefs"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "mediarefs"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, [Lcom/google/android/apps/plus/api/MediaRef;

    move-object v8, v1

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "album_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "album_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v9, v1

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "stream_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "stream_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v10, v1

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "photos_of_user_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "photos_of_user_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v11, v1

    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "photo_picker_mode"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "photo_picker_crop_mode"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    sget v1, Lcom/google/android/apps/plus/R$id;->tag_position:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v14

    invoke-interface {v14, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    const/16 v1, 0x8

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v1, 0x5

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v1, 0x9

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v1, 0xc

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_4
    if-eqz v1, :cond_5

    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->PANORAMA:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumType:Ljava/lang/String;

    const-string v5, "camera_photos"

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    if-nez v8, :cond_7

    new-instance v1, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v5, 0x0

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    const/4 v3, 0x1

    new-array v8, v3, [Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v3, 0x0

    aput-object v1, v8, v3

    move-object v3, v1

    :goto_6
    if-eqz v12, :cond_8

    const/4 v1, 0x7

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v4, v1, v3, v13}, Lcom/google/android/apps/plus/phone/Intents;->getPhotoPickerIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;I)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_7
    return-void

    :cond_0
    const/4 v1, 0x0

    move-object v8, v1

    goto/16 :goto_0

    :cond_1
    const/4 v1, 0x0

    move-object v9, v1

    goto/16 :goto_1

    :cond_2
    const/4 v1, 0x0

    move-object v10, v1

    goto/16 :goto_2

    :cond_3
    const/4 v1, 0x0

    move-object v11, v1

    goto/16 :goto_3

    :cond_4
    const/4 v1, 0x0

    goto :goto_4

    :cond_5
    const/16 v1, 0xb

    invoke-interface {v14, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_6

    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_5

    :cond_6
    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_5

    :cond_7
    new-instance v1, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v6, 0x0

    move-object v5, v15

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    move-object v3, v1

    goto :goto_6

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v1

    instance-of v4, v1, Lcom/google/android/apps/plus/phone/Pageable;

    if-eqz v4, :cond_a

    check-cast v1, Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v1}, Lcom/google/android/apps/plus/phone/Pageable;->getCurrentPage()I

    move-result v1

    :goto_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoOneUpActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setMediaRefs([Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoOfUserId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEventId:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setEventId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPageHint(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mHereFromNotification:Z

    if-eqz v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mNotificationId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mNotificationId:Ljava/lang/String;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setNotificationId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    :cond_9
    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_7

    :cond_a
    const/4 v1, -0x1

    goto :goto_8
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    if-eqz p1, :cond_14

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "INTENT"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    const-string v3, "ALBUM_NAME"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "ALBUM_NAME"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    :cond_0
    const-string v3, "STATE_PICKER_MODE"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "STATE_PICKER_MODE"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v3, "STATE_PICKER_TITLE"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerTitleResourceId:I

    const-string v3, "STATE_PICKER_SHARE_ON_ZERO"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerShareWithZeroSelected:Z

    :cond_1
    const-string v3, "SELECTED_ITEMS"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "SELECTED_ITEMS"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    const/4 v1, 0x0

    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    aget-object v3, v2, v1

    check-cast v3, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v5, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const-string v3, "refresh_request"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "refresh_request"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    :cond_3
    const-string v3, "delete_request"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "delete_request"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    :cond_4
    const-string v3, "loader_active"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "loader_active"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLoaderActive:Z

    :cond_5
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "owner_id"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "owner_id"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "album_name"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "album_name"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    :cond_7
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "album_id"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "album_id"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumId:Ljava/lang/String;

    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "auth_key"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "auth_key"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAuthkey:Ljava/lang/String;

    :cond_9
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "album_type"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "album_type"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumType:Ljava/lang/String;

    :cond_a
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "stream_id"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "stream_id"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mStreamId:Ljava/lang/String;

    :cond_b
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "event_id"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "event_id"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEventId:Ljava/lang/String;

    :cond_c
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "photos_of_user_id"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "photos_of_user_id"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPhotoOfUserId:Ljava/lang/String;

    :cond_d
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "notif_id"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mNotificationId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mNotificationId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_15

    const/4 v3, 0x1

    :goto_2
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mHereFromNotification:Z

    if-nez v0, :cond_e

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "photo_picker_mode"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "photo_picker_mode"

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :cond_e
    if-eqz v0, :cond_f

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    :cond_f
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v4, "photo_picker_title"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v4, "photo_picker_title"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerTitleResourceId:I

    :cond_10
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v4, "photo_picker_share_on_zero"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v4, "photo_picker_share_on_zero"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerShareWithZeroSelected:Z

    :cond_11
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v4, "take_photo"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v4, "take_photo"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mTakePhoto:Z

    :cond_12
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v4, "take_video"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v4, "take_video"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mTakeVideo:Z

    :cond_13
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v4, "photo_picker_selected"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_16

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v4, "photo_picker_selected"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    const/4 v1, 0x0

    :goto_3
    array-length v3, v2

    if-ge v1, v3, :cond_16

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExcludedPhotoMediaRefs:Ljava/util/ArrayList;

    aget-object v3, v2, v1

    check-cast v3, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_14
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/content/EsAccountsData;->queryLastPhotoNotificationTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLastNotificationTime:J

    iget-wide v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLastNotificationTime:J

    const-wide/16 v7, 0x0

    cmp-long v3, v5, v7

    if-gez v3, :cond_5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const-wide/32 v7, 0xdbba00

    sub-long/2addr v5, v7

    iput-wide v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLastNotificationTime:J

    goto/16 :goto_1

    :cond_15
    move v3, v4

    goto/16 :goto_2

    :cond_16
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->loadAlbumName()V

    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 12
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x0

    packed-switch p1, :pswitch_data_0

    move-object v0, v5

    :goto_0
    return-object v0

    :pswitch_0
    const/4 v9, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExcludedPhotoMediaRefs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExcludedPhotoMediaRefs:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExcludedPhotoMediaRefs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lcom/google/android/apps/plus/api/MediaRef;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isLocalCameraAlbum()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "hide_camera_videos"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    new-instance v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v0, v1, v2, v9, v11}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Lcom/google/android/apps/plus/api/MediaRef;Z)V

    :goto_1
    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/plus/phone/Pageable;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v1, p0}, Lcom/google/android/apps/plus/phone/Pageable;->setLoadingListener(Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/plus/phone/AlbumViewLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPhotoOfUserId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mStreamId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEventId:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAuthkey:Ljava/lang/String;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/phone/AlbumViewLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lcom/google/android/apps/plus/api/MediaRef;)V

    goto :goto_1

    :pswitch_1
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_BY_ALBUM_AND_OWNER_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v10, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_ALBUM_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    new-instance v1, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget-object v4, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$AlbumDetailsQuery;->PROJECTION:[Ljava/lang/String;

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v0, Lcom/google/android/apps/plus/R$layout;->hosted_album_view:I

    invoke-super {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/plus/views/PhotoAlbumView;

    sget v0, Lcom/google/android/apps/plus/R$id;->grid:I

    invoke-virtual {v9, v0}, Lcom/google/android/apps/plus/views/PhotoAlbumView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ColumnGridView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$dimen;->album_photo_grid_spacing:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v11

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v11, v11, v11, v11}, Lcom/google/android/apps/plus/views/ColumnGridView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/views/ColumnGridView;->setItemMargin(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getPhotoColumns(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    new-instance v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumType:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    move-object v5, p0

    move-object v6, p0

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;Lcom/google/android/apps/plus/views/ColumnGridView;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->setSelectedMediaRefs(Ljava/util/HashSet;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "photo_picker_crop_mode"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v10

    const/4 v0, 0x3

    if-eq v10, v0, :cond_0

    const/4 v0, 0x2

    if-ne v10, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$2;

    invoke-direct {v2, p0, v10}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;I)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->setStateFilter(Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$StateFilter;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->list_selected_holo:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelector(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updatePickerMode(I)V

    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateView(Landroid/view/View;)V

    sget v0, Lcom/google/android/apps/plus/R$string;->no_photos:I

    invoke-static {v9, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->setupEmptyView(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$3;

    invoke-direct {v2, p0, v9}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;Lcom/google/android/apps/plus/views/PhotoAlbumView;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOnScrollListener(Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$4;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->registerSelectionListener(Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;)V

    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Lcom/google/android/apps/plus/views/PhotoAlbumView;->enableDateDisplay(Z)V

    sget v0, Lcom/google/android/apps/plus/R$id;->acl_display:I

    invoke-virtual {v9, v0}, Lcom/google/android/apps/plus/views/PhotoAlbumView;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setVisibility(I)V

    const-string v0, "camerasync"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mStreamId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/apps/plus/R$string;->acl_private_album:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    return-object v9
.end method

.method public final onDataSourceLoading(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLoaderActive:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateSpinner()V

    return-void
.end method

.method public final onDestroyView()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onDestroyView()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->unregisterSelectionListener()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOnScrollListener(Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;)V

    return-void
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const-string v0, "delete_dialog"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isLocalCameraAlbum()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/service/EsService;->deleteLocalPhotos(Landroid/content/Context;Ljava/util/ArrayList;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$plurals;->delete_photo_pending:I

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "progress_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v0, "HostedPhotosFragment"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedPhotosFragment"

    const-string v1, "Found a photo from phone which is not owned by the current user."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v3, v2, v1}, Lcom/google/android/apps/plus/service/EsService;->deletePhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public final onDoneButtonClick()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updatePickerMode(I)V

    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 9
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v8, 0x3

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, -0x1

    const/4 v7, 0x0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->loadAlbumName()V

    iput v7, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExcludedCount:I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    instance-of v0, v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    check-cast v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->getExcludedCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExcludedCount:I

    :cond_1
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExcludedCount:I

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->refresh()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "photo_picker_crop_mode"

    invoke-virtual {v0, v2, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/google/android/apps/plus/R$id;->message:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    if-eq v2, v8, :cond_3

    if-ne v2, v5, :cond_4

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->isAnyPhotoDisabled()Z

    move-result v2

    if-eqz v2, :cond_4

    sget v2, Lcom/google/android/apps/plus/R$string;->photo_picker_album_message_cover_photo:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mHereFromNotification:Z

    if-eqz v0, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLastNotificationTime:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEventId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLastNotificationTime:J

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getTimestampForItem(I)J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v6, v4, v5}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveLastPhotoNotificationTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLastNotificationTime:J

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getMediaRefForItem(I)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    move v0, v1

    :goto_2
    if-ge v0, v4, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getTimestampForItem(I)J

    move-result-wide v5

    cmp-long v1, v5, v2

    if-lez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getMediaRefForItem(I)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    instance-of v0, v0, Lcom/google/android/apps/plus/phone/AlbumViewLoader;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    check-cast v0, Lcom/google/android/apps/plus/phone/AlbumViewLoader;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/AlbumViewLoader;->getExcludedCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExcludedCount:I

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0, v8}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updatePickerMode(I)V

    goto/16 :goto_0

    :pswitch_1
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->invalidateActionBar()V

    :cond_7
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumCount:I

    if-ne v0, v2, :cond_8

    invoke-interface {p2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, -0x2

    :goto_3
    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumCount:I

    :cond_8
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumCount:I

    if-eq v0, v2, :cond_9

    const-string v0, "HostedPhotosFragment"

    const/4 v3, 0x4

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "HostedPhotosFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Size of album: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v5, :cond_b

    sget v0, Lcom/google/android/apps/plus/R$string;->acl_private_album:I

    move v1, v0

    :goto_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/google/android/apps/plus/R$id;->acl_display:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    if-eq v1, v2, :cond_0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_a
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_3

    :cond_b
    if-ne v0, v1, :cond_c

    sget v0, Lcom/google/android/apps/plus/R$string;->acl_limited_album:I

    move v1, v0

    goto :goto_4

    :cond_c
    if-nez v0, :cond_d

    sget v0, Lcom/google/android/apps/plus/R$string;->acl_public_album:I

    move v1, v0

    goto :goto_4

    :cond_d
    move v1, v2

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    if-nez v1, :cond_4

    const-string v1, "posts"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mStreamId:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "messenger"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mStreamId:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "profile"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mStreamId:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    move v1, v3

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isShareableAlbum()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isLocalCameraAlbum()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isInstantUploadAlbum()Z

    move-result v4

    if-nez v4, :cond_1

    if-eqz v1, :cond_3

    :cond_1
    move v1, v3

    :goto_1
    if-eqz v1, :cond_4

    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updatePickerMode(I)V

    sget v1, Lcom/google/android/apps/plus/R$id;->tag_position:I

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->select(I)V

    :goto_2
    return v3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_2
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->select_item:I

    if-ne v0, v1, :cond_0

    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updatePickerMode(I)V

    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    sget v1, Lcom/google/android/apps/plus/R$id;->delete_photos:I

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->showDeleteConfirmationDialog()V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final onPause()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/phone/Pageable;->setLoadingListener(Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;)V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    invoke-virtual {p1, p0}, Lcom/google/android/apps/plus/views/HostActionBar;->setOnDoneButtonClickListener(Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;)V

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    packed-switch v2, :pswitch_data_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->finishContextActionMode()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isLocalCameraAlbum()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isShareableAlbum()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x4

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_actionbar_reshare:I

    sget v4, Lcom/google/android/apps/plus/R$string;->from_your_phone_initiate_share:I

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    :cond_1
    :goto_0
    iput-boolean v7, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mHideParentActionBarItems:Z

    :cond_2
    :goto_1
    return-void

    :pswitch_0
    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerTitleResourceId:I

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isLocalCameraAlbum()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    goto :goto_1

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mTakeVideo:Z

    if-eqz v2, :cond_3

    if-nez v0, :cond_3

    const/4 v2, 0x3

    sget v3, Lcom/google/android/apps/plus/R$drawable;->icn_add_video:I

    sget v4, Lcom/google/android/apps/plus/R$string;->post_take_video_button:I

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mTakePhoto:Z

    if-eqz v2, :cond_4

    if-nez v0, :cond_4

    const/4 v2, 0x2

    sget v3, Lcom/google/android/apps/plus/R$drawable;->icn_events_add_photo:I

    sget v4, Lcom/google/android/apps/plus/R$string;->post_take_photo_button:I

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    :cond_4
    if-gtz v0, :cond_5

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerShareWithZeroSelected:Z

    if-eqz v2, :cond_6

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$plurals;->from_your_phone_selected_count:I

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showDoneTitle(Ljava/lang/String;)V

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_actionbar_reshare:I

    sget v3, Lcom/google/android/apps/plus/R$string;->from_your_phone_initiate_share:I

    invoke-virtual {p1, v6, v2, v3}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mHideParentActionBarItems:Z

    goto :goto_1

    :cond_6
    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerTitleResourceId:I

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->startContextActionMode()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$plurals;->from_your_phone_selected_count:I

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showDoneTitle(Ljava/lang/String;)V

    if-lez v0, :cond_7

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mHideParentActionBarItems:Z

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_actionbar_reshare:I

    sget v3, Lcom/google/android/apps/plus/R$string;->from_your_phone_initiate_share:I

    invoke-virtual {p1, v6, v2, v3}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    goto/16 :goto_1

    :cond_7
    iput-boolean v7, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mHideParentActionBarItems:Z

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 6
    .param p1    # Landroid/view/Menu;

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget v3, Lcom/google/android/apps/plus/R$id;->select_item:I

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$id;->delete_photos:I

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iget v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isLocalCameraAlbum()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isInstantUploadAlbum()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_1
    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :pswitch_2
    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$plurals;->delete_photo:I

    invoke-virtual {v3, v4, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_2
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final onResume()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/phone/Pageable;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v1, p0}, Lcom/google/android/apps/plus/phone/Pageable;->setLoadingListener(Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLoaderActive:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v1}, Lcom/google/android/apps/plus/phone/Pageable;->isDataSourceLoading()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->onDataSourceLoading(Z)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->showEmptyViewProgress(Landroid/view/View;)V

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->handlePhotoDelete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateSpinner()V

    return-void

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    if-eqz v1, :cond_5

    const-string v1, "INTENT"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "ALBUM_NAME"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    if-eqz v1, :cond_1

    const-string v1, "STATE_PICKER_MODE"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "STATE_PICKER_TITLE"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerTitleResourceId:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "STATE_PICKER_SHARE_ON_ZERO"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerShareWithZeroSelected:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v0, v1, [Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    const-string v1, "SELECTED_ITEMS"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, "refresh_request"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, "delete_request"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_4
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLoaderActive:Z

    if-eqz v1, :cond_5

    const-string v1, "loader_active"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_5
    return-void
.end method

.method public final onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onStop()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->onStop()V

    return-void
.end method

.method public final onViewUsed(I)V
    .locals 7
    .param p1    # I

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isPaused()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getCount()I

    move-result v5

    iget v6, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExcludedCount:I

    add-int v0, v5, v6

    iget v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumCount:I

    if-eq v0, v5, :cond_0

    iget v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mCount:I

    if-eq v5, v0, :cond_2

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mCount:I

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshable:Z

    :cond_2
    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshable:Z

    if-eqz v5, :cond_0

    const/16 v5, 0x10

    if-ge v0, v5, :cond_4

    move v1, v3

    :goto_1
    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->refresh()V

    :cond_3
    :goto_2
    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshable:Z

    goto :goto_0

    :cond_4
    move v1, v4

    goto :goto_1

    :cond_5
    iget v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExcludedCount:I

    sub-int v5, v0, v5

    add-int/lit8 v5, v5, -0x40

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-lt p1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v3

    if-eqz v3, :cond_3

    instance-of v5, v3, Lcom/google/android/apps/plus/phone/Pageable;

    if-eqz v5, :cond_6

    check-cast v3, Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v3}, Lcom/google/android/apps/plus/phone/Pageable;->hasMore()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v3}, Lcom/google/android/apps/plus/phone/Pageable;->loadMore()V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateView(Landroid/view/View;)V

    goto :goto_2
.end method

.method public final refresh()V
    .locals 7

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->refresh()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mStreamId:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mStreamId:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0x1f4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAuthkey:Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->getStreamPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateView(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPhotoOfUserId:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPhotoOfUserId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getPhotosOfUser(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumId:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAuthkey:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->getAlbumPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEventId:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->readEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    goto :goto_1
.end method

.method protected final shareSelectedPhotos()V
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final shouldHideParentActionBarItems()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mHideParentActionBarItems:Z

    return v0
.end method
