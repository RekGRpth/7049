.class final Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "HostedProfileFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateProfilePlusOneRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handlePlusOneCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onDeleteProfilePlusOneRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handlePlusOneCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onGetProfileAndContactComplete$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onGetProfileAndContactComplete(); requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleProfileServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onInsertCameraPhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onInsertCameraPhotoComplete(); requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handlerInsertCameraPhoto$b5e9bbb(I)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$300(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onReportAbuseRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleReportAbuseCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onSetCircleMembershipComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSetCircleMembershipComplete(); requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleProfileServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onSetCoverPhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSetCoverPhotoComplete(); requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleCoverPhotoCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$200(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onSetMutedRequestComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Z
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleSetMutedCallback(IZLcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onSetScrapbookInfoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSetCoverPhotoComplete(); requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleCoverPhotoCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$200(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onUploadCoverPhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onUploadCoverPhotoComplete(); requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleCoverPhotoCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$200(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onUploadProfilePhotoComplete(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 5
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onUploadProfilePhotoComplete(); requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0, p1, p3}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleProfileServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$100(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->getProfileAndContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v1

    # setter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$002(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    :cond_1
    return-void
.end method
