.class final Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;
.super Ljava/lang/Object;
.source "HostedProfileFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;"
        }
    .end annotation

    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedProfileFragment"

    const-string v1, "Loader<ProfileAndContactData> onCreateLoader()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/fragments/ProfileLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v3, "person_id"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/ProfileLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 9
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v8, 0x3

    const/4 v7, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    check-cast p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    const-string v0, "HostedProfileFragment"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedProfileFragment"

    const-string v3, "Loader<ProfileAndContactData> onLoadFinished()"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$402(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContactUpdateTime:J
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$500(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)J

    move-result-wide v3

    iget-wide v5, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->contactUpdateTime:J

    cmp-long v0, v3, v5

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileUpdateTime:J
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$600(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)J

    move-result-wide v3

    iget-wide v5, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileUpdateTime:J

    cmp-long v0, v3, v5

    if-eqz v0, :cond_9

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-wide v3, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->contactUpdateTime:J

    # setter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContactUpdateTime:J
    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$502(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;J)J

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-wide v3, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileUpdateTime:J

    # setter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileUpdateTime:J
    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$602(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;J)J

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$700(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$700(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    move-result-object v0

    # getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->mProfileCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->access$800(Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;)Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-result-object v0

    if-eqz v0, :cond_9

    move v0, v2

    :goto_0
    iget v3, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileState:I

    if-eq v3, v8, :cond_2

    iget v3, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileState:I

    const/4 v4, 0x6

    if-eq v3, v4, :cond_2

    iget v3, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileState:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$900(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-nez v3, :cond_7

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iput-boolean v2, v1, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mError:Z

    iget v1, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileState:I

    if-eqz v1, :cond_4

    iget v1, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileState:I

    if-ne v1, v2, :cond_6

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1000(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    sget v4, Lcom/google/android/apps/plus/R$string;->profile_load_error:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->showError(Ljava/lang/String;)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->invalidateActionBar()V
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1100(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v0, v2, v7}, Lcom/google/android/apps/plus/phone/StreamAdapter;->triggerStreamObservers(ZI)V

    :cond_5
    :goto_2
    return-void

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1000(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    sget v4, Lcom/google/android/apps/plus/R$string;->profile_does_not_exist:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->showError(Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iput-boolean v1, v3, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mError:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1000(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->setProfileData(Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1000(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isPlusPage()Z

    move-result v3

    # setter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsPlusPage:Z
    invoke-static {v1, v3}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1202(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1000(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isBlocked()Z

    move-result v3

    # setter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsBlocked:Z
    invoke-static {v1, v3}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1302(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1000(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isMuted()Z

    move-result v3

    # setter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMute:Z
    invoke-static {v1, v3}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1402(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->invalidateActionBar()V
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1500(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->onAsyncData()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-static {v1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1600(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->refreshProfile()V

    :cond_8
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v0, v2, v7}, Lcom/google/android/apps/plus/phone/StreamAdapter;->triggerStreamObservers(ZI)V

    goto :goto_2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
