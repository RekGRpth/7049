.class final Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;
.super Landroid/database/MergeCursor;
.source "HostedProfileFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ProfileMergeCursor"
.end annotation


# instance fields
.field private mProfileCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

.field private mStreamCursor:Landroid/database/Cursor;


# direct methods
.method public constructor <init>([Landroid/database/Cursor;)V
    .locals 1
    .param p1    # [Landroid/database/Cursor;

    invoke-direct {p0, p1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    const/4 v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->mProfileCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    const/4 v0, 0x1

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->mStreamCursor:Landroid/database/Cursor;

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;)Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->mProfileCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    return-object v0
.end method


# virtual methods
.method public final wrapsStreamCursor(Landroid/database/Cursor;)Z
    .locals 1
    .param p1    # Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->mStreamCursor:Landroid/database/Cursor;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
