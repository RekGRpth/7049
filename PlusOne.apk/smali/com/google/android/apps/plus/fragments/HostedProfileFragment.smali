.class public Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
.super Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
.source "HostedProfileFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/fragments/BlockFragment$Listener;
.implements Lcom/google/android/apps/plus/fragments/BlockPersonDialog$PersonBlocker;
.implements Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;
.implements Lcom/google/android/apps/plus/fragments/UnblockPersonDialog$PersonUnblocker;
.implements Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedStreamFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/fragments/BlockFragment$Listener;",
        "Lcom/google/android/apps/plus/fragments/BlockPersonDialog$PersonBlocker;",
        "Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;",
        "Lcom/google/android/apps/plus/fragments/UnblockPersonDialog$PersonUnblocker;",
        "Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;"
    }
.end annotation


# instance fields
.field private mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

.field private mBlockInProgress:Z

.field private mChoosePhotoTarget:I

.field private final mCircleContentObserver:Landroid/database/DataSetObserver;

.field private mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private mContactUpdateTime:J

.field private mContext:Landroid/content/Context;

.field private mControlPrimarySpinner:Z

.field private mCurrentSpinnerPosition:I

.field private mHadConfigurationChange:Z

.field private final mHandler:Landroid/os/Handler;

.field private mHasGaiaId:Z

.field private mInsertCameraPhotoRequestId:Ljava/lang/Integer;

.field private mIsBlocked:Z

.field private mIsMute:Z

.field private mIsMyProfile:Z

.field private mIsPlusPage:Z

.field private mLandscape:Z

.field private mMuteRequestId:Ljava/lang/Integer;

.field private mMuteRequestIsMuted:Z

.field private mPersonId:Ljava/lang/String;

.field private mPlusOneRequestId:Ljava/lang/Integer;

.field private mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

.field private final mProfileAndContactDataLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;"
        }
    .end annotation
.end field

.field private mProfileAndContactLoaderActive:Z

.field private mProfileIsExpanded:Z

.field private mProfilePendingRequestId:Ljava/lang/Integer;

.field private final mProfileServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mProfileUpdateTime:J

.field private mReportAbuseRequestId:Ljava/lang/Integer;

.field private mSetCoverPhotoRequestId:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHandler:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mControlPrimarySpinner:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCurrentSpinnerPosition:I

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAndContactLoaderActive:Z

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAndContactDataLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCircleContentObserver:Landroid/database/DataSetObserver;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->invalidateActionBar()V

    return-void
.end method

.method static synthetic access$1202(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsPlusPage:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsBlocked:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMute:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->invalidateActionBar()V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)Z
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
    .param p1    # Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileUpdateTime:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xdbba0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedProfileFragment"

    const-string v1, "Refreshing because profile info is stale."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleCoverPhotoCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handlerInsertCameraPhoto$b5e9bbb(I)V

    return-void
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAndContactLoaderActive:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)J
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContactUpdateTime:J

    return-wide v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;J)J
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContactUpdateTime:J

    return-wide p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)J
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileUpdateTime:J

    return-wide v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;J)J
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileUpdateTime:J

    return-wide p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    return v0
.end method

.method private canShowConversationActions()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAndContactLoaderActive:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsPlusPage:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsBlocked:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mError:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private canShowRefreshInActionBar()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v0

    iget v1, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mLandscape:Z

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private handleCoverPhotoCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 5
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->getProfileAndContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    goto :goto_1
.end method

.method private handlerInsertCameraPhoto$b5e9bbb(I)V
    .locals 9
    .param p1    # I

    const/4 v8, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->getLastCameraMediaLocation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$string;->camera_photo_error:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_1
    instance-of v0, v7, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    if-eqz v0, :cond_2

    move-object v0, v7

    check-cast v0, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;->hideInsertCameraPhotoDialog()V

    :cond_2
    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    goto :goto_0

    :cond_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    const-wide/16 v2, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-object v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mChoosePhotoTarget:I

    packed-switch v2, :pswitch_data_0

    const/4 v2, 0x2

    move v3, v8

    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget v5, Lcom/google/android/apps/plus/R$string;->change_photo_crop_title:I

    invoke-virtual {v7, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v4, v5, v0, v3}, Lcom/google/android/apps/plus/phone/Intents;->getPhotoPickerIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    :pswitch_0
    const/4 v3, 0x3

    const/4 v2, 0x5

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private isDialogVisible(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private launchEditActivity(ILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {v1, v2, p1, p2, p3}, Lcom/google/android/apps/plus/phone/Intents;->getProfileEditActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->PROFILE_EDIT_START:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    return-void
.end method

.method private safeStartActivity(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "HostedProfileFragment"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "HostedProfileFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot launch activity: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private setPersonBlocked(Z)V
    .locals 7
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isPlusPage()Z

    move-result v4

    move v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/BlockFragment;->getInstance(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/android/apps/plus/fragments/BlockFragment;

    move-result-object v6

    const/4 v0, 0x0

    invoke-virtual {v6, p0, v0}, Lcom/google/android/apps/plus/fragments/BlockFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/fragments/BlockFragment;->show(Landroid/support/v4/app/FragmentActivity;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->beginBlockInProgress()V

    return-void
.end method

.method private updateCoverPhoto(Ljava/lang/String;Landroid/graphics/RectF;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getScrapbookLayout()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, p1, p2, v0}, Lcom/google/android/apps/plus/service/EsService;->setScrapbookInfo(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/graphics/RectF;Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/plus/R$string;->setting_cover_photo:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    return-void
.end method

.method private updateFullBleedPhoto(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getScrapbookLayout()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, p1, p2, v0}, Lcom/google/android/apps/plus/service/EsService;->setScrapbookInfo(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ILjava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/plus/R$string;->setting_cover_photo:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    return-void
.end method


# virtual methods
.method public final blockPerson(Ljava/io/Serializable;)V
    .locals 1
    .param p1    # Ljava/io/Serializable;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->setPersonBlocked(Z)V

    return-void
.end method

.method protected final createStreamAdapter(Landroid/content/Context;Lcom/google/android/apps/plus/views/StreamGridView;Lcom/google/android/apps/plus/util/StreamLayoutInfo;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;Lcom/google/android/apps/plus/phone/ComposeBarController;Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)Lcom/google/android/apps/plus/phone/StreamAdapter;
    .locals 15
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p3    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p5    # Landroid/view/View$OnClickListener;
    .param p6    # Lcom/google/android/apps/plus/views/ItemClickListener;
    .param p7    # Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;
    .param p8    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;
    .param p9    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;
    .param p10    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;
    .param p11    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;
    .param p12    # Lcom/google/android/apps/plus/phone/ComposeBarController;
    .param p13    # Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    move-object/from16 v14, p6

    new-instance v6, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$4;

    invoke-direct {v6, p0, v14}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Lcom/google/android/apps/plus/views/ItemClickListener;)V

    new-instance v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    const/4 v12, 0x0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v13}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/StreamGridView;Lcom/google/android/apps/plus/util/StreamLayoutInfo;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;Lcom/google/android/apps/plus/phone/ComposeBarController;Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)V

    return-object v0
.end method

.method public final doPickPhotoFromAlbums(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x1

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mChoosePhotoTarget:I

    if-ne v2, v4, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/Intents;->newAlbumsActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPersonId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotosHome(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setShowCameraAlbum(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->photo_picker_album_label_profile:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerTitleResourceId(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setCropMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setHideCameraVideos(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2, v5}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mChoosePhotoTarget:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget v2, Lcom/google/android/apps/plus/R$string;->app_name:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Dogfood limitation: camera album temporarily hidden"

    sget v4, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "coming_soon"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    const-string v3, "115239603441691718952"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    const-string v3, "5745127577944303633"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->photo_picker_album_label_cover_photo:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerTitleResourceId(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setCropMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2, v6}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPersonId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getScrapbookAlbumId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->photo_picker_album_label_cover_photo:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerTitleResourceId(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setCropMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2, v6}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final doRepositionCoverPhoto()V
    .locals 10

    const/4 v5, 0x0

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getScrapbookCoverPhotoOwnerId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getScrapbookCoverPhotoId()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getScrapbookCoverPhotoUrl()Ljava/lang/String;

    move-result-object v4

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->hasCoverPhotoUpgrade()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    :goto_0
    invoke-static {v2, v3, v5, v0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getPhotoPickerIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;I)Landroid/content/Intent;

    move-result-object v8

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->hasCoverPhotoUpgrade()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getScrapbookCoverPhotoCoordinates()Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;

    move-result-object v9

    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7}, Landroid/graphics/RectF;-><init>()V

    iget-object v1, v9, Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;->left:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, v7, Landroid/graphics/RectF;->left:F

    iget-object v1, v9, Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;->top:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, v7, Landroid/graphics/RectF;->top:F

    iget-object v1, v9, Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;->right:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, v7, Landroid/graphics/RectF;->right:F

    iget-object v1, v9, Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;->bottom:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, v7, Landroid/graphics/RectF;->bottom:F

    const-string v1, "coordinates"

    invoke-virtual {v8, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :goto_1
    const/4 v1, 0x6

    invoke-virtual {p0, v8, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_0
    const/4 v1, 0x2

    goto :goto_0

    :cond_1
    const-string v1, "top_offset"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getScrapbookCoverPhotoOffset()I

    move-result v2

    invoke-virtual {v8, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1
.end method

.method protected final doShowEmptyView(Landroid/view/View;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method protected final doShowEmptyViewProgress(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method public final doTakePhoto()V
    .locals 8

    const/4 v3, 0x1

    :try_start_0
    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mChoosePhotoTarget:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$string;->app_name:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->coming_soon:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "coming_soon"

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mChoosePhotoTarget:I

    if-ne v4, v3, :cond_1

    move v2, v3

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    const-string v4, "camera-profile.jpg"

    invoke-static {v4}, Lcom/google/android/apps/plus/phone/Intents;->getCameraIntentPhoto$3a35108a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->change_photo_no_camera:I

    invoke-static {v4, v5, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    const/4 v2, 0x4

    goto :goto_1
.end method

.method public final getExtrasForLogging()Landroid/os/Bundle;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "extra_gaia_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final getStreamHeaderCursor()Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getViewIsExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PROFILE:Lcom/google/android/apps/plus/analytics/OzViews;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_0
.end method

.method protected final handlePlusOneCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected final handleProfileServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_4
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    goto :goto_0
.end method

.method protected final handleReportAbuseCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/plus/R$string;->report_abuse_completed_toast:I

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected final handleSetMutedCallback(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 4
    .param p1    # I
    .param p2    # Z
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    if-eqz p2, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/plus/R$string;->report_mute_completed_toast:I

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/plus/R$string;->report_unmute_completed_toast:I

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected final initCirclesLoader()V
    .locals 0

    return-void
.end method

.method protected final isAdapterEmpty()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCount()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isLocalDataAvailable(Landroid/database/Cursor;)Z
    .locals 2
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isProgressIndicatorVisible()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isProgressIndicatorVisible()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAndContactLoaderActive:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final needsAsyncData()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final onActionButtonClicked(I)V
    .locals 9
    .param p1    # I

    const/4 v8, 0x2

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->safeStartActivity(Landroid/content/Intent;)V

    :cond_1
    return-void

    :pswitch_0
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    const-string v7, "e:"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    new-instance v5, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v5, v3, v2, v1}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/PersonData;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    invoke-static {v6, v7, v0}, Lcom/google/android/apps/plus/phone/Intents;->getNewConversationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_0

    :cond_2
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    if-eqz v6, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    goto :goto_1

    :pswitch_1
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    const-string v7, "e:"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :goto_2
    new-instance v5, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v5, v3, v2, v1}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/PersonData;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v6, v7, v8, v0}, Lcom/google/android/apps/plus/phone/Intents;->getNewHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_0

    :cond_3
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    if-eqz v6, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 16
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v12, -0x1

    move/from16 v0, p2

    if-eq v0, v12, :cond_1

    const/4 v12, 0x7

    move/from16 v0, p1

    if-ne v0, v12, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v14, Lcom/google/android/apps/plus/analytics/OzActions;->PROFILE_EDIT_CANCEL:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v15

    invoke-static {v12, v13, v14, v15}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "original_circle_ids"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "selected_circle_ids"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHandler:Landroid/os/Handler;

    new-instance v13, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$5;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v6, v10}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {v12, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    instance-of v12, v1, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    if-eqz v12, :cond_2

    move-object v12, v1

    check-cast v12, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    invoke-interface {v12}, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;->showInsertCameraPhotoDialog()V

    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v13, "camera-profile.jpg"

    invoke-static {v1, v12, v13}, Lcom/google/android/apps/plus/service/EsService;->insertCameraPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    goto :goto_0

    :pswitch_2
    if-eqz p3, :cond_0

    const-string v12, "data"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v4

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHandler:Landroid/os/Handler;

    new-instance v13, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$6;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v4}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$6;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;[B)V

    invoke-virtual {v12, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :pswitch_3
    if-eqz p3, :cond_0

    const-string v12, "photo_picker_crop_mode"

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v12, "photo_id"

    const-wide/16 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13, v14}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    const-wide/16 v12, 0x0

    cmp-long v12, v7, v12

    if-eqz v12, :cond_6

    const-string v12, "is_gallery_photo"

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    const/4 v12, 0x3

    if-ne v3, v12, :cond_4

    const-string v12, "coordinates"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHandler:Landroid/os/Handler;

    new-instance v13, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$7;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v9, v2, v5}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$7;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Ljava/lang/String;Landroid/graphics/RectF;Z)V

    invoke-virtual {v12, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_3
    :goto_1
    if-eqz v5, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v14, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_CHOOSE_GALLERY:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v15

    invoke-static {v12, v13, v14, v15}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    goto/16 :goto_0

    :cond_4
    const/4 v12, 0x2

    if-ne v3, v12, :cond_3

    const-string v12, "top_offset"

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHandler:Landroid/os/Handler;

    new-instance v13, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$8;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v9, v11, v5}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$8;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Ljava/lang/String;IZ)V

    invoke-virtual {v12, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v14, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_CHOOSE_OWN_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v15

    invoke-static {v12, v13, v14, v15}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    goto/16 :goto_0

    :cond_6
    const-string v12, "data"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v4

    if-eqz v4, :cond_7

    const/4 v12, 0x3

    if-ne v3, v12, :cond_8

    const-string v12, "coordinates"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHandler:Landroid/os/Handler;

    new-instance v13, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$9;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v4, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$9;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;[BLandroid/graphics/RectF;)V

    invoke-virtual {v12, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_7
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v14, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_CHOOSE_OWN_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v15

    invoke-static {v12, v13, v14, v15}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    goto/16 :goto_0

    :cond_8
    const/4 v12, 0x2

    if-ne v3, v12, :cond_7

    const-string v12, "top_offset"

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHandler:Landroid/os/Handler;

    new-instance v13, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$10;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v4, v11}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$10;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;[BI)V

    invoke-virtual {v12, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    :pswitch_4
    const-string v12, "photo_picker_crop_mode"

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v12, "photo_id"

    const-wide/16 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13, v14}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    const/4 v12, 0x3

    if-ne v3, v12, :cond_a

    const-string v12, "coordinates"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateCoverPhoto(Ljava/lang/String;Landroid/graphics/RectF;)V

    :cond_9
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v14, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_REPOSITION:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v15

    invoke-static {v12, v13, v14, v15}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    goto/16 :goto_0

    :cond_a
    const/4 v12, 0x2

    if-ne v3, v12, :cond_9

    const-string v12, "top_offset"

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v11}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateFullBleedPhoto(Ljava/lang/String;I)V

    goto :goto_3

    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    const/4 v15, 0x1

    invoke-static {v12, v13, v14, v15}, Lcom/google/android/apps/plus/service/EsService;->getProfileAndContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v14, Lcom/google/android/apps/plus/analytics/OzActions;->PROFILE_EDIT_SAVE:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v15

    invoke-static {v12, v13, v14, v15}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final onAddressClicked(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "geo:0,0?q="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/util/MapUtils;->launchMapsActivity(Landroid/content/Context;Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onAttach(Landroid/app/Activity;)V

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mLandscape:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAvatarClicked()V
    .locals 4

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mChoosePhotoTarget:I

    const-string v1, "change_photo"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->isDialogVisible(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;

    sget v2, Lcom/google/android/apps/plus/R$string;->change_photo_dialog_title:I

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/Intents;->isCameraIntentRegistered(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->setIsCameraSupported(Z)V

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "change_photo"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->profile_photos_stream_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const-string v2, "profile"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onBlockCompleted(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->endBlockInProgress(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->invalidateActionBar()V

    return-void
.end method

.method public final onCirclesButtonClicked()V
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    const/4 v5, 0x1

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/Intents;->getCircleMembershipActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final onCoverPhotoClicked(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoOneUpActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->profile_cover_photos_stream_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getScrapbookAlbumId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getScrapbookPhotoId(I)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoId(Ljava/lang/Long;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_9

    const-string v1, "profile_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "profile_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    :cond_0
    const-string v1, "plusone_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "plusone_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    :cond_1
    const-string v1, "abuse_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "abuse_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    :cond_2
    const-string v1, "mute_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "mute_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    const-string v1, "mute_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestIsMuted:Z

    :cond_3
    const-string v1, "camera_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "camera_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    :cond_4
    const-string v1, "cover_photo_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "cover_photo_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    :cond_5
    const-string v1, "block_in_progress"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "block_in_progress"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    :cond_6
    const-string v1, "profile_is_expanded"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "profile_is_expanded"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileIsExpanded:Z

    :cond_7
    const-string v1, "choose_photo_target"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "choose_photo_target"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mChoosePhotoTarget:I

    :cond_8
    const-string v1, "contact_update"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContactUpdateTime:J

    const-string v1, "profile_update"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileUpdateTime:J

    const-string v1, "configuration_change"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHadConfigurationChange:Z

    :cond_9
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "person_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/16 v2, 0x64

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAndContactDataLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    new-instance v1, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCircleContentObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->registerObserver(Landroid/database/DataSetObserver;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    if-nez p1, :cond_a

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->fetchStreamContent(Z)Z

    :cond_a
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "HostedProfileFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Loader<Cursor> onCreateLoader() -- "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    const-string v0, "POSTS_LOADER_ID"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.SENDTO"

    const-string v7, "sms"

    const-string v8, ""

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v4, v5, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/high16 v7, 0x10000

    invoke-virtual {v5, v4, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->init(Ljava/lang/String;ZZZLcom/google/android/apps/plus/fragments/CircleNameResolver;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->setOnClickListener(Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->beginBlockInProgress()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileIsExpanded:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->setViewIsExpanded(Z)V

    return-object v6

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final onDeviceLocationClicked(Ljava/lang/Double;Ljava/lang/Double;)V
    .locals 9
    .param p1    # Ljava/lang/Double;
    .param p2    # Ljava/lang/Double;

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/Intents;->getLocationsMapActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;DD)Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CURRENT_LOCATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    return-void
.end method

.method public final onDeviceLocationHelpClicked()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->url_param_help_location:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startExternalActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v2, 0x1

    const-string v1, "cover_photo_upgrade"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "change_photo"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->isDialogVisible(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v5, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;

    sget v1, Lcom/google/android/apps/plus/R$string;->change_cover_photo_dialog_title:I

    invoke-direct {v5, v1}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->isCameraIntentRegistered(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v5, v1}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->setIsCameraSupported(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getScrapbookAlbumId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->hasFullBleedPhotoUpgrade()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getScrapbookCoverPhotoId()Ljava/lang/Long;

    move-result-object v4

    :goto_1
    invoke-virtual {v5, v2, v1, v4}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->setIsForCoverPhoto(ZZLjava/lang/Long;)V

    invoke-virtual {v5, p0, v3}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "change_photo"

    invoke-virtual {v5, v1, v2}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    const-string v1, "coming_soon"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->newAlbumsActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPersonId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotosHome(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setShowCameraAlbum(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$string;->photo_picker_album_label_cover_photo:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerTitleResourceId(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setCropMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setHideCameraVideos(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_2
.end method

.method public final onEditDeviceLocationClicked()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getDeviceLocationSettingsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onEditEducationClicked()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getEducationList()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getSharingRosterData()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->launchEditActivity(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final onEditEmploymentClicked()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getEmploymentList()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getSharingRosterData()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->launchEditActivity(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final onEditPlacesLivedClicked()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getPlacesLivedList()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getSharingRosterData()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->launchEditActivity(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final onEmailClicked(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v2

    :cond_0
    if-eqz v2, :cond_1

    array-length v3, v2

    if-nez v3, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v3, 0x0

    aget-object v0, v2, v3

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/text/util/Rfc822Token;->setName(Ljava/lang/String;)V

    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.SENDTO"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "mailto:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->safeStartActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onExpandClicked(Z)V
    .locals 8
    .param p1    # Z

    const/4 v4, 0x0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileIsExpanded:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->setViewIsExpanded(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->triggerStreamObservers(ZI)V

    if-eqz p1, :cond_0

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->PROFILE:Lcom/google/android/apps/plus/analytics/OzViews;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    move-object v5, v4

    move-object v7, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-void

    :cond_0
    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PROFILE:Lcom/google/android/apps/plus/analytics/OzViews;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_0
.end method

.method public final onLinkClicked(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->safeStartActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public final onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 5
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    const/4 v4, 0x3

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, "HostedProfileFragment"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loader<Cursor> onLoadFinished() -- "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->wrapsStreamCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-interface {p2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;I)V

    sget-object v1, Lcom/google/android/apps/plus/phone/StreamAdapter$StreamQuery;->PROJECTION_STREAM:[Ljava/lang/String;

    array-length v1, v1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/database/Cursor;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    aput-object p2, v1, v3

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;-><init>([Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    :cond_3
    const-string v0, "HostedProfileFragment"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "HostedProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loader<Cursor> onLoadFinished() -- POSTS_LOADER_ID, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rows"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    invoke-super {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showContent(Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onLocalCallClicked(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DIAL"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "tel:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startExternalActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onLocalDirectionsClicked(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/MapUtils;->launchMapsActivity(Landroid/content/Context;Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final onLocalMapClicked(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/MapUtils;->launchMapsActivity(Landroid/content/Context;Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final onLocalReviewClicked(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-static {v1, v2, v3, p1, p2}, Lcom/google/android/apps/plus/phone/Intents;->getLocalReviewActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;II)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->safeStartActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onLocationClicked(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "geo:0,0?q="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/util/MapUtils;->launchMapsActivity(Landroid/content/Context;Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 10
    .param p1    # Landroid/view/MenuItem;

    const/4 v9, 0x0

    const/4 v6, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    sget v7, Lcom/google/android/apps/plus/R$id;->refresh:I

    if-ne v4, v7, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->refresh()V

    :goto_0
    return v6

    :cond_0
    sget v7, Lcom/google/android/apps/plus/R$id;->mute:I

    if-ne v4, v7, :cond_2

    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsPlusPage:Z

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v5

    :goto_1
    new-instance v1, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v7, "name"

    invoke-virtual {v0, v7, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "gender"

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getGender()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "target_mute"

    invoke-virtual {v0, v7, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {v1, p0, v9}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "mute_profile"

    invoke-virtual {v1, v7, v8}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getGivenName()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_2
    sget v7, Lcom/google/android/apps/plus/R$id;->unmute:I

    if-ne v4, v7, :cond_4

    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsPlusPage:Z

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v5

    :goto_2
    new-instance v1, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v7, "name"

    invoke-virtual {v0, v7, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "gender"

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getGender()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "target_mute"

    invoke-virtual {v0, v7, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {v1, p0, v9}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "unmute_profile"

    invoke-virtual {v1, v7, v8}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getGivenName()Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :cond_4
    sget v7, Lcom/google/android/apps/plus/R$id;->block:I

    if-ne v4, v7, :cond_5

    new-instance v1, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isPlusPage()Z

    move-result v7

    invoke-direct {v1, v7}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;-><init>(Z)V

    invoke-virtual {v1, p0, v9}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "block_person"

    invoke-virtual {v1, v7, v8}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    sget v7, Lcom/google/android/apps/plus/R$id;->unblock:I

    if-ne v4, v7, :cond_6

    new-instance v1, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isPlusPage()Z

    move-result v8

    invoke-direct {v1, v7, v8}, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v1, p0, v9}, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "unblock_person"

    invoke-virtual {v1, v7, v8}, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    sget v7, Lcom/google/android/apps/plus/R$id;->report_abuse:I

    if-ne v4, v7, :cond_7

    new-instance v1, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;-><init>()V

    invoke-virtual {v1, p0, v9}, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "report_abuse"

    invoke-virtual {v1, v7, v8}, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    sget v7, Lcom/google/android/apps/plus/R$id;->help:I

    if-ne v4, v7, :cond_8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$string;->url_param_help_profile:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7, v3}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v7, Landroid/content/Intent;

    const-string v8, "android.intent.action.VIEW"

    invoke-direct {v7, v8, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startExternalActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_8
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v6

    goto/16 :goto_0
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public final onPhoneNumberClicked(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DIAL"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "tel:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->safeStartActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public final onPlusOneClicked()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isPlusOnedByMe()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->isProfilePlusOnePending(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->deleteProfilePlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->isProfilePlusOnePending(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->createProfilePlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    goto :goto_0
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mControlPrimarySpinner:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/ProfileActivity;->createSpinnerAdapter(Landroid/content/Context;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCurrentSpinnerPosition:I

    if-gez v2, :cond_5

    move v2, v3

    :goto_0
    invoke-virtual {p1, v0, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->canShowRefreshInActionBar()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    :cond_1
    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->canShowConversationActions()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->isProgressIndicatorVisible()Z

    move-result v2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->canShowConversationActions()Z

    move-result v5

    if-nez v5, :cond_6

    move v2, v3

    :goto_1
    if-eqz v2, :cond_4

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_startmessenger:I

    sget v5, Lcom/google/android/apps/plus/R$string;->start_conversation_action_label:I

    invoke-virtual {p1, v3, v2, v5}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/service/Hangout;->isHangoutCreationSupported(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_starthangout:I

    sget v3, Lcom/google/android/apps/plus/R$string;->start_hangout_action_label:I

    invoke-virtual {p1, v4, v2, v3}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    :cond_4
    return-void

    :cond_5
    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCurrentSpinnerPosition:I

    goto :goto_0

    :cond_6
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v5

    iget v5, v5, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v5, :cond_7

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mLandscape:Z

    if-nez v5, :cond_7

    if-eqz v2, :cond_7

    move v2, v3

    goto :goto_1

    :cond_7
    move v2, v4

    goto :goto_1
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 9
    .param p1    # Landroid/view/Menu;

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    if-eqz v8, :cond_0

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    if-nez v8, :cond_0

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-nez v8, :cond_0

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMute:Z

    if-nez v8, :cond_0

    move v1, v6

    :goto_0
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    if-eqz v8, :cond_1

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    if-nez v8, :cond_1

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    if-nez v8, :cond_1

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    if-nez v8, :cond_1

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-nez v8, :cond_1

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMute:Z

    if-eqz v8, :cond_1

    move v5, v6

    :goto_1
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    if-eqz v8, :cond_2

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-nez v8, :cond_2

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsBlocked:Z

    if-nez v8, :cond_2

    move v0, v6

    :goto_2
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    if-eqz v8, :cond_3

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    if-nez v8, :cond_3

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    if-nez v8, :cond_3

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-nez v8, :cond_3

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsBlocked:Z

    if-eqz v8, :cond_3

    move v4, v6

    :goto_3
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    if-eqz v8, :cond_4

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    if-nez v8, :cond_4

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-nez v8, :cond_4

    move v3, v6

    :goto_4
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->canShowRefreshInActionBar()Z

    move-result v8

    if-nez v8, :cond_5

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->canShowConversationActions()Z

    move-result v8

    if-eqz v8, :cond_5

    move v2, v6

    :goto_5
    sget v6, Lcom/google/android/apps/plus/R$id;->refresh:I

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v6, Lcom/google/android/apps/plus/R$id;->mute:I

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v6, Lcom/google/android/apps/plus/R$id;->unmute:I

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v6, Lcom/google/android/apps/plus/R$id;->block:I

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v6, Lcom/google/android/apps/plus/R$id;->unblock:I

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v6, Lcom/google/android/apps/plus/R$id;->report_abuse:I

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsPlusPage:Z

    if-eqz v6, :cond_6

    sget v6, Lcom/google/android/apps/plus/R$id;->block:I

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->menu_item_block_profile:I

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    sget v6, Lcom/google/android/apps/plus/R$id;->unblock:I

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->menu_item_unblock_profile:I

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    :goto_6
    return-void

    :cond_0
    move v1, v7

    goto/16 :goto_0

    :cond_1
    move v5, v7

    goto/16 :goto_1

    :cond_2
    move v0, v7

    goto/16 :goto_2

    :cond_3
    move v4, v7

    goto :goto_3

    :cond_4
    move v3, v7

    goto :goto_4

    :cond_5
    move v2, v7

    goto :goto_5

    :cond_6
    sget v6, Lcom/google/android/apps/plus/R$id;->block:I

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->menu_item_block_person:I

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    sget v6, Lcom/google/android/apps/plus/R$id;->unblock:I

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->menu_item_unblock_person:I

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_6
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .locals 5
    .param p1    # I

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mControlPrimarySpinner:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCurrentSpinnerPosition:I

    if-eq v1, p1, :cond_0

    packed-switch p1, :pswitch_data_0

    :goto_0
    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCurrentSpinnerPosition:I

    :cond_0
    return-void

    :pswitch_0
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->refresh()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getHostedProfileAlbumsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onResume()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleProfileServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleReportAbuseCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestIsMuted:Z

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleSetMutedCallback(IZLcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handlePlusOneCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handlerInsertCameraPhoto$b5e9bbb(I)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleCoverPhotoCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_5
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHadConfigurationChange:Z

    if-nez v1, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->refreshProfile()V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    return-void
.end method

.method public final onReviewAuthorAvatarClicked(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    invoke-static {v1, v2, p1, v3}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->safeStartActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, "profile_request_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, "plusone_request_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, "abuse_request_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, "mute_request_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "mute_state"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestIsMuted:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, "camera_request_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    const-string v1, "cover_photo_request_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_5
    const-string v1, "block_in_progress"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "profile_is_expanded"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileIsExpanded:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "choose_photo_target"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mChoosePhotoTarget:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "contact_update"

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContactUpdateTime:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "profile_update"

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileUpdateTime:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/app/Activity;->getChangingConfigurations()I

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHadConfigurationChange:Z

    const-string v1, "configuration_change"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHadConfigurationChange:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void

    :cond_6
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final onSendTextClicked(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sms:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startExternalActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onSetArguments(Landroid/os/Bundle;)V

    const-string v0, "person_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "show_empty_stream"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onZagatExplanationClicked()V
    .locals 3

    new-instance v0, Lcom/google/android/apps/plus/fragments/ProfileZagatExplanationDialog;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/ProfileZagatExplanationDialog;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "zagat_explanation"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProfileZagatExplanationDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public final refresh()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->refresh()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->refreshProfile()V

    return-void
.end method

.method public final refreshProfile()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->getProfileAndContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    return-void
.end method

.method public final relinquishPrimarySpinner()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mControlPrimarySpinner:Z

    return-void
.end method

.method public final reportAbuse(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const-string v1, "IMPERSONATION"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$string;->report_user_dialog_title:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->report_impersonation_dialog_message:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x104000a

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog_warning"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    invoke-static {v1, v2, v3, p1}, Lcom/google/android/apps/plus/service/EsService;->reportProfileAbuse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/plus/R$string;->report_abuse_operation_pending:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    goto :goto_0
.end method

.method protected final setCircleMembership(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v7, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v8, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->setCircleMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/apps/plus/R$string;->add_to_circle_operation_pending:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    :goto_2
    return-void

    :cond_4
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    sget v0, Lcom/google/android/apps/plus/R$string;->remove_from_circle_operation_pending:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    goto :goto_2

    :cond_5
    sget v0, Lcom/google/android/apps/plus/R$string;->moving_between_circles_operation_pending:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    goto :goto_2
.end method

.method protected final setCoverPhoto(Ljava/lang/String;Landroid/graphics/RectF;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/graphics/RectF;
    .param p3    # Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getScrapbookCoverPhotoId()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateCoverPhoto(Ljava/lang/String;Landroid/graphics/RectF;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, p1, p2, p3}, Lcom/google/android/apps/plus/service/EsService;->setCoverPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/graphics/RectF;Z)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/plus/R$string;->setting_cover_photo:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    goto :goto_0
.end method

.method protected final setCoverPhoto([BLandroid/graphics/RectF;)V
    .locals 2
    .param p1    # [B
    .param p2    # Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/plus/service/EsService;->uploadCoverPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[BLandroid/graphics/RectF;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    sget v0, Lcom/google/android/apps/plus/R$string;->setting_cover_photo:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    return-void
.end method

.method protected final setFullBleedPhoto(Ljava/lang/String;IZ)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getScrapbookCoverPhotoId()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateFullBleedPhoto(Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, p1, p2, p3}, Lcom/google/android/apps/plus/service/EsService;->setCoverPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;IZ)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/plus/R$string;->setting_cover_photo:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    goto :goto_0
.end method

.method protected final setFullBleedPhoto([BI)V
    .locals 2
    .param p1    # [B
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/plus/service/EsService;->uploadFullBleedPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[BI)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mSetCoverPhotoRequestId:Ljava/lang/Integer;

    sget v0, Lcom/google/android/apps/plus/R$string;->setting_cover_photo:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    return-void
.end method

.method public final setPersonMuted(Z)V
    .locals 3
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    invoke-static {v0, v1, v2, p1}, Lcom/google/android/apps/plus/service/EsService;->setPersonMuted(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestIsMuted:Z

    sget v0, Lcom/google/android/apps/plus/R$string;->mute_operation_pending:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    return-void
.end method

.method protected final setProfilePhoto([B)V
    .locals 2
    .param p1    # [B

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->uploadProfilePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    sget v0, Lcom/google/android/apps/plus/R$string;->setting_profile_photo:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    return-void
.end method

.method public final unblockPerson(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->setPersonBlocked(Z)V

    return-void
.end method

.method protected final updateSpinner()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->canShowRefreshInActionBar()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->invalidateActionBar()V

    goto :goto_0
.end method
