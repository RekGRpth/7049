.class final Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "HostedSquareListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGetSquaresComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mNewerReqId:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->access$002(Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;Z)Z

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;

    sget v2, Lcom/google/android/apps/plus/R$string;->squares_load_error:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->setError(Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->updateSpinner()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$1$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$1$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$1;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->clearError()V

    goto :goto_1
.end method
