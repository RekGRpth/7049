.class public final Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "HostedSquareListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SquaresLoader"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader$SquaresQuery;
    }
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mIsDataStale:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_URI:Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-void
.end method

.method private insertSquareGroup([Ljava/lang/Object;Landroid/database/Cursor;Ljava/util/HashSet;Lcom/google/android/apps/plus/phone/EsMatrixCursor;I)V
    .locals 10
    .param p1    # [Ljava/lang/Object;
    .param p2    # Landroid/database/Cursor;
    .param p4    # Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            "Landroid/database/Cursor;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/plus/phone/EsMatrixCursor;",
            "I)V"
        }
    .end annotation

    const/4 v9, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v2, -0x1

    invoke-interface {p2, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_0
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/16 v2, 0xc

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_3

    move v2, v3

    :goto_1
    packed-switch p5, :pswitch_data_0

    :cond_1
    move v2, v6

    :goto_2
    :pswitch_0
    if-eqz v2, :cond_0

    if-nez v0, :cond_2

    const/4 v2, 0x0

    invoke-static {p1, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v2, 0xe

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, p1, v2

    packed-switch p5, :pswitch_data_1

    move v2, v6

    :goto_3
    const/16 v7, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p1, v7

    invoke-virtual {p4, p1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    const/4 v0, 0x1

    :cond_2
    const/4 v2, 0x0

    invoke-static {p1, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v2, 0xe

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, p1, v2

    const/16 v2, 0x10

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, p1, v2

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p1, v6

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v3

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, p1, v4

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, p1, v5

    const/4 v2, 0x4

    const/4 v7, 0x4

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, p1, v2

    invoke-interface {p2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, p1, v9

    const/4 v2, 0x6

    const/4 v7, 0x6

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, p1, v2

    const/4 v2, 0x7

    const/4 v7, 0x7

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, p1, v2

    const/16 v2, 0x8

    const/16 v7, 0x8

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, p1, v2

    const/16 v2, 0x9

    const/16 v7, 0x9

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, p1, v2

    const/16 v2, 0xa

    const/16 v7, 0xa

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, p1, v2

    invoke-virtual {p4, p1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    invoke-virtual {p3, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_3
    move v2, v6

    goto/16 :goto_1

    :pswitch_1
    const/4 v7, 0x6

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const/16 v7, 0xb

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_4

    move v7, v3

    :goto_4
    if-ne v8, v9, :cond_5

    if-nez v2, :cond_5

    if-nez v7, :cond_5

    move v2, v3

    goto/16 :goto_2

    :cond_4
    move v7, v6

    goto :goto_4

    :cond_5
    move v2, v6

    goto/16 :goto_2

    :pswitch_2
    const/16 v7, 0xd

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_6

    move v7, v3

    :goto_5
    if-eqz v7, :cond_7

    if-nez v2, :cond_7

    move v2, v3

    goto/16 :goto_2

    :cond_6
    move v7, v6

    goto :goto_5

    :cond_7
    move v2, v6

    goto/16 :goto_2

    :pswitch_3
    move v2, v3

    goto/16 :goto_3

    :pswitch_4
    move v2, v4

    goto/16 :goto_3

    :pswitch_5
    move v2, v5

    goto/16 :goto_3

    :cond_8
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 10

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsSquaresData;->queryLastSquaresSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v6

    const-wide/32 v8, 0xdbba0

    cmp-long v0, v0, v8

    if-lez v0, :cond_1

    move v0, v5

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;->mIsDataStale:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$Query;->PROJECTION:[Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/plus/content/EsSquaresData;->getAllSquares(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    new-instance v4, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader$SquaresQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v4, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader$SquaresQuery;->PROJECTION:[Ljava/lang/String;

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/Object;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;->insertSquareGroup([Ljava/lang/Object;Landroid/database/Cursor;Ljava/util/HashSet;Lcom/google/android/apps/plus/phone/EsMatrixCursor;I)V

    const/4 v5, 0x2

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;->insertSquareGroup([Ljava/lang/Object;Landroid/database/Cursor;Ljava/util/HashSet;Lcom/google/android/apps/plus/phone/EsMatrixCursor;I)V

    const/4 v5, 0x3

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;->insertSquareGroup([Ljava/lang/Object;Landroid/database/Cursor;Ljava/util/HashSet;Lcom/google/android/apps/plus/phone/EsMatrixCursor;I)V

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_0
    return-object v4

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isDataStale()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;->mIsDataStale:Z

    return v0
.end method
