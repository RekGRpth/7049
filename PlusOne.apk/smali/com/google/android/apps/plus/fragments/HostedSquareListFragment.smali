.class public Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedSquareListFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;,
        Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$Query;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/phone/SquareCardAdapter;

.field private mContext:Landroid/content/Context;

.field private mDataPresent:Z

.field private mErrorText:Ljava/lang/String;

.field private mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field private final mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mRefreshNeeded:Z

.field private mSquaresLoaderActive:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mSquaresLoaderActive:Z

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mRefreshNeeded:Z

    return v0
.end method

.method private fetchData()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mNewerReqId:Ljava/lang/Integer;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mDataPresent:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsService;->getSquares(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mNewerReqId:Ljava/lang/Integer;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->updateSpinner()V

    return-void
.end method

.method private hasError()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mErrorText:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final clearError()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mErrorText:Ljava/lang/String;

    return-void
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/SquareCardAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/SquareCardAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isProgressIndicatorVisible()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->isProgressIndicatorVisible()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mSquaresLoaderActive:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActionButtonClicked(I)V
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getSquareSearchActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onAttach(Landroid/app/Activity;)V

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mContext:Landroid/content/Context;

    return-void
.end method

.method public final onClick(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1, v2, v2}, Lcom/google/android/apps/plus/phone/Intents;->getSquareStreamActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v1, "squares_refresh"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mRefreshNeeded:Z

    const-string v1, "squares_datapresent"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mDataPresent:Z

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "refresh"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mRefreshNeeded:Z

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mSquaresLoaderActive:Z

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v1, Lcom/google/android/apps/plus/R$layout;->hosted_squares_fragment:I

    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$id;->grid:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ColumnGridView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v1, Lcom/google/android/apps/plus/phone/SquareCardAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-direct {v1, v2, v3, p0, v4}, Lcom/google/android/apps/plus/phone/SquareCardAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;Lcom/google/android/apps/plus/views/ColumnGridView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/SquareCardAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/SquareCardAdapter;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->list_selected_holo:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelector(I)V

    sget v1, Lcom/google/android/apps/plus/R$string;->no_squares:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->setupEmptyView(Landroid/view/View;I)V

    return-object v0
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const-string v3, "dismiss_invitation"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "square_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v3, v2}, Lcom/google/android/apps/plus/service/EsService;->declineSquareInvitation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    const-string v3, "extra_square_id"

    invoke-static {v3, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_DECLINE_INVITATION:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v0, v3, v4, v5, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public final onInvitationDismissed(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/plus/R$string;->square_dismiss_invitation_text:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->square_dialog_decline_button:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "square_id"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "dismiss_invitation"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public final onInviterImageClick(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "g:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v2, v3, v1, v4, v5}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast p2, Landroid/database/Cursor;

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mSquaresLoaderActive:Z

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mDataPresent:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/SquareCardAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/SquareCardAdapter;->changeCursor(Landroid/database/Cursor;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->hasError()Z

    move-result v0

    if-nez v0, :cond_1

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mRefreshNeeded:Z

    check-cast p1, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;->isDataStale()Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mRefreshNeeded:Z

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->invalidateActionBar()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mDataPresent:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->showContent(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mRefreshNeeded:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->hasError()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mErrorText:Ljava/lang/String;

    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    sget v0, Lcom/google/android/apps/plus/R$string;->no_squares:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1    # Landroid/view/MenuItem;

    const/4 v3, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v4, Lcom/google/android/apps/plus/R$id;->refresh:I

    if-ne v2, v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->refresh()V

    :goto_0
    return v3

    :cond_0
    sget v4, Lcom/google/android/apps/plus/R$id;->help:I

    if-ne v2, v4, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->url_param_help_squares:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mContext:Landroid/content/Context;

    invoke-static {v4, v1}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v4, v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->startExternalActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    goto :goto_0
.end method

.method public final onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButtonIfRoom()V

    const/4 v0, 0x0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_menu_search_holo_light:I

    sget v2, Lcom/google/android/apps/plus/R$string;->menu_search:I

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    sget v0, Lcom/google/android/apps/plus/R$string;->home_screen_squares_label:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 5
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->isRefreshButtonVisible()Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v2

    :goto_0
    sget v4, Lcom/google/android/apps/plus/R$id;->refresh:I

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    if-nez v1, :cond_1

    :goto_1
    invoke-interface {v4, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method public final onResume()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_SQUARES:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mRefreshNeeded:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->fetchData()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->updateSpinner()V

    return-void
.end method

.method protected final onResumeContentFetched(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResumeContentFetched(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mRefreshNeeded:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mErrorText:Ljava/lang/String;

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "squares_refresh"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mRefreshNeeded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "squares_datapresent"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mDataPresent:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public final refresh()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->refresh()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->fetchData()V

    return-void
.end method

.method protected final setError(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mErrorText:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mDataPresent:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mErrorText:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mErrorText:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public final shouldPersistStateToHost()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final showContent(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showContent(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    return-void
.end method

.method protected final showEmptyView(Landroid/view/View;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    return-void
.end method
