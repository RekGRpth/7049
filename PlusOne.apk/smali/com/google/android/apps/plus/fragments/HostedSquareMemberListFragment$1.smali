.class final Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "HostedSquareMemberListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReadSquareMembersComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 5
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    iput-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mNewerReqId:Ljava/lang/Integer;

    :goto_0
    const-string v0, "SquareMembers"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SquareMembers"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onReadSquareMembersComplete: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mLoaderError:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->access$000(Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    sget v2, Lcom/google/android/apps/plus/R$string;->people_list_error:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->updateSpinner()V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mOlderReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mOlderReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    iput-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mOlderReqId:Ljava/lang/Integer;

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mLoaderError:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->access$000(Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    invoke-virtual {v0, v4, v3, v1}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_1
.end method
