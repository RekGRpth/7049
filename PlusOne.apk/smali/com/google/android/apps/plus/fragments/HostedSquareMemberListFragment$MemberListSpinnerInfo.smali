.class final Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;
.super Ljava/lang/Object;
.source "HostedSquareMemberListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MemberListSpinnerInfo"
.end annotation


# instance fields
.field private final mMemberListType:I

.field private final mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;->mName:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;->mMemberListType:I

    return-void
.end method


# virtual methods
.method public final getMemberListType()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;->mMemberListType:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;->mName:Ljava/lang/String;

    return-object v0
.end method
