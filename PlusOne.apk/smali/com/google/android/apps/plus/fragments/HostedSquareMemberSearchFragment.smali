.class public Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;
.super Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;
.source "HostedSquareMemberSearchFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;

.field private mListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    new-instance v0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->mSquareId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->isSquareAdmin()Z

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->setListener(Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;)V

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v1, Lcom/google/android/apps/plus/R$layout;->square_member_list_fragment:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-object v0
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showSearchView()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->getSearchViewAdapter()Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->search_people_hint_text:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setQueryHint(I)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->getSearchViewAdapter()Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->addOnChangeListener(Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;)V

    return-void
.end method

.method public final onQueryClose()V
    .locals 0

    return-void
.end method

.method public final onQueryTextChanged(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->setQueryString(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onQueryTextSubmitted(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    return-void
.end method

.method public final onResume()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->getView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->mListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->showContent(Landroid/view/View;)V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->onSaveInstanceState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->onStart()V

    return-void
.end method

.method public final onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SquareMemberSearchAdapter;->onStop()V

    return-void
.end method
