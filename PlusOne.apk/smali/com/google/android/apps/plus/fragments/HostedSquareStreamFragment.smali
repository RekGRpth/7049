.class public Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;
.super Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
.source "HostedSquareStreamFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$SquareStreamSpinnerInfo;
    }
.end annotation


# instance fields
.field private mAutoSubscribe:Z

.field private mCanSeePosts:Ljava/lang/Boolean;

.field private mCurrentSpinnerPosition:I

.field private mFirstStreamListLoad:Z

.field private mFragmentCreated:Z

.field private mGetSquareRequestId:Ljava/lang/Integer;

.field private mIsMember:Ljava/lang/Boolean;

.field private mOperationType:I

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$SquareStreamSpinnerInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSquareHeaderCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

.field protected mSquareId:Ljava/lang/String;

.field private mSquareIsExpanded:Ljava/lang/Boolean;

.field private mSquareLoaderActive:Z

.field private mSquareMembers:Lcom/google/android/apps/plus/content/AudienceData;

.field protected mSquareName:Ljava/lang/String;

.field private final mSquareServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

.field protected mStreamId:Ljava/lang/String;

.field protected mStreamName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOperationType:I

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareLoaderActive:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mFirstStreamListLoad:Z

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOperationType:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method private createSquareEmbed(Z)Lcom/google/android/apps/plus/content/DbEmbedSquare;
    .locals 6
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->getTagline()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->getPhotoUrl()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Lcom/google/android/apps/plus/content/DbEmbedSquare;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareName:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/content/DbEmbedSquare;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 9
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v8, 0x0

    const/4 v7, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "req_pending"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/DialogFragment;

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->updateSpinner()V

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v3

    instance-of v3, v3, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v3, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/api/OzServerException;->getUserErrorMessage(Landroid/content/Context;)Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;

    move-result-object v0

    :cond_3
    if-eqz v0, :cond_4

    iget-object v3, v0, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;->message:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x1080027

    invoke-static {v3, v4, v5, v8, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setCancelable(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v3, v4, v8}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_1
    iput v7, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOperationType:I

    goto :goto_0

    :cond_4
    iget v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOperationType:I

    packed-switch v3, :pswitch_data_0

    sget v1, Lcom/google/android/apps/plus/R$string;->operation_failed:I

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :pswitch_0
    sget v1, Lcom/google/android/apps/plus/R$string;->square_join_error:I

    goto :goto_2

    :pswitch_1
    sget v1, Lcom/google/android/apps/plus/R$string;->square_request_to_join_error:I

    goto :goto_2

    :pswitch_2
    sget v1, Lcom/google/android/apps/plus/R$string;->square_cancel_join_request_error:I

    goto :goto_2

    :pswitch_3
    sget v1, Lcom/google/android/apps/plus/R$string;->square_decline_invitation_error:I

    goto :goto_2

    :cond_5
    iget v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOperationType:I

    packed-switch v3, :pswitch_data_1

    goto :goto_1

    :pswitch_4
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mFirstStreamListLoad:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->refresh()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
    .end packed-switch
.end method

.method private refreshSquare()V
    .locals 3

    const-string v0, "HostedSquareStreamFrag"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedSquareStreamFrag"

    const-string v1, "refreshSquare"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareMembers:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getSquare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mGetSquareRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->updateSpinner()V

    return-void
.end method

.method private showPrimarySpinnerOrTitle(Lcom/google/android/apps/plus/views/HostActionBar;I)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {p1, v0, p2}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->hasSquareData()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showProgressDialog()V
    .locals 5

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOperationType:I

    packed-switch v2, :pswitch_data_0

    sget v1, Lcom/google/android/apps/plus/R$string;->loading:I

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "req_pending"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :pswitch_0
    sget v1, Lcom/google/android/apps/plus/R$string;->square_joining:I

    goto :goto_0

    :pswitch_1
    sget v1, Lcom/google/android/apps/plus/R$string;->square_sending_join_request:I

    goto :goto_0

    :pswitch_2
    sget v1, Lcom/google/android/apps/plus/R$string;->square_canceling_join_request:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updateComposeBar()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mIsMember:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->forceShow()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->forceHide()V

    goto :goto_0
.end method

.method private updateSelectedStream(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v0, "HostedSquareStreamFrag"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedSquareStreamFrag"

    const-string v1, "updateSelectedStream"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamName:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mFirstLoad:Z

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mContinuationToken:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->prepareLoaderUri()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "stream_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1, v4, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mResetAnimationState:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->updateComposeBar()V

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->refresh()V

    return-void
.end method


# virtual methods
.method protected final createStreamAdapter(Landroid/content/Context;Lcom/google/android/apps/plus/views/StreamGridView;Lcom/google/android/apps/plus/util/StreamLayoutInfo;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;Lcom/google/android/apps/plus/phone/ComposeBarController;Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)Lcom/google/android/apps/plus/phone/StreamAdapter;
    .locals 14
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p3    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p5    # Landroid/view/View$OnClickListener;
    .param p6    # Lcom/google/android/apps/plus/views/ItemClickListener;
    .param p7    # Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;
    .param p8    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;
    .param p9    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;
    .param p10    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;
    .param p11    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;
    .param p12    # Lcom/google/android/apps/plus/phone/ComposeBarController;
    .param p13    # Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    new-instance v0, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v13}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/StreamGridView;Lcom/google/android/apps/plus/util/StreamLayoutInfo;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;Lcom/google/android/apps/plus/phone/ComposeBarController;Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)V

    return-object v0
.end method

.method protected final fetchContent(Z)V
    .locals 9
    .param p1    # Z

    const/4 v3, 0x0

    const/4 v2, 0x4

    const-string v0, "HostedSquareStreamFrag"

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedSquareStreamFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "fetchContent - newer="

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->showEmptyStream()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-nez p1, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mEndOfStream:Z

    if-nez v0, :cond_1

    :cond_3
    if-eqz p1, :cond_5

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mContinuationToken:Ljava/lang/String;

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mContinuationToken:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/service/EsService;->getActivityStream(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    if-eqz p1, :cond_6

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mNewerReqId:Ljava/lang/Integer;

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->updateSpinner()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mContinuationToken:Ljava/lang/String;

    if-nez v0, :cond_4

    goto :goto_0

    :cond_6
    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOlderReqId:Ljava/lang/Integer;

    goto :goto_1
.end method

.method public final getExtrasForLogging()Landroid/os/Bundle;
    .locals 2

    const-string v0, "extra_square_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method protected final getStreamHeaderCursor()Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareHeaderCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_LANDING:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final handleGetSquareServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mGetSquareRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mGetSquareRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mError:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->updateServerErrorView()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->hasSquareData()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->people_list_error:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mGetSquareRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->updateSpinner()V

    goto :goto_0
.end method

.method protected final initCirclesLoader()V
    .locals 0

    return-void
.end method

.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isProgressIndicatorVisible()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isProgressIndicatorVisible()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareLoaderActive:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mGetSquareRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onBlockingHelpLinkClicked(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->startExternalActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_6

    const-string v0, "square_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "square_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mGetSquareRequestId:Ljava/lang/Integer;

    :cond_0
    const-string v0, "pending_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "pending_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_1
    const-string v0, "square_expanded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "square_expanded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareIsExpanded:Ljava/lang/Boolean;

    :cond_2
    const-string v0, "square_members"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "square_members"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareMembers:Lcom/google/android/apps/plus/content/AudienceData;

    :cond_3
    const-string v0, "square_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "square_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareName:Ljava/lang/String;

    :cond_4
    const-string v0, "square_stream_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "square_stream_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamName:Ljava/lang/String;

    :cond_5
    const-string v0, "operation_type"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOperationType:I

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mFragmentCreated:Z

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/16 v1, 0x64

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void

    :cond_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mFragmentCreated:Z

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    new-instance v0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamId:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :sswitch_1
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/content/EsSquaresData;->SQUARES_PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    check-cast v1, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->setOnClickListener(Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareIsExpanded:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->setViewIsExpanded(Ljava/lang/Boolean;)V

    return-object v0
.end method

.method public final onDeclineInvitationClicked()V
    .locals 5

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/plus/R$string;->square_dismiss_invitation_text:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->square_dialog_decline_button:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "decline_invitation"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v2, "decline_invitation"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x4

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOperationType:I

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->declineSquareInvitation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    const-string v2, "extra_square_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_DECLINE_INVITATION:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_LANDING:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v0, v2, v3, v4, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public final onExpandClicked(Z)V
    .locals 3
    .param p1    # Z

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareIsExpanded:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->setViewIsExpanded(Ljava/lang/Boolean;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->triggerStreamObservers(ZI)V

    return-void
.end method

.method public final onInviteClicked()V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getPostTextActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "square_embed"

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->createSquareEmbed(Z)Lcom/google/android/apps/plus/content/DbEmbedSquare;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "disable_location"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "circle_usage_type"

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "filter_null_gaia_ids"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onJoinButtonClicked(I)V
    .locals 7
    .param p1    # I

    const/4 v3, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_LANDING:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v1

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOperationType:I

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAutoSubscribe:Z

    if-eqz v3, :cond_0

    const-string v3, "JOIN_WITH_SUBSCRIPTION"

    :goto_1
    invoke-static {v0, v4, v5, v6, v3}, Lcom/google/android/apps/plus/service/EsService;->editSquareMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->showProgressDialog()V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAutoSubscribe:Z

    if-eqz v3, :cond_1

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_JOIN_WITH_SUBSCRIPTION:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_2
    invoke-static {v0, v4, v3, v2, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    const-string v3, "JOIN"

    goto :goto_1

    :cond_1
    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_JOIN:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_2

    :pswitch_1
    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOperationType:I

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAutoSubscribe:Z

    if-eqz v3, :cond_2

    const-string v3, "ACCEPT_INVITATION_WITH_SUBSCRIPTION"

    :goto_3
    invoke-static {v0, v4, v5, v6, v3}, Lcom/google/android/apps/plus/service/EsService;->editSquareMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->showProgressDialog()V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAutoSubscribe:Z

    if-eqz v3, :cond_3

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_ACCEPT_INVITATION_WITH_SUBSCRIPTION:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_4
    invoke-static {v0, v4, v3, v2, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_2
    const-string v3, "ACCEPT_INVITATION"

    goto :goto_3

    :cond_3
    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_ACCEPT_INVITATION:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_4

    :pswitch_2
    const/4 v3, 0x2

    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOperationType:I

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAutoSubscribe:Z

    if-eqz v3, :cond_4

    const-string v3, "APPLY_TO_JOIN_WITH_SUBSCRIPTION"

    :goto_5
    invoke-static {v0, v4, v5, v6, v3}, Lcom/google/android/apps/plus/service/EsService;->editSquareMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->showProgressDialog()V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAutoSubscribe:Z

    if-eqz v3, :cond_5

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_APPLY_TO_JOIN_WITH_SUBSCRIPTION:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_6
    invoke-static {v0, v4, v3, v2, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_4
    const-string v3, "APPLY_TO_JOIN"

    goto :goto_5

    :cond_5
    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_APPLY_TO_JOIN:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_6

    :pswitch_3
    const/4 v3, 0x3

    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOperationType:I

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    const-string v5, "CANCEL_JOIN_REQUEST"

    invoke-static {v0, v3, v4, v6, v5}, Lcom/google/android/apps/plus/service/EsService;->editSquareMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->showProgressDialog()V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_CANCEL_JOIN_REQUEST:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v3, v4, v2, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 15
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    invoke-super/range {p0 .. p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    :goto_0
    return-void

    :pswitch_0
    const-string v4, "HostedSquareStreamFrag"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "HostedSquareStreamFrag"

    const-string v5, "onLoadFinished - SquareLoader"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareLoaderActive:Z

    const/4 v3, 0x0

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_15

    const/16 v4, 0x15

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v1

    const-wide/32 v6, 0xdbba0

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    const/4 v3, 0x1

    :cond_1
    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-lez v4, :cond_14

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->setSquareData(Landroid/database/Cursor;)V

    const/16 v4, 0xc

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_7

    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mCanSeePosts:Ljava/lang/Boolean;

    const/16 v4, 0x8

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x1

    :goto_2
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mIsMember:Ljava/lang/Boolean;

    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareName:Ljava/lang/String;

    const/16 v4, 0x17

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_9

    const/4 v4, 0x1

    :goto_3
    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAutoSubscribe:Z

    const/16 v4, 0x12

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/content/DbSquareStream;->deserialize([B)[Lcom/google/android/apps/plus/content/DbSquareStream;

    move-result-object v10

    if-eqz v10, :cond_a

    array-length v6, v10

    :goto_4
    const/4 v4, 0x1

    if-ne v6, v4, :cond_b

    const/4 v6, 0x0

    const/4 v4, 0x0

    aget-object v4, v10, v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbSquareStream;->getStreamId()Ljava/lang/String;

    move-result-object v5

    const/4 v4, 0x0

    aget-object v4, v10, v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbSquareStream;->getName()Ljava/lang/String;

    move-result-object v4

    move v7, v6

    move-object v6, v5

    move-object v5, v4

    :goto_5
    const/4 v4, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v8}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {v4, v8}, Ljava/lang/Math;->max(II)I

    move-result v9

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mFirstStreamListLoad:Z

    if-nez v4, :cond_2

    if-ne v7, v9, :cond_2

    if-eqz v6, :cond_c

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamId:Ljava/lang/String;

    invoke-static {v6, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    :cond_2
    const/4 v4, 0x1

    move v8, v4

    :goto_6
    const-string v4, "HostedSquareStreamFrag"

    const/4 v11, 0x3

    invoke-static {v4, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "HostedSquareStreamFrag"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "populatePrimarySpinner firstLoad="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v12, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mFirstStreamListLoad:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " numStreams="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " old="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, " streamId="

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, " old="

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamId:Ljava/lang/String;

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mFirstStreamListLoad:Z

    if-nez v8, :cond_4

    const/4 v4, 0x0

    move v9, v4

    :goto_7
    if-ge v9, v7, :cond_4

    aget-object v4, v10, v9

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbSquareStream;->getStreamId()Ljava/lang/String;

    move-result-object v11

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    add-int/lit8 v12, v9, 0x1

    invoke-virtual {v4, v12}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$SquareStreamSpinnerInfo;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$SquareStreamSpinnerInfo;->getStreamId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v11, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    const/4 v8, 0x1

    :cond_4
    if-eqz v8, :cond_10

    const/4 v8, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4}, Landroid/widget/ArrayAdapter;->clear()V

    if-lez v7, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    new-instance v9, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$SquareStreamSpinnerInfo;

    sget v11, Lcom/google/android/apps/plus/R$string;->square_all_categories:I

    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-direct {v9, v11, v12}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$SquareStreamSpinnerInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    :cond_5
    const/4 v4, 0x0

    move v14, v4

    move v4, v8

    move v8, v14

    :goto_8
    if-ge v8, v7, :cond_e

    aget-object v9, v10, v8

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/DbSquareStream;->getStreamId()Ljava/lang/String;

    move-result-object v9

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    new-instance v12, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$SquareStreamSpinnerInfo;

    aget-object v13, v10, v8

    invoke-virtual {v13}, Lcom/google/android/apps/plus/content/DbSquareStream;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13, v9}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$SquareStreamSpinnerInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11, v12}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamId:Ljava/lang/String;

    invoke-static {v11, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    add-int/lit8 v4, v8, 0x1

    :cond_6
    add-int/lit8 v8, v8, 0x1

    goto :goto_8

    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_a
    const/4 v6, 0x0

    goto/16 :goto_4

    :cond_b
    const/4 v5, 0x0

    const/4 v4, 0x0

    move v7, v6

    move-object v6, v5

    move-object v5, v4

    goto/16 :goto_5

    :cond_c
    const/4 v4, 0x0

    move v8, v4

    goto/16 :goto_6

    :cond_d
    add-int/lit8 v4, v9, 0x1

    move v9, v4

    goto :goto_7

    :cond_e
    const-string v7, "HostedSquareStreamFrag"

    const/4 v8, 0x4

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_f

    const-string v7, "HostedSquareStreamFrag"

    const-string v8, "primary spinner changed"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    const/4 v7, -0x1

    iput v7, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v7

    invoke-direct {p0, v7, v4}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->showPrimarySpinnerOrTitle(Lcom/google/android/apps/plus/views/HostActionBar;I)V

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_10

    invoke-direct {p0, v6, v5}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->updateSelectedStream(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->updateComposeBar()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->showContent(Landroid/view/View;)V

    const-string v4, "HostedSquareStreamFrag"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_11

    const-string v4, "HostedSquareStreamFrag"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "- setSquareData name="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    :goto_9
    if-eqz v3, :cond_12

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->refreshSquare()V

    :cond_12
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->hasSquareData()Z

    move-result v5

    if-eqz v5, :cond_13

    new-instance v4, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "_id"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-direct {v4, v5, v6}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;I)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Integer;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->showEmptyStream()Z

    move-result v5

    if-eqz v5, :cond_13

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Integer;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_13
    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareHeaderCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->updateSpinner()V

    goto/16 :goto_0

    :cond_14
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getView()Landroid/view/View;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_9

    :cond_15
    const/4 v3, 0x1

    goto :goto_9

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public final onMembersClicked()V
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->getMembershipStatus()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->getJoinability()I

    move-result v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/phone/Intents;->getSquareMembersListActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;II)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$layout;->simple_spinner_item:I

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->showPrimarySpinnerOrTitle(Lcom/google/android/apps/plus/views/HostActionBar;I)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    return-void
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .locals 3
    .param p1    # I

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mCurrentSpinnerPosition:I

    if-eq v2, p1, :cond_0

    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mCurrentSpinnerPosition:I

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$SquareStreamSpinnerInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$SquareStreamSpinnerInfo;->getStreamId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$SquareStreamSpinnerInfo;->getStreamName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->updateSelectedStream(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onResume()V

    sget-object v1, Lcom/google/android/apps/plus/util/Property;->ENABLE_SQUARES:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mGetSquareRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mGetSquareRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mGetSquareRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mGetSquareRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->handleGetSquareServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mFragmentCreated:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mFragmentCreated:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->refreshSquare()V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->updateSpinner()V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mGetSquareRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "square_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mGetSquareRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    const-string v0, "pending_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareMembers:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v0, :cond_2

    const-string v0, "square_members"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareMembers:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareName:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v0, "square_name"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamName:Ljava/lang/String;

    if-eqz v0, :cond_4

    const-string v0, "square_stream_name"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareIsExpanded:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    const-string v0, "square_expanded"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareIsExpanded:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_5
    const-string v0, "operation_type"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOperationType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onSetArguments(Landroid/os/Bundle;)V

    const-string v0, "square_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    const-string v0, "stream_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamId:Ljava/lang/String;

    return-void
.end method

.method public final onSettingsClicked()V
    .locals 6

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->getSettings()Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->getVisibility()I

    move-result v5

    invoke-static {v2, v3, v4, v5, v1}, Lcom/google/android/apps/plus/phone/Intents;->getSquareSettingsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ILcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onShareClicked()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getPostTextActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "square_embed"

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->createSquareEmbed(Z)Lcom/google/android/apps/plus/content/DbEmbedSquare;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "disable_location"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected final prepareLoaderUri()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->buildSquareStreamKey(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mPostsUri:Landroid/net/Uri;

    return-void
.end method

.method public final refresh()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->refresh()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->refreshSquare()V

    return-void
.end method

.method protected final showEmptyStream()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mCanSeePosts:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mCanSeePosts:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final startActivityForCompose(Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v2, Lcom/google/android/apps/plus/content/SquareTargetData;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamId:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, ""

    :goto_0
    invoke-direct {v2, v3, v4, v5, v1}, Lcom/google/android/apps/plus/content/SquareTargetData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/SquareTargetData;)V

    :cond_0
    const-string v1, "audience"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivityForCompose(Landroid/content/Intent;)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mStreamName:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final startStreamOneUp(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mSquareStreamAdapter:Lcom/google/android/apps/plus/phone/SquareStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SquareStreamAdapter;->isSquareAdmin()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "square_admin"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    const-string v0, "refresh"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startStreamOneUp(Landroid/content/Intent;)V

    return-void
.end method
