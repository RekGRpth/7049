.class public final Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;
.super Lcom/google/android/apps/plus/phone/EsMatrixCursor;
.source "HostedStreamFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CircleSettingsStreamCursor"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mCircleAvatarsCursor:Landroid/database/Cursor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "member_count"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "subscribed"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(IZLandroid/database/Cursor;)V
    .locals 4
    .param p1    # I
    .param p2    # Z
    .param p3    # Landroid/database/Cursor;

    const/4 v0, 0x0

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;->PROJECTION:[Ljava/lang/String;

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;I)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    if-eqz p2, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;->addRow([Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;->mCircleAvatarsCursor:Landroid/database/Cursor;

    return-void
.end method


# virtual methods
.method public final getCircleAvatarsCursor()Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;->mCircleAvatarsCursor:Landroid/database/Cursor;

    return-object v0
.end method
