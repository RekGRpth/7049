.class final Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;
.super Ljava/lang/Object;
.source "HostedStreamFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CircleSpinnerInfo"
.end annotation


# instance fields
.field private final mCircleId:Ljava/lang/String;

.field private final mCircleName:Ljava/lang/String;

.field private mMemberCount:I

.field private final mRealCircleId:Ljava/lang/String;

.field private mSubscribed:Z

.field private final mView:I

.field private mVolume:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZI)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Z
    .param p6    # I

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mRealCircleId:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mMemberCount:I

    iput-boolean p5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mSubscribed:Z

    iput p6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mVolume:I

    const-string v0, "v.all.circles"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mView:I

    sget v0, Lcom/google/android/apps/plus/R$string;->stream_circles:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleName:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleId:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const-string v0, "v.whatshot"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mView:I

    sget v0, Lcom/google/android/apps/plus/R$string;->stream_whats_hot:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleName:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleId:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v0, "v.nearby"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mView:I

    sget v0, Lcom/google/android/apps/plus/R$string;->stream_nearby:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleName:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleId:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mView:I

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final getCircleId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleId:Ljava/lang/String;

    return-object v0
.end method

.method public final getCircleName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleName:Ljava/lang/String;

    return-object v0
.end method

.method public final getMemberCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mMemberCount:I

    return v0
.end method

.method public final getRealCircleId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mRealCircleId:Ljava/lang/String;

    return-object v0
.end method

.method public final getView()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mView:I

    return v0
.end method

.method public final getVolume()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mVolume:I

    return v0
.end method

.method public final isSubscribed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mSubscribed:Z

    return v0
.end method

.method public final setMemberCount(I)I
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mMemberCount:I

    return p1
.end method

.method public final setSubscribed(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mSubscribed:Z

    return-void
.end method

.method public final setVolume(I)I
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mVolume:I

    return p1
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleName:Ljava/lang/String;

    return-object v0
.end method
