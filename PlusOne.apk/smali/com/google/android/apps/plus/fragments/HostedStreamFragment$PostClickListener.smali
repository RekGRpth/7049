.class public final Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;
.super Ljava/lang/Object;
.source "HostedStreamFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x14
    name = "PostClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAvatarClick$16da05f7(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "view_as_plus_page"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    if-nez v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamOwnerUserId:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v1, :cond_0

    :cond_3
    const-string v2, "extra_gaia_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v3, v3, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamOwnerUserId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v4, v4, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v5

    invoke-static {v3, v4, v2, v5, v0}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v4, v4, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x0

    invoke-static {v3, v4, p1, v5}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .locals 0
    .param p1    # Landroid/text/style/URLSpan;

    return-void
.end method
