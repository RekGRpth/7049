.class public final Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "HostedStreamFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreatePostPlusOne$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->plusone_error:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public final onDeletePostPlusOne$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->delete_plusone_error:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public final onEditModerationStateComplete$1b131e9(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEditModerationStateRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->access$400(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEditModerationStateRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->access$400(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->handleOnEditModerationStateCallback(Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V
    invoke-static {v0, p3, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->access$500(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onGetActivities$35a362dd(IZILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 6
    .param p1    # I
    .param p2    # Z
    .param p3    # I
    .param p4    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    if-eqz p4, :cond_2

    invoke-virtual {p4}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v2, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    const-string v0, "HostedStreamFrag"

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedStreamFrag"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onGetActivities - mError="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-boolean v3, v3, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p2, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iput-object v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNewerReqId:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    # setter for: Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamChangeLastCheckTimeMs:J
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->access$102(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;J)J

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->access$200(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;Z)V

    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->access$300(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    if-eqz v0, :cond_7

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    # setter for: Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I
    invoke-static {v0, p3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->access$302(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;I)I

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v0, v1, v4, v2}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateServerErrorView()V

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mOlderReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mOlderReqId:Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iput-object v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mOlderReqId:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iput-boolean v1, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPreloadRequested:Z

    const-string v0, "HostedStreamFrag"

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "HostedStreamFrag"

    const-string v1, "onGetActivities - mPreloadRequested=false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public final onPromoSetCircleMembershipComplete$2f8b9cab(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 6
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    move-object v1, p3

    move v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->access$000(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;Lcom/google/android/apps/plus/service/ServiceResult;ILjava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method
