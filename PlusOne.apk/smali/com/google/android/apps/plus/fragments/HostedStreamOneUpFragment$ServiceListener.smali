.class final Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "HostedStreamOneUpFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;ILcom/google/android/apps/plus/service/ServiceResult;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    return v0
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$800(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$800(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$802(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2000(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mOperationType:I
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2600(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    sget v0, Lcom/google/android/apps/plus/R$string;->operation_failed:I

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2700(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :sswitch_0
    sget v0, Lcom/google/android/apps/plus/R$string;->reshare_post_error:I

    goto :goto_1

    :sswitch_1
    sget v0, Lcom/google/android/apps/plus/R$string;->remove_post_error:I

    goto :goto_1

    :sswitch_2
    sget v0, Lcom/google/android/apps/plus/R$string;->mute_activity_error:I

    goto :goto_1

    :sswitch_3
    sget v0, Lcom/google/android/apps/plus/R$string;->report_activity_error:I

    goto :goto_1

    :sswitch_4
    sget v0, Lcom/google/android/apps/plus/R$string;->comment_post_error:I

    goto :goto_1

    :sswitch_5
    sget v0, Lcom/google/android/apps/plus/R$string;->comment_delete_error:I

    goto :goto_1

    :sswitch_6
    sget v0, Lcom/google/android/apps/plus/R$string;->comment_moderate_error:I

    goto :goto_1

    :sswitch_7
    sget v0, Lcom/google/android/apps/plus/R$string;->get_acl_error:I

    goto :goto_1

    :sswitch_8
    sget v0, Lcom/google/android/apps/plus/R$string;->photo_tag_approve_error:I

    goto :goto_1

    :sswitch_9
    sget v0, Lcom/google/android/apps/plus/R$string;->photo_tag_deny_error:I

    goto :goto_1

    :sswitch_a
    sget v0, Lcom/google/android/apps/plus/R$string;->remove_post_error:I

    goto :goto_1

    :sswitch_b
    sget v0, Lcom/google/android/apps/plus/R$string;->restore_post_error:I

    goto :goto_1

    :sswitch_c
    sget v0, Lcom/google/android/apps/plus/R$string;->square_ban_member_error:I

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mOperationType:I
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2600(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    :cond_3
    :goto_2
    const/4 v1, 0x1

    goto :goto_0

    :sswitch_d
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityMuted:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2800(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Z

    move-result v1

    if-nez v1, :cond_3

    :sswitch_e
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x11 -> :sswitch_2
        0x12 -> :sswitch_3
        0x13 -> :sswitch_0
        0x20 -> :sswitch_4
        0x21 -> :sswitch_5
        0x22 -> :sswitch_6
        0x30 -> :sswitch_7
        0x50 -> :sswitch_b
        0x51 -> :sswitch_a
        0x52 -> :sswitch_c
        0x53 -> :sswitch_c
        0x60 -> :sswitch_8
        0x61 -> :sswitch_9
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x10 -> :sswitch_e
        0x11 -> :sswitch_d
        0x12 -> :sswitch_e
        0x51 -> :sswitch_e
        0x53 -> :sswitch_e
    .end sparse-switch
.end method


# virtual methods
.method public final onCreateComment$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 7
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v6, 0x0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$500(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$500(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v1

    instance-of v2, v1, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v2

    const/16 v3, 0xe

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2000(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    sget v3, Lcom/google/android/apps/plus/R$string;->post_not_sent_title:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    sget v4, Lcom/google/android/apps/plus/R$string;->post_restricted_mention_error:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    sget v5, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "StreamPostRestrictionsNotSupported"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onCreatePostPlusOne$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2200(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->plusone_error:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public final onDeleteActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method

.method public final onDeleteComment$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method

.method public final onDeletePostPlusOne$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2300(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->delete_plusone_error:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public final onEditActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method

.method public final onEditComment$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method

.method public final onEditModerationStateComplete$1b131e9(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method

.method public final onEditSquareMembershipComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Z
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method

.method public final onGetActivity$51e3eb1f(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1100(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1500(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1500(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1502(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->updateProgressIndicator()V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1602(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Z)Z

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityDataNotFound:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1700(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1800(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    sget v3, Lcom/google/android/apps/plus/R$string;->comments_activity_not_found:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1300(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1300(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1900(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Landroid/view/View;)V

    goto :goto_0
.end method

.method public final onGetActivityAudience$6db92636(ILcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/AudienceData;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # setter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2402(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showAudience(Lcom/google/android/apps/plus/content/AudienceData;)V
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2500(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Lcom/google/android/apps/plus/content/AudienceData;)V

    :cond_0
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method

.method public final onModerateComment$56b78e3(ILjava/lang/String;ZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1300(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->removeFlaggedComment(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1300(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->addFlaggedComment(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onMuteActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method

.method public final onNameTagApprovalComplete$4894d499(IJLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # J
    .param p4    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method

.method public final onPlusOneComment$56b78e3(ZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # Z
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2100(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;

    move-result-object v1

    if-eqz p1, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$string;->plusone_error:I

    :goto_0
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$string;->delete_plusone_error:I

    goto :goto_0
.end method

.method public final onRemoveReportAndBan$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method

.method public final onReportActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method

.method public final onReshareActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method

.method public final onTagSuggestionApprovalComplete$63505a2b(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method
