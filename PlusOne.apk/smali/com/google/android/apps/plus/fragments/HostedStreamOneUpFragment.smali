.class public Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;
.super Lcom/google/android/apps/plus/phone/HostedFragment;
.source "HostedStreamOneUpFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog$BanPostAuthorListener;
.implements Lcom/google/android/apps/plus/fragments/EditModerationStateDialog$ModerationListener;
.implements Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
.implements Lcom/google/android/apps/plus/views/OneUpListener;
.implements Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;,
        Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$MyTextWatcher;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/HostedFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog$BanPostAuthorListener;",
        "Lcom/google/android/apps/plus/fragments/EditModerationStateDialog$ModerationListener;",
        "Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;",
        "Lcom/google/android/apps/plus/views/OneUpListener;",
        "Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;"
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mActivityDataNotFound:Z

.field private mActivityId:Ljava/lang/String;

.field private mActivityRequestId:Ljava/lang/Integer;

.field private mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

.field private mAppInviteData:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

.field private mAppLinkHelper:Lcom/google/android/apps/plus/util/DeepLinkHelper;

.field private mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

.field private mAuthorId:Ljava/lang/String;

.field private mAutoPlay:Z

.field private mCommentButton:Landroid/view/View;

.field private mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

.field private mContentDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

.field private mCreationSource:Ljava/lang/String;

.field private mCreationSourceId:Ljava/lang/String;

.field private mEditableText:Ljava/lang/String;

.field private mFlaggedComments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mGetActivityComplete:Z

.field private mIsActivityMuted:Z

.field private mIsGraySpam:Z

.field private mIsLimited:Z

.field private mIsMyActivity:Ljava/lang/Boolean;

.field private mIsSquarePost:Z

.field private mListView:Landroid/widget/ListView;

.field private mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

.field private mMuteProcessed:Z

.field private mOperationType:I

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mPlusOnedByData:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPostLink:Ljava/lang/String;

.field private mReadProcessed:Z

.field private mReshare:Z

.field private final mServiceListener:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;

.field private mShowKeyboard:Z

.field private mSourceAuthorId:Ljava/lang/String;

.field private mSourcePackageName:Ljava/lang/String;

.field private mSquareId:Ljava/lang/String;

.field private mStageMediaLoaded:Z

.field private mTextWatcher:Landroid/text/TextWatcher;

.field private mViewerIsSquareAdmin:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mOperationType:I

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;I)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;
    .param p1    # I

    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;
    .param p1    # Ljava/lang/Integer;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mGetActivityComplete:Z

    return v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityDataNotFound:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Landroid/view/View;)V
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;
    .param p1    # Landroid/view/View;

    const/16 v0, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$id;->loading:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    sget v3, Lcom/google/android/apps/plus/R$id;->stage:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mStageMediaLoaded:Z

    if-eqz v3, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mGetActivityComplete:Z

    if-eqz v3, :cond_3

    :goto_2
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "hsouf_pending"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2402(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Lcom/google/android/apps/plus/content/AudienceData;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    return-void
.end method

.method static synthetic access$2600(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mOperationType:I

    return v0
.end method

.method static synthetic access$2700(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2800(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityMuted:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mShowKeyboard:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mShowKeyboard:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->isPaused()Z

    move-result v0

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mMuteProcessed:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method private doReportComment(ZLjava/lang/String;Z)V
    .locals 6
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const-string v4, "extra_comment_id"

    invoke-static {v4, p2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v4, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    if-eqz p3, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_undo_report_dialog_title:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    if-eqz p3, :cond_1

    sget v4, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_undo_report_dialog_question:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    sget v4, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v2, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, p0, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "comment_id"

    invoke-virtual {v4, v5, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "delete"

    invoke-virtual {v4, v5, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "is_undo"

    invoke-virtual {v4, v5, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "hsouf_report_comment"

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_0
    sget v4, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_report_dialog_title:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    sget v4, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_report_dialog_question:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private launchDeepLink(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAppLinkHelper:Lcom/google/android/apps/plus/util/DeepLinkHelper;

    const-string v1, "StreamOneUp"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCreationSourceId:Ljava/lang/String;

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move v8, p6

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/plus/util/DeepLinkHelper;->launchDeepLink$5724b368(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method private logSquareAction(Lcom/google/android/apps/plus/analytics/OzActions;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/analytics/OzActions;
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "extra_activity_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSquareId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "extra_square_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSquareId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "extra_gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x0

    :cond_3
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    return-void
.end method

.method private showAudience(Lcom/google/android/apps/plus/content/AudienceData;)V
    .locals 10
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    const-string v7, "StreamOneUp"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "StreamOneUp"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Hidden count: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getHiddenUserCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "StreamOneUp"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Audience users: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v1

    array-length v5, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v6, v1, v4

    const-string v7, "StreamOneUp"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Users: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getAclText()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;

    invoke-direct {v3}, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v7, "account"

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2, v7, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v7, "audience"

    invoke-virtual {v2, v7, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v7, "people_list_title"

    invoke-virtual {v2, v7, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "hsouf_audience"

    invoke-virtual {v3, v7, v8}, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showPlusOnePeople(Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    new-instance v1, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account"

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "plus_one_id"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "total_plus_ones"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "hsouf_plus_ones"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showProgressDialog(I)V
    .locals 4
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mOperationType:I

    const/16 v1, 0x30

    if-ne p1, v1, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$string;->loading:I

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "hsouf_pending"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_0
    sget v1, Lcom/google/android/apps/plus/R$string;->post_operation_pending:I

    goto :goto_0
.end method


# virtual methods
.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getShouldAutoPlay()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAutoPlay:Z

    return v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAppLinkHelper:Lcom/google/android/apps/plus/util/DeepLinkHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/util/DeepLinkHelper;->handleActivityResult$6eb84b56(II)Z

    return-void
.end method

.method public final onAppInviteButtonClicked(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedDeepLink;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getClientPackageNames()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getDeepLinkId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAppLinkHelper:Lcom/google/android/apps/plus/util/DeepLinkHelper;

    const-string v1, "StreamOneUp"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {p3}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getClientPackageNames()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {p3}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getDeepLinkId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getUrl()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAuthorId:Ljava/lang/String;

    const/4 v8, 0x1

    move-object v3, p1

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/plus/util/DeepLinkHelper;->launchDeepLink$5724b368(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p3}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p3}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v2, p1, v3}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onAvatarClick$16da05f7(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "extra_gaia_id"

    invoke-static {v2, p1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    invoke-static {v0, v2, p1, v3}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onBanPostAuthor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCreationSourceId:Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v6, p4

    move v7, p5

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/service/EsService;->removeReportAndBan(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-nez p4, :cond_0

    if-eqz p5, :cond_1

    :cond_0
    const/16 v0, 0x53

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_BAN_MEMBER_VIA_POST_OPTIONS_MENU:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->logSquareAction(Lcom/google/android/apps/plus/analytics/OzActions;Ljava/lang/String;)V

    return-void

    :cond_1
    const/16 v0, 0x52

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sget v6, Lcom/google/android/apps/plus/R$id;->footer_post_button:I

    if-ne v3, v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-nez v6, :cond_0

    const-string v6, "extra_activity_id"

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_POST_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v6, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v5}, Lcom/google/android/apps/plus/api/ApiUtils;->buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v1, v6, v7, v4}, Lcom/google/android/apps/plus/service/EsService;->createComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v6, 0x20

    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    invoke-virtual {v6, v8}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    sget v6, Lcom/google/android/apps/plus/R$id;->gray_spam_bar:I

    if-ne v3, v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSquareId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;

    move-result-object v6

    invoke-virtual {v6, p0, v8}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "hsouf_moderation"

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAppInviteData:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAppInviteData:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getClientPackageNames()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAppInviteData:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getDeepLinkId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCreationSource:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAppInviteData:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getClientPackageNames()Ljava/util/ArrayList;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAppInviteData:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getDeepLinkId()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAppInviteData:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getUrl()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAuthorId:Ljava/lang/String;

    const/4 v6, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->launchDeepLink(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAppInviteData:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAppInviteData:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCreationSourceId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onCommentButtonClicked()V
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mShowKeyboard:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final onCommentClicked(Lcom/google/android/apps/plus/views/StreamOneUpCommentView;)V
    .locals 15
    .param p1    # Lcom/google/android/apps/plus/views/StreamOneUpCommentView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getAuthorId()Ljava/lang/String;

    move-result-object v0

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v12, v0}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v3

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v13, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    invoke-virtual {v13}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getActivityAuthorId()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v4

    new-instance v7, Ljava/util/ArrayList;

    const/4 v12, 0x5

    invoke-direct {v7, v12}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v5, Ljava/util/ArrayList;

    const/4 v12, 0x5

    invoke-direct {v5, v12}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPlusOneByMe()Z

    move-result v8

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPlusOneId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPlusOneCount()I

    move-result v9

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->isFlagged()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v8, :cond_5

    sget v12, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_option_plusminus:I

    :goto_0
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v12, 0x25

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz v3, :cond_6

    sget v12, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_option_edit:I

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v12, 0x26

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    if-nez v4, :cond_1

    if-nez v3, :cond_1

    iget-boolean v12, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mViewerIsSquareAdmin:Z

    if-eqz v12, :cond_2

    :cond_1
    sget v12, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_option_delete:I

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v12, 0x21

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-boolean v12, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mViewerIsSquareAdmin:Z

    if-eqz v12, :cond_3

    if-nez v3, :cond_3

    sget v12, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_option_ban_comment_author:I

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v12, 0x52

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    if-eqz v10, :cond_4

    if-lez v9, :cond_4

    sget v12, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_option_plus_ones:I

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v12, 0x40

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v12

    new-array v6, v12, [Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    sget v12, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_options_title:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    const/4 v12, 0x0

    invoke-virtual {v1, p0, v12}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "comment_action"

    invoke-virtual {v12, v13, v5}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "comment_id"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getCommentId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "comment_content"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getCommentContent()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "plus_one_id"

    invoke-virtual {v12, v13, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "plus_one_by_me"

    invoke-virtual {v12, v13, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "plus_one_count"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPlusOneCount()I

    move-result v14

    invoke-virtual {v12, v13, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "comment_author_id"

    invoke-virtual {v12, v13, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v12

    const-string v13, "hsouf_delete_comment"

    invoke-virtual {v1, v12, v13}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_5
    sget v12, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_option_plusone:I

    goto/16 :goto_0

    :cond_6
    if-eqz v2, :cond_7

    sget v12, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_option_undo_report:I

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v12, 0x23

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_7
    sget v12, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_option_report:I

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v4, :cond_8

    const/16 v12, 0x24

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_8
    const/16 v12, 0x22

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v2, "activity_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    const-string v2, "square_admin"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mViewerIsSquareAdmin:Z

    const-string v2, "square_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSquareId:Ljava/lang/String;

    new-instance v2, Lcom/google/android/apps/plus/util/DeepLinkHelper;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/google/android/apps/plus/util/DeepLinkHelper;-><init>(Lcom/google/android/apps/plus/phone/HostedFragment;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAppLinkHelper:Lcom/google/android/apps/plus/util/DeepLinkHelper;

    const-string v2, "show_keyboard"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mShowKeyboard:Z

    if-eqz p1, :cond_4

    const-string v2, "pending_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "pending_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_0
    const-string v2, "activity_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "activity_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    :cond_1
    const-string v2, "audience_data"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    const-string v2, "flagged_comments"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_2
    const-string v2, "operation_type"

    invoke-virtual {p1, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mOperationType:I

    const-string v2, "mute_processed"

    invoke-virtual {p1, v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mMuteProcessed:Z

    const-string v2, "read_processed"

    invoke-virtual {p1, v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mReadProcessed:Z

    const-string v2, "source_package_name"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSourcePackageName:Ljava/lang/String;

    const-string v2, "source_author_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSourceAuthorId:Ljava/lang/String;

    const-string v2, "get_activity_complete"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mGetActivityComplete:Z

    const-string v2, "stage_media_loaded"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mStageMediaLoaded:Z

    const-string v2, "show_keyboard"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mShowKeyboard:Z

    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAutoPlay:Z

    :cond_3
    :goto_0
    return-void

    :cond_4
    const-string v2, "refresh"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->refresh()V

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    packed-switch p1, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1efab34d
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v5, 0x1

    const/4 v9, 0x0

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v1

    sget v4, Lcom/google/android/apps/plus/R$layout;->stream_one_up_fragment:I

    invoke-virtual {p1, v4, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    const v4, 0x102000a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Landroid/widget/ListView;

    new-instance v4, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    invoke-direct {v4, v1, v9, p0, p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/apps/plus/views/OneUpListener;Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;)V

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->setLoading(Z)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Landroid/widget/ListView;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    invoke-virtual {v4, v7}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget v4, Lcom/google/android/apps/plus/R$id;->footer:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    sget v4, Lcom/google/android/apps/plus/R$id;->footer_text:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v4, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setEnabled(Z)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v4, v9}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v0, v1, v4, v7}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {v4, p0, v7, v8, v9}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->init(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/views/AudienceView;)V

    sget v4, Lcom/google/android/apps/plus/R$id;->footer_post_button:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    if-lez v4, :cond_0

    move v4, v5

    :goto_0
    invoke-virtual {v7, v4}, Landroid/view/View;->setEnabled(Z)V

    new-instance v4, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$MyTextWatcher;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    invoke-direct {v4, v5, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$MyTextWatcher;-><init>(Landroid/view/View;B)V

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mTextWatcher:Landroid/text/TextWatcher;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    new-instance v5, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    const v5, 0x1efab34d

    invoke-virtual {v4, v5, v9, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-object v3

    :cond_0
    move v4, v6

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->destroy()V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onDestroyView()V

    return-void
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 20
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    const-string v2, "comment_action"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v16

    if-nez v16, :cond_0

    const-string v2, "StreamOneUp"

    const-string v3, "No actions for comment option dialog"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, p1

    if-lt v0, v2, :cond_1

    const-string v2, "StreamOneUp"

    const-string v3, "Option selected outside the action list"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v2, "comment_author_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const-string v2, "comment_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v2, "comment_content"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v2, "plus_one_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v2, "plus_one_by_me"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v18

    const-string v2, "plus_one_count"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v2, "extra_comment_id"

    invoke-static {v2, v7}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_DELETE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->menu_delete_comment:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->comment_delete_question:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->ok:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->cancel:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "comment_id"

    invoke-virtual {v3, v4, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "hsouf_delete_comment"

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1
    if-eqz v8, :cond_2

    if-nez v18, :cond_3

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    const-wide/16 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/plus/service/EsService;->plusOneComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Z)I

    goto/16 :goto_0

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    const-wide/16 v5, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/plus/service/EsService;->plusOneComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Z)I

    goto/16 :goto_0

    :sswitch_2
    const-string v2, "extra_comment_id"

    invoke-static {v2, v7}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_EDIT_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v12, v7

    invoke-static/range {v9 .. v15}, Lcom/google/android/apps/plus/phone/Intents;->getEditCommentActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_3
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v7, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->doReportComment(ZLjava/lang/String;Z)V

    goto/16 :goto_0

    :sswitch_4
    const/4 v2, 0x0

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v7, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->doReportComment(ZLjava/lang/String;Z)V

    goto/16 :goto_0

    :sswitch_5
    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v7, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->doReportComment(ZLjava/lang/String;Z)V

    goto/16 :goto_0

    :sswitch_6
    sget v2, Lcom/google/android/apps/plus/R$string;->menu_item_ban_user:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->ban_user_question:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->yes:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->no:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "ban_user_id"

    move-object/from16 v0, v17

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "hsouf_ban_comment_author"

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v8, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showPlusOnePeople(Ljava/lang/String;I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x21 -> :sswitch_0
        0x22 -> :sswitch_3
        0x23 -> :sswitch_4
        0x24 -> :sswitch_5
        0x25 -> :sswitch_1
        0x26 -> :sswitch_2
        0x40 -> :sswitch_7
        0x52 -> :sswitch_6
    .end sparse-switch
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const-string v0, "hsouf_delete_activity"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->deleteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "hsouf_delete_comment"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    const-string v3, "comment_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->deleteComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v0, 0x21

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    goto :goto_0

    :cond_2
    const-string v0, "hsouf_report_comment"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    const-string v3, "comment_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "delete"

    invoke-virtual {p1, v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    const-string v9, "is_undo"

    invoke-virtual {p1, v9, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->moderateComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZZZ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    goto :goto_0

    :cond_3
    const-string v0, "hsouf_mute_activity"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityMuted:Z

    if-nez v3, :cond_4

    :goto_1
    invoke-static {v0, v1, v2, v5}, Lcom/google/android/apps/plus/service/EsService;->muteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    goto :goto_0

    :cond_4
    move v5, v6

    goto :goto_1

    :cond_5
    const-string v0, "hsouf_report_activity"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mViewerIsSquareAdmin:Z

    if-eqz v0, :cond_6

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCreationSourceId:Ljava/lang/String;

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v2, v8}, Lcom/google/android/apps/plus/service/EsService;->reportActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    goto/16 :goto_0

    :cond_7
    const-string v0, "hsouf_cancel_edits"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0

    :cond_8
    const-string v0, "hsouf_remove_activity"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSquareId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    const-string v4, "REJECTED"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->editModerationState(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v0, 0x51

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_REMOVE_POST_VIA_POST_OPTIONS_MENU:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, v0, v8}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->logSquareAction(Lcom/google/android/apps/plus/analytics/OzActions;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const-string v0, "hsouf_ban_comment_author"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ban_user_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSquareId:Ljava/lang/String;

    const-string v3, "BAN"

    invoke-static {v0, v1, v2, v7, v3}, Lcom/google/android/apps/plus/service/EsService;->editSquareMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v0, 0x52

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_BAN_MEMBER_FROM_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, v0, v7}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->logSquareAction(Lcom/google/android/apps/plus/analytics/OzActions;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final onJoinHangout(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedHangout;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->isJoinable()Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https://www.youtube.com/watch?v="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getYoutubeLiveId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v2, "com.google.android.youtube"

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/hangout/Utils;->isAppInstalled(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com.google.android.youtube"

    const-string v3, "com.google.android.youtube.WatchActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {v2, v0, p1, p2, p3}, Lcom/google/android/apps/plus/service/Hangout;->enterGreenRoom(Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedHangout;)V

    goto :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 13
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/16 v12, 0x19

    const/16 v3, 0x8

    const-wide/16 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityDataNotFound:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->refresh()V

    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    sget v1, Lcom/google/android/apps/plus/R$id;->gray_spam_bar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsGraySpam:Z

    if-eqz v0, :cond_17

    move v0, v2

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->setFlaggedComments(Ljava/util/HashSet;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mMuteProcessed:Z

    if-nez v1, :cond_4

    const-string v1, "mute"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getView()Landroid/view/View;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$3;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_4
    const-string v1, "enable_comment_action"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$id;->stage:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Landroid/view/View;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_5
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityDataNotFound:Z

    const/16 v0, 0xd

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCreationSourceId:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mReadProcessed:Z

    if-nez v0, :cond_8

    const-string v0, "extra_activity_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCreationSourceId:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    if-nez v0, :cond_6

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :cond_6
    const-string v4, "extra_creation_source_id"

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCreationSourceId:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v4, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mReadProcessed:Z

    :cond_8
    const/16 v0, 0x16

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/16 v0, 0x9

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbLocation;->deserialize([B)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

    const/4 v0, 0x4

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAuthorId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAuthorId:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsMyActivity:Ljava/lang/Boolean;

    const/16 v0, 0xf

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_f

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsLimited:Z

    const/16 v0, 0x14

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_10

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityMuted:Z

    invoke-interface {p2, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mReshare:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mViewerIsSquareAdmin:Z

    if-eqz v0, :cond_12

    const/16 v0, 0x10

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_12

    const-wide/32 v6, 0x8000

    and-long/2addr v6, v4

    cmp-long v0, v6, v10

    if-eqz v0, :cond_12

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsGraySpam:Z

    const/16 v0, 0x1d

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPostLink:Ljava/lang/String;

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getId()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_9

    new-instance v6, Landroid/util/Pair;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v6, v7, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPlusOnedByData:Landroid/util/Pair;

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->invalidateActionBar()V

    const/16 v0, 0x11

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_13

    move v0, v1

    :goto_7
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setEnabled(Z)V

    if-eqz v0, :cond_a

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mShowKeyboard:Z

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    new-instance v7, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$2;

    invoke-direct {v7, p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V

    const-wide/16 v8, 0xfa

    invoke-virtual {v6, v7, v8, v9}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_a
    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    sget v6, Lcom/google/android/apps/plus/R$string;->compose_comment_hint:I

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHint(I)V

    :goto_8
    invoke-interface {p2, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_15

    const/16 v0, 0x18

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mEditableText:Ljava/lang/String;

    :goto_9
    const/16 v0, 0xe

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCreationSource:Ljava/lang/String;

    const-wide/16 v6, 0x2000

    and-long/2addr v6, v4

    cmp-long v0, v6, v10

    if-eqz v0, :cond_b

    const/16 v0, 0x21

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAppInviteData:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    :cond_b
    const-wide/16 v6, 0x800

    and-long/2addr v6, v4

    cmp-long v0, v6, v10

    if-eqz v0, :cond_c

    const/16 v0, 0x20

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mContentDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    :cond_c
    const/16 v0, 0x15

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eq v0, v1, :cond_d

    check-cast p1, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->needToRefreshComments()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->refresh()V

    :cond_e
    const-wide/32 v6, 0x8000

    and-long/2addr v4, v6

    cmp-long v0, v4, v10

    if-eqz v0, :cond_16

    :goto_a
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsSquarePost:Z

    const/16 v0, 0x1e

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->deserialize([B)Lcom/google/android/apps/plus/content/DbSquareUpdate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->getSquareId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSquareId:Ljava/lang/String;

    goto/16 :goto_1

    :cond_f
    move v0, v2

    goto/16 :goto_3

    :cond_10
    move v0, v2

    goto/16 :goto_4

    :cond_11
    move v0, v2

    goto/16 :goto_5

    :cond_12
    move v0, v2

    goto/16 :goto_6

    :cond_13
    move v0, v2

    goto/16 :goto_7

    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    sget v6, Lcom/google/android/apps/plus/R$string;->compose_comment_not_allowed_hint:I

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHint(I)V

    goto/16 :goto_8

    :cond_15
    const/16 v0, 0x17

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mEditableText:Ljava/lang/String;

    goto :goto_9

    :cond_16
    move v1, v2

    goto :goto_a

    :cond_17
    move v0, v3

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1efab34d
        :pswitch_0
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onLocationClick$75c560e7(Lcom/google/android/apps/plus/content/DbLocation;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/util/MapUtils;->showActivityOnMap(Landroid/content/Context;Lcom/google/android/apps/plus/content/DbLocation;)V

    return-void
.end method

.method public final onMediaClick(Lcom/google/android/apps/plus/api/MediaRef;Ljava/lang/String;Z)V
    .locals 9
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz p3, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-nez v5, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v1, v5, v4, v6}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoOneUpActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setRefreshAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsSquarePost:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setDisableComments(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setIsStreamPost(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    const-string v5, "extra_gaia_id"

    invoke-static {v5, v3}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v5, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1    # Landroid/view/MenuItem;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/R$id;->feedback:I

    if-ne v1, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/GoogleFeedback;->launch(Landroid/app/Activity;)V

    move v2, v4

    :goto_0
    return v2

    :cond_0
    sget v2, Lcom/google/android/apps/plus/R$id;->show_location:I

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/MapUtils;->showActivityOnMap(Landroid/content/Context;Lcom/google/android/apps/plus/content/DbLocation;)V

    move v2, v4

    goto :goto_0

    :cond_1
    sget v2, Lcom/google/android/apps/plus/R$id;->edit:I

    if-ne v1, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mEditableText:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mReshare:Z

    invoke-static {v0, v2, v3, v5, v6}, Lcom/google/android/apps/plus/phone/Intents;->getEditPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    move v2, v4

    goto :goto_0

    :cond_2
    sget v2, Lcom/google/android/apps/plus/R$id;->delete_post:I

    if-ne v1, v2, :cond_3

    const-string v2, "extra_activity_id"

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REMOVE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v5, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->menu_remove_post:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v5, Lcom/google/android/apps/plus/R$string;->post_delete_question:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v5, v6, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v2

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v5, "hsouf_delete_activity"

    invoke-virtual {v2, v3, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v2, v4

    goto :goto_0

    :cond_3
    sget v2, Lcom/google/android/apps/plus/R$id;->remove_post:I

    if-ne v1, v2, :cond_4

    sget v2, Lcom/google/android/apps/plus/R$string;->menu_item_remove_post:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v5, Lcom/google/android/apps/plus/R$string;->post_remove_question:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->yes:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->no:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v5, v6, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v2

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v5, "hsouf_remove_activity"

    invoke-virtual {v2, v3, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v2, v4

    goto/16 :goto_0

    :cond_4
    sget v2, Lcom/google/android/apps/plus/R$id;->ban_user:I

    if-ne v1, v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAuthorId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSquareId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v2, v5, v6}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;

    move-result-object v2

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v5, "hsouf_ban_activity_author"

    invoke-virtual {v2, v3, v5}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v2, v4

    goto/16 :goto_0

    :cond_5
    sget v2, Lcom/google/android/apps/plus/R$id;->plus_oned_by:I

    if-ne v1, v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPlusOnedByData:Landroid/util/Pair;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPlusOnedByData:Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPlusOnedByData:Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showPlusOnePeople(Ljava/lang/String;I)V

    :cond_6
    move v2, v4

    goto/16 :goto_0

    :cond_7
    sget v2, Lcom/google/android/apps/plus/R$id;->report_abuse:I

    if-ne v1, v2, :cond_8

    const-string v2, "extra_activity_id"

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v5, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->menu_report_abuse:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v5, Lcom/google/android/apps/plus/R$string;->post_report_question:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v5, v6, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v2

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "activity_id"

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v5, "hsouf_report_activity"

    invoke-virtual {v2, v3, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v2, v4

    goto/16 :goto_0

    :cond_8
    sget v2, Lcom/google/android/apps/plus/R$id;->mute_post:I

    if-ne v1, v2, :cond_b

    const-string v2, "extra_activity_id"

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_MUTE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v5, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityMuted:Z

    if-eqz v2, :cond_9

    sget v2, Lcom/google/android/apps/plus/R$string;->menu_unmute_post:I

    :goto_1
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityMuted:Z

    if-eqz v2, :cond_a

    sget v2, Lcom/google/android/apps/plus/R$string;->post_unmute_question:I

    :goto_2
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v6, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v2, v6, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v2

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "activity_id"

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v5, "hsouf_mute_activity"

    invoke-virtual {v2, v3, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v2, v4

    goto/16 :goto_0

    :cond_9
    sget v2, Lcom/google/android/apps/plus/R$string;->menu_mute_post:I

    goto :goto_1

    :cond_a
    sget v2, Lcom/google/android/apps/plus/R$string;->post_mute_question:I

    goto :goto_2

    :cond_b
    sget v2, Lcom/google/android/apps/plus/R$id;->share_link:I

    if-ne v1, v2, :cond_d

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-ge v2, v5, :cond_c

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v5, "clipboard"

    invoke-virtual {v2, v5}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/text/ClipboardManager;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPostLink:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v5, Lcom/google/android/apps/plus/R$string;->menu_share_link_toast:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    move v2, v4

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v5, "clipboard"

    invoke-virtual {v2, v5}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ClipboardManager;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPostLink:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/content/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_d
    move v2, v3

    goto/16 :goto_0
.end method

.method public final onPause()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPause()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Landroid/widget/ListView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v2, v1, Lcom/google/android/apps/plus/views/OneUpBaseView;

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onStop()V

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public final onPlaceClick(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    invoke-static {v1, v2, p1, v3}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public final onPlusOne(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->isPostPlusOnePending(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->deletePostPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->createPostPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->updateProgressIndicator()V

    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v1, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$id;->show_location:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsMyActivity:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsMyActivity:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    sget v1, Lcom/google/android/apps/plus/R$id;->edit:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v1, Lcom/google/android/apps/plus/R$id;->delete_post:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsLimited:Z

    if-nez v1, :cond_2

    sget v1, Lcom/google/android/apps/plus/R$id;->share_link:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPlusOnedByData:Landroid/util/Pair;

    if-eqz v1, :cond_3

    sget v1, Lcom/google/android/apps/plus/R$id;->plus_oned_by:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_3
    sget v1, Lcom/google/android/apps/plus/R$id;->feedback:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void

    :cond_4
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mViewerIsSquareAdmin:Z

    if-eqz v1, :cond_5

    sget v1, Lcom/google/android/apps/plus/R$id;->remove_post:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v1, Lcom/google/android/apps/plus/R$id;->ban_user:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_1
    sget v1, Lcom/google/android/apps/plus/R$id;->mute_post:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityMuted:Z

    if-eqz v1, :cond_6

    sget v1, Lcom/google/android/apps/plus/R$string;->menu_unmute_post:I

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    :cond_5
    sget v1, Lcom/google/android/apps/plus/R$id;->report_abuse:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_6
    sget v1, Lcom/google/android/apps/plus/R$string;->menu_mute_post:I

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public final onRemoveActivity(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v2, "REJECTED"

    invoke-static {v0, v1, p1, p2, v2}, Lcom/google/android/apps/plus/service/EsService;->editModerationState(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v0, 0x51

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GREY_SPAM_REMOVE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->logSquareAction(Lcom/google/android/apps/plus/analytics/OzActions;Ljava/lang/String;)V

    return-void
.end method

.method public final onReshareClicked()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsLimited:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x9

    :goto_0
    invoke-static {v2, v3, v4, v1}, Lcom/google/android/apps/plus/phone/Intents;->getReshareActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsLimited:Z

    if-eqz v1, :cond_1

    const-string v1, "extra_activity_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->OPEN_RESHARE_SHAREBOX:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    sget v1, Lcom/google/android/apps/plus/R$string;->reshare_dialog_title:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->reshare_dialog_message:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->reshare_dialog_positive_button:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/fragments/ConfirmIntentDialog;->newInstance(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "reshare_activity"

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const/4 v1, 0x5

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public final onRestoreActivity(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v2, "APPROVED"

    invoke-static {v0, v1, p1, p2, v2}, Lcom/google/android/apps/plus/service/EsService;->editModerationState(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v0, 0x50

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GREY_SPAM_RESTORE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->logSquareAction(Lcom/google/android/apps/plus/analytics/OzActions;Ljava/lang/String;)V

    return-void
.end method

.method public final onResume()V
    .locals 5

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onResume()V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Landroid/widget/ListView;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getChildCount()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Lcom/google/android/apps/plus/views/OneUpBaseView;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onStart()V

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    # invokes: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z
    invoke-static {v3, v4, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->access$300(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;ILcom/google/android/apps/plus/service/ServiceResult;)Z

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->updateProgressIndicator()V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, "pending_request_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, "activity_request_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v1, :cond_2

    const-string v1, "audience_data"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    const-string v1, "flagged_comments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_3
    const-string v1, "operation_type"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mOperationType:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "mute_processed"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mMuteProcessed:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "read_processed"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mReadProcessed:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "source_package_name"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSourcePackageName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "source_author_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSourceAuthorId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "get_activity_complete"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mGetActivityComplete:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "stage_media_loaded"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mStageMediaLoaded:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "show_keyboard"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mShowKeyboard:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onSetArguments(Landroid/os/Bundle;)V

    const-string v0, "auto_play_music"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAutoPlay:Z

    return-void
.end method

.method public final onSkyjamBuyClick(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v2, "com.android.vending"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onSkyjamListenClick(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.music.SHARED_PLAY"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "url"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "authAccount"

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "accountType"

    const-string v3, "com.google"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.google.android.music"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v2, "market://details?id=com.google.android.music"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onSourceAppContentClick(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->launchDeepLink(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .locals 7
    .param p1    # Landroid/text/style/URLSpan;

    const/4 v1, 0x0

    const/4 v6, 0x1

    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v2

    const-string v0, "acl:"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getAclText()Ljava/lang/String;

    move-result-object v3

    sget v0, Lcom/google/android/apps/plus/R$string;->acl_public:I

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$string;->acl_description_public:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "hsouf_audience"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$string;->acl_private_contacts:I

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/apps/plus/R$string;->acl_description_private_contacts:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/apps/plus/R$string;->acl_extended_network:I

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/apps/plus/R$string;->acl_description_extended_network:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget v0, Lcom/google/android/apps/plus/R$string;->acl_limited:I

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    sget v0, Lcom/google/android/apps/plus/R$string;->acl_description_domain:I

    new-array v2, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v2, v0, v4}, Lcom/google/android/apps/plus/service/EsService;->getActivityAudience(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v0, 0x30

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    move-object v0, v1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    goto :goto_1

    :cond_6
    const-string v0, "+1:"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    aget-object v1, v0, v6

    const/4 v2, 0x2

    aget-object v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showPlusOnePeople(Ljava/lang/String;I)V

    goto :goto_1

    :cond_7
    const-string v0, "https://plus.google.com/s/%23"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x1d

    invoke-virtual {v4, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/plus/phone/Intents;->getPostSearchActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_8
    invoke-static {v4}, Lcom/google/android/apps/plus/phone/Intents;->isProfileUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {v4}, Lcom/google/android/apps/plus/phone/Intents;->getPersonIdFromProfileUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "extra_gaia_id"

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PERSON:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCreationSourceId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v4, v2, v3}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mContentDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mContentDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getClientPackageNames()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mContentDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getDeepLinkId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCreationSource:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mContentDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getClientPackageNames()Ljava/util/ArrayList;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mContentDeepLink:Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getDeepLinkId()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAuthorId:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->launchDeepLink(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_1

    :catch_0
    move-exception v1

    goto :goto_2
.end method

.method public final onSquareClick(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    invoke-static {v1, v2, p1, p2, v3}, Lcom/google/android/apps/plus/phone/Intents;->getSquareStreamActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final refresh()V
    .locals 5

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->refresh()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mGetActivityComplete:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSquareId:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mViewerIsSquareAdmin:Z

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->getActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->updateProgressIndicator()V

    return-void
.end method

.method protected final updateProgressIndicator()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_3

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->setLoading(Z)V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->hideProgressIndicator()V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method
