.class final Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "LocationPickerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/LocationPickerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)V

    return-void
.end method


# virtual methods
.method public final onLocationQuery$260d7f24(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 5
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-object v1, v1, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iget-object v1, v1, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, p1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    iput-object v4, v1, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->checkin_places_error:I

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLoadPlacesNeeded:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$100(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # setter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLoadPlacesNeeded:Z
    invoke-static {v1, v3}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$102(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Z)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-virtual {v0, v3, v4, v1}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLoadSearchNeeded:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$200(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    # setter for: Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->mLoadSearchNeeded:Z
    invoke-static {v1, v3}, Lcom/google/android/apps/plus/fragments/LocationPickerFragment;->access$202(Lcom/google/android/apps/plus/fragments/LocationPickerFragment;Z)Z

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/LocationPickerFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/LocationPickerFragment;

    invoke-virtual {v0, v1, v4, v2}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_2
    return-void
.end method
