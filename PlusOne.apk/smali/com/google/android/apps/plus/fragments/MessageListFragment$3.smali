.class final Lcom/google/android/apps/plus/fragments/MessageListFragment$3;
.super Ljava/lang/Object;
.source "MessageListFragment.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/MessageListFragment;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1
    .param p1    # Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingVisibilityChanged:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$900(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAnimateTypingVisibilityRunnable:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$1000(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method
