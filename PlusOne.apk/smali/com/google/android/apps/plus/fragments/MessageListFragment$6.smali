.class final Lcom/google/android/apps/plus/fragments/MessageListFragment$6;
.super Ljava/lang/Object;
.source "MessageListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/MessageListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$6;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    const/4 v2, 0x0

    instance-of v3, p2, Lcom/google/android/apps/plus/views/MessageListItemView;

    if-eqz v3, :cond_2

    move-object v1, p2

    check-cast v1, Lcom/google/android/apps/plus/views/MessageListItemView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MessageListItemView;->getMessage()Ljava/lang/CharSequence;

    move-result-object v2

    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$6;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-string v4, "clipboard"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v2}, Landroid/content/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$6;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->copied_to_clipboard:I

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :cond_1
    const/4 v3, 0x1

    return v3

    :cond_2
    instance-of v3, p2, Lcom/google/android/apps/plus/views/SystemMessageListItemView;

    if-eqz v3, :cond_0

    move-object v1, p2

    check-cast v1, Lcom/google/android/apps/plus/views/SystemMessageListItemView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/SystemMessageListItemView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_0
.end method
