.class final Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "MessageListFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/MessageListItemViewImage$OnMeasuredListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/MessageListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MessageCursorAdapter"
.end annotation


# instance fields
.field final mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

.field final mViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;Landroid/widget/AbsListView;Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/fragments/MessageListFragment;
    .param p2    # Landroid/widget/AbsListView;
    .param p3    # Landroid/database/Cursor;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mViews:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter$1;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;)V

    invoke-virtual {p2, v0}, Landroid/widget/AbsListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 19
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mDataValid:Z

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "this should only be called when the cursor is valid"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "couldn\'t move cursor to position "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/LayoutInflater;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v3, 0x7

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/4 v2, 0x1

    if-ne v10, v2, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/16 v3, 0xb

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_b

    if-eqz p2, :cond_6

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;

    if-eqz v2, :cond_6

    move-object/from16 v12, p2

    check-cast v12, Lcom/google/android/apps/plus/views/MessageListItemViewImage;

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mViews:Ljava/util/List;

    invoke-interface {v3, v12}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mViews:Ljava/util/List;

    invoke-interface {v3, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->clear()V

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x3

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x5

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long v16, v3, v5

    move-wide/from16 v0, v16

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/plus/util/Dates;->getShortRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v12, v2, v3}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setMessageRowId(J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/16 v3, 0x9

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setAuthorName(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/16 v3, 0xb

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_4

    const-string v2, "//"

    invoke-virtual {v13, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v7, v2

    :goto_1
    const/4 v2, 0x0

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->getImageWidth()Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->getImageWidth()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->getImageHeight()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const-string v4, "MessageListFragment"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "MessageListFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "BindUserImageMessageView image width "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " height "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const-string v2, "content://"

    invoke-virtual {v7, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    :goto_2
    invoke-virtual {v12, v13, v2}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setImage(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isFirst()Z

    move-result v2

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v3, 0x5

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v5, 0x3

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v5, 0x7

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_16

    sub-long v2, v16, v2

    const-wide/32 v4, 0xea60

    cmp-long v2, v2, v4

    if-gtz v2, :cond_16

    const/4 v2, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move v14, v2

    :cond_5
    if-eqz v15, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$1300(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsGroup:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$1400(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Z

    move-result v2

    if-nez v2, :cond_9

    const/4 v2, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x6

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v12, v3, v2}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setMessageStatus(IZ)V

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setTimeSince(Ljava/lang/CharSequence;)V

    invoke-static {v15}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setGaiaId(Ljava/lang/String;)V

    if-eqz v14, :cond_a

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->hideAuthor()V

    :goto_5
    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->updateContentDescription()V

    :goto_6
    return-object v12

    :cond_6
    sget v2, Lcom/google/android/apps/plus/R$layout;->message_list_item_view_image:I

    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/views/MessageListItemViewImage;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v12, v2}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setMessageClickListener(Lcom/google/android/apps/plus/views/MessageClickListener;)V

    goto/16 :goto_0

    :cond_7
    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v3, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v2, v13, v3}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    goto/16 :goto_2

    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setOnMeasureListener(Lcom/google/android/apps/plus/views/MessageListItemViewImage$OnMeasuredListener;)V

    goto/16 :goto_2

    :cond_9
    const/4 v2, 0x0

    goto :goto_4

    :cond_a
    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->showAuthor()V

    goto :goto_5

    :cond_b
    if-eqz p2, :cond_f

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/plus/views/MessageListItemView;

    if-eqz v2, :cond_f

    move-object/from16 v12, p2

    check-cast v12, Lcom/google/android/apps/plus/views/MessageListItemView;

    :goto_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mViews:Ljava/util/List;

    invoke-interface {v2, v12}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mViews:Ljava/util/List;

    invoke-interface {v2, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_c
    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemView;->clear()V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v5, 0x3

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v6, 0x5

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-static {v3, v5, v6}, Lcom/google/android/apps/plus/util/Dates;->getShortRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v8, 0x0

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    invoke-virtual {v12, v13, v14}, Lcom/google/android/apps/plus/views/MessageListItemView;->setMessageRowId(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/16 v8, 0x9

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Lcom/google/android/apps/plus/views/MessageListItemView;->setAuthorName(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v8, 0x4

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Lcom/google/android/apps/plus/views/MessageListItemView;->setMessage(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isFirst()Z

    move-result v3

    if-nez v3, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v3

    if-eqz v3, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v8, 0x5

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    const-wide/16 v15, 0x3e8

    div-long/2addr v13, v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v8, 0x3

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v8, 0x7

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v8, 0x1

    if-ne v3, v8, :cond_d

    sub-long/2addr v5, v13

    const-wide/32 v13, 0xea60

    cmp-long v3, v5, v13

    if-gtz v3, :cond_d

    const/4 v2, 0x1

    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    :cond_e
    move v3, v2

    if-eqz v4, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$1300(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsGroup:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$1400(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Z

    move-result v2

    if-nez v2, :cond_10

    const/4 v2, 0x1

    :goto_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v6, 0x6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v12, v5, v2}, Lcom/google/android/apps/plus/views/MessageListItemView;->setMessageStatus(IZ)V

    invoke-virtual {v12, v7}, Lcom/google/android/apps/plus/views/MessageListItemView;->setTimeSince(Ljava/lang/CharSequence;)V

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/google/android/apps/plus/views/MessageListItemView;->setGaiaId(Ljava/lang/String;)V

    if-eqz v3, :cond_11

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemView;->hideAuthor()V

    :goto_9
    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemView;->updateContentDescription()V

    goto/16 :goto_6

    :cond_f
    sget v2, Lcom/google/android/apps/plus/R$layout;->message_list_item_view:I

    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/views/MessageListItemView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v12, v2}, Lcom/google/android/apps/plus/views/MessageListItemView;->setMessageClickListener(Lcom/google/android/apps/plus/views/MessageClickListener;)V

    goto/16 :goto_7

    :cond_10
    const/4 v2, 0x0

    goto :goto_8

    :cond_11
    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemView;->showAuthor()V

    goto :goto_9

    :cond_12
    const/4 v2, 0x6

    if-ne v10, v2, :cond_14

    if-eqz p2, :cond_13

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;

    if-eqz v2, :cond_13

    move-object/from16 v12, p2

    check-cast v12, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;

    :goto_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x7

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v12, v3}, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->setType(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x5

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/util/Dates;->getShortRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->setTimeSince(Ljava/lang/CharSequence;)V

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->updateContentDescription()V

    goto/16 :goto_6

    :cond_13
    sget v2, Lcom/google/android/apps/plus/R$layout;->hangout_tile_event_message_list_item_view:I

    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;

    goto :goto_a

    :cond_14
    if-eqz p2, :cond_15

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/plus/views/SystemMessageListItemView;

    if-eqz v2, :cond_15

    move-object/from16 v12, p2

    check-cast v12, Lcom/google/android/apps/plus/views/SystemMessageListItemView;

    :goto_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x7

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v12, v3}, Lcom/google/android/apps/plus/views/SystemMessageListItemView;->setType(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Lcom/google/android/apps/plus/views/SystemMessageListItemView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x5

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/util/Dates;->getShortRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/google/android/apps/plus/views/SystemMessageListItemView;->setTimeSince(Ljava/lang/CharSequence;)V

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/SystemMessageListItemView;->updateContentDescription()V

    goto/16 :goto_6

    :cond_15
    sget v2, Lcom/google/android/apps/plus/R$layout;->system_message_list_item_view:I

    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/views/SystemMessageListItemView;

    goto :goto_b

    :cond_16
    move v2, v14

    goto/16 :goto_3

    :cond_17
    move-object v7, v13

    goto/16 :goto_1
.end method

.method public final onMeasured(Landroid/view/View;)V
    .locals 11
    .param p1    # Landroid/view/View;

    const/4 v1, 0x0

    instance-of v2, p1, Lcom/google/android/apps/plus/views/MessageListItemViewImage;

    if-eqz v2, :cond_1

    move-object v9, p1

    check-cast v9, Lcom/google/android/apps/plus/views/MessageListItemViewImage;

    invoke-virtual {v9, v1}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setOnMeasureListener(Lcom/google/android/apps/plus/views/MessageListItemViewImage$OnMeasuredListener;)V

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->getImageWidth()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->getImageHeight()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const-string v2, "MessageListFragment"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "MessageListFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onMeasured image width "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " height "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->getFullResUrl()Ljava/lang/String;

    move-result-object v8

    const-string v2, "content://"

    invoke-virtual {v8, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    const-wide/16 v2, 0x0

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-object v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    :goto_0
    invoke-virtual {v9, v8, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setImage(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;)V

    :cond_1
    return-void

    :cond_2
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v1, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v0, v8, v1}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method
