.class public Lcom/google/android/apps/plus/fragments/MuteProfileDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "MuteProfileDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private mGender:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mTargetMuteState:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mTargetMuteState:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->setPersonMuted(Z)V

    goto :goto_0

    :pswitch_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 13
    .param p1    # Landroid/os/Bundle;

    const/4 v12, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v8, "name"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mName:Ljava/lang/String;

    const-string v8, "gender"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mGender:Ljava/lang/String;

    const-string v8, "target_mute"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mTargetMuteState:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mTargetMuteState:Z

    if-eqz v8, :cond_0

    sget v8, Lcom/google/android/apps/plus/R$string;->mute_dialog_title:I

    :goto_0
    new-array v9, v12, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mName:Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v8, 0x104000a

    invoke-virtual {v2, v8, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v8, 0x1040000

    invoke-virtual {v2, v8, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    sget v9, Lcom/google/android/apps/plus/R$layout;->block_profile_confirm_dialog:I

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$id;->message:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mGender:Ljava/lang/String;

    const-string v9, "MALE"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mTargetMuteState:Z

    if-eqz v8, :cond_1

    sget v8, Lcom/google/android/apps/plus/R$string;->mute_dialog_content_male:I

    :goto_1
    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_2
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v8, Lcom/google/android/apps/plus/R$id;->explanation:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    return-object v8

    :cond_0
    sget v8, Lcom/google/android/apps/plus/R$string;->unmute_dialog_title:I

    goto :goto_0

    :cond_1
    sget v8, Lcom/google/android/apps/plus/R$string;->unmute_dialog_content_male:I

    goto :goto_1

    :cond_2
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mGender:Ljava/lang/String;

    const-string v9, "FEMALE"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mTargetMuteState:Z

    if-eqz v8, :cond_3

    sget v8, Lcom/google/android/apps/plus/R$string;->mute_dialog_content_female:I

    :goto_3
    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_3
    sget v8, Lcom/google/android/apps/plus/R$string;->unmute_dialog_content_female:I

    goto :goto_3

    :cond_4
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mTargetMuteState:Z

    if-eqz v8, :cond_5

    sget v8, Lcom/google/android/apps/plus/R$string;->mute_dialog_content_general:I

    :goto_4
    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_5
    sget v8, Lcom/google/android/apps/plus/R$string;->unmute_dialog_content_general:I

    goto :goto_4
.end method
