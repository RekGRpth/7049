.class public Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;
.super Lcom/google/android/apps/plus/fragments/EsFragment;
.source "NetworkStatisticsFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment$NetworkStatisticsQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsFragment;",
        "Landroid/content/DialogInterface$OnClickListener;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field protected static final sSortColumns:[Ljava/lang/String;

.field protected static final sValueColumns:[[I


# instance fields
.field protected mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field protected mBarGraphView:Lcom/google/android/apps/plus/views/BarGraphView;

.field protected mPendingViewType:I

.field protected mViewType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment$NetworkStatisticsQuery;->PROJECTION:[Ljava/lang/String;

    aget-object v2, v2, v6

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment$NetworkStatisticsQuery;->PROJECTION:[Ljava/lang/String;

    aget-object v2, v2, v7

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment$NetworkStatisticsQuery;->PROJECTION:[Ljava/lang/String;

    aget-object v1, v1, v6

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment$NetworkStatisticsQuery;->PROJECTION:[Ljava/lang/String;

    aget-object v1, v1, v7

    aput-object v1, v0, v6

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment$NetworkStatisticsQuery;->PROJECTION:[Ljava/lang/String;

    aget-object v2, v2, v8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment$NetworkStatisticsQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment$NetworkStatisticsQuery;->PROJECTION:[Ljava/lang/String;

    aget-object v1, v1, v8

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment$NetworkStatisticsQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment$NetworkStatisticsQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->sSortColumns:[Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [[I

    new-array v1, v6, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v4

    new-array v1, v5, [I

    aput v6, v1, v4

    aput-object v1, v0, v5

    new-array v1, v5, [I

    aput v7, v1, v4

    aput-object v1, v0, v6

    new-array v1, v6, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v7

    new-array v1, v5, [I

    aput v8, v1, v4

    aput-object v1, v0, v8

    const/4 v1, 0x5

    new-array v2, v5, [I

    const/4 v3, 0x5

    aput v3, v2, v4

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v5, [I

    const/4 v3, 0x6

    aput v3, v2, v4

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->sValueColumns:[[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x3
    .end array-data

    :array_1
    .array-data 4
        0x4
        0x5
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mViewType:I

    return-void
.end method

.method private updateTitle(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$array;->network_statistics_types:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mViewType:I

    aget-object v2, v1, v2

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget v2, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mViewType:I

    aget-object v2, v1, v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setTitlebarTitle(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected final isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    iput p2, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mPendingViewType:I

    :goto_0
    return-void

    :pswitch_0
    iget v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mPendingViewType:I

    iget v1, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mViewType:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mPendingViewType:I

    iput v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mViewType:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->updateTitle(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)V

    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    :pswitch_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v1, "view_type"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "view_type"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mViewType:I

    iget v1, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mViewType:I

    iput v1, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mPendingViewType:I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    move-object v0, v4

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment$NetworkStatisticsQuery;->PROJECTION:[Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->sSortColumns:[Ljava/lang/String;

    iget v7, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mViewType:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " DESC"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v1, Lcom/google/android/apps/plus/R$layout;->network_statistics_fragment:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$id;->bar_graph:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/BarGraphView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mBarGraphView:Lcom/google/android/apps/plus/views/BarGraphView;

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 11
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v1, 0x0

    check-cast p2, Landroid/database/Cursor;

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    new-array v4, v3, [Lcom/google/android/apps/plus/views/BarGraphView$RowInfo;

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/views/BarGraphView$RowInfo;

    invoke-direct {v0}, Lcom/google/android/apps/plus/views/BarGraphView$RowInfo;-><init>()V

    aput-object v0, v4, v2

    aget-object v0, v4, v2

    const/4 v5, 0x1

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/google/android/apps/plus/views/BarGraphView$RowInfo;->mLabel:Ljava/lang/String;

    aget-object v0, v4, v2

    const-wide/16 v5, 0x0

    iput-wide v5, v0, Lcom/google/android/apps/plus/views/BarGraphView$RowInfo;->mValue:J

    sget-object v0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->sValueColumns:[[I

    iget v5, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mViewType:I

    aget-object v0, v0, v5

    array-length v5, v0

    move v0, v1

    :goto_1
    if-ge v0, v5, :cond_0

    aget-object v6, v4, v2

    iget-wide v7, v6, Lcom/google/android/apps/plus/views/BarGraphView$RowInfo;->mValue:J

    sget-object v9, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->sValueColumns:[[I

    iget v10, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mViewType:I

    aget-object v9, v9, v10

    aget v9, v9, v0

    invoke-interface {p2, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    add-long/2addr v7, v9

    iput-wide v7, v6, Lcom/google/android/apps/plus/views/BarGraphView$RowInfo;->mValue:J

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mBarGraphView:Lcom/google/android/apps/plus/views/BarGraphView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$array;->network_statistics_types_units:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mViewType:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v4, v1}, Lcom/google/android/apps/plus/views/BarGraphView;->update([Lcom/google/android/apps/plus/views/BarGraphView$RowInfo;Ljava/lang/String;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onMenuItemSelected(Landroid/view/MenuItem;)V
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/R$id;->customize:I

    if-ne v1, v2, :cond_1

    iget v2, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mViewType:I

    iput v2, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mPendingViewType:I

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->menu_network_customize:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/apps/plus/R$array;->network_statistics_types:I

    iget v3, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mViewType:I

    invoke-virtual {v0, v2, v3, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v2, Lcom/google/android/apps/plus/R$id;->clear:I

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsNetworkData;->resetStatsData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method public final onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->updateTitle(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "view_type"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/NetworkStatisticsFragment;->mViewType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
