.class public Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;
.super Landroid/support/v4/app/Fragment;
.source "OobInstantUploadFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;
    }
.end annotation


# instance fields
.field private mButtonTextViews:[Landroid/widget/TextView;

.field private mCheckMarks:[Landroid/view/View;

.field private mSelection:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    sget-object v0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->WIFI_AND_MOBILE:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mSelection:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    invoke-static {}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->values()[Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mButtonTextViews:[Landroid/widget/TextView;

    invoke-static {}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->values()[Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mCheckMarks:[Landroid/view/View;

    return-void
.end method

.method private doNextStep()V
    .locals 7

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mSelection:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    sget-object v1, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->WIFI_AND_MOBILE:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v5, 0x1

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/phone/OobDeviceActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v0, "account"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZZ)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    if-eqz v5, :cond_3

    if-eqz v4, :cond_2

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_WIFI_ONLY_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_1
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->OOB_CAMERA_SYNC:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mSelection:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    sget-object v1, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->WIFI_ONLY:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v5, 0x1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_OPTED_OUT:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_1
.end method

.method private updateRadioButtons()V
    .locals 5

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mSelection:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->ordinal()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mButtonTextViews:[Landroid/widget/TextView;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mButtonTextViews:[Landroid/widget/TextView;

    aget-object v3, v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-ne v0, v1, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$color;->oob_radio_button_highlight_text:I

    :goto_1
    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mCheckMarks:[Landroid/view/View;

    aget-object v3, v2, v0

    if-ne v0, v1, :cond_1

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget v2, Lcom/google/android/apps/plus/R$color;->oob_radio_button_text:I

    goto :goto_1

    :cond_1
    const/16 v2, 0x8

    goto :goto_2

    :cond_2
    return-void
.end method


# virtual methods
.method public final commit()Z
    .locals 9

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mSelection:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    sget-object v6, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->DISABLE:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v6, "account"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3}, Lcom/google/android/apps/plus/phone/InstantUpload;->isSyncEnabled$1f9c1b43(Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v1

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    :cond_0
    if-eqz v0, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->doNextStep()V

    move v3, v4

    :goto_0
    return v3

    :cond_2
    if-nez v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "photo_master_dialog"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    if-nez v4, :cond_3

    sget v4, Lcom/google/android/apps/plus/R$string;->oob_master_sync_dialog_title:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v6, Lcom/google/android/apps/plus/R$string;->oob_master_sync_dialog_message:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v4, v6, v7, v8}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v4

    invoke-virtual {v4, p0, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    const-string v6, "photo_master_dialog"

    invoke-virtual {v4, v3, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_3
    :goto_1
    move v3, v5

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v6, "photo_sync_dialog"

    invoke-virtual {v3, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v6

    if-nez v6, :cond_3

    sget v6, Lcom/google/android/apps/plus/R$string;->es_google_iu_provider:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->oob_enable_sync_dialog_message:I

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v6, v4, v5

    invoke-virtual {p0, v7, v4}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    sget v6, Lcom/google/android/apps/plus/R$string;->oob_enable_sync_dialog_title:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->yes:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$string;->no:I

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v4, v7, v8}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v4

    invoke-virtual {v4, p0, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    const-string v6, "photo_sync_dialog"

    invoke-virtual {v4, v3, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->button_wifi_and_mobile:I

    if-ne v0, v1, :cond_2

    sget-object v1, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->WIFI_AND_MOBILE:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mSelection:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->updateRadioButtons()V

    :cond_1
    return-void

    :cond_2
    sget v1, Lcom/google/android/apps/plus/R$id;->button_wifi_only:I

    if-ne v0, v1, :cond_3

    sget-object v1, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->WIFI_ONLY:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mSelection:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    goto :goto_0

    :cond_3
    sget v1, Lcom/google/android/apps/plus/R$id;->button_disable:I

    if-ne v0, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->DISABLE:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mSelection:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v5, 0x0

    sget v4, Lcom/google/android/apps/plus/R$layout;->oob_instant_upload_fragment:I

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mButtonTextViews:[Landroid/widget/TextView;

    sget-object v4, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->WIFI_AND_MOBILE:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->ordinal()I

    move-result v7

    sget v4, Lcom/google/android/apps/plus/R$id;->text_view_wifi_and_mobile:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    aput-object v4, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mButtonTextViews:[Landroid/widget/TextView;

    sget-object v4, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->WIFI_ONLY:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->ordinal()I

    move-result v7

    sget v4, Lcom/google/android/apps/plus/R$id;->text_view_wifi_only:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    aput-object v4, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mButtonTextViews:[Landroid/widget/TextView;

    sget-object v4, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->DISABLE:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->ordinal()I

    move-result v7

    sget v4, Lcom/google/android/apps/plus/R$id;->text_view_disable:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    aput-object v4, v6, v7

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mCheckMarks:[Landroid/view/View;

    sget-object v6, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->WIFI_AND_MOBILE:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->ordinal()I

    move-result v6

    sget v7, Lcom/google/android/apps/plus/R$id;->check_mark_wifi_and_mobile:I

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    aput-object v7, v4, v6

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mCheckMarks:[Landroid/view/View;

    sget-object v6, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->WIFI_ONLY:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->ordinal()I

    move-result v6

    sget v7, Lcom/google/android/apps/plus/R$id;->check_mark_wifi_only:I

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    aput-object v7, v4, v6

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mCheckMarks:[Landroid/view/View;

    sget-object v6, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->DISABLE:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->ordinal()I

    move-result v6

    sget v7, Lcom/google/android/apps/plus/R$id;->check_mark_disable:I

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    aput-object v7, v4, v6

    sget v4, Lcom/google/android/apps/plus/R$id;->button_wifi_and_mobile:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v4, Lcom/google/android/apps/plus/R$id;->button_wifi_only:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v4, Lcom/google/android/apps/plus/R$id;->button_disable:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p3, :cond_0

    const-string v4, "key_selection"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mSelection:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-string v6, "connectivity"

    invoke-virtual {v4, v6}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/ConnectivityManager;

    invoke-virtual {v4, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    if-nez v4, :cond_3

    const/4 v4, 0x1

    :goto_0
    if-eqz v4, :cond_2

    sget v4, Lcom/google/android/apps/plus/R$id;->section_wifi_and_mobile:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mSelection:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    sget-object v5, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->WIFI_AND_MOBILE:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    if-ne v4, v5, :cond_1

    sget-object v4, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->WIFI_ONLY:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mSelection:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    :cond_1
    sget v4, Lcom/google/android/apps/plus/R$drawable;->oob_white_top_radio_button_background:I

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->updateRadioButtons()V

    return-object v3

    :cond_3
    move v4, v5

    goto :goto_0
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->doNextStep()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/OobDeviceActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onContinue()V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "key_selection"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mSelection:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$Selection;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
