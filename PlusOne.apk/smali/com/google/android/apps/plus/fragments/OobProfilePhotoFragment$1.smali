.class final Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "OobProfilePhotoFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGetProfileAndContactComplete$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const-string v0, "OobProfilePhotoFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "OobProfilePhotoFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onGetProfileAndContactComplete(); requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->handleProfileServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->access$000(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    # setter for: Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mPhotoAdded:Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->access$302(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->updateViews()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->access$400(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->access$200(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;)V

    return-void
.end method

.method public final onInsertCameraPhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const-string v0, "OobProfilePhotoFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "OobProfilePhotoFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onInsertCameraPhotoComplete(); requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->handlerInsertCameraPhoto$b5e9bbb(I)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->access$500(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onUploadProfilePhotoComplete(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    const-string v0, "OobProfilePhotoFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "OobProfilePhotoFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onUploadProfilePhotoComplete(); requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->handleProfileServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p3}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->access$000(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, p2, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->getProfileAndContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v1

    # setter for: Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfilePendingRequestId:Ljava/lang/Integer;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->access$102(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->access$200(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;)V

    goto :goto_0
.end method
