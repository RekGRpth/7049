.class final Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment$AccountNamesAdapter;
.super Landroid/widget/ArrayAdapter;
.source "OobSelectPlusPageFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AccountNamesAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final mDrawablePadding:I

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;Landroid/content/Context;ILjava/util/List;)V
    .locals 2
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment$AccountNamesAdapter;->this$0:Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->oob_text_drawable_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment$AccountNamesAdapter;->mDrawablePadding:I

    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment$AccountNamesAdapter;->this$0:Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mHasGooglePlusProfile:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->access$200(Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment$AccountNamesAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_0

    instance-of v2, v1, Landroid/widget/CheckedTextView;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(I)V

    sget v2, Lcom/google/android/apps/plus/R$drawable;->add:I

    invoke-virtual {v0, v2, v3, v3, v3}, Landroid/widget/CheckedTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget v2, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment$AccountNamesAdapter;->mDrawablePadding:I

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setCompoundDrawablePadding(I)V

    :cond_0
    return-object v1
.end method
