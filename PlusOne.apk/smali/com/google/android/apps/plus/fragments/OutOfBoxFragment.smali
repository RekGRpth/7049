.class public Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;
.super Landroid/support/v4/app/Fragment;
.source "OutOfBoxFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/oob/ActionCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/OutOfBoxFragment$OobEsServiceListener;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final DIALOG_IDS:[Ljava/lang/String;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

.field private final mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mLastRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

.field private mOobFields:Landroid/view/ViewGroup;

.field private mOutOfBoxDialogInflater:Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;

.field private mOutOfBoxInflater:Lcom/google/android/apps/plus/oob/OutOfBoxInflater;

.field private mOutOfBoxResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mSignUpLayout:Landroid/view/ViewGroup;

.field private mUpgradeOrigin:Ljava/lang/String;

.field private mViewSwitcher:Landroid/widget/ViewSwitcher;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-class v0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->$assertionsDisabled:Z

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "sending"

    aput-object v3, v0, v2

    const-string v2, "net_failure"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "event"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "server_error"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->DIALOG_IDS:[Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment$OobEsServiceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment$OobEsServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method private close()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    return-void
.end method

.method public static createInitialTag()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 12
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v4, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->removeOutOfBoxResponse(I)Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->removeAccountSettingsResponse(I)Lcom/google/android/apps/plus/content/AccountSettingsData;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->close()V

    goto :goto_0

    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v4

    if-eqz v4, :cond_6

    :cond_3
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v1

    instance-of v4, v1, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v4, :cond_5

    check-cast v1, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/api/OzServerException;->getUserErrorMessage(Landroid/content/Context;)Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;

    move-result-object v4

    if-nez v4, :cond_4

    new-instance v4, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    sget v8, Lcom/google/android/apps/plus/R$string;->signup_title_no_connection:I

    sget v9, Lcom/google/android/apps/plus/R$string;->signup_error_network:I

    invoke-direct {v4, v5, v8, v9}, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;-><init>(Landroid/content/Context;II)V

    :cond_4
    iget-object v5, v4, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;->title:Ljava/lang/String;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;->message:Ljava/lang/String;

    sget v8, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v4, v8, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/support/v4/app/DialogFragment;->setCancelable(Z)V

    invoke-virtual {v4, p0, v6}, Landroid/support/v4/app/DialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "server_error"

    invoke-virtual {v4, v5, v6}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    sget v4, Lcom/google/android/apps/plus/R$string;->signup_title_no_connection:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->signup_error_network:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v7, Lcom/google/android/apps/plus/R$string;->signup_retry:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v5, v7, v8}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/support/v4/app/DialogFragment;->setCancelable(Z)V

    invoke-virtual {v4, p0, v6}, Landroid/support/v4/app/DialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "net_failure"

    invoke-virtual {v4, v5, v6}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    sget-object v9, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->DIALOG_IDS:[Ljava/lang/String;

    array-length v10, v9

    move v8, v6

    :goto_1
    if-ge v8, v10, :cond_8

    aget-object v4, v9, v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v11

    invoke-virtual {v11, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Landroid/support/v4/app/DialogFragment;

    if-eqz v4, :cond_7

    invoke-virtual {v4}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_7
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    iget-object v4, v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->signupComplete:Ljava/lang/Boolean;

    if-eqz v4, :cond_b

    iget-object v4, v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->signupComplete:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-static {v8}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v8}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-static {v8, v4, v3, v6}, Lcom/google/android/apps/plus/phone/Intents;->getNextOobIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v4

    :goto_2
    if-eqz v4, :cond_a

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_9
    move-object v4, v7

    goto :goto_2

    :cond_a
    const/4 v4, -0x1

    invoke-virtual {v8, v4}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v8}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_b
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->isDialog()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v4

    if-lez v4, :cond_e

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->popBackStack()V

    move v4, v5

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_c

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getTag()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$id;->oob_container:I

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mUpgradeOrigin:Ljava/lang/String;

    invoke-static {v8, v2, v9}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->newInstance(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;

    move-result-object v8

    invoke-virtual {v5, v7, v8, v6}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    if-eqz v4, :cond_d

    invoke-virtual {v5, v6}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    :cond_d
    invoke-virtual {v5}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto/16 :goto_0

    :cond_e
    move v4, v6

    goto :goto_3

    :cond_f
    move v4, v5

    goto :goto_3
.end method

.method private isDialog()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->view:Lcom/google/api/services/plusi/model/OutOfBoxView;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxView;->dialog:Lcom/google/api/services/plusi/model/OutOfBoxDialog;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p1    # Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "oob_resp"

    new-instance v3, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;

    invoke-direct {v3, p1}, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;-><init>(Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;)V

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "upgrade_origin"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private updateActionButtons()V
    .locals 11

    const/4 v9, 0x1

    const/4 v8, 0x0

    move v7, v8

    :goto_0
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    if-ge v7, v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->shouldPreventCompletionAction()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v6}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    move v3, v9

    :goto_1
    const/4 v4, 0x0

    :goto_2
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    if-ge v4, v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    invoke-virtual {v6, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    const-string v6, "CONTINUE"

    invoke-virtual {v2}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getActionType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    if-nez v3, :cond_3

    move v6, v9

    :goto_3
    invoke-virtual {v2, v6}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->setActionEnabled(Z)V

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_0

    :cond_2
    move v3, v8

    goto :goto_1

    :cond_3
    move v6, v8

    goto :goto_3

    :cond_4
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/BottomActionBar;->getButtons()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;

    const-string v6, "CONTINUE"

    iget-object v7, v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    if-nez v3, :cond_6

    move v6, v9

    :goto_5
    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_4

    :cond_6
    move v6, v8

    goto :goto_5

    :cond_7
    return-void
.end method


# virtual methods
.method public final onAction(Lcom/google/api/services/plusi/model/OutOfBoxAction;)V
    .locals 4
    .param p1    # Lcom/google/api/services/plusi/model/OutOfBoxAction;

    const-string v0, "URL"

    iget-object v1, p1, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxAction;->url:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->viewUrl(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "BACK"

    iget-object v1, p1, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->popBackStackImmediate()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->close()V

    goto :goto_0

    :cond_2
    const-string v0, "CLOSE"

    iget-object v1, p1, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->close()V

    goto :goto_0

    :cond_3
    new-instance v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->input:Ljava/util/List;

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v3

    iget-object v3, v3, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    if-eqz v3, :cond_4

    iget-object v3, v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->input:Ljava/util/List;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->newFieldFromInput()Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_5
    new-instance v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OutOfBoxAction;-><init>()V

    iput-object v0, v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    iget-object v0, v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->sendOutOfBoxRequest(Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)V

    goto :goto_0
.end method

.method public final onActionId(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    const-string v2, "BACK"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->popBackStackImmediate()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->close()V

    :cond_0
    :goto_1
    return-void

    :pswitch_0
    const-string v0, "CLOSE"

    goto :goto_0

    :pswitch_1
    const-string v0, "CONTINUE"

    goto :goto_0

    :pswitch_2
    const-string v0, "URL"

    goto :goto_0

    :pswitch_3
    const-string v0, "BACK"

    goto :goto_0

    :cond_1
    const-string v2, "CLOSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->close()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v2, "OutOfBoxFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to parse actionId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", not calling action on this event."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_2
    :try_start_1
    new-instance v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;-><init>()V

    new-instance v3, Lcom/google/api/services/plusi/model/OutOfBoxAction;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/OutOfBoxAction;-><init>()V

    iput-object v0, v3, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->sendOutOfBoxRequest(Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "account"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "oob_resp"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;->getResponse()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "upgrade_origin"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mUpgradeOrigin:Ljava/lang/String;

    sget v3, Lcom/google/android/apps/plus/R$layout;->out_of_box_fragment:I

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$id;->switcher:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ViewSwitcher;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    sget v3, Lcom/google/android/apps/plus/R$id;->signup_layout:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mSignUpLayout:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/apps/plus/R$id;->signup_items:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/apps/plus/R$id;->bottom_bar:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/BottomActionBar;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

    new-instance v3, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mSignUpLayout:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;-><init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Lcom/google/android/apps/plus/views/BottomActionBar;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxInflater:Lcom/google/android/apps/plus/oob/OutOfBoxInflater;

    new-instance v4, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    sget v3, Lcom/google/android/apps/plus/R$id;->dialog_content:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->view:Lcom/google/api/services/plusi/model/OutOfBoxView;

    invoke-direct {v4, v5, v3, v6, p0}, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;-><init>(Landroid/support/v4/app/FragmentActivity;Landroid/view/ViewGroup;Lcom/google/api/services/plusi/model/OutOfBoxView;Lcom/google/android/apps/plus/oob/ActionCallback;)V

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxDialogInflater:Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->isDialog()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxDialogInflater:Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->inflate()V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    sget v5, Lcom/google/android/apps/plus/R$id;->dialog_frame:I

    invoke-virtual {v4, v5}, Landroid/widget/ViewSwitcher;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ViewSwitcher;->indexOfChild(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v4, v3}, Landroid/widget/ViewSwitcher;->setDisplayedChild(I)V

    :goto_0
    if-eqz p3, :cond_1

    const-string v3, "last_request"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->getRequest()Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mLastRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    :cond_0
    const-string v3, "reqid"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "reqid"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_1
    return-object v2

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxInflater:Lcom/google/android/apps/plus/oob/OutOfBoxInflater;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->view:Lcom/google/api/services/plusi/model/OutOfBoxView;

    invoke-virtual {v3, v4, p0}, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->inflateFromResponse(Lcom/google/api/services/plusi/model/OutOfBoxView;Lcom/google/android/apps/plus/oob/ActionCallback;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->updateActionButtons()V

    goto :goto_0
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "OOB dialog not cancelable"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-boolean v0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-string v0, "net_failure"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->close()V

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const-string v0, "net_failure"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mLastRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mLastRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->sendOutOfBoxRequest(Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "server_error"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->close()V

    goto :goto_0
.end method

.method public final onInputChanged$7c32a9fe()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->updateActionButtons()V

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public final onResume()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "last_request"

    new-instance v1, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mLastRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;-><init>(Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "reqid"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public final sendOutOfBoxRequest(Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)V
    .locals 3
    .param p1    # Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    const/4 v0, 0x0

    sget v1, Lcom/google/android/apps/plus/R$string;->signup_sending:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "sending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mUpgradeOrigin:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->upgradeOrigin:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mLastRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->sendOutOfBoxRequest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    return-void
.end method
