.class public Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;
.super Landroid/support/v4/app/Fragment;
.source "ParticipantsGalleryFragment.java"


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mBackgroundColor:Ljava/lang/Integer;

.field private mCommandListener:Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;

.field private mEmptyMessage:Ljava/lang/String;

.field private mParticipantListButtonVisibility:Z

.field private mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mParticipantListButtonVisibility:Z

    return-void
.end method


# virtual methods
.method public final addParticipants(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->addParticipant(Landroid/view/LayoutInflater;Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final getParticipantsGalleryView()Lcom/google/android/apps/plus/views/ParticipantsGalleryView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    new-instance v0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mBackgroundColor:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mBackgroundColor:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setBackgroundColor(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mEmptyMessage:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mEmptyMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setEmptyMessage(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mCommandListener:Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mCommandListener:Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setCommandListener(Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mParticipantListButtonVisibility:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setParticipantListButtonVisibility(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    return-object v0
.end method

.method public final onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # Landroid/os/Bundle;

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-object v1, Lcom/google/android/apps/plus/R$styleable;->ParticipantsGalleryFragment:[I

    invoke-virtual {p1, p2, v1}, Landroid/app/Activity;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mBackgroundColor:Ljava/lang/Integer;

    :cond_0
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mEmptyMessage:Ljava/lang/String;

    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->dismissAvatarMenuDialog()V

    return-void
.end method

.method public final removeAllParticipants()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->removeAllParticipants()V

    return-void
.end method

.method public final setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_0
    return-void
.end method

.method public final setCommandListener(Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mCommandListener:Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setCommandListener(Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;)V

    :cond_0
    return-void
.end method

.method public final setParticipantListButtonVisibility(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mParticipantListButtonVisibility:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setParticipantListButtonVisibility(Z)V

    :cond_0
    return-void
.end method

.method public final setParticipants(Ljava/util/HashMap;Ljava/util/HashSet;Ljava/util/HashSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->mView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setParticipants(Ljava/util/HashMap;Ljava/util/HashSet;Ljava/util/HashSet;)V

    :cond_0
    return-void
.end method
