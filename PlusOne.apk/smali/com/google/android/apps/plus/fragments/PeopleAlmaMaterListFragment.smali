.class public Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;
.super Lcom/google/android/apps/plus/fragments/PeopleListFragment;
.source "PeopleAlmaMaterListFragment.java"


# instance fields
.field private mEndYear:Ljava/lang/Integer;

.field private mListType:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private final mPeopleSuggestionsLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/google/api/services/plusi/model/PeopleViewDataResponse;",
            ">;"
        }
    .end annotation
.end field

.field private mStartYear:Ljava/lang/Integer;

.field private mSuggestionsAdapter:Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mPeopleSuggestionsLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mPeopleSuggestionsLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mListType:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mListType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mStartYear:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mEndYear:Ljava/lang/Integer;

    return-object v0
.end method


# virtual methods
.method public final bind(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/api/services/plusi/model/PeopleViewPerson;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mSuggestionsAdapter:Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->bindPeopleViewPerson(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mSuggestionsAdapter:Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_IN_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final onAddToCircleForceLoadLoaderId()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v2, "list_type"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mListType:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "people_view_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mName:Ljava/lang/String;

    const-string v2, "people_view_start_year"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "people_view_start_year"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mStartYear:Ljava/lang/Integer;

    :cond_1
    const-string v2, "people_view_end_year"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "people_view_end_year"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mEndYear:Ljava/lang/Integer;

    :cond_2
    new-instance v2, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;-><init>(Lcom/google/android/apps/plus/fragments/PeopleListFragment;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mSuggestionsAdapter:Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mPeopleSuggestionsLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v1, v2, v3, v4}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mSuggestionsAdapter:Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->setAdapter(Landroid/widget/ListAdapter;)V

    return-object v0
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "list_type"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;->mListType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
