.class final Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$1;
.super Ljava/lang/Object;
.source "PeopleCelebritiesListFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->mLoaderIsActive:Z

    new-instance v0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->mCelebrityCategory:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->access$000(Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;)Ljava/lang/String;

    const-wide/32 v3, 0x7fffffff

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->mLoaderIsActive:Z

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    iget-object v0, p2, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;->category:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->bindCelebrities(Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
