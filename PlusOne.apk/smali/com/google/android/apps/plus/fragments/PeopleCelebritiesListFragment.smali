.class public Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;
.super Lcom/google/android/apps/plus/fragments/PeopleListFragment;
.source "PeopleCelebritiesListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;
    }
.end annotation


# instance fields
.field mCelebrityAdapter:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;

.field private mCelebrityCategory:Ljava/lang/String;

.field private final mCelebritySuggestionsLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->mCelebritySuggestionsLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->mCelebrityCategory:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final bindCelebrities(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->mCelebrityAdapter:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->mCelebrityCategory:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->bind(Ljava/util/ArrayList;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->mCelebrityAdapter:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->invalidateActionBar()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method protected final onAddToCircleForceLoadLoaderId()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v4, Lcom/google/android/apps/plus/R$id;->view_all:I

    if-ne v1, v4, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$id;->people_suggestion_type:I

    invoke-virtual {p1, v4}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget v4, Lcom/google/android/apps/plus/R$id;->people_suggestion_data:I

    invoke-virtual {p1, v4}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lcom/google/android/apps/plus/phone/Intents;->getCelebrityCategorySuggestionsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onClick(Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v1, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;-><init>(Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;B)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->mCelebrityAdapter:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->mCelebritySuggestionsLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, p1, v2}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->mCelebrityAdapter:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->setAdapter(Landroid/widget/ListAdapter;)V

    return-object v0
.end method

.method public final onPause()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onPause()V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->mCelebrityAdapter:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->mCelebrityAdapter:Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment$CelebritiesAdapter;->getSingleCategoryName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    return-void

    :cond_0
    sget v1, Lcom/google/android/apps/plus/R$string;->find_people_all_interesting:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onResume()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/content/Loader;->forceLoad()V

    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onSetArguments(Landroid/os/Bundle;)V

    const-string v0, "category_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;->mCelebrityCategory:Ljava/lang/String;

    return-void
.end method
