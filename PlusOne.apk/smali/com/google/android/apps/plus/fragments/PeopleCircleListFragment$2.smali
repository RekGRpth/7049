.class final Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;
.super Ljava/lang/Object;
.source "PeopleCircleListFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mLoaderIsActive:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->access$100(Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "15"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/fragments/BlockedPeopleListLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->CIRCLE_MEMBERS_PROJECTION:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mShownPersonIds:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->access$200(Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/BlockedPeopleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/util/ArrayList;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleListLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->CIRCLE_MEMBERS_PROJECTION:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleId:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->access$100(Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "interaction_sort_key DESC"

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/PeopleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mLoaderIsActive:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mShownPersonIds:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->access$200(Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mCircleId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->access$100(Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "15"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->mShownPersonIds:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->access$200(Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;->bindCircleMembers(Landroid/database/Cursor;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
