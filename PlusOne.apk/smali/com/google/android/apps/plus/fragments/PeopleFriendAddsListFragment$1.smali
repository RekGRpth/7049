.class final Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment$1;
.super Ljava/lang/Object;
.source "PeopleFriendAddsListFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/google/api/services/plusi/model/PeopleViewDataResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/api/services/plusi/model/PeopleViewDataResponse;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;->mLoaderIsActive:Z

    new-instance v5, Lcom/google/api/services/plusi/model/DataRequestParameter;

    invoke-direct {v5}, Lcom/google/api/services/plusi/model/DataRequestParameter;-><init>()V

    const-string v0, "FRIEND_ADDS"

    iput-object v0, v5, Lcom/google/api/services/plusi/model/DataRequestParameter;->listType:Ljava/lang/String;

    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v5, Lcom/google/api/services/plusi/model/DataRequestParameter;->maxResults:Ljava/lang/Integer;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v5, Lcom/google/api/services/plusi/model/DataRequestParameter;->preferPhotos:Ljava/lang/Boolean;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    const-wide/32 v3, 0x7fffffff

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/api/services/plusi/model/DataRequestParameter;)V

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 3
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v2, 0x0

    check-cast p2, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;

    iput-boolean v2, v0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;->mLoaderIsActive:Z

    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;->listResponse:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;

    iget-object v0, p2, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;->listResponse:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/ListResponse;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ListResponse;->people:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;->bind(Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/api/services/plusi/model/PeopleViewDataResponse;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
