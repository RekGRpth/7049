.class public Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;
.super Lcom/google/android/apps/plus/fragments/PeopleListFragment;
.source "PeopleFriendAddsListFragment.java"


# instance fields
.field private final mPeopleSuggestionsLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/google/api/services/plusi/model/PeopleViewDataResponse;",
            ">;"
        }
    .end annotation
.end field

.field private mSuggestionsAdapter:Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;->mPeopleSuggestionsLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    return-void
.end method


# virtual methods
.method public final bind(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/api/services/plusi/model/PeopleViewPerson;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;->mSuggestionsAdapter:Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->bindPeopleViewPerson(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;->mSuggestionsAdapter:Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_IN_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final onAddToCircleForceLoadLoaderId()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v1, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;-><init>(Lcom/google/android/apps/plus/fragments/PeopleListFragment;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;->mSuggestionsAdapter:Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;->mPeopleSuggestionsLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;->mSuggestionsAdapter:Lcom/google/android/apps/plus/fragments/PeopleListFragment$DataSuggestedPersonAdapter;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;->setAdapter(Landroid/widget/ListAdapter;)V

    return-object v0
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    sget v0, Lcom/google/android/apps/plus/R$string;->find_people_friends_add:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    return-void
.end method
