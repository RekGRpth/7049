.class final Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;
.super Landroid/widget/BaseAdapter;
.source "PeopleListDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PeopleListAdapter"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/AudienceData;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/AudienceData;

    const/4 v11, 0x1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v7, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;

    invoke-direct {v7, v5}, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;-><init>(Lcom/google/android/apps/plus/content/PersonData;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/AudienceData;->getHiddenUserCount()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$plurals;->audience_hidden_user_count:I

    new-array v8, v11, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v1, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v7, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;

    invoke-direct {v7, v11, v2}, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;-><init>(ILjava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;

    iget v0, v0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;->mType:I

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v8, 0x0

    if-nez p2, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;->mContext:Landroid/content/Context;

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    sget v6, Lcom/google/android/apps/plus/R$layout;->acl_row_view:I

    invoke-virtual {v3, v6, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    :goto_0
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;

    invoke-virtual {v5, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    sget v6, Lcom/google/android/apps/plus/R$id;->avatar:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iget v6, v2, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;->mType:I

    packed-switch v6, :pswitch_data_0

    :goto_1
    sget v6, Lcom/google/android/apps/plus/R$id;->name:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v6, v2, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;->mContent:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v5

    :cond_0
    move-object v5, p2

    goto :goto_0

    :pswitch_0
    iget-object v6, v2, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;->mPerson:Lcom/google/android/apps/plus/content/PersonData;

    if-eqz v6, :cond_1

    iget-object v6, v2, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;->mPerson:Lcom/google/android/apps/plus/content/PersonData;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, v2, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;->mPerson:Lcom/google/android/apps/plus/content/PersonData;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getCompressedPhotoUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v6, v2, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;->mPerson:Lcom/google/android/apps/plus/content/PersonData;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    goto :goto_1

    :pswitch_1
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method
