.class public Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "PeopleListDialogFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;,
        Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAdapter:Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;->dismiss()V

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v4, Lcom/google/android/apps/plus/R$layout;->list_layout_acl:I

    invoke-virtual {p1, v4, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const v4, 0x102000a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    const-string v4, "audience"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/AudienceData;

    const-string v4, "account"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v4, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v4, v5, v2}, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/AudienceData;)V

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    sget v4, Lcom/google/android/apps/plus/R$id;->ok:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v4, Lcom/google/android/apps/plus/R$id;->cancel:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v4

    const-string v5, "people_list_title"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    return-object v3
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;

    invoke-virtual {v5, p3}, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;

    iget v5, v2, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;->mType:I

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v3, v2, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment$PeopleListItem;->mPerson:Lcom/google/android/apps/plus/content/PersonData;

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "g:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_2
    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v5, v6, v4, v7, v8}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;->dismiss()V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PeopleListDialogFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
