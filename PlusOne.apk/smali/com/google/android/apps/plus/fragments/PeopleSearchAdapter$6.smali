.class final Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$6;
.super Landroid/widget/Filter;
.source "PeopleSearchAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$6;->this$0:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method public final convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 6
    .param p1    # Ljava/lang/Object;

    const/4 v5, -0x1

    move-object v1, p1

    check-cast v1, Landroid/database/Cursor;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const-string v4, ""

    :goto_0
    return-object v4

    :cond_1
    const-string v4, "circle_name"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v5, :cond_2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_2
    const-string v4, "name"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    if-eq v3, v5, :cond_3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_3
    const-string v4, "address"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v5, :cond_4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_4
    const-string v4, ""

    goto :goto_0
.end method

.method protected final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 3
    .param p1    # Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$6;->this$0:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    # invokes: Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->releaseLatch()V
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->access$200(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$6;->this$0:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    # setter for: Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterLatch:Ljava/util/concurrent/CountDownLatch;
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->access$302(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;Ljava/util/concurrent/CountDownLatch;)Ljava/util/concurrent/CountDownLatch;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$6;->this$0:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    # getter for: Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->access$500(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$6$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$6$1;-><init>(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$6;Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$6;->this$0:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterLatch:Ljava/util/concurrent/CountDownLatch;
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->access$302(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;Ljava/util/concurrent/CountDownLatch;)Ljava/util/concurrent/CountDownLatch;

    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    return-object v1

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/widget/Filter$FilterResults;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$6;->this$0:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getCount()I

    move-result v0

    iput v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    return-void
.end method
