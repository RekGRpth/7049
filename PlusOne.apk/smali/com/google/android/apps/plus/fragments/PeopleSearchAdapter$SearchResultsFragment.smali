.class public Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;
.super Landroid/support/v4/app/Fragment;
.source "PeopleSearchAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SearchResultsFragment"
.end annotation


# instance fields
.field private mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;->setRetainInstance(Z)V

    return-void
.end method


# virtual methods
.method public final getPeopleSearchResults()Lcom/google/android/apps/plus/fragments/PeopleSearchResults;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    return-object v0
.end method

.method public final setPeopleSearchResults(Lcom/google/android/apps/plus/fragments/PeopleSearchResults;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    return-void
.end method
